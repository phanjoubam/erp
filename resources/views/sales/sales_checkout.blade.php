@extends('layouts.erp_theme_03')
@section('content')
<?php 
   $cdn_url =    config('app.app_cdn') ;  
    $cdn_path =     config('app.app_cdn_path') ; // cnd - /var/www/html/api/public 
    $total_cost = $delivery_cost = 0.0; 
    $cartno = $data['cartno'];
    $new_cartno = $data['new_cartno'];

    if( $new_cartno > $cartno)
    {
       $carttoupdate= $new_cartno;
    }
    else 
    {
       $carttoupdate =  $cartno;
    }

    $allcarts = $data['total_temp_cart']; 
?>


<div class="container">
		<span id="spanErronEnter" class="necessary-field"></span>
 
    <div class="card">
  <div class="card-header">
     <div class='row'>
          <div class='col-md-6'>
            <h4 class="card-title">Order Checkout For Cart # <span class='badge badge-success  '>{{  $carttoupdate  }}</span></h4> 
          </div>
          <div class='col-md-6 text-right'>
            @php 
             $pos = 1;
            @endphp 
            @foreach($allcarts as $cart)

              @if( $pos == 1  )
                @php 
                  $color_code = "danger";
                @endphp  
              @else 
                  @if($pos == 2)
                      @php 
                        $color_code = "warning";
                      @endphp 
                  @else 
                    @if($pos ==3 )
                        @php 
                          $color_code = "info";
                        @endphp 
                    @else 
                      @if($pos == 4 )
                          @php 
                            $color_code = "success";
                          @endphp 
                      @else 
                        @php 
                          $color_code = "info";
                        @endphp  
                      @endif 
                    @endif 
                  @endif  
              @endif 
              @php 
               $pos++;
              @endphp 
                
                <a class="btn btn-{{ $color_code }} btn-sm" href="{{ URL::to('/pos/checkout-order')}}?enccno={{ Crypt::encrypt( $cart->cartNo ) }}">Cart # {{ $cart->cartNo }}</a> 
            @endforeach

 
            <a class='btn btn-primary btn-sm'  href="{{URL::to('/pos/prepare-sales-window')}}">
              <i class='fa fa-plus-circle'></i>
            </a>

            <a type="button"  data-widget='w1' data-param="{{  $data['cartno']  }}"  class="btn btn-danger  btn-sm btnremove">
                <i class='fa fa-trash'></i>
             </a>  
 

             <a type="button" href="{{ URL::to('/pos/checkout-order')}}?enccno={{ Crypt::encrypt( $data['cartno'] ) }}" class="btn btn-success btn-sm btnrefresh"><i class='fa fa-sync'></i></a>


          </div>
        </div>
  </div>
  <div class="card-body"> 


    @if( isset( $data['cart_items']  ) )
        @php 
          $total_cost =0;
          $memid = 0;
          $outofstockfound= false;
          $item_total =0;
          $package_total  =0;
          $cgst_pc =0;
          $sgst_pc =0;
         @endphp

  <form method="post" action="{{ action('Erp\PosController@placePosOrder') }}"> 
    {{ csrf_field() }}

   <div class='row'>
    <div class='col-md-7'>  
      <table class="table table-colored"> 
         <tr>
                <th width='90px'></th>
                <th style='width: 250px; text-align:left' >Item</th> 
                <th class='text-right' style="width: 250px;">Item + Packing</th>
            </tr>  
          @php 
            $img_url = $cdn_url .  "/assets/image/no-image.jpg";  
          @endphp 
        @foreach( $data['cart_items']  as $cartitem)

        <tr>
          <td width='90px'>

            @foreach($data['photos'] as $photo)
              @if($cartitem->subid == $photo->prsubid)
               @php 
                $img_url = $photo->image_url;
                @endphp
                @break
              @endif
            @endforeach

            <img class='rounded ' width='40px' src="{{  $img_url  }}" alt='{{ $cartitem->subid }}'/>


          </td>
          <td style='width:250px; text-align:left' >
            @php 
              $product_name = "N/A";
            @endphp
             @foreach($data['products'] as $product)
              @if($cartitem->subid == $product->prsubid)
                @php 
                  $product_name = $product->pr_name;
                @endphp
                @break
              @endif
            @endforeach

            <strong>{{ $product_name }}</strong> <br/> 
            @if(  $cartitem->stock_inhand == 0  )
            <button type="button"   class="btn btn-outline-primary btn-sm  " disabled>Out of stock</button>
            &nbsp;
            <button type="button"   class='btn btn-danger btn-sm btndel' data-key="{{ $cartitem->key }}"><i class='fa fa-trash'></i></button>
            </td>
            <td class='text-right'>0.00</td> 
            @php  
          $outofstockfound= true;
         @endphp 

            @else  
                <div class="input-group input-group-sm " style='width:200px;'>
                    <div class="input-group-prepend">
                            <button type="button" class="input-group-text btnupdatecart" 
                            data-prid="{{  $cartitem->subid }}" 
                            data-subid="{{  $cartitem->subid }}"  
                            data-bin="{{  $data['bin'] }}"   
                            data-step='rem' 
                            data-key="{{ $cartitem->key }}">-</button> 
                          </div>
                          <input type="number" class="form-control qty" aria-label="Quantity" step="1" min="1" max="" name="quantity" id='tbqty{{ $cartitem->key }}' value="{{ $cartitem->qty }}" title="Qty" >
                          <div class="input-group-append">
                           <button type="button" class="input-group-text btnupdatecart" 
                            data-prid="{{  $cartitem->subid }}" 
                            data-subid="{{  $cartitem->subid }}"  
                            data-bin="{{  $data['bin'] }}"  
                            data-step='add' data-key="{{ $cartitem->key }}">+</button> 
                          </div>
                    </div> 
                </td>  
                   
                <td class='text-right'>{{ ( $cartitem->actual_price *  $cartitem->qty ) + 
                    ($cartitem->packaging * $cartitem->qty)  }}</td>
            

             @php 
                
                $item_total  += ( $cartitem->actual_price *  $cartitem->qty )  ;  

                $package_total  +=  ($cartitem->packaging * $cartitem->qty) ;  
                $cgst_pc += ( $cartitem->actual_price *  $cartitem->qty ) + ($cartitem->packaging * $cartitem->qty) ;  
                $sgst_pc += ( $cartitem->actual_price *  $cartitem->qty ) + ($cartitem->packaging * $cartitem->qty) ; 


             @endphp   

        @endif  
        </tr>                  
        @endforeach
      </table>
 

    </div> 
    <div class='col-md-5'>

      <div class="form-group row">
        <label for="tbitemcost" class="col-sm-6 col-form-label">Item Cost:</label>
        <div class="col-sm-6">
          <input type='number' min='0' step='0.05' value='{{ $item_total   }}' id='tbitemcost' readonly name='tbitemcost' class='form-control form-control-sm' />
        </div>
      </div>

      <div class="form-group row">
        <label for="tbpackaging" class="col-sm-6 col-form-label">Packaging Cost:</label>
        <div class="col-sm-6">
          <input type='number' min='0' step='0.05' value='{{ $package_total   }}' id='tbpackaging' readonly name='tbpackaging' class='form-control form-control-sm' />
        </div>
      </div>

      <div class="form-group row">
        <label for="tbtotalamount" class="col-sm-6 col-form-label">Total Amount:</label>
        <div class="col-sm-6">
          <input type='number' min='0' step='0.05' value='{{ $item_total + $package_total   }}' id='tbtotalamount' readonly name='tbtotalamount' class='form-control form-control-sm' />
        </div>
      </div>

      <div class="form-group row">
          <label for="tbdiscount" class="col-sm-6 col-form-label">Discount:</label>
          <div class="col-sm-6">
             <input type='number' min='0' step='0.05' value='0.00' name='tbdiscount' id='tbdiscount' class='form-control form-control-sm' />
          </div>
        </div>

        <div class="form-group row">
          <label for="extracost" class="col-sm-6 col-form-label">Extra Charge:</label>
          <div class="col-sm-6">
             <input type='number' step='.05' min='0'  name='extracost' id='extracost' class='form-control form-control-sm'  
                 value='0.00'   />
          </div>
        </div>

        <div class="form-group row">
          <label for="tbcgst" class="col-sm-6 col-form-label">CGST %:</label>
          <div class="col-sm-6">
             <input type='number' min='0' step='0.05' value='0.00' id='tbcgst'  name='tbcgst' class='form-control form-control-sm' />
          </div>
        </div>

        <div class="form-group row">
          <label for="tbsgst" class="col-sm-6 col-form-label">SGST %:</label>
          <div class="col-sm-6">
             <input type='number' min='0' step='0.05' value='0.00' id='tbsgst'  name='tbsgst' class='form-control form-control-sm' />
          </div>
        </div>
           
      <div class="form-group row">
          <label for="tbtotalcost" class="col-sm-6 col-form-label">Total Order Cost:</label>
          <div class="col-sm-6">
             <input type='number' min='0' step='0.05' value='{{ $item_total  +  $package_total  }}' id='tbtotalcost'  name='tbtotalcost' class='form-control form-control-sm' />
          </div>
        </div>
      <div class="form-group row">
          <label for="radiocash" class="col-sm-6 col-form-label">Payment Type:</label>
          <div class="col-sm-6">
             <div class="custom-control custom-radio custom-control-inline">
  <input type="radio" id="radiocash" name="paymode" class="custom-control-input" value="cash" checked>
  <label class="custom-control-label" for="radiocash">Cash</label>
</div>
<div class="custom-control custom-radio custom-control-inline">
  <input type="radio" id="radioonline" name="paymode" class="custom-control-input"  value="online">
  <label class="custom-control-label" for="radioonline">Online</label>
</div>
          </div>
        </div>

     <div class="form-group row">
        <label for="cname" class="col-sm-6 col-form-label">Customer Name:</label>
        <div class="col-sm-6">
          <input type='text' name='cname' id='cname' class='form-control form-control-sm' value='customer name' />
        </div>
      </div>

      <div class="form-group row">
        <label for="cphone" class="col-sm-6 col-form-label">Customer Phone:</label>
        <div class="col-sm-6">
         <input type='text' name='cphone' id='cphone' class='form-control form-control-sm' required  placeholder='phone'/> 
        </div>
      </div>
 
      <div class="form-group row">
        <label for="caddress" class="col-sm-6 col-form-label">Address:</label>
        <div class="col-sm-6">
         <textarea type='text' rows='2' id='caddress'  name='caddress' class='form-control form-control-sm' ></textarea>
        </div>
      </div>
       
      <div class="form-group row">
        <label for="remarks" class="col-sm-6 col-form-label">Any order remarks:</label>
        <div class="col-sm-6">
         <textarea type='text' rows='3'  id='remarks'  name='remarks' class='form-control form-control-sm' ></textarea>
        </div>
      </div>
    <div class="form-group row">
        <label for="date" class="col-sm-6 col-form-label">Order Date:</label>
        <div class="col-sm-6">
         <input type='text' name='date' id='date' class='form-control form-control-sm calendar' required  placeholder='Order Date'/>
        </div>
      </div>
       
      <input type='hidden' name='bin' value='{{ $data["bin"] }}' />   
        @if( $outofstockfound  )
            <button type='button'  class="btn btn-primary" disabled>Place Order</button> 
        @else  
            <button type='submit' value='placeorder' name='btnorder'  class="btn btn-primary">Place Order</button> 
        @endif
        <input type="hidden" name="enccno" value="{{ Crypt::encrypt($data['cartno']) }}" />
        <input type='hidden' id="newcartno" name='newcartno' value='{{ $carttoupdate }}' />  

    </div>
   </div>
    @endif    
    </div>
 </div> 
</form>  
</div>
 


 <form method="post" action="{{ action('Erp\PosController@removeItemFromSalesWindow') }}"> 
    {{ csrf_field() }}
<div class="modal wgconfirm" tabindex="-1">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Confirm Item Removal</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Selected item will be removed from cart. Are you sure?</p>
      </div>
      <div class="modal-footer">
         <input type="hidden" name="key" id="delkey" />
         <input type="hidden" name="enccno" value="{{ Crypt::encrypt($data['cartno']) }}" />
        <button type="button" class="btn btn-primary" data-dismiss="modal">No</button>
        <button type="submit" value='delete' name='btndelete' class="btn btn-danger">Yes</button>
      </div>
    </div>
  </div>
</div>
</form>

 <form method="post" action="{{ action('Erp\PosController@removeTemporaryCart') }}"> 
    {{ csrf_field() }}
<div class="modal wgcartrem" tabindex="-1">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Confirm Cart Removal</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Selected cart will be removed. Are you sure?</p>
      </div>
      <div class="modal-footer"> 
         <input type="hidden" name="uitype" value="modern"/>
         <input type="hidden" name="enccno" value="{{ Crypt::encrypt($data['cartno']) }}" /> 
        <button type="submit" value='delete' name='btndelete' class="btn btn-danger">Delete</button>
        <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
      </div>
    </div>
  </div>
</div>
</form>

<div class="modal wgsystem" tabindex="-1">
  <div class="modal-dialog modal-dialog-centered modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">System Notification</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <span id='msgarea'></span>
      </div>
      <div class="modal-footer"> 
         <input type="hidden" name="uitype" value="modern"/>
         <input type="hidden" name="enccno" value="{{ Crypt::encrypt($data['cartno']) }}" />  
        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>


@endsection
@section("script")

<script>

window.onload = function() {
  document.getElementById("key").focus();
};


$('#key').on('change',function(e){
  addItemToCart();
});
 
$('#tbdiscount').on('input',function(e){
    recalcTotal();
});

 
$('#extracost').on('input',function(e){
   recalcTotal();
});

$('#tbcgst').on('input',function(e){
    recalcTotal();
});

$('#tbsgst').on('input',function(e){
    recalcTotal();
});


function recalcTotal()
{
   $total = $('#tbitemcost').val();
   $packaging = $('#tbpackaging').val();
   $discount  = $('#tbdiscount').val();
   $extra  = eval($('#extracost').val()) ;
   $totalamount = eval( $total )  -  eval($discount)   +  $extra; 
   $cgst = eval($('#tbcgst').val());
   $sgst = eval($('#tbsgst').val());  
   $totaltaxed = $totalamount + eval( $packaging )  + (( $cgst + $sgst )*$totalamount) /  100;  
   $('#tbtotalcost').val(  $totaltaxed);  
}



$(document).on("click", ".btnremove", function() {
 
  $(".wgcartrem").modal('show');
}); 

 

$(document).on("click", ".btndel", function() {

  $("#delkey").val($(this).attr("data-key"));
  $(".wgconfirm").modal('show');
});

   

//adding item to temp 
$(document).on("click", ".btnaddproduct", function()
{
  addItemToCart();
})

//adding item to temp 
$(document).on("click", ".btnupdatecart", function()
{ 

  var subid =  $(this).attr("data-subid") ; 
  var qty =  1;
  var action = $(this).attr("data-step") ; 
  var cartno = $("#newcartno").val(); 
  var gbin = $("#gbin").html();
  var guid = $("#guid").html(); 

  var json = {};
  json['guid'] =  guid  ;
  json['gbin'] =  gbin ; 
  json['subid'] = subid; 
  json['qty'] =  qty ;
  json['action'] =  action ;
  json['cartno'] =  cartno ;
  json['uitype'] = "modern" ;
 

  $.ajax({

    type: 'post',
    url: erp  + "api/v3/pos/add-item-to-basket" ,
    data: json,
    success: function(data)
    {
      data = $.parseJSON(data);  
      if(data.status_code == 6001){

       window.location.href =  "<?php echo URL::to('/pos/prepare-sales-window')  ?>?enccno=" + data.enccno;
      }
      else{
         
        $("#msgarea").html( data.detailed_msg);
        $(".wgsystem").modal('show'); 
      }
      },
      error: function( ) 
      {
         $("#msgarea").html(  'An error while connecting the server, please try again after checking Internet connection!' ); 
      }
    });

}) 


function addItemToCart()
{
  var subid =  $("#key").val(); 
  if(subid == "") return;
  var qty =  1;
  var action = "add"; 
  var cartno = $("#newcartno").val(); 
  var gbin = $("#gbin").html();
  var guid = $("#guid").html(); 

  var json = {};
  json['guid'] =  guid  ;
  json['gbin'] =  gbin ; 
  json['subid'] = subid; 
  json['qty'] =  qty ;
  json['action'] =  action ;
  json['cartno'] =  cartno ;
  json['uitype'] = "modern" ;

  $.ajax({

    type: 'post',
    url: erp  + "api/v3/pos/add-item-to-basket" ,
    data: json,
    success: function(data)
    {
      data = $.parseJSON(data);  
      if(data.status_code == 6001){

       window.location.href =  "<?php echo URL::to('/pos/prepare-sales-window')  ?>?enccno=" + data.enccno;
      }
      else{
         
        $("#msgarea").html( data.detailed_msg);
        $(".wgsystem").modal('show'); 
      }
      },
      error: function( ) 
      {
         $("#msgarea").html(  'An error while connecting the server, please try again after checking Internet connection!' ); 
      }
    });
}
 

$(document).on("click", ".btnplusminus", function()
{
    var step = $(this).attr("data-step");  
    var key = $(this).attr("data-key");
    var oldValue = parseFloat($("#tbqty"+key).val());
    var stockValue = parseFloat($("#stock_quantity"+key).val());
    var amount = parseFloat($("#price"+key).val());
    var discount = parseFloat($("#discount").val());
    var delivery_charge = parseFloat($("#delivery_charge").val());
    
    
    //alert($(this).attr("data-key"));
       
    if ($(this).attr("data-action") == "increase"+key ) {

        	
        	if(oldValue==stockValue)
        	{
        		alert("You cannot add more quantity for the selected product");
        		$('#tbqty'+key).val(stockValue);
        		return;
        	}else{

        	var newVal = parseFloat(oldValue) + 1;

        	var total = parseFloat(newVal) * parseFloat(amount);

        	$("#product_amount"+key).text(total);
    			$("#amount"+key).val(total);

          var total_=parseFloat(0);
          $(".totalamount").each(function () {
            total_ += parseFloat($(this).val());
          });

          
    			$("#total_amount").val(parseFloat(discount)+parseFloat(delivery_charge)+parseFloat(total_));
        }

      } else {
            // Don't allow decrementing below 1
            if (oldValue > 1) {
            var newVal = parseFloat(oldValue) - 1;

            var total = parseFloat(newVal) * parseFloat(amount);
            $("#product_amount"+key).text(total);
      			$("#amount"+key).val(total);
            var total_=parseFloat(0);
            $(".totalamount").each(function () {
            total_ += parseFloat($(this).val());
            });

      			$("#total_amount").val(parseFloat(discount)+parseFloat(delivery_charge)+parseFloat(total_));

            } else {
                newVal = 1;
                $(this).attr("data-action").addClass('inactive');
            }
        }
        $('#tbqty'+key).val(newVal);
});


$("#saleslist").on('click', '.btnDelete', function () {

    var key = $(this).attr("data-key-delete");
    $.ajax({

        type: 'get',
        data: {'productid':key},
        url : 'delete-product-temp-cart',
        dataType: 'json',
        contentType: 'application/json',
        success: function (response) {
           
            if (response.success) {

            }else {
                 alert("Unable to perform action");
            }
        }
    });


    
    $(this).closest('tr').remove();
    var rowCount = $('#saleslist >tbody >tr').length;

    if (rowCount>0) {
      var discount = parseFloat($("#discount").val());
      var delivery_charge = parseFloat($("#delivery_charge").val());
      var total_=parseFloat(0);

      $(".totalamount").each(function () {
              total_ += parseFloat($(this).val());
       });

      $("#total_amount").val(parseFloat(discount)+parseFloat(delivery_charge)+parseFloat(total_));

    }else{  
        
        $('.tb2 input[type="text"]').val("0");
        $('.tb2').hide();
        $('.tb1').hide();
        $("#prod_search").val("");
        $("#prodcode").val("");
    }
});
 

function showReceipt() {
  window.frames["printf"].print();
}
 
$(function() {
    $('.calendar').pignoseCalendar( 
    {
      format: 'DD-MM-YYYY' 
    });
});
 

</script>


@endsection 