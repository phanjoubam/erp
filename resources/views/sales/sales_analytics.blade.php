@extends('layouts.erp_theme_03')
@section('content')


<div class="row ">
<div class="col-md-4"> 
    <div class="card mt-3">
        <div class="card-header text-center">
        <h3 class="card-title">Total Sales Volume</h3>
    </div>
<canvas id="doughnut_chart"></canvas> 
    
</div> 

</div>


<div class="col-md-4"> 
<div class="card mt-3">
<div class="card-header text-center">
    <h3 class="card-title">Yearly Revenue Report</h3></div> 
<!-- <canvas id="revenue_chart"></canvas> -->
<canvas id="graph_chart"></canvas>
</div>
</div>


<div class="col-md-4"> 
<div class="card mt-3">
<div class="card-header text-center">
    <h3 class="card-title">Most Sales Product</h3></div> 
 
<table class="table-responsive table">
            
            <thead>
            <tr>
                <th>Product Name</th>
                <th>Quantity</th>
            </tr>
            </thead>

            <tbody>
                @foreach($most_sales_product as $top_product)
                <tr>
                    <td>{{$top_product->pr_name}}</td>
                    <td>{{$top_product->quantity}}</td>
                </tr>
                @endforeach
            </tbody>
</table>
</div>
</div>

<div class="col-md-12">
<div class="card mt-3">
    <div class="card-body">
        <div class="card-header text-center">
<h3>
    <span class="">Product List</span></h3>
</div>  
<table class="table table-striped table-hover">
    <thead>
        
        <th>Product Name</th>
        <th>Qnty.</th>
        <th>CGST</th>
        <th>SGST</th>
        <th>Amount</th>

    </thead>
 
@php 
$total = 0;
$unit=0;
$cgst=0;
$sgst=0;
@endphp
@foreach ($serv_list as $result)
 
 @php
 $totalUnitPrice = sprintf("%.2f", $result->actualPrice);
 @endphp

<tr>

<td>{{    $result->pr_name  }}</td>
<td>{{$result->quantity}}</td>
<td>{{$result->cGST}}</td> 
<td>{{$result->sGST}}</td> 
<td>{{$totalUnitPrice}}</td>

</tr>
@php 

$cgst += $result->cGST;
$sgst += $result->sGST;
$total += $result->actualPrice;
@endphp
@endforeach

<tr>
<td></td>
<td></td>
<td></td>
<td><span class="badge badge-primary">Total amount: </span></td>
<td><span class="badge badge-secondary">Rs. {{$total + $cgst + $sgst}}</span></td> 
</tr>

</table>


</div>
</div>

</div>


<div class="col-md-12">

 <div class="card mt-3">
    <div class="card-body">
        <div class="card-header text-center">
<h3>
    <span class="">Pick & Drop Sales List</span></h3>
</div>  
<table class="table table-striped table-hover scrollable">
    <thead>
        <th>Customer Remarks</th>
        <th>Source</th>
        <th>Commission</th>
        <th>Amount</th>
    </thead>
 
@php 
$total_pndamount=0;
$amount=0;
$totalafterminus=0;
@endphp
@foreach ($pnd_list as $pnd)
 
 @php
    
 @endphp

<tr>
<td>{{$pnd->cust_remarks}}</td>
<td>{{$pnd->source}}</td>

<!-- commission check section -->

@if($pnd->source=='booktou' or $pnd->source=='customer')
@php
$actual_amount = $pnd->total_amount;
$percentage = $pnd->commission;

$amount=$actual_amount * $percentage/100;
$totalafterminus += $amount;
@endphp
<td>{{$amount}}</td>
<td>{{$pnd->total_amount-$amount}}</td>
@else
<td>{{0.00}}</td>
<td>{{$pnd->total_amount}}</td>
@endif

<!-- commission check section ends here --> 
</tr>
@php 
$total_pndamount+= $pnd->total_amount;
@endphp
@endforeach

<tr>
 <td></td>
<td></td>
<td><span class="badge badge-primary">Total amount: </span></td>
<td><span class="badge badge-secondary">Rs. {{$total_pndamount-$totalafterminus}}</span></td> 
</tr>

</table>


</div>
</div>
</div>




</div>

@endsection
@section("script")
<script>
var ctx2 = document.getElementById('graph_chart').getContext('2d');
    
    var chart = new Chart(ctx2, {
    // The type of chart we want to create
    type: 'bar',

    // The data for our dataset
    data: {
        labels: ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'],
        datasets: [{
            label: 'Normal Sales',
            backgroundColor: 'rgb(255, 205, 86)',
            borderColor: 'rgb(129, 181, 230)',
            data: <?php echo json_encode($revdata); ?>
        },
            {
            label: 'PND Sales',
            backgroundColor: 'rgb(54, 162, 235)',
            borderColor: 'rgb(129, 181, 230)',
            data: <?php echo json_encode($pnddata); ?>
            }
        ]
    },

    // Configuration options go here
    options: {}
});

    


    var ctx4 = document.getElementById('doughnut_chart').getContext('2d');
    var chart = new Chart(ctx4, {
    // The type of chart we want to create
    type: 'doughnut',

    // The data for our dataset
    data: {
        labels: ['Normal sales','PND Sales'],
        datasets: [{
            label: 'Total Sales Volume',
            backgroundColor:['rgb(255, 205, 86)'
            ,'rgb(54, 162, 235)'
            ],
            data: [<?php echo json_encode($total);?>,<?php echo json_encode($pndsales);?>]
        }],
    },

    // Configuration options go here
    options: {}
});

</script>
@endsection 