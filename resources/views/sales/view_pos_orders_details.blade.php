@extends('layouts.erp_theme_03')
@section('content')

 
  <div class="row">

  @if (session('err_msg'))
  <div class="col-md-12">
    <div class="alert alert-info">
  {{ session('err_msg') }}
  </div>
  </div>
@endif



   <div class="col-md-8 mt-2"> 
 
     <div class="card panel-default">
           <div class="card-header">
              <div class="card-title"> 
                 <h5>Order # {{ $order->id  }}  
                  
                      <span> Order Date: {{ date('d-m-Y H:i:s', strtotime( $order->book_date)) }}</span> 
                      <span class="badge badge-success">COMPLETED</span>
                      <span>Payment Mode:</span>
                     <span class="badge badge-success">{{$order->payment_type}}</span> 


                    </h5> 
                </div>
            </div>
       <div class="card-body">   
           <div class="table-responsive"> 
                  <table class="table">
                    <thead class=" text-primary">
                      <th>Sl.No</th>
                        <th style='width: 210px'>Item</th>
                        <th>Qty</th>
                        <th>Unit Price</th> 
                         <th>Packing</th> 
                        <th class='text-right'>Item + Packing = Sub-Total</th> 
                    </thead>
                    <tbody>
                      <?php $i = 0 ;
                      $item_total  =0;
                      $pack_total = 0;
                      $sub_total = 0;
                      ?>
               
               @foreach ( $all_orders   as $item) 
               <?php $i++;

               $item_total += ( $item->unit_price * $item->qty ) ;
               $sub_total += ( $item->unit_price * $item->qty )  +  ( $item->package_charge * $item->qty ); 
               $pack_total += ( $item->package_charge * $item->qty );
                ?>

                    <tr >
                      <td>{{$i}}</td>
                      <td style='width: 210px'>
                      <span> {{$item->pr_name}}</span><br/>
                      <small>{{ implode(' ', array_slice(str_word_count( $item->description ,1), 0,10)) }}</small>
                      </span>
                    </td>
                      <td>
                      {{$item->qty}} {{$item->unit}}  
                    </td>   

                    <td>
                       {{ $item->unit_price }}  
                    </td>  
                     <td>
                       {{ $item->package_charge * $item->qty  }}  
                    </td>  

                     <td class='text-right'>
                         ( {{ $item->unit_price }} X {{ number_format( $item->qty   , 0 , ".", "" )   }} ) +  ( {{ $item->package_charge }} X {{ number_format( $item->qty   , 0 , ".", "" )  }} )  =     {{  number_format(  ( $item->unit_price * $item->qty )  +  ( $item->package_charge * $item->qty )  , 2, ".", "" ) }}  
                    </td> 


                    </tr>
                   @endforeach

                   <tr> 
                    <td colspan="5">
                      <strong>Packaging Cost:</strong>
                    </td>
                    <td class='text-right'>
                      {{ number_format( $pack_total   , 2, ".", "" )   }}
                    </td>

                   </tr>

                    <tr>

                    <td colspan="5">
                      <strong>Total Item Cost:</strong>
                    </td>
                    <td class='text-right'>
                     {{ number_format(  $item_total , 2, ".", "" )   }}
                    </td>

                   </tr>



                   <tr> 
                    <td colspan="5">
                      <strong>Total Cost: (Item Cost + Packaging Cost)</strong>
                    </td>
                   
                    <td class='text-right'>
                     <?php 
                     $total_ = $item_total + $pack_total;  ?>

                     {{ number_format(  ( $total_)   , 2, ".", "" )   }}
                    </td>

                   </tr> 

                   <tr>

                    <td colspan="5">
                      <strong>Total Cost:</strong>
                    </td>
                    <td class='text-right'>
                      {{ number_format(  ( $total_)   , 2, ".", "" )   }}
                    </td>

                   </tr>
 
                   <tr>

                    <td colspan="5">
                      <strong>Extra Charge:</strong>
                    </td>
                    <td class='text-right'>
                      {{ number_format( $order->extra_cost , 2, ".", "" )   }} 
                    </td> 
                   </tr> 

                   <tr>

                    <td colspan="5">
                      <strong>CGST:</strong>
                    </td>
                    <td class='text-right'>
                      {{ number_format( $order->cgst , 2, ".", "" )   }} 
                    </td> 
                   </tr> 

                   <tr>

                    <td colspan="5">
                      <strong>SGST:</strong>
                    </td>
                    <td class='text-right'>
                      {{ number_format( $order->sgst , 2, ".", "" )   }} 
                    </td> 
                   </tr> 
                    <tr>

                    <td colspan="5">
                      <strong>Discount Applied:</strong>
                    </td>
                    <td class='text-right'>
                      {{ number_format( $order->discount , 2, ".", "" )   }} 
                    </td> 
                   </tr> 

                   <tr>

                    <td colspan="5">
                      <strong>Total Payable After Discount:</strong>
                    </td>
                    <td class='text-right'>
                      {{ number_format(  ( $order->total_cost)   , 2, ".", "" )   }}
                    </td>

                   </tr>

                     
                   <tr>
                   	<td colspan="5">
                   		<strong>Download Invoice:</strong>
                   	</td>
                   	<td class="text-right">
                   		<a class="btn-print" data-pdfurl="{{URL::to('pos/orders/view-bill')}}?o={{$order->id}}" ><i class="fa fa-download"></i></a>
                       

                   	</td>
                   </tr>
				
				  </tbody>
                  </table>
                </div>
              </div>
            </div>
 


 </div>


 <div class="col-md-4">   

  <div class="card panel-default mt-2">
      <div class="card-header">
        <h5  >Order Remarks</h5>
      </div> 
<div class="card-body"> 
                <p>{{ ( $order->cust_remarks == "") ? "No remarks added!" : $order->cust_remarks }}</p> 
               </div>  
            </div>



    <div class="card panel-default mt-2">
      <div class="card-header">
        <h5  >Customer</h5>
      </div> 
<div class="card-body">
     <h4 class="card-title">{{$order->customer_name}}</h4>
                <p>{{$order->address}}<br/> 
                  <i class='fa fa-phone'></i> {{$order->customer_phone}}</p> 
               </div>  
            </div>
  
     </div>   
       </div>  
  

@endsection
@section("script")
<script type="text/javascript">
	
 
$(document).on("click", ".btn-print", function()
{
    window.open($(this).attr("data-pdfurl"), "popupWindow", "width=600,height=600,scrollbars=yes"); 
});
 
</script>
@endsection 