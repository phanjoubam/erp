@extends('layouts.erp_theme_03')
@section('content')

<div class='outer-top-tm'> 
<div class="container">
 
  <div class="row">
     <div class="col-md-12">

    <div class='card'> 
      <div class='card-body'>
        <div class="row">
                <div class="col-md-10">
                  All POS Orders
                </div>
                <div class="col-md-4">
           
                </div> 
 		</div> 
      </div> 
    </div>
     </div>
 </div>
 <div class="row">
 
               
              <div class="col-md-12"> 
                @foreach($all_orders as $item)
                <div class="card mt-1">
                  <div class="card-body ">

                  <div class="dropdown pull-right"> 
                      </div>
                 <div class="row"> 
                   
                 
                 <div class='col-md-2' id="tr" > 
                  <div class="pa-2 " >      
                    <strong>Order #</strong><br/>
                  
                    <a class='h1' href="{{URL::to('pos/view-order-details')}}?key={{$item->id}}"  >
                     {{$item->biz_order_no}}
                     </a> 
              </div> 
            </div>
               
                <div class='col-md-3'  > 
                 <div class="pa-2">  
                  <strong>Order Summary</strong><br/>
                
                <strong>Order Date:</strong> {{ date('d-m-Y', strtotime( $item->book_date )) }} <br/>   
                    <strong>Order Status:</strong> 
                      <span class="badge badge-success">
                      	COMPLETED
					            </span> 
                      <br/>
                      <strong>Payment Type:</strong> 
                      <span class="badge badge-info">
                      {{$item->payment_type}}
                      </span> 
                      <br/>
 
                  </div>   
                </div>

                 <div class='col-md-3'  > 
                  <div class="pa-2">  
                  <strong>Total Amount</strong><br/>
                  <span class='h1' style='color: #669900'>{{ number_format(  ( $item->total_cost)   , 2, ".", "" )   }} ₹</span>
                  
				  </div> 
                </div>

                <div class='col-md-3 text-right' style='color: #c4c4c4;' > 
                  <div class="pa-2">  
                  
				          </div> 
                </div>


</div>
</div>
                </div>
                @endforeach


                  
  
              	
           		
 </div> 
                 
          
              
             </div>
        </div>
    </div>  
   
<div class="mt-3"></div>







@endsection
@section("script")
<script type="text/javascript">
	


</script>
@endsection 