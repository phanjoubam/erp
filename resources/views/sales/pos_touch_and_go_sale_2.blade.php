@extends('layouts.erp_theme_03_hollow')


@section('style')

<style>

.flex-container {
  display: flex;
  flex-wrap: nowrap; 
}

.flex-container > div {  
  margin: 10px; 
}

</style>


@endsection

@section('content')
<?php 
   $cdn_url =    config('app.app_cdn') ;  
    $cdn_path =     config('app.app_cdn_path') ; // cnd - /var/www/html/api/public 
    $total_cost = $delivery_cost = 0.0; 
    $cartno = $data['cartno'];
    $new_cartno = $data['new_cartno'];
    $bin = $data['bin'];

    if( $new_cartno > $cartno)
    {
       $carttoupdate= $new_cartno;
    }
    else 
    {
       $carttoupdate =  $cartno;
    }
    
    $allcarts = $data['total_temp_cart']; 
?>
  @php 
        $colcount=1;  
        $pagesize= $data['pagesize']; 
        $cctr = 1; 
        $totalitems = count($data['all_products']);
       @endphp 


  <div class='panelleft'>

    @if($totalitems < 8)

    <div class="flex-container"  id='dynload'  >  

      @foreach($data['all_products'] as $product)   
              @php 
                $img_url = $cdn_url .  "/assets/image/no-image.jpg";   
              @endphp  
        
              @foreach($data['all_photos'] as $photo)
                @if($product->prsubid == $photo->prsubid)
                @php 
                  $img_url = $photo->image_url;
                  @endphp
                  @break
                @endif
              @endforeach
  

            @if( $cctr == 1 )
              <div class="item" style="width:180px"> 
            @endif
            <div class="image-container" style="background-image: url('{{ $img_url }}');background-size:cover"> 
                <div class="after">{{$product->pr_name}}
                  <div class="info"> 
                        @if($product->unit_price == $product->actual_price)
                          <p class="price-text-color">₹ {{$product->actual_price }}</p>
                        @else
                          <p class="price-text-color">₹ {{$product->actual_price }} <br/><span class="old-price" style='text-decoration:line-through; color: #f00'>₹ {{$product->unit_price}}</span></p>
                        @endif 
                        <div class="separator clear-leftd">
                          @if(  $product->stock_inhand == 0  )
                            <button type="button"   class="btn btn-outline-danger btn-sm btn-block" disabled>Out of stock</button> 
                          @else
                            <button type="button" data-action="add" data-qty="1" data-bin="{{   $bin }}" 
                                        data-key="{{  $product->prsubid }}"  class="btn btn-success btn-sm btnadditem btn-block">Add to Cart</button> 
                          @endif 
                        </div>
                      <div class="clearfix"></div>
                    </div> 
              </div>
            </div>

            @if( $cctr > 1 && $cctr % 2 == 0 )
              </div> 
              @if( $cctr < $pagesize )
                <div class="item" style="width:180px"> 
              @endif
            @else
              @if( $cctr == $totalitems )
                </div> 
              @endif 
            @endif

          @php
            $colcount++; 
            $cctr++;
          @endphp  
      @endforeach 

    

    @else 
    
    <div class="owl-carousel"  id='dynload'  >  

      @foreach($data['all_products'] as $product)   
              @php 
                $img_url = $cdn_url .  "/assets/image/no-image.jpg";   
              @endphp  
        
              @foreach($data['all_photos'] as $photo)
                @if($product->prsubid == $photo->prsubid)
                @php 
                  $img_url = $photo->image_url;
                  @endphp
                  @break
                @endif
              @endforeach
  

            @if( $cctr == 1 )
              <div class="item" style="width:180px"> 
            @endif
            <div class="image-container" style="background-image: url('{{ $img_url }}');background-size:cover"> 
                <div class="after">{{$product->pr_name}}
                  <div class="info"> 
                        @if($product->unit_price == $product->actual_price)
                          <p class="price-text-color">₹ {{$product->actual_price }}</p>
                        @else
                          <p class="price-text-color">₹ {{$product->actual_price }} <br/><span class="old-price" style='text-decoration:line-through; color: #f00'>₹ {{$product->unit_price}}</span></p>
                        @endif 
                        <div class="separator clear-leftd">
                          @if(  $product->stock_inhand == 0  )
                            <button type="button"   class="btn btn-outline-danger btn-sm btn-block" disabled>Out of stock</button> 
                          @else
                            <button type="button" data-action="add" data-qty="1" data-bin="{{   $bin }}" 
                                        data-key="{{  $product->prsubid }}"  class="btn btn-success btn-sm btnadditem btn-block">Add to Cart</button> 
                          @endif 
                        </div>
                      <div class="clearfix"></div>
                    </div> 
              </div>
            </div>

            @if( $cctr > 1 && $cctr % 2 == 0 )
              </div> 
              @if( $cctr < $pagesize )
                <div class="item" style="width:180px"> 
              @endif
            @else
              @if( $cctr == $totalitems )
                </div> 
              @endif 
            @endif

          @php
            $colcount++; 
            $cctr++;
          @endphp  
      @endforeach 
    @endif


 
     <div id='dynpager'  >  
  </div>  
</div> 
       



 
 </div> 
  <div class='panelright'  >

    <div id="cartsummary" class=''> 
      <div id='cartsummaryheader'>
        <div class='row'>
            <div class='col-md-8'>
              <h5>Current Cart # <span class='badge badge-success  '>{{  $carttoupdate  }}</span></h5>
            </div>
            <div class='col-md-4 text-right'>
               <a type="button"  data-widget='w1' data-param="{{  $data['cartno']  }}"  class="btn btn-danger  btn-sm btnremove">
                  <i class='fa fa-trash'></i>
               </a>  
   
               <a type="button" href="{{ URL::to('/pos/pos-sales-window-touch-and-go')}}?enccno={{ Crypt::encrypt( $data['cartno'] ) }}" class="btn btn-success btn-sm btnrefresh"><i class='fa fa-sync'></i></a>
            </div>
          </div>
        </div>

 
          @if( isset( $data['cart_items']  ) ) 

      <form method="post" action="{{ action('Erp\PosController@placePosOrder') }}"> 
      {{ csrf_field() }}
      <div class='cartcontent scrollY' id='cartsummarybody' >  

        <div class="order-details-confirmation"  >     
        @php 
          $total_cost =0;
          $memid = 0;
          $outofstockfound= false;
          $item_total =0;
          $package_total  =0;
          $cgst_pc =0;
          $sgst_pc =0;


         @endphp 
        <table class="table table-colored"> 
         <tr>
                <th width='50px'></th>
                <th style='width:350px; text-align:left' >Item</th>  
                <th class='text-right' style='width: 180px'>Cost</th>
            </tr>  
          @php 
            $img_url = $cdn_url .  "/assets/image/no-image.jpg";  
          @endphp 
        @foreach( $data['cart_items']  as $cartitem) 
        <tr>
          <td width='50px'>

            @foreach($data['photos'] as $photo)
              @if($cartitem->subid == $photo->prsubid)
               @php 
                $img_url = $photo->image_url;
                @endphp
                @break
              @endif
            @endforeach

            <img class='rounded ' width='40px' src="{{  $img_url  }}" alt='{{ $cartitem->subid }}'/>


          </td>
          <td style='width:250px; text-align:left' >
            @php 
              $product_name = "N/A";
            @endphp
             @foreach($data['products'] as $product)
              @if($cartitem->subid == $product->prsubid)
                @php 
                  $product_name = $product->pr_name;
                @endphp
                @break
              @endif
            @endforeach 
            <strong>{{ $product_name }}</strong>
            <br/>
              @if(  $cartitem->stock_inhand == 0  )
              <button type="button"   class="btn btn-outline-primary btn-sm  " disabled>Out of stock</button>
              &nbsp;
              <button type="button"   class='btn btn-danger btn-sm btndel' data-key="{{ $cartitem->key }}"><i class='fa fa-trash'></i></button>

            </td>
            <td class='text-right' style='width: 180px'>0.00</td>

                @php  
              $outofstockfound= true;
             @endphp 

           @else  
                <div class="input-group input-group-sm ">
                    <div class="input-group-prepend">
                            <button type="button" class="input-group-text btnupdatecart" 
                            data-prid="{{  $cartitem->subid }}" 
                            data-subid="{{  $cartitem->subid }}"  
                            data-bin="{{  $data['bin'] }}"   
                            data-step='rem' 
                            data-key="{{ $cartitem->key }}">-</button> 
                          </div>
                          <input type="number" class="form-control qty" aria-label="Quantity" step="1" min="1" max="" name="quantity" id='tbqty{{ $cartitem->key }}' value="{{ $cartitem->qty }}" title="Qty" >
                          <div class="input-group-append">
                           <button type="button" class="input-group-text btnupdatecart" 
                            data-prid="{{  $cartitem->subid }}" 
                            data-subid="{{  $cartitem->subid }}"  
                            data-bin="{{  $data['bin'] }}"  
                            data-step='add' data-key="{{ $cartitem->key }}">+</button> 
                          </div>
                    </div> 
              </td>  
              <td class='text-right' style='width: 180px'>{{ ( $cartitem->actual_price *  $cartitem->qty ) + 
                    ($cartitem->packaging * $cartitem->qty)  }}</td>
            

             @php 
                
                $item_total  += ( $cartitem->actual_price *  $cartitem->qty ) + ($cartitem->packaging * $cartitem->qty) ; 
                $package_total  +=  ($cartitem->packaging * $cartitem->qty) ;  
                $cgst_pc += ( $cartitem->actual_price *  $cartitem->qty ) + ($cartitem->packaging * $cartitem->qty) ;  
                $sgst_pc += ( $cartitem->actual_price *  $cartitem->qty ) + ($cartitem->packaging * $cartitem->qty) ; 
             @endphp   

        @endif  
        </tr>                  
        @endforeach    

 </table>
  <input type="hidden" name="enccno" value="{{ Crypt::encrypt( $data['cartno']) }}" />
        <input type='hidden' name='bin' value='{{ $data["bin"] }}' />
        <input type='hidden' id="newcartno" name='newcartno' value='{{ $carttoupdate }}' /> 
     </div> 
 </div>  
</form> 
<div id='cartsummaryfooter' class=' text-center' >

        @if( $outofstockfound  )
            <button type='button'  class="btn btn-danger" disabled>Checkout Block</button> 
        @else  
            <a type='submit' value='placeorder' name='btnorder' href="{{  URL::to('/pos/checkout-order')}}?enccno={{ Crypt::encrypt( $data['cartno']) }}"  class="btn btn-success">Checkout Order</a> 
        @endif
</div>
@endif 
 </div> 
</div> 
 

<div class='clearer'></div>

 <form method="post" action="{{ action('Erp\PosController@removeItemFromSalesWindow') }}"> 
    {{ csrf_field() }}
<div class="modal wgconfirm" tabindex="-1">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Confirm Item Removal</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Selected item will be removed from cart. Are you sure?</p>
      </div>
      <div class="modal-footer">
         <input type="hidden" name="key" id="delkey" />
         <input type="hidden" name="enccno" value="{{ Crypt::encrypt($data['cartno']) }}" />
        <button type="submit" value='delete' name='btndelete' class="btn btn-danger">Delete</button>
        <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
      </div>
    </div>
  </div>
</div>
</form>

 <form method="post" action="{{ action('Erp\PosController@removeTemporaryCart') }}"> 
    {{ csrf_field() }}
<div class="modal wgcartrem" tabindex="-1">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Confirm Cart Removal</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Selected cart will be removed. Are you sure?</p>
      </div>
      <div class="modal-footer"> 
         <input type="hidden" name="uitype" value="modern"/>
         <input type="hidden" name="enccno" value="{{ Crypt::encrypt($data['cartno']) }}" /> 
        <button type="submit" value='delete' name='btndelete' class="btn btn-danger">Delete</button>
        <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
      </div>
    </div>
  </div>
</div>
</form>

<div class="modal wgsystem" tabindex="-1">
  <div class="modal-dialog modal-dialog-centered modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">System Notification</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <span id='msgarea'></span>
      </div>
      <div class="modal-footer"> 
         <input type="hidden" name="uitype" value="modern"/>
         <input type="hidden" name="enccno" value="{{ Crypt::encrypt($data['cartno']) }}" />  
        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>


@endsection
@section("script")

<script> 
  
 

$('#key').on('change',function(e){
  addItemToCart();
});
 
 
$('#tbdiscount').on('input',function(e){
  recalcTotal();
});


 
$('#extracost').on('input',function(e){
   
   recalcTotal();
   
});
 

$('#tbcgst').on('input',function(e){
 recalcTotal();
});

$('#tbsgst').on('input',function(e){
   
   recalcTotal();

});

 

 function recalcTotal()
{
   $total = $('#tbitemcost').val();
   $packaging = $('#tbpackaging').val();
   $discount  = $('#tbdiscount').val();
   $extra  = eval($('#extracost').val()) ;
   $totalamount = eval( $total )  -  eval($discount)   +  $extra; 
   $cgst = eval($('#tbcgst').val());
   $sgst = eval($('#tbsgst').val());  
   $totaltaxed = $totalamount + eval( $packaging )  + (( $cgst + $sgst )*$totalamount) /  100;  
   $('#tbtotalcost').val(  $totaltaxed);  
}





$(document).on("click", ".btndel", function() {

  $("#delkey").val($(this).attr("data-key"));
  $(".wgconfirm").modal('show');
});

$(document).on("click", ".btnremove", function() {
 
  $(".wgcartrem").modal('show');
}); 
 

//adding item to temp 
$(document).on("click", ".btnaddproduct", function()
{
  addItemToCart();
})

//adding item to temp 
$(document).on("click", ".btnupdatecart", function()
{ 

  var subid =  $(this).attr("data-subid") ; 
  var qty =  1;
  var action = $(this).attr("data-step") ; 
  var cartno = $("#newcartno").val(); 
  var gbin = $("#gbin").html();
  var guid = $("#guid").html(); 

  var json = {};
  json['guid'] =  guid  ;
  json['gbin'] =  gbin ; 
  json['subid'] = subid; 
  json['qty'] =  qty ;
  json['action'] =  action ;
  json['cartno'] =  cartno ;
  json['uitype'] = "modern" ;

  console.log(json);

  $.ajax({

    type: 'post',
    url: erp  + "api/v3/pos/add-item-to-basket" ,
    data: json,
    success: function(data)
    {
      data = $.parseJSON(data);  
      if(data.status_code == 6001){

       window.location.href =  "<?php echo URL::to('/pos/pos-sales-window-touch-and-go')  ?>?enccno=" + data.enccno;
      }
      else{
         
        $("#msgarea").html( data.detailed_msg);
        $(".wgsystem").modal('show'); 
      }
      },
      error: function( ) 
      {
         $("#msgarea").html(  'An error while connecting the server, please try again after checking Internet connection!' ); 
      }
    });

}) 



$(document).on("click", ".btnadditem", function()
{
  var subid =  $(this).attr("data-key");
  addItemToCart(subid);
})


function addItemToCart(subid)
{
  if(subid == "") return;
  var qty =  1;
  var action = "add"; 
  var cartno = $("#newcartno").val(); 
  var gbin = $("#gbin").html();
  var guid = $("#guid").html();  
  var json = {};
  json['guid'] =  guid  ;
  json['gbin'] =  gbin ; 
  json['subid'] = subid; 
  json['qty'] =  qty ;
  json['action'] =  action ;
  json['cartno'] =  cartno ;
  json['uitype'] = "modern" ; 

  $.ajax({

    type: 'post',
    url: erp  + "api/v3/pos/add-item-to-basket" ,
    data: json,
    success: function(data)
    {
      data = $.parseJSON(data);  
      if(data.status_code == 6001){

       window.location.href =  "<?php echo URL::to('/pos/pos-sales-window-touch-and-go')  ?>?enccno=" + data.enccno;
      }
     else{
         
        $("#msgarea").html( data.detailed_msg);
        $(".wgsystem").modal('show'); 
      }
      },
      error: function( ) 
      {
         $("#msgarea").html(  'An error while connecting the server, please try again after checking Internet connection!' ); 
      }
    });
}

 


$("#saleslist").on('click', '.btnDelete', function () {

    var key = $(this).attr("data-key-delete");
    $.ajax({

        type: 'get',
        data: {'productid':key},
        url : 'delete-product-temp-cart',
        dataType: 'json',
        contentType: 'application/json',
        success: function (response) {
           
            if (response.success) {

            }else {
                 alert("Unable to perform action");
            }
        }
    });


    
    $(this).closest('tr').remove();
    var rowCount = $('#saleslist >tbody >tr').length;

    if (rowCount>0) {
      var discount = parseFloat($("#discount").val());
      var delivery_charge = parseFloat($("#delivery_charge").val());
      var total_=parseFloat(0);

      $(".totalamount").each(function () {
              total_ += parseFloat($(this).val());
       });

      $("#total_amount").val(parseFloat(discount)+parseFloat(delivery_charge)+parseFloat(total_));

    }else{  
        
        $('.tb2 input[type="text"]').val("0");
        $('.tb2').hide();
        $('.tb1').hide();
        $("#prod_search").val("");
        $("#prodcode").val("");
    }
});
 

function showReceipt() {
  window.frames["printf"].print();
}
 
$(function() {
    $('.calendar').pignoseCalendar( 
    {
      format: 'DD-MM-YYYY' 
    });
});
 
  
 $(document).ready(function(){ 

  $(".owl-carousel").owlCarousel({
      margin:10,
      loop:true,
      autoWidth:true,
      items:4,
      nav: true,
      navText: ["<i class='fa fa-chevron-left owl-navigator'></i>","<i class='fa fa-chevron-right owl-navigator'></i>"]
  });

/*
  $(".autocomplete").selectize({
     create: true,
     sortField: "text",
     onChange: function(value) {
      autoLoadProducts(value);
    }
  });
  */

  $("#keyword").keyup(function(){
    
    var keyword = $(this).val();
    if(keyword.length < 3) return;  
    var gbin = $("#gbin").html();
    var guid = $("#guid").html();  
    var json = {};
    json['guid'] =  guid  ;
    json['gbin'] =  gbin ; 
    json['keyword'] = keyword;  


    $.ajax({
 
      type: 'post',
      url: erp  + "api/v3/pos/load-all-products" ,
      data: json, 
      success: function(data)
      { 
         
        $("#productList").empty();
        data = $.parseJSON(data);  

        $("#productList").append("<li value='"+keyword+"'>"+keyword+"</li>");
        $.each(data.results, function(index, item)
        { 
          $("#productList").append("<li class='media'>" +
            "<img class='rounded ' width='30px' src='" + item.photo + "'  /> " + 
            "<div class='media-body'>" +
            "<p class='mt-0 mb-1'>" +  item.name +"</p>" +  
            "</div></li>");
        });

        // binding click event to li
        $("#productList li").bind("click",function(){
          refreshProductsGrid(this);
        });
      }
    });
  }); 
});

 
 


function refreshProductsGrid(element){

  var keyword = $(element).text();  
  if(keyword.length < 3) return;  

  $("#keyword").val(keyword);
  $("#productList").empty();

  var gbin = $("#gbin").html();
  var guid = $("#guid").html(); 

  var json = {};
  json['guid'] =  guid  ;
  json['gbin'] =  gbin ; 
  json['keyword'] = keyword;    

  $.ajax({

    type: 'post',
    url: erp  + "api/v3/pos/search-item-by-keyword" ,
    data: json,
    success: function(data)
    {
      data = $.parseJSON(data);  
       
      var dynamic_content = '<div   class="card-group mt-1">'; //fix
        
      if(data.status_code == 6007){
        var colcount=1;
        $.each(data.results, function(index, item)
        {
          dynamic_content += "<div class='image-container' style='background-image: url(\"" + item.photo  +"\"); background-size:cover'>" + 
              "<div class='after'>" + item.name +  
                "<div class='info'>" ;
          if( item.unit_price == item.price)
          {
              dynamic_content += "<p class='price-text-color'>₹ " + item.price + "</p>";
          }
          else
          { 
            dynamic_content += "<p class='price-text-color'>₹ " + item.price + 
            " <br/><span class='old-price' style='text-decoration:line-through; color: #f00'>₹ " + item.unit_price + "</span></p>";
          }
          dynamic_content += '<div class="separator clear-left">';

          if(   item.stock == 0  )
          {
            dynamic_content += '<button type="button" class="btn btn-outline-danger btn-sm btn-block" disabled>Out of stock</button>'; 
          }
          else
          {
            dynamic_content +='<button type="button" data-action="add" data-qty="1" data-bin="' + item.bin  +'"' + 
              'data-key="' + item.prsubid  + '"  class="btn btn-success btn-sm btnadditem btn-block">Add to Cart</button>'; 
          } 

          dynamic_content += '<div class="clearfix"></div></div></div></div></div>';

          colcount++;

 
        }); 

        if(dynamic_content!="")
        {
          document.getElementById('dynload').innerHTML = '';   
        }
        
        $("#dynload").html(dynamic_content); 
      } 
      },
      error: function( ) 
      {
         $("#msgarea").html(  'An error while connecting the server, please try again after checking Internet connection!' ); 
      }
    });
}



$(document).on('change', '#dbcategory', function()
{
  var category = $("select#dbcategory").find('option:selected').val();  
  //loadProductUnderCategory(category);
});
 


function loadProductUnderCategory(category){

  var gbin = $("#gbin").html();
  var guid = $("#guid").html();
  var json = {};
  json['guid'] =  guid  ;
  json['gbin'] =  gbin ;  
  json['category'] = category;   

  $.ajax({

    type: 'post',
    url: erp  + "api/v3/pos/load-products-under-keyword" ,
    data: json,
    success: function(data)
    {
      data = $.parseJSON(data); 
      if(data.status_code == 6009){
 
         var   selectized = $('.autocomplete').selectize();
         var control = selectized[0].selectize;
         control.clear();

 
          $.each(data.results, function(index, item)
          {
            //$('.autocomplete').append($('<option>', {value: item.name , text:  item.name   }));

            control.addOption({value:  item.name , text: item.name });
            control.addItem( item.name ); 
            control.refreshItems();

          }); 
 

      } 
      } 
    });
 
}
 

</script>
 

@endsection