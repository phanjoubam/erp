 
                        
@extends('layouts.erp_theme_03')
                   
 

@section('content')

@php 
   
  $total_new_normalOrders=0;
  $total_new_pndOrders = 0;

  $totalPndOrdersCompleted =0;
  $totalOrdersCompleted = 0; 
   
  $totalNormalOrders = 0;
  $totalPndOrders =0; 

  $totalassitsOrderSales = 0;
  $totalPndOrderSales=0;
  $totalnormalOrderSales=0;
    
@endphp

<?php

foreach($normal as $item)
  {
     if( $item->book_status == "delivered" || $item->book_status == "completed"   )
     {
      $totalOrdersCompleted++;
      $totalNormalOrders++;
      $totalnormalOrderSales+= $item->total_cost;
     }

     $total_new_normalOrders++;

  }

foreach($pnd as $item)
  {
    if( $item->book_status == "delivered" || $item->book_status == "completed"   )
     {
      $totalPndOrdersCompleted++;
       $totalPndOrders++;
      $totalPndOrderSales+=$item->total_amount;
     }

     $total_new_pndOrders++;
   }
?>



  <!-- Begin Page Content -->
               

                   
                    <!-- Content Row -->
                    <div class="row">
  
<?php
        $dates = array(); 
        $pnddates = $normaldates = array(); 
        $pnd_orders  = array();
        $normal_orders  = array();

       // $pnd_orders[]= $normal_orders[]=0;

        $begin = new Datetime($report_date);
        $end = new Datetime($todayDate);

        for($i = $begin; $i <= $end; $i->modify('+1 day')){
           $dates[] ='"'.date('Y-m-d',strtotime($i->format('Y-m-d'))).'"';
           $normaldates[]='"'.date('Y-m-d',strtotime($i->format('Y-m-d'))).'"'; 
           $pnddates[]='"'.date('Y-m-d',strtotime($i->format('Y-m-d'))).'"';
        }

          
 
            $arrayLength = count($dates);
        
            $i = 0;
            while ($i < $arrayLength)
            {
                
                foreach($normal_sales_chart_data as $item)
                    {  
                       if ('"'.$item->serviceDate.'"'==$dates[$i]) {
                           
                          $normaldates[$i] = $item->normalOrder;
                          
                       }  

                    


                    } 

                 

                foreach($pnd_sales_chart_data as $item)
                    {
                       
                      if ('"'.$item->serviceDate.'"'==$dates[$i]) {
                           
                           $pnddates[$i] = $item->pndOrders;
                          
                       }  
                         
                    }  
              
                   
                   

                $i++;


            }

            $j = 0;
            while ($j < $arrayLength)
            {

                   
                        
                      if ($pnddates[$j]==$dates[$j]) {
                           
                           $pnddates[$j]=0;
                          
                       } 


                       if ($normaldates[$j]==$dates[$j]) {
                           
                           $normaldates[$j]=0;
                          
                       }  
                         
                     
            $j++;
          }
              

             

         //echo json_encode(implode(",",  $dates));

        

?>
                        <!-- Earnings (Monthly) Card Example -->
                        <div class="col-xl-3 col-md-6 mb-4">
                            <div class="card border-left-primary shadow h-100 py-2">
                                <div class="card-body">
                                  <a href="{{URL::to('reports/billing-and-clearance/daily-sales')}}">  <div class="row no-gutters align-items-center">
                                        <div class="col mr-2">
                                            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                                Normal Order Count ({{$totalNormalOrders}})
                                            </div>
                                            <div class="h5 mb-0 font-weight-bold text-gray-800"> 
                                            ₹ {{$totalnormalOrderSales}}</div>
                                        </div>
                                        <div class="col-auto">
                                            <i class="fas fa-calendar fa-2x text-gray-300"></i>
                                        </div>
                                    </div>
                                  </a>
                                </div>
                            </div>
                        </div>

                        <!-- Earnings (Monthly) Card Example -->
                        <div class="col-xl-3 col-md-6 mb-4">
                            <div class="card border-left-success shadow h-100 py-2">
                                <div class="card-body">
                                  <a href="{{URL::to('reports/billing-and-clearance/daily-sales')}}">  <div class="row no-gutters align-items-center">
                                         <div class="col mr-2">
                                            <div class="text-xs font-weight-bold text-success text-uppercase mb-1">
                                                PND Order Count ({{$totalPndOrders}})</div>
                                            <div class="h5 mb-0 font-weight-bold text-gray-800">
                                            ₹ {{$totalPndOrderSales}} </div>
                                        </div>
                                        <div class="col-auto">
                                            <i class="fas fa-calendar fa-2x text-gray-300"></i>
                                        </div>

                                    </div>
                                  </a>
                                </div>
                            </div>
                        </div>

                        <!-- Earnings (Monthly) Card Example -->
                        <div class="col-xl-3 col-md-6 mb-4">
                            <div class="card border-left-info shadow h-100 py-2">
                                <div class="card-body">
                                    <div class="row no-gutters align-items-center">
                                        <div class="col mr-2">
                                            <div class="text-xs font-weight-bold text-info text-uppercase mb-1"> 
                                            </div>
                                            <div class="row no-gutters align-items-center">
                                                <div class="col-auto">
                                                  <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">
                                                   
                                                  </div>
                                                </div>
                                                <div class="col">
                                                     
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-auto">
              
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Pending Requests Card Example -->
                        <div class="col-xl-3 col-md-6 mb-4">
                            <div class="card border-left-warning shadow h-100 py-2">
                                <div class="card-body">
                                    <a href="{{URL::to('export/daily-sales-and-service-export-to-excel')}}?bin={{$bin}}">
                                      <div class="row no-gutters align-items-center">
                                        <div class="col mr-2">
                                            <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">
                                                Total Sales </div>
                                            <div class="h5 mb-0 font-weight-bold text-gray-800">
                                            ₹ {{$totalnormalOrderSales + $totalPndOrderSales}} </div>
                                        </div>
                                        <div class="col-auto">
                                           <i class="fas fa-download fa-2x text-gray-300"></i>
                                        </div>
                                    </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>

                    
                    

           
                <!-- /.container-fluid -->


<!-- Content Row -->

                    <div class="row">

                        <!-- Area Chart -->
                        <div class="col-xl-8 col-lg-7">
                            <div class="card shadow mb-4">
                                <!-- Card Header - Dropdown -->
                                <div
                                    class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                                    <h6 class="m-0 font-weight-bold text-primary">Daily Sales Overview</h6>
                                    <div class="dropdown no-arrow"> 
                                    </div>
                                </div>
                                <!-- Card Body -->
                                <div class="card-body">
                                    <div class="chart-area">
                                        <canvas id="myAreaChart"></canvas>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Pie Chart -->
                        <div class="col-xl-4 col-lg-5">
                            <div class="card shadow mb-4">
                                <!-- Card Header - Dropdown -->
                                <div
                                    class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                                    <h6 class="m-0 font-weight-bold text-primary">Revenue Score</h6>
                                    
                                </div>
                                <!-- Card Body -->
                                <div class="card-body">
                                    <div class="chart-pie pt-4 pb-2">
                                        <canvas id="myPieChart1"></canvas>
                                    </div>
                                    <div class="mt-4 text-center small">
                                        <span class="mr-2">
                                            <i class="fas fa-circle text-primary"></i> Normal
                                        </span>
                                        <span class="mr-2">
                                            <i class="fas fa-circle text-success"></i> Pick and Drop
                                        </span>
                                         
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Content Row -->
 

 
@endsection

@section("script")

<script>
 //Area chart


  <?php
        //$dates = array();  
          //$pnd_orders  = array();
          //$normal_orders  = array();


         
    
          
  ?>  

      var data_1_1 = [ <?php echo implode(',', $pnddates) ; ?> ]; 
      var data_1_2 = [ <?php echo implode(',', $normaldates) ; ?> ];
  // Set new default font family and font color to mimic Bootstrap's default styling
Chart.defaults.global.defaultFontFamily = 'Nunito', '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
Chart.defaults.global.defaultFontColor = '#858796';

function number_format(number, decimals, dec_point, thousands_sep) {
  // *     example: number_format(1234.56, 2, ',', ' ');
  // *     return: '1 234,56'
  number = (number + '').replace(',', '').replace(' ', '');
  var n = !isFinite(+number) ? 0 : +number,
    prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
    sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
    dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
    s = '',
    toFixedFix = function(n, prec) {
      var k = Math.pow(10, prec);
      return '' + Math.round(n * k) / k;
    };
  // Fix for IE parseFloat(0.55).toFixed(0) = 0;
  s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
  if (s[0].length > 3) {
    s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
  }
  if ((s[1] || '').length < prec) {
    s[1] = s[1] || '';
    s[1] += new Array(prec - s[1].length + 1).join('0');
  }
  return s.join(dec);
}

// Area Chart Example
var ctx = document.getElementById("myAreaChart");
var myLineChart = new Chart(ctx, {
  type: 'line',
  data: {
    labels: [<?php echo implode(",",  $dates ); ?>],
    datasets: [{
      label: "Normal Orders",
      lineTension: 0.3,
      backgroundColor: "rgba(78, 115, 223, 0.05)",
      borderColor: "rgba(78, 115, 223, 1)",
      pointRadius: 3,
      pointBackgroundColor: "rgba(78, 115, 223, 1)",
      pointBorderColor: "rgba(78, 115, 223, 1)",
      pointHoverRadius: 3,
      pointHoverBackgroundColor: "rgba(78, 115, 223, 1)",
      pointHoverBorderColor: "rgba(78, 115, 223, 1)",
      pointHitRadius: 10,
      pointBorderWidth: 2,
      data: data_1_2,
    },

    {
      label: "PND Orders",
      lineTension: 0.3,
      backgroundColor: "rgba(128, 115, 223, 0.15)",
      borderColor: "rgb(28, 200, 138)",
      pointRadius: 3,
      pointBackgroundColor: "rgb(28, 200, 138)",
      pointBorderColor: "rgb(28, 200, 138)",
      pointHoverRadius: 3,
      pointHoverBackgroundColor: "rgb(28, 200, 138)",
      pointHoverBorderColor: "rgb(28, 200, 138)",
      pointHitRadius: 10,
      pointBorderWidth: 2,
      data: data_1_1,
    }],
  },
  options: {
    maintainAspectRatio: false,
    layout: {
      padding: {
        left: 10,
        right: 25,
        top: 25,
        bottom: 0
      }
    },
    scales: {
      xAxes: [{
        time: {
          unit: 'date'
        },
        gridLines: {
          display: false,
          drawBorder: false
        },
        ticks: {
          maxTicksLimit: 7
        }
      }],
      yAxes: [{
        ticks: {
          maxTicksLimit: 5,
          padding: 10,
          // Include a dollar sign in the ticks
          callback: function(value, index, values) {
            return  number_format(value);
          }
        },
        gridLines: {
          color: "rgb(234, 236, 244)",
          zeroLineColor: "rgb(234, 236, 244)",
          drawBorder: false,
          borderDash: [2],
          zeroLineBorderDash: [2]
        }
      }],
    },
    legend: {
      display: false
    },
    tooltips: {
      backgroundColor: "rgb(255,255,255)",
      bodyFontColor: "#858796",
      titleMarginBottom: 10,
      titleFontColor: '#6e707e',
      titleFontSize: 14,
      borderColor: '#dddfeb',
      borderWidth: 1,
      xPadding: 15,
      yPadding: 15,
      displayColors: false,
      intersect: false,
      mode: 'index',
      caretPadding: 10,
      callbacks: {
        label: function(tooltipItem, chart) {
          var datasetLabel = chart.datasets[tooltipItem.datasetIndex].label || '';
          return datasetLabel + ':' + number_format(tooltipItem.yLabel);
        }
      }
    }
  }
});

//area chart example


//pie chart example

<?php

if ($pnd_revenue=="") {

  $pnd_revenue=0;
}

if ($sales_revenue=="") {

  $sales_revenue=0;
}

?>
      
// Set new default font family and font color to mimic Bootstrap's default styling
Chart.defaults.global.defaultFontFamily = 'Nunito', '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
Chart.defaults.global.defaultFontColor = '#858796';

// Pie Chart Example
var ctx = document.getElementById("myPieChart1");
var myPieChart = new Chart(ctx, {
  type: 'doughnut',
  data: {
    labels: ["Normal Orders", "Pick and Drop Orders"],
    datasets: [{
      data: [<?php echo $sales_revenue;?>, <?php echo $pnd_revenue;?>],
      backgroundColor: ['#4e73df', '#1cc88a', '#36b9cc'],
      hoverBackgroundColor: ['#2e59d9', '#17a673'],
      hoverBorderColor: "rgba(234, 236, 244, 1)",
    }],
  },
  options: {
    maintainAspectRatio: false,
    tooltips: {
      backgroundColor: "rgb(255,255,255)",
      bodyFontColor: "#858796",
      borderColor: '#dddfeb',
      borderWidth: 1,
      xPadding: 15,
      yPadding: 15,
      displayColors: false,
      caretPadding: 10,
    },
    legend: {
      display: false
    },
    cutoutPercentage: 80,
  },
});

    
//pie charts ends here
 

</script> 

@endsection 