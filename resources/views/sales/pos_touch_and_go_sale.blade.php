@extends('layouts.erp_theme_03')
@section('content')
<?php 
   $cdn_url =    config('app.app_cdn') ;  
    $cdn_path =     config('app.app_cdn_path') ; // cnd - /var/www/html/api/public 
    $total_cost = $delivery_cost = 0.0; 
    $cartno = $data['cartno'];
    $new_cartno = $data['new_cartno'];
    $bin = $data['bin'];

    if( $new_cartno > $cartno)
    {
       $carttoupdate= $new_cartno;
    }
    else 
    {
       $carttoupdate =  $cartno;
    }

    $allcarts = $data['total_temp_cart']; 
?>


<div class="container">
  <div class='row'> 
    <div class='col-md-7'>

      <div class="card">
  <div class="card-header">
<div class='row'> 
    
<div class='col-md-12'> 
 
  <div class="form-row align-items-center"> 
    <div class="col-auto"> 
      <div class="input-group mb-2">
        <div class="input-group-prepend">
          <div class="input-group-text">Search Product</div> 
        </div>

        <select  class="form-control"  name='dbcategory' id='dbcategory'>
          @foreach($data['categories'] as $item)
            <option>{{ $item->category }}</option> 
          @endforeach
        </select>
        <input type="text" autocomplete="off"  class="form-control" id="keyword" placeholder="Product Name">
      </div>
    </div> 
 
  </div> 
  </div>
    </div>

  </div>

    


  <div class="card-body scrollbar bluescroll" id='dynload'  style='max-height: 680px; overflow-y: scroll;'>
      <div   class="card-group mt-1">
     @php 
      $colcount=1; 
     @endphp 
     @foreach($data['all_products'] as $product) 
            @php 
              $img_url = $cdn_url .  "/assets/image/no-image.jpg";  
            @endphp  
       
            @foreach($data['all_photos'] as $photo)
              @if($product->prsubid == $photo->prsubid)
               @php 
                $img_url = $photo->image_url;
                @endphp
                @break
              @endif
            @endforeach

    
        <div class="card">
          <img src="{{ $img_url }}"    class="card-img-top" alt="{{$product->pr_name}}">
          <div class="card-body">
            <div class="info">
              <p>{{$product->pr_name}}</p> 
              @if($product->unit_price == $product->actual_price)
                <p class="price-text-color">₹ {{$product->actual_price }}</p>
              @else
                <p class="price-text-color">₹ {{$product->actual_price }} <span class="old-price" style='text-decoration:line-through; color: #f00'>₹ {{$product->unit_price}}</span></p>
              @endif 
              <div class="separator clear-leftd">
                @if(  $product->stock_inhand == 0  )
                  <button type="button"   class="btn btn-outline-primary btn-sm btn-block" disabled>Out of stock</button> 
                @else
                  <button type="button" data-action="add" data-qty="1" data-bin="{{   $bin }}" 
                               data-key="{{  $product->prsubid }}"  class="btn btn-primary btn-sm btnadditem btn-block">Add to Cart</button> 
                @endif 
              </div>
            <div class="clearfix"></div>
          </div>  
          </div>
        </div>

      @if( $colcount % 4 == 0 )  
        </div>
        <div class="card-group  mt-1"> 
      @endif 

    @php
      $colcount++; 
    @endphp  
    @endforeach 
        
      </div>  
   </div> 
  <div id='dynpager' class="card-footer"> 
     {{ $data['all_products']->withQueryString()->links() }}
  </div> 
</div>
 
    </div>

    <div class='col-md-5'> 
    <div class="card">
  <div class="card-header">
    <div class='row'>
          <div class='col-md-10'>
            <h4 class="card-title">Create New Sales Order</h4> 
          </div>
          <div class='col-md-2 text-right'> 
           
          </div>
        </div>
  </div>
  <div class="card-body"> 

    @php 
             $pos = 1;
            @endphp 
            @foreach($allcarts as $cart)

              @if( $pos == 1  )
                @php 
                  $color_code = "danger";
                @endphp  
              @else 
                  @if($pos == 2)
                      @php 
                        $color_code = "warning";
                      @endphp 
                  @else 
                    @if($pos ==3 )
                        @php 
                          $color_code = "info";
                        @endphp 
                    @else 
                      @if($pos == 4 )
                          @php 
                            $color_code = "success";
                          @endphp 
                      @else 
                        @php 
                          $color_code = "info";
                        @endphp  
                      @endif 
                    @endif 
                  @endif  
              @endif 
              @php 
               $pos++;
              @endphp  
                <a class="btn btn-{{ $color_code }} btn-xs" href="{{ URL::to('/pos/pos-sales-window-touch-and-go')}}?enccno={{ Crypt::encrypt( $cart->cartNo ) }}">Cart # {{ $cart->cartNo }}</a> 
            @endforeach

            <a class='btn btn-primary btn-xs'  href="{{URL::to('/pos/pos-sales-window-touch-and-go')}}">
              <i class='fa fa-plus-circle'></i>
            </a>

            <input type='hidden' id="newcartno" name='newcartno' value='{{ $carttoupdate }}' />  
  </div>
</div>
 


 <form method="post" action="{{ action('Erp\PosController@placePosOrder') }}"> 
    {{ csrf_field() }}
 <div class='card mt-3'>
      <div class='card-body'>

         <div class='row'>
          <div class='col-md-8'>
            <h5>Current Cart # <span class='badge badge-success  '>{{  $carttoupdate  }}</span></h5>
          </div>
          <div class='col-md-4 text-right'>
             <a type="button"  data-widget='w1' data-param="{{  $data['cartno']  }}"  class="btn btn-danger  btn-sm btnremove">
                <i class='fa fa-trash'></i>
             </a>  
 
             <a type="button" href="{{ URL::to('/pos/pos-sales-window-touch-and-go')}}?enccno={{ Crypt::encrypt( $data['cartno'] ) }}" class="btn btn-success btn-sm btnrefresh"><i class='fa fa-sync'></i></a>
          </div>
        </div>



        <div class="order-details-confirmation" style='margin-bottom: 20px; margin-top: 10px;'>              
            
 
    @if( isset( $data['cart_items']  ) )
        @php 
          $total_cost =0;
          $memid = 0;
          $outofstockfound= false;
          $item_total =0;
          $package_total  =0;
          $cgst_pc =0;
          $sgst_pc =0;


         @endphp 
    <table class="table table-colored">
         
         <tr>
                <th width='60px'></th>
                <th style='width:250px; text-align:left' >Item</th>
                <th style='width: 400px'>Quantity</th>  
                <th class='text-right' style='width: 180px'>Cost</th>
            </tr>  
          @php 
            $img_url = $cdn_url .  "/assets/image/no-image.jpg";  
          @endphp 
        @foreach( $data['cart_items']  as $cartitem) 
        <tr>
          <td width='60px'>

            @foreach($data['photos'] as $photo)
              @if($cartitem->subid == $photo->prsubid)
               @php 
                $img_url = $photo->image_url;
                @endphp
                @break
              @endif
            @endforeach

            <img class='rounded ' width='40px' src="{{  $img_url  }}" alt='{{ $cartitem->subid }}'/>


          </td>
          <td style='width:250px; text-align:left' >
            @php 
              $product_name = "N/A";
            @endphp
             @foreach($data['products'] as $product)
              @if($cartitem->subid == $product->prsubid)
                @php 
                  $product_name = $product->pr_name;
                @endphp
                @break
              @endif
            @endforeach


            <strong>{{ $product_name }}</strong></td>
          <td style='width: 400px'>
            @if(  $cartitem->stock_inhand == 0  )
            <button type="button"   class="btn btn-outline-primary btn-sm  " disabled>Out of stock</button>
            &nbsp;
            <button type="button"   class='btn btn-danger btn-sm btndel' data-key="{{ $cartitem->key }}"><i class='fa fa-trash'></i></button>
            </td>
            <td class='text-right' style='width: 180px'>0.00</td>

            @php  
          $outofstockfound= true;
         @endphp 

            @else  
                <div class="input-group input-group-sm ">
                    <div class="input-group-prepend">
                            <button type="button" class="input-group-text btnupdatecart" 
                            data-prid="{{  $cartitem->subid }}" 
                            data-subid="{{  $cartitem->subid }}"  
                            data-bin="{{  $data['bin'] }}"   
                            data-step='rem' 
                            data-key="{{ $cartitem->key }}">-</button> 
                          </div>
                          <input type="number" class="form-control qty" aria-label="Quantity" step="1" min="1" max="" name="quantity" id='tbqty{{ $cartitem->key }}' value="{{ $cartitem->qty }}" title="Qty" >
                          <div class="input-group-append">
                           <button type="button" class="input-group-text btnupdatecart" 
                            data-prid="{{  $cartitem->subid }}" 
                            data-subid="{{  $cartitem->subid }}"  
                            data-bin="{{  $data['bin'] }}"  
                            data-step='add' data-key="{{ $cartitem->key }}">+</button> 
                          </div>
                    </div> 
                </td>  
                   
                <td class='text-right' style='width: 180px'>{{ ( $cartitem->actual_price *  $cartitem->qty ) + 
                    ($cartitem->packaging * $cartitem->qty)  }}</td>
            

             @php 
                
                $item_total  += ( $cartitem->actual_price *  $cartitem->qty ) + ($cartitem->packaging * $cartitem->qty) ; 
                $package_total  +=  ($cartitem->packaging * $cartitem->qty) ;  
                $cgst_pc += ( $cartitem->actual_price *  $cartitem->qty ) + ($cartitem->packaging * $cartitem->qty) ;  
                $sgst_pc += ( $cartitem->actual_price *  $cartitem->qty ) + ($cartitem->packaging * $cartitem->qty) ; 
             @endphp   

        @endif  
        </tr>                  
        @endforeach   
            <tr>
                   
                <td colspan='2' class='text-right'><strong>Item Cost:</strong></td>    
                <td class='text-right' colspan='2'>
                <input type='number' min='0' step='0.05' value='{{ $item_total   }}' id='tbitemcost' readonly name='tbitemcost' class='form-control form-control-sm' />
              </td>
            </tr>    
          <tr>
                   
                <td colspan='2' class='text-right'><strong>Packaging Cost:</strong></td>    
                <td class='text-right' colspan='2'>
                <input type='number' min='0' step='0.05' value='{{ $package_total   }}' id='tbpackaging' readonly name='tbpackaging' class='form-control form-control-sm' />
              </td>
            </tr>  
           <tr>
                   
                <td colspan='2' class='text-right'><strong>Total Amount:</strong></td>    
                <td class='text-right' colspan='2'>
                <input type='number' min='0' step='0.05' value='{{ $item_total + $package_total   }}' id='tbtotalamount' readonly name='tbtotalamount' class='form-control form-control-sm' />
              </td>
            </tr> 

        <tr>   
                <td colspan='2' class='text-right'><strong>Discount:</strong></td>    
                <td class='text-right' colspan='2'>
                  <input type='number' min='0' step='0.05' value='0.00' name='tbdiscount' id='tbdiscount' class='form-control form-control-sm' />
                </td>
        </tr>

 <tr>
             
                <td colspan='2'  class='text-right'><strong>Extra Charge:</strong></td>    
                <td class='text-right' colspan='2'>
                 <input type='number' step='.05' min='0'  name='extracost' id='extracost' class='form-control form-control-sm'  
                 value='0.00'   />
                </td>
        </tr>

        <tr>    
                <td colspan='2' class='text-right'><strong>CGST %:</strong></td>    
                <td class='text-right' colspan='2'>
                  <input type='number' min='0' step='0.05' value='0.00' id='tbcgst'  name='tbcgst' class='form-control form-control-sm' />
                </td>
        </tr>
 <tr>    
                <td colspan='2' class='text-right'><strong>SGST %:</strong></td>    
                <td class='text-right' colspan='2'>
                  <input type='number' min='0' step='0.05' value='0.00' id='tbsgst'  name='tbsgst' class='form-control form-control-sm' />
                </td>
        </tr>

<tr>    
                <td colspan='2' class='text-right'><strong>Total Cost:</strong></td>    
                <td class='text-right' colspan='2'>
                  <input type='number' min='0' step='0.05' value='{{ $item_total  +  $package_total  }}' id='tbtotalcost'  name='tbtotalcost' class='form-control form-control-sm' />
                </td>
        </tr>

        <tr>     
                <td colspan='2' class='text-right'><strong>Payment Type:</strong></td>    
                <td class='text-right' colspan='2'>
                   <div class="custom-control custom-radio custom-control-inline">
  <input type="radio" id="radiocash" name="paymode" class="custom-control-input" value="cash" checked>
  <label class="custom-control-label" for="radiocash">Cash</label>
</div>
<div class="custom-control custom-radio custom-control-inline">
  <input type="radio" id="radioonline" name="paymode" class="custom-control-input"  value="online">
  <label class="custom-control-label" for="radioonline">Online</label>
</div>
                </td>
        </tr>
 

<tr>
      
                <td colspan='2' class='text-right'><strong>Customer Name:</strong></td>    
                <td class='text-right' colspan='2'>
                  <input type='text' name='cname' class='form-control form-control-sm' value='customer name' />
                </td>
        </tr> 
        <tr>
             
                <td colspan='2'  class='text-right'><strong>Customer Phone:</strong></td>    
                <td class='text-right' colspan='2'>
                 <input type='text' name='cphone' name='cphone' class='form-control form-control-sm' required  placeholder='phone'/>
                </td>
        </tr>


        <tr>
     
                <td colspan='2' class='text-right'><strong>Address:</strong></td>    
                <td class='text-right' colspan='2'>
                  <textarea type='text' rows='2' id='caddress'  name='caddress' class='form-control form-control-sm' ></textarea>
                </td>
        </tr>
        <tr>
     
          <td colspan='2' class='text-right'><strong>Any order remarks:</strong></td>    
          <td class='text-right' colspan='2'>
            <textarea type='text' rows='3'  id='remarks'  name='remarks' class='form-control form-control-sm' ></textarea>
          </td>
        </tr> 
        <tr>
     
          <td colspan='2' class='text-right'><strong>Order Date:</strong></td>    
          <td class='text-right' colspan='2'>
            <input type='text' name='date' name='date' class='form-control form-control-sm calendar' required  placeholder='Order Date'/>
          </td>
        </tr>
      <tr>
        <td colspan='4' class='text-right'>        
        <input type='hidden' name='bin' value='{{ $data["bin"] }}' />  
        
        @if( $outofstockfound  )
            <button type='button'  class="btn btn-primary" disabled>Place Order</button> 
        @else  
            <button type='submit' value='placeorder' name='btnorder'  class="btn btn-primary">Place Order</button> 
        @endif
        <input type="hidden" name="enccno" value="{{ Crypt::encrypt( $data['cartno']) }}" />
      </td>
  </tr>
 </table>
@endif   
    </div>
    </div>
 </div> 
</form>

    </div> 
  </div> 
</div>
 


 <form method="post" action="{{ action('Erp\PosController@removeItemFromSalesWindow') }}"> 
    {{ csrf_field() }}
<div class="modal wgconfirm" tabindex="-1">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Confirm Item Removal</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Selected item will be removed from cart. Are you sure?</p>
      </div>
      <div class="modal-footer">
         <input type="hidden" name="key" id="delkey" />
         <input type="hidden" name="enccno" value="{{ Crypt::encrypt($data['cartno']) }}" />
        <button type="submit" value='delete' name='btndelete' class="btn btn-danger">Delete</button>
        <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
      </div>
    </div>
  </div>
</div>
</form>

 <form method="post" action="{{ action('Erp\PosController@removeTemporaryCart') }}"> 
    {{ csrf_field() }}
<div class="modal wgcartrem" tabindex="-1">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Confirm Cart Removal</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Selected cart will be removed. Are you sure?</p>
      </div>
      <div class="modal-footer"> 
         <input type="hidden" name="uitype" value="modern"/>
         <input type="hidden" name="enccno" value="{{ Crypt::encrypt($data['cartno']) }}" /> 
        <button type="submit" value='delete' name='btndelete' class="btn btn-danger">Delete</button>
        <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
      </div>
    </div>
  </div>
</div>
</form>

<div class="modal wgsystem" tabindex="-1">
  <div class="modal-dialog modal-dialog-centered modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">System Notification</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <span id='msgarea'></span>
      </div>
      <div class="modal-footer"> 
         <input type="hidden" name="uitype" value="modern"/>
         <input type="hidden" name="enccno" value="{{ Crypt::encrypt($data['cartno']) }}" />  
        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>


@endsection
@section("script")

<script>

 

 
$('#keyword').on('keyup',function(e){
  
  var keyword = $(this).val(); 
  if(keyword.length < 4) return;

  var category = $("select#dbcategory").find('option:selected').val();

  var gbin = $("#gbin").html();
  var guid = $("#guid").html(); 

  var json = {};
  json['guid'] =  guid  ;
  json['gbin'] =  gbin ; 
  json['keyword'] = keyword; 
  json['category'] = category; 
 
  $.ajax({

    type: 'post',
    url: erp  + "api/v3/pos/search-item-by-keyword" ,
    data: json,
    success: function(data)
    {
      data = $.parseJSON(data);  
       
      var dynamic_content = '<div   class="card-group mt-1">'; //fix
        
      if(data.status_code == 6007){
        var colcount=1;
        $.each(data.results, function(index, item)
        {
          dynamic_content +=  "<div class='card'><img src='" +  item.photo + 
            "' class='card-img-top' alt='" + item.name+ "'><div class='card-body'>";
          dynamic_content +=  "<div class='info'><p>" + item.name + "</p>";
          dynamic_content += "<p class='price-text-color'>₹ " + item.price+ "</p>";
          dynamic_content += "<div class='separator clear-leftd'>";

          if( item.stock  == 0  )
             dynamic_content += "<button type='button' class='btn btn-outline-primary btn-sm btn-block' disabled>Out of stock</button>";
          else
            dynamic_content += "<button type='button' data-action='add' data-qty='1' data-bin='"  + item.bin + "' " + 
          " data-key='" + item.prsubid  + "' class='btn btn-primary btn-sm btnadditem btn-block'>Add to Cart</button>"; 

          dynamic_content += "</div><div class='clearfix'></div></div></div></div>";

          if( colcount % 4 == 0 )
          {
            dynamic_content += "</div><div class='card-group  mt-1'>"; 
          }
          colcount++;

        }); 

        if(dynamic_content!="")
        {
          document.getElementById('dynload').innerHTML = ''; 
          document.getElementById('dynpager').innerHTML = '';   
        }
        
        $("#dynload").html(dynamic_content); 
      } 
      },
      error: function( ) 
      {
         $("#msgarea").html(  'An error while connecting the server, please try again after checking Internet connection!' ); 
      }
    });


 
});

 

$('#key').on('change',function(e){
  addItemToCart();
});
 
 
$('#tbdiscount').on('input',function(e){
   $total = $('#tbtotalamount').val();
   $discount  = $('#tbdiscount').val();
   $extra  = eval($('#extracost').val()) ; 
   $('#tbtotalcost').val(  eval( $total )  -  eval($discount)   +  $extra );
});


 
$('#extracost').on('input',function(e){
   $total = $('#tbtotalamount').val();
   $discount  = $('#tbdiscount').val();
   $extra  = eval($('#extracost').val()) ;

  $('#tbtotalcost').val(  eval( $total )  -  eval($discount)   +  $extra );
});
 

$('#tbcgst').on('input',function(e){
   $total = $('#tbtotalamount').val();
   $discount  = $('#tbdiscount').val();
   $extra  = eval($('#extracost').val()) ; 
   $totalamount = eval( $total )  -  eval($discount)   +  $extra; 
   $cgst = eval($('#tbcgst').val());
   $sgst = eval($('#tbsgst').val()); 
   $totaltaxed = $totalamount + (( $cgst + $sgst )*$totalamount) /100; 
   $('#tbtotalcost').val( $totaltaxed  );
});

$('#tbsgst').on('input',function(e){
   $total = $('#tbtotalamount').val();
   $discount  = $('#tbdiscount').val();
   $extra  = eval($('#extracost').val()) ;
   $totalamount = eval( $total )  -  eval($discount)   +  $extra; 
   $cgst = eval($('#tbcgst').val());
   $sgst = eval($('#tbsgst').val());  
   $totaltaxed = $totalamount + (( $cgst + $sgst )*$totalamount) /  100;  
   $('#tbtotalcost').val(  $totaltaxed);
});

 


$(document).on("click", ".btndel", function() {

  $("#delkey").val($(this).attr("data-key"));
  $(".wgconfirm").modal('show');
});

$(document).on("click", ".btnremove", function() {
 
  $(".wgcartrem").modal('show');
}); 
 

//adding item to temp 
$(document).on("click", ".btnaddproduct", function()
{
  addItemToCart();
})

//adding item to temp 
$(document).on("click", ".btnupdatecart", function()
{ 

  var subid =  $(this).attr("data-subid") ; 
  var qty =  1;
  var action = $(this).attr("data-step") ; 
  var cartno = $("#newcartno").val(); 
  var gbin = $("#gbin").html();
  var guid = $("#guid").html(); 

  var json = {};
  json['guid'] =  guid  ;
  json['gbin'] =  gbin ; 
  json['subid'] = subid; 
  json['qty'] =  qty ;
  json['action'] =  action ;
  json['cartno'] =  cartno ;
  json['uitype'] = "modern" ;

  $.ajax({

    type: 'post',
    url: erp  + "api/v3/pos/add-item-to-basket" ,
    data: json,
    success: function(data)
    {
      data = $.parseJSON(data);  
      if(data.status_code == 6001){

       window.location.href =  "<?php echo URL::to('/pos/pos-sales-window-touch-and-go')  ?>?enccno=" + data.enccno;
      }
      else{
         
        $("#msgarea").html( data.detailed_msg);
        $(".wgsystem").modal('show'); 
      }
      },
      error: function( ) 
      {
         $("#msgarea").html(  'An error while connecting the server, please try again after checking Internet connection!' ); 
      }
    });

}) 



$(document).on("click", ".btnadditem", function()
{
  var subid =  $(this).attr("data-key");

  addItemToCart(subid);
})


function addItemToCart(subid)
{
  if(subid == "") return;
  var qty =  1;
  var action = "add"; 
  var cartno = $("#newcartno").val(); 
  var gbin = $("#gbin").html();
  var guid = $("#guid").html();  
  var json = {};
  json['guid'] =  guid  ;
  json['gbin'] =  gbin ; 
  json['subid'] = subid; 
  json['qty'] =  qty ;
  json['action'] =  action ;
  json['cartno'] =  cartno ;
  json['uitype'] = "modern" ;

 


  $.ajax({

    type: 'post',
    url: erp  + "api/v3/pos/add-item-to-basket" ,
    data: json,
    success: function(data)
    {
      data = $.parseJSON(data);  
      if(data.status_code == 6001){

       window.location.href =  "<?php echo URL::to('/pos/pos-sales-window-touch-and-go')  ?>?enccno=" + data.enccno;
      }
     else{
         
        $("#msgarea").html( data.detailed_msg);
        $(".wgsystem").modal('show'); 
      }
      },
      error: function( ) 
      {
         $("#msgarea").html(  'An error while connecting the server, please try again after checking Internet connection!' ); 
      }
    });
}


 


$("#saleslist").on('click', '.btnDelete', function () {

    var key = $(this).attr("data-key-delete");
    $.ajax({

        type: 'get',
        data: {'productid':key},
        url : 'delete-product-temp-cart',
        dataType: 'json',
        contentType: 'application/json',
        success: function (response) {
           
            if (response.success) {

            }else {
                 alert("Unable to perform action");
            }
        }
    });


    
    $(this).closest('tr').remove();
    var rowCount = $('#saleslist >tbody >tr').length;

    if (rowCount>0) {
      var discount = parseFloat($("#discount").val());
      var delivery_charge = parseFloat($("#delivery_charge").val());
      var total_=parseFloat(0);

      $(".totalamount").each(function () {
              total_ += parseFloat($(this).val());
       });

      $("#total_amount").val(parseFloat(discount)+parseFloat(delivery_charge)+parseFloat(total_));

    }else{  
        
        $('.tb2 input[type="text"]').val("0");
        $('.tb2').hide();
        $('.tb1').hide();
        $("#prod_search").val("");
        $("#prodcode").val("");
    }
});
 

function showReceipt() {
  window.frames["printf"].print();
}
 
$(function() {
    $('.calendar').pignoseCalendar( 
    {
      format: 'DD-MM-YYYY' 
    });
});
 
 



</script>

<style>

.scrollbar
{
  margin-left: 30px;
  float: left;  
  overflow-y: scroll;
  margin-bottom: 25px;
}

.bluescroll::-webkit-scrollbar-track
{
  -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
  background-color: #F5F5F5;
  border-radius: 10px;
}

.bluescroll::-webkit-scrollbar
{
  width: 10px;
  background-color: #F5F5F5;
}

.bluescroll::-webkit-scrollbar-thumb
{
  border-radius: 10px;
  background-image: -webkit-gradient(linear,
                     left bottom,
                     left top,
                     color-stop(0.44, rgb(122,153,217)),
                     color-stop(0.72, rgb(73,125,189)),
                     color-stop(0.86, rgb(28,58,148)));
}

.col-item
{
    border: 1px solid #E1E1E1;
    border-radius: 5px;
    background: #FFF;
}
.col-item .photo img
{
    margin: 0 auto;
    width: 100%;
}

.col-item .info
{
    padding: 10px;
    border-radius: 0 0 5px 5px;
    margin-top: 1px;
}

.col-item:hover .info {
    background-color: #F5F5DC;
}
.col-item .price
{
    /*width: 50%;*/
    float: left;
    margin-top: 5px;
}

.col-item .price h5
{
    line-height: 20px;
    margin: 0;
}

.price-text-color
{
    color: #219FD1;
}

.col-item .info .rating
{
    color: #777;
}

.col-item .rating
{
    /*width: 50%;*/
    float: left;
    font-size: 17px;
    text-align: right;
    line-height: 52px;
    margin-bottom: 10px;
    height: 52px;
}

.col-item .separator
{
    border-top: 1px solid #E1E1E1;
}

.clear-left
{
    clear: left;
}

.col-item .separator p
{
    line-height: 20px;
    margin-bottom: 0;
    margin-top: 10px;
    text-align: center;
}

.col-item .separator p i
{
    margin-right: 5px;
}
.col-item .btn-add
{
    width: 50%;
    float: left;
}

.col-item .btn-add
{
    border-right: 1px solid #E1E1E1;
}

.col-item .btn-details
{
    width: 50%;
    float: left;
    padding-left: 10px;
}
.controls
{
    margin-top: 20px;
}
[data-slide="prev"]
{
    margin-right: 10px;
}


</style>

@endsection 