@extends('layouts.erp_theme_03')
@section('content')

<div class="row">
     <div class="col-md-8">
 
     <div class="panel panel-default">
           <div class="panel-heading">
              
 <?php 

$product = $data['product'];
 
?>

                <div class="card-title text-center"><h5>Edit/Update Product</h5></div>
 

              @if (session('err_msg'))
              <div class="col-12">
               <div class="alert alert-info">
                {{ session('err_msg') }}
                </div>
                 </div>
              @endif


              <form method='post' action="{{ action('Erp\ErpController@editBusinessProductSave') }}" enctype="multipart/form-data">
                {{ csrf_field() }}
                 
                <div class="pl-lg-4"> 
                  <div class="row">


                       <div class="col-lg-6">
                    
                      <div class="form-group">
                        <label class="form-control-label" for="product_name">Product Name:</label>
                        <input type="text" id="product_name" name='productName' class="form-control form-control-alternative" placeholder="Enter Product Name" value="{{$product->pr_name}}">

                      </div>
                    </div>

                    <div class="col-lg-6">
                    
                      <div class="form-group">
                        <label class="form-control-label" for="category">Category:</label>
                         <select   class='form-control form-control-sm custom-select my-1 mr-sm-2 ' name='categoryName' >

                          @foreach ($data['productCategory'] as $item)
                         
                          <option value='{{$item->category_name}}'>{{$item->category_name}}</option>
                         
                    
                    @endforeach
                </select>
                      </div>
                    </div>


                    </div>

                      <div class="row">
                    <div class="col-lg-6">
                      <div class="form-group">  
                      <label class="form-control-label" for="description">Description:</label>
                      <textarea type="text" id="description" class="form-control form-control-alternative" placeholder="Type some description here" name="productDescription" value="">{{$product->description}}</textarea>
                      
                      </div>
                    </div>

                      <div class="col-lg-6">
                           <div class="form-group">
                        <label class="form-control-label" for="unit">Unit:</label>
                         <select   class='form-control form-control-sm custom-select my-1 mr-sm-2 ' name='prtoductUnit' >

                          @foreach ($data['productUnits'] as $itemU)
                          

                          <option value='{{$itemU->unit}}'>{{$itemU->unit}}</option>
                         

                    
                         @endforeach 
                </select>
                      </div>
                    </div>
                  </div>

                    <div class="row">
                        <div class="col-lg-4">
                      <div class="form-group">
                        <label class="form-control-label" for="initial_stock">Initial Stock:</label>
                        <input type="number" id="initial_stock" name='initialStock' class="form-control form-control-alternative" placeholder="Only number" value="{{$product->initial_stock}}">
                      </div>
                    </div>

                   <div class="col-lg-4">
                      <div class="form-group">
                        <label class="form-control-label" for="stock_inhand">Stock in hand:</label>
                        <input type="number" id="stock_inhand" name='stockInHand' class="form-control form-control-alternative" placeholder="Only number" value="{{$product->stock_inhand}}">
                      </div>
                    </div>

                  <div class="col-lg-4">
                    <div class="form-group">
                        <label class="form-control-label" for="min_stock">Minimum Stock:</label>
                        <input type="number" id="min_stock" name='minStock' class="form-control form-control-alternative" placeholder="Only number" value="{{$product->min_required}}">
                      </div>
                    </div>

                  </div>

                  <div class="row">

                     <div class="col-lg-6">
                      <div class="form-group">
                        <label class="form-control-label" for="max_order">Maximum Order/Buy:</label>
                        <input type="number" id="max_order" name='maxOrder' class="form-control form-control-alternative" placeholder="Only number" value="{{$product->max_order}}">
                      </div>
                    </div>


                       <div class="col-lg-6">
                    
                      <div class="form-group">
                        <label class="form-control-label" for="unit_price">Price:</label>
                        <input type="number" id="unit_price" name='unitPrice' class="form-control form-control-alternative" placeholder="Only number" value="{{$product->unit_price}}">

                      </div>
                    </div>
                  </div>

                  <input type="hidden" name="productId" value="{{$product->id}}">

               
                  <button type="submit"  name='btnSave' class="btn btn-primary"  value='Save'>Update</button>
                 
              </div>

              
               
                 
              </form>
            </div>
          </div>
        </div>

       
        </div>

   @endsection  

