@extends('layouts.erp_theme_03')
@section('content')
 
<div class="row">
     <div class="col-md-12"> 
      <div class="card"  >
        <div class='card-header'>
          <div class='row'>
          <div class='col-md-8'>
             <h4 class="card-title">Manage Products</h4> 
          </div>
          <div class='col-md-4 text-right'>

            <form action="{{ action('Erp\ErpToolsController@viewAllProducts') }}" method='get'>
  <div class="form-row align-items-center">
    <div class="col-auto">
      <label class="sr-only" for="keyword">Search</label>
      <input type="text" class="form-control mb-2" id="keyword" name='keyword' placeholder="Search product">
    </div> 
    <div class="col-auto">
      <button type="submit" class="btn btn-primary  btn-xs mb-2" name='btnsearch' value='search'><i class='fa fa-search'></i></button>
      <button type="button"  class='btn btn-primary btn-xs mb-2' data-toggle="modal" data-target="#wgt-01"><i class='fa fa-plus'></i></button>
    </div>
  </div>
</form>


            
          </div>
        </div>

         
    </div>
      <div class="card-body" style="padding:0">  
              <div class="d"> 
              <div id="spreadsheet"></div> 
                <!-- <table class="table table-responsive">
                  <thead>
                    <tr>
                      <th>Product Code</th>
                      <th>Product Name</th>
                      <th>desc</th>
                      <th>Stock</th>
                      <th>Max </th>
                      <th>Min</th>
                      <th>Unit </th>
                      <th>Price</th>
                       <th>Disc </th>
                      <th>Packaging</th>
                      <th>Category</th>
                      
                    </tr>
                  </thead>
                  @foreach($data['products'] as $product)
                  <tbody>
                    <tr>
                      <td>{{$product->pr_code}}</td>
                      <td>{{$product->pr_name}}</td>
                      <td>{{$product->description}}</td> 
                      <td>{{$product->stock_inhand}}</td>
                      <td>{{$product->max_order}}</td>
                      <td>{{$product->min_required}}</td>
                      <td>{{$product->unit_name}}</td>
                      <td>{{$product->unit_price}}</td>
                      <td>{{$product->discount}}</td>
                      <td>{{$product->packaging}}</td>
                      <td>{{$product->category}}</td>
                       
                    </tr>
                  </tbody>
                   @endforeach

                </table> --> 
              </div>   
  
 
   </div> 
   <div class='card-footer'>
     <div class='row'>
          <div class='col-md-8'>
            {{ $data['products']->appends(request()->input())->links() }} 
          </div>
          <div class='col-md-4 text-right'>
            <div class='action_bar'>
              <button data-target="0" class='btn btn-primary btn-sm disabled btnview'>View Product</button> 
              <button data-target="0" class='btn btn-primary btn-sm disabled btneditimage'>Edit Image</button>
              <button data-target="0" class='btn btn-danger btn-sm disabled delproduct'>Delete</button>
            </div>
          </div>
    </div>  
    </div>
 </div>
</div>
</div>


<div class="modal confirmmodel" tabindex="-1" role="dialog"> 
  <form method='post' action="{{ action('Erp\ErpToolsController@deleteProduct') }}">
  {{  csrf_field() }}
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Confirm Product Deletion</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body"> 
        <p id='wgtdelthint'>You are about to delete this product. Are you sure about this operation?</p> 
      </div> 
      <div class="modal-footer"> 
          <input type="hidden" id='sbid' name='sbid'   >    
         <button type="button" class="btn btn-info btn-sm" data-dismiss="modal">No</button>
        <button type="submit" name="btnDelete" value="delete" class="btn btn-danger btn-sm">Yes</button>
      </div>
    </div>
  </div>
</form>
</div>

 
<form action="{{action('Erp\ErpToolsController@saveProduct')}}" method="post">
  {{csrf_field()}}
<div class="modal " id='wgt-01' tabindex="-1">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Add New Product</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          
        <div class="col-lg-12">
          
          <div class="form-row">
          <div class="form-group col-md-5">
            <label>Product Category:</label>
    <select class="form-control" name="productCategory">  
      @foreach($data['product_categories'] as $cat)
        <option value="{{$cat->category_name}}" >{{$cat->category_name}}</option>
      @endforeach
    </select> 
          </div> 

          <div class="form-group col-md-4">
        <label>Bar Code: (Optional)</label>
        <input type="text" class="form-control" name="barCode" placeholder="Bar Code" 
        @if(isset($barcode)) value="{{$barcode}}" @else value="" @endif>
      </div>

         
          </div>

<div class="form-row">

   <div class="form-group col-md-7">
            <label>Product Name:</label>
    <input type="text" class="form-control" name="productName" placeholder="Product Name"   />

  </div> 

<div class="form-group col-md-5">
    <label>Food Type:</label>
<select class="form-control" name="productFoodType">  
  <option value="Not-applicable">Not-applicable</option> 
  <option value="Veg"  >Veg</option>
  <option value="Non-Veg"  >Non-Veg</option>
</select>
  </div>


</div>

<div class="form-row">

  <div class="form-group col-md-3">
    <label>Amount:</label>
    <input type="number" class="form-control" name="productprice" placeholder="0.00"  id="productPrice" >
  </div>
  <div class="form-group col-md-3">
    <label>Package Charge:</label>
    <input type="number" class="form-control" name="productPackCharge" placeholder="0.00" />
  </div> 
   <div class="form-group col-md-3">
    <label>Discount Percentage:</label>
    <input type="number" class="form-control" name="productDiscountPercentage" step=".01" min='0.00'  id="productDiscountPC"  placeholder="0.00"  required >
  </div>
  <div class="form-group col-md-3">
    <label>Discount:</label>
    <input type="number" class="form-control" name="productDiscount" step=".01" min='0.00'  id="productDiscount" placeholder="0.00"  required >
  </div>


</div>

 
<div class="form-row">
 
  
  <div class="form-group col-md-3">
    <label>CGST %:</label>
    <input type="number" class="form-control" name="cgst" step=".01" min='0.00'  id="cgst" placeholder="CGST %"  required > 
  </div>

  <div class="form-group col-md-3">
    <label>SGST %:</label>
    <input type="number" class="form-control" name="sgst" step=".01" min='0.00'  id="sgst" placeholder="SGST %"  required >
  </div>

</div>

<div class="form-row">


  <div class="form-group col-md-4">
    <label>Unit Value</label>
    <input type="number" class="form-control" name="productUnitValue" 
    placeholder="Unit value"  >
  </div>


  <div class="form-group col-md-4">
    <label>Unit Name:</label>
<select class="form-control" name="productUnit">
  <option value="">--Select Unit Type--</option>
  @foreach($data['units'] as $val)
  <option value="{{$val->unit}}"  >{{$val->unit}}</option>
  @endforeach
</select>
  </div>

  <div class="form-group col-md-4">
    <label>Volume of product: (Optional)</label>
    <input type="number" class="form-control" name="productVolume" placeholder="Volume"   >
  </div>

</div>

<div class="form-row">
  <div class="form-group col-md-6">
    <label>Initial Stock:</label>
    <input type="number" step="1" min='1'    class="form-control" name="productInitialStock" 
    placeholder="Initial stock"  >
  </div>

 <div class="form-group col-md-6">
    <label>Stock in Hand:</label>
    <input type="number" step="1" min='1'   class="form-control" name="productStock" 
    placeholder="Stock in hand" >
  </div>

 <div class="form-group col-md-6">
    <label>Minimum Qty Per Order:</label>
    <input type="number" step="1" min='1'   class="form-control" name="productMiniumStock" 
    placeholder="Minimum Stock"  >
  </div>

<div class="form-group col-md-6">
    <label>Maximum Qty Per Order:</label>
    <input type="number" step="1" min='1'   class="form-control" name="productMaximumOrder" 
    placeholder="Maximum order" >
  </div>
</div>
<div class="form-row">
  <div class="form-group col-md-12">
            
    <label>Description:</label>
    <textarea class="form-control" required placeholder="Product Description" name="productDescription" height="100px"></textarea>         
          </div>
</div> 
      </div>
 
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary" name='btn_save' value='save'>Save changes</button>
      </div>
    </div>
  </div>
</div>


@endsection


@section("script")

<script>

 <?php 

    $cdn = config('app.app_cdn');

    $rows =  array(); 
    $rows_string ="";
    foreach ($data['products'] as $item)
    {
       $columns = array();
         
       $image_url =  $cdn  . "assets/image/no-image.jpg"; 
       
      $columns[] =  addcslashes($item->prsubid, "'") ;  
      $columns[] =  addcslashes($item->pr_name, "'") ; 
      $columns[] =  $item->stock_inhand;
      $columns[] =  $item->max_order;
      $columns[] =  $item->min_required;
      $columns[] = strtoupper( addcslashes($item->unit_name, "'")  ) ;  
      $columns[] =  $item->unit_price;  
      $columns[] =  $item->discount; 
      $columns[] =  $item->packaging;  
      $columns[] =  addcslashes( $item->category, "'\\"); 
      $rows[] =  "['" . implode( "','" , $columns) . "']" ; 

  }

  $categoryNames[] = array();  
      foreach($data['allCategories'] as $cat)
      {
        $category_names[] = "'". addcslashes(   $cat->category_name, "'\\")  .  "'" ;
      }
 

?>
var data = [  <?php echo implode(",", $rows) ; ?>  ];
 
 
 var changed = function(instance, cell, col, row, value) 
 {
    var key = extable.getValue("A" + ( parseInt(row) + 1) ) ;  
 
    if(key === null)
    {
      return;
    }

    var json = {}; 
    json['key'] = key; 
    var cellName = jexcel.getColumnNameFromId([col, row ]);  

 


   if(  col == 1)
    {
          json['col'] = "prname"  ;
          json['value'] =  value ;
    } 
    else if(  col == 2)
    {   
          json['col'] = "stock"  ;
          json['value'] =  value ;
     }
    else if( col == 3)
    {  
          json['col'] = "maxorder"  ;
          json['value'] =  value ;
    }
     else if( col == 4)
    {  
          json['col'] = "minstock"  ;
          json['value'] =  value ;
    }
    else if( col == 5)
    {  
          json['col'] = "untname"  ;
          json['value'] =  value ;
    }
    else if( col == 6)
    {  
          json['col'] = "untprice"  ;
          json['value'] =  value ; 
    } 
  
  else if( col == 7)
    {  
          json['col'] = "discount"  ;
          json['value'] =  value ; 
    } 
    else if( col == 8)
    {  
          json['col'] = "packcharge"  ;
          json['value'] =  value ; 
    } 
    else if( col == 9)
    {  
          json['col'] = "category"  ;
          json['value'] =  value ; 
    } 
  
 
    var url = api + "v2/erp/products/update-product-info" ;   
    apicall( json,url);   
 
}


var selectionActivated = function(instance,  x1, y1, x2, y2, origin) { 

var cellName1 = jexcel.getColumnNameFromId([x1, y1]);
var cellName2 = jexcel.getColumnNameFromId([x2, y2]);

var cellName1 = jexcel.getColumnNameFromId([x1, y1]);
var cellName2 = jexcel.getColumnNameFromId([x2, y2]);

 

if(  y1 >= -1  )
{
  $(".btnview").removeClass("disabled"); 
  $(".btnview").attr("data-row",  y1);
  $(".btnview").attr("data-col", x1 ); 
  $(".btneditimage").removeClass("disabled"); 
  $(".btneditimage").attr("data-row",  y1);
  $(".btneditimage").attr("data-col", x1 );  
  $(".delproduct").removeClass("disabled"); 
  $(".delproduct").attr("data-row",  y1);
  $(".delproduct").attr("data-col", x1 );

}
else 
{
    $(".btnview").addClass("disabled");
    $(".btnview").attr("data-row",  -9);
    $(".btnview").attr("data-col", -9); 
    $(".btneditimage").addClass("disabled");
    $(".btneditimage").attr("data-row", -9);
    $(".btneditimage").attr("data-col", -9 );  
    $(".delproduct").removeClass("disabled"); 
    $(".delproduct").attr("data-row",  y1);
    $(".delproduct").attr("data-col", x1 ); 

}

}



$(document).on('click', ".delproduct", function(){ 
  var row = $(this).attr("data-row");  
  var col = $(this).attr("data-col");     

  var r = $(this).attr("data-row");  
  if(r == -9) return;
  var key = extable.getValue("A" + ( parseInt(r) + 1) ) ;   
  var pname = extable.getValue("B" + ( parseInt(r) + 1) ) ;  
  if(key === null )
  {
      return;
  } 
  
  if(pname !== null)
    $("#wgtdelthint").html("You are about to delete this <span class='badge badge-info'>" + pname+ "</span>. Are you sure about this operation?");

 
  $("#sbid").val(key);
  $(".confirmmodel").modal("show");

});



var extable = jexcel(document.getElementById('spreadsheet'), {
    data:data,
    columnSorting:false,
     contextMenu:function() { return false; },
    colHeaders:  ['ID', 'Product Name', 'Current Stock', 'Max Order Qty', 'Minimum Stock', 'Unit Name', 'Unit Price',  'Discount', 'Packaging Cost', 'Category'  ],
    colWidths: [100,  200, 100, 100, 100, 100,100,100,70,100 ], 
  

    columns: [
        { type: 'text', readOnly:true  }, 
        { type: 'text'   },
        { type: 'text' },
        { type: 'text'  }, 
        { type: 'text'  },
        { type: 'dropdown' , source:[ 'PACK' , 'BOX', 'SACHET' , 'BAG' , 'BOTTLE', 'KG', 'LITER', 'METER', 'BUNDLE', 'CAN', 'GM' ,'TRAY', 'OTHER']  },
        { type: 'text'  }, 

        { type: 'text'  }, 
        { type: 'text'  },  
        { type: 'dropdown' , source:[  <?php echo implode(",", $category_names) ; ?>   ]  },  
     ],  
     onchange: changed,
     onselection: selectionActivated

});


function apicall(json, url ){  

    $.ajax({
      type: 'post',
      url: url ,
      data: json,
      success: function(data)
      {
        data = $.parseJSON(data);
        console.log(data);
      },
      error: function( ) {
        alert(  'Something went wrong, please try again')
      }
    });

  } 


  $(document).on('click', ".btnview", function(){

    var r = $(this).attr("data-row");  
    if(r == -9) return;

    var key = extable.getValue("A" + ( parseInt(r) + 1) ) ;   
    var key2 = extable.getValue("B" + ( parseInt(r) + 1) ) ;  
    if(key === null && key2==null)
    {
      return;
    } 

    window.location.href =  erp + "admin/product/enter-save-product?subid="+key+"&id="+key2;

  });
 
  $(document).on('click', ".btneditimage", function(){

    var r = $(this).attr("data-row");  
    if(r == -9) return;

    var key = extable.getValue("A" + ( parseInt(r) + 1) ) ;  
 


    if(key === null ) 
    {
      return;
    } 


    window.location.href =  siteurl + "product/edit-images?key=" + key ;

});



/*
 $(document).on('click', ".btn_del", function(){

  var id = $(this).attr("data-id"); 

  $("#pr_id").val(  id  );   
  $(".confirmmodel").modal("show");
 
});

$('#edit_table').Tabledit({
    url: '/erp/business/product-update',
    editButton: false,
    deleteButton: false,
    hideIdentifier: true,
    columns: {
        identifier: [0, '$item->id'],
        editable: [[0,'$image_url'],[1, '$item->pr_name'], [2, '$item->description'],
         [3, '$item->stock_inhand'], [4, '$item->max_order'], [5, '$item->unit_name'], 
         [6, '$item->unit_price']]
    }

  
});

*/


</script>
@endsection  


     



 
  
