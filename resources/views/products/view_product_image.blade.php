@extends('layouts.erp_theme_03')
@section('content')

<?php
  $product_info = $data['product_info'];
  $product_variant = $data['product_variant'];
?>


 
   <div class="row">
     <div class="col-md-12"> 
        <div class="col-md-12"> 
            @if(session('err_msg')) 
              <div class="alert alert-info">
                  {{ session('err_msg') }}
              </div>
            @endif 
         </div>
</div>


<div class='col-md-12'> 
  <div class="card card-default">  
      <div class="card-header"> 
        <div class='row'>
          <div class='col-md-10'>
            <h4 class="card-title">Product Images for {{ $product_info->pr_name}}</h4> 
          </div>
          <div class='col-md-2 text-right'>
            <button class='btn btn-primary btn-xs' data-toggle="modal" data-target="#wgt-01">+</button>
          </div>
        </div>
          
      </div>
  <div class="card-body"> 
<div class="row"> 
  <?php 

  $images =   $data['product_photos'] ; 
  $pos=1; 
  if(count($images) > 0 )
  {
    foreach($images as $image )
    {
      ?>
      <div class='col-md-3'>

       
      <div class="card mt-2"> <div class="card-body">  
         <div class="dropdown" style="text-align: right;">
  <button class="btn btn-secondary btn-xs  " type="button" id="ddm{{ $pos }}" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    <i class='fa fa-cog'></i>
  </button>
  <div class="dropdown-menu" aria-labelledby="ddm{{ $pos }}">
    <button class="dropdown-item btnconfirmdel" data-imgid="{{ $image->id }}" data-key="{{ $image->prsubid }}"   >Delete</button>
  </div>
</div>
<hr/>

<img width="160px" src="{{ $image->image_url  }}"    > 

</div>
</div>
</div>

      <?php  
    } 
  }
  else 
  {
    ?>
    <p class='alert alert-info'>No product image uploaded so far.</p>
    <?php 
  }
  ?>
    </div>
  </div>
  </div>  
</div>

 


</div>



</div>



          </div> 
      <hr/> 
       
 
 </div>
          </div>
        </div>  
   </div>
 
  
 
 </div>

  <form action="{{ action('Erp\ErpToolsController@uploadProductImage') }}" method="post" enctype="multipart/form-data">
   {{ csrf_field() }} 
   <div id="wgt-01" class="modal" tabindex="-1">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Upload New Product Image</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <p>Select image from your local device and upload.</p> 

            <input type='file' name='photos[]' multiple placeholder='Select Product Image' /> 
                      
                
        </div>
        <div class="modal-footer">
          <input type="hidden" name='key' value="{{  $data['key']}}"   />   
          <button type="submit" name='btn_save' class="btn btn-primary btn-sm">Upload</button>
           <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Cancel</button> 
        </div>
      </div>
    </div>
  </div>
</form>


<form action="{{ action('Erp\ErpToolsController@removeProductImage') }}" method="post" enctype="multipart/form-data">
   {{ csrf_field() }} 
<div class="modal" id="wgt-02" tabindex="-1">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Confirm Image Deletion</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>You are about to remove an image. This action is not recoverable. Are you sure about this operation?</p>
      </div>
      <div class="modal-footer">
        <input type="hidden" value="" id="key01" name="key"/>
        <input type="hidden" value="" id="imageid" name="imageid"/>
        <button type="submit" name='btndelete' value='delete' class="btn btn-danger">Confirm Delete</button>
        <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
      </div>
    </div>
  </div>
</div>
</form>

@endsection  

@section("script")


<script type="text/javascript">


  $(".btnconfirmdel").on('click', function(){

    $key = $(this).attr("data-key");  
    $("#key01").val($key);

    $imageid = $(this).attr("data-imgid");  
    $("#imageid").val($imageid); 
 

  $("#wgt-02").modal('show');
});
 


                  function readURL(input) {
                    if (input.files && input.files[0]) {
                      var reader = new FileReader();
            
                    reader.onload = function (e) {
                        $('#input-icon-tag').attr('src', e.target.result);
                    }
                     reader.readAsDataURL(input.files[0]);
                   }
                 }
                 $("#input-icon").change(function(){
                  readURL(this);
                });
              

 

$(document).on("click", ".btn_edit", function()
{
  var id = $(this).attr("data-id"); 
  var name = $(this).attr("data-name"); 
  var url = $(this).attr("data-url"); 
var desc = $(this).attr("data-desc"); 


    $("#category_name").val(name);
    $("#catimage").attr("src",  url); 
    $("#category_description").val(desc);


})





$(document).on("click", ".btn_del_category", function()
{
    var id = $(this).attr("data-id");
     $("#hidcid").val( id ); 

    $("#del_category").modal("show"); 

})


 </script>
             

@endsection
