@extends('layouts.erp_theme_03')
@section('content')

<div class="row">
     <div class="col-md-12">
 
     <div class="card card-default">
           <div class="card-header">
              <div class="card-title"> 
               <div class="row">

                <div class="col-md-4">
                   <h3>Product List:</h3>
                </div>

                <div class="col-md-8">
                  <div class="form-row">
                  <div class="col-md-2"> 
                  <button data-target="0" class='btn btn-primary disabled btnview m-10'>View Details</button> 

                  </div>

                  <div class="col-md-6"> 
                  {{  $data['products']->withQueryString()->links() }}
                  </div>
    
            	     </div>
                </div>


            </div>
            </div>

            </div>
        </div>
    </div>
</div>



<div class="row">
<div class="col-md-12">
<div class="card m">
 

 <div id="spreadsheet"></div>

              

</div>
</div>
</div>




@endsection
@section("script")
<script>

 <?php 

    $cdn = config('app.app_cdn');

    $rows =  array(); 
    $rows_string ="";
    foreach ($data['products'] as $item)
    {
       $columns = array();
      
      $columns[] =  addcslashes($item->prsubid, "'") ; 
      $columns[] =  addcslashes($item->pr_code, "'") ; 
      $columns[] =  addcslashes($item->pr_name, "'") ;
      $columns[] =  $item->stock_inhand;
       
      $columns[] = strtoupper( addcslashes($item->unit_name, "'")  ) ;  
      $columns[] =  $item->unit_price;  
        
      $columns[] =  $item->category;  
      $rows[] =  "['" . implode( "','" , $columns) . "']" ; 

  }

  $categoryNames[] = array();  
      foreach($data['allCategories'] as $cat)
      {
        $category_names[] = "'". $cat->category_name .  "'" ;
      }
 

?>
var data = [  <?php echo implode(",", $rows) ; ?>  ];
 
 
 var changed = function(instance, cell, col, row, value) 
 {
    var key = extable.getValue("A" + ( parseInt(row) + 1) ) ;  
 
    if(key === null)
    {
      return;
    }

    var json = {}; 
    json['key'] = key; 
    var cellName = jexcel.getColumnNameFromId([col, row ]);  
 

   if(  col == 1)
    {
          json['col'] = "prname"  ;
          json['value'] =  value ;
    }
     
    else if(  col == 2)
    {   
          json['col'] = "stock"  ;
          json['value'] =  value ;
     }
    
    else if( col == 3)
    {  
          json['col'] = "untname"  ;
          json['value'] =  value ;
    }
    else if( col == 4)
    {  
          json['col'] = "untprice"  ;
          json['value'] =  value ; 
    } 
  
    
    else if( col == 5)
    {  
          json['col'] = "category"  ;
          json['value'] =  value ; 
    } 
   
 
    var url = api + "v2/erp/products/update-product-info" ;   
    apicall( json,url);   
 
}


var selectionActivated = function(instance,  x1, y1, x2, y2, origin) { 

var cellName1 = jexcel.getColumnNameFromId([x1, y1]);
var cellName2 = jexcel.getColumnNameFromId([x2, y2]);

var cellName1 = jexcel.getColumnNameFromId([x1, y1]);
var cellName2 = jexcel.getColumnNameFromId([x2, y2]);

 

if(  y1 >= -1  )
{
  $(".btnview").removeClass("disabled"); 
  $(".btnview").attr("data-row",  y1);
  $(".btnview").attr("data-col", x1 );


  $(".btneditimage").removeClass("disabled"); 
  $(".btneditimage").attr("data-row",  y1);
  $(".btneditimage").attr("data-col", x1 );



}
else 
{
  $(".btnview").addClass("disabled");
  $(".btnview").attr("data-row",  -9);
  $(".btnview").attr("data-col", -9);

  $(".btneditimage").addClass("disabled");
  $(".btneditimage").attr("data-row", -9);
  $(".btneditimage").attr("data-col", -9 ); 

}
 
 

}



var extable = jexcel(document.getElementById('spreadsheet'), {
    data:data,
    columnSorting:false,
     contextMenu:function() { return false; },
    colHeaders:  ['ID','Code', 'Product Name','Current Stock', 
    'Unit Name', 'Unit Price','Category' ],
    colWidths: [ 100,200, 400, 100, 100, 100, 200 ], 
  

    columns: [
        { type: 'text', readOnly:true  },
        { type: 'text'  },
        { type: 'text'   },
        { type: 'text'   },
        { type: 'text'   },
        { type: 'text'  },  
        { type: 'dropdown' , source:[  <?php echo implode(",", $category_names) ; ?>   ]  } 
        
     ],  
     onchange: changed,
     onselection: selectionActivated

});


function apicall(json, url ){  

    $.ajax({
      type: 'post',
      url: url ,
      data: json,
      success: function(data)
      {
        data = $.parseJSON(data);
        console.log(data);
      },
      error: function( ) {
        alert(  'Something went wrong, please try again')
      }
    });

  } 


  $(document).on('click', ".btnview", function(){

    var r = $(this).attr("data-row");  
    if(r == -9) return;

    var key = extable.getValue("A" + ( parseInt(r) + 1) ) ;   
    var key2 = extable.getValue("B" + ( parseInt(r) + 1) ) ;  
 
    if(key === null)
    {
      return;
    }

    window.location.href =  erp + "admin/product/enter-save-product?subid="+key+"&id="+key2;

  });
 
  $(document).on('click', ".btneditimage", function(){

    var r = $(this).attr("data-row");  
    if(r == -9) return;

    var key = extable.getValue("A" + ( parseInt(r) + 1) ) ;   
 
    if(key === null)
    {
      return;
    } 


    window.location.href =  erp + "product/edit-images?key=" + key;

});

</script>
@endsection 