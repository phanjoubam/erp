@extends('layouts.erp_theme_03')
@section('content')

<?php 

if(isset($all['prod'])) { 
 
  $val = $all['prod'];
  $category = $val->category;
  $prodcode = $val->pr_code;
  $barcode= $val->barcode;
  $prodname = $val->pr_name;
  $proddescription = $val->description;
  $proddescription = $val->description;
  $price = $val->unit_price;
  $package = $val->packaging;
  
  $foodtype = $val->food_type;
  $discountPC = $val->discountPc;
  $discount = $val->discount;

  $unitname = $val->unit_name;
  $unitvolume = $val->volume;
  $unitvalue = $val->unit_value;

  $stockinitial = $val->initial_stock;
  $stockinhand = $val->stock_inhand;
  $minorder = $val->min_required;
  $maxorder = $val->max_order;

  $cgst = $val->cgst_pc;
  $sgst = $val->sgst_pc;

  $id = $val->id;
  $subid = $val->prsubid;

}
  
?>

@if (session('err_msg'))
<div class="row mt-12"> 
<div class="col-md-6 offset-md-3">
    <div class="alert alert-info">
        {{ session('err_msg') }}
    </div>
    </div>
    </div>
@endif
 
<div class="container-fluid page-content content-paddding">


<div class="row mt-12">
  
<div class="col-md-8 offset-md-3">

  <div class="card  ">


    <div class="card-body">
      <div class="row">
<form action="{{action('Erp\ErpToolsController@saveProduct')}}" method="post">
  {{csrf_field()}}
        <input type="hidden" @if(isset($subid)) value="{{$subid}}" @else value="" @endif  name="subid">
        <div class="col-lg-12">
          @if(isset($id))<h5>Update Product <span class='badge badge-info badge-pill'>{{$prodname}}</span></h5> @else<h5>Add New Product</h5> @endif
          
          <hr>
          <div class="form-row">
          <div class="form-group col-md-4">
            <label>Product Category:</label>
    <select class="form-control" name="productCategory">
      <option value="0">--Select Product Category--</option>

      @foreach($all['cat_'] as $cat)
      <option value="{{$cat->category_name}}" 

        @if(isset($category))
        {{ $cat->category_name == $category ? 'selected' : '' }}
        @endif
        >

        {{$cat->category_name}}


      </option>
      @endforeach
    </select>
  </div>
  <div class="form-group col-md-4">
        <label>Bar Code: (Optional)</label>
        <input type="text" class="form-control" name="barCode" placeholder="Bar Code" 
        @if(isset($barcode)) value="{{$barcode}}" @else value="" @endif>
      </div>
          
  </div>

<div class="form-row">
<div class="form-group col-md-7">
            <label>Product Name:</label>
    <input type="text" class="form-control" name="productName" placeholder="Product Name" 
    @if(isset($prodname)) value="{{$prodname}}" @else value="" @endif>

          </div>

          <div class="form-group col-md-5">
    <label>Food Type:</label>
<select class="form-control" name="productFoodType">  
  <option value="Not-applicable" 
  @if(isset($foodtype))
  {{ $foodtype == "Not-applicable" ? 'selected' : '' }}
  @endif
  >Not-applicable</option>

  <option value="Veg" 
  @if(isset($foodtype))
  {{ $foodtype == "Veg" ? 'selected' : '' }}
  @endif
  >Veg</option>
  <option value="Non-Veg" 
  @if(isset($foodtype))
  {{ $foodtype == "Non-Veg" ? 'selected' : '' }}
  @endif
  >Non-Veg</option>
</select>
  </div>


          </div>
 
<div class="form-row">

  <div class="form-group col-md-3">
    <label>Amount:</label>
    <input type="number" class="form-control" name="productprice" placeholder="Price" 
   id="productPrice" @if(isset($price)) value="{{$price}}" @else value="" @endif >
  </div>
  <div class="form-group col-md-3">
    <label>Package Charge:</label>
    <input type="number" class="form-control" name="productPackCharge" placeholder="Packaging charges" @if(isset($package)) value="{{$package}}" @else value="" @endif >
  </div>


 
  <div class="form-group col-md-3">
    <label>Discount Percentage:</label>
    <input type="number" class="form-control" name="productDiscountPercentage" step=".01" min='0.00' 
    id="productDiscountPC" 
    placeholder="Discount Percentage" @if(isset($discountPC)) value="{{$discountPC}}" @else value="0.00" @endif >
  </div>
  <div class="form-group col-md-3">
    <label>Discount:</label>
    <input type="number" class="form-control" name="productDiscount" step=".01" min='0.00'  id="productDiscount" placeholder="Discount"
    @if(isset($discount)) value="{{$discount}}" @else value="0.00" @endif >
  </div>


</div>

<div class="form-row">
    

  <div class="form-group col-md-3">
    <label>CGST %:</label>
    <input type="number" class="form-control" name="cgst" step=".01" min='0.00'  id="cgst" placeholder="CGST %"  required @if(isset($cgst)) value="{{$cgst}}" @else value="" @endif> 
  </div>

  <div class="form-group col-md-3">
    <label>SGST %:</label>
    <input type="number" class="form-control" name="sgst" step=".01" min='0.00'  id="sgst" placeholder="SGST %"  required @if(isset($sgst)) value="{{$sgst}}" @else value="" @endif >
  </div>

</div>

<div class="form-row"> 

  <div class="form-group col-md-4">
    <label>Unit Value</label>
    <input type="number" class="form-control" name="productUnitValue" 
    placeholder="Unit value" @if(isset($unitvalue)) value="{{$unitvalue}}" @else value="" @endif >
  </div>


  <div class="form-group col-md-4">
    <label>Unit Name:</label>
<select class="form-control" name="productUnit">
  <option value="">--Select Unit Type--</option>
  @foreach($all['unit'] as $val)
  <option value="{{$val->unit}}" 
    
    @if(isset($unitname)) 
    {{ $val->unit == $unitname ? 'selected' : '' }}
    @endif >
    
    {{$val->unit}}

  </option>
  @endforeach
</select>
  </div>

  <div class="form-group col-md-4">
    <label>Volume:</label>
    <input type="number" class="form-control" name="productVolume" 
    placeholder="Volume" @if(isset($unitvolume)) value="{{$unitvolume}}" @else value="0" @endif >
  </div>

</div>

<div class="form-row">
  <div class="form-group col-md-3">
    <label>Initial Stock:</label>
    <input type="number" step="1" min='1'   class="form-control" name="productInitialStock" 
    placeholder="Initial stock" @if(isset($stockinitial)) value="{{$stockinitial}}" @else value="0" @endif>
  </div>

  <div class="form-group col-md-3">
    <label>Stock in Hand:</label>
    <input type="number" step="1" min='1'   class="form-control" name="productStock" 
    placeholder="Stock in hand" 
    @if(isset($stockinhand)) value="{{$stockinhand}}" @else value="0" @endif>
  </div>

  <div class="form-group col-md-3">
    <label>Minimum Qty Per Order:</label>
    <input type="number" step="1" min='1'  class="form-control" name="productMiniumStock" 
    placeholder="Minimum Stock" 
    @if(isset($minorder)) value="{{$minorder}}" @else value="1" @endif>
  </div>

  <div class="form-group col-md-3">
    <label>Maximum Qty Per Order:</label>
    <input type="number" step="1" min='1'  class="form-control" name="productMaximumOrder" 
    placeholder="Maximum order"
    @if(isset($maxorder)) value="{{$maxorder}}" @else value="1" @endif>
  </div>
</div>
<div class="form-row">
  <div class="form-group col-md-12">
            
    <label>Description:</label>
    <textarea class="form-control" required placeholder="Product Description" name="productDescription" height="100px">@if(isset($proddescription)){{$proddescription}}@endif</textarea>         
          </div>
</div>
<div class="form-row">
  <div class="form-group col-md-4">
    @if(isset($id))
    <button class="btn btn-primary">Update</button>    
   @else
    <button class="btn btn-primary" name="btn_save" value="Save">Save</button>
   @endif
   </div>
</div>




      </div>
    </form>
      </div>
    </div>
  </div>

</div>

 </div>
</div>


@endsection
@section("script")

<script type="text/javascript">
  

  $('#productDiscountPC').keyup(function() {
    var price = $('#productPrice').val();
    if (price=='') {

      $('#productPrice').focus();
    }
    var pc = $('#productDiscountPC').val();

    var discount = price * pc/100;
    $('#productDiscount').val(parseFloat(discount).toFixed(2));

    });

  
$('#productDiscount').keyup(function() {
    var price = $('#productPrice').val();
    if (price=='') {
      
      $('#productPrice').focus();
    }
    var discount = $('#productDiscount').val();

    var pc = discount/price * 100;
    $('#productDiscountPC').val(parseFloat(pc).toFixed(2));

    });

</script>

@endsection 