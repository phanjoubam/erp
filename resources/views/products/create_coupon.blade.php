@extends('layouts.erp_theme_03')
@section('content')
<?php 
    $cdn_url =    config('app.app_cdn') ;  
    $cdn_path =     config('app.app_cdn_path') ; // Content Delivery Network - /var/www/html/api/public
?>
<form method="post" action="{{action('Erp\ErpToolsController@saveCoupon')}}">
<div class="row">
     <div class="col-md-12" >
     	<div class="card card-default" style="background: #efefef;">
           <div class="card-header">
              <div class="card-title"> 
              	<div class="row">
              		<div class="col-md-8">
              <h7>Create Coupons</h7>
              <br>
               @if (session('err_msg'))
               
               <div class="alert alert-info">
                {{ session('err_msg') }}
                </div>
                
              @endif

          </div>
          <div class="col-md-4">
          	
              <h7>Category List</h7>
				<br>
				<select class="form-control selectCategory" id="selectCategory" name="category">
				<option value="0">All</option>
				@foreach($category as $item)
				 <option value="{{$item->category_name}}">{{$item->category_name}}</option>

				@endforeach 
				</select>
			</div>

</div>

              </div>
          </div>
      </div>
	</div>
</div>



 <div class="card mt-2">
 	<div class="card-body">
<div class="row">


<div class="col-md-7">
<h4 class="badge badge-info">Products List:</h4>

 <div id="productList" class="overflow-auto" style="height:700px;overflow-y: scroll;">
 
 </div>

</div>





<div class="col-md-5">
	<div class="card">
<div class="card-body overflow-auto" style="height:700px;overflow-y: scroll;">
<h4 class="badge badge-success">Generate Coupon For Selected Products:</h4>

	{{csrf_field()}}
<table class="table">
<tbody>
	<tr>
		<td class="col-md-5">
			<input type="text" class="form-control" placeholder="Coupon" name="couponcode">
		</td>

		<td class="col-md-6">
			<input type="text" class="form-control" placeholder="Description" name="description">
		</td>

		<td><button class="btn btn-primary">Save</button></td>
	</tr>
</tbody>
</table>
<hr>

<table class="table table-bordered overflow-auto" id="selectedproductList">
<thead>
	<th>Product Name</th>
	<th></th>
</thead>
<tbody>
	

</tbody>
</table>
	
</div>
</div>
</div>

 
</div>
</div>
</div>
</form> 


<div class="modal  fade" id="showInfoDialogue" tabindex="-1" aria-labelledby="showInfoDialogue" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"><span class="badge badge-info">Warning Status</span></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
         <p>Selected product already present in the coupon list</p>
      </div>
      <div class="modal-footer">
      	<button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>





@endsection
 

@section("script") 

<script> 

         
$(".selectCategory").change(function(){

	var key = $("#selectCategory").val();

	 
$.ajax({
         url: 'get-product-by-category',
         type: 'get',
		 data: {'code':key
			},          
         dataType: 'json',
         contentType: 'application/json',
         success: function(response){ 
           if (response.success) {
              $("#productList").empty().append(response.html);
           }else{
             alert("failed");
           }
         }
         });

 
});



$(document).on("change", "input[name='products']", function () {
    
    if (this.checked) {
		var key = $(this).attr("data-key");

		$.ajax({
         url: 'product-select-list',
         type: 'get',
		 data: {'code':key
			},          
         dataType: 'json',
         contentType: 'application/json',
         success: function(response){ 
           if (response.success) {
               
           	var selected = $('#prod'+key).val();

           	var list = $('#pr'+key).val();
           	 
           	if (list==selected) {
           		$('#prod'+selected).prop('checked', false);
           		$("#showInfoDialogue").modal("show");
           		return;
           	}

            $("#selectedproductList > tbody:last").append(response.html);

           }else{
             alert("failed");
           }
         }
         });


    }else{
	 var key = 	$(this).attr("data-key");

	   $('#'+key).remove(); 
   

    }
});


$(document).on("click", "#btnDelete", function () {

	var key = $(this).attr("data-key-delete");

	var key2 = $("#prod"+key).val();
	  
	$('#'+key).remove();
	
	if (key==key2) {

		$('#prod'+key2).prop('checked', false);

	}
});

</script>
@endsection