@extends('layouts.hollow')

@section('content')
<div class="container">
    <div class="row " style='margin-top: 220px'>
        <div class="col-md-4 offset-md-4">
            <div class="card card-default">
                <div class="card-header text-center">
<img src="{{ URL::to('/public') }}/assets/image/logo.png"  width='70' />

                </div>

                <div class="card-body"> 
                 @if (session('err_msg')) <div class="col-12">
                   <div class="alert alert-info">
                    {{ session('err_msg') }}
                    </div>
                     </div>
                 @endif

                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="phone" class="col-md-12 col-form-label ">{{ __('Phone') }}</label>

                            <div class="col-md-12">
                                <input id="phone" type="text" class="form-control" name="phone" value="{{ old('phone') }}" required autocomplete="phone" autofocus> 
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-12 col-form-label  ">{{ __('Password') }}</label>

                            <div class="col-md-12">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        


                        <div class="form-group row">
                            <div class="col-md-12  ">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mb-0 text-center">
                            <div class="col-md-12">
                                <input type='hidden'    name="category"  value='10000' />  
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Login') }}
                                </button>
<br/>

                                @if (Route::has('password.request'))
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                @endif
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
