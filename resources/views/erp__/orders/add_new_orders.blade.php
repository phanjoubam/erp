@extends('layouts.erp_theme_02')
@section('content')

<div class="row">
     <div class="col-md-12">
 
     <div class="panel panel-default">
           <div class="panel-heading">
              
              <div class="row">
                <div class="col-lg-8">
                <div class="card-title"><h5>Customer Details</h5></div>
              </div>

              <div class="col-lg-4">
                <div class="text-center">
                Date : {{date('d-m-Y')}}
              </div>
              </div>
            </div> 
                <hr/>  
              @if (session('err_msg'))
              <div class="col-12">
               <div class="alert alert-info">
                {{ session('err_msg') }}
                </div>
                 </div>
              @endif


              <form method='post' action="{{ action('Erp\ErpToolsController@saveNewOrder') }}" enctype="multipart/form-data">
                {{ csrf_field() }}
                 
                <div class="pl-lg-4"> 
                  <div class="row">


                       <div class="col-lg-6">
                    
                      <div class="form-group">
                        <label class="form-control-label" for="customer_name">Customer Name:</label>
                        <input type="text" id="customer_name" name='customerName' class="form-control form-control-alternative" placeholder="Enter Customer Name" value="">

                      </div>
                    </div>

                    <div class="col-lg-4">
                    
                      <div class="form-group">
                        <label class="form-control-label" for="phone">Phone Number:</label>
                        <input type="number" id="phone" name='phone' class="form-control form-control-alternative" placeholder="Enter Phone Number" value="">

                    <!--    <label class="form-control-label" for="category">Category:</label>
                         <select   class='form-control form-control-sm custom-select my-1 mr-sm-2 ' name='categoryName' >

                          @foreach ($data['productCategory'] as $item)
                         
                          <option value='{{$item->category_name}}'>{{$item->category_name}}</option>
                         
                    
                    @endforeach
                </select> -->
                      </div>
                    </div>


                    </div>

                      <div class="row">

                      <div class="col-lg-6">
                           <div class="form-group">
                        <label class="form-control-label" for="address">Address:</label>
                       <input type="text" id="address" name='address' class="form-control form-control-alternative" placeholder="Address" value="">
                      </div>
                    </div>


                  </div>

                 <!-- <button type="submit"  name='btnSave' class="btn btn-primary"  value='Save'>Add</button> -->
                 
              </div>

              
               
                 
              
            </div>
          </div>
        </div>

       
        </div>

     <div class="row">
     <div class="col-md-12">
 
     <div class="panel panel-default">
           <div class="panel-heading">
              

                <div class="card-title"><h5>Sales Item Details</h5></div>

                <hr/>

                 <table id="edit_table" class="table table-bordered">
      

  <thead class="text-primary">
    <tr>
    <th>Product Name</th>
    <th>Quantity</th>
    <th>Price</th>
    <th>Amount</th> 
    <th class="text-center"><a href="#" class="addRow"><i class="glyphicon glyphicon-plus"></i></a></th>
    </tr>
    </thead>

    <tbody>


     <tr>
      <td>
        <select class="form-control selectpicker" data-live-search="true" name="productName[]" id="productName">
                         <option value="0" selected="true" disabled="true">Select Product</option>

                          @foreach ($data['products'] as $itemP)
                          <option value="{{$itemP->id}}">{{$itemP->pr_name}}</option>
                         
                    
                    @endforeach
                </select>
      </td>

      <td><input type="number" id="quantity" name="quantity[]" class="form-control form-control-alternative qty" placeholder="Only number" value=""></td>

      <td><input type="number" id="price" name="unitPrice[]" class="form-control form-control-alternative price" placeholder="Only number" value=""></td>

      <td><input type="text" id="amount" name="amount[]" class="form-control form-control-alternative amount" placeholder="Only number" value=""></td>

    

         <td class="text-center">
          <a href="#" class="btn btn-danger btn-sm remove"><i class="glyphicon glyphicon-remove"></i></a>      
        </td>

    </tr>
  
    
 
   </tbody>

   <tfoot>
     <tr>
       <td style="border: none"></td>
       <td style="border: none"></td>
       <td><b>Total</b></td>
       <td ><b class="total"></b></td>
       
       <td></td>
     </tr>
   </tfoot>


  </table>

  <hr/>
 
<button type="submit"  name='btnSave' class="btn btn-primary"  value='Save'>Save</button>
            
          </div>
        </div>

       
        </div>
      </div>

      </form>

   @endsection  


   @section("script")

<script>
$('tbody').delegate('.selectpicker','change', function(){


   var tr = $(this).parent().parent();
   var id = tr.find('.selectpicker').val();

   var json = {};
   json['pid'] =  id ; 


 $.ajax({
      type: 'post',
      url: api + "v2/erp/orders/get-products" ,
      data : json,
      success: function(data)
      {
       // data = $.parseJSON(data);
        
          tr.find('.price').val(data.price);  
         
         
      },
     error: function( ) 
      {
        alert(  'Something went wrong, please try again'); 
      }

    }); 
 
}); 
  $('tbody').delegate('.qty,.price,.amount','keyup', function()
{
 
   var tr = $(this).parent().parent();
   var qty = tr.find('.qty').val();
   var price = tr.find('.price').val();
   var amount = (qty * price);
   tr.find('.amount').val(amount);
   total();
 
});

  function total()
 {
    var total = 0;
    $('.amount').each(function(i,e){
      var amount = $(this).val()-0;
      total += amount;
    })
    $('.total').html(total);
   // $('.total').html(total.formatMoney(2,',','.'));

 
 };

 /*Number.prototype.formatMoney = function(number, decPlaces, decSep, thouSep) {
  var n = this,
decPlaces = isNaN(decPlaces = Math.abs(decPlaces)) ? 2 : decPlaces,
decSep = typeof decSep === "undefined" ? "." : decSep;
thouSep = typeof thouSep === "undefined" ? "," : thouSep;
var sign = n < 0 ? "-" : "";
var i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(decPlaces)));
var j = (j = i.length) > 3 ? j % 3 : 0;

return sign +
  (j ? i.substr(0, j) + thouSep : "") +
  i.substr(j).replace(/(\decSep{3})(?=\decSep)/g, "$1" + thouSep) +
  (decPlaces ? decSep + Math.abs(number - i).toFixed(decPlaces).slice(2) : "");
}; */


 $('.addRow').on('click', function()
{

 addRow();
 
});

 function addRow()
 {

   var tr = '<tr>'+
      '<td>'+
      '<select class="form-control selectpicker" data-live-search="true" name="productName[]" id="productName">'+

                    '<option value="0" selected="true" disabled="true">Select Product</option>'+

                         '@foreach ($data["products"] as $itemP)'+
                         
                          '<option value="{{$itemP->id}}">{{$itemP->pr_name}}</option>'+
                         
                    '@endforeach'+
                '</select>'+
      '</td>'+

      '<td><input type="number" id="quantity" name="quantity[]" class="form-control form-control-alternative qty" placeholder="Only number" value=""></td>'+

      '<td><input type="number" id="price" name="unitPrice[]" class="form-control form-control-alternative price" placeholder="Only number" value=""></td>'+

      '<td><input type="text" id="amount" name="amount[]" class="form-control form-control-alternative amount" placeholder="Only number" value=""></td>'+

      '<td class="text-center"><a href="#" class="btn btn-danger btn-sm remove"><i class="glyphicon glyphicon-remove"></i></a></td>'+

    '</tr>';

    $('tbody').append(tr);
    $('.selectpicker').selectpicker('refresh');
 };


  $(document).live('click', ".remove", function()
{

  $(this).parent().parent().remove();
  total();
 
});




</script>
@endsection 

