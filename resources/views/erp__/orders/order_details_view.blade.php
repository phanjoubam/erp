@extends('layouts.erp_theme_02')
@section('content')

<style>
.img-hover img {
    -webkit-transition: all .3s ease; /* Safari and Chrome */
    -moz-transition: all .3s ease; /* Firefox */
    -o-transition: all .3s ease; /* IE 9 */
    -ms-transition: all .3s ease; /* Opera */
    transition: all .3s ease;
    position:relative;
}
.img-hover img:hover {
    -webkit-backface-visibility: hidden;
    backface-visibility: hidden;
    -webkit-transform:translateZ(0) scale(2.40); /* Safari and Chrome */
    -moz-transform:scale(2.40); /* Firefox */
    -ms-transform:scale(2.40); /* IE 9 */
    -o-transform:translatZ(0) scale(2.20); /* Opera */
    transform:translatZ(0) scale(2.20);
}
.grayscale {
  -webkit-filter: brightness(1.10) grayscale(100%) contrast(90%);
  -moz-filter: brightness(1.10) grayscale(100%) contrast(90%);
  filter: brightness(1.10) grayscale(100%); 
}
</style>

 
  <?php 

    $order_info = $data['order_info'];
    $order_items  = $data['order_items'];
    $customer = $data['customer'];
    $agent = $data['agent_info'];  


    $order_no =$order_info->id ;


  ?> 


 


 <div class="row">


   <div class="col-md-8"> 
 
     <div class="board">
    <div class="panel panel-primary">

      <div class="card-title"> 
       <div class="title">Order Details</div>
     </div>

     <hr/>

                <h5 class="card-category">Order # {{ $order_info->id  }} 
                @switch($order_info->book_status)

                      @case("new")
                        <span class='badge badge-primary'>New</span>
                      @break

                      @case("confirmed")
                        <span class='badge badge-info'>Confirmed</span>
                      @break
 

                      @case("order_packed")
                        <span class='badge badge-info'>Order Packed</span>
                      @break

                      @case("package_picked_up")
                        <span class='badge badge-info'>Package Picked Up</span>
                      @break

                       @case("pickup_did_not_come")
                        <span class='badge badge-warning'>Pickup Didn't Come</span>
                      @break

                       @case("in_route")
                        <span class='badge badge-success'>In Route</span>
                      @break

                       @case("completed")
                        <span class='badge badge-success'>Completed</span>
                      @break

                      @case("delivered")
                        <span class='badge badge-success'>Delivered</span>
                      @break

                       @case("delivery_scheduled")
                        <span class='badge badge-success'>Delivery Scheduled</span>
                      @break 
                      @endswitch

                      <span> Order Date: {{ date('d-m-Y H:i:s', strtotime( $order_info->book_date)) }}</span> 


                    </h5> 
          
           <div> 
                 <table class="table table-bordered">
                    <thead class=" text-primary">
                      <th></th>
                        <th>Item</th>
                        <th>Quantity</th>
                        <th>Unit Price</th> 
                        <th class='text-right'>Total</th> 
                    </thead>
                    <tbody>
                      <?php $i = 0 ;

                      $sub_total = 0;
                      ?>
               

               @foreach ( $order_items   as $item)

               <?php $i++;

               $sub_total += $item->price * $item->qty;
                ?>

                    <tr >
                      <td class="text-center">
          <?php 

            if($item->image=="")
             { 
                $image_url =  URL::to('/') . "/public/assets/image/no-image.jpg"; 
             } 
          else
             {
                 $image_url =  URL::to('/') .  $item->image;
             }     
         ?>
      <div class="img-hover text-center">
        <img src='{{ $image_url }}' alt="..." height="80px" width="100px">
      </div>
     </td>
                      <td>
                      <span> {{$item->pr_name}}  </span>
                    </td>
                      <td>
                      {{$item->qty}} {{$item->unit}}  
                    </td>  
 

                    <td>
                       {{ $item->price }}  
                    </td>  
                     <td class='text-right'>
                       {{  number_format( $item->price * $item->qty, 2, ".", "" ) }}  
                    </td> 


                    </tr>
                   @endforeach
                      
                 
                    


                    </tbody>

     <tfoot>
     <tr>
       <td style="border: none"></td>
       <td style="border: none"></td>
       <td style="border: none"></td>
       <td><b>Total Amount :</b></td>
       <td class="text-right">{{ number_format( $sub_total , 2, ".", "" )   }}</td>

     </tr>
   </tfoot>




                  </table>

 @if( $order_info->book_status=="confirmed" )
    <a href="#" class="btn btn-primary btn-sm btnModal001"  data-id="{{$order_no}}">Order Pack</a>
   @elseif( $order_info->book_status=="order_packed" )
    <a href="#" class="btn btn-primary btn-sm btnModal002"  data-ono="{{$order_no}}">Pickup Didn't Come</a>
    @elseif( $order_info->book_status=="delivered"||$order_info->book_status=="completed" )
    <a href="#" class="btn btn-info btn-sm">Completed</a>
    
@else 
     <a class="btn btn-danger btn-sm">Order Cancelled</a>
@endif

                       <a href="{{  URL::to('/erp/order/sales/pdf' )}}/{{$order_no}}" class='btn btn-secondary btn-sm' name='btn_update'>Print Layout</a>


                </div>
              </div>
            </div>




 </div>


 <div class="col-md-4">  


     <div class="board">
    <div class="panel panel-primary">

      <div class="card-title"> 
       
        <div class="title">Customer Details</div>

        <hr/>

       
     </div> 
 
                 <p> <h4 class="title">{{$customer->fullname}} </h4> <br/>
                  Delivery Address:<br/>
                  {{$order_info->address}}<br/>
                  {{$order_info->landmark}}<br/>
                  {{$order_info->city}}<br/>
                  {{$order_info->state}} - {{ $order_info->pin_code}} <br/>
                  <i class='fa fa-phone'></i> {{$customer->phone}}</p> 
          
            </div>

            @if(isset($agent))


      <div class="panel panel-primary">

      <div class="card-title"> 
       
        <div class="title">Delivery Agent</div>

        <hr/>

       
     </div> 
      
                 <p>
                  <h4 class="panel-title">{{$agent->deliveryAgentName}}</h4><br/>
                  <i class='fa fa-phone'></i> {{$agent->deliveryAgentPhone}}</p> 
 
            </div>

            @endif


          </div>  

       </div> 
     </div>


<!--Order Pack Modal -->
<div class="modal fade" id="dialogModal001" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">

   <form method='post' action="{{URL::to('/erp/order/update-status') }}">
  {{  csrf_field() }}
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Confirmation</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>
          This is final step for confirmation. No rollback possible.
          Are you sure about this operation? 
      </p>
      </div>
      <div class="modal-footer">
        <input type='hidden' name='order_no' id='order_no'/>
        <input type='hidden' name='orderStatus' id='orderStatus' value="order_packed" />
        <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
        <button type="submit" name="btnSave" value="Save" class="btn btn-primary">Yes</button>
      </div>
    </div>
  </div>
</form>

</div>

<!--Pickup didn't come Modal-->
<div class="modal fade" id="dialogModal002" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <form method='post' action="{{URL::to('/erp/order/update-status') }}">
  {{  csrf_field() }}

  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Confirmation</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div>
        <p class="text-center">
          This is final step for confirmation. No rollback possible.
          Are you sure about this operation? 
      </p>
      </div>
      <div class="modal-footer">
        <input type='hidden' name='order_no' id='ono'/>
        <input type='hidden' name='orderStatus' id='orderStatus' value="pickup_did_not_come" />
        <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
        <button type="submit" name="btnSave" value="Save" class="btn btn-primary">Yes</button>
      </div>
    </div>
  </div>
</form>

</div> 

@endsection


@section("script")

<script>

  $(document).on("click", ".btnModal001", function()
{
    var id = $(this).attr("data-id");
     $("#order_no").val( id ); 

    $("#dialogModal001").modal("show"); 

})

$(document).on("click", ".btnModal002", function()
{
    var id = $(this).attr("data-ono");
     $("#ono").val( id ); 

    $("#dialogModal002").modal("show"); 

})

  
</script>

@endsection



   