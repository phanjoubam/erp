<script src="{{ URL::to('/public/assets/admin') }}/js/core/jquery.min.js"></script> 
<script src="{{ URL::to('/public/assets/admin') }}/js/core/bootstrap.min.js"></script> 
<script src="{{ URL::to('/public/theme/sbdash') }}/assets/vendors/js/vendor.bundle.base.js"></script> 
<script src="{{ URL::to('/public/assets/admin') }}/js/core/popper.min.js"></script>
<script src="{{ URL::to('/public/theme/sbdash') }}/assets/js/shared/graph.js"></script>  
 

<script src="{{ URL::to('/public/theme/sbdash') }}/assets/js/shared/jquery.cookie.js" type="text/javascript"></script> 

<script src="{{ URL::to('/public/theme/sbdash') }}/assets/js/shared/off-canvas.js"></script> 

<script 
src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC9KrtxONZFci3FxO0SPZJSejUXHpvgmHI&callback=initMap&libraries=&v=weekly" 
defer></script> 
<script src="{{ URL::to('/public/assets/vendor') }}/jexcel/jexcel.js"></script> 
<script src="{{ URL::to('/public/assets/vendor') }}/jsuites/dist/jsuites.js"></script> 
<script src="{{ URL::to('/public/assets') }}/vendor/pn/js/pignose.calendar.full.min.js" type="text/javascript"></script>  
<script src="{{ URL::to('/public/assets') }}/admin/js/core/acore.js"></script>
 


 
<script>

	$.toastDefaults = {
	  position: 'bottom-right', 
	  dismissible: true, 
	  stackable: true, 
	  pauseDelayOnHover: true, 
	  style: {
	    toast: '',
	    info: '',
	    success: '',
	    warning: '',
	    error: '',
	  }
  
	};


  $(document).ready(  function()
  { 
  	setInterval(checkNewOrders, 10000);
  });

 

	function checkNewOrders()
	{

		if (typeof $.cookie('lid') === 'undefined'){
		 	var lid  = 0;
		} else {
		   var lid = $.cookie("lid");
		} 
	 
	  
	    var json = {};
	    json['lastScanId'] =  lid;

	    $.ajax({
	      type: 'post',
	      url: api + "v3/web/orders/scan-new-orders" ,
	      data:   json ,
	      success: function(data)
	      {
	        data = $.parseJSON(data);
	        $.cookie('lid',  data.lastId ); 
	        if( data.lastId > lid )
	        {
	        	$.toast({
		  		position:'bottom-right', 
				title: 'bookTou Notification', 
				content: 'There is a new order coming in.',
				type: 'info',
				delay: 120000,
				dismissible: true,
				  img: {
				    src: '<?php echo URL::to("/public/assets/image/notification.png"); ?>',
				    class: 'rounded',
				    title: 'BTN',
				    alt: 'BTN'
				  }
			});	

	        }
	      }
	    });
	}

</script>

 
 