<nav class="navbar navbar-expand-lg navbar-light bg-light sub-nav">
 
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
  <ul class="nav">
	<li class="nav-item">
		<a class="nav-link active" href="{{ URL::to('/recent-products') }}">Recent Products</a>
	</li>
	<li class="nav-item">
		<a class="nav-link" href="{{ URL::to('/all-products') }}">All Products</a>
	</li>
	<li class="nav-item">
		<a class="nav-link" href="{{ URL::to('/low-stock-products') }}">Low Stocks</a>
	</li> 
	</ul>
   
  </div>
</nav>



	
 