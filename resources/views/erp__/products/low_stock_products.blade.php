@extends('layouts.erp_theme_02')
@section('content')
  
<div class="row">
     <div class="col-md-12"> 
      <div class="card"  >
        <div class="card-body card-zeropad">  
              <div class="grid-table"> 
                <div id="spreadsheet"></div>
              </div> 
   </div> 
 </div>
</div>
</div>


<div class="modal confirmmodel" tabindex="-1" role="dialog"> 
  <form method='post' action="{{URL::to('/erp/product-categories/products-delete') }}">
  {{  csrf_field() }}
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Confirm Product Deletion</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body"> 
        <p>You are about to delete this product. Are you sure about this operation?</p> 
      </div> 
      <div class="modal-footer"> 
           <input type="hidden" id='pr_id' name='pr_id'   >    
         <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
        <button type="submit" name="btnDelete" value="delete" class="btn btn-primary">Yes</button>
      </div>
    </div>
  </div>
</form>
</div>

 

@endsection


@section("script")

<script>

 <?php 

    $rows =  array(); 
    $rows_string ="";
    foreach ($data['products'] as $item)
    {
       $columns = array();
       if($item->photos=="")
       { 
          $image_url =  URL::to('/') . "/public/assets/image/no-image.jpg"; 
       } 
       else
       {
          $image_url =  URL::to('/') .  $item->photos;
       }

      $columns[] =  addcslashes($item->pr_code, "'") ; 
      $columns[] =  addcslashes($item->pr_name, "'") ;
      $columns[] = addcslashes($item->description, "'") ; 
      $columns[] =  $item->stock_inhand;
      $columns[] =  $item->max_order;
      $columns[] =  $item->min_required;
      $columns[] = strtoupper( addcslashes($item->unit_name, "'")  ) ;  
      $columns[] =  $item->unit_price;  
      $columns[] =  $item->discount; 
      $columns[] =  $item->packaging; 
 
      $columns[] =  $item->category; 
 
      $rows[] =  "['" . implode( "','" , $columns) . "']" ;

 


  }

  $categoryNames[] = array();  
      foreach($data['allCategories'] as $cat)
      {
        $category_names[] = "'". $cat->category_name .  "'" ;
      }
      
 

?>
var data = [  <?php echo implode(",", $rows) ; ?>  ];
 
 
 var changed = function(instance, cell, col, row, value) 
 {
    var key = extable.getValue("A" + ( parseInt(row) + 1) ) ;  
 
    if(key === null)
    {
      return;
    }

    var json = {}; 
    json['key'] = key; 
    var cellName = jexcel.getColumnNameFromId([col, row ]);  
 

   if(  col == 1)
    {
          json['col'] = "prname"  ;
          json['value'] =  value ;
    }
    else if( col == 2)
    {  
          json['col'] = "prdesc"  ;
          json['value'] =  value ;
    }
    else if(  col == 3)
    {   
          json['col'] = "stock"  ;
          json['value'] =  value ;
     }
    else if( col == 4)
    {  
          json['col'] = "maxorder"  ;
          json['value'] =  value ;
    }
     else if( col == 5)
    {  
          json['col'] = "minstock"  ;
          json['value'] =  value ;
    }
    else if( col == 6)
    {  
          json['col'] = "untname"  ;
          json['value'] =  value ;
    }
    else if( col == 7)
    {  
          json['col'] = "untprice"  ;
          json['value'] =  value ; 
    } 
  
  else if( col == 8)
    {  
          json['col'] = "discount"  ;
          json['value'] =  value ; 
    } 
    else if( col == 9)
    {  
          json['col'] = "packcharge"  ;
          json['value'] =  value ; 
    } 
    else if( col == 10)
    {  
          json['col'] = "category"  ;
          json['value'] =  value ; 
    } 

 
    var url = api + "v2/erp/products/update-product-info" ;   
    apicall( json,url);  


 
}

var extable = jexcel(document.getElementById('spreadsheet'), {
    data:data,
    columnSorting:false,
     contextMenu:function() { return false; },
    colHeaders:  ['Code', 'Product Name', 'Description', 'Current Stock', 'Max Order Qty', 'Minimum Stock', 'Unit Name', 'Unit Price',  
    'Discount', 'Packaging Cost', 'Category',  'Select' ],
    colWidths: [ 100, 250, 400, 100, 100, 100, 100 ], 
  

    columns: [
        { type: 'text', readOnly:true  },
        { type: 'text'  },
        { type: 'text'   },
        { type: 'text' },
        { type: 'text'  }, { type: 'text'  },
        { type: 'dropdown' , source:[ 'PACK' , 'SACHET' , 'BAG' , 'BOTTLE', 'KG', 'LITER', 'METER', 'BUNDLE', 'CAN', 'GM' ,'TRAY', 'OTHER']  },
        { type: 'text'  }, 

        { type: 'text'  }, 
        { type: 'text'  },  
        { type: 'dropdown' , source:[  <?php echo implode(",", $category_names) ; ?>   ]  },

        
        
        { type: 'checkbox'  }, 
     ],  
     onchange: changed,

});


function apicall(json, url ){  

    $.ajax({
      type: 'post',
      url: url ,
      data: json,
      success: function(data)
      {
        data = $.parseJSON(data);
        console.log(data);
      },
      error: function( ) {
        alert(  'Something went wrong, please try again')
      }
    });

  }


 


/*
 $(document).on('click', ".btn_del", function(){

  var id = $(this).attr("data-id"); 

  $("#pr_id").val(  id  );   
  $(".confirmmodel").modal("show");
 
});

$('#edit_table').Tabledit({
    url: '/erp/business/product-update',
    editButton: false,
    deleteButton: false,
    hideIdentifier: true,
    columns: {
        identifier: [0, '$item->id'],
        editable: [[0,'$image_url'],[1, '$item->pr_name'], [2, '$item->description'],
         [3, '$item->stock_inhand'], [4, '$item->max_order'], [5, '$item->unit_name'], 
         [6, '$item->unit_price']]
    }

  
});

*/


</script>
@endsection  


     



 
  
