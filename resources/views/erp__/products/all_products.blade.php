@extends('layouts.erp_theme_02')
@section('content')
 
<div class="row">
     <div class="col-md-12"> 
      <div class="card"  >
        <div class="card-body card-zeropad">  
              <div class="grid-table"> 
                <div id="spreadsheet"></div>
              </div> 
              <div class='p-4'>
<div class='action_bar'>
   <button data-target="0" class='btn btn-primary btn-sm disabled btnview' >View Product</button> 
   <button data-target="0" class='btn btn-primary btn-sm disabled btneditimage' >Edit Image</button>
</div>

<hr/>

{{ $data['products']->appends(request()->input())->links() }}  
</div> 
   </div> 
 </div>
</div>
</div>


<div class="modal confirmmodel" tabindex="-1" role="dialog"> 
  <form method='post' action="{{URL::to('/erp/product-categories/products-delete') }}">
  {{  csrf_field() }}
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Confirm Product Deletion</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body"> 
        <p>You are about to delete this product. Are you sure about this operation?</p> 
      </div> 
      <div class="modal-footer"> 
           <input type="hidden" id='pr_id' name='pr_id'   >    
         <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
        <button type="submit" name="btnDelete" value="delete" class="btn btn-primary">Yes</button>
      </div>
    </div>
  </div>
</form>
</div>

 

@endsection


@section("script")

<script>

 <?php 

    $cdn = config('app.app_cdn');

    $rows =  array(); 
    $rows_string ="";
    foreach ($data['products'] as $item)
    {
       $columns = array();
       if($item->photos=="")
       { 
          $image_url =  $cdn  . "assets/image/no-image.jpg"; 
       } 
       else
       {
          $image_url =  $cdn . $item->photos;
       }
 

      $columns[] = "'". addcslashes($item->pr_code, "'")  ."'"; 
      $columns[] =  "'".addcslashes($item->pr_name, "'") ."'"; 
      $columns[] ="'". addcslashes($item->description, "'") ."'"; 
      $columns[] =  $item->stock_inhand;
      $columns[] =  $item->max_order;
      $columns[] =  $item->min_required;
      $columns[] ="'". strtoupper( addcslashes($item->unit_name, "'")  )."'";  
      $columns[] =  $item->unit_price;  
      $columns[] =  $item->discount; 
      $columns[] =  $item->packaging;  
      $columns[] = "'". addcslashes($item->category, "'")."'";    

      $rows[] =  "[" . implode( "," , $columns) . "]" ;
       
       

  }

  $categoryNames[] = array();  
      foreach($data['allCategories'] as $cat)
      {
        $category_names[] = "'". $cat->category_name .  "'" ;
      }
   

?>
var data = [  <?php echo implode(",", $rows) ; ?>  ]; 
 
 var changed = function(instance, cell, col, row, value) 
 {
    var key = extable.getValue("A" + ( parseInt(row) + 1) ) ;  
 
    if(key === null)
    {
      return;
    }

    var json = {}; 
    json['key'] = key; 
    var cellName = jexcel.getColumnNameFromId([col, row ]);  
 

   if(  col == 1)
    {
          json['col'] = "prname"  ;
          json['value'] =  value ;
    }
    else if( col == 2)
    {  
          json['col'] = "prdesc"  ;
          json['value'] =  value ;
    }
    else if(  col == 3)
    {   
          json['col'] = "stock"  ;
          json['value'] =  value ;
     }
    else if( col == 4)
    {  
          json['col'] = "maxorder"  ;
          json['value'] =  value ;
    }
     else if( col == 5)
    {  
          json['col'] = "minstock"  ;
          json['value'] =  value ;
    }
    else if( col == 6)
    {  
          json['col'] = "untname"  ;
          json['value'] =  value ;
    }
    else if( col == 7)
    {  
          json['col'] = "untprice"  ;
          json['value'] =  value ; 
    } 
  
  else if( col == 8)
    {  
          json['col'] = "discount"  ;
          json['value'] =  value ; 
    } 
    else if( col == 9)
    {  
          json['col'] = "packcharge"  ;
          json['value'] =  value ; 
    } 
    else if( col == 10)
    {  
          json['col'] = "category"  ;
          json['value'] =  value ; 
    } 
  
 
    var url = api + "v2/erp/products/update-product-info" ;   
    apicall( json,url);   
 
}


var selectionActivated = function(instance,  x1, y1, x2, y2, origin) { 

var cellName1 = jexcel.getColumnNameFromId([x1, y1]);
var cellName2 = jexcel.getColumnNameFromId([x2, y2]);

var cellName1 = jexcel.getColumnNameFromId([x1, y1]);
var cellName2 = jexcel.getColumnNameFromId([x2, y2]);

 

if(  y1 >= -1  )
{
  $(".btnview").removeClass("disabled"); 
  $(".btnview").attr("data-row",  y1);
  $(".btnview").attr("data-col", x1 );


  $(".btneditimage").removeClass("disabled"); 
  $(".btneditimage").attr("data-row",  y1);
  $(".btneditimage").attr("data-col", x1 );



}
else 
{
  $(".btnview").addClass("disabled");
  $(".btnview").attr("data-row",  -9);
  $(".btnview").attr("data-col", -9);

  $(".btneditimage").addClass("disabled");
  $(".btneditimage").attr("data-row", -9);
  $(".btneditimage").attr("data-col", -9 ); 

}
 
 

}



var extable = jexcel(document.getElementById('spreadsheet'), {
    data:data,
    columnSorting:false,
     contextMenu:function() { return false; },
    colHeaders:  ['Code', 'Product Name', 'Description', 'Current Stock', 'Max Order Qty', 'Minimum Stock', 'Unit Name', 'Unit Price',  
    'Discount', 'Packaging Cost', 'Category'  ],
    colWidths: [ 100, 250, 400, 100, 100, 100, 100 ], 
  

    columns: [
        { type: 'text', readOnly:true  },
        { type: 'text'  },
        { type: 'text'   },
        { type: 'text' },
        { type: 'text'  }, { type: 'text'  },
        { type: 'dropdown' , source:[ 'PACK' , 'SACHET' , 'BAG' , 'BOTTLE', 'KG', 'LITER', 'METER', 'BUNDLE', 'CAN', 'GM' ,'TRAY', 'OTHER']  },
        { type: 'text'  }, 

        { type: 'text'  }, 
        { type: 'text'  },  
        { type: 'dropdown' , source:[  <?php echo implode(",", $category_names) ; ?>   ]  },  
     ],  
     onchange: changed,
     onselection: selectionActivated

});


function apicall(json, url ){  

    $.ajax({
      type: 'post',
      url: url ,
      data: json,
      success: function(data)
      {
        data = $.parseJSON(data);
        console.log(data);
      },
      error: function( ) {
        alert(  'Something went wrong, please try again')
      }
    });

  } 


  $(document).on('click', ".btnview", function(){

    window.location.href =  erp + "products/view-details/";

  });
 
  $(document).on('click', ".btneditimage", function(){

    var r = $(this).attr("data-row");  
    if(r == -9) return;

    var key = extable.getValue("A" + ( parseInt(r) + 1) ) ;   
 
    if(key === null)
    {
      return;
    } 


    window.location.href =  erp + "product/edit-images?key=" + key;

});


/*
 $(document).on('click', ".btn_del", function(){

  var id = $(this).attr("data-id"); 

  $("#pr_id").val(  id  );   
  $(".confirmmodel").modal("show");
 
});

$('#edit_table').Tabledit({
    url: '/erp/business/product-update',
    editButton: false,
    deleteButton: false,
    hideIdentifier: true,
    columns: {
        identifier: [0, '$item->id'],
        editable: [[0,'$image_url'],[1, '$item->pr_name'], [2, '$item->description'],
         [3, '$item->stock_inhand'], [4, '$item->max_order'], [5, '$item->unit_name'], 
         [6, '$item->unit_price']]
    }

  
});

*/


</script>
@endsection  


     



 
  
