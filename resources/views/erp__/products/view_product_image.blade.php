@extends('layouts.erp_theme_02')
@section('content')

<?php
  $product = $data['product'];
?>


 
   <div class="row">
     <div class="col-md-12"> 
        <div class="col-md-12"> 
            @if(session('err_msg')) 
              <div class="alert alert-info">
                  {{ session('err_msg') }}
              </div>
            @endif 
         </div>
</div>

      <div class="col-md-6">  
          <div class="card card-default">
              <div class="card-header"> 
                <h4 class="card-title">Product Details</h4> 
              </div>
              <div class="card-body "> 
                <div class="row">  
                  <div class="col-lg-12">
                     <strong>{{$product->pr_name}} </strong>
                  </div> 
             
                  <div class="col-lg-12">
                    <strong>{{$product->description}}</strong>
                  </div>

              </div>    
              <hr/> 
              <div class="card-body ">
              <form action="{{ action('Erp\ErpToolsController@uploadProductImage') }}" method="post" enctype="multipart/form-data">
                  {{ csrf_field() }} 
                   
                    <div class="form-group"> 
                      <input type='file' name='photos[]' multiple placeholder='Select Product Image' />
                      <small>Upload product images</small>
                    </div>
                    
                    <div class="form-group">  
                        <input type="hidden" name='key' value="{{  $data['key']}}"   /> 
                        <button type="submit" name='btn_save' class="btn btn-primary btn-sm">Save</button>
                    </div>

              </form>
            </div> 
             </div> 
          </div>
</div>

<div class='col-md-6'> 
  <div class="card card-default">  
      <div class="card-header"> 
          <h4 class="card-title">Product Images</h4> 
      </div>
  <div class="card-body"> 

  <?php  
      
      $first_photo  = URL::to("/public/assets/image/no-image.jpg");
      $slides = "";
      $files =   $data['product']->photos ; 
 

      if(count($files) > 0 )
      {
          
          foreach($files as $file )
          {
            echo   '<img width="240px" src="' . $file  .  '" class="img-thumbnail"  alt="...">'; 
             
          } 
      }
     
           
      ?> 

      
      </div>  
    </div>  
  </div>
</div>



</div>



          </div> 
      <hr/> 
       
 
 </div>
          </div>
        </div>  
   </div>
 
 



 
 </div>

@endsection  

@section("script")
         <!--Upload Image/File script-->
                <script type="text/javascript">
                  function readURL(input) {
                    if (input.files && input.files[0]) {
                      var reader = new FileReader();
            
                    reader.onload = function (e) {
                        $('#input-icon-tag').attr('src', e.target.result);
                    }
                     reader.readAsDataURL(input.files[0]);
                   }
                 }
                 $("#input-icon").change(function(){
                  readURL(this);
                });
              

 

$(document).on("click", ".btn_edit", function()
{
  var id = $(this).attr("data-id"); 
  var name = $(this).attr("data-name"); 
  var url = $(this).attr("data-url"); 
var desc = $(this).attr("data-desc"); 


    $("#category_name").val(name);
    $("#catimage").attr("src",  url); 
    $("#category_description").val(desc);


})





$(document).on("click", ".btn_del_category", function()
{
    var id = $(this).attr("data-id");
     $("#hidcid").val( id ); 

    $("#del_category").modal("show"); 

})


 </script>
             

@endsection
