<!DOCTYPE html>
<html lang="en">
  <head> 
    
		@include('templates.head')
	 </head>
	 <body>
 
   	<div class="container-scroller">  
    	@include('templates.nav_bar')  

    	<div class="container-fluid page-body-wrapper"> 

    	@include('templates.side_bar')   
      <div class="main-panel">
          <div class="content-wrapper">

          	@include('templates.header_bar')  

          @yield('content') 
      	</div>

      	@include('templates.footer') 

  </div>
    </div>
 </div>


 	@include('templates.footer-scripts') 
 	@yield('script') 



 </body> 
</html> 