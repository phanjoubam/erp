<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">  
<head>
    @include('template-01.head')
</head>

<body class="" style='background: #2f3e8f;'> 
   <div id="container"> 
   
    @yield('content')  
 
 </div> 

   @include('template-01.footer-scripts') 
   
   @yield('script') 
 </body> 
</html> 

