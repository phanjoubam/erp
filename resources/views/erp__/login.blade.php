@extends('layouts.hollow')

@section('content')
<div class="container">
    <div class="row " style='margin-top: 220px'>
        <div class="col-md-4 offset-md-4">
            <div class="card card-default">
                <div class="card-header text-center">
<img src="{{ URL::to('/public') }}/assets/image/logo.png"  width='70' />

                </div>

                <div class="card-body"> 
                 @if (session('err_msg')) <div class="col-12">
                   <div class="alert alert-info">
                    {{ session('err_msg') }}
                    </div>
                     </div>
                 @endif

                    <form action="{{ action('Auth\LoginController@erpLogin') }}" method="post">
                            {{ csrf_field() }}
                            <div class="row">
                                
                                <div class="col-12 mb-3">
                                    <label for="phone">Phone Number: <span>*</span></label>
                                    <input type="text" class="form-control" id="phone" name='phone' value="" placeholder='Specify phone number'>
                                </div>
                               
                               
                                <div class="col-12 mb-4">
                                    <label for="email_address">Password: <span>*</span></label>
                                    <input type="password" class="form-control" id="email_address" value="" name='password' placeholder='Specify password'>
                                </div>
                                <div class="col-12 mb-4">
                                
                                <button type="submit" value='login' name='login' class="btn btn-primary">Login</button>
                                <br/>  
 </div>

                            </div>
                        </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


 