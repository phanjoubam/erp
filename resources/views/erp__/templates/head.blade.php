<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="icon" type="image/x-icon" href="{{ URL::to('/')  }}/favicon.ico"> 
<title>mERP - bookTou Cloud ERP</title>
<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' /> 
<link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' /> 

<link rel="stylesheet" href="{{ URL::to('/public/theme/sbdash') }}/assets/vendors/iconfonts/mdi/css/materialdesignicons.min.css">
    <link rel="stylesheet" href="{{ URL::to('/public/theme/sbdash') }}/assets/vendors/iconfonts/ionicons/dist/css/ionicons.css">
    <link rel="stylesheet" href="{{ URL::to('/public/theme/sbdash') }}/assets/vendors/iconfonts/flag-icon-css/css/flag-icon.min.css">
    <link rel="stylesheet" href="{{ URL::to('/public/theme/sbdash') }}/assets/vendors/css/vendor.bundle.base.css">
    <link rel="stylesheet" href="{{ URL::to('/public/theme/sbdash') }}/assets/vendors/css/vendor.bundle.addons.css"> 
    <link rel="stylesheet" href="{{ URL::to('/public/theme/sbdash') }}/assets/css/shared/style.css"> 
    <link rel="stylesheet" href="{{ URL::to('/public/theme/sbdash') }}/assets/css/demo_1/style.css">


 


<link href="{{ URL::to('/public/assets/vendor') }}/jexcel/jexcel.css" rel="stylesheet">
<link rel="stylesheet" href="{{ URL::to('/public/assets/vendor') }}/jsuites/dist/jsuites.css" type="text/css" /> 
<link href="{{ URL::to('/public/assets/admin') }}/atheme/css/style.css" rel="stylesheet" /> 

<style type="text/css">
      /* Set the size of the div element that contains the map */
      #agentmap {
        height: 400px;
        /* The height is 400 pixels */
        width: 100%;
        /* The width is the width of the web page */
      }

      #map_wrapper {
    height: 600px;
}

#map_canvas {
    width: 100%;
    height: 100%;
}
 

</style>