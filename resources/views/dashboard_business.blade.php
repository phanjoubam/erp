@extends('layouts.erp_theme_02')
@section('content')

<?php 
  
  $total_orders = count( $all_orders);

  $completed = $active =0;
  foreach($all_orders as $item)
  {
    switch($item->book_status)
    {
      case  "completed":
        $completed++;  
        break;
      case  "new":
      case  "confirmed":
      case  "in_queue":
      case  "order_packed":
      case  "pickup_did_not_come":
      case  "in_route":
      case  "package_picked_up":
      case  "delivery_scheduled":
      case  "package_picked_up":
          $active++;
          break; 

    }

  }


?>

 <div class="row">
              <div class="col-md-12 grid-margin">
                <div class="card">
                  <div class="card-body">
                    <div class="row">
                      <div class="col-lg-3 col-md-6">
                        <div class="d-flex">
                          <div class="wrapper">
                            <h3 class="mb-0 font-weight-semibold">{{ $total_orders }}</h3>
                            <h5 class="mb-0 font-weight-medium text-primary">Total Orders</h5>
                            <p class="mb-0 text-muted"></p>
                          </div> 
                          <div class="wrapper my-auto ml-auto ml-lg-4">
                            <canvas height="50" width="100" id="stats-line-graph-1"></canvas>
                          </div>
                        </div>
                      </div>
                      <div class="col-lg-3 col-md-6 mt-md-0 mt-4">
                        <div class="d-flex">
                          <div class="wrapper">
                            <h3 class="mb-0 font-weight-semibold">{{ $completed }}</h3>
                            <h5 class="mb-0 font-weight-medium text-primary">Completed Orders</h5>
                            <p class="mb-0 text-muted"></p>
                          </div>
                          <div class="wrapper my-auto ml-auto ml-lg-4">
                            <canvas height="50" width="100" id="stats-line-graph-2"></canvas>
                          </div>
                        </div>
                      </div> 
 
              
                      <div class="col-lg-3 col-md-6 mt-md-0 mt-4">
                        <div class="d-flex">
                          <div class="wrapper">
                            <h3 class="mb-0 font-weight-semibold">{{ $active }}</h3>
                            <h5 class="mb-0 font-weight-medium text-primary">Active Orders</h5>
                            <p class="mb-0 text-muted"></p>
                          </div>
                          <div class="wrapper my-auto ml-auto ml-lg-4">
                            <canvas height="50" width="100" id="stats-line-graph-3"></canvas>
                          </div>
                        </div>
                      </div>
                       <div class="col-lg-3 col-md-6 mt-md-0 mt-4">
                        <div class="d-flex">
                          <div class="wrapper">
                            <h3 class="mb-0 font-weight-semibold">2000.00 &#x20b9;</h3>
                            <h5 class="mb-0 font-weight-medium text-primary">Today's Sales</h5>
                            <p class="mb-0 text-muted"></p>
                          </div>
                          <div class="wrapper my-auto ml-auto ml-lg-4">
                            <canvas height="50" width="100" id="stats-line-graph-3"></canvas>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div> 


   <div class="row">    
     <div class="col-md-12"> 
                    <div class="card "> 
                        <div class="card-body card-zeropad">
                            <div class="table-responsive">
                               @if( count($all_orders ) > 0 ) 

                                <table class="table table-striped">
                                    <thead>
                                        <tr> 
                                            <th>Order #</th>
                                            <th>Customer</th>
                                             <th>Address</th>
                                            <th>Payment</th> 
                                            <th>Total Cost</th>
                                            <th>Status</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($all_orders as $item)
                                        <tr> 
                                          <td class="text-left">
                                            <a href="{{ URL::to('/order/view-details') }}/{{  $item->id  }}" target='_blank'>
                                              {{$item->id }}
                                            </a> 
                                          </td> 
                                          <td class="text-left">{{ $item->customer_name }}</td>
                                          <td class="text-left">{{ $item->address }}</td>
                                          <td class="text-left"><span class='badge badge-primary'>{{ $item->pay_mode  }}</span></td> 
                                          <td>{{ $item->total_cost }}</td>

                                          <td>
                                          @switch($item->book_status)

                      @case("new")
                        <span class='badge badge-primary'>New</span>
                      @break

                      @case("confirmed")
                        <span class='badge badge-info'>Confirmed</span>
                      @break
 

                      @case("order_packed")
                        <span class='badge badge-info'>Order Packed</span>
                      @break

                      @case("package_picked_up")
                        <span class='badge badge-info'>Package Picked Up</span>
                      @break

                       @case("pickup_did_not_come")
                        <span class='badge badge-warning'>Pickup Didn't Come</span>
                      @break

                       @case("in_route")
                        <span class='badge badge-success'>In Route</span>
                      @break

                       @case("completed")
                        <span class='badge badge-success'>Completed</span>
                      @break

                      @case("delivered")
                        <span class='badge badge-success'>Delivered</span>
                      @break

                       @case("delivery_scheduled")
                        <span class='badge badge-success'>Delivery Scheduled</span>
                      @break 

                        @case("canceled")
                        <span class='badge badge-danger'>Cancelled</span>
                      @break 

                      @endswitch

                    </td>


                                        </tr>
                                         
                                        @endforeach
                                    </tbody>
                                </table>

                                 @else 
                                <p class='alert alert-info'>No New signup today.</p>
                                @endif 

                            </div>
                        </div>
                    </div> 
                 
                   
                </div>
 
 </div>
 
@endsection