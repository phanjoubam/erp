@extends('layouts.erp_theme_03')

@section('style') 

<style>

td.td-text{  width: 2500px; word-wrap:break-word;}

</style>

@endsection


@section('content') 

<?php 
    $all_services = $data['services'];  
    $cdn_url =   $_ENV['APP_CDN'] ;  
    $cdn_path =   $_ENV['APP_CDN_PATH'] ; 

?>

 <div class="row">
   <div class="col-md-12"> 
 
     <div class="card card-default">
       
       <div class="card-body"> 
  @if (session('err_msg'))
  <div class="col-md-12">
    <div class="alert alert-info">
  {{ session('err_msg') }}
  </div>
  </div>
@endif
  <div class="table-responsive">
  <table class="table"> 
    <thead class=" text-primary">
      <tr >
        <th >Image</th>
        <th  width="200px;">Service Details</th> 
        <th >Min Charge</th>
        <th >Max Charge</th> 
        <th >Service Type</th>
        <th >Category</th> 
        <th class='text-center' >Action</th>
      </tr>
    </thead> 
    <tbody> 
    @foreach ($data['services'] as $itemP)  
     <tr >
     <td>

      <?php  
      $first_photo  = $cdn_url . "/assets/image/no-image.jpg" ;

      $all_photos = array(); 
      $files =  explode(",",  $itemP->photos ); 

      if(count($files) > 0 )
      {
          $files=array_filter($files);
          $folder_path = $cdn_path .  "/assets/image/store/bin_" .   $itemP->bin .  "/";
          foreach($files as $file )
          {
            if(file_exists(   $folder_path .   $file ))
            {
              $first_photo = $cdn_url .   "/assets/image/store/bin_" .   $itemP->bin .  "/"  .    $file  ;
              break;
            } 
          } 
      } 
           
      ?> 

      <img src="{{ $first_photo   }}" alt="..." height="50px" width="50px">

    </td>
     <td class='td-text' width="200px;"><p class=""><span class='badge badge-primary' >{{ $itemP->id }}</span><br/>
      {{ $itemP->srv_name }}</p></td> 
      <td>{{$itemP->pricing}}</td>
      <td>{{$itemP->max_price}}</td>
      <td>{{$itemP->service_type}}</td> 
      <td>{{$itemP->gender}}</td>
   <td>{{$itemP->duration_hr}} hr {{$itemP->duration_min}} min</td>

 
      <td class="text-center">

                       
                     <div class="dropdown">
                        <a class="btn btn-primary btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          <i class="fa fa-cog "></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow"> 

                        <a class="dropdown-item"  href="{{ URL::to('/services/manage-services/edit') }}/{{ $itemP->id  }}">Edit</a> 
                          <a class="dropdown-item"  href="{{ URL::to('/admin/customer-care/business/view-product-photo') }}/{{ $itemP->bin }}/{{ $itemP->id  }}">Upload Images</a> 
                       
                        </div>
                      </div>
                  
        </td>
 
    </tr>
    @endforeach
   </tbody>

  </table>

  {{ $data['services']->links() }}

 </div>

  </div>

   
</div>    
  
   </div> 
 </div>
 

@endsection

@section("script")

<script>
 

</script>

 @endsection
