@extends('layouts.erp_theme_03')
@section('content')

<?php  
    $cdn_url =   $_ENV['APP_CDN'] ;  
    $cdn_path =   $_ENV['APP_CDN_PATH'] ; 
 
?>
<div class="row mt-5">
     <div class="col-md-6 offset-md-3"> 
        <div class="card panel-default">
           <div class="card-body"> 
                <div class="row">
                    <div class="col-lg-12"> 
                            <h5>New Service Information</h5> 
                </div>  
                </div> 
                <hr/>  
              @if (session('err_msg'))
              <div class="col-12">
               <div class="alert alert-info">
                {{ session('err_msg') }}
                </div>
                 </div>
              @endif

              <form action="{{ action('Erp\ErpToolsController@saveServiceDetails') }}" method='post'>
              @csrf
              <div class="form-row"> 

<div class="form-group col-md-4">
  <label for="category">Select Service Category:</label>
  <select  name ="category" required class="form-control" id="category">
    @foreach($service_categories as $item) 
        <option value="{{ $item->category }}"   >{{ $item->category }}</option>
    @endforeach
  </select>
</div>
<div class="form-group col-md-8">
  <label for="service">Service Name</label>
  <input type="text" class="form-control" id="service" name="service" value="">
</div>  
</div> 
 

<div class="form-row">
    <div class="form-group col-md-4">
      <label for="servicetime">Service Duration (Hr:min)</label>

      <div class="input-group  "> 
            <select  name ="hour" required  class="form-control" id="hour">
                <?php for($i=0; $i < 24; $i++) 
                {
                    ?>
                    
                <option   value="{{ $i }}">{{ $i }}</option>

                <?php  }  ?>
             </select> 
        <div class="input-group-append">
            <span class="input-group-text">:</span>
            <select  name ="minute" required  class="form-control" id="minute">
                <?php for($i=0; $i < 60; $i=$i+5) 
                {
                    ?>
                    
                <option   value="{{ $i }}">{{ $i }}</option>

                <?php  }  ?>
             </select>  
        </div>
        </div>

 
    </div>
    <div class="form-group col-md-4">
      <label for="minprice">Service Minimum Price</label>
      <input type="number" min="0.00" step="0.00" class="form-control" id="minprice" name='minprice' >
    </div>

    <div class="form-group col-md-4">
      <label for="maxprice">Service Maximum Price</label>
      <input type="number" min="0.00" step="0.10" class="form-control" id="maxprice" name='maxprice' >
    </div>
  </div>

  <div class="form-row">
  <div class="form-group col-md-4">
  <label for="gender">Select Service Gender:</label>
  <select  name ="gender" required class="form-control" id="gender"> 
        <option value="male"  >Only for Male</option> 
        <option value="female"  >Only for Female</option> 
        <option value="any"  >Any Gender</option> 

  </select>
</div>

<div class="form-group col-md-4">
  <label for="servicetype">Select Service Type:</label>
  <select  name ="servicetype" required class="form-control" id="servicetype"> 
        <option value="home" >Home Visit Service</option> 
        <option value="salon" >Salon Service</option>  
        <option value="studio" >Studio Service</option> 
  </select>
</div> 
</div>  


 
  <div class="form-group">
    <label for="details">Service Details</label>
    <textarea type="text" class="form-control" id="details" name="details" rows='5' placeholder="Description about the service"></textarea>
  </div>
   
  <button type="submit" value='save' class="btn btn-primary">Save Staff Profile</button>
</form>

 
              
            </div>
          </div>
        </div>



      

 
        </div> 

   @endsection  


   @section("script")
 

 
@endsection 

