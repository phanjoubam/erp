@extends('layouts.erp_theme_03')
@section('content')

<?php  
    $cdn_url =   $_ENV['APP_CDN'] ;  
    $cdn_path =   $_ENV['APP_CDN_PATH'] ; 

 

?>
<div class="row mt-5">
     <div class="col-md-4 offset-md-4"> 
        <div class="card panel-default">
           <div class="card-body"> 
                <div class="row">
                    <div class="col-lg-12"> 
                            <h5>Service Information</h5> 
                </div>  
                </div> 
                <hr/>  
              @if (session('err_msg'))
              <div class="col-12">
               <div class="alert alert-info">
                {{ session('err_msg') }}
                </div>
                 </div>
              @endif

              <form action="{{ action('Erp\ErpToolsController@updateServiceDetails') }}" method='post'>
              @csrf
              <div class="form-row"> 

<div class="form-group col-md-4">
  <label for="category">Select Service Category:</label>
  <select  name ="category" required class="form-control" id="category">
    @foreach($service_categories as $item) 
        <option value="{{ $item->category }}"  @if($service_product->service_category ==  $item->category )  checked @endif    >{{ $item->category }}</option>
    @endforeach
  </select>
</div>
<div class="form-group col-md-8">
  <label for="service">Service Name</label>
  <input type="text" class="form-control" id="service" name="service" value="{{ $service_product->srv_name }}">
</div>  
</div> 
 

<div class="form-row">
    <div class="form-group col-md-4">
      <label for="servicetime">Service Duration (Hr:min)</label>

      <div class="input-group  "> 
            <select  name ="hour" required  class="form-control" id="hour">
                <?php for($i=0; $i < 24; $i++) 
                {
                    ?>
                    
                <option @if($service_product->duration_hr ==  $i )  checked @endif  value="{{ $i }}">{{ $i }}</option>

                <?php  }  ?>
             </select> 
        <div class="input-group-append">
            <span class="input-group-text">:</span>
            <select  name ="minute" required  class="form-control" id="minute">
                <?php for($i=0; $i < 60; $i=$i+5) 
                {
                    ?>
                    
                <option @if($service_product->duration_min ==  $i )  checked @endif  value="{{ $i }}">{{ $i }}</option>

                <?php  }  ?>
             </select>  
        </div>
        </div>

 
    </div>
    <div class="form-group col-md-4">
      <label for="minprice">Service Minimum Price</label>
      <input type="number" min="0.00" step="0.00" class="form-control" id="minprice" name='minprice' value="{{ $service_product->pricing}}">
    </div>

    <div class="form-group col-md-4">
      <label for="maxprice">Service Maximum Price</label>
      <input type="number" min="0.00" step="0.10" class="form-control" id="maxprice" name='maxprice' value="{{ $service_product->max_price }}">
    </div>
  </div>

  <div class="form-row">
  <div class="form-group col-md-4">
  <label for="gender">Select Service Gender:</label>
  <select  name ="gender" required class="form-control" id="gender"> 
        <option value="male" @if($service_product->gender ==  "male" )  checked @endif  >Only for Male</option> 
        <option value="female" @if($service_product->gender ==  "female" )  checked @endif >Only for Female</option> 
        <option value="any" @if($service_product->gender ==  "all"  )  checked @endif  >Any Gender</option> 

  </select>
</div>

<div class="form-group col-md-4">
  <label for="servicetype">Select Service Type:</label>
  <select  name ="servicetype" required class="form-control" id="servicetype"> 
        <option value="home" @if($service_product->service_type ==  "home" )  checked @endif>Home Visit Service</option> 
        <option value="salon" @if($service_product->service_type ==  "salon" )  checked @endif>Salon Service</option>  
        <option value="studio" @if($service_product->service_type ==  "studio" )  checked @endif>Studio Service</option> 
  </select>
</div> 
</div>  


 
  <div class="form-group">
    <label for="details">Service Details</label>
    <textarea type="text" class="form-control" id="details" name="details" rows='5' placeholder="Description about the service">{{ $service_product->srv_details }}</textarea>
  </div>
  
  <input type="hidden" name="serviceid" value="{{ $serviceid }}">
  <button type="submit" value='save' class="btn btn-primary">Save Staff Profile</button>
</form>


                 
              
            </div>
          </div>
        </div>



      

 
        </div> 

   @endsection  


   @section("script")
 

 
@endsection 

