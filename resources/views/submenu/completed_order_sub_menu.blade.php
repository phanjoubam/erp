<div class="card">
	 <div class="card-body">
	<div class='row'>
		<div class='col-md-4'> 
	<ul class="nav nav-pills">
        <li class="nav-item"><a  class="nav-link"  href="{{ URL::to('/orders/view-active-orders') }}">Active Orders</a></li>  
        <li class="nav-item"><a class="nav-link"  href="{{URL::to('/orders/view-completed-orders')}}">Completed Orders</a></li> 
    </ul> 

	</div> 

<div class='col-md-8'>
 <form class="form-inline offset-1" method="post" action="{{action('Erp\ErpToolsController@viewCompletedOrders')}}">
            {{ csrf_field() }}
            
            <label class="my-1 mr-2" for="inlineFormCustomSelectPref">Date:</label> 
            <input type="text" class='form-control form-control-sm custom-select my-1 mr-sm-2 calendar' 
            value="" name='filter_date' />

             <label class="my-1 mr-2" for="inlineFormCustomSelectPref">Status:</label> 
             <select   class='form-control form-control-sm custom-select my-1 mr-sm-2 '   name='filter_status' >
                  <option value='-1'>All</option> 
                    
                    <option value='confirmed'>Confirmed</option> 
                    <option value='delivery_scheduled'>Wating Agent Pickup</option>  
                    <option value='order_packed'>Packed and Ready for delivery</option>    
                    <option value='in_route'>In Route</option>   
                </select> 
                <button type="submit" class="btn btn-primary my-1" value='search' name='btn_search'>Search</button>
            </form>
	</div> 
	</div> </div> 
</div> 