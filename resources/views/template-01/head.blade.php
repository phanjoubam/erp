<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="icon" type="image/x-icon" href="{{ URL::to('/')  }}/favicon.ico"> 
<title>Dashboard - bookTou</title>
<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' /> 
<link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' /> 


<link rel="stylesheet" href="{{ URL::to('/public/theme/sbdash') }}/assets/vendors/iconfonts/mdi/css/materialdesignicons.min.css">
<link rel="stylesheet" href="{{ URL::to('/public/theme/sbdash') }}/assets/vendors/iconfonts/ionicons/dist/css/ionicons.css">
<link rel="stylesheet" href="{{ URL::to('/public/theme/sbdash') }}/assets/vendors/iconfonts/flag-icon-css/css/flag-icon.min.css">
<link rel="stylesheet" href="{{ URL::to('/public/theme/sbdash') }}/assets/vendors/css/vendor.bundle.base.css">
<link rel="stylesheet" href="{{ URL::to('/public/theme/sbdash') }}/assets/vendors/css/vendor.bundle.addons.css"> 

<link rel="stylesheet" href="{{ URL::to('/public/assets/vendor/toast/toast.min.css') }}" />

<link rel="stylesheet" href="{{ URL::to('/public/theme/sbdash') }}/assets/css/shared/style.css"> 
<link rel="stylesheet" href="{{ URL::to('/public/theme/sbdash') }}/assets/css/demo_1/style.css"> 
<link rel="stylesheet" href="{{ URL::to('/public/theme/sbdash') }}/assets/css/jquery-ui.css"> 

 

<link href="{{ URL::to('/public/assets/vendor/fa5/css') }}/all.css" rel="stylesheet">

<link href="{{ URL::to('/public/assets') }}/vendor/pn/css/pignose.calendar.min.css" rel="stylesheet" /> 
<link href="{{ URL::to('/public/assets/vendor') }}/jexcel/jexcel.css" rel="stylesheet">
<link rel="stylesheet" href="{{ URL::to('/public/assets/vendor') }}/jsuites/dist/jsuites.css" type="text/css" /> 
<link href="{{ URL::to('/public/assets/core') }}/css/style.css" rel="stylesheet" /> 
 

 <style type="text/css">
 	
.ui-menu img{
  width:70px;
  height:70px;
  padding: 5px;
}
.ui-menu li span{
  font-size:2em;
  padding:0 0 5px 5px;
  margin:0 0 5px 0 !important;
  white-space:nowrap;
}
 </style>