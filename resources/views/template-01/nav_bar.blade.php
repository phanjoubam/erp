<nav class="navbar default-layout col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
        <div class="text-center navbar-brand-wrapper d-flex align-items-top justify-content-center">
          <a class="navbar-brand brand-logo" href="{{ URL::to('/') }}">
            <img   src="{{ URL::to('/public/assets/image/logo_white.png') }}" /> </a>
          <a class="navbar-brand brand-logo-mini" href="{{ URL::to('/') }}">
            <img   src="{{ URL::to('/public/assets/image/logo_white.png') }}" /></a>
        </div>


        <div class="navbar-menu-wrapper d-flex align-items-center">
          
          <form method="get" action="{{action('Erp\ErpToolsController@searchByDataEntered')}}">
          <div class="form-row">
            <div class="col-10">
         
          <input type="text" class="form-control" name="searchProduct" 
          placeholder="Search Any keyword"> 
          
        

          </div>

          <div class="col-2">
             <button type="submit" class="btn btn-primary btn-sm" value='search' name='btnSearch'>Search
          </button>
          </div>

         </div>
          </form>  
         

          @if( isset(  $data['page_title'] ) &&  $data['page_title'] != "")
            <h1 class="page-title white">{{ $data['page_title'] }}</h1>
          @endif

          <ul class="navbar-nav ml-auto">

            <div class="custom-control custom-switch">
                      <input type="checkbox" class="custom-control-input" id="customSwitch1" name="theme"
                        @if($theme->theme_name=="new-theme") 
                        unchecked
                        @elseif($theme->theme_name=="old-theme") 
                        checked
                        @endif
                      >
                      <label class="custom-control-label" for="customSwitch1">Change Theme</label>
                    </div>

          <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" id="notificationDropdown" href="#" data-toggle="dropdown">
                Orders &amp; Bookings
              </a>

              <div class="dropdown-menu dropdown-menu-right navbar-dropdown preview-list pb-0" aria-labelledby="notificationDropdown">
              
              <a class="dropdown-item preview-item py-3"  href="{{  URL::to('/admin/customer-care/pick-and-drop-orders/view-all') }}" >
                  <div class="preview-thumbnail">
                    <i class="mdi fa fa-shopping-cart m-auto text-primary"></i>
                  </div>
                  <div class="preview-item-content">
                    <h6 class="preview-subject font-weight-normal text-dark mb-1">Pick & Drop Order</h6> 
                  </div>
              </a>

              <a class="dropdown-item preview-item py-3"  href="{{  URL::to('/orders/view-active-orders') }}" >
                  <div class="preview-thumbnail">
                    <i class="mdi fa fa-shopping-cart m-auto text-primary"></i>
                  </div>
                  <div class="preview-item-content">
                    <h6 class="preview-subject font-weight-normal text-dark mb-1">View Orders</h6> 
                  </div>
                </a>


                <a class="dropdown-item preview-item py-3" href="{{ URL::to('/services/view-bookings') }}">
                  <div class="preview-thumbnail">
                    <i class="mdi mdi-airballoon m-auto text-primary"></i>
                  </div>
                  <div class="preview-item-content">
                    <h6 class="preview-subject font-weight-normal text-dark mb-1">View Bookings</h6> 
                  </div>
                </a>

                <a class="dropdown-item preview-item py-3" href="{{ URL::to('/services/add-new-booking') }}">
                  <div class="preview-thumbnail">
                    <i class="mdi  fa-calendar m-auto text-primary"></i>
                  </div>
                  <div class="preview-item-content">
                    <h6 class="preview-subject font-weight-normal text-dark mb-1">New Booking</h6> 
                  </div>
                </a>

                <a class="dropdown-item preview-item py-3" 
                href="{{ URL::to('product/enter-sales-details') }}">
                  <div class="preview-thumbnail">
                    <i class="mdi  fa-shopping-cart m-auto text-primary"></i>
                  </div>
                  <div class="preview-item-content">
                    <h6 class="preview-subject font-weight-normal text-dark mb-1">New Sales</h6> 
                  </div>
                </a>


              </div>
            </li>


            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" id="messageDropdown" href="#" data-toggle="dropdown" aria-expanded="false">
                 Products
              </a>
              <div class="dropdown-menu dropdown-menu-right navbar-dropdown preview-list pb-0" aria-labelledby="messageDropdown">

             
              <a class="dropdown-item preview-item py-3"   href="{{ URL::to('/recent-products') }}" >
                  <div class="preview-thumbnail">
                    <i class="fa fa-check-circle  m-auto text-primary"></i>
                  </div>
                  <div class="preview-item-content">
                    <h6 class="preview-subject font-weight-normal text-dark mb-1">Recently Added Products</h6> 
                  </div>
                </a>


               
              <a class="dropdown-item preview-item py-3"  href="{{ URL::to('/all-products') }}"> 
                  <div class="preview-thumbnail">
                    <i class="fa fa-cube m-auto text-primary"></i>
                  </div>
                  <div class="preview-item-content">
                    <h6 class="preview-subject font-weight-normal text-dark mb-1">All Products</h6> 
                  </div>
                </a>

                <a class="dropdown-item preview-item py-3"  href="{{ URL::to('/low-stock-products') }}"> 
                  <div class="preview-thumbnail">
                    <i class="fa fa-shopping-basket m-auto text-primary"></i>
                  </div>
                  <div class="preview-item-content">
                    <h6 class="preview-subject font-weight-normal text-dark mb-1">Low Stock Products</h6> 
                  </div>
                </a>


                <a class="dropdown-item preview-item py-3"  
                href="{{ URL::to('/admin/product/enter-save-product') }}"> 
                  <div class="preview-thumbnail">
                    <i class="fa fa-plus m-auto text-primary"></i>
                  </div>
                  <div class="preview-item-content">
                    <h6 class="preview-subject font-weight-normal text-dark mb-1">New Product</h6> 
                  </div>
                </a>

 
 
              </div>
            </li>
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" id="notificationDropdown" href="#" data-toggle="dropdown">
                Services
              </a>
              <div class="dropdown-menu dropdown-menu-right navbar-dropdown preview-list pb-0" aria-labelledby="notificationDropdown">

                <a class="dropdown-item preview-item py-3" href="{{ URL::to('/services/add-new-service') }}">
                  <div class="preview-thumbnail">
                    <i class="mdi mdi-settings m-auto text-primary"></i>
                  </div>
                  <div class="preview-item-content">
                    <h6 class="preview-subject font-weight-normal text-dark mb-1">New Service</h6> 
                  </div>
                </a>

               
                <a class="dropdown-item preview-item py-3" href="{{ URL::to('/services/manage-services') }}">
                  <div class="preview-thumbnail">
                    <i class="mdi mdi-alert m-auto text-primary"></i>
                  </div>
                  <div class="preview-item-content">
                    <h6 class="preview-subject font-weight-normal text-dark mb-1">Manage Services</h6> 
                  </div>
                </a>
               
               
              </div>
            </li>



            <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" id="notificationDropdown" href="#" data-toggle="dropdown">
                Staffs &amp; Attendance
            </a>
            <div class="dropdown-menu dropdown-menu-right navbar-dropdown preview-list pb-0" aria-labelledby="notificationDropdown">

            <a class="dropdown-item preview-item py-3" href="{{ URL::to('/services/add-new-staff') }}">
                  <div class="preview-thumbnail">
                    <i class="fa fa-user m-auto text-primary"></i>
                  </div>
                  <div class="preview-item-content">
                    <h6 class="preview-subject font-weight-normal text-dark mb-1">Add Staff</h6> 
                  </div>
            </a>

               
            <a class="dropdown-item preview-item py-3" href="{{ URL::to('/services/manage-staffs') }}">
                  <div class="preview-thumbnail">
                    <i class="fa fa-users m-auto text-primary"></i>
                  </div>
                  <div class="preview-item-content">
                    <h6 class="preview-subject font-weight-normal text-dark mb-1">Manage Staffs</h6> 
                  </div>
            </a>

                <a class="dropdown-item preview-item py-3" href="{{ URL::to('/services/staff/add-day-off') }}">
                  <div class="preview-thumbnail">
                    <i class="fa fa-calendar m-auto text-primary"></i>
                  </div>
                  <div class="preview-item-content">
                    <h6 class="preview-subject font-weight-normal text-dark mb-1">Add Staff Day Off</h6> 
                  </div>
                </a>
 
            </div>
            </li>
            <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" id="notificationDropdown" href="#" data-toggle="dropdown">
              Reports
            </a>
            <div class="dropdown-menu dropdown-menu-right navbar-dropdown preview-list pb-0" aria-labelledby="notificationDropdown">

            <a class="dropdown-item preview-item py-3" href="{{ URL::to('reports/sales-analytics')}}">
                  <div class="preview-thumbnail">
                    <i class="fa fa-user m-auto text-primary"></i>
                  </div>
                  <div class="preview-item-content">
                    <h6 class="preview-subject font-weight-normal text-dark mb-1">Sales Analytics</h6> 
                  </div>
            </a>

            <a class="dropdown-item preview-item py-3" href="{{ URL::to('reports/billing-and-clearance/daily-sales')}}">
                  <div class="preview-thumbnail">
                    <i class="fa fa-user m-auto text-primary"></i>
                  </div>
                  <div class="preview-item-content">
                    <h6 class="preview-subject font-weight-normal text-dark mb-1">Daily Sales</h6> 
                  </div>
            </a>
             
                <a class="dropdown-item preview-item py-3" href="{{ URL::to('reports/monthly-earning')}}">
                  <div class="preview-thumbnail">
                    <i class="fa fa-user m-auto text-primary"></i>
                  </div>
                  <div class="preview-item-content">
                    <h6 class="preview-subject font-weight-normal text-dark mb-1">Monthly Sales</h6> 
                  </div>
            </a>
 
          <a class="dropdown-item preview-item py-3" href="{{ URL::to('reports/sales-and-service-per-cycle')}}">
                  <div class="preview-thumbnail">
                    <i class="fa fa-user m-auto text-primary"></i>
                  </div>
                  <div class="preview-item-content">
                    <h6 class="preview-subject font-weight-normal text-dark mb-1">Weekly Sales Cycle</h6> 
                  </div>
            </a>
                
              </div>
            </li>

            <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" id="notificationDropdown" href="#" data-toggle="dropdown">
              Billing & Clearance
            </a>
            <div class="dropdown-menu dropdown-menu-right navbar-dropdown preview-list pb-0" aria-labelledby="notificationDropdown">

            <a class="dropdown-item preview-item py-3" href="{{ URL::to('clearance/show-payment-due')}}">
                  <div class="preview-thumbnail">
                    <i class="fa fa-user m-auto text-primary"></i>
                  </div>
                  <div class="preview-item-content">
                    <h6 class="preview-subject font-weight-normal text-dark mb-1">Weekly Clearance</h6> 
                  </div>
            </a>

            <a class="dropdown-item preview-item py-3" href="{{ URL::to('reports/billing-and-clearance/sales-and-clearance-report')}}">
                  <div class="preview-thumbnail">
                    <i class="fa fa-user m-auto text-primary"></i>
                  </div>
                  <div class="preview-item-content">
                    <h6 class="preview-subject font-weight-normal text-dark mb-1">Clearance Report</h6> 
                  </div>
            </a> 
            
              </div>
            </li>



            <li class="nav-item dropdown d-none d-xl-inline-block user-dropdown">
              <a class="nav-link dropdown-toggle" id="UserDropdown" href="#" data-toggle="dropdown" aria-expanded="false">
                <i class='fa fa-user'></i> </a>
              <div class="dropdown-menu dropdown-menu-right navbar-dropdown" aria-labelledby="UserDropdown">
                <div class="dropdown-header text-center">
                 
                  <p class="mb-1 mt-3 font-weight-semibold">Welcome, </p>
                  <p class="font-weight-light text-muted mb-0">User</p>
                </div>
                <a class="dropdown-item">My Profile <span class="badge badge-pill badge-danger">1</span><i class="dropdown-item-icon ti-dashboard"></i></a>
                
                <a class="dropdown-item" href='{{ URL::to("/logout") }}'>Sign Out<i class="dropdown-item-icon ti-power-off"></i></a>
              </div>
            </li>
          </ul>
          <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
            <span class="mdi mdi-menu"></span>
          </button>
        </div>
      </nav>