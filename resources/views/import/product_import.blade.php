@extends('layouts.admin_theme_01')
@section('content')
 
<div class="panel-header panel-header-sm"> 

</div> 

<div class="cardbodyy margin-top-bg">  
  

 <div class="col-md-6">
 
          
    <form action="{{ action('Erp\ErpToolsController@prepareProductImportExcel') }}" method="post" enctype="multipart/form-data">
                  {{ csrf_field() }}
                     

                    <div class="row">

                      <div class="col-md-12">
                        <label class="control-label">Select Product Excel:</label><br/>

                    <input type='file' name='file' placeholder='Select EPIC records excel file' />

                    <small class='red'>Please use .xlsx file if possible</small>

                      </div>

                      </div>

                      <div class="row">
                      <div class="col">

                      <div class="form-group">
                      <br/>
                        <button type="submit" name='btn_save' class="btn btn-primary">Save</button>
                      </div> </div>
                      </div>


                                    </form>
       

  </div> 
</div>   
 

@endsection


 