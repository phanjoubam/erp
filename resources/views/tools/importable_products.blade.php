@extends('layouts.erp_theme_03')
@section('content')

<div class="panel-header panel-header-sm">

</div>


<div class="cardbodyy margin-top-bg">


  <div class="col-md-12">

    <form method="post" action="{{  action('Erp\ErpToolsController@saveImportableProducts') }}">
               {{ csrf_field() }}

 <div class="card">
 	  <div class="card-header">
 <div class="row">
   <div class="col-md-6">
    <h3>Verify and Import Products</h3>
   </div>

  </div>

       </div>


 	<div class="card-body">


       <table class="table">
         <thead class=" text-primary">
		<tr class="text-center">
      <th></th>
		<th scope="col">Product Code/Bar Code</th>
		<th scope="col">Name</th>
		<th scope="col">Description</th>
		<th scope="col">Price</th>
    <th scope="col">Stock</th>
    <th scope="col">Action</th>
		</tr>
	</thead>

		<tbody>

			 <?php $i = 0 ?>
		@foreach ($data['products'] as $item)


		 <tr>
        <td><input name='cb_select[]' value="{{ $item->id  }}"  type='checkbox' class='cbselector' /></td>
        <td><input name='barcode[]' class='form-control form-control-sm' value='{{ $item->barcode }}' /></td>
        <td><input name='pr_name[]' class='form-control form-control-sm' value='{{ $item->pr_name }}' /></td>
        <td><input name='pr_desc[]' class='form-control form-control-sm'value='{{$item->description}}' /></td>
        <td><input name='pr_price[]' class='form-control form-control-sm'value='{{$item->unit_price}}' /></td>
        <td><input name='pr_stock[]' class='form-control form-control-sm'value='{{$item->stock}}' /></td>
        <td>
          <button   type='button' class='btn btn-primary btn-sm btnshowwidget'
          data-wgtid="1"
          data-pid="{{ $item->id  }}"
          data-bar="{{ $item->barcode }}"
          data-name="{{ $item->pr_name }}"
          data-desc="{{$item->description}}"
          data-price="{{$item->unit_price}}"
          data-stock="{{$item->stock}}" >Import</button>
        </td>
		 </tr>
     <?php
       $i++  ;
      ?>
	@endforeach


	</tbody>
		</table>
	</div>
<div class='card-footer'>
  <div class='row'>
    <div class='col-md-6'>
      {{ $data['products']->links() }}
    </div>
    <div class='col-md-6 text-right'>
      <input name='bin'  value='{{ $data["bin"] }}' type='hidden' />
      <button name='btnsave' value='delete' type='submit' class='btn btn-danger btn-sm'>Delete Selection</button>
      <button name='btnsave' value='save' type='submit' class='btn btn-primary btn-sm'>Import Selection</button>
    </div>
  </div>
</div>
</div>
	 </form>


</div>

</div>


<form method="post" action="{{  action('Erp\ErpToolsController@saveImportableProduct') }}">
  {{ csrf_field() }}
  <div class="modal" id="wgt-1" tabindex="-1">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Product Import</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          
        <div class="form-row">
          <div class="form-group col-md-6">
            <label for="inputEmail4">Product Name</label>
            <input type="text" class="form-control" id="pname" name='pname'>
          </div>
          <div class="form-group col-md-6">
            <label for="barcode">Bar Code/Product Code</label>
            <input type="text" class="form-control" id="barcode" name="barcode">
          </div>
      </div>
    
      <div class="form-group">
        <label for="desc">Description</label>
        <input type="text" class="form-control" id="desc" name="desc" placeholder="Product description">
      </div> 
 

    <div class="form-row"> 
      <div class="form-group col-md-4">
        <label for="stock">Current Stock</label>
        <input type="number" step='any' min='1' class="form-control" id="stock" name='stock'  >
      </div>
   
      <div class="form-group col-md-4">
        <label for="uniprice">Unit Price</label>
        <input type="number" step='.50' min='1' class="form-control" id="uniprice" name='uniprice'  >
      </div> 
  </div>
        <div class="modal-footer">
          <input type="hidden" id="key1" name="key" />
          <button type="submit" value='save' name="btnsave" class="btn btn-primary">Save Product</button>
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
          
        </div>
      </div>
    </div>
  </div>
</form>



@endsection


 @section("script")


 <script>


 $('#cb_select_all').change(function () {

    var checked = $(this).is(":checked");
    $(".cbselector"). prop("checked", checked);

 });



 $('.btnshowwidget').on('click', function () {
 
  $("#pname").val(  $(this).attr("data-bar") );
  $("#barcode").val(  $(this).attr("data-name") );
  $("#desc").val(  $(this).attr("data-desc") );
  $("#uniprice").val(  $(this).attr("data-price") );
  $("#stock").val(  $(this).attr("data-stock") );
  $("#key1").val(  $(this).attr("data-pid") ); 
  $("#wgt-1").modal("show"); 

});




</script>

@endsection
