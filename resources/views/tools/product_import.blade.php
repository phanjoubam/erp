@extends('layouts.erp_theme_03')
@section('content')


  <div class="row">
     <div class="col-md-6">
 <form action="{{ action('Erp\ErpToolsController@uploadProductImportExcel') }}" method="post" enctype="multipart/form-data">

  {{ csrf_field() }}


    <div class='card'>
      <div class='card-body'>

<div class="form-group row">
  <label for="fileselector" class="col-sm-4 col-form-label">Select File</label>
  <div class="col-sm-8">
<div class="input-group">
  <div class="custom-file">
    <input type="file" name="file" class="custom-file-input" id="fileselector" aria-describedby="fileselector">
    <label class="custom-file-label" for="fileselector" name='file' >Choose file</label>
  </div>
</div>
<small class='red'>Please use .xlsx file if possible</small>

 </div>
 </div>


  </div>

  <div class='card-footer'>
<button type="submit" name='btn_save' class="btn btn-primary">Upload &amp; Save</button>
  </div>
</div>

</form>



 </div>
</div>

@endsection
