<!DOCTYPE html>
<html lang="en">
  	<head>
  		@include('template-01.head')
      @yield('style')  
    </head>
 <body>
   @include('template-01.nav_bar')   
 <?php 
   if( isset( $data['sub_menu'] ) &&  $data['sub_menu'] != "")
   {
      $sub_menu ="submenu." .  $data['sub_menu'];  
      ?>
      @include(  $sub_menu ) 
      <?php 

      $content_padding = "";
   } 
   else 
   {
      $content_padding = "content-paddding";
   }
  ?>
   
      <div class="container-fluid page-content {{ $content_padding }}">   

         @yield('content') 
         

      </div>
 
   @include('template-01.footer') 
   @include('template-01.footer-scripts') 
   @yield('script') 
 

 </body> 
</html> 

 