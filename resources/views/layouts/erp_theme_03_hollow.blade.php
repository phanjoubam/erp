<?php 
  $footer_scripts = 'template-01.footer-scripts';
?> 
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>New Order - bookTou POS</title> 
    <!-- Custom fonts for this template-->
    <link href="{{ URL::to('/public/assets/bs2') }}/vendor/fontawesome-free/css/all.min.css"  rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <!-- Custom styles for this template-->
    <link href="{{ URL::to('/public/assets/bs2') }}/css/sb-admin-2.min.css" rel="stylesheet">
    <link href="{{ URL::to('/public/assets/bs2') }}/css/jquery-ui.css" rel="stylesheet"> 
    <link href="{{ URL::to('/public/assets/bs2/vendor') }}/pn/css/pignose.calendar.min.css" rel="stylesheet" />  
    <link href="{{ URL::to('/public/assets/bs2') }}/css/style.css" rel="stylesheet"> 
    <link rel="stylesheet" href="{{ URL::to('/public') }}/vendor/ocv2/assets/owl.carousel.min.css">
    <link rel="stylesheet" href="{{ URL::to('/public') }}/vendor/ocv2/assets/owl.theme.default.min.css"> 
    <link type="text/css" rel="stylesheet" href="{{ URL::to('/public/vendor/jscroll') }}/style.css">

    <link href="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.13.3/css/selectize.bootstrap4.css" rel="stylesheet" />
<link href="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.13.3/css/selectize.css" rel="stylesheet" />

     

<style>
  html {
    overflow: scroll;
    overflow-x: hidden;
}
::-webkit-scrollbar {
    width: 0;  /* Remove scrollbar space */
    background: transparent;  /* Optional: just make scrollbar invisible */
}
/* Optional: show position indicator in red */
::-webkit-scrollbar-thumb {
    background: #FF0000;
}

body
{
  background-color: #5d5e5d;
}


.input-group {
     
}

 

.bg-black{
    background-color: #343434;
    color: #fff;
}
#sticky-header
{
  position: fixed;
  top: 0;
  width: 100%;
  z-index: 10000;
}

#sticky-footer
{
  position: fixed;
  bottom: 0;
  width: 100%;
  z-index: 10000;
  padding: 10px;
}

#contentarea
{
  margin-top: 60px;
  max-width: 1360px;
  margin-left: auto;
  margin-right: auto;
}


.panelleft {
    cursor: grab;
    width: 900px; 
    position: fixed;
    background-color: #5d5e5d;
    color: #fff !important;
    padding:  30px 10px 20px 10px;
    height: 100%;
}

.panelright {
    width: auto;
    margin-left: 900px; 
    padding: 20px 20px 120px 20px; 
    overflow-x: hidden;
}

#cartsummary
{
  background-color: #ffff; 
  border-radius: 5px; 
  width: 430px; 
}

#cartsummaryheader
{
    padding: 20px  10px; 
    border-bottom: 1px  solid #efefef;
} 

#cartsummarybody
{
    padding: 20px 10px 50px  10px;  
    margin-left: auto;
    margin-right: auto;
    width: 100%;
    height: 390px;
    overflow-y: scroll;
 }

#cartsummaryfooter
{
    padding: 20px  10px; 
    border-top: 1px  solid #efefef;
}
 
 

 .clearer  
 {
  clear:both;
    margin-top: 20px;
  }
 
a.btn.btn-xs {
    font-size: 12px;
    padding: 5px;
}

.owl-nav
 {
  text-align: center;
 }
 .owl-navigator {
    font-size: 35px;
    background-color: #343434;
    color: #efefef; 
    width: 50px;
    height: 50px;
    padding-top: 8px;
    display: inline-block;
}
.owl-prev .owl-navigator
{
  border-top-left-radius: 10px;
  border-bottom-left-radius: 10px;
}

.owl-next .owl-navigator
{
  border-top-right-radius: 10px;
  border-bottom-right-radius: 10px;
}

.scrollbar
{
  margin-left: 30px;
  float: left;  
  overflow-y: scroll;
  margin-bottom: 25px;
}
 

.price-text-color
{
    color: #fff;
    font-weight: bold;
    font-size: 1.2em;
}
 

 

.image-container 
{
    position: relative;
    width: 160px;
    height:  200px;
    display:inline-block;
    margin: 5px;
    padding: 10px 5px !important; 
    text-align: center;
    border: 1px solid #cac9c9; 
    overflow: hidden;   border-radius: 5px;
}
.image-container .after {
     position: absolute;
    bottom: 0;
    left: 0;
    width: 100%;
    height:100%;
    display: none;
    color: #FFF;
    padding: 10px;
    cursor: default; border-radius: 5px;
}
.image-container:hover .after {
    display: block;
    padding-top:  10px;
    background: rgba(0, 0, 0, .6);
}

.info 
{
  padding:  10px;
  color:  #fff !important;
  cursor: default !important;
}

 

#cart-box-sticky {
/* position:fixed; */
top: 180px; 
padding:  10px;
background-color:  #fff;
border: 1px solid #efefef ;
}



  .jscroll 
  {
      width: 750px;
      height: 250px;
      margin: 10pt auto;
      border: 1pt solid #CCC;
  }

  .jscroll .content > div {
      padding: 10pt;
      background: #09F;
    }

    .jscroll.scrollX .content > div {
      width: 2000px;
    }

.selectize-input.not-full > input {
  width: 100% !important;
}


 

#productList{
    list-style: none;
    padding: 0px;
    width: auto;
    position: absolute;
    margin:  40px  0 0 130px; 
    overflow-y: scroll;
    height: 500px !important;
}

#productList li{
    background: #f9f9ff;
    color:  #343434 !important;
    padding: 4px; 
}
 

#productList li:hover{
    cursor: pointer;
}

input[type=text]{
    padding: 5px;
    width: 250px;
    letter-spacing: 1px;
}

</style>

@yield('style') 


</head>
 


<body  >  
          
<div class='bg-black' id='sticky-header'>
<div class="container-fluid">
  <div class="row">
    <div class="col-md-1 pt-3"> 
    <a class="sidebar-brand d-flex align-items-center justify-content-center" 
            href="{{  URL::to('/') }}">
                <div class="sidebar-brand-icon ">
                    <img class="img-fluid"  src="{{ URL::to('/public/assets/image/logo.png') }}" style="width: 40px;" />
                </div>
               
            </a>
</div>
    <div class="col-md-7 pt-3">  

      <form autocomplete="off">
      <div class="form-row align-items-center">
        <div class="col-md-7">
          <label class="sr-only" for="inlineFormInputGroup">Search</label>
          

          <div class="input-group mb-3">
            <div class="input-group-prepend">
              <div class="input-group-text">Product Name</div>
            </div>
            <input type="text"  class="form-control autocomplete"   autocomplete="off"  name='keyword' id='keyword' 
            style="border-top-right-radius: 5px;border-bottom-right-radius: 5px;">
            <ul id="productList" class='list-unstyled'></ul>
           <div class="clear"></div> 
          </div>
        </div>

         

 
      </div>
   </form>

   
    </div> 

<div class='col-md-4 text-right pt-3'>  

@php 
             $pos = 1;
            @endphp 
            @foreach($allcarts as $cart)

              @if( $pos == 1  )
                @php 
                  $color_code = "danger";
                @endphp  
              @else 
                  @if($pos == 2)
                      @php 
                        $color_code = "warning";
                      @endphp 
                  @else 
                    @if($pos ==3 )
                        @php 
                          $color_code = "info";
                        @endphp 
                    @else 
                      @if($pos == 4 )
                          @php 
                            $color_code = "success";
                          @endphp 
                      @else 
                        @php 
                          $color_code = "info";
                        @endphp  
                      @endif 
                    @endif 
                  @endif  
              @endif 
              @php 
               $pos++;
              @endphp  
                <a class="btn btn-{{ $color_code }} btn-xs" href="{{ URL::to('/pos/pos-sales-window-touch-and-go')}}?enccno={{ Crypt::encrypt( $cart->cartNo ) }}">Cart # {{ $cart->cartNo }}</a> 
            @endforeach

            <a class='btn btn-primary btn-xs'  href="{{URL::to('/pos/pos-sales-window-touch-and-go')}}">
              <i class='fa fa-plus-circle'></i>
            </a>

            

      
      </div>
 
  </div>
</div>
</div>
<div id='contentarea'>

@yield('content') 

</div> 
 
<footer id="sticky-footer" class="bg-black"> 
                    <div class="copyright text-center ">
                       <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright &copy; {{ date('Y') }}</span><br/> 
                       All right reserved. Service offered by <a href="https://booktou.in">bookTou.in</a>
               
                </div>
</footer>


@if( session()->has('_bin_') ) 
  <span id='gbin' style='display: none; height:0'>{{ Crypt::encrypt(   session()->get('_bin_') )  }}</span>
@else
  <span style='display: none;height:0'  id='gbin'>{{ Crypt::encrypt(  0 )  }}</span>
@endif

@if( session()->has('__user_id_') ) 
  <span id='guid' style='display: none; height:0'>{{ Crypt::encrypt(   session()->get('__user_id_') )  }}</span>
@else
  <span style='display: none;height:0'  id='guid'>{{ Crypt::encrypt(  0 )  }}</span>
@endif



<script src="{{ URL::to('/public/assets/bs2') }}/vendor/jquery/jquery.min.js"></script>
<script src="{{ URL::to('/public/assets/bs2') }}/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="{{ URL::to('/public/assets/bs2') }}/vendor/jquery-easing/jquery.easing.min.js"></script>
<script src="{{ URL::to('/public/assets/bs2') }}/js/sb-admin-2.min.js"></script> 
<script src="{{ URL::to('/public/assets/core') }}/js/jquery/jquery-ui.js"></script>
<script src="{{ URL::to('/public/assets/bs2') }}/vendor/pn/js/pignose.calendar.full.min.js" type="text/javascript"></script>  
<script src="{{ URL::to('/public/assets/core/js') }}/acore.js"></script> 
<script src="{{ URL::to('/public/vendor/ocv2') }}/owl.carousel.min.js"></script>
<script type="text/javascript" src="{{ URL::to('/public/vendor/jscroll') }}/plugin.js"></script> 

 <script src="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.13.3/js/standalone/selectize.js"></script>


<script>
 

 
</script> 
 

 @yield('script')
 
 
</body>
 
</html>