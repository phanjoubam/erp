   
      
      <?php
      
        $header = "template-02.head";
        $navbar = 'template-02.nav_bar';
        $topnavbar ='template-02.nav_top_bar';
        $footer='template-02.footer';
        $footer_scripts = 'template-02.footer-scripts'; 
      ?>
 

      @include($header)  

 




<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
                        
  @include($navbar)  



        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
<div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
<div id="content">

                <!-- Topbar -->
                      
  @include('template-02.nav_top_bar') 

 
               


                <!-- End of Topbar -->
  <div class="container-fluid">
    <?php 
   if( isset( $data['sub_menu'] ) &&  $data['sub_menu'] != "")
   {
      $sub_menu ="submenu." .  $data['sub_menu'];  
      ?>
      @include(  $sub_menu ) 
      <?php 

      $content_padding = "";
   } 
   else 
   {
      $content_padding = "content-paddding";
   }
  ?>
                @yield('content') 

 </div>

            </div>
            <!-- End of Main Content -->

            <!-- Footer -->
             
            @include($footer) 
          


 
            <!-- End of Footer -->

        </div>
        <!-- End of Content Wrapper -->

    </div>
    


    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>
                         
 
 


@if( session()->has('_bin_') ) 
  <span id='gbin' style='display: none; height:0'>{{ Crypt::encrypt(   session()->get('_bin_') )  }}</span>
@else
  <span style='display: none;height:0'  id='gbin'>{{ Crypt::encrypt(  0 )  }}</span>
@endif

@if( session()->has('__user_id_') ) 
  <span id='guid' style='display: none; height:0'>{{ Crypt::encrypt(   session()->get('__user_id_') )  }}</span>
@else
  <span style='display: none;height:0'  id='guid'>{{ Crypt::encrypt(  0 )  }}</span>
@endif

 @include($footer_scripts)
 @yield('script')
 
</body>


</html>