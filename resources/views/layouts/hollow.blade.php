<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">  
<head>
    @include('template-02.head')
</head>

<body class="" style='background: rgb(135,124,198); background: radial-gradient(circle, rgba(135,124,198,1) 0%, rgba(34,119,242,1) 100%);'> 
   <div id="container"> 
   
    @yield('content')  
 
 </div> 

   @include('template-01.footer-scripts') 
   
   @yield('script') 
 </body> 
</html> 

