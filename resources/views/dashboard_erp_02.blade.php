 
@extends('layouts.erp_theme_03')

@section('content')
<style type="text/css">
    .card .card-header {
        background: #fff;
    }
    .card .card-header {
        background: white;
        padding: 1.88rem 1.81rem;
        border-bottom: 1px solid #dee2e6;
    }

    </style>
    <?php 

    $total_orders = count( $all_orders);

    $completed = $active =0;
    foreach($all_orders as $item)
    {
        switch($item->book_status)
        {
          case  "completed":
          $completed++;  
          break;
          case  "new":
          case  "confirmed":
          case  "in_queue":
          case  "order_packed":
          case  "pickup_did_not_come":
          case  "in_route":
          case  "package_picked_up":
          case  "delivery_scheduled":
          case  "package_picked_up":
          $active++;
          break; 

      }

      $bk_date = strtotime($item->book_date);
      $book_date = date("d-m-Y", $bk_date);
      $serv_date = strtotime($item->service_date);
      $service_date = date("d-m-Y", $serv_date);
  } 
  ?> 
  <?php 

    $total_orders = count( $today_orders);

    $completed = $active =0;
    foreach($today_orders as $item)
    {
        switch($item->book_status)
        {
          case  "completed":
          $completed++;  
          break;
          case  "new":
          case  "confirmed":
          case  "in_queue":
          case  "order_packed":
          case  "pickup_did_not_come":
          case  "in_route":
          case  "package_picked_up":
          case  "delivery_scheduled":
          case  "package_picked_up":
          $active++;
          break; 

      }

      $bk_date = strtotime($item->book_date);
      $book_date = date("d-m-Y", $bk_date);
      $serv_date = strtotime($item->service_date);
      $service_date = date("d-m-Y", $serv_date);
  } 
  ?> 
  <div class="row">
   <div class="col-md-12">
      <div class='card'> 
        <div class='card-body'>
          <div class="row">
            <div class="col-md-11">
                <h3> Active Orders:
                  <span class="badge badge-success">
                    {{ date('Y-m-d',strtotime($today)) }}
                </span>

                @if (session('err_msg'))
                <div class="alert alert-info p-0">
                    {{session('err_msg')}}
                </div>
                @endif
            </h3>
        </div>
        <div class="col-md-1">
        <a class='btn btn-primary btn-xs right'  href="{{ URL::to('/customer-care/add-pick-and-drop-orders') }}">
              <i class='fa fa-plus-circle'></i>
        </a>
        </div>
    </div> 
</div> 
</div>
</div>
</div>
<div class="row">
    <div class="col-md-12"> 
      <div class="card card-default ">       
        <div class="card-body">
            <div class="row ">
                @if( count($all_orders ) > 0 )
                @foreach($all_orders as $item)
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div class="card">
                      <div class="container">
                        <div class="row ">                          
                          <div class="col-md-6 ">
                            Order #
                        </div>
                        <div class="col-md-6 ">
                            {{$item->id}}
                        </div>                       
                    </div><hr>
                </div>
                <div class="card-body">
                    <div class="row">                          
                      <div class="col-md-6">Customer</div><div class="col-md-6">{{ $item->customer_name }}</div>
                      <div class="col-md-6"> Customer Phone</div><div class="col-md-6">{{$item->customer_phone}}</div>
                      <div class="col-md-6"> Order Date</div><div class="col-md-6">{{$book_date}}</div>
                      <div class="col-md-6">Service Date</div><div class="col-md-6">{{$service_date}}</div>
                      <div class="col-md-6">Order Amount</div><div class="col-md-6">{{$item->total_cost}}</div>
                      <div class="col-md-6">Book Status</div>
                      <div class="col-md-6">
                        
                      <span class="badge  badge-primary ">{{ $item->book_status  }}</span>                       
                    </div>
                    <div class="col-md-6">Payment Mode</div>
                      <div class="col-md-6">
                       
                        <span class="badge  badge-success ">{{ $item->payment_type  }}</span>
                        
                    </div>
                    <div class="col-md-6">Payment Status</div>
                      <div class="col-md-6">
                        @if( $item->payment_status == "pending")
                        <span class="badge  badge-warning ">{{ $item->payment_status  }}</span>
                         @else
                        <span class="badge badge-success">{{ $item->payment_status  }}</span>
                        @endif
                    </div>

                    <div class="col-md-6 mt-1">
                        <button type="button" class="btn btn-outline-danger btn-sm"  ata-toggle="modal" data-target="#modalepg" data-key="{{$item->id}}" style="border-radius:8px;color: #e83e8c;border-color: #e83e8c;background: white;">
                        <a  href="{{ URL::to('/Erp/customer-care/order/view-details') }}/{{$item->id}}" target='_blank' style="color: #e83e8c;">View details</a>
                        </button>

                    </div>
                    <div class="col-md-6 mt-1">


                        <button  class="btn btn-outline-dark btn-sm timer" id="timer{{$item->id}}" 
                             data-key='{{ $item->id }}'
                                    data-value='{{$item->book_date}}'  value='{{$item->book_date}}' name='counter' style="border-radius:8px;color:#e83e8c;;height:33px;width: 110px;background: white;">

                                    <strong>
                                        <b class="text-danger" id="display_timer{{ $item->id }}" ></b>
                                    </strong>

                        </button>
                               
                       
                    </div>

            </div>
        </div>
    </div>
</div>
@endforeach
@else 
<p class='alert alert-info'>No New Active Orders.</p>
@endif 
</div>
</div>
</div>
</div>
</div>
<!-- not new status -->
<div class="row mt-2">
    <div class="col-md-12"> 
      <div class="card card-default ">
      <div class="card-header">
          <span class="btn btn-primary">Today Order List:</span>
      </div>       
        <div class="card-body">
            <div class="row ">
                @if( count($today_orders ) > 0 )
                @foreach($today_orders as $item)
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div class="card">
                      <div class="container">
                        <div class="row">                          
                          <div class="col-md-6">
                            Order #
                        </div>
                        <div class="col-md-6">
                            {{$item->id}}
                        </div>                       
                    </div><hr>
                </div>
                <div class="card-body">
                    <div class="row">                          
                      <div class="col-md-6">Customer</div><div class="col-md-6">{{ $item->customer_name }}</div>
                      <div class="col-md-6"> Customer Phone</div><div class="col-md-6">{{$item->customer_phone}}</div>
                      <div class="col-md-6"> Order Date</div><div class="col-md-6">{{$book_date}}</div>
                      <div class="col-md-6">Service Date</div><div class="col-md-6">{{$service_date}}</div>
                      <div class="col-md-6">Order Amount</div><div class="col-md-6">{{$item->total_cost}}</div>
                      <div class="col-md-6">Book Status</div>
                      <div class="col-md-6">
                        @if( $item->book_status == "confirmed")
                        <span class="badge  badge-success ">{{ $item->book_status  }}</span>
                        @elseif($item->book_status == "cancelled" || $item->book_status == "cancel_by_owner")
                        <span class="badge badge-danger">{{ $item->book_status  }}</span>
                        @else
                        <span class="badge badge-warning">{{ $item->book_status  }}</span>
                        @endif

                    </div>
                    <div class="col-md-6">Payment Mode</div>
                      <div class="col-md-6">
                       
                        <span class="badge  badge-success ">{{ $item->payment_type  }}</span>
                        
                    </div>
                    <div class="col-md-6">Payment Status</div>
                      <div class="col-md-6">
                        @if( $item->payment_status == "pending")
                        <span class="badge  badge-warning ">{{ $item->payment_status  }}</span>
                         @else
                        <span class="badge badge-success">{{ $item->payment_status  }}</span>
                        @endif
                    </div>

                    <div class="col-md-6 mt-1">
                        <button type="button" class="btn btn-outline-danger btn-sm"  ata-toggle="modal" data-target="#modalepg" data-key="{{$item->id}}" style="border-radius:8px;color: #e83e8c;border-color: #e83e8c;background: white;">
                        <a  href="{{ URL::to('/Erp/customer-care/order/view-details') }}/{{$item->id}}" target='_blank' style="color: #e83e8c;">View details</a>
                        </button>

                    </div>
                    <div class="col-md-6 mt-1">


                        <button  class="btn btn-outline-dark btn-sm timer" id="timer{{$item->id}}" 
                             data-key='{{ $item->id }}'
                                    data-value='{{$item->book_date}}'  value='{{$item->book_date}}' name='counter' style="border-radius:8px;color:#e83e8c;;height:33px;width: 110px;background: white;">

                                    <strong>
                                        <b class="text-danger" id="display_timer{{ $item->id }}" ></b>
                                    </strong>

                        </button>
                               
                       
                    </div>

            </div>
        </div>
    </div>
</div>
@endforeach
@else 
<p class='alert alert-info'>No New Active Orders.</p>
@endif 
</div>
</div>
</div>
</div>
</div>

@endsection
@section("script")

<script> 

    $(document).on("click", ".confirm", function()
    {

        $("#widget-m2").modal("show");
    });


    $(document).ready(function () {

        var timers = $(".timer");


        $.each(timers, function(i, v){

            var key = $(v).attr("data-key");
            var value = $(v).attr("data-value");

            var $date = new Date(value);
            var $countdown = new Date(value);
            $countdown.setMinutes($date.getMinutes()+5);
    //adding 5 minute to the time of order

    var countDownDate = new Date($countdown).getTime();
    var x = setInterval(function() {

        var now = new Date().getTime();

        var distance = countDownDate - now;
        var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        var seconds = Math.floor((distance % (1000 * 60)) / 1000);

        var counter =  minutes + "m " + seconds + "s ";
        $("#display_timer" + key).html(counter);
        if (distance < 0) {
            clearInterval(x);
            $("#display_timer" + key).html("EXPIRED");
        }
    }, 1000);
   

})

    });

</script> 
@endsection
