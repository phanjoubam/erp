@extends('layouts.erp_theme_03')
@section('content')
<?php 
    $cdn_url =    config('app.app_cdn') ;  
    $cdn_path =     config('app.app_cdn_path') ;  
 
?>

 

  <div class="row">
     <div class="col-md-12">
 
     <div class="card card-default">
           <div class="card-header">
              <div class="card-title"> 
                 <div class="row">

                   <div class="col-md-7">
                  <h2 >
                    Daily Sales Report for <span class='badge badge-primary'>{{ date('d-m-Y', strtotime($date)) }}</span>  
                  </h2> 
                </div>

<div class="col-md-2 text-right"> 
                      Change Period:
                      </div> 
                    <div class="col-md-3">  
        <form class="form-inline" method="get" action="{{ action('Erp\AccountsAndBillingController@dailySalesReport') }}"   > 
      
      <div class="form-row">
    <div class="col-md-12">
      <input  class="form-control form-control-sm calendar" name='todayDate'  /> 
        <button type="submit" class="btn btn-primary btn-sm" value='search' name='s'><i class='fa fa-search'></i></button>
    </div>

  </div>
  
      </form>
      </div>

       </div>
                </div>
            </div>
       <div class="card-body" style='overflow-x: scroll;'>    
    <table class="table" max-width='960px;'>
      <thead>
        <tr>
          <th>Order No</th>
          <th>Business</th> 
          <th>Type | Source | Status | Mode | Target</th>  
          <th class='text-right'>Sale Amount</th> 
          <th class='text-right'>Packaging</th> 
          <th class='text-right'>Comission</th>
          <th class='text-right'>Delivery Fee</th> 
          <th class='text-right'>Due On Merchant</th> 
          <th class='text-right'>Total Earning</th> 
        </tr>
      </thead>
      
      <tbody>
        @php  

          $orderCost = $packagingCost = $commission =   $deliveryFee =  $commissionPc = $dueOnMerchant = 0.00; 
          $totalOrderCost  = $totalPackagingCost = $totalDueOnMerchant  = $totalCommission = $totalDeliveryFee = 0.00;  
          $totalCashMerchant = $totalOnlineMerchant =     0.00; 
          $totalCashbookTou = $totalOnlinebookTou = 0.00;
          $totalCommCashbookTou =  $totalCommOnlinebookTou =   0.00;
          $totalDeliveryCashbookTou = $totalDeliveryOnlinebookTou =  0.00;  
          $totalCommCashMerchant = $totalDeliveryCashMerchant = 0.00;
          $totalCommOnlineMerchant= $totalDeliveryOnlineMerchant =     0.00;
          $totalPayable= 0.00;

        @endphp
          @foreach ($sales as $normal)
 
 

             @php 
              $business = null;
            @endphp
            @foreach($businesses as $bitem)

                @if( $normal->bin == $bitem->id )
                  @php 
                    $business = $bitem;
                  @endphp
                  @break
                @endif
 
          @endforeach

          @php
            $tempPackagingCost = 0.00;
          @endphp

          @if($normal->bookingStatus == "delivered")
            <tr>
              @if($normal->orderType == "normal")

                @foreach($cart_items as $cart)
                    @if($normal->id == $cart->order_no) 
                      @php 
                        $tempPackagingCost += $cart->qty * $cart->package_charge;
                      @endphp 
                    @endif
                @endforeach

                @php 
                  $normal->packingCost  = $tempPackagingCost;
                @endphp


                <td><a target='_blank' href="{{ URL::to('/admin/customer-care/order/view-details/') }}/{{$normal->id }}">{{$normal->id }}</a></td>
                <td>{{ $normal->name }}</td>
                <td class='text-center'><span class='badge badge-success'>{{ $normal->orderType  }}</span> by 
                    <span class='badge badge-{{  ($normal->source=="business" || $normal->source=="merchant" ) ? "warning" : "success" }}'>{{ $normal->source  }}</span> - 
                    <span class='badge badge-success'>{{ $normal->bookingStatus }}</span> - 
                    @if( strcasecmp($normal->paymentType , "ONLINE") == 0 || strcasecmp($normal->paymentType , "Pay online (UPI)") == 0 )
                      <span class='badge badge-info'>ONLINE</span> - 
                    @else 
                      <span class='badge badge-primary'>CASH</span> - 
                    @endif
                    <span class='badge badge-success'>bookTou</span>
                  </td> 
                  @php 
                    $dueOnMerchant =0.00;
                    $commissionPc = $business->commission ; 
                    $orderCost = $normal->orderCost;
                    $deliveryFee = $normal->deliveryCharge; 
                    $packagingCost = $normal->packingCost; 
                    $commission = ( $commissionPc * (  $orderCost - $packagingCost ) ) / 100 ;  
                    $totalOrderCost += $orderCost;
                    $totalPackagingCost += $packagingCost;   
                  @endphp

                   @if( strcasecmp($normal->paymentType , "online") == 0 || 
                        strcasecmp($normal->paymentType , "Pay online (UPI)") == 0  )  
                    
                    @php 
                      $totalOnlinebookTou +=  $orderCost; //packing cost already included 
                      $totalDeliveryOnlinebookTou +=   $deliveryFee;
                      $totalCommOnlinebookTou +=  $commission;  
                      $dueOnMerchant = 0.00;
                      $totalPayable +=  $orderCost;
                    @endphp  

                   @else 

                    @php 
                      $totalCashbookTou +=  $orderCost  ;  //packing cost already included
                      $totalDeliveryCashbookTou +=   $deliveryFee;
                      $totalCommCashbookTou +=  $commission;   
                       
                        $dueOnMerchant = 0.00; 
                      $totalPayable +=  $orderCost;
                    @endphp

                  @endif

                  <td class='text-right'>{{ number_format( $orderCost, 2 )   }}</td>
                  <td class='text-right'>{{ number_format( $packagingCost, 2 )  }}</td> 
                  <td class='text-right'>{{ number_format( $commission, 2 )  }} ( @ {{ $commissionPc }} % )</td> 
                  <td class='text-right'>{{ number_format( $deliveryFee, 2 )  }}</td> 
                  <td class='text-right'>{{ number_format( $dueOnMerchant, 2 ) }}</td> 
                  <td class='text-right'>{{ number_format( $commission + $deliveryFee, 2 )  }}</td> 


                @elseif($normal->orderType == "pnd")

                  <td><a target='_blank'  href="{{ URL::to('/admin/customer-care/pnd-order/view-details/') }}/{{$normal->id }}">{{$normal->id }}</a></td>
                  <td>{{ $normal->name }}</td>
                  <td class='text-center'><span class='badge badge-primary'>{{ $normal->orderType  }}</span> by 
                    <span class='badge badge-{{  ($normal->source=="business" || $normal->source=="merchant" ) ? "warning" : "success" }}'>
                      {{  ($normal->source=="business" || $normal->source=="merchant" ) ? "Merchant" : "Customer" }}
                    </span> - <span class='badge badge-success'>{{ $normal->bookingStatus }}</span> - 

                  @if( strcasecmp($normal->paymentType , "ONLINE") == 0)
                    <span class='badge badge-info'>ONLINE</span> - 
                  @else 
                    <span class='badge badge-primary'>CASH</span> - 
                  @endif
                   
                  @if( strcasecmp($normal->paymentTarget , "merchant") == 0)
                     <span class='badge badge-warning'>Merchant</span>
                  @else 
                      @if( strcasecmp($normal->paymentType , "ONLINE") == 0 &&  ( $normal->source == "business" || $normal->source == "merchant" )  )
                          <span class='badge badge-danger'>bookTou</span>
                       @else 
                          <span class='badge badge-success'>bookTou</span>
                       @endif 
                  @endif
                </td>  

                  @if( strcasecmp($normal->source , "merchant") != 0 && strcasecmp($normal->source , "business") != 0)

                      @php  
                        $commissionPc = ($business == null) ? 0 : $business->commission;
                        $orderCost = $normal->orderCost;
                        $deliveryFee = $normal->deliveryCharge; 
                        $packagingCost = $normal->packingCost; 
                        $commission = ( $commissionPc * (  $orderCost - $packagingCost ) ) / 100 ;  
                        $totalOrderCost += $orderCost;
                        $totalPackagingCost += $packagingCost;    
                      @endphp 
                      
                      @if( strcasecmp($normal->paymentType , "online") == 0  ) 
                          @if(   strcasecmp($normal->paymentTarget , "merchant") == 0  || 
                            strcasecmp($normal->paymentTarget , "business") == 0 ) 
                            
                            @php
                              $totalOnlineMerchant +=  $orderCost; 
                              $totalCommOnlineMerchant  +=  $commission; 
                              $totalDeliveryOnlineMerchant +=   $deliveryFee;  
                              $dueOnMerchant = $deliveryFee  + $commission;   
                            @endphp

                          @else

                              @php
                                $totalOnlinebookTou +=  $orderCost; 
                                $totalDeliveryOnlinebookTou +=   $deliveryFee;
                                $totalCommOnlinebookTou += $commission;
                                $dueOnMerchant =  0.00; 
                                $totalPayable +=  $orderCost;
                              @endphp 

                          @endif   

                      @else

                          @php 
                            $totalCashbookTou +=  $orderCost; 
                            $totalDeliveryCashbookTou +=   $deliveryFee;
                            $totalCommCashbookTou +=  $commission;  
                            $dueOnMerchant =  0.00; 
                            $totalPayable +=  $orderCost;
                          @endphp 

                      @endif 

                 
                  @else 
 
                      @php  
                        $commission = $commissionPc =   0  ;
                        $orderCost = $normal->orderCost;
                        $deliveryFee = $normal->deliveryCharge; 
                        $packagingCost = $normal->packingCost; 
                        $totalOrderCost += $orderCost;
                        $totalPackagingCost += $packagingCost;    
                      @endphp 

                      @if( strcasecmp($normal->paymentType , "online") == 0  )   
                          @if(  strcasecmp($normal->paymentTarget , "merchant") == 0  || 
                                strcasecmp($normal->paymentTarget , "business") == 0 )  
                            
                            @php 
                              $totalOnlineMerchant +=  $orderCost; 
                              $totalCommOnlineMerchant  +=  $commission; 
                              $totalDeliveryOnlineMerchant +=   $deliveryFee;  
                              $dueOnMerchant = $deliveryFee  + $commission;   
                            @endphp 

                          @else
                            
                            @php
                              $totalOnlinebookTou +=  $orderCost; 
                              $totalDeliveryOnlinebookTou +=   $deliveryFee;
                              $totalCommOnlinebookTou += $commission; 
                              $dueOnMerchant =  0.00; 
                              $totalPayable +=  $orderCost;
                            @endphp 

                          @endif 
                           
                       @else
                            @php 
                              $totalCashbookTou +=  $orderCost; 
                              $totalDeliveryCashbookTou +=   $deliveryFee;
                              $totalCommCashbookTou +=  $commission;
                              $dueOnMerchant =  0.00; 
                              $totalPayable +=  $orderCost;
                            @endphp     
                        @endif  

                  @endif 
                    
                  <td class='text-right'>{{ number_format($orderCost,2)  }}</td>
                  <td class='text-right'>{{ number_format( $packagingCost, 2 )  }}</td> 
                  <td class='text-right'>{{ number_format( $commission, 2 )  }} ( @ {{ $commissionPc }} % )</td> 
                  <td class='text-right'>{{ number_format($deliveryFee,2) }}</td> 
                  <td class='text-right'>{{ number_format($dueOnMerchant,2) }}</td> 
                   <td class='text-right'>{{ number_format( $commission + $deliveryFee, 2 )  }}</td> 

                @elseif($normal->orderType == "assist")

                  <td><a target='_blank' href="{{ URL::to('/admin/customer-care/assist-order/view-details') }}/{{$normal->id }}">{{$normal->id }}</a></td>
                  <td>{{ $normal->name }}</td>
                   <td class='text-center'><span class='badge badge-warning'>{{ $normal->orderType  }}</span> by 
                    <span class='badge badge-{{  ($normal->source=="business" || $normal->source=="merchant" ) ? "warning" : "success" }}'>{{ $normal->source  }}</span> - <span class='badge badge-success'>{{ $normal->bookingStatus }}</span> -  
                  
                  @if( strcasecmp($normal->paymentType , "ONLINE") == 0)
                  <span class='badge badge-info'>ONLINE</span> - 
                  @else
                  <span class='badge badge-primary'>CASH</span> - 
                  @endif
                  
                  
                  @if( strcasecmp($normal->paymentTarget , "merchant") == 0)
                  <span class='badge badge-danger'>Merchant</span> 
                  @else 
                  <span class='badge badge-success'>bookTou</span>
                  @endif
                  </td> 


                  @if( strcasecmp($normal->source , "merchant") != 0 && strcasecmp($normal->source , "business") != 0)

                    @php
                      $commission = $commissionPc =   0  ;
                      $orderCost = $normal->orderCost;
                      $deliveryFee = $normal->deliveryCharge; 
                      $packagingCost = $normal->packingCost;  
                      $totalOrderCost += $orderCost;
                      $totalPackagingCost += $packagingCost;    
                    @endphp

                      
                    @if( strcasecmp($normal->paymentType , "online") == 0  )  

                        @if(   strcasecmp($normal->paymentTarget , "merchant") == 0  || 
                        strcasecmp($normal->paymentTarget , "business") == 0 ) 
                          
                          @php
                            $totalOnlineMerchant +=  $orderCost; 
                            $totalCommOnlineMerchant  +=  $commission; 
                            $totalDeliveryOnlineMerchant +=   $deliveryFee; 
                            $dueOnMerchant = $deliveryFee  + $commission;  
                          @endphp

                        @else

                            @php
                              $totalOnlinebookTou +=  $orderCost; 
                              $totalDeliveryOnlinebookTou +=   $deliveryFee;
                              $totalCommOnlinebookTou += $commission;
                              $dueOnMerchant =  0.00;  
                              $totalPayable +=  $orderCost;
                            @endphp 

                        @endif   

                    @else
                          @php 
                            $totalCashbookTou +=  $orderCost; 
                            $totalDeliveryCashbookTou +=   $deliveryFee;
                            $totalCommCashbookTou +=  $commission;  
                            $dueOnMerchant =  0.00; 
                            $totalPayable +=  $orderCost;
                          @endphp 
                        
                    @endif

                     

                  @else 
                    @continue
                  @endif
 
                  <td class='text-right'>{{ number_format($orderCost,2)  }}</td>
                  <td class='text-right'>{{ number_format( $packagingCost, 2 )  }}</td> 
                  <td class='text-right'>{{ number_format( $commission, 2 )  }} ( @ {{ $commissionPc }} % )</td> 
                  <td class='text-right'>{{ number_format($deliveryFee,2) }}</td> 
                  <td class='text-right'>{{ number_format($dueOnMerchant,2) }}</td> 
                   <td class='text-right'>{{ number_format( $commission + $deliveryFee, 2 )  }}</td> 



                @endif   

              </tr>
             @php
                  $totalDueOnMerchant += $dueOnMerchant;  
                  $totalCommission += $commission  ; 
                  $totalDeliveryFee += $deliveryFee  ; 
              @endphp
           @else

            <tr> 
              
                @if($normal->orderType == "normal")
                  <td><a target='_blank' href="{{ URL::to('/admin/customer-care/order/view-details') }}/{{$normal->id }}">{{$normal->id }}</a></td>
                  <td>{{ $normal->name }}</td>
                  <td class='text-center'><span class='badge badge-success'>{{ $normal->orderType  }}</span> by 
                @elseif($normal->orderType == "pnd")
                  <td><a target='_blank'  href="{{ URL::to('/admin/customer-care/pnd-order/view-details') }}/{{$normal->id }}">{{$normal->id }}</a></td>
                  <td>{{ $normal->name }}</td>
                  <td class='text-center'><span class='badge badge-info'>{{ $normal->orderType  }}</span> by 
                @else
                  <td><a target='_blank'  href="{{ URL::to('/admin/customer-care/assist-order/view-details') }}/{{$normal->id }}">{{$normal->id }}</a></td>
                  <td>{{ $normal->name }}</td>
                  <td class='text-center'><span class='badge badge-warning'>{{ $normal->orderType  }}</span> by 
                @endif 
                <span class='badge badge-{{  ($normal->source=="business" || $normal->source=="merchant" ) ? "warning" : "success" }}'>{{ $normal->source  }}</span> - 
                <span class='badge badge-danger'>{{ $normal->bookingStatus }}</span> -              
                @if( strcasecmp($normal->paymentType , "ONLINE") == 0)
                  <span class='badge badge-info'>ONLINE</span> -  
                @else 
                  <span class='badge badge-primary'>CASH</span> - 
                @endif
              
                @if( strcasecmp($normal->paymentTarget , "merchant") == 0)
                  <span class='badge badge-danger'>Merchant</span> 
                @else 
                  <span class='badge badge-success'>bookTou</span>
                @endif
              </td> 
              <td class='text-right'>0.00</td> 
              <td class='text-right'>0.00</td> 
              <td class='text-right'>0.00</td> 
              <td class='text-right'>0.00</td> 
              <td class='text-right'>0.00</td> 
              <td class='text-right'>0.00</td>  
          </tr>

  

           @endif
  
        @endforeach
     
        <tr>
          <th>Order No</th>
          <th>Business</th> 
          <th>Type | Source | Status | Mode | Target</th>  
          <th class='text-right'>Sale Amount</th> 
          <th class='text-right'>Packaging</th> 
          <th class='text-right'>Comission</th>
          <th class='text-right'>Delivery Fee</th> 
          <th class='text-right'>Due On Merchant</th> 
          <th class='text-right'>Total Earning</th> 
        </tr>  
        <tr>
            <th colspan='3' class='text-right'>Sum Totals</th> 
            <th class='text-right'>A = {{ number_format( $totalOrderCost  ,2) }}</th>
            <th></th>
            <th class='text-right'>B = {{ number_format( $totalCommission,2) }}</th>
            <th class='text-right'>C = {{ number_format($totalDeliveryFee, 2) }}</th>
            <th class='text-right'>D = {{ number_format($totalDueOnMerchant,2) }}</th>
            <th class='text-right'>E = {{ number_format($totalCommission + $totalDeliveryFee,2) }}</th> 

        </tr> 
<tfoot>
     
</tfoot>

      </tbody> 
    </table> 

      <hr/>
<form>
   

<div class="form-group row">
    <label  class="col-sm-2 col-form-label col-form-label-sm text-right">Cash Collected:</label>
    <div class="col-sm-1">
      <input type="text" class="form-control form-control-sm is-valid" readonly 
      value="{{ number_format( $totalCashbookTou + $totalDeliveryCashbookTou  , 2, '.', '') }}"  >
    </div>

    <label  class="col-sm-2 col-form-label col-form-label-sm text-right">Online Collected:</label>
    <div class="col-sm-1">
      <input type="text" class="form-control form-control-sm is-valid" readonly 
      value="{{ number_format( $totalOnlinebookTou  + $totalDeliveryOnlinebookTou,2, '.', '') }}"  >
    </div>

    <label  class="col-sm-2 col-form-label col-form-label-sm text-right">Due towards Merchant:</label>
    <div class="col-sm-1">
      <input type="text" class="form-control form-control-sm" readonly 
      value="{{ number_format( $totalDueOnMerchant ,2, '.', '') }}"  >
    </div>


     <label  class="col-sm-2 col-form-label col-form-label-sm text-right">Total Collected:</label>
    <div class="col-sm-1">
      <input type="email" class="form-control form-control-sm is-valid" readonly 
      value="{{ number_format(  $totalCashbookTou + $totalDeliveryCashbookTou  + $totalOnlinebookTou  + $totalDeliveryOnlinebookTou  ,2, '.', '') }}"  >
    </div>
 
  </div> 
  <div class="form-group row">
    <label  class="col-sm-2 col-form-label col-form-label-sm text-right">Payable to Merchants:</label>
    <div class="col-sm-1">
      <input type="text" class="form-control form-control-sm is-valid" readonly 
      value="{{ number_format( $totalPayable  , 2, '.', '') }}"  >
    </div>

  </div>
</form>

 </div> 
  </div> 
	
	</div> 
</div>
 
 
@endsection



@section("script")

<script>
 
  

 
$(function() {
    $('.calendar').pignoseCalendar( 
    {
      format: 'DD-MM-YYYY' 
    });
});


</script>  


@endsection

