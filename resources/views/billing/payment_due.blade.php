@extends('layouts.erp_theme_03')
@section('content')
<?php 
    $cdn_url =    config('app.app_cdn') ;  
    $cdn_path =     config('app.app_cdn_path') ;  
    $cyclesize = count($cycles) - 1;
    $cyclevalue =0 ;
?>


<div class="row">
     <div class="col-md-12"> 
     <div class="card ">

       <div class="card-body" style='overflow-x: scroll;'>    

         <h4>Sales clearance for <span class='button button-info'>
        
         </span>
              @foreach($cycles as $item)
                @switch($item)
                  @case(1)
                  	<?php $cyclevalue = $item?>
                    <span class='badge badge-info badge-pill'>First Cycle</span> 
                  @break
                  @case(2)
                  	<?php $cyclevalue = $item?>
                    <span class='badge badge-primary badge-pill'>Second Cycle</span> 
                  @break
                  @case(3)
                  	<?php $cyclevalue = $item?>
                    <span class='badge badge-warning badge-pill'>Third Cycle</span> 
                  @break
                  @case(4)
                  	<?php $cyclevalue = $item?>
                    <span class='badge badge-danger badge-pill'>Fourth Cycle</span> 
                  @break
                  @case(5)
                  <?php $cyclevalue = $item?>
                    <span class='badge badge-success badge-pill'>Last Cycle</span> 
                  @break 
                @endswitch
              @endforeach 

               
            </h4>
            <hr/>
            
    <table class="table table-bordered" max-width='960px;'>
      <thead>
        <tr>
          
          <th colspan="12  ">
            <form class="form-inline" method="post" action="{{ action('Erp\ErpToolsController@showPaymentDue') }}"   >
     {{ csrf_field() }}
  <div class="form-row  "> 
 <div class="col-md-3">
  <select name ="bin" class="selectize" id="bin"  >
                      @foreach($businesses as $bitem)
                        @if($business->id == $bitem->id )
                          <option value="{{ $bitem->id  }}" selected>{{ $bitem->name }} ({{ $bitem->frno }})</option>
                        @else
                          <option value="{{ $bitem->id  }}" selected>{{ $bitem->name }} ({{ $bitem->frno }})</option>
                        @endif
                      @endforeach
                    </select>
      </div>              
    <div class="col-md-3">

   <div class="multiselect">

    <div class="dropdown">
    <button class="btn btn-info btn-sm dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
      Select Cycle
    </button>
    <div class="dropdown-menu  " aria-labelledby="dropdownMenuButton">
      <ul  class="list-group list-group-flush">

        <li class="list-group-item"><label  for="cycleone "><input type="checkbox" id="cycleone" name='cycle[]' value='1' 
          <?php if( $cycles[0] == 1 || $cycles[$cyclesize] == 1  ) echo "checked"; ?>/>1st Week</label></li>
       <li class="list-group-item"><label  for="cycletwo"><input type="checkbox" id="cycletwo" name='cycle[]'   value='2' 
        <?php if( $cycles[0] == 2 || $cycles[$cyclesize] == 2  )  echo "checked"; ?>/>2nd Week</label></li>
       <li class="list-group-item"><label  for="cyclethree"><input type="checkbox" id="cyclethree" name='cycle[]'  value='3'  
        <?php if( $cycles[0] == 3 || $cycles[$cyclesize] == 3  )  echo "checked"; ?>/>3rd Week</label></li>
       <li class="list-group-item"><label for="cyclefour"><input type="checkbox" id="cyclefour" name='cycle[]'   value='4' 
        <?php if( $cycles[0] == 4 || $cycles[$cyclesize] == 4  )  echo "checked"; ?>/>4th Week</label></li>
      <li class="list-group-item"><label for="cyclelast"><input type="checkbox" id="cyclelast"  name='cycle[]'  value='5' 
        <?php if( $cycles[0] == 5 || $cycles[$cyclesize] == 5  )  echo "checked"; ?>/>Last Week</label></li>
    </div>
  </div>


  
  </div>
  
    </div>
   <div class="col-md-6">

      <label class="sr-only" for="inlineFormInputGroup">Month</label>
       <select name='month' class="form-control form-control-sm  ">
          <option <?php if( $month  == 1 ) echo "selected"; ?> value='1'>January</option>
          <option <?php if( $month== 2 ) echo "selected"; ?>  value='2'>February</option>
          <option <?php if( $month == 3 ) echo "selected"; ?> value='3'>March</option>
          <option <?php if( $month== 4 ) echo "selected"; ?> value='4'>April</option>
          <option <?php if( $month == 5 ) echo "selected"; ?> value='5'>May</option>
          <option <?php if( $month == 6 ) echo "selected"; ?> value='6'>June</option>
          <option <?php if( $month == 7 ) echo "selected"; ?> value='7'>July</option>
          <option <?php if( $month == 8 ) echo "selected"; ?> value='8'>August</option>
          <option <?php if( $month == 9 ) echo "selected"; ?> value='9'>September</option>
          <option <?php if($month == 10 ) echo "selected"; ?> value='10'>October</option>
          <option <?php if( $month== 11 ) echo "selected"; ?> value='11'>November</option>
          <option <?php if( $month == 12 ) echo "selected"; ?> value='12'>December</option> 
        </select>
   
      <label class="sr-only" for="inlineFormInputGroup">Year</label>
        <input readonly name='year' class="form-control form-control-sm " value="{{ date('Y') }}" /> 
     
       <button type="submit" class="btn btn-primary btn-xs" value='search' name='btnSearch'><i class='fa fa-search'></i></button>
    </div>
  </div>
  </form>

          </th>
         
        </tr>
        <tr>
          <th>Order No</th> 
          <th>Date</th> 
          <th>Order Type | Source</th>
          <th class='text-center'>Status | Pay Mode | Payment Target</th>
          <th class='text-right'>Comission %</th>
          <th class='text-right'>Sale Amount</th> 
          <th class='text-right'>Packaging</th> 
          <th class='text-right'>Comission</th>
          <th class='text-right'>Delivery</th> 
          <th class='text-right'>Due on Merchant</th> 
          <th class='text-right'>Total Earning</th> 
        </tr>
      </thead>
      <form method="post" action="{{action('Erp\ErpToolsController@saveSalesClearance')}}">
      	{{csrf_field()}}
      <tbody>
        @php 
          $packagingCost = $commission = $dueOnMerchant = $deliveryFee = $orderCost = $earning = $commissionPc = 0.00;
          $totalCash = $totalOnline =  $totalCollectedAtMerchant = $totalDueOnMerchant =$totalSale = $totalFee = $totalCommission = $totalEarning = $totalDeliveryFee = $totalOnlineMerchant = $totalPackagingCost =0.00; 
        @endphp
          @foreach ($results as $normal)
           @if($normal->bookingStatus == "delivered") 

            <tr>
                
                @if($normal->orderType == "normal")
                  @php 
                    $tempPackagingCost = 0.00;
                  @endphp
                  @foreach($cart_items as $cart)
                    @if($normal->id == $cart->order_no)
                      
                      @php 
                        $tempPackagingCost += $cart->qty * $cart->package_charge;
                      @endphp

                    @endif
                  @endforeach

                  @php 
                      $normal->packingCost  = $tempPackagingCost;
                  @endphp

                  <td><input type="checkbox" name="paymentClearance[]" value="{{$normal->id }}" checked> <a target='_blank' href="{{ URL::to('/admin/customer-care/order/view-details/') }}/{{$normal->id }}">{{$normal->id }}</a></td>
                 
                 <td class='text-center'>{{ date('d-m-Y', strtotime($normal->service_date))  }}</td> 
                  <td class='text-center'><span class='badge badge-success'>{{ $normal->orderType  }}</span> by 
                    <span class='badge badge-{{  ($normal->source=="business" || $normal->source=="merchant" ) ? "warning" : "success" }}'>{{ $normal->source  }}</span>
                  </td> 
                  <td class='text-center'>  
                    <span class='badge badge-success'>{{ $normal->bookingStatus }}</span> - 
                    @if( strcasecmp($normal->paymentType , "ONLINE") == 0 || strcasecmp($normal->paymentType , "Pay online (UPI)") == 0)
                     <span class='badge badge-info'>ONLINE</span> - 
                    @else 
                     <span class='badge badge-primary'>CASH</span> - 
                    @endif 
                    <span class='badge badge-success'>bookTou</span>
                  </td>
   

                  @php
                      $dueOnMerchant =0.00;
                      $commissionPc = $business->commission ; 
                      $orderCost = $normal->orderCost;
                      $packagingCost = $normal->packingCost;
                      $deliveryFee = $normal->deliveryCharge;
                      $commission = ( $commissionPc * (  $orderCost - $packagingCost ) ) / 100 ;
                      $earning =  $deliveryFee +  $commission;  
                      $totalSale += $orderCost;
                      $totalPackagingCost += $packagingCost;
                      $totalFee += $deliveryFee;
                      $totalCommission += $commission;
                      $totalEarning += $earning;  

                  @endphp
 

                  @if( strcasecmp($normal->paymentType , "online") == 0 || strcasecmp($normal->paymentType , "Pay online (UPI)") == 0  )  
                    @php 
                      $totalOnline += $normal->orderCost  ;  //packing cost already included
                    @endphp
                  @else 
                    @php
                      $totalCash += $normal->orderCost  ;  //packing cost already included  
                    @endphp
                  @endif 


                  <td class='text-right'>{{ $commissionPc }} %</td> 
                  <td class='text-right'>{{ number_format( $orderCost, 2 )   }}</td>
                  <td class='text-right'>{{ number_format( $packagingCost, 2 )  }}</td> 
                  <td class='text-right'>{{ number_format( $commission, 2 )  }}</td> 
                  <td class='text-right'>{{ number_format( $deliveryFee, 2 )  }}</td> 
                  <td class='text-right'>{{ number_format( $dueOnMerchant, 2 ) }}</td> 
                  <td class='text-right'>{{ number_format( $earning, 2 )  }}</td> 


                @elseif($normal->orderType == "pnd")

                  <td><input type="checkbox" name="paymentClearance[]" value="{{$normal->id }}" checked> <a target='_blank'  href="{{ URL::to('/admin/customer-care/pnd-order/view-details/') }}/{{$normal->id }}">{{$normal->id }}</a></td>
                   <td class='text-center'>{{ date('d-m-Y', strtotime($normal->service_date))  }}</td>
                  <td class='text-center'><span class='badge badge-primary'>{{ $normal->orderType  }}</span> by 
                    <span class='badge badge-{{  ($normal->source=="business" || $normal->source=="merchant" ) ? "warning" : "success" }}'>
                      {{  ($normal->source=="business" || $normal->source=="merchant" ) ? "Merchant" : "Customer" }}
                    </span>
                  </td>
                  <td class='text-center'>  
                    <span class='badge badge-success'>{{ $normal->bookingStatus }}</span> - 
                    @if( strcasecmp($normal->paymentType , "ONLINE") == 0)
                     <span class='badge badge-info'>ONLINE</span> - 
                    @else 
                     <span class='badge badge-primary'>CASH</span> - 
                    @endif 
                    @if( strcasecmp($normal->paymentTarget , "merchant") == 0)
                      <span class='badge badge-danger'>Merchant</span>
                    @else 
                      <span class='badge badge-success'>bookTou</span>
                    @endif
                  </td>
 

                  @if( strcasecmp($normal->source , "merchant") != 0 && strcasecmp($normal->source , "business") != 0)
 
                    @php
                      $dueOnMerchant =0.00;       
                      $commissionPc = $business->commission  ; 
                      $orderCost = $normal->orderCost;
                      $packagingCost = $normal->packingCost;
                      $deliveryFee = $normal->deliveryCharge;
                      $commission = ( $commissionPc * (  $orderCost - $packagingCost ) ) / 100 ;
                      $earning =  $deliveryFee +  $commission;  
                      $totalSale += $orderCost;
                      $totalFee += $deliveryFee;
                      $totalCommission += $commission;
                      $totalEarning += $earning; 
                    @endphp 

                    @if( strcasecmp($normal->paymentType , "online") == 0  ) 
                        @if(   strcasecmp($normal->paymentTarget , "merchant") == 0  || strcasecmp($normal->paymentTarget , "business") == 0 ) 
                          @php 
                            $dueOnMerchant  = $normal->deliveryCharge; 
                            $totalCollectedAtMerchant += $normal->orderCost;  
                            $totalPackagingCost += $packagingCost;
                            $totalDueOnMerchant += $dueOnMerchant; 
                            $totalOnlineMerchant += $normal->orderCost + $normal->packingCost;
                          @endphp
                        @else
                            @php
                              $totalOnline += $normal->orderCost + $normal->packingCost;
                            @endphp  
                        @endif  
                   @else
                        @php 
                          $dueOnMerchant  = 0.00; 
                          $totalCash += $normal->orderCost + $normal->packingCost;   
                        @endphp     
                    @endif  

                  @else 
  
                      @php
                            $commissionPc = 0.00 ; 
                            $orderCost = $normal->orderCost;
                            $packagingCost = $normal->packingCost;
                            $deliveryFee = $normal->deliveryCharge;
                            $commission = ( $commissionPc * (  $orderCost - $packagingCost ) ) / 100 ;
                            $earning =  $deliveryFee +  $commission; 
                            $totalSale += $orderCost;
                            $totalPackagingCost += $packagingCost;
                            $totalFee += $deliveryFee;
                            $totalCommission += $commission;
                            $totalEarning += $earning;  
                      @endphp 

                    @if( strcasecmp($normal->paymentType , "online") == 0  )   
                        @if(   strcasecmp($normal->paymentTarget , "merchant") == 0  || strcasecmp($normal->paymentTarget , "business") == 0 )  
                          @php
                             $totalOnlineMerchant += $normal->orderCost;  
                             $totalCollectedAtMerchant += $normal->orderCost; 
                             $totalPackagingCost += $normal->packingCost; 
                             $totalDueOnMerchant += $normal->deliveryCharge; 
                             $dueOnMerchant  = $normal->deliveryCharge;  
                          @endphp  
                        @else
                          @php
                            $dueOnMerchant  = 0.00;
                            $totalOnline += $normal->orderCost + $normal->packingCost;
                          @endphp 
                        @endif 
                    @else
                        @php 
                          $dueOnMerchant  = 0.00;
                          $totalCash += $normal->orderCost + $normal->packingCost;   
                        @endphp     
                    @endif   

                  @endif 
                   
                  <td class='text-right'>{{ $commissionPc }} %</td> 
                  <td class='text-right'>{{ number_format($orderCost,2)  }}</td>
                  <td class='text-right'>{{ number_format( $packagingCost, 2 )  }}</td> 
                  <td class='text-right'>{{ number_format($commission,2) }}</td> 
                  <td class='text-right'>{{ number_format($deliveryFee,2) }}</td> 
                  <td class='text-right'>{{ number_format($dueOnMerchant,2) }}</td> 
                  <td class='text-right'>{{ number_format( $earning, 2 )  }}</td>  

                @elseif($normal->orderType == "assist")

                  <td><input type="checkbox" name="paymentClearance[]" value="{{$normal->id }}" checked> <a target='_blank' href="{{ URL::to('/admin/customer-care/assist-order/view-details') }}/{{$normal->id }}">{{$normal->id }}</a></td>
                 <td class='text-center'>{{ date('d-m-Y', strtotime($normal->service_date))  }}</td>
                  <td class='text-center'><span class='badge badge-warning'>{{ $normal->orderType  }}</span> by 
                    <span class='badge badge-{{  ($normal->source=="business" || $normal->source=="merchant" ) ? "warning" : "success" }}'>{{ $normal->source  }}</span>
                  </td>
                  <td class='text-center'>
                    <span class='badge badge-success'>{{ $normal->bookingStatus }}</span> -  
                    @if( strcasecmp($normal->paymentType , "ONLINE") == 0)
                      <span class='badge badge-info'>ONLINE</span> - 
                    @else 
                      <span class='badge badge-primary'>CASH</span> - 
                    @endif
                  
                    @if( strcasecmp($normal->paymentTarget , "merchant") == 0)
                      <span class='badge badge-danger'>Merchant</span>
                    @else 
                      <span class='badge badge-success'>bookTou</span>
                    @endif  
                  </td> 



                  @if( strcasecmp($normal->source , "merchant") != 0 && strcasecmp($normal->source , "business") != 0)

                    @php
                      $dueOnMerchant =0.00;
                      $totalDueOnMerchant = 0.00;
                      $totalCollectedAtMerchant = 0.00;
                      $packagingCost = 0.00;                  
                    @endphp
                      
                    @if( strcasecmp($normal->paymentType , "online") == 0  ) 
                        @if(   strcasecmp($normal->paymentTarget , "merchant") == 0 || strcasecmp($normal->paymentTarget , "business") == 0) 
                          @php
                            $dueOnMerchant = $normal->deliveryCharge;
                            $totalCollectedAtMerchant += $normal->orderCost;  
                            $totalDueOnMerchant += $dueOnMerchant; 
                            $totalOnlineMerchant += $totalCollectedAtMerchant;  
                          @endphp 
                        @else
                          @php
                            $totalOnline += $normal->orderCost;
                          @endphp  
                        @endif
                    @else
                          @php 
                            $totalCash += $normal->orderCost;  
                          @endphp 
                        
                    @endif

                    @php 
                      $commissionPc =  $business->commission  ; 
                      $orderCost = $normal->orderCost;
                      $deliveryFee = $normal->deliveryCharge;
                      $commission = ( $commissionPc * $orderCost ) / 100 ;
                      $earning =  $deliveryFee +  $commission;  
                      $totalSale += $orderCost;
                      $totalFee += $deliveryFee;
                      $totalCommission += $commission;
                      $totalEarning += $earning; 
                    @endphp 
 

                  @else 


                    @php
                      $dueOnMerchant =0.00;
                      $commissionPc =  0.00 ; 
                      $orderCost = $normal->orderCost;
                      $deliveryFee = $normal->deliveryCharge;
                      $commission = ( $commissionPc * $orderCost ) / 100 ;
                      $earning =  $deliveryFee +  $commission; 
                      $totalSale += $orderCost;
                      $totalFee += $deliveryFee;
                      $totalCommission += $commission;
                      $totalEarning += $earning;
                      $totalDueOnMerchant = 0.00;
                      $totalCollectedAtMerchant = 0.00; 

                    @endphp 


                  @endif

                  <td class='text-right'>{{ $commissionPc }} %</td> 
                  <td class='text-right'>{{ number_format($orderCost,2)  }}</td>
                  <td class='text-right'>{{ number_format( $packagingCost, 2 )  }}</td> 
                  <td class='text-right'>{{ number_format($commission,2) }}</td> 
                  <td class='text-right'>{{ number_format($deliveryFee,2) }}</td> 
                  <td class='text-right'>{{ number_format($dueOnMerchant,2) }}</td> 
                  <td class='text-right'>{{ number_format( $earning, 2 )  }}</td>  

                @endif   

              </tr>

           @else

            <tr> 
              
                @if($normal->orderType == "normal")
                  <td><a target='_blank' href="{{ URL::to('/admin/customer-care/order/view-details') }}/{{$normal->id }}">{{$normal->id }}</a></td>
                 <td class='text-center'>{{ date('d-m-Y', strtotime($normal->service_date))  }}</td> 
                  <td class='text-center'><span class='badge badge-success'>{{ $normal->orderType  }}</span> by 
                @elseif($normal->orderType == "pnd")
                  <td><a target='_blank'  href="{{ URL::to('/admin/customer-care/pnd-order/view-details') }}/{{$normal->id }}">{{$normal->id }}</a></td>
                  <td class='text-center'>{{ date('d-m-Y', strtotime($normal->service_date))  }}</td>
                  <td class='text-center'><span class='badge badge-info'>{{ $normal->orderType  }}</span> by 
                @else
                  <td><a target='_blank'  href="{{ URL::to('/admin/customer-care/assist-order/view-details') }}/{{$normal->id }}">{{$normal->id }}</a></td>
                 <td class='text-center'>{{ date('d-m-Y', strtotime($normal->service_date))  }}</td>
                  <td class='text-center'><span class='badge badge-warning'>{{ $normal->orderType  }}</span> by 
                @endif 
                <span class='badge badge-{{  ($normal->source=="business" || $normal->source=="merchant" ) ? "warning" : "success" }}'>{{ $normal->source  }}</span>
              </td>
              <td  class='text-center'>
                <span class='badge badge-danger'>{{ $normal->bookingStatus }}</span> - 
                @if( strcasecmp($normal->paymentType , "ONLINE") == 0)
                  <span class='badge badge-info'>ONLINE</span> - 
                @else 
                  <span class='badge badge-primary'>CASH</span> - 
                @endif
              
                @if( strcasecmp($normal->paymentTarget , "merchant") == 0)
                  <span class='badge badge-danger'>Merchant</span>
                @else 
                  <span class='badge badge-success'>bookTou</span>
                @endif
              </td>
              <td class='text-right'>0 %</td> 
              <td class='text-right'>0.00</td> 
              <td class='text-right'>0.00</td> 
              <td class='text-right'>0.00</td> 
              <td class='text-right'>0.00</td> 
              <td class='text-right'>0.00</td> 
              <td class='text-right'>0.00</td>  
          </tr> 

           @endif
  
        @endforeach
     
        <tr>
          <th>Order No</th> 
          <th>Date</th>
          <th>Order Type | Source</th>
          <th class='text-center'>Status | Pay Mode | Payment Target</th>
          <th class='text-right'>Commission %</th>
          <th class='text-right'>Amount</th> 
          <th class='text-right'>Packaging</th> 
          <th class='text-right'>Commission</th>
          <th class='text-right'>Delivery</th> 
          <th class='text-right'>Due on Merchant</th> 
          <th class='text-right'>Earning</th> 
        </tr>  
        <tr>
          <th colspan='4' class='text-right' style='color:red'><i>i) F=C+D; ii) D includes E</i> </th> 
            <th   class='text-right'>Total</th> 
            <th class='text-right'>A = {{ number_format($totalSale,2, '.', '') }}</th>
            <th class='text-right'>B = {{ number_format($totalPackagingCost,2, '.', '') }}</th>
            <th class='text-right'>C = {{ number_format($totalCommission,2, '.', '') }}</th> 
            <th class='text-right'>D = {{ number_format($totalFee,2, '.', '') }}</th> 
            <th class='text-right'>E = {{ number_format($totalDueOnMerchant,2, '.', '') }}</th>
            <th class='text-right'>F = {{ number_format($totalEarning,2, '.', '') }}</th>  
        </tr> 
<tfoot>
        
</tfoot> 
      </tbody> 
    </table> 
<hr/>
 
    <div class="form-group row">

    <label  class="col-sm-2 col-form-label col-form-label-sm text-right">Cash to bookTou:</label>
    <div class="col-sm-2">
      <input type="email" class="form-control form-control-sm" readonly   value="{{ number_format( $totalCash ,2, '.', '') }}"  >
    </div>

    <label  class="col-sm-2 col-form-label col-form-label-sm text-right">Online to bookTou:</label>
    <div class="col-sm-2">
      <input type="email" class="form-control form-control-sm" readonly value="{{ number_format( $totalOnline ,2, '.', '') }}"  >
    </div>

    <label  class="col-sm-2 col-form-label col-form-label-sm text-right">Commission for bookTou:</label>
    <div class="col-sm-2">
      <input type="email" class="form-control form-control-sm" readonly value="{{ number_format( $totalCommission ,2, '.', '') }}"  >
    </div>


  </div>

<div class="form-group row">
	<label  class="col-sm-2 col-form-label col-form-label-sm text-right">Amount To Collect from bookTou:</label>
    <div class="col-sm-2">
      <input type="email" class="form-control form-control-sm "  value="{{ number_format( ( $totalCash + $totalOnline - $totalCommission - $totalDueOnMerchant)  ,2, '.', '') }}"  readonly>

      <input type="hidden" class="form-control form-control-sm "  value="{{ number_format( ( $totalCash + $totalOnline - $totalCommission - $totalDueOnMerchant)  ,2, '.', '') }}" name="amount">

      <input type="hidden" class="form-control form-control-sm "  value="{{ $cyclevalue}}" name="cycle">


    </div>
    <label  class="col-sm-2 col-form-label col-form-label-sm text-right">Payment Mode</label>
    <div class="col-sm-2">


      <select  class="form-control" id="paymode" name='paymode'>
          <option>UPI</option>
          <option>Google Pay</option>
          <option>Online Transfer</option>
          <option>Cash</option> 
      </select>
    </div>

    <label  class="col-sm-2 col-form-label col-form-label-sm text-right">Transaction Ref.</label>
    <div class="col-sm-2">
       <input type="text" class="form-control form-control-sm"  value=""  name="refno" 
       placeholder="Reference No.">
    </div>

    <label  class="col-sm-2 col-form-label col-form-label-sm text-right">Clearance Date</label>
    <div class="col-sm-2">
       <input type="date" class="form-control form-control-sm"  value=""  name="clearedon" 
       placeholder="Clearance Date" >
    </div>

      <label  class="col-sm-2 col-form-label col-form-label-sm text-right">Remarks(Optional)</label>
    <div class="col-md-6">
       <textarea name="remarks" rows="1" cols="59" placeholder="Remarks"></textarea>
    </div>
 
  </div>

 <div class="form-group row">
 <div class="col-md-4"> 
 	
 </div>

 <div class="col-md-4"> 
 </div>

 <div class="col-md-4 text-right">
 <button class="btn btn-primary" type="submit" name="btnsubmit" value="btnsubmit">Save</button>  
 </div>
 </div>
</form>

 </div> 
  </div> 
  
  </div> 
</div>
  







@endsection



@section("script")

<style>
.multiselect {
  width: 200px;
}

.selectBox {
  position: relative;
}

.selectBox select {
  width: 100%;
  font-weight: bold;
}

.overSelect {
  position: absolute;
  left: 0;
  right: 0;
  top: 0;
  bottom: 0;
}

#checkboxes {
  display: none;
  border: 1px #dadada solid;
}

#checkboxes label {
  display: block;
}

#checkboxes label:hover {
  background-color: #1e90ff;
}

</style>

<script>

var expanded = false;

function showCheckboxes() {
  var checkboxes = document.getElementById("checkboxes");
  if (!expanded) {
    checkboxes.style.display = "block";
    expanded = true;
  } else {
    checkboxes.style.display = "none";
    expanded = false;
  }
}


$(document).on("click", ".btn_add_promo", function()
{

    $("#bin").val($(this).attr("data-bin"));

      $("#modal_add_promo").modal("show")

});

 



$('body').delegate('.btnToggleBlock','click',function(){
  
   var bin  = $(this).attr("data-id"); 
   var status = $(this).is(":checked"); 
 var json = {};
    json['bin'] =  bin ;
    json['status'] =  status ;



 
   $.ajax({
      type: 'post',
      url: api + "v3/web/business/toggle-business-block" ,
      data: json,
      success: function(data)
      {
        data = $.parseJSON(data);    
            
      },
      error: function( ) 
      {
        $(".loading_span").html(" "); 
        alert(  'Something went wrong, please try again'); 
      } 

    });
  

  
})

 




$('body').delegate('.btnToggleClose','click',function(){
  
   var bin  = $(this).attr("data-id"); 
   var status = $(this).is(":checked"); 

    var json = {};
    json['bin'] =  bin ;
    json['status'] =  status ;
 
 
   $.ajax({
      type: 'post',
      url: api + "v3/web/business/toggle-business-open" ,
      data: json,
      success: function(data)
      {
        data = $.parseJSON(data);        
      },
      error: function( ) 
      {
        $(".loading_span").html(" "); 
        alert(  'Something went wrong, please try again'); 
      } 

    });
  

  
})

 
 

    

      $('.selectize').select2({
        selectOnClose: true
      });


      $(".selectize").val({{ $bin }}).trigger('change');


$(document).on("click", ".btn-print", function()
{
  window.open($(this).attr("data-pdfurl"), "popupWindow", "width=600,height=600,scrollbars=yes"); 
});

</script> 

@endsection