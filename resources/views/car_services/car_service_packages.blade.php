@extends('layouts.erp_theme_03')
@section('content')

<?php 
$cdn_url =   env('APP_CDN') ;  
    $cdn_path =   env('APP_CDN_PATH') ; // cnd - /var/www/html/api/public
    ?>
    <div class="row">
     <div class="col-md-12"> 
       <div class="card card-default">
         <div class="card-header">
          <div class="card-title"> 
            <div class="row">
              <div class="col-md-8">
                <div class="title-block"> 
                 <h5>Service Listed Under <span class='badge badge-primary'>{{  $data['business']->name }}</span></h5> 
               </div> 
             </div>
             <div class="col-md-4 text-right"> 
              <button class='btn btn-primary btn-sm' data-toggle="modal" data-target="#modalepg" ><i class='fa fa-plus'></i></button>
            </div>

          </div>
        </div>
      </div>
      <div class="card-body">
        @if (session('err_msg'))
        <div class="col-md-12">
          <div class="alert alert-info">
           {{session('err_msg')}}
         </div>
       </div>
       @endif 
       <div class="table-responsive">
        <table class="table"> 
          <thead class=" text-primary">
            <tr >
              <th scope="col">Image</th>
              <th scope="col">Item </th> 
              <th scope="col">Price</th>
              <th scope="col">Discount</th>
              <th scope="col">Service Category</th>
              <th scope="col">Service type</th>
              <th scope="col">Gender</th>
              <th scope="col">Delete</th>
              <th scope="col">Action</th>
            </tr>
          </thead> 
          <tbody> 
            @foreach ( $data['services'] as $item)  
            <tr >
              <td> 
              <img src="{{ $item->photos  }}" alt="..." height="50px" width="50px"> 
            </td>
             <td style="white-space: normal;"><span class='badge badge-primary'>{{ $item->id }}</span><br/>
              {{ $item->srv_name }}<br/>     
              <td>{{$item->pricing}}</td>
              <td>{{$item->discount}}</td> 
              <td>{{$item->service_category}}</td>
              <td>{{$item->service_type}}</td>
              <td>{{$item->gender}}</td>
               <td><button class="btn btn-danger btndel" 
                        data-id="{{$item->id}}" data-widget="del"
                        >Delete</button></td>
                        <td class="text-center">
                          <div class="dropdown">
                           <a class="btn btn-primary btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-cog "></i>
                          </a>
                          <ul class="dropdown-menu dropdown-user pull-right"> 
                            <li><a class="dropdown-item showdialogm1"
                              data-name="{{ $item->service_category }}" 
                              data-key="{{ $item->id }}" 
                              data-srvname="{{ $item->srv_name}}"
                              data-gender="{{ $item->gender}}"
                              data-price="{{ $item->pricing}}"
                              data-srvtype="{{ $item->service_type}}"
                              >Update services</a></li>
                              <li><a class="dropdown-item" href="{{URL::to('/admin/customer-care/business/view-more-details-crs')}}/{{$item->id}}">View More Details</a>
                              </li>
                            </ul>
                          </div>
                        </td>
            </tr>
            @endforeach
          </tbody>
        </table>
        {{ $data['services']->links() }}
      </div>
    </div>
  </div>    
</div> 
</div>

<form action="{{action('Erp\CarRentalServiceController@addCarRentalServices')}}" method="post" enctype="multipart/form-data">
  {{ csrf_field() }}
  <div class="modal fade" id="modalepg" tabindex="-1" aria-labelledby="modalepg" aria-hidden="true">
    <div class="modal-dialog modal-xl modal-dialog-centered">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="modalepglabel">Add new car rental services</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="row">
           <div class="col-md-4 ">
            <label  class="form-label">Service category:</label>

            <select class="form-control" name="service_category" id="service_category">
           @foreach($data['srv_category'] as $item)           
            <option value="{{$item->category}}">
              {{$item->category}}
            </option>
           @endforeach
          </select>
          </div>
          <div class="col-md-4 ">
            <label  class="form-label"> Service name:</label>
            <input type="text" name="srv_name" class=" form-control">
          </div>
          <div class="col-md-4 ">
            <label  class="form-label"> Service details:</label>
            <input type="text" name="srv_details" class=" form-control">
          </div>
        </div>
        <div class="row">
         <div class="col-md-4 ">
          <label  class="form-label"> Pricing:</label>
          <input type="text" name="pricing" class=" form-control">
        </div>
        <div class="col-md-4 ">
          <label  class="form-label">Actual price:</label>
          <input type="text" name="actual_price" class=" form-control">
        </div>
        <div class="col-md-4 ">
          <label  class="form-label">Maximum Price:</label>
          <input type="text" name="max_price" class=" form-control" placeholder="">
        </div>
      </div>
      <div class="row">
       <div class="col-md-4 ">
        <label  class="form-label">Discount:</label>
        <input type="text" name="discount" class=" form-control" placeholder="0.00">
      </div>
      <!-- <div class="col-md-4 ">
        <label  class="form-label">GST code:</label>
        <input type="text" name="gst_code" class=" form-control">
      </div> -->
      <div class="col-md-4 ">
        <label  class="form-label">Duration hour:</label>
        <input type="text" name="duration_hr" class=" form-control">
      </div>
    </div>
    <div class="row">
     <div class="col-md-4 ">
      <label  class="form-label">Duration minute:</label>
      <input type="text" name="duration_min" class=" form-control">
    </div>
    <div class="col-md-4 ">
      <label  class="form-label">Photos:</label>
      <input type="file" name="photo" class=" form-control">
    </div>
    <div class="col-md-4 ">
      <label  class="form-label">Service status :</label>
      <select class="form-control form-control-sm boxed "  name='srv_status' >
      <option>bookable</option>
      <option>not_bookable</option>                       
    </select> 
    </div>
  </div>
  <div class="row">
   <div class="col-md-3 ">
    <label  class="form-label">service type :</label>
    <input type="text" name="service_type" class=" form-control">
  </div>
 
  <div class="col-md-3 ">
    <label  class="form-label">Valid start date:</label>
    <input type="text" name="valid_start_date" class="calendar form-control">
  </div>
  <div class="col-md-3 ">
    <label  class="form-label">Valid end date:</label>
    <input type="text" name="valid_end_date" class="calendar form-control">
  </div>
</div>
</div>
  <div class="modal-footer"> 
        <span class='loading_span'></span>
         <input type='hidden' name='key' id='key' />
          <input type='hidden' name='gender' id='gender' value="not-relevant" />
        <input type='hidden' name='bin' id='bin' value="{{ $data['business']->id  }}" />
        <button type="submit" class="btn btn-success" id="btnsaveconfirmation" name='btnsaveconfirmation' value='save'  >Save &amp; Promote</button> 
        <button type="button" class="btn btn-danger" id="closeconfmodal" data-dismiss="modal">Close</button> 
      </div>
</div>
</div>
</div>
</form>
<form action='{{ action("Erp\CarRentalServiceController@editCarRentalService") }}' method='post' enctype="multipart/form-data" >
  {{ csrf_field() }}
  <div class="modal" id='widget-m1' tabindex="-1" role="dialog">
    <div class="modal-dialog  modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Edit car rental services</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
         <div class="form-row"> 
           <div class="col-md-4">
            <label  class="form-label">Category-Image</label>
            <input class="form-control" type="file" id="photo" name="photo">
            
          </div>
    
          <div class="col-md-4 ">
            <label  class="form-label"> Service name:</label>
            <input type="text" name="srv_name" class=" form-control" id="srvname">
          </div>

  <div class="col-md-4 ">
          <label  class="form-label"> Pricing:</label>
          <input type="text" name="pricing" class=" form-control" id="price">
        </div>
        <div class="col-md-3 ">
    <label  class="form-label">service type :</label>
    <input type="text" name="service_type" class=" form-control" id="srvtype">
  </div>
        </div>         
      </div>
      <div class="modal-footer"> 
        <span class='loading_span'></span>
        <input type='hidden' name='key' id='keys' />
        <input type='hidden' name='bin' id='bin' value="{{ $data['business']->id  }}" />
        <button type="submit" class="btn btn-success" id="btnsaveconfirmation" name='btnsaveconfirmation' value='save'  >Save &amp; Promote</button> 
        <button type="button" class="btn btn-danger" id="closeconfmodal" data-dismiss="modal">Close</button> 
      </div>
    </div>
  </div>
</div>
</form>

<form action="{{action('Erp\CarRentalServiceController@deleteCarRentalServices')}}" method="post" enctype="multipart/form-data"> 
   {{ csrf_field() }}
   <div class="modal fade" id="widget-del" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="confirmDel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" >Confirm Record Deletion</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">You are about to delete this record. Please confirm your action?</div>
        <div class="modal-footer">
         <input type='hidden' name='key' id='keydel'/>
         <input type='hidden' name='bin' id='bin' value="{{ $data['business']->id  }}" />
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <button type='submit' class="btn btn-danger" value='delete' name='btndel'>Delete</button>
        </div>
      </div>
    </div>
  </div>
 </form>
@endsection
@section("script")
<script>
$(function() {
    $('.calendar').pignoseCalendar( 
    {
      format: 'DD-MM-YYYY' 
    });
  });
$(".btndel").on('click', function(){ 
      var key  = $(this).attr("data-id"); 

      $("#keydel").val(key); 
      $widget  = $(this).attr("data-widget"); 
      $("#widget-"+$widget).modal('show'); 
});

  $(document).on("click", ".showdialogm1", function()
  {
    $("#serviceCategory").val( $(this).attr("data-name"));
    $("#keys").val( $(this).attr("data-key"));
    $("#photo").attr("src", $(this).attr("data-photo"));
    $("#srvname").val( $(this).attr("data-srvname"));
    $("#price").val( $(this).attr("data-price"));
    $("#srvtype").val( $(this).attr("data-srvtype"));
    $("#gender").val( $(this).attr("data-gender")); 

    $("#widget-m1").modal("show")
  });
</script>

@endsection
