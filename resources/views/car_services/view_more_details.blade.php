@extends('layouts.erp_theme_03')
@section('content')
<?php 
  $cdn_url =    config('app.app_cdn') ;  
  $cdn_path =     config('app.app_cdn_path') ;  
  $crs_services = $data['crs_services']; 
?>


@if (session('err_msg'))
    <div class="col-md-12 alert alert-success">
        {{ session('err_msg') }}
    </div>
@endif
<div class="row">
    
 <div class='col-md-6' > 
   <div class="card  ">
    <div class="card-header"> 
      <div class="row">
       <div class='col-md-10'> 
         <h5><span class='badge badge-primary'>{{ $crs_services->srv_name}}</span></h5> 
      </div>
      <div class="col-md-2 text-right">
        <div class="dropdown">
         <a class="btn btn-primary btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <i class="fa fa-cog "></i>
        </a>
        <ul class="dropdown-menu dropdown-user pull-right"> 
        <!--   <li><a class="dropdown-item btnpangstfssai" 
            href="#">
          PAN/GST/FSSAI</a>
        </li> -->          
      </ul>
    </div>
  </div>
</div>
</div>
<div class="card-body  ">
  <div class='row'>
     <div class='col-md-4'>
      <img src="{{ $crs_services->photos  }}" alt="..." height="170px" width="170px">

      </div>
   <div class='col-md-8'>
  <strong>Service type :</strong>{{ $crs_services->service_type}}
  <br><strong>Service name :</strong>{{ $crs_services->srv_name}}
  <br><strong>Service details :</strong>{{ $crs_services->srv_details}}
  <br><strong>Discount :</strong>{{ $crs_services->discount}}<br>
  <strong>Duration hour :</strong>{{ $crs_services->duration_hr}}<br>
  
    <hr>
  <!--   <div class='row'>
      
      
    
      <div class='col-md-6'>
        <strong>Verified Business: </strong>        
        <i class='fa fa-check-circle fa-2x' style='color: blue'></i>       
       <i class='fa fa-times   fa-2x' style='color: red'></i>           
      </div>
      <div class='col-md-6'>
        <strong>Self Pickup Business</strong>          
        <i class='fa fa-check-circle fa-2x' style='color: green'></i>      
        <i class='fa fa-times   fa-2x' style='color: red'></i>   
      </div>
    </div> -->
     
  </div>
</div>
</div>
</div>
</div>

<div class='col-md-6'>
  <div class="card  ">
    <div class="card-header"> 
      <div class='row'>
        <div class='col-md-10'>
          <h4 class="card-title text-center"> Add technical specification</h4>
        </div>
        <div class='col-md-2 text-right'>
         <button class='btn btn-primary btn-sm showdialogm1' data-toggle="modal" data-target="#modalepg" data-key="{{ $crs_services->id }}"
        ><i class='fa fa-plus'></i>
   
         </button>
        </div>
      </div>
    </div>
    <div class="card-body text-center">
      <strong>Technical specification list:</strong>
      <div class="table-responsive table table-bordered ">
        <table class="table"> 
          <thead class=" text-primary">
            <tr >

               <th scope="col">Service product Id</th>
              <th scope="col">Meta key</th>
              <th scope="col">Meta value </th>
              <th scope="col">Edit </th>  
            </tr>
   
          </thead> 
          <tbody> 
             
         @foreach($data['srv_package_spec'] as $item)
            <tr >
              <td>{{$item->srv_prid}}</td>       
              <td>{{$item->meta_key}}</td>
               <td>{{$item->meta_value}}</td>
               <td>

                <div class="dropdown  ">
                  <a class="btn btn-primary btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-cog "></i></a>
                  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                  <a class="dropdown-item btnedit"  data-widget="del"
                    data-key="{{$item->id}}"
                    data-srvprid="{{$item->srv_prid}}"
                    data-metakey="{{$item->meta_key}}"
                    data-metavalue="{{$item->meta_value}}"
                    data-metatext="{{$item->meta_key_text}}" 
                    data-widget="edit"
                    >Edit</a>
                   <a class="dropdown-item btndel" data-key="{{$item->id}}" data-widget="del">Delete</a>
                  </div>
                </div>
                </td>                       
            </tr>
          @endforeach
          </tbody>
        </table>
      
      </div>   
    </div>
  </div>
</div>
</div>

     

<form action='{{ action("Erp\CarRentalServiceController@addServicePackageSpec") }}' method='post' enctype="multipart/form-data">
  {{ csrf_field() }}
  

 <div class="modal" id='widget-m1' tabindex="-1" role="dialog" >
  <div class="modal-dialog  modal-dialog-centered" >    
    <div class="modal-content ">
      <div class="modal-header">
        <h5 class="modal-title">Add technical specification</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" >        
        <div class="row "> 
         <div class="col-md-6">
          <label for="meta_key_text" class="form-label">Meta key</label>
          <select class="form-control meta_key_text" name="metakey" id="metakey">
           @foreach($data['pk_meta_key'] as $item) 
            <option value="{{$item->meta_key}}" >
             {{$item->meta_key_text}}
            </option>                      
           @endforeach
          </select>
         </div>
             <div class="col-md-6" > 
             <label for="meta_key_text" class="form-label">Meta key text</label>       
          <select  class="form-control" name="meta_key_text" id="meta_key_text">
           @foreach($data['pk_meta_key'] as $item)           
            <option value="{{$item->meta_key_text}}">
              {{$item->meta_key_text}}
            </option>
           @endforeach
          </select>
         </div> 
         <div class="col-md-6">
          <label class="form-label">Meta value</label>
          <input type="text" name="metavalue" id="metavalue" class="form-control">
         </div>

                     
      </div>

      <div class="modal-footer mt-1">

        <input type='hidden' name='srv_prid' id='srv_prid' value="{{$crs_services->id}}" />
        <button type="submit" class="btn btn-success" id="btnsaveconfirmation" name='btnsaveconfirmation' value='save'  >Save &amp; Promote</button> 
        <button type="button" class="btn btn-danger" id="closeconfmodal" data-dismiss="modal">Close</button> 
      </div>
    </div>
  
  </div>
</div>
</div>
</form>
<form action='{{ action("Erp\CarRentalServiceController@editServicePackageSpec") }}' method='post' enctype="multipart/form-data">
  {{ csrf_field() }}
 <div class="modal" id='widget-m2' tabindex="-1" role="dialog" >
  <div class="modal-dialog  modal-dialog-centered" >    
    <div class="modal-content ">
      <div class="modal-header">
        <h5 class="modal-title">Edit technical specification</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" >        
        <div class="row "> 
         <div class="col-md-6">
          <label  class="form-label">Meta key</label>
         <!--  <select class="form-control" name="metakey" id="metakey">
           @foreach($data['pk_meta_key'] as $item)           
            <option value="{{$item->meta_key}}">
              {{$item->meta_key}}
            </option>
           @endforeach
          </select> -->
          <input type="text" name="meta_key" id="meta_key" class="form-control">
         </div>
         <div class="col-md-6">
          <label class="form-label">Meta value</label>
          <input type="text" name="meta_value" id="meta_value" class="form-control">
         </div>
         <div class="col-md-6">
          <label class="form-label">Meta key value</label>
          <input type="text" name="meta_keytext" id="meta_keytext" class="form-control">
         </div>                 
      </div>

      <div class="modal-footer mt-1"> 
        <input type='hidden' name='key' id='key' />
        <input type='hidden' name='srv_prid' id='srv_prid' value="{{$crs_services->id}}" />
        <button type="submit" class="btn btn-success" id="btnsaveconfirmation" name='btnsaveconfirmation' value='save'  >Save &amp; Promote</button> 
        <button type="button" class="btn btn-danger" id="closeconfmodal" data-dismiss="modal">Close</button> 
      </div>
    </div>
  
  </div>
</div>
</div>
</form>

<form action="{{action('Erp\CarRentalServiceController@deleteServicePackageSpec')}}" method="post" enctype="multipart/form-data"> 
   {{ csrf_field() }}
   <div class="modal fade" id="widget-m3" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="confirmDel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" >Confirm Record Deletion</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">You are about to delete this record. Please confirm your action?</div>
        <div class="modal-footer">
         <input type='hidden' name='key' id='keydel' />
        <input type='hidden' name='srv_prid' id='srv_prid' value="{{$crs_services->id}}" />
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <button type='submit' class="btn btn-danger" value='delete' name='btndel'>Delete</button>
        </div>
      </div>
    </div>
  </div>
 </form>
@endsection
 @section("script")

<script>

$(document).on("click", ".showdialogm1", function()
{
  $("#widget-m1").modal("show")
});
$(document).on("click", ".btnedit", function()
  {   
    $("#key").val( $(this).attr("data-key"));
    $("#meta_key").val( $(this).attr("data-metakey"));
    $("#meta_value").val( $(this).attr("data-metavalue"));
    $("#meta_keytext").val( $(this).attr("data-metatext")); 
    $("#widget-m2").modal("show")
  });
$(".btndel").on('click', function(){ 
       $("#keydel").val( $(this).attr("data-key"));
        $("#widget-m3").modal("show") 
});
  </script>

 @endsection





 
  
 
