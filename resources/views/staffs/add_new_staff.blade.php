@extends('layouts.erp_theme_03')
@section('content')

<div class="row">
     <div class="col-md-6"> 
        <div class="card panel-default">
           <div class="card-body"> 
                <div class="row">
                    <div class="col-lg-12"> 
                            <h5>Staff Information</h5> 
                </div>  
                </div> 
                <hr/>  
              @if (session('err_msg'))
              <div class="col-12">
               <div class="alert alert-info">
                {{ session('err_msg') }}
                </div>
                 </div>
              @endif

              <form action="{{ action('Erp\ErpToolsController@saveStaff') }}" method='post'>
              @csrf
              <div class="form-row"> 

<div class="form-group col-md-4">
  <label for="fname">First Name</label>
  <input type="text" name ="fname" required class="form-control" id="fname">
</div>
<div class="form-group col-md-4">
  <label for="mname">Middle Name</label>
  <input type="text" class="form-control" id="mname" name="mname">
</div>
<div class="form-group col-md-4">
  <label for="lname">Last Name</label>
  <input type="text" class="form-control" required id="lname" name='lname'>
</div>

</div> 


  <div class="form-row">
  <div class="form-group col-md-4">
      <label for="phone">Phone</label>
      <input type="text" class="form-control" required name='phone' id="phone">
    </div>


    <div class="form-group col-md-4">
      <label for="email">Email</label>
      <input type="email" class="form-control" required id="email" name='email'>
    </div>
    <div class="form-group col-md-4">
      <label for="password">Password</label>
      <input type="password" class="form-control" required id="password" name='password'>
    </div>


  </div> 


  <div class="form-group">
    <label for="address">Address</label>
    <input type="text" class="form-control" required  name="address" id="address" placeholder="Address">
  </div>
  <div class="form-group">
    <label for="landmark">Landmark</label>
    <input type="text" class="form-control" id="landmark" name="landmark" placeholder="Landmark to easily identify address">
  </div>
  <div class="form-row">
    <div class="form-group col-md-6">
      <label for="city">City</label>
      <input type="text" class="form-control" id="city" name="city">
    </div>
    <div class="form-group col-md-4">
      <label for="state">State</label>
      <select id="state" name='state' class="form-control">
        <option selected>Choose...</option>
        <option>...</option>
      </select>
    </div>
    <div class="form-group col-md-2">
      <label for="pin">Pin Code</label>
      <input type="text" name='pin' class="form-control" id="pin">
    </div>
  </div>
 
  <button type="submit" value='save' class="btn btn-primary">Save Staff Profile</button>
</form>



               

              
               
                 
              
            </div>
          </div>
        </div>
 
        </div> 

   @endsection  


   @section("script")
 

 
@endsection 

