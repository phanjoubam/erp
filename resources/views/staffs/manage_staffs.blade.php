@extends('layouts.erp_theme_03')
@section('content')

<?php  
    $cdn_url =   $_ENV['APP_CDN'] ;  
    $cdn_path =   $_ENV['APP_CDN_PATH'] ; 

?>

 <div class="row">
   <div class="col-md-12">  
     <div class="card card-default">
     <div class="card-body"> 
     <div class="row">
                    <div class="col-lg-12"> 
                            <h5>Manage All Staffs</h5> 
                </div>  
                </div> 
                <hr/> 


       <div class="card-body"> 
  @if (session('err_msg'))
  <div class="col-md-12">
    <div class="alert alert-info">
  {{ session('err_msg') }}
  </div>
  </div>
@endif
  <div class="table-responsive">
  <table class="table"> 
    <thead class=" text-primary">
      <tr >
        <th >Sl. No.</th>
        <th >Full Name</th> 
        <th >Email</th>
        <th >Phone</th> 
        <th >Address</th> 
        <th class='text-center' >Action</th>
      </tr>
    </thead> 
    <tbody> 
    @foreach ($staffs as $item)  
     <tr >
     <td>

      <?php  
      $profile_photo  = $cdn_url . "/assets/image/no-image.jpg" ;
 

      if(  $item->profile_photo  != ""  )
      { 
          $profile_photo =  $item->profile_photo   ;  
      } 
           
      ?> 

      <img src="{{ $profile_photo   }}" alt="..." height="50px" width="50px">

    </td>
     <td><span class='badge badge-primary'>{{ $item->fullname }}</span></td> 
      <td>{{$item->email}}</td>
      <td>{{$item->phone}}</td> 
      <td>{{$item->locality}}</td>  
 
      <td class="text-center">

                       
                     <div class="dropdown">
                        <a class="btn btn-primary btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          <i class="fa fa-cog "></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow"> 

                        <a class="dropdown-item"  href="{{ URL::to('/services/manage-services/edit') }}/{{ $item->id  }}">Edit</a> 
                        <a class="dropdown-item"  href="{{ URL::to('/services/manage-services/edit') }}/{{ $item->id  }}">Upload Photo</a> 
                        <div class="dropdown-divider"></div>
                        <button class="dropdown-item delstaff" data-id="{{ $item->id }}"  >Remove Staff</button> 

                        </div>
                      </div>
                  
        </td>
 
    </tr>
    @endforeach
   </tbody> 
  </table>  
 </div> 
  </div>
  </div>   
   
</div>    
  
   </div> 
 </div>
 

 <form action="{{ action('Erp\ErpToolsController@removeStaff') }}" method='post'>
 @csrf
 <div class="modal fade" id="delconfirm" tabindex="-1" aria-labelledby="delconfirm" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="delconfirm">Confirm Staff Deletion</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>You choose to delete a staff. This action is non-recoverable. Please confirm your action.<br/>
        Are you sure you want to delete staff?</p>
      </div>
      <div class="modal-footer">
      <input type="hidden"   id="key" name='key'>
        <button type="submit" class="btn btn-danger">Yes</button>
        <button type="button" class="btn btn-primary"  data-dismiss="modal">No</button>
      </div>
    </div>
  </div>
</div>
</form>


@endsection

@section("script")

<script>
 

 $(".delstaff").on("click", function(){ 
     
    $("#key").val( $(this).attr("data-id") );
    $("#delconfirm").modal("show");

 })

 


</script>

 @endsection
