@extends('layouts.erp_theme_03')
@section('content')

<?php  
    $cdn_url =   $_ENV['APP_CDN'] ;  
    $cdn_path =   $_ENV['APP_CDN_PATH'] ; 

?>
<div class="row">
     <div class="col-md-4"> 
        <div class="card panel-default">
           <div class="card-body"> 
                <div class="row">
                    <div class="col-lg-12"> 
                            <h5>Staff Information</h5> 
                </div>  
                </div> 
                <hr/>  
              @if (session('err_msg'))
              <div class="col-12">
               <div class="alert alert-info">
                {{ session('err_msg') }}
                </div>
                 </div>
              @endif

              <form action="{{ action('Erp\ErpToolsController@saveStaffDayOff') }}" method='post'>
              @csrf
              <div class="form-row"> 

<div class="form-group col-md-8">
  <label for="staff">Select Staff:</label>
  <select type="text" name ="staff" required class="form-control" id="staff">
    @foreach($staffs as $staff)
        <option value="{{ $staff->id }}">{{ $staff->fullname }} ({{ $staff->phone }})</option>
    @endforeach
  </select>
</div>
<div class="form-group col-md-4">
  <label for="dayoff">Select Date</label>
  <input type="date" class="form-control" id="dayoff" name="dayoff">
</div> 
 

</div> 


  <div class="form-row">
 
  <div class="form-group form-check">
    <input type="checkbox" class="form-check-input" id="onleave" name='onleave'>
    <label class="form-check-label" for="onleave">Set Staff as on leave</label>
  </div>
 

  </div> 

 
 
  <button type="submit" value='save' class="btn btn-primary">Save Staff Profile</button>
</form>



               

              
               
                 
              
            </div>
          </div>
        </div>



        <div class="col-md-8">  
     <div class="card card-default">
     <div class="card-body"> 
     <div class="row">
                    <div class="col-lg-12"> 
                            <h5>Day Offs for staffs</h5> 
                </div>  
                </div> 
                <hr/> 


       <div class="card-body"> 
 
  <div class="table-responsive">
  <table class="table"> 
    <thead class=" text-primary">
      <tr >
        <th >Sl. No.</th>
        <th >Full Name</th>   
        <th >Off Date</th> 
        <th class='text-center' >Day Off</th>
      </tr>
    </thead> 
    <tbody> 
    @foreach ($day_offs as $item)  
     <tr >
     <td>

      <?php  
      $profile_photo  = $cdn_url . "/assets/image/no-image.jpg" ;
 

      if(  $item->profile_photo  != ""  )
      { 
          $profile_photo =  $item->profile_photo   ;  
      } 
           
      ?> 

      <img src="{{ $profile_photo   }}" alt="..." height="50px" width="50px">

    </td>
     <td><span class='badge badge-primary'>{{ $item->fullname }}</span> ( {{$item->phone}} )</td> 
      <td>{{ date('d-m-Y', strtotime( $item->off_date)) }}</td>   
      <td class="text-center">
            <div class="custom-control custom-switch">
                          <input type="checkbox" class="custom-control-input switch"   data-id='{{$item->id }}' id="switchON{{ $item->id }}"   checked  />
                          <label class="custom-control-label" for="switchON{{$item->id }}">Confirmed</label>
                        </div>      
                  
        </td>
 
    </tr>
    @endforeach
   </tbody> 
  </table>  
 </div> 
  </div>
  </div>   
   
</div>    
  
   </div> 

 
        </div> 

   @endsection  


   @section("script")
 

 
@endsection 

