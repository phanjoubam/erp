@extends('layouts.erp_theme_03')
@section('content')

<div class="row">
 
            
    @foreach ($data['productCategory'] as $item)
    
 <div class="col-md-2"> 
  <a class="card-link" href="{{URL::to('/erp/product-categories/all-products/'.$data['bin'] . '/' . $item->categoryId)}}">
 

  <div class='card'>
    <div class='card-body'>

          <?php 

          if($item->image == "")
          { 
              $image_url =  URL::to('/') . "/public/assets/image/no-image.jpg"; 
          } 
          else
          {
            if(file_exists(  base_path() .   $item->image ))
              {
                $image_url =  URL::to('/') .  $item->image;
              }
              else 
              {
                $image_url = URL::to('/') . "/public/assets/image/no-image.jpg"; 
                
              } 
             }     
         ?>
    <img src='{{ $image_url }}' width='90px' class="card-img-top" alt="...">

      <div class='row'>
      <div class='col-md-12 ml-l text-center'>
      <strong class="text-center">{{$item->categoryName}} ({{$item->itemCount}})</strong><br/> 
   
</div>
</div> 
</div>  
</div>
  </a> 
     </div>

 


  @endforeach
 
 
</div>

@endsection  
