<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

            <!-- Sidebar - Brand -->
            <a class="sidebar-brand d-flex align-items-center justify-content-center"
            href="{{  URL::to('/') }}">
                <div class="sidebar-brand-icon ">
                    <img class="img-fluid"  src="{{ URL::to('/public/assets/image/logo_white.png') }}" />
                </div>

            </a>

            <!-- Divider -->
            <hr class="sidebar-divider my-0">

            <!-- Nav Item - Dashboard -->
            <li class="nav-item active">
                <a class="nav-link" href="{{  URL::to('/') }}">
                    <i class="fas fa-fw fa-tachometer-alt"></i>
                    <span>Dashboard</span></a>
            </li>

            <!-- Divider -->
            <hr class="sidebar-divider">

            <!-- Heading -->
            <div class="sidebar-heading">
               ERP Module
            </div>

            <!-- Nav Item - Pages Collapse Menu -->
            <li class="nav-item">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#navCollapsePos"
                    aria-expanded="true" aria-controls="navCollapsePos">
                   <i class="fa fa-cart-arrow-down"></i>
                    <span>POS</span>
                </a>
                <div id="navCollapsePos" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">
                    <a class="collapse-item" href="{{ URL::to('/pos/overview') }}">Overview</a>
                    <a class="collapse-item" href="{{ URL::to('/pos/prepare-sales-window') }}">New Sale (Classic)</a>
                    <a class="collapse-item" href="{{ URL::to('/pos/pos-sales-window-touch-and-go') }}">New Sale (Modern)</a>
                    <a class="collapse-item" href="{{ URL::to('/pos/view-sales-orders') }}">All Orders</a>
                    </div>
                </div>
            </li>


            <li class="nav-item">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#navCollapsefirst"
                    aria-expanded="true" aria-controls="navCollapsefirst">
                   <i class="fa fa-shopping-basket"></i>
                    <span>Orders & Bookings</span>
                </a>
                <div id="navCollapsefirst" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">

                        <a class="collapse-item"  href="{{  URL::to('/admin/customer-care/pick-and-drop-orders/view-all') }}">Pick and Drop</a>
                        <a class="collapse-item"  href="{{  URL::to('/pick-and-drop-orders/view-active-orders') }}">Pick and Drop Orders</a>
                        <a class="collapse-item" href="{{  URL::to('/orders/view-active-orders') }}">View Orders</a>
                        <a class="collapse-item" href="{{ URL::to('/services/view-bookings') }}" >View Bookings</a>
                        <a class="collapse-item" href="{{ URL::to('/services/add-new-booking') }}">New Booking</a>
                    </div>
                </div>
            </li>

            <?php
                $main_module = session()->get('MAIN_MODULE');
            ?>
           
         @if(strcasecmp($main_module,"booking") == 0)
        
            <li class="nav-item">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#navCollapseSecond"
                    aria-expanded="true" aria-controls="navCollapseSecond">
                   <i class="fa fa-shopping-basket"></i>
                    <span>Car Rental Service</span>
                </a>
                <div id="navCollapseSecond" class="collapse" aria-labelledby="headingCRS" data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">                        
                        <a class="collapse-item" href="{{URL::to('/view-car-packages')}}">Add And listing CRS</a>
                        
                    </div>
                </div>
            </li>          
      
     
        @endif
            <!-- Nav Item - Utilities Collapse Menu -->
            <li class="nav-item">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#navCollapsesecond"
                    aria-expanded="true" aria-controls="navCollapsesecond">
                    <i class="fas fa-box-open"></i>
                    <span>Products</span>
                </a>
                <div id="navCollapsesecond" class="collapse" aria-labelledby="headingUtilities"
                    data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">
                        <a class="collapse-item" href="{{ URL::to('/products/create-coupon') }}">Create Coupon</a>
                        <a class="collapse-item" href="{{ URL::to('/recent-products') }}">Recently Add Products</a>
                        <a class="collapse-item" href="{{ URL::to('/all-products') }}">All Products</a>
                        <a class="collapse-item" href="{{ URL::to('/low-stock-products') }}">Low Stock</a>
                        <a class="collapse-item" href="{{ URL::to('/admin/product/enter-save-product') }}">New Products</a>
                    </div>
                </div>
            </li>


            <li class="nav-item">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#navCollapseThird"
                    aria-expanded="true" aria-controls="navCollapseThird">
                    <i class="fas fa-fw fa-wrench"></i>
                    <span>Service</span>
                </a>
                <div id="navCollapseThird" class="collapse" aria-labelledby="headingUtilities"
                    data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">

                        <a class="collapse-item" href="{{ URL::to('/services/add-new-service') }}">New Service</a>
                        <a class="collapse-item" href="{{ URL::to('/services/manage-services') }}">Manage Services</a>

                    </div>
                </div>
            </li>




            <li class="nav-item">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#navCollapseFourth"
                    aria-expanded="true" aria-controls="navCollapseFourth">
                    <i class="fas fa-users"></i>
                    <span>Staff & Attendance</span>
                </a>
                <div id="navCollapseFourth" class="collapse" aria-labelledby="headingUtilities"
                    data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">

                        <a class="collapse-item" href="{{ URL::to('/services/add-new-staff') }}">Add Staff</a>
                        <a class="collapse-item" href="{{ URL::to('/services/manage-staffs') }}">Manage Staffs</a>
                        <a class="collapse-item" href="{{ URL::to('/services/staff/add-day-off') }}">Add Staffs Day Off</a>
                    </div>
                </div>
            </li>



            <li class="nav-item">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#navCollapseFifth"
                    aria-expanded="true" aria-controls="navCollapseFifth">
                    <i class="fas fa-clipboard-list"></i>
                    <span>Reports</span>
                </a>
                <div id="navCollapseFifth" class="collapse" aria-labelledby="headingUtilities"
                    data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">

                        <a class="collapse-item" href="{{ URL::to('reports/sales-analytics')}}">Sales Analytics</a>

<a class="collapse-item" href="{{ URL::to('reports/billing-and-clearance/daily-sales')}}">Daily Sales</a>
<a class="collapse-item" href="{{ URL::to('reports/monthly-earning')}}">Monthly Sales</a>
<a class="collapse-item" href="{{ URL::to('reports/sales-and-service-per-cycle')}}">Weekly Sales Cycle</a>

                    </div>
                </div>
            </li>

            <li class="nav-item">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#navCollapseSixth"
                    aria-expanded="true" aria-controls="navCollapseSixth">
                    <i class="fas fa-clipboard-list"></i>
                    <span>Billing & Clearance</span>
                </a>
                <div id="navCollapseSixth" class="collapse" aria-labelledby="headingUtilities"
                    data-parent="#accordionSidebar">
        <div class="bg-white py-2 collapse-inner rounded">
            <a class="collapse-item" href="{{ URL::to('clearance/show-payment-due')}}">Weekly Clearance</a>
            <a class="collapse-item" href="{{ URL::to('reports/billing-and-clearance/sales-and-clearance-report')}}">Clearance Report</a>
                    </div>
                </div>
            </li>


            <li class="nav-item">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#navTools"
                    aria-expanded="true" aria-controls="navCollapseSixth">
                    <i class="fas fa-clipboard-list"></i>
                    <span>Tools</span>
                </a>
                <div id="navTools" class="collapse" aria-labelledby="headingUtilities"
                    data-parent="#accordionSidebar">
        <div class="bg-white py-2 collapse-inner rounded">
            <a class="collapse-item" href="{{ URL::to('tools/prepare-product-import')}}">Import Products</a>
            <a class="collapse-item" href="{{ URL::to('tools/view-imported-products')}}">View Import List</a>
                     </div>
                </div>
            </li>




            <!-- Divider -->
            <hr class="sidebar-divider d-none d-md-block">

            <!-- Sidebar Toggler (Sidebar) -->
            <div class="text-center d-none d-md-inline">
                <button class="rounded-circle border-0" id="sidebarToggle"></button>
            </div>



        </ul>
