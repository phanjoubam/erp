@extends('layouts.erp_theme_03')
@section('content')


<?php
$order_info = $data['order_info'];  

$aitem = $data['active_agents'];  

$agent = $data['agent_info'];   
$order_no =$order_info->id ; 

$total_amount_to_clear = 0;
$total_commission=0;
?> 



<div class="row"> 

 @if( $order_info->milestone != "" && $order_info->milestone != null  )
 
 <div class="col-md-12">

  <p class='alert alert-success h2'><i class='fa fa-trophy  '></i>  {{ $order_info->milestone }}</p>

</div>  
@endif


<div class="col-md-4">    
 <div class="card panel-default">
   <div class="card-header"> 
     <h5 class="card-category">Order # {{ $order_info->id  }} 
      @switch($order_info->book_status)

      @case("new")
      <span class='badge badge-primary'>New</span>
      @break

      @case("confirmed")
      <span class='badge badge-info'>Confirmed</span>
      @break


      @case("order_packed")
      <span class='badge badge-info'>Order Packed</span>
      @break

      @case("package_picked_up")
      <span class='badge badge-info'>Package Picked Up</span>
      @break

      @case("pickup_did_not_come")
      <span class='badge badge-warning'>Pickup Didn't Come</span>
      @break

      @case("in_route")
      <span class='badge badge-success'>In Route</span>
      @break

      @case("completed")
      <span class='badge badge-success'>Completed</span>
      @break

      @case("delivered")
      <span class='badge badge-success'>Delivered</span>
      @break

      @case("delivery_scheduled")
      <span class='badge badge-success'>Delivery Scheduled</span>
      @break 
      @case("canceled")
      <span class='badge badge-danger'>Cancelled</span>
      @break 
      @endswitch  
    </h5>  
  </div>
  <div class="card-body">
   <div class='row'  >
    <div class='col-md-12'> 
      <p>
        Ordere Date <span class='badge badge-primary'>{{ date('d-m-Y H:i:s', strtotime( $order_info->book_date)) }}</span> 
        Service Date <span class='badge badge-success'>{{ date('d-m-Y', strtotime( $order_info->service_date)) }}</span>
        <hr/>  
        <span>Order is from </span> 

        @if(  strcasecmp($order_info->source,"business") == 0 || 
        strcasecmp($order_info->source,"merchant") == 0    )
        <span class='badge badge-info float-right'>Merchant</span>
        @else 
        <span class='badge badge-primary float-right'>bookTou</span>
        <?php 
        $percent = $order_info->commission / 100;

        $total_commission = $percent * $order_info->total_amount;
        ?>

        @endif

        <hr/>

        <span>Paid</span> 

        @if( strcasecmp($order_info->pay_mode,"cash") == 0    )
        <span class='badge badge-warning'>CASH</span>
        @else 
        <span class='badge badge-success'>ONLINE</span>
        @endif

        to

        @if( strcasecmp($order_info->payment_target,"business") == 0 || 
        strcasecmp($order_info->payment_target,"merchant") == 0   )
        <span class='badge badge-danger'>Merchant</span>
        @else 
        <span class='badge badge-success'>bookTou</span>
        @endif
        <hr/> 
        Merchat Cost: <span class='badge badge-info float-right'>{{ number_format( $order_info->total_amount,2) }}</span>
        <hr/> 
        Packaging Cost: <span class='badge badge-primary float-right'>{{ number_format( $order_info->packaging_cost,2) }}</span> 
        <hr/>
        bookTou Service Fee: <span class='badge badge-primary float-right'>{{  number_format( $order_info->service_fee,2) }}</span>
        <hr/> 
        bookTou Commission: <span class='badge badge-primary float-right'>{{  number_format( $total_commission,2) }}</span>
        <hr/>

        Amount to clear: <span class='badge badge-success float-right'>{{ number_format(  
         $total_commission + $order_info->service_fee + $order_info->total_amount + $order_info->packaging_cost,2) }}</span>
         <hr/>  
         <strong>Order Details</strong><br/>
         @if($order_info->pickup_details !="")
         {{ $order_info->pickup_details}}
         @else 
         No Order details Provided.
         @endif 
       </p> 
     </div> 
   </div>
 </div>



</div> 

</div>


<div class="col-md-4"> 

  <div class="card panel-default">
  <div class="card-header">
    <h5 class="card-category">Pickup from</h5>
    <h4 class="card-title">{{$order_info->pickup_name}}</h4>
  </div>
  <div class="card-body"> 

   <p>{{$order_info->pickup_address}} 
   {{$order_info->pickup_landmark}}<br/>


   <br/>
   <i class='fa fa-phone'></i> {{$order_info->pickup_phone}}</p> 
 </div> 
</div> 
<div class="card panel-default mt-3">
  <div class="card-header">
    <h5 class="card-category">Drop to</h5>
    <h4 class="card-title">{{$order_info->drop_name}} </h4>
  </div> 
  <div class="card-body"> 
   <p>Delivery Address:<br/>
    {{$order_info->drop_address}}<br/>
    {{$order_info->drop_landmark}}<br/>  
    <i class='fa fa-phone'></i> {{$order_info->drop_phone}}</p> 
  </div> 
</div>  

</div>

<div class="col-md-4">
  <div class="card panel-default">
  <div class="card-header">
    <h5 class="card-title">Select a agent to deliver order:</h5>
               @if( $order_info->book_status == "new" ||  $order_info->book_status == "confirmed" ||   $order_info->book_status == 'order_packed' )
               
                <div class="input-group mb-3"> 

                    <select   class='form-control  ' id="aid_{{$order_info->id }}"  name='agents[]' >
                       @foreach($data['active_agents'] as $aitem) 
                      @if($aitem->isFree == "yes" )
                        <option value='{{ $aitem->id }}' >{{ $aitem->nameWithTask }}</option>
                      @endif 
                      @endforeach
                    </select>
                    <div class="input-group-append">
                    <button class="btn btn-primary agent-assign" data-pnd='{{$order_info->id}}' data-agent='{{$aitem->id}}' >Assign</button>
                  </div> 
                </div> 
         @endif

  </div>
  <div class="card-body"> 

   
 </div> 
</div> 
 <div class="card panel-default mt-3">
   <div class="card-header">
    <h5 class="card-category">Cash Memo/Receipt</h5> 
  </div>
  <div class="card-body text-center">  
   @if($order_info->order_receipt != null)      
   
   <a data-src="{{ $order_info->order_receipt }}"  class="btn btn-link btnViewRcpt" href='#'  >
    <img src="{{  $order_info->order_receipt }}" width='100' class="img-fluid" alt="Responsive image">
    <br/>
    <br/>
    
  </a>
  <small>Click on the image enlarge</small>
  @else 
  <p class='alert alert-info'>No order receipt uploaded.</p>
  @endif
</div>
</div>
<div class="card panel-default mt-3">
 <div class="card-header">
  <h5 class="card-category">Remarks</h5> 
</div>
<div class="card-body  text-center">  

  @if($order_info->agent_remarks != null) 
  <table>
    <tr> 
      <td>
        <span class='badge badge-danger'>Agent Remarks</span><br/>
        {{  $order_info->agent_remarks }}</td> 
      </tr>
    </table> 
    @endif


    @if(isset( $data['remarks'] ) && count($data['remarks']) > 0)
    <table>
      @foreach($data['remarks'] as $rem)
      <tr> 
        <td>
          <span class='badge badge-danger'>{{ $rem->rem_type}}</span><br/>
          {{ $rem->remarks}}</td> 
        </tr>
        @endforeach
      </table> 
      @else 
      <p class='alert alert-info'>No remarks recorded.</p>
      @endif 
      
    </div>
  </div>
</div>  
</div> 


<div class="modal hide fade modal_processing" id='modal_processing'   role="dialog">
  <div class="modal-dialog " role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title ">Processing Order</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body text-center"> 
        <div class='loading'>
        </div> 
      </div>
      <div class="modal-footer">

        <button type="button" class="btn btn-secondary btn_action" data-action='non' data-dismiss="modal">Close</button> 
      </div>
    </div>
  </div>
</div>
@endsection
@section('script')
<script>
// var siteurl =  "//localhost/erp/" ;
// var ap =  "api/" ;

$(document).on("click", ".agent-assign", function()
{
    var pnd_id  = $(this).attr("data-pnd");
    var agent_id =$("#aid_" + pnd_id ).val()  ;
  console.log (pnd_id)
 console.log (agent_id)
    var json = {}; 
    json['pnd_id'] = pnd_id;
    json['agent_id'] =  agent_id ; 
    json['otype'] =  'pnd' ; 
    
    
    $('#modal_processing .loading').html("<img   src='" + siteurl + "/public/assets/image/processing.gif' alt='Loading ...' />");

    $('#modal_processing').modal('show');
    $.ajax({
      type: 'post',
      // url: siteurl+ap + "v3/web/customer-care/orders/pnd-assign-request" ,
      url: api + "v3/web/customer-care/orders/pnd-assign-request",
      data: json,
      success: function(data)
      {
        
        data = $.parseJSON(data);  
        $('#modal_processing .loading').html( data.detailed_msg); 

        if(data.status_code == 7001)
        {
           $('.btn_action').attr("data-action", "refresh");
        }

      },
      error: function( xhr, status, errorThrown) 
      { 
        console.log(xhr);  
      } 

    });

});


  $(document).on("click", ".showremmodal", function()
  {
    $("#key3").val($(this).attr("data-key"));
    $(".modalrem").modal("show")

  }); 

  $(document).on("click", ".btnpoppaybox", function()
  {
    $("#key4").val($(this).attr("data-key"));
    $(".modalpayment").modal("show")

  }); 



  $(document).on("click", ".btnchangestatus", function()
  {
    $("#key5").val($(this).attr("data-key"));
    $(".modalstatus").modal("show")

  }); 


  $(document).on("click", ".btnRemoveAgent", function()
  {
    $("#key2").val($(this).attr("data-key"));
    $("#modalConfDelAgent").modal("show")

  });



  $(document).on("click", ".showmodal", function()
  {
    var widget = $(this).attr("data-widget");
    var key = $(this).attr("data-key");
    $("#key").val(key);
    $("#widget-" + widget ).modal("show") ;

  });


  $(document).on("click", ".btnViewRcpt", function()
  {
    var img = $(this).attr("data-src");
    $("#imgreceipt").attr("src", img );
    $("#modalViewReceipt").modal("show");

  });




  $(function() {
    $('.calendar').pignoseCalendar( 
    {
      format: 'DD-MM-YYYY' 
    });
  });

</script>


@endsection

