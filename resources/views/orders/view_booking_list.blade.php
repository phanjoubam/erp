<div class="row" >  
   {{csrf_field()}}  
     <div class="col-md-12"> 
                   
                               @if( count($orders ) > 0 ) 
                               <div class='card'>
                               <div class="card-body">
                               <table class="table table-striped"  >
                                    <thead>
                                        <tr>  
                                            <th width='100px'>Order #</th>
                                            <th width='100px'>Booking Date</th> 
                                            <th width='100px'>Service Time</th>
                                            <th width='200px'>Services</th>
                                            <th width='200px'>Customer</th>
                                            <th width='200px'>Address</th>
                                            <th width='100px'>Total Cost</th>  
                                            <th width='100px'>Payment Type</th>
                                            <th width='200px' class='text-center'>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                 </table>   
                                
                               <div id='sortable'> 
                               @foreach($orders as $item) 
                               <div class='ui-state-default mt-2'> 
                                <table class="table table-striped"  > 
                                    <tbody> 
                                        <tr  > 
                                          <td class="text-left" width='100px'>
                                            <a href="{{ URL::to('/orders/pnd/view-details') }}/{{  $item->id  }}" target='_blank'>
                                              {{$item->id }}
                                            </a> 
                                          </td>
                                          <td width='100px'>{{date('d-m-Y', strtotime( $item->book_date)) }}</td> 
                                          @foreach($booked_services as $bookeditem)
                                            @if($bookeditem->book_id == $item->id )
                                           
                                            <td width='100px' ><span class="badge badge-success">{{ date('h:i a', strtotime( $bookeditem->service_time ))  }}</span></td> 
                                            <td width='200px' class="text-left">{{   $bookeditem->service_name  }}</td>
                                              @break
                                            @endif 
                                          @endforeach
                                          
                                          <td width='200px' class="text-left">
                                          @foreach($customers as $customer)
                                            @if( $customer->id  == $item->book_by )
                                              {{  $customer->fullname  }} 
                                              @break
                                            @endif 
                                          @endforeach
                                          </td>
                                          <td width='200px' class="text-left">{{ $item->address == "business" ? "Business Location" :  $item->address  }}</td>
                                          <td width='100px' >{{ $item->total_cost }}</td>
                                          <td width='100px' class="text-left"><span class='badge badge-primary'>{{ $item->pay_mode  }}</span></td> 

                                          <td width='200px' class="text-center">
                                          

                                          <div class="btn-group" role="group" aria-label="Basic example">
                                            <button type="button" class="btn btn-info redirect" data-key="{{ $item->id   }}" data-pgc='2' ><i class="fa fa-info-circle"></i></button>
                                            <button type="button" class="btn btn-success redirect" data-key="{{ $item->id   }}" data-pgc='1' ><i class="fa fa-file-invoice"></i></button>

                                        <button type="button" class="btn btn-danger delete"  
                                                data-key="{{$item->id}}">

                                              <i class="fa fa-trash"></i>
                                              </button>

 

 

                                          </div>
                                          

                                          </td> 


                                        </tr>
                                        </tbody>
                                </table>
                            </div>  
                            @endforeach
                            </div>

                            </div>
                                 </div>

                                 @else 
                                <p class='alert alert-info'>No New signup today.</p>
                                @endif 

                           
                   
     </div>
 
 </div>
