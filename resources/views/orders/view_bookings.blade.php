@extends('layouts.erp_theme_03')
@section('content')
 
<?php  
    $cdn_url =   $_ENV['APP_CDN'] ;  
    $cdn_path =   $_ENV['APP_CDN_PATH'] ;  
     
    $total_orders = count( $orders); 
    $completed = $active =0;
    foreach($orders as $item)
    {
      switch($item->book_status)
      {
        case  "completed":
          $completed++;  
          break;
        case  "new":
        case  "confirmed":
        case  "in_queue":
        case  "order_packed":
        case  "pickup_did_not_come":
        case  "in_route":
        case  "package_picked_up":
        case  "delivery_scheduled":
        case  "package_picked_up":
            $active++;
            break; 

      }

    } 

?>  
 
<div class="">
<div class="row mt-12">
<div class="col-md-12 offset-md-12">
<div class="card mt-2 panel-default">
  <form method="get" action="{{action('Erp\ErpToolsController@manageBooking')}}" class="form-inline">
    <div class="card-body">
      <div class="row">
        <div class="col-md-6">
          <h5>Search Booking Details:</h5>
        </div>
        <div class="col-md-6 text-right">
          <div class="form-row">

          <div class="form-group">
          <input type="date" name="filter_date" class="form-control form-control-sm  my-1 mr-sm-4">
          </div>

          <div class="form-group">
          
          <select name="status_code" class="form-control form-control-sm  my-1 mr-sm-4">
            <option value="">Select Booking Status</option>
            <option value="new">New</option>
            <option value="confirmed">Confirmed</option>
            <option value="engaged">Engaged</option>
            <option value="in_queue">In Queue</option>
            <option value="completed">Completed</option>
            <option value="order_packed">Order Packed</option>
            <option value="in_route">In Route</option>
            <option value="delivered">Delivered</option>
            <option value="returned">Returned</option>
            <option value="canceled">Canceled</option>
            <option value="no_show">No Show</option>

          </select>
          </div>
          <div class="form-group">
            <button type="submit" class="btn btn-primary">Search</button>

          </div>

          </div>
        </div>        
      </div>
    </div>
    </form>
</div>
</div>
</div>
</div>




 <div id="divResult">
   <div class="row">    
     <div class="col-md-12"> 
                   
                               @if( count($orders ) > 0 ) 
                               <div class='card'>
                               <div class="card-body">
                               <table class="table table-striped"  >
                                    <thead>
                                        <tr>  
                                            <th width='100px'>Order #</th>
                                            <th width='100px'>Booking Date</th> 
                                            <th width='100px'>Service Time</th>
                                            <th width='200px'>Services</th>
                                            <th width='200px'>Customer</th>
                                            <th width='200px'>Address</th>
                                            <th width='100px'>Total Cost</th>  
                                            <th width='100px'>Payment Type</th>
                                            <th width='200px' class='text-center'>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                 </table>   
                                 
                               @foreach($orders as $item) 
                               <div class='ui-state-default mt-2'> 
                                <table class="table table-striped"  > 
                                    <tbody> 
                                        <tr  > 
                                          <td class="text-left" width='100px'>
                                            <a href="{{ URL::to('/order/view-details') }}/{{  $item->id  }}" target='_blank'>
                                              {{$item->id }}
                                            </a> 
                                          </td>
                                          <td width='100px'>{{date('d-m-Y', strtotime( $item->service_date )) }}</td> 
                                          @foreach($booked_services as $bookeditem)
                                            @if($bookeditem->book_id == $item->id )
                                           
                                            <td width='100px' ><span class="badge badge-success">{{ date('h:i a', strtotime( $bookeditem->service_time ))  }}</span></td> 
                                            <td width='200px' class="text-left">{{   $bookeditem->service_name  }}</td>
                                              @break
                                            @endif 
                                          @endforeach
                                          
                                          <td width='200px' class="text-left">
                                          @foreach($customers as $customer)
                                            @if( $customer->id  == $item->book_by )
                                              {{  $customer->fullname  }} 
                                              @break
                                            @endif 
                                          @endforeach
                                          </td>
                                          <td width='200px' class="text-left">{{ $item->address == "business" ? "Business Location" :  $item->address  }}</td>
                                          <td width='100px' >{{ $item->total_cost }}</td>
                                          <td width='100px' class="text-left"><span class='badge badge-primary'>{{ $item->pay_mode  }}</span></td> 

                                          <td width='200px' class="text-center">
                                          <div class="btn-group" role="group" aria-label="Basic example">
                                            
                                           
                                            <a class="btn btn-info " href="{{ URL::to('/order/view-details') }}/{{  $item->id  }}" target='_blank'>
                                              <i class="fa fa-info-circle"></i>
                                            </a> 

                                            <button type="button" class="btn btn-warning btn-process" data-widget="1"  data-key="{{ $item->id   }}" data-pgc='1' >
                                              <i class="fa fa-cog"></i></button>

                                            <button type="button" class="btn btn-success redirect" data-key="{{ $item->id   }}" data-pgc='1' ><i class="fa fa-file-invoice"></i></button>
                                             
                                             <button type="button" class="btn btn-danger delete"  
                                                data-key="{{$item->id}}">

                                              <i class="fa fa-trash"></i>
                                              </button>

                                          </div>
                                          </td> 


                                        </tr>
                                        </tbody>
                                </table>
                            </div>  
                            @endforeach
                        

                            </div>
                                 </div>

                                 @else 
                                <p class='alert alert-info'>No New Bookings.</p>
                                @endif 
 
                   
     </div>
 
 </div>


</div>




 <form action="{{ action('Erp\ErpToolsController@updateBookingStatus') }}" method="post">
 {{  @csrf_field() }}
 <div class="modal" id="wg1" tabindex="-1">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Confirm Booking Progress Status</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
  
      <div class="form-group row">
      <label for="status" class="col-sm-12 col-md-4 col-form-label">Booking Status</label>
      <div class="col-sm-12 col-md-8">
      <select id="status" name='status' class="form-control">
      <option value='engaged'>Engage</option>
        <option value='cancelled'>Cancelled</option>
        <option value='no_show'>No Show</option>
        <option value='completed'>Completed</option> 
      </select>
    </div>

  
  </div> 

      </div>
      <div class="modal-footer">
      <input type='hidden' value="" id="key1" name="bookingno" />
      <button type="submit" class="btn btn-primary">Save</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      
      </div>
    </div>
  </div>
</div>
</form>


<!-- Modal HTML -->
 <div id="showModal" class="modal fade">
  <div class="modal-dialog modal-confirm">
    <div class="modal-content">
      <div class="modal-header flex-column text-center">
        <h4 class="modal-title w-100 text-center"><i class="fa fa-trash text-center"></i> Are you sure? </h4>

                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      </div>
      <div class="modal-body">
        <p>Do you really want to delete these records? This process cannot be undone.</p>
      </div>
      <div class="modal-footer justify-content-center">
        <button type="button" class="btn btn-secondary" data-dismiss="modal" id="deleteCancel">Cancel</button>
        <input type="hidden" id="deletebookingid">
        <button type="button" class="btn btn-danger" data-dismiss="modal" id="deleteConfirm">Delete</button>
      </div>
    </div>
  </div>
</div> 
<!-- end modal html -->
 
@endsection


@section("script")

<script>
  $( function() {
    $( "#sortable" ).sortable();
    $( "#sortable" ).disableSelection();
  } );

  $(document).on("click", ".btn-process", function(){ 
    
    var widgetno = $(this).attr("data-widget");
    $("#key" + widgetno).val(    $(this).attr("data-key") );
    $("#wg" + widgetno).modal('show');

 });


 $(document).on("click", ".delete", function(){ 
    
    var key = $(this).attr("data-key");

    $('#deletebookingid').val(key);

    $('#showModal').modal('show'); 

 });
  
 
 $("#deleteConfirm").on("click", function(){

    var key = $('#deletebookingid').val();
    $.ajax({

        type: 'get',
        url : 'remove-bookings',
        data: { 
          'code': key
      },
        dataType: 'json',
        contentType: 'application/json',
        success: function (response) {
           
            if (response.success) {
                $("#divResult").empty().append(response.html);
              }
            else {
                 alert("Unable to perform action");
            }
        }
    });


  });
  
 

 

    
 

  </script>

 @endsection

 