@extends('layouts.erp_theme_03')
@section('content')
   @include('submenu.pnd_sub_menu') 

   <div class="row">
     <div class="col-md-12"> 

       @if (session('err_msg'))  
       <div class="alert alert-info">
        {{ session('err_msg') }}
        </div> 
      @endif
   

<div class='row'>  
    <?php
      $i=1;
      $date2  = new \DateTime( date('Y-m-d H:i:s') );
      foreach ($data['results'] as $item)
      {
        $date1 = new \DateTime( $item->book_date ); 
        $interval = $date1->diff($date2); 
        $order_age =  $interval->format('%a days %H hrs  %i mins');   
      ?> 


  <div class='col-sm-12 col-md-3'> 
 <div class="card">
  <div class="card-body">   

   @if($item->order_receipt != null)  
      <div class="dropdown ddright">
            <a class="btn btn-primary btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" 
            aria-expanded="false">
            <i class="fa fa-cog "></i>
            </a> 
              <ul class="dropdown-menu dropdown-user dropdown-menu-right">  
                      <li><a data-src="{{  $item->order_receipt }}"  class="dropdown-item btn btn-link btnViewRcpt" href='#'  >View Receipt</a></li>     
              </ul>
       </div>
  @endif

     <div class='p-2' <?php 
          if(  $item->book_status == "delivered"  ) 
            echo 'style="background-color:#ff6600;color:#fff;"' ;
          else  
                echo 'style="background-color: #ff6600;color:#fff"' ;
            ?> >
      <strong>Order #</strong> <a href="{{ URL::to('/admin/customer-care/pnd-order/view-details') }}/{{ $item->id }}" target='_blank'><span class="badge badge-primary badge-pill">{{$item->id}}</span></a> 
      <br/>
      <strong>Order Source</strong><br/>  
      <span scope="col">Business/Seller</span><br/>
      {{$item->name}} 
     </div> 
 
    <div class='p-2'>
      <strong>Delivery Location Information</strong><br/>
      {{$item->fullname }}<br/>
      {{$item->address}} 
    </div> 
 


 <hr/>
      <ul class="list-group">  
      <li class="list-group-item d-flex justify-content-between align-items-center">
           Service/Delivery Fee
          <span class="badge badge-primary badge-pill">{{ $item->service_fee }}</span>
      </li> 
      <li class="list-group-item d-flex justify-content-between align-items-center">
           Merchant Cost
          <span class="badge badge-primary badge-pill">{{ $item->total_amount    }}</span>
      </li>
      <li class="list-group-item d-flex justify-content-between align-items-center">
           Status
          <span class="badge badge-primary badge-pill">
            @switch($item->book_status)
              @case("new")
                New
              @break
              @case("confirmed")
                Confirmed
              @break
                @case("order_packed")
                Order Packed
                      @break

                      @case("package_picked_up")
                      Package Picked Up
                      @break

                       @case("pickup_did_not_come")
                       Pickup Didn't Come
                      @break

                       @case("in_route")
                       In Route
                      @break

                       @case("completed")
                       Completed
                      @break

                      @case("delivered")
                      Delivered
                      @break

                       @case("delivery_scheduled")
                       Delivery Scheduled
                      @break 
                      @endswitch
                    </span>
      </li>

      <li class="list-group-item d-flex justify-content-between align-items-center">
        Payment Mode
        <span class="badge badge-primary badge-pill">{{ $item->pay_mode }}</span>
      </li>

      <li class="list-group-item d-flex justify-content-between align-items-center">
        <strong>Order Remarks</strong> 
      </li>

      <li class="list-group-item d-flex justify-content-between align-items-center">
         @if($item->pickup_details !=  "") 
            {{ $item->pickup_details }}
          @else 
            Not provided
          @endif 
      </li>


      <li class="list-group-item d-flex justify-content-between align-items-center">
        <strong>Delivery Agent Information</strong>
      </li>

      
        @if( $item->book_status == "new" ||  $item->book_status == "confirmed" ||   $item->book_status == 'order_packed' ) 
        <li class="list-group-item d-flex justify-content-between align-items-center">
          <form>
          <div class="form-row">
            <div class="form-group col-md-10"> 
          <select   class='form-control form-control-sm' id="aid_{{$item->id }}" name='agents[]' >
                              @foreach($data['all_agents'] as $aitem) 
                                 @if($aitem->isFree == "yes" )
                                  <option value='{{ $aitem->id }}' data-img="{{ $aitem->profile_photo }}"  >{{ $aitem->nameWithTask }}</option>
                                @endif 
                             @endforeach 
                          </select>
          </div>
<div class="form-group col-md-2">
  <button class='btn btn-secondary btn-xs showTaskList' data-cid="aid_{{$item->id }}"  ><i class='fa fa-eye'></i></button>
</div>
</div>
</form>

      </li>
      
      <li class="list-group-item d-flex justify-content-between align-items-center">
        <button class="btn btn-primary btn-assign" data-oid="{{$item->id}}" >Assign Agent</button>
      </li>

 
     
        
        @else
         <li class="list-group-item d-flex justify-content-between align-items-center">
          @foreach($data['delivery_orders_list'] as $ditem) 
              @if( $item->id  == $ditem->order_no )
                {{ $ditem->fullname  }}  
                @break;
                @endif  
            @endforeach

         </li>   
        @endif 

    </ul>  
</div>
</div>
</div>
<?php
  $i++; 
}
?>

</div>
             </div>
              </div>
 </div>
</div>
</div>
</div>  
  

  


<!-- edit order -->
 
<div class="modal" id='modalViewReceipt' tabindex="-1">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">View Order Receipt</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <img class="img-fluid" width='600'  src="" id='imgreceipt' alt='Order Receipt' /> 
      </div>
      <div class="modal-footer">

        <input type="hidden" id="erono" name='erono'> 
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> 
      </div>
    </div>
  </div>
</div> 


@endsection

@section("script")

<script>

 

$(document).on("click", ".btnViewRcpt", function()
{
    var img = $(this).attr("data-src");
    $("#imgreceipt").attr("src", img );
    $("#modalViewReceipt").modal("show");

});
 
 
  

$(function() {
    $('.calendar').pignoseCalendar( 
    {
      format: 'DD-MM-YYYY' 
    });
});

</script> 

@endsection 