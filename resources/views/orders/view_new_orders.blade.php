@extends('layouts.erp_theme_03')
@section('content')
 



   <div class="row">
     <div class="col-md-12">
 
     <div class="board">
 

               <div class="panel panel-primary">

   <div class="card-title"> 
    <div class="row">
  <div class="col-md-4">

    <h5>New Orders</h5>
   </div>
 

  <div class="col-md-8">
 <form class="form-inline" method="post" action="{{  action('Erp\ErpToolsController@viewNewOrders') }}">

            {{ csrf_field() }}
             <label class="my-1 mr-2" for="inlineFormCustomSelectPref">Date:</label> 

             <input type="text" class='form-control form-control-sm custom-select my-1 mr-sm-2 calendar'  
             value="{{ date('d-m-Y', strtotime($data['last_keyword_date']))  }}" name='filter_date' />

              
              <label class="my-1 mr-2" for="inlineFormCustomSelectPref">Status:</label> 
                <select   class='form-control form-control-sm custom-select my-1 mr-sm-2 '   name='filter_status' >
                  <option value='-1'>All</option> 
                    <option value='confirmed' <?php if($data['last_keyword']  == "confirmed") echo "selected";  ?>>Confirmed</option> 
                      <option value='delivery_scheduled' <?php if($data['last_keyword']  == "delivery_scheduled") echo "selected";  ?>>Wating Agent Pickup</option>  
                    <option value='order_packed' <?php if($data['last_keyword']  == "order_packed") echo "selected";  ?>>Packed and Ready for delivery</option>    
                    <option value='in_route' <?php if($data['last_keyword'] == "in_route") echo "selected";  ?>>In Route</option>    

                </select>

                <button type="submit" class="btn btn-primary my-1" value='search' name='btn_search'>Search</button>
            </form>
      </div>
    </div>
  </div>


                 <div>
                  <table class="table" style='min-height: 30px;'>
                    <thead class="text-primary"> 
 
                  <tr style='font-size: 12px'> 
                  <th scope="col">Order No. #</th> 
                    <th scope="col">Customer</th>
                    <th scope="col">Time Elapsed</th> 
                    <th scope="col">Order Status</th>  
                    <th scope="col">Assigned To</th>
                    <th scope="col">Action</th>
                  </tr>
                </thead>

                     <tbody>
                <?php 

                  $i=1;

                  $date2  = new \DateTime( date('Y-m-d H:i:s') );
                
                foreach ($data['results'] as $item)
                {

                  $date1 = new \DateTime( $item->book_date ); 
                  $interval = $date1->diff($date2); 
                  $order_age =  $interval->format('%a days %H hrs  %i mins');  

                ?>
  
                  <tr id="tr{{$item->id }}">
                  <td>{{$item->id }}</td>  
                   <td> 
                  {{$item->fullname}}
                  </td>
                

                    <td>  {{ $order_age }} </td>

                    <td>

                      @switch($item->book_status)

                      @case("new")
                        <span class='badge badge-primary'>New</span>
                      @break

                      @case("confirmed")
                        <span class='badge badge-info'>Confirmed</span>
                      @break
 

                      @case("order_packed")
                        <span class='badge badge-info'>Order Packed</span>
                      @break

                      @case("package_picked_up")
                        <span class='badge badge-info'>Package Picked Up</span>
                      @break

                       @case("pickup_did_not_come")
                        <span class='badge badge-warning'>Pickup Didn't Come</span>
                      @break

                       @case("in_route")
                        <span class='badge badge-success'>In Route</span>
                      @break

                       @case("completed")
                        <span class='badge badge-success'>Completed</span>
                      @break

                      @case("delivered")
                        <span class='badge badge-success'>Delivered</span>
                      @break

                       @case("delivery_scheduled")
                        <span class='badge badge-success'>Delivery Scheduled</span>
                      @break 
                      @endswitch

                    </td>  
               


                       <td>
                      

                          @foreach($data['delivery_orders_list'] as $ditem) 
                                
                                 {{ $ditem->fullname  }}  
                              
                          @endforeach
            
 
                       </td>  
                                      
   
        
                    <td>
                       
                     <div class="dropdown">

                       <a class="btn btn-primary btn-sm btn-icon-only text-light dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          <i class="fa fa-cog "></i>
                     </a>


                      <ul class="dropdown-menu dropdown-user pull-right"> 
                      
                          <li><a  class="dropdown-item btn btn-link " href="{{ URL::to('/erp/order/view-details') }}/{{$item->id}}">View Order Details</a></li> 
                         

 
                           </ul>
                      </div>
                    </td>
   
                         </tr>
                         <?php
                          $i++; 
                       }

                          ?>
                </tbody>
                  </table>
             </div>
      </div>
 </div>
</div>
</div>

        

  



 
@endsection
