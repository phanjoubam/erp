@extends('layouts.erp_theme_03')
@section('content')

 
  <?php
    $order_info = $data['order_info'];  
    $agent = $data['agent_info'];   
    $order_no =$order_info->id ; 
  ?> 
 


 <div class="row"> 
  @if (session('err_msg'))
  <div class="col-md-12">
    <div class="alert alert-info">
  {{ session('err_msg') }}
  </div>
  </div>
@endif



   <div class="col-md-8"> 
 
     <div class="card panel-default mt-3">
           <div class="card-header">
              <div class="card-title"> 
                 <h5 class="card-category">Order # {{ $order_info->id  }} 
                @switch($order_info->book_status)

                      @case("new")
                        <span class='badge badge-primary'>New</span>
                      @break

                      @case("confirmed")
                        <span class='badge badge-info'>Confirmed</span>
                      @break
 

                      @case("order_packed")
                        <span class='badge badge-info'>Order Packed</span>
                      @break

                      @case("package_picked_up")
                        <span class='badge badge-info'>Package Picked Up</span>
                      @break

                       @case("pickup_did_not_come")
                        <span class='badge badge-warning'>Pickup Didn't Come</span>
                      @break

                       @case("in_route")
                        <span class='badge badge-success'>In Route</span>
                      @break

                       @case("completed")
                        <span class='badge badge-success'>Completed</span>
                      @break

                      @case("delivered")
                        <span class='badge badge-success'>Delivered</span>
                      @break

                       @case("delivery_scheduled")
                        <span class='badge badge-success'>Delivery Scheduled</span>
                      @break 
                      @endswitch

                      <span> Order Date: {{ date('d-m-Y H:i:s', strtotime( $order_info->book_date)) }}</span> 
                      OTP: <span class='badge badge-info'>{{ $order_info->otp }}</span> 
                      <span>Payment Mode:</span> 
          @if( strcasecmp($order_info->pay_mode,"cash") == 0    )
            <span class='badge badge-warning'>CASH</span>
          @else 
            <span class='badge badge-success'>ONLINE</span>
          @endif  



                    </h5> 
                </div>
            </div>
       <div class="card-body">
         <div class='row'  >
          <div class='col-md-6'> 
            @if($order_info->cust_remarks!="")
              {{ $order_info->cust_remarks}}
            @else 
              No Order Remark Provided.
            @endif
          </div>
          <div class='col-md-6'>
              @if($order_info->order_receipt != null)  
          <img src="{{  $order_info->order_receipt }}" width='100' class="img-fluid" alt="Responsive image">
          <a href='' ></a>
        @endif
          </div>
        </div>
              </div>
            </div>
 


 </div>


 <div class="col-md-4">    

            <div class="card panel-default mt-3">
              <div class="card-header">
                <h5 class="card-category">Shop/Business</h5>
                <h4 class="card-title">{{$order_info->businessName}}</h4>
                </div>
              <div class="card-body"> 
                 <p>{{$order_info->businessLocality}}</p> 
                  {{$order_info->businessLandmark}}<br/>
                  {{$order_info->businessCity}}<br/>
                  {{$order_info->businessState}} - {{ $order_info->businessPin}}<br/>
                  <i class='fa fa-phone'></i> {{$order_info->businessPhone}}</p> 
              </div> 
            </div> 

             <div class="card panel-default mt-3">
              <div class="card-header">
                <h5 class="card-category">Customer</h5>
                <h4 class="card-title">{{$order_info->fullname}} </h4>
               </div> 
              <div class="card-body"> 
                 <p>Delivery Address:<br/>
                  {{$order_info->address}}<br/>
                  {{$order_info->landmark}}<br/>  
                  <i class='fa fa-phone'></i> {{$order_info->phone}}</p> 
              </div> 
            </div>

            @if(isset($agent))
              <div class="card card-default">
              <div class="card-header">
                <h5 class="card-category">Delivery Agent</h5>
                <h4 class="card-title">{{$agent->deliveryAgentName}}</h4>
                </div>
              <div class="card-body"> 
                 <p>
                  <i class='fa fa-phone'></i> {{$agent->deliveryAgentPhone}}</p> 
              </div>
              <div class="card-footer">
                 <button class='btn btn-danger btn-sm btnRemoveAgent' data-key='{{ $order_info->id   }}'>Remove Agent</button>
              </div>
            </div>

            @endif


          </div>  

       </div> 
 
 


 


@endsection


@section('script')
 

<script>


  $(document).on("click", ".btnRemoveAgent", function()
{
  $("#key2").val($(this).attr("data-key"));
  $("#modalConfDelAgent").modal("show")

});


 
$(document).on("click", ".showmodal", function()
{
  var widget = $(this).attr("data-widget");
  var key = $(this).attr("data-key");
   $("#key").val(key);
  $("#widget-" + widget ).modal("show") ;

});





$(function() {
    $('.calendar').pignoseCalendar();
});

</script>


@endsection

   