@extends('layouts.erp_theme_03')
@section('content')
<?php 
$active_agent = $data['agents_status'];
$is_premium = $data['is_premium']; 
?>
<div class="row">
    <div class="col-md-8 "> 
     <div class="card">

      <div class="card-header">
          <div class="row">
             <div class="col-md-4"> <h5>Add New Pick-And-Drop Request</h5></div>
             <div class="col-md-8"> 
                @if (session('err_msg'))
                <div class="card">
                    <div class="alert alert-info p-0">
                        {{session('err_msg')}}
                    </div>
                </div>
                @endif 
            </div>
        </div>
    </div>
    <div class="card-body">

      <form action="{{ action('Erp\ErpToolsController@newPickAndDropRequest') }}"  method="post">
        {{ csrf_field() }}

        <div class="">
           <div class="drop-zone" style='padding: 10px;'> 
            <div class="form-row">

                <div class="form-group col-md-7">
                  <label for="pucname">Pickup Contact Name:</label>
                  <input type="text" class="form-control minutesInput" id="pucname" name='pucname' value="{{ $data['business']->name }}">
              </div> 

              <div class="form-group col-md-5">
                  <label for="pucphone">Pickup Contact Phone:</label>
                  <input type="text" class="form-control minutesInput" id="pucphone" name='pucphone' value="{{ $data['business']->phone_pri }}">
              </div>

              <div class="form-group col-md-12">
                <label for="puaddress">Pickup Address:</label>
                <input type="text" class="form-control minutesInput" id="puaddress" name='puaddress' placeholder="Delivery Address" value="{{ $data['business']->locality }}">
            </div>

            <div class="form-group col-md-12">
                <label for="pulandmark">Pickup Landmark (optional):</label>
                <input type="text" class="form-control minutesInput" id="pulandmark" name='pulandmark' placeholder="Nearest landmark to easily locate address" value="{{ $data['business']->landmark }}">
            </div>

        </div> 
    </div>

    <div class="drop-zone" style='background-color: #abc7fb; padding: 10px;'>
       <div class="form-row">
        <div class="form-group col-md-7">
          <label for="cname">Drop Contact Name:</label>
          <input type="text" class="form-control minutesInput" id="cname" name='cname'>
      </div> 

      <div class="form-group col-md-5">
          <label for="cphone">Drop Customer Phone:</label>
          <input type="text" class="form-control minutesInput" id="cphone" name='cphone'>
      </div>

      <div class="form-group col-md-12">
        <label for="address">Drop Address:</label>
        <input type="text" class="form-control minutesInput" id="address" name='address' placeholder="Delivery Address">
    </div>

    <div class="form-group col-md-12">
        <label for="landmark">Drop Landmark (optional):</label>
        <input type="text" class="form-control minutesInput" id="landmark" name='landmark' placeholder="Nearest landmark to easily locate address">
    </div>

</div>
</div>

<div class="drop-zone" style='padding: 10px;'>
  <div class="form-row">
   <div class="form-group col-md-3">
      <label for="servicedate">Service Date:</label>
      <input type="text" class="form-control calendar minutesInput" id="servicedate" name='servicedate'>
      <small class='red '>(date of delivery)</small>
  </div>

  <div class="form-group col-md-3">
     <label for="total">Order Cost:</label>
      <input type="number" min='0' step='0.1' class="form-control minutesInput" id="total" name='total'>
      <small class='red'>(total item cost)</small>
  </div>

  <div class="form-group col-md-3">
      <label for="total">Packaging:</label>
      <input type="number" min='0' step='0.1' value='0.00' class="form-control minutesInput" id="packcost" name='packcost'>
      <small class='red'>(if any)</small>
  </div>
  <div class="form-group col-md-3">
      <label for="inputState" class="form-label">Delivery Model</label>
  <select  class="form-control" type="text" name="delivery_model" id="delivery_model">
    @if($is_premium =="yes") 
    <option value="assured delivery ">Assured delivery </option>
    @else 
    <option value="standard delivery">Standard delivery</option>
    <option value="express delivery">Express delivery</option>
    @endif             
</select>
  </div>

</div>
 <div class="form-row">
   <div class="form-group col-md-3">
      <label for="etotal">Payment Mode:</label>
  <br/>
  <div class="custom-control custom-radio custom-control-inline">
    <input type="radio" checked id="paymode1" name="paymode" class="custom-control-input" value='CASH'>
    <label class="custom-control-label" for="paymode1">COD</label>
</div>
<div class="custom-control custom-radio custom-control-inline">
    <input type="radio" id="paymode2" name="paymode" class="custom-control-input" value='ONLINE'>
    <label class="custom-control-label" for="paymode2">ONLINE</label>
</div>
  </div>

  <div class="form-group col-md-3">
      <label for="sfee">Delivery agent fee:</label>
      <input type="number"  class="form-control minutesInput" id="num1" name='delagentfee'>      
  </div>
  <div class="form-group col-md-3">
      <label for="total">Convince fee:</label>
      <input type="number" class="form-control minutesInput" id="num2" name='convincefee'>      
  </div>
  <div class="form-group col-md-3">
      <label for="total">Tax:</label>
      <input type="text" class="form-control " id="tax" name='tax' readonly>     
  </div>

</div>

<div class="form-row">
   <div class="form-group col-md-12">
      <label for="remarks">Remarks (if any):</label>
      <textarea  class="form-control" id="remarks" rows='4' name='remarks'></textarea>
  </div>   
</div> 
</div>
</div>
<div class="modal-footer">
  <input type="hidden" name='staff' value="{{ Session::get('_full_name') }}">
  <input type='hidden' name='bin' id='bin' value="{{ $data['business']->id  }}" />
        
  <button type="submit" name='btnsave' value='save' class="btn btn-primary">Save changes</button>
  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> 
</div>

</form>
</div>
</div>
</div>

<div class="row">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">           

   <div class="card card-default ">
      <div class="card-header">
        <h5 class="card-category">Availability Agent</h5> 
    </div>
    <div class="card-body"> 
     <div class="">
       <h6 class="card-category">
          Active agent available:

          @if($active_agent ==0)
          <span class='badge badge-warning badge-pill'>All agent are busy!</span>
          @else 
          <span class='badge badge-info badge-pill'> Available</span>
          @endif                 
      </h6>
  </div>
</div>
</div>
</div> 
</div>
</div>

@endsection 
@section("script")
<script>

  $(document).ready(function() {
    sum();
    $("#num1, #num2").on("keydown keyup", function() {
        sum();
    });
});

function sum() {
            var num1 = document.getElementById('num1').value;
            var num2 = document.getElementById('num2').value;
      var result = parseInt(num1) + parseInt(num2);
      var result1 = Math.ceil(parseInt(result) * 0.18);          
        document.getElementById('tax').value = result1;
       
        }

    $(function() {
        $('.calendar').pignoseCalendar( 
        {
          format: 'DD-MM-YYYY' 
      });
    });

</script>


@endsection


