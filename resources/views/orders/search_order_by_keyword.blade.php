@extends('layouts.erp_theme_03')
@section('content')



<div class="row">
     <div class="col-md-12">
 
     <div class="card card-default">
           <div class="card-header">
              <div class="card-title"> 
               <div class="row">

                <div class="col-md-12">
                   <h3> 
                   	
                   	@if (session('err_msg'))
					    <div class="alert alert-info">
					        {{ session('err_msg') }}
					    </div>
					@endif

                   </h3>
                </div>

                 
            </div>
            </div>

            </div>
        </div>
    </div>
</div>




@endsection
@section("script")
 
@endsection 