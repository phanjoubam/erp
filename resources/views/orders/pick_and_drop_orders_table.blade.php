@extends('layouts.erp_theme_03')
@section('content')
   
   @include('submenu.pnd_sub_menu') 

   <div class="row">
     <div class="col-md-12">  
       @if (session('err_msg'))  
       <div class="alert alert-info">
        {{ session('err_msg') }}
        </div> 
      @endif
    </div>
  </div>
    <?php 

      $i=1;

      $date2  = new \DateTime( date('Y-m-d H:i:s') );
    
    foreach ($data['results'] as $item)
    {

      $date1 = new \DateTime( $item->book_date ); 
      $interval = $date1->diff($date2); 
      $order_age =  $interval->format('%a days %H hrs  %i mins');   
    ?>
 
   
<div class="p-2" style='background-color: #fff !important; margin-bottom: 20px;'>
<div class="row">

 <div class='col-sm-12 col-md-4 text-right'> 
   
      <ul class="list-group">  
        <li class="list-group-item d-flex justify-content-between align-items-center">  
        @if($item->orderType=="assist")
        <h3>{{ strtoupper( $item->orderType ) }} Order # <a href="{{ URL::to('/admin/customer-care/assist-order/view-details/') }}/{{$item->id}}" target='_blank'>
        <span class="badge badge-primary badge-pill display-5">{{$item->id}}</span></a>  
        </h3>
        @else
        <h3>{{ strtoupper( $item->orderType ) }} Order # <a href="{{ URL::to('/admin/customer-care/pnd-order/view-details/') }}/{{$item->id}}" target='_blank'>
        <span class="badge badge-primary badge-pill display-5">{{$item->id}}</span></a> 
        </h3>
        @endif 
        </li> 

        <li class="list-group-item d-flex justify-content-between align-items-center"
        <?php 
              if( $interval->format('%H') >=  1  ) 
                echo 'style="background-color:#ff6e6e;color:#fff;"' ;
              else 
                  if( $interval->format('%H') < 1 )
                    echo 'style="background-color: #67cafa;color:#fff"' ;
                       
                  ?>
                  >
           Ordered On
          <span class="badge badge-primary badge-pill">{{ date('d-m-Y', strtotime( $item->book_date )) }}</span>
      </li>


      <li class="list-group-item d-flex justify-content-between align-items-center">
           Delivery Date
          <span class="badge badge-primary badge-pill">{{ date('d-m-Y', strtotime( $item->service_date )) }}</span>
      </li>
      <li class="list-group-item d-flex justify-content-between align-items-center">
          bookTou Fee
          <span class="badge badge-primary badge-pill">{{number_format( $item->service_fee,2) }}</span>
      </li>
      <li class="list-group-item d-flex justify-content-between align-items-center">
           Item Cost
          <span class="badge badge-primary badge-pill">{{ number_format($item->total_amount,2)  }}</span>
      </li>
      <li class="list-group-item d-flex justify-content-between align-items-center">
           Packaging charges (if any)
          <span class="badge badge-primary badge-pill">{{ number_format($item->packaging_cost,2)  }}</span>
      </li>

      <li class="list-group-item d-flex justify-content-between align-items-center">
           Status
          <span class="badge badge-primary badge-pill">
            @switch($item->book_status)
              @case("new")
                New
              @break
              @case("confirmed")
                Confirmed
              @break
                @case("order_packed")
                Order Packed
                      @break

                      @case("package_picked_up")
                      Package Picked Up
                      @break

                       @case("pickup_did_not_come")
                       Pickup Didn't Come
                      @break

                       @case("in_route")
                       In Route
                      @break

                       @case("completed")
                       Completed
                      @break

                      @case("delivered")
                      Delivered
                      @break

                       @case("delivery_scheduled")
                       Delivery Scheduled
                      @break 
                      @endswitch
                    </span>
      </li>
      <li class="list-group-item d-flex justify-content-between align-items-center">
           Payment Type
          <span class="badge badge-info badge-pill">{{ $item->pay_mode  }}</span> to 
          @if( strcasecmp($item->payment_target,"business") == 0 || 
              strcasecmp($item->payment_target,"merchant") == 0   )
            <span class='badge badge-danger badge-pill'>Merchant</span>
          @else 
                <span class='badge badge-success badge-pill'>bookTou</span>
          @endif
      </li>
   
      <li class="list-group-item d-flex justify-content-between align-items-center">
           Order Source 

            @if($item->source == "business")
             <span class="badge badge-success badge-pill">Merchant</span>
            @else  
              @foreach($data['staffs'] as $staff_item)
                 @if( $staff_item->user_id == $item->staff_user_id )
                  <span class="badge badge-info badge-pill">{{ $staff_item->fullname }}</span>
                  @break
                 @endif
              @endforeach
            @endif 
      </li> 
    </ul>
 
</div>



  <div class='col-sm-12 col-md-4'>
    <div class="card">
     <div class="card-body">  
        <h3>Order Details</h3> 
        <hr/>
      <span>Time Elapse</span>  <span class="badge badge-primary badge-pill">{{ $order_age }}</span> 
      <a class='badge badge-info' target='_blank' href="{{ URL::to('/shopping/where-is-my-order') }}/{{ $item->tracking_session }}">Where is it?</a>
      <hr/> 
      <strong>Order Remarks</strong>
      <hr/>
      @if($item->pickup_details !=  "") 
        {{ $item->pickup_details }}
      @else 
        Not provided
      @endif  

  <hr/>
  @if($item->order_receipt != null) 
     <div class="media">
      <img src="{{  $item->order_receipt }}"  width='35' height='45' class="mr-3" alt="Receipt"> 
      <div class="media-body"> 
        <a data-src="{{  $item->order_receipt }}" class="btn btn-success btnViewRcpt" href="#">View Receipt</a>  
      </div>
    </div>  
  @endif
 

</div>

<div class="card-footer">  
  <button type='button' class="btn btn-primary btn-sm  btnedit"  
  data-type="{{$item->orderType}}"     
  data-requestby="{{$item->request_by}}"  
  data-receipt="{{$item->order_receipt}}"  
  data-ono="{{$item->id}}" 
  data-fee="{{$item->service_fee}}" 
  data-total="{{$item->total_amount}}" 
  data-date="{{ date('d-m-Y', strtotime( $item->service_date )) }}" 
  data-fullname="{{$item->fullname}}" 
  data-phone="{{$item->phone}}" 
  data-address="{{$item->address}}" 
  data-landmark="{{$item->landmark}}" 
  data-pay_mode="{{ $item->pay_mode}}" 
  data-paytarget="{{$item->payment_target}}"  
  data-remarks="{{ $item->cust_remarks }}"
  data-pickupdetails="{{ $item->pickup_details }}" 
  data-pfullname="{{$item->pickup_name}}" 
  data-pphone="{{$item->pickup_phone}}" 
  data-paddress="{{$item->pickup_address}}" 
  data-plandmark="{{$item->pickup_landmark}}" 
  data-packcost="{{$item->packaging_cost}}"
  data-deliverymodel="{{$item->delivery_model}}" 
    >Edit Order</button>
<button type='button' class="btn btn-info btn-sm showremmodal"  data-key="{{$item->id}}" >Add Remarks</button> 

@if( $item->book_status  == "delivery_scheduled" )
                 
@else

 <button type='button' class="btn btn-danger btn-sm  btn_deletePND" data-pnd-key="{{$item->id}}">Delete</button>                                  
@endif 
 
  </div>

</div>
</div>

  
  <div class='col-sm-12 col-md-4'>
    <div class="card">
        <div class='p-3'>
            <h4>Pickup & Drop Locations</h4>  
            <hr/>
       
       @if($item->orderType=="assist")       
        <strong>Pickup From:</strong><br/>   
        {{$item->pickup_name}}</br>
        {{ $item->pickup_address }}<br/>
        {{ $item->pickup_landmark }}<br/>
        <i class='fa fa-phone'></i> {{ $item->pickup_phone }}
        <hr/> 
     
    <strong>Drop To:</strong><br/>
    {{$item->drop_name }}<br/>
    {{$item->drop_address}}<br/> 
    {{$item->drop_landmark}}<br/> 
    <i class='fa fa-phone'></i> {{ $item->drop_phone }}
   
    @else
          
          <strong>Pickup From:</strong><br/>  
          <span scope="col">Business/Seller</span><br/>
          {{$item->name}}</br>
          {{ $item->pickup_address }} 
          <small><br/>
          <i class='fa fa-phone'></i> {{ $item->primaryPhone }}</small>  
          <hr/>
  
       
      <strong>Drop To:</strong><br/>
      {{$item->fullname }}<br/>
      {{$item->drop_address}}<br/> 
      {{$item->drop_landmark}}<br/> 
       <small><i class='fa fa-phone'></i> {{ $item->phone }}</small> 
     
    @endif
  
  </div>

    @foreach($data['banned_list'] as $banentry) 
     @if($banentry->phone == $item->phone && $banentry->flag != "none" ) 
       <div class='p-3'>
          <div  class='alert alert-info mt-2'>
            Cutomer is <span class='badge badge-danger'>{{ $banentry->flag }}</span>
          </div>
      </div>
      @break
    @endif
  @endforeach

  @if( $item->book_status == "new"   )  
   @foreach($data['requests_to_process'] as $reqitem) 
      @if($item->id == $reqitem->order_no )
      <div class='card '>
        <div class="card-body">
          <h5 class="card-title">Agent Requesting to handle order</h5>
            <span class='badge badge-primary'>{{ $reqitem->fullname }}</span> 
              <br/>
            <button class="btn btn-success btn-sm btnconfselfassign" 
            data-aid="{{$reqitem->id}}"  data-oid="{{$item->id}}" data-action="allow"><i class='fa fa-tick'></i> Allow</button>  
            <button class="btn btn-danger btn-sm btnconfselfassign" 
            data-aid="{{$reqitem->id}}" 
            data-oid="{{$item->id}}"  data-action="deny"><i class='fa fa-cross'></i> Deny</button> 
          </div>
      </div>
        @break
      @endif 
   @endforeach 
 @endif



  
   <div class='p-3'>
      
          @foreach($data['delivery_orders_list'] as $ditem) 
              @if( $item->id  == $ditem->order_no )
              <strong>Agent Assigned:</strong><br/>
                <p class='text-left'> 
                                 @if(  $ditem->agent_ready == "yes")
                                 <span class='badge badge-success white'>TASK ACCEPTED</span>  
                                  @else 
                                  <span class='badge badge-danger white'>NO ACTION</span>
                                 @endif
                                  </p>
                @break;
                @endif  
            @endforeach 
          
    </div>    
 

 

    </div>
  </div> 
</div> <!-- end of row -->

</div>

<?php
  $i++; 
}
?>

 

 
<div class="modal hide fade modal_cancel"   role="dialog">
  <div class="modal-dialog " role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title ">Confirm Order Cancellation</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body "> 
            <div class='confloading'>
        </div> 
        <div class="form-group"> 
    <textarea  placeholder='Reason for cancelling' class="form-control form-control-sm  " id="tareason" name="tareason" rows='5'></textarea>

  </div>
  

      </div>
      <div class="modal-footer">
        <input type='hidden' id='orderno' name="orderno" />
        <button type="button" class="btn btn-primary btnconfim" >Cancel</button> 
        <button type="button" class="btn btn-secondary" data-action='non' data-dismiss="modal">Close</button> 
      </div>
    </div>
  </div>
</div>
 
 

<div class="modal hide fade modal_processing" id='modal_processing'   role="dialog">
  <div class="modal-dialog " role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title ">Processing Order</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body text-center"> 
        <div class='loading'>
        </div> 
      </div>
      <div class="modal-footer">

        <button type="button" class="btn btn-secondary btn_action" data-action='non' data-dismiss="modal">Close</button> 
      </div>
    </div>
  </div>
</div>



<div class="modal fade" id="modalTaskList" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
  <img src='{{ URL::to("/public/assets/image/no-image.jpg") }}' id='apimg' width="40px" height="40px" class="img-rounded" style='margin-right:10px; display:inline-block'/>
<h5 class="modal-title" id="exampleModalLongTitle">Delivery Tasks for <strong id='span_anm'></strong></h5>
  

        
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="content_area">
         <div class='loading'>
        </div> 
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button> 
      </div>
    </div>
  </div>
</div>


<div class="modal" id='confirmOrder' tabindex="-1" role="dialog">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Confirm Customer Order By Call</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p class='alert alert-danger'>THIS ACTION CANNOT BE REVERSED!</p>
        <p>

          Have you called back the customer to confirm this order?
          <br/>

          If you haven't call back, pleaes do call and confirm first.
          <br/> 
        </p>
      </div>
      <div class="modal-footer"> 
        <span class='loading_span'></span>
        <button type="button" class="btn btn-success" id="btnsaveconfirmation" data-conf="" data-id="" >Save Changes</button> 
        <button type="button" class="btn btn-danger" id="closeconfmodal" data-dismiss="modal">Close</button> 
      </div>
    </div>
  </div>
</div>


  <form action="{{ action('Erp\ErpToolsController@newPickAndDropRequest') }}"  method="post">
    {{ csrf_field() }}
<div class="modal" id='modalneworder' tabindex="-1">
  <div class="modal-dialog ">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Add New Pick-And-Drop Request</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body-nopad">
 <div class="drop-zone" style='padding: 10px;'> 
    <div class="form-row">
    
    <div class="form-group col-md-7">
      <label for="pucname">Pickup Contact Name:</label>
      <input type="text" class="form-control" id="pucname" name='pucname' value="{{ $data['business']->name }}">
    </div> 

    <div class="form-group col-md-5">
      <label for="pucphone">Pickup Contact Phone:</label>
      <input type="text" class="form-control" id="pucphone" name='pucphone' value="{{ $data['business']->phone_pri }}">
    </div>

    <div class="form-group col-md-12">
        <label for="puaddress">Pickup Address:</label>
    <input type="text" class="form-control" id="puaddress" name='puaddress' placeholder="Delivery Address" value="{{ $data['business']->locality }}">
    </div>

<div class="form-group col-md-12">
        <label for="pulandmark">Pickup Landmark (optional):</label>
    <input type="text" class="form-control" id="pulandmark" name='pulandmark' placeholder="Nearest landmark to easily locate address" value="{{ $data['business']->landmark }}">
    </div>

  </div> 
</div>
 
 <div class="drop-zone" style='background-color: #abc7fb; padding: 10px;'>
 <div class="form-row">
    <div class="form-group col-md-7">
      <label for="cname">Drop Contact Name:</label>
      <input type="text" class="form-control" id="cname" name='cname'>
    </div> 
    
    <div class="form-group col-md-5">
      <label for="cphone">Drop Customer Phone:</label>
      <input type="text" class="form-control" id="cphone" name='cphone'>
    </div>

    <div class="form-group col-md-12">
        <label for="address">Drop Address:</label>
        <input type="text" class="form-control" id="address" name='address' placeholder="Delivery Address">
    </div>

    <div class="form-group col-md-12">
        <label for="landmark">Drop Landmark (optional):</label>
        <input type="text" class="form-control" id="landmark" name='landmark' placeholder="Nearest landmark to easily locate address">
    </div>

  </div>
  </div>
  
  <div class="drop-zone" style='padding: 10px;'>
  <div class="form-row">

     <div class="form-group col-md-3">
      <label for="servicedate">Service Date:</label>
      <input type="text" class="form-control calendar" id="servicedate" name='servicedate'>
      <small class='red '>(date of delivery)</small>
    </div>

    <div class="form-group col-md-3">
      <label for="sfee">Service Fee:</label>
      <input type="number" min='0' step='0.1' class="form-control" id="sfee" name='sfee'>
      <small class='red'>(bookTou fee)</small>
    </div>

    <div class="form-group col-md-3">
      <label for="total">Order Cost:</label>
      <input type="number" min='0' step='0.1' class="form-control" id="total" name='total'>
      <small class='red'>(total item cost)</small>
    </div>
 <div class="form-group col-md-3">
      <label for="total">Packaging:</label>
      <input type="number" min='0' step='0.1' value='0.00' class="form-control" id="packcost" name='packcost'>
      <small class='red'>(if any)</small>
    </div>
    
  </div>
 <div class="form-row">
   <div class="form-group col-md-6">
      <label for="etotal">Payment Mode:</label>
      <br/>
      <div class="custom-control custom-radio custom-control-inline">
        <input type="radio" checked id="paymode1" name="paymode" class="custom-control-input" value='CASH'>
        <label class="custom-control-label" for="paymode1">COD</label>
      </div>
      <div class="custom-control custom-radio custom-control-inline">
        <input type="radio" id="paymode2" name="paymode" class="custom-control-input" value='ONLINE'>
        <label class="custom-control-label" for="paymode2">ONLINE</label>
      </div> 

    </div>
  
      </div>

 <div class="form-row">
 <div class="form-group col-md-12">
      <label for="remarks">Remarks (if any):</label>
      <textarea  class="form-control" id="remarks" rows='4' name='remarks'></textarea>
    </div>  
  </div> 
   </div>
      </div>
      <div class="modal-footer">
          <input type="hidden" name='staff' value="{{ Session::get('_full_name') }}">
        <button type="submit" name='btnsave' value='save' class="btn btn-primary">Save changes</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> 
      </div>
    </div>
  </div>
</div>
</form>



<!-- edit order -->
<form action="{{ action('Erp\ErpToolsController@convertReceiptToPickAndDropRequest') }}"  method="post">
    {{ csrf_field() }}
<div class="modal" id='modalcopyreceipt' tabindex="-1">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Edit Pick-And-Drop Order</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body-nopad"> 

        <div class='row'>
  <div class='col-md-6'>  

 <div class="drop-zone" style='padding: 10px;'>
    <div class="form-row">
    <div class="form-group col-md-5">
      <label for="esource">Order Source:</label>
      <select name ="esource" class="form-control" id="esource"  > 
          <option value="business">On behalf of merchant</option> 
          <option value="booktou">On behalf of bookTou customer</option> 
      </select>
    </div> 

    <div class="form-group col-md-7">
      <label for="staff">Business Name</label><br/>
      <select name ="ebin" class="eselectize" id="ebin">
        @foreach($data['all_business'] as $bitem)
          <option value="{{ $bitem->id  }}">{{ $bitem->name }}</option>
        @endforeach
      </select>
    </div>  
  </div>
 
  <div class="form-row">
    <div class="form-group col-md-8">
      <label for="epucname">Pickup Contact Name:</label>
      <input type="text" class="form-control" id="epucname" name='pucname'>
    </div> 
 <div class="form-group col-md-4">
      <label for="epucphone">Pickup Contact Phone:</label>
      <input type="text" class="form-control" id="epucphone" name='pucphone'>
    </div>

    <div class="form-group col-md-12">
        <label for="epuaddress">Pickup Address:</label>
    <input type="text" class="form-control" id="epuaddress" name='puaddress' placeholder="Delivery Address">
    </div>

<div class="form-group col-md-12">
        <label for="epulandmark">Pickup Landmark (optional):</label>
    <input type="text" class="form-control" id="epulandmark" name='pulandmark' placeholder="Nearest landmark to easily locate address">
    </div>

  </div> 
</div>
  
  
 <div class="drop-zone" style='background-color: #abc7fb; padding: 10px;'>
       
         

  <div class="form-row">
    <div class="form-group col-md-7">
      <label for="ecname">Drop Customer Name:</label>
      <input type="text" class="form-control" id="ecname" name='cname'>
    </div> 
 <div class="form-group col-md-5">
      <label for="ecphone">Drop Customer Phone:</label>
      <input type="text" class="form-control" id="ecphone" name='cphone'>
    </div>

    <div class="form-group col-md-12">
        <label for="eaddress">Drop Address:</label>
    <input type="text" class="form-control" id="eaddress" name='address' placeholder="Delivery Address">
    </div>

<div class="form-group col-md-12">
        <label for="elandmark">Drop Landmark (optional):</label>
    <input type="text" class="form-control" id="elandmark" name='landmark' placeholder="Nearest landmark to easily locate address">
    </div>

  </div> 
 
  <div class="form-row">

     <div class="form-group col-md-3">
      <label for="eservicedate">Service Date:</label>
      <input type="text" class="form-control calendar" id="eservicedate" name='servicedate'>
      <small class='red '>(date of delivery)</small>
    </div>

    <div class="form-group col-md-3">
      <label for="esfee">Service Fee:</label>
      <input type="number" min='0' step='0.1' class="form-control" id="esfee" name='sfee'>
      <small class='red'>(bookTou fee)</small>
    </div>

    <div class="form-group col-md-3">
      <label for="etotal">Order Cost:</label>
      <input type="number" min='0' step='0.1' class="form-control" id="etotal" name='total'>
      <small class='red'>(total item cost)</small>
    </div>

      <div class="form-group col-md-3">
      <label for="etotal">Packaging:</label>
      <input type="number" min='0' step='0.1' class="form-control" id="epackcost" name='packcost'>
      <small class='red'>(if any)</small>
    </div>

 
    <div class="form-group col-md-6">
      <label for="etotal">Payment Mode:</label>
      <br/>
      <div class="custom-control custom-radio custom-control-inline">
        <input type="radio" id="paymode1" name="paymode" class="custom-control-input" value='CASH'>
        <label class="custom-control-label" for="paymode1">COD</label>
      </div>
      <div class="custom-control custom-radio custom-control-inline">
        <input type="radio" id="paymode2" name="paymode" class="custom-control-input" value='ONLINE'>
        <label class="custom-control-label" for="paymode2">ONLINE</label>
      </div> 

    </div>
 

     <div class="form-group col-md-6">
        <label for="paytarget">Payment Target</label>
        <select name="paytarget" class="form-control" id="epaytarget">
            <option value='booktou'>bookTou</option>
            <option value='merchant'>Merchant</option> 
        </select>
      </div>
 
      <div class="form-group col-md-6">
        <label for="deliverymodel">Delivery Model</label>
        <input type="text" id="deliverymodel" name="deliverymodel" class="form-control">
      </div>
      
 
  </div>
 
 <div class="form-row">
 <div class="form-group col-md-12">
      <label for="eremarks">Remarks (if any):</label>
      <textarea  class="form-control" id="eremarks" rows='4' name='remarks'></textarea>
    </div>  </div>
  
  </div>
           
         
 
  
  </div>
    <div class='col-md-6'>
      <img class="img-fluid"   width='700' src="{{ config('app.app_cdn') .  '/assets/image/no-image.jpg'  }}" 
      id='receipt' alt='Order Receipt' />
    </div> 
  </div> 
</div>
<div class="modal-footer">
        <input type="hidden" id="erono" name='erono'>
        <input type="hidden"  name='user_id' value="{{ Session::get('__user_id_') }}"> 
        <button type="submit" name='btnsave' value='save' class="btn btn-primary">Save changes</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> 
      </div>
    </div>
  </div>
</div>
</form>



<div class="modal fade" id="modalConfDelAgent" tabindex="-1" role="dialog" aria-labelledby="modalDisable" aria-hidden="true">
  <form method='post' action="{{ action('Erp\ErpToolsController@removeAgentFromPnDOrder') }}">
  {{  csrf_field() }}
   
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modalDisable">Confirmation</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>
          Are you sure you want to remove agent from this delivery task?
      </p>
      </div>
      <div class="modal-footer">
        <input type='hidden' name='key' id='key2'/> 
        <button type="submit" name="btnRemoveAgent" value="save" class="btn btn-primary btn-sm">Yes</button>
        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">No</button> 
      </div>
    </div>
  </div>
 </form>
</div>

<div class="modal" id='modalViewReceipt' tabindex="-1">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">View Order Receipt</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <img class="img-fluid" width='700'  src="" id='imgreceipt' alt='Order Receipt' /> 
      </div>
      <div class="modal-footer">

        <input type="hidden" id="erono" name='erono'>
        <button type="submit" name='btnsave' value='save' class="btn btn-primary">Save changes</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> 
      </div>
    </div>
  </div>
</div>



<form action="{{ action('Erp\ErpToolsController@processAgentSelfAssignRequest') }}" method='post'>
 {{ csrf_field() }}
<div class="modal" id='modalconfselfassign' tabindex="-1">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Confirm Agent Self Assign Request</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       <p class='alert alert-danger'>This action is non recoverable.</p>
       <div class='modalinfo'>
       </div>
      </div>
      <div class="modal-footer"> 
        <input type="hidden" id="confsakey" name='confsakey'>
        <input type="hidden" id="confsaaid" name='confsaaid'>
        <button type="submit" name='btnupdatesa' value='save' class="btn btn-primary">Yes</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal">No</button> 
      </div>
    </div>
  </div>
</div>
</form>



 
<form action="{{ action('Erp\ErpToolsController@updatePnDOrderRemarks') }}" method='post'>
  {{ csrf_field() }}
  <div class="modal modalrem" tabindex="-1">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Remarks Update</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body"> 

          <div class="form-group">
          <label for="type">Remark Type</label>
          <select  class="form-control" id="type" name='type' aria-describedby="type">
            <option>Customer Feedback</option>
            <option>CC Remarks</option>
            <option>Agent Feedback</option>
            <option>Completeion Remark</option>
          </select>
           
        </div>


          <div class="form-group">
            <label for="remarks">Remarks</label>
            <textarea class="form-control" id="remarks" name='remarks' rows="4"></textarea>
          </div> 

        </div>
        <div class="modal-footer">
          <input type='hidden' name='turl' id='list'/> 
           <input type='hidden' name='orderno' id='key3'/> 
           <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> 
          <button type="submit" name='btnsubmit' value='save' class="btn btn-primary">Save Remarks</button>
          
        </div>
      </div>
    </div>
  </div>
</form> 




<!-- edit order -->
<form action="{{ action('Erp\ErpToolsController@updateAssistOrder') }}"  method="post">
    {{ csrf_field() }}
<div class="modal" id='modalupdateassist' tabindex="-1">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Edit Assist Order</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body-nopad"> 

        <div class='row'>
  <div class='col-md-6'>  

 <div class="drop-zone" style='padding: 10px;'>
      
  <div class="form-row">
    <div class="form-group col-md-8">
      <label for="epucname">Pickup Contact Name:</label>
      <input type="text" class="form-control" id="aepucname" name='pucname'>
    </div> 
 <div class="form-group col-md-4">
      <label for="epucphone">Pickup Contact Phone:</label>
      <input type="text" class="form-control" id="aepucphone" name='pucphone'>
    </div>

    <div class="form-group col-md-12">
        <label for="epuaddress">Pickup Address:</label>
    <input type="text" class="form-control" id="aepuaddress" name='puaddress' placeholder="Delivery Address">
    </div>

<div class="form-group col-md-12">
        <label for="epulandmark">Pickup Landmark (optional):</label>
    <input type="text" class="form-control" id="aepulandmark" name='pulandmark' placeholder="Nearest landmark to easily locate address">
    </div>

  </div> 
</div>
  
  
 <div class="drop-zone" style='background-color: #abc7fb; padding: 10px;'>
       
         

  <div class="form-row">
    <div class="form-group col-md-7">
      <label for="ecname">Drop Customer Name:</label>
      <input type="text" class="form-control" id="aecname" name='cname'>
    </div> 
 <div class="form-group col-md-5">
      <label for="ecphone">Drop Customer Phone:</label>
      <input type="text" class="form-control" id="aecphone" name='cphone'>
    </div>

    <div class="form-group col-md-12">
        <label for="eaddress">Drop Address:</label>
    <input type="text" class="form-control" id="aeaddress" name='address' placeholder="Delivery Address">
    </div>

<div class="form-group col-md-12">
        <label for="elandmark">Drop Landmark (optional):</label>
    <input type="text" class="form-control" id="aelandmark" name='landmark' placeholder="Nearest landmark to easily locate address">
    </div>

  </div> 
 
  <div class="form-row">

     <div class="form-group col-md-2">
      <label for="eservicedate">Service Date:</label>
      <input type="text" class="form-control calendar" id="aeservicedate" name='servicedate'>
    </div>

    <div class="form-group col-md-2">
      <label for="esfee">Service Fee:</label>
      <input type="number" min='0' step='0.1' class="form-control" id="aesfee" name='sfee'>
    </div>

    <div class="form-group col-md-4">
      <label for="etotal">Total Amount:</label>
      <input type="number" min='0' step='0.1' class="form-control" id="aetotal" name='total'>
    </div>
 
    <div class="form-group col-md-4">
      <label for="apaymode1">Payment Mode:</label>
      <br/>
      <div class="custom-control custom-radio custom-control-inline">
        <input type="radio" id="apaymode1" name="paymode" class="custom-control-input" value='CASH'>
        <label class="custom-control-label" for="paymode1">COD</label>
      </div>
      <div class="custom-control custom-radio custom-control-inline">
        <input type="radio" id="apaymode2" name="paymode" class="custom-control-input" value='ONLINE'>
        <label class="custom-control-label" for="paymode2">ONLINE</label>
      </div> 
    <div class="form-group col-md-6">
        <label for="paytarget">Delivery Model</label>
         <input type="type" id="deliverymodel" name="deliverymodel" class="form-control">
      </div> 
    </div>
 
  </div>

 <div class="form-row">
 <div class="form-group col-md-12">
      <label for="eremarks">Remarks (if any):</label>
      <textarea  class="form-control" id="aeremarks" rows='4' name='remarks'></textarea>
    </div>  </div>
  
  </div>
           
         
  
  
  </div>
    <div class='col-md-6'>
      <img class="img-fluid"   width='700' src="{{ config('app.app_cdn') .  '/assets/image/no-image.jpg'  }}" 
      id='receipt' alt='Order Receipt' />
    </div> 
  </div> 
</div>
<div class="modal-footer">
        <input type="hiddens" id="aerono" name='erono'>
        <input type="hidden"  name='user_id' value="{{ Session::get('__user_id_') }}"> 
        <button type="submit" name='btnsave' value='save' class="btn btn-primary">Save changes</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> 
      </div>
    </div>
  </div>
</div>
</form>



<div class="modal fade" id="modalConfDelAgent" tabindex="-1" role="dialog" aria-labelledby="modalDisable" aria-hidden="true">
  <form method='post' action="{{ action('Erp\ErpToolsController@removeAgentFromPnDOrder') }}">
  {{  csrf_field() }}
   
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modalDisable">Confirmation</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>
          Are you sure you want to remove agent from this delivery task?
      </p>
      </div>
      <div class="modal-footer">
        <input type='hidden' name='key' id='key2'/> 
        <button type="submit" name="btnRemoveAgent" value="save" class="btn btn-primary btn-sm">Yes</button>
        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">No</button> 
      </div>
    </div>
  </div>
 </form>
</div>

<div class="modal" id='modalViewReceipt' tabindex="-1">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">View Order Receipt</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <img class="img-fluid" width='700'  src="" id='imgreceipt' alt='Order Receipt' /> 
      </div>
      <div class="modal-footer">

        <input type="hidden" id="erono" name='erono'>
        <button type="submit" name='btnsave' value='save' class="btn btn-primary">Save changes</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> 
      </div>
    </div>
  </div>
</div>


<!-- modal for removing pnd order -->
<div class="modal fade" id="modalConfDelPND" tabindex="-1" role="dialog" aria-labelledby="modalDisable" aria-hidden="true">
  <form method='post' action="{{ action('Erp\ErpToolsController@removePickAndDropOrders') }}">
  {{  csrf_field() }}
   
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modalDisable">Confirmation</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>
          Are you sure you want to remove partcular order from list?
      </p>
      </div>
      <div class="modal-footer">
        <input type='hidden' name='pndid' id='pnd_no'/> 
    <button type="submit" name="btnRemovePNDorder" value="save" class="btn btn-primary btn-sm">Yes</button>
    <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">No</button> 
      </div>
    </div>
  </div>
 </form>
</div>

@endsection

@section("script")

<script>


$(document).on("click", ".showremmodal", function()
{
  $("#key3").val($(this).attr("data-key"));
  $(".modalrem").modal("show")

}); 
  

$(document).on("click", ".addOrder", function()
{
    $("#modalneworder").modal("show");

});


$(document).on("click", ".btncancel", function()
{

    $("#orderno").val($(this).attr("data-ono"));
    $(".modal_cancel").modal("show")

});


$(document).on("click", ".btn_deletePND", function()
{
  $("#pnd_no").val($(this).attr("data-pnd-key"));
  $("#modalConfDelPND").modal("show")

});

$(document).on("click", ".btnedit", function()
{

  var rbin =  $(this).attr("data-requestby")  ;
  var type =  $(this).attr("data-type")  ;
  var rcpt = $(this).attr("data-receipt"); 

  if(type == "assist")
  {
      $("#aepucname").val( $(this).attr("data-pfullname") );
      $("#aepucphone").val( $(this).attr("data-pphone") );  
      $("#aepuaddress").val( $(this).attr("data-paddress") );  
      $("#aepulandmark").val( $(this).attr("data-plandmark") );    
      $("#aerono").val($(this).attr("data-ono"));  
      $("#areceipt").attr("src",  (  rcpt != "" ? rcpt :  "{{ config('app.app_cdn') .  '/assets/image/no-image.jpg'  }}"   ) );   
      $("#aecname").val( $(this).attr("data-fullname") );
      $("#aecphone").val( $(this).attr("data-phone") );  
      $("#aeaddress").val( $(this).attr("data-address") );  
      $("#aelandmark").val( $(this).attr("data-landmark") );   
      $("#aesfee").val( $(this).attr("data-fee") ); 
      $("#aetotal").val( $(this).attr("data-total") ); 
      $("#aeservicedate").val( $(this).attr("data-date") );  
      $("#aeremarks").val( $(this).attr("data-pickupdetails") ); 
      $("#epackcost").val( $(this).attr("data-packcost") ); 
      $("#deliverymodel").val($(this).attr("data-deliverymodel"));
      if($(this).attr("data-pay_mode") == "ONLINE")
        $("#apaymode2").prop("checked", true);
      else
        $("#apaymode1").prop("checked", true);


      if( $(this).attr("data-paytarget") == "merchant" || $(this).attr("data-paytarget") == "business")
        $("#epaytarget").val('merchant').trigger('change'); 
      else
        $("#epaytarget").val('booktou').trigger('change'); 
 
       

    $("#modalupdateassist").modal("show") 
  }
  else 
  {

      $("#epucname").val( $(this).attr("data-pfullname") );
      $("#epucphone").val( $(this).attr("data-pphone") );  
      $("#epuaddress").val( $(this).attr("data-paddress") );  
      $("#epulandmark").val( $(this).attr("data-plandmark") );    
      $("#erono").val($(this).attr("data-ono"));  
      $("#receipt").attr("src",  (  rcpt != "" ? rcpt :  "{{ config('app.app_cdn') .  '/assets/image/no-image.jpg'  }}"  ) );   
      $("#ecname").val( $(this).attr("data-fullname") );
      $("#ecphone").val( $(this).attr("data-phone") );  
      $("#eaddress").val( $(this).attr("data-address") );  
      $("#elandmark").val( $(this).attr("data-landmark") );   
      $("#esfee").val( $(this).attr("data-fee") ); 
      $("#etotal").val( $(this).attr("data-total") ); 
      $("#eservicedate").val( $(this).attr("data-date") );  
      $("#eremarks").val( $(this).attr("data-pickupdetails") );
      $("#epackcost").val( $(this).attr("data-packcost") ); 
      $("#deliverymodel").val($(this).attr("data-deliverymodel"));
      if($(this).attr("data-pay_mode") == "ONLINE")
        $("#paymode2").prop("checked", true);
      else
        $("#paymode1").prop("checked", true);

      if( $(this).attr("data-paytarget") == "merchant" || $(this).attr("data-paytarget") == "business")
        $("#epaytarget").val('merchant').trigger('change'); 
      else
        $("#epaytarget").val('booktou').trigger('change'); 


      $(".eselectize").val(rbin).trigger('change');  
      $("#modalcopyreceipt").modal("show")  
  } 

  
});




$(document).on("click", ".btnconfim", function()
{

  var json = {};
  json['oid'] =  $("#orderno").val( ) ;
  json['reason'] =  $("#tareason").val( ) ;
 

   $('.confloading').html("<img   src='" + siteurl + "/public/assets/image/processing.gif' alt='Loading ...' />");
 

    $.ajax({
      type: 'post',
      url: api + "/v2/web/customer-care/pnd-order/cancel" ,
      data: json,
      success: function(data)
      {
        data = $.parseJSON(data);  
        $('.confloading').html( "<div class='alert alert-info'>" + data.detailed_msg  + "</div>");  
        $(".modal_cancel").modal("hide") ;

         $("#tr" + $("#orderno").val( )).remove();
      },
      error: function( ) 
      {
        alert(  'Something went wrong, please try again'); 
      } 

    });
    

});

  


 
$(document).on("click", ".showTaskList", function()
{

    var cid = $(this).attr("data-cid"); 
    var aid  = $('#' + cid + ' option:selected').val() ;
    var anm = $('#' + cid + ' option:selected').text() ; 
    var img = $('#' + cid + ' option:selected').attr("data-img");
 
    $("#span_anm").text(anm);  
    $("#apimg").attr('src', img); 

    $('#modalTaskList .loading').html("<img  src='" + siteurl + "/public/assets/image/processing.gif' alt='Loading ...' />");
    $('#modalTaskList').modal('show');

      var json = {};
      json['aid'] =  aid ; 

    $.ajax({
      type: 'post',
      url: api + "v2/web/customer-care/agents/view-assigned-tasks" ,
      data: json,
      success: function(data)
      {
        data = $.parseJSON(data);

        if(data.status_code == 5007)
        {
           $('#modalTaskList .loading').html(""); 

            var totalTask = 0  ;
            var tableHeader = '<table>';


            var htmlOut = "<tr><th>Order #</th><th>Pickup Location</th><th>Drop Location</th><th>Distance (Kms)</th></tr>";

            $.each(data.results, function(index, item)
            {
               
              htmlOut += "<tr id='tr" +  item.orderId  +  "'>"; 
              htmlOut += "<td>";
              htmlOut +=  item.orderId ;
              htmlOut += "</td>";
              htmlOut += "<td>";
              htmlOut +=  "<strong>"  + item.storeName + '</strong>, ' + "<br/>Addresss:" +item.pickUpLocality  +  item.pickUpLandmark;
              htmlOut += "</td>";
              htmlOut += "<td>";
              htmlOut +=  "<strong>"  + item.customerName + '</strong>, <i class="fa fa-phone"></i>' + item.customerPhone + "<br/>Addresss:" +  item.deliveryAddress  + item.deliveryLandmark     ;
              htmlOut += "</td>";
              htmlOut += "<td>";
              htmlOut +=  "0" ;
              htmlOut += "</td>"; 
              htmlOut += "</tr>";

              totalTask++;
            });
  
            htmlOut += "</table>";  
            var html = tableHeader + "<tr><th colspan='3'>Total Task:</th><th>" + totalTask + "</th></tr>" + htmlOut;  

            if(totalTask > 0)
            {
              $("#content_area").html(html);  
              $( "#content_area table" ).addClass( "table" );
              $( "#content_area table" ).addClass( "table-striped" );
            }
            else 
            {
              $("#content_area").html("<p class='alert alert-info'>No delivery tasks assigned yet!</p>");  
            $( "#content_area p.alert" ).addClass( "alert" );
            $( "#content_area p.alert" ).addClass( "alert-info" );

            }
             
        } 
      },
      error: function( ) 
      {
         $('#modalTaskList .loading').html("Something went wrong, please try again");   
      } 

    });  

});




$(document).on("click", ".btn-assign", function()
{
    var oid   =   $(this).attr("data-oid")  ;
    var aid    =   $("#aid_" + oid ).val()  ;

    var json = {}; 
    json['oid'] = oid  ;
    json['aid'] =  aid ; 
    json['otype'] =  'pickup' ; 

    $('#modal_processing .loading').html("<img   src='" + siteurl + "/public/assets/image/processing.gif' alt='Loading ...' />");

    $('#modal_processing').modal('show');

  

    $.ajax({
      type: 'post',
      url: api + "v2/web/customer-care/orders/assign-to-agent" ,
      data: json,
      success: function(data)
      {
        
        data = $.parseJSON(data);  
        $('#modal_processing .loading').html( data.detailed_msg); 

        if(data.status_code == 7001)
        {
           $('.btn_action').attr("data-action", "refresh");
        }

      },
      error: function( xhr, status, errorThrown) 
      { 
        console.log(xhr);  
       // alert(  'Something went wrong, please try again'); 
      } 

    });

});


$(document).on("click", ".btn_action", function()
{
  var action = $(this).attr("data-action"); 
  if(action == "refresh")
     location.reload();

})

$(document).on("change", ".switch", function()
{
   var confirmation = "no";
   var oid = $(this).attr("data-id");

   if($(this).is(":checked") == true )
   {
      confirmation = "yes";
      $("#btnsaveconfirmation").attr("data-id",  oid);
      $("#btnsaveconfirmation").attr("data-conf", confirmation ); 
      $("#confirmOrder").modal("show");   
   }  

})

 
  

$(function() {
    $('.calendar').pignoseCalendar( 
    {
      format: 'DD-MM-YYYY' 
    });
});


 

$(document).on("click", ".btnRemoveAgent", function()
{
  $("#key2").val($(this).attr("data-key"));
  $("#modalConfDelAgent").modal("show")

});


 
$(document).on("click", ".btnViewRcpt", function()
{
    var img = $(this).attr("data-src");
    $("#imgreceipt").attr("src", img );
    $("#modalViewReceipt").modal("show");

});



$(document).on("click", ".btnconfselfassign", function()
{
    var oid = $(this).attr("data-oid");
    var aid = $(this).attr("data-aid");  
    var action = $(this).attr("data-action");

    $("#confsakey").val( oid);
    $("#confsaaid").val(   aid ); 

    if(action== "allow")
    {
      $(".modalinfo").html("<p>Request to self assign will be deleted. Are you sure, you want to take this action?</p>");
    }
    else 
    {
      $(".modalinfo").html("<p>Request to self assign will be granted. Are you sure, you want to take this action?</p>");
    }

    $("#modalconfselfassign").modal("show");

});

 

    $(document).ready(function () {
        

      $('.selectize').select2({
        selectOnClose: true
      });
 

    $('.selectize').on('select2:select', function (e)
    {
      var data = e.params.data; 
 
            
            //load business address
            var json = {}; 
            json['bin'] = data.id  ; 


            $.ajax({
            type: 'post',
            url: api + "v3/web/business/get-business-info" ,
            data: json,
            success: function(data)
            {
              
              data = $.parseJSON(data);    
              if(data.status_code == 5507)
              {
 
                $("#pucname").val(  data.result.name  );
                $("#pucphone").val(  data.result.phone  );
                $("#puaddress").val(  data.result.locality  );
                $("#pulandmark").val(  data.result.landmark  );

              }

            },
            error: function( xhr, status, errorThrown) 
            { 
              console.log(xhr);  
             // alert(  'Something went wrong, please try again'); 
            } 

          }); 

        });


      $('.eselectize').select2({
        selectOnClose: true,
         theme: 'bootstrap4',
      });



$('.eselectize').on('select2:select', function (e)
{
    // Do something
    var data = e.params.data; 

    //load business address
            var json = {}; 
            json['bin'] = data.id  ; 


            $.ajax({
            type: 'post',
            url: api + "v3/web/business/get-business-info" ,
            data: json,
            success: function(data)
            {
              
              data = $.parseJSON(data);    
              if(data.status_code == 5507)
              {
 
                $("#epucname").val(  data.result.name  );
                $("#epucphone").val(  data.result.phone  );
                $("#epuaddress").val(  data.result.locality  );
                $("#epulandmark").val(  data.result.landmark  );

              }

            },
            error: function( xhr, status, errorThrown) 
            { 
              console.log(xhr);  
             // alert(  'Something went wrong, please try again'); 
            } 

          }); 


});
   

        



    });
 
 


</script> 

@endsection 