@extends('layouts.erp_theme_03')
@section('content')

 
  <?php
    $order_info = $data['order_info'];  
    $agent = $data['agent_info'];   
    $order_no =$order_info->id ; 
  ?> 
 


 <div class="row"> 
 
 @if( $order_info->milestone != "" && $order_info->milestone != null  )
 
  <div class="col-md-12">
  
       <p class='alert alert-success h2'><i class='fa fa-trophy  '></i>  {{ $order_info->milestone }}</p>
 
  </div>  
    @endif

 
 <div class="col-md-4">    
     <div class="card panel-default">
           <div class="card-header">
              <div class="card-title"> 
                 <h5 class="card-category">Order # {{ $order_info->id  }} 
                @switch($order_info->book_status)

                      @case("new")
                        <span class='badge badge-primary'>New</span>
                      @break

                      @case("confirmed")
                        <span class='badge badge-info'>Confirmed</span>
                      @break
 

                      @case("order_packed")
                        <span class='badge badge-info'>Order Packed</span>
                      @break

                      @case("package_picked_up")
                        <span class='badge badge-info'>Package Picked Up</span>
                      @break

                       @case("pickup_did_not_come")
                        <span class='badge badge-warning'>Pickup Didn't Come</span>
                      @break

                       @case("in_route")
                        <span class='badge badge-success'>In Route</span>
                      @break

                       @case("completed")
                        <span class='badge badge-success'>Completed</span>
                      @break

                      @case("delivered")
                        <span class='badge badge-success'>Delivered</span>
                      @break

                       @case("delivery_scheduled")
                        <span class='badge badge-success'>Delivery Scheduled</span>
                      @break 
                      @case("canceled")
                        <span class='badge badge-danger'>Cancelled</span>
                      @break 
                      @endswitch  
                    </h5> 
                </div>
            </div>
       <div class="card-body">
         <div class='row'  >
          <div class='col-md-12'> 
            <p>
              <span> Order Date: {{ date('d-m-Y H:i:s', strtotime( $order_info->book_date)) }}</span>
            </p>
            <p>
               
              <span>Payment Mode:</span> 
              @if( strcasecmp($order_info->pay_mode,"cash") == 0    )
                <span class='badge badge-warning'>CASH</span>
              @else 
                <span class='badge badge-success'>ONLINE</span>
              @endif
             
             </p>

             <p>
              <p><strong>Order Details</strong><br/>
               @if($order_info->pickup_details!="")
                {{ $order_info->pickup_details}}
              @else 
                No Order Remark Provided.
              @endif
              
            </p>

            <!-- <p>Merchat Cost: <span class='badge badge-info'>{{ $order_info->total_amount }}</span></p> -->
            <p>bookTou Service Fee: <span class='badge badge-primary'>{{  $order_info->service_fee }}</span></p>
            <p>Total to collect Fee: <span class='badge badge-success'>{{ $order_info->total_amount }}</span></p> 
          </div> 
        </div>
              </div>

  

            </div> 

 </div>

 
    <div class="col-md-4">   
         <div class="card panel-default">
       <div class="card-header">
                <h5 class="card-category">Cash Memo/Receipt</h5> 
                </div>
 <div class="card-body text-center">  
                 @if($order_info->order_receipt != null)      
   
   <a data-src="{{ $order_info->order_receipt }}"  class="btn btn-link btnViewRcpt" href='#'  >
    <img src="{{  $order_info->order_receipt }}" width='100' class="img-fluid" alt="Responsive image">
    <br/>
        <br/>
    
   </a>
<small>Click on the image enlarge</small>
     @else 
     <p class='alert alert-info'>No order receipt uploaded.</p>
      @endif
       </div>
    </div>


    <div class="card panel-default mt-3">
       <div class="card-header">
          <h5 class="card-category">Remarks</h5> 
        </div>
    <div class="card-body  text-center">  
      
      @if($order_info->agent_remarks != null) 
       <table>
        <tr> 
          <td>
            <span class='badge badge-danger'>Agent Remarks</span><br/>
            {{  $order_info->agent_remarks }}</td> 
        </tr>
        </table> 
      @endif


      @if(isset( $data['remarks'] ) && count($data['remarks']) > 0)
                  <table>
                    @foreach($data['remarks'] as $rem)
                      <tr> 
                        <td>
                          <span class='badge badge-danger'>{{ $rem->rem_type}}</span><br/>
                          {{ $rem->remarks}}</td> 
                      </tr>
                    @endforeach
                  </table> 
                  @else 
                  <p class='alert alert-info'>No remarks recorded.</p>
      @endif 


      
       </div>
    </div>



  </div>

 <div class="col-md-4">   
 


            <div class="card panel-default">
              <div class="card-header">
                <h5 class="card-category">Pickup from</h5>
                <h4 class="card-title">{{$order_info->pickup_name}}</h4>
                </div>
              <div class="card-body"> 

                 <p>{{$order_info->pickup_address}}</p> 
                    {{$order_info->pickup_landmark}}<br/>
                     

              <br/>
                  <i class='fa fa-phone'></i> {{$order_info->pickup_phone}}</p> 
              </div> 
            </div> 

             <div class="card panel-default mt-3">
              <div class="card-header">
                <h5 class="card-category">Drop to</h5>
                <h4 class="card-title">{{$order_info->drop_name}} </h4>
               </div> 
              <div class="card-body"> 
                 <p>Delivery Address:<br/>
                  {{$order_info->drop_address}}<br/>
                  {{$order_info->drop_landmark}}<br/>  
                  <i class='fa fa-phone'></i> {{$order_info->drop_phone}}</p> 
              </div> 
            </div>

              
            

          </div>  

       </div> 
 

 



 


 

 
<div class="modal" id='modalViewReceipt' tabindex="-1">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Enlarged Image</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <img class="img-fluid" width='600'  src="" id='imgreceipt' alt='Order Receipt' /> 
      </div>
      <div class="modal-footer">

        <input type="hidden" id="erono" name='erono'> 
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> 
      </div>
    </div>
  </div>
</div>

 


<form action="{{ action('Erp\ErpToolsController@updatePnDOrderRemarks') }}" method='post'>
  {{ csrf_field() }}
  <div class="modal modalrem" tabindex="-1">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Add Order Remarks</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body"> 

          <div class="form-group">
          <label for="type">Remark Type</label>
          <select  class="form-control" id="type" name='type' aria-describedby="type">
            <option>Customer Feedback</option>
            <option>CC Remarks</option>
            <option>Agent Feedback</option>
            <option>Completeion Remark</option>
          </select>
           
        </div>


          <div class="form-group">
            <label for="remarks">Remarks</label>
            <textarea class="form-control" id="remarks" name='remarks' rows="4"></textarea>
          </div> 

        </div>
        <div class="modal-footer">
           <input type='hidden' name='orderno' id='key3'/> 
           <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> 
          <button type="submit" name='btnsubmit' value='save' class="btn btn-primary">Save Remarks</button>
          
        </div>
      </div>
    </div>
  </div>
</form> 


 


 

<div class="modal hide fade modal_processing" id='modal_processing'   role="dialog">
  <div class="modal-dialog " role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title ">Processing Order</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body text-center"> 
        <div class='loading'>
        </div> 
      </div>
      <div class="modal-footer">

        <button type="button" class="btn btn-secondary btn_action" data-action='non' data-dismiss="modal">Close</button> 
      </div>
    </div>
  </div>
</div>
@endsection


@section('script')
 

<script>





$(document).on("click", ".btn-assign", function()
{
    var oid   =   $(this).attr("data-oid")  ;
    var aid    =   $("#aid_" + oid ).val()  ;

    var json = {}; 
    json['oid'] = oid  ;
    json['aid'] =  aid ; 
    json['otype'] =  'pickup' ; 

    $('#modal_processing .loading').html("<img   src='" + siteurl + "/public/assets/image/processing.gif' alt='Loading ...' />");

    $('#modal_processing').modal('show');
 
    $.ajax({
      type: 'post',
      url: api + "v2/web/customer-care/orders/assign-to-agent" ,
      data: json,
      success: function(data)
      {
        
        data = $.parseJSON(data);  
        $('#modal_processing .loading').html( data.detailed_msg); 

        if(data.status_code == 7001)
        {
           $('.btn_action').attr("data-action", "refresh");
        }

      },
      error: function( xhr, status, errorThrown) 
      { 
        console.log(xhr);  
       // alert(  'Something went wrong, please try again'); 
      } 

    });

});



$(document).on("click", ".showremmodal", function()
{
  $("#key3").val($(this).attr("data-key"));
  $(".modalrem").modal("show")

}); 

 $(document).on("click", ".btnpoppaybox", function()
{
  $("#key4").val($(this).attr("data-key"));
  $(".modalpayment").modal("show")

}); 
 
 

$(document).on("click", ".btnchangestatus", function()
{
  $("#key5").val($(this).attr("data-key"));
  $(".modalstatus").modal("show")

}); 


  $(document).on("click", ".btnRemoveAgent", function()
{
  $("#key2").val($(this).attr("data-key"));
  $("#modalConfDelAgent").modal("show")

});


 
$(document).on("click", ".showmodal", function()
{
  var widget = $(this).attr("data-widget");
  var key = $(this).attr("data-key");
   $("#key").val(key);
  $("#widget-" + widget ).modal("show") ;

});


$(document).on("click", ".btnViewRcpt", function()
{
    var img = $(this).attr("data-src");
    $("#imgreceipt").attr("src", img );
    $("#modalViewReceipt").modal("show");

});




$(function() {
    $('.calendar').pignoseCalendar();
});

</script>


@endsection

   