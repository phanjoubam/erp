@extends('layouts.erp_theme_03')
@section('content')
 
<?php  
    $cdn_url =   $_ENV['APP_CDN'] ;  
    $cdn_path =   $_ENV['APP_CDN_PATH'] ;   
    $total_orders = count( $orders); 
    $completed = $active =0;
    foreach($orders as $item)
    {
      switch($item->book_status)
      {
        case  "completed":
          $completed++;  
          break;
        case  "new":
        case  "confirmed":
        case  "in_queue":
        case  "order_packed":
        case  "pickup_did_not_come":
        case  "in_route":
        case  "package_picked_up":
        case  "delivery_scheduled":
        case  "package_picked_up":
            $active++;
            break; 

      }

    } 

?> 


   <div class="row">    
     <div class="col-md-12"> 
                   
                               @if( count($orders ) > 0 ) 
                               <div class='card'>
                               <div class="card-body">
                               <table class="table table-striped"  >
                                    <thead>
                                        <tr>  
                                            <th width='100px'>Order #</th>
                                            <th width='100px'>Booking Date</th> 
                                            <th width='100px'>Service Time</th>
                                            <th width='200px'>Services</th>
                                            <th width='200px'>Customer</th>
                                            <th width='200px'>Address</th>
                                            <th width='100px'>Total Cost</th>  
                                            <th width='100px'>Payment Type</th>
                                            <th width='200px' class='text-center'>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                 </table>   
                                
                               <div id='sortable'> 
                               @foreach($orders as $item) 
                               <div class='ui-state-default mt-2'> 
                                <table class="table table-striped"  > 
                                    <tbody> 
                                        <tr  > 
                                          <td class="text-left" width='100px'>
                                            <a href="{{ URL::to('/orders/pnd/view-details') }}/{{  $item->id  }}" target='_blank'>
                                              {{$item->id }}
                                            </a> 
                                          </td>
                                          <td width='100px'>{{date('d-m-Y', strtotime( $item->service_date )) }}</td> 
                                          @foreach($booked_services as $bookeditem)
                                            @if($bookeditem->book_id == $item->id )
                                           
                                            <td width='100px' ><span class="badge badge-success">{{ date('h:i a', strtotime( $bookeditem->service_time ))  }}</span></td> 
                                            <td width='200px' class="text-left">{{   $bookeditem->service_name  }}</td>
                                              @break
                                            @endif 
                                          @endforeach
                                          
                                          <td width='200px' class="text-left">
                                          @foreach($customers as $customer)
                                            @if( $customer->id  == $item->book_by )
                                              {{  $customer->fullname  }} 
                                              @break
                                            @endif 
                                          @endforeach
                                          </td>
                                          <td width='200px' class="text-left">{{ $item->address == "business" ? "Business Location" :  $item->address  }}</td>
                                          <td width='100px' >{{ $item->total_cost }}</td>
                                          <td width='100px' class="text-left"><span class='badge badge-primary'>{{ $item->pay_mode  }}</span></td> 

                                          <td width='200px' class="text-center">
                                          <div class="btn-group" role="group" aria-label="Basic example">
                                            <button type="button" class="btn btn-info redirect" data-key="{{ $item->id   }}" data-pgc='2' ><i class="fa fa-info-circle"></i></button>
                                            <button type="button" class="btn btn-success btn-process" data-widget="1"  data-key="{{ $item->id   }}" data-pgc='1' ><i class="fa fa-cog"></i></button>
                                            <button type="button" class="btn btn-success redirect" data-key="{{ $item->id   }}" data-pgc='1' ><i class="fa fa-file-invoice"></i></button>
                                            <button type="button" class="btn btn-danger delete" data-key="{{ $item->id   }}"  ><i class="fa fa-trash"></i></button>
                                          </div>
                                          </td> 


                                        </tr>
                                        </tbody>
                                </table>
                            </div>  
                            @endforeach
                            </div>

                            </div>
                                 </div>

                                 @else 
                                <p class='alert alert-info'>No New signup today.</p>
                                @endif 
 
                   
     </div>
 
 </div>


 <form action="{{ action('Erp\ErpToolsController@updateBookingStatus') }}" method="post">
 {{  @csrf_field() }}
 <div class="modal" id="wg1" tabindex="-1">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Confirm Booking Progress Status</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
  
      <div class="form-group row">
      <label for="status" class="col-sm-12 col-md-4 col-form-label">Booking Status</label>
      <div class="col-sm-12 col-md-8">
      <select id="status" name='status' class="form-control">
      <option value='engaged'>Engage</option>
        <option value='cancelled'>Cancelled</option>
        <option value='no_show'>No Show</option>
        <option value='completed'>Completed</option> 
      </select>
    </div>

  
  </div> 

      </div>
      <div class="modal-footer">
      <input type='hidden' value="" id="key1" name="bookingno" />
      <button type="submit" class="btn btn-primary">Save</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      
      </div>
    </div>
  </div>
</div>
</form>
 
@endsection


@section("script")

<script>
  $( function() {
    $( "#sortable" ).sortable();
    $( "#sortable" ).disableSelection();
  } );

  $(document).on("click", ".btn-process", function(){ 
    
    var widgetno = $(this).attr("data-widget");
    $("#key" + widgetno).val(    $(this).attr("data-key") );
    $("#wg" + widgetno).modal('show');

 });

  </script>

 @endsection

 