@extends('layouts.erp_theme_03')
@section('content')
 @include('submenu.order_sub_menu') 
 
<?php 
  
  $total_orders = count( $all_orders);

  $completed = $active =0;
  foreach($all_orders as $item)
  {
    switch($item->book_status)
    {
      case  "completed":
        $completed++;  
        break;
      case  "new":
      case  "confirmed":
      case  "in_queue":
      case  "order_packed":
      case  "pickup_did_not_come":
      case  "in_route":
      case  "package_picked_up":
      case  "delivery_scheduled":
      case  "package_picked_up":
          $active++;
          break; 

    }

  } 

?> 


<div class="row">
<div class="col-md-12">
<div class="card m"> 
                     
                        <div class="card-body card-zeropad">
                            <div class="table-responsive">
                               @if( count($all_orders ) > 0 ) 

                                <table class="table table-striped">
                                    <thead>
                                        <tr> 
                                            <th>Order #</th>
                                            <th>Type</th>
                                            <th>Total Cost</th>
                                            <th>Customer</th>
                                             <th>Address</th>
                                            <th>Payment</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($all_orders as $item)
                                        <tr> 
                                          <td class="text-left">
                                            <a href="{{ URL::to('/order/view-details') }}/{{$item->id}}" target='_blank'>
                                              {{$item->id }}
                                            </a> 
                                          </td>
                                          <td class="text-left">
                                          @if( $item->order_type == "pnd" || $item->order_type == "assist")
                                          <span class="badge  badge-primary">{{ $item->order_type  }}</span>
                                            @else
                                            <span class="badge badge-warning">{{ $item->order_type  }}</span>
                                            @endif
                                          </td> 
                                          <td>{{ $item->total_cost }}</td>
                                          <td class="text-left">{{ $item->customer_name }}</td>
                                          <td class="text-left">{{ $item->address }}</td>
                                          <td class="text-left"><span class='badge badge-primary'>{{ $item->pay_mode  }}</span></td> 
                                        </tr>
                                         
                                        @endforeach
                                    </tbody>
                                </table>

                                 @else 
                                <p class='alert alert-info'>No New Active Orders.</p>
                                @endif 

                            </div>
                        </div>
                    </div> 
                 
                   
                </div>
 
 </div>
 
@endsection


@section("script")

<script>

$(function() {
    $('.calendar').pignoseCalendar( 
    {
      format: 'DD-MM-YYYY' 
    });
});

</script> 

@endsection