@extends('layouts.erp_theme_02')
@section('content')

@php 
   
  $total_new_normalOrders=0;
  $total_new_pndOrders = 0;

  $totalPndOrdersCompleted =0;
  $totalOrdersCompleted = 0; 
   
  $totalNormalOrders = 0;
  $totalPndOrders =0; 

  $totalassitsOrderSales = 0;
  $totalPndOrderSales=0;
  $totalnormalOrderSales=0;
    
@endphp

<?php

foreach($normal as $item)
  {
     if( $item->book_status == "delivered" || $item->book_status == "completed"   )
     {
      $totalOrdersCompleted++;
      $totalNormalOrders++;
      $totalnormalOrderSales+= $item->total_cost;
     }

     $total_new_normalOrders++;

  }

foreach($pnd as $item)
  {
    if( $item->book_status == "delivered" || $item->book_status == "completed"   )
     {
      $totalPndOrdersCompleted++;
       $totalPndOrders++;
      $totalPndOrderSales+=$item->total_amount;
     }

     $total_new_pndOrders++;
   }
?>



<div class=" row ">
    <div class="col-md-3">
<div class="card">
    <div class="card-body">
      <div class="row no-gutters align-items-center">
                                        <div class="col mr-2">
                                            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                                Normal Order Count ({{$totalNormalOrders}})</div>
                                            <div class="h5 mb-0 font-weight-bold text-gray-800">₹ {{$totalnormalOrderSales}}</div>
                                        </div>
                                        <div class="col-auto">
                                            <i class="fas fa-calendar fa-2x text-gray-300"></i>
                                        </div>
                                    </div>
</div>
</div>
</div>

<div class="col-md-3">
<div class="card">
    <div class="card-body">
  <div class="row no-gutters align-items-center">
                                        <div class="col mr-2">
                                            <div class="text-xs font-weight-bold text-success text-uppercase mb-1">
                                                PND Order Count({{$totalPndOrders}})</div>
                                            <div class="h5 mb-0 font-weight-bold text-gray-800">₹ {{$totalPndOrderSales}}</div>
                                        </div>
                                        <div class="col-auto">
                                            <i class="fas fa-dollar-sign fa-2x text-gray-300"></i>
                                        </div>
                                    </div>
</div></div>
</div>

<div class="col-md-3">
<div class="card">
    <div class="card-body">
  <div class="row no-gutters align-items-center">
                                        <div class="col mr-2">
                                            <div class="text-xs font-weight-bold text-info text-uppercase mb-1">Order Delivered
                                            </div>
                                            <div class="row no-gutters align-items-center">
                                                <div class="col-auto">
                                                    <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">0%</div>
                                                </div>
                                                <div class="col">
                                                    <div class="progress progress-sm mr-2">
                                                        <div class="progress-bar bg-info" role="progressbar" style="width:0%" 
                                                        aria-valuenow="{{$totalPndOrders+ $totalNormalOrders}}" aria-valuemin="0" aria-valuemax="{{$total_new_pndOrders+$total_new_normalOrders}}"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-auto">
                                            <i class="fas fa-clipboard-list fa-2x text-gray-300"></i>
                                        </div>
                                    </div>
</div></div>
</div>

<div class="col-md-3">
<div class="card">
    <div class="card-body">
        
<div class="row no-gutters align-items-center">
                                        <div class="col mr-2">
                                            <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">
                                                Total Sales</div>
                                            <div class="h5 mb-0 font-weight-bold text-gray-800">₹ {{ $totalnormalOrderSales + $totalPndOrderSales}}</div>
                                        </div>
                                        <div class="col-auto">
                                            <i class="fas fa-comments fa-2x text-gray-300"></i>
                                        </div>
                                    </div>

    </div></div>
</div>
 
 </div>
 

<div class="row ">
   
 <div class="col-md-8"> 
    <div class="card mt-3">
        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
        <h6 class="m-0 font-weight-bold text-primary">Earnings Chart</h6>
                                     
                                </div>
                                <!-- Card Body -->
                                <div class="card-body">
                                    <div class="chart-area">
                                       <canvas class="mt-5" height="120" id="sales-statistics-overview"></canvas>
                                    </div>
                                </div>
                            </div>
</div> 




<div class="col-md-4">
 <div class="card mt-3">
 <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
        <h6 class="m-0 font-weight-bold text-primary">Revenue Chart</h6>
                                     
                                </div>
<div class="card-body">
                                    <div class="chart-area">
                                        <canvas id=""></canvas>
                                    </div>
                                </div>
                            </div>

</div>
</div>






</div>

  

 




 


</div>

 
@endsection

@section("script")

<script>
 
$(function() {
  'use strict'; 

    var lineStatsOptions = {
      scales: {
        yAxes: [{
          display: false
        }],
        xAxes: [{
          display: false
        }]
      },
      legend: {
        display: false
      },
      elements: {
        point: {
          radius: 0
        },
        line: {
          tension: 0
        }
      },
      stepsize: 100
    }



    if ($('#sales-statistics-overview').length) {
      var salesChartCanvas = $("#sales-statistics-overview").get(0).getContext("2d");
      var gradientStrokeFill_1 = salesChartCanvas.createLinearGradient(0, 0, 0, 450);
      gradientStrokeFill_1.addColorStop(1, 'rgba(255,255,255, 0.0)');
      gradientStrokeFill_1.addColorStop(0, 'rgba(102,78,235, 0.2)');
      var gradientStrokeFill_2 = salesChartCanvas.createLinearGradient(0, 0, 0, 400);
      gradientStrokeFill_2.addColorStop(1, 'rgba(255, 255, 255, 0.01)');
      gradientStrokeFill_2.addColorStop(0, '#14c671');
  
      <?php 
        
        $dates = array();  
        $pnd_orders  = array();
        $normal_orders  = array();

        foreach($pnd_sales_chart_data as $item)
        {
          
          $dates[] =  '"'.  $item->serviceDate . '"' ;
          $pnd_orders[] =  $item->pndOrders ;
        }

        foreach($normal_sales_chart_data as $item)
        { 
          $normal_orders[] =  $item->normalOrder ;
        }

      ?>  

      var data_1_1 = [ <?php echo implode(',', $pnd_orders) ; ?> ]; 
      var data_1_2 = [ <?php echo implode(',', $normal_orders) ; ?> ];
      var areaData = {
        labels: [ <?php echo implode(",",  $dates ); ?>],
        datasets: [{
          label: 'PnD Orders',
          data: data_1_1,
          borderColor: infoColor,
          backgroundColor: gradientStrokeFill_1,
          borderWidth: 2
        }, {
          label: 'Normal Orders',
          data: data_1_2,
          borderColor: successColor,
          backgroundColor: gradientStrokeFill_2,
          borderWidth: 2
        }]
      };
      var areaOptions = {
        responsive: true,
        animation: {
          animateScale: true,
          animateRotate: true
        },
        elements: {
          point: {
            radius: 3,
            backgroundColor: "#fff"
          },
          line: {
            tension: 0
          }
        },
        layout: {
          padding: {
            left: 0,
            right: 0,
            top: 0,
            bottom: 0
          }
        },
        legend: false,
        legendCallback: function (chart) {
          var text = [];
          text.push('<div class="chartjs-legend"><ul>');
          for (var i = 0; i < chart.data.datasets.length; i++) { 
            text.push('<li>');
            text.push('<span style="background-color:' + chart.data.datasets[i].borderColor + '">' + '</span>');
            text.push(chart.data.datasets[i].label);
            text.push('</li>');
          }
          text.push('</ul></div>');
          return text.join("");
        },
        scales: {
          xAxes: [{
            display: false,
            ticks: {
              display: false,
              beginAtZero: false
            },
            gridLines: {
              drawBorder: false
            }
          }],
          yAxes: [{
            ticks: {
              max: 170,
              min: 0,
              stepSize: 50,
              fontColor: "#858585",
              beginAtZero: false
            },
            gridLines: {
              color: '#e2e6ec',
              display: true,
              drawBorder: false
            }
          }]
        }
      }
      var salesChart = new Chart(salesChartCanvas, {
        type: 'line',
        data: areaData,
        options: areaOptions
      });
      document.getElementById('sales-statistics-legend').innerHTML = salesChart.generateLegend(); 
    }
  


 
}) ;
 

    

 

</script> 

@endsection 