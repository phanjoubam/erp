@extends('layouts.erp_theme_03')

@section('content') 
<style>
 .timer{
  border-radius:8px;
  color:#e83e8c;
  background: white;
  border-color: black;
  font-size: 25px;
 }
 .confirm{
  border-radius:8px;
  font-size: 25px;
  background: #4e73df;
  border-color: black;
  color: white;
 }
 .cancel{
  border-radius:8px;
  font-size: 25px;
  background: red;
  border-color: black;
  color: white;
 }
 th 
  {
    border-bottom: solid 1px #adb1b1 !important;
    color: #343434 !important;
  }
  td  
  {
    border-bottom: dashed 1px #adb1b1 !important;
    padding-top: 5px;
    padding-bottom: 5px;
  }

  td img 
  {
    border-radius: 50px;
  }
</style>
<?php 
$order_info = $data['order_info'];
$order_items  = $data['order_items'];
$customer = $data['customer'];
$agent = $data['agent_info'];    
$order_no =$order_info->id ;
$active_agent = $data['agents_status']; 
?> 
<div class="row">
 <div class="col-md-12">
  <div class='card'> 
    <div class='card-body'>
      <div class="row">
        <div class="col-md-12">
         @if (session('err_msg'))
         <div class="alert alert-info p-0">
          {{session('err_msg')}}
        </div>
        @endif
      </div>
    </div> 
  </div> 
</div>
</div>
</div>
<div class="row">
  <div class="col-md-12"> 
    <div class="card card-default ">       
      <div class="card-body">
        <div class="row ">              
         <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12"> 
           <div class="card panel-default">
             <div class="card-header">
              <div class="card-title"> 
                <h5 class="card-category">
                  Order # {{ $order_info->id  }}
                  OTP: <span class='badge badge-info'>{{ $order_info->otp }}</span> 
                  @switch($order_info->book_status)

                  @case("new")
                  <span class='badge badge-primary'>New</span>
                  @break

                  @case("confirmed")
                  <span class='badge badge-info'>Confirmed</span>
                  @break
                  @case("order_packed")
                  <span class='badge badge-info'>Order Packed</span>
                  @break

                  @case("package_picked_up")
                  <span class='badge badge-info'>Package Picked Up</span>
                  @break

                  @case("pickup_did_not_come")
                  <span class='badge badge-warning'>Pickup Didn't Come</span>
                  @break

                  @case("in_route")
                  <span class='badge badge-success'>In Route</span>
                  @break

                  @case("completed")
                  <span class='badge badge-success'>Completed</span>
                  @break

                  @case("delivered")
                  <span class='badge badge-success'>Delivered</span>
                  @break

                  @case("delivery_scheduled")
                  <span class='badge badge-success'>Delivery Scheduled</span>
                  @break 
                  @case("canceled")
                  <span class='badge badge-danger'>Order Cancelled</span>
                  @break 
                  @case("cancelled")
                  <span class='badge badge-danger'>Order Cancelled</span>
                  @break 
                  @endswitch
                  <span> Order Date: {{ date('d-m-Y H:i:s', strtotime( $order_info->book_date)) }}</span> 
                  <span>Payment Mode:</span> 
                  @if( strcasecmp($order_info->payment_type,"Cash on delivery") == 0   || strcasecmp($order_info->payment_type,"COD") == 0  || strcasecmp($order_info->payment_type,"POD") == 0   )
                  <span class='badge badge-warning'>CASH</span>
                  @else 
                  <span class='badge badge-success'>ONLINE</span>
                  @endif
                </h5> 
              </div>
            </div>
            <div class="card-body">     
              @if($data['adminFrno'] == $order_info->route_to_frno)
              @else 
              <h5>Routed to: <span class="badge badge-success badge-pill">{{$data['routed_frno']->zone}} </span></h5>
              @endif  
              <table class="table table-responsive" border='0' cellpadding='1' cellspacing='1' style='width: 100% !important; margin-left:auto; margin-right:auto; font-size:13px; '  >

                <thead class="table-light">
                  <tr>
                    <th >Sl.No</th>
                    <th>Image</th>
                    <th style='width: 200px'>Item</th>
                    <th style='width: 80px'>Qty</th>
                    <th style='width: 50px'>Unit</th>
                    <th style='width: 140px'>Unit Price</th> 
                    <th style='width: 50px'>Packing</th> 
                    <th class='text-right' style='width: 220px'>Item + Packing = Sub-Total</th>
                  </tr>  
                </thead>
                <tbody class=""  style="font-weight:bold;color: black;">
                  <?php $i = 0 ;
                  $item_total  =0;
                  $pack_total = 0;
                  $sub_total = 0;
                  ?>
                  @foreach ( $order_items   as $item)

                  <?php $i++;

                  $item_total += ( $item->price * $item->qty ) ;
                  $sub_total += ( $item->price * $item->qty )  +  ( $item->package_charge * $item->qty );

                  $pack_total += ( $item->package_charge * $item->qty );
                  ?>
                  <tr class=" text-dark"><b>
                    <td>{{$i}}</td>
                    <td >
                      <img style="width: 30px;height:30px"   src="{{ $item->photos }}"  data-src="{{ $item->photos }}"
                      data-item="{{ $item->description }}" class="mr-3 showpreview" alt="{{$item->pr_name}}">  
                    </td>
                    <td style='width: 200px'>
                      <?php echo preg_replace('~((\w+\s){4})~', '$1' . "\n", $item->pr_name ) ;?><br/>
                      <small>{{ implode(' ', array_slice(str_word_count( $item->description ,1), 0,10)) }}</small>
                      <span style='cursor: pointer;' data-src="{{ $item->photos }}"  data-item="{{ $item->description }}"  class="showpreview">[ ... ]</span>
                    </td>
                    <td>
                      {{$item->qty}} <span class='badge badge-danger showconfirmdel' data-itemno="{{ $item->id }}" data-oid="{{ $order_info->id }}" title="Remove item from order"><i class="fa fa-times"></i></span> 
                    </td>   
                    <td>
                      {{$item->unit}}
                    </td> 
                    <td>
                     {{ $item->price }}  
                   </td>  
                   <td>
                     {{ $item->package_charge * $item->qty  }}  
                   </td>  

                   <td class='text-right'>
                     ( {{ $item->price }} X {{ number_format( $item->qty   , 0 , ".", "" )   }} ) +  ( {{ $item->package_charge }} X {{ number_format( $item->qty   , 0 , ".", "" )  }} )  =     {{  number_format(  ( $item->price * $item->qty )  +  ( $item->package_charge * $item->qty )  , 2, ".", "" ) }}  
                   </td> 
                  </b>

               </tr>
               @endforeach
               <tr>
                <td colspan="7">
                  <strong>Packaging Cost:</strong>
                </td>
                <td class='text-right'>
                  {{ number_format( $pack_total   , 2, ".", "" )   }}
                </td>
              </tr>
              <tr>
                <td colspan="7">
                  <strong>Total Item Cost:</strong>
                </td>
                <td class='text-right'>
                 {{ number_format(  $item_total , 2, ".", "" )   }}
                </td>
             </tr>
             <tr> 
              <td colspan="7">
                <strong>Total Cost: (Item Cost + Packaging Cost)</strong>
              </td>
              <td class='text-right'>
                {{ number_format( $order_info->total_cost, 2, ".", "" )   }} 
                </td>
              </tr> 
              <tr> 
                <td colspan="7">
                  <strong>To refund:</strong>
                </td>
                <td class='text-right'>
                  {{ number_format( $order_info->refunded, 2, ".", "" ) }} 
                  </td>
                </tr> 
                @if( Session::get('_user_role_') >= 10000  )
                @endif
                <tr>
                  <td colspan="7">
                    <strong>Actual Delivery Charge:</strong>
                  </td>
                  <td class='text-right'>
                    {{ number_format( $order_info->delivery_charge , 2, ".", "" )   }}
                  </td>
                </tr>
                <tr>
                  <td colspan="7">
                    <strong>Total Cost:</strong>
                  </td>
                  <td class='text-right'>
                    {{ number_format(  ( $sub_total + $order_info->delivery_charge - $order_info->refunded )   , 2, ".", "" )   }}
                  </td>
                </tr>
                @if($order_info->coupon)
                <tr>
                  <td colspan="7">
                    <strong>Discount Coupon:</strong> <button data-widget="rem-coupon" data-key='{{ $order_info->id }}' class='btn btn-sm btn-danger showmodal'>Remove</button>
                  </td>
                  <td class='text-right'>
                    <span class='badge badge-info'>{{ $order_info->coupon }}</span> 
                  </td> 
                </tr>
                @endif 
                <tr>
                  <td colspan="7">
                    <strong>Discount Applied:</strong>
                  </td>
                  <td class='text-right'>
                    {{ number_format( $order_info->discount , 2, ".", "" )   }} 
                  </td> 
                </tr>
                <tr>
                  <td colspan="7">
                    <strong>Total Payable After Discount:</strong>
                  </td>
                  <td class='text-right'>
                    {{ number_format(  ( $order_info->total_cost + $order_info->delivery_charge) - $order_info->discount   - $order_info->refunded , 2, ".", "" )   }}
                  </td>
                </tr>
              </tbody>
            </table> 
          </div>
        </div>
      </div>
      <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
        <!--  -->
        <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">           
          
             <div class="card card-default ">
              <div class="card-header">
                <h5 class="card-category">Availability Agent</h5> 
              </div>
              <div class="card-body"> 
               <div class="">
                 <h6 class="card-category">
                  Active agent available:

                  @if($active_agent ==0)
                  <span class='badge badge-warning badge-pill'>All agent are busy!</span>
                @else 
                  <span class='badge badge-info badge-pill'> Available</span>
                @endif                 
                </h6>
               </div>
           </div>
         </div>
       </div> 
     </div>
     <div class="card mt-2">

      <div class="container">
        <div class="row">
          <div class="col-md-12 mt-2">
            <div class="container">

              <div class="alert alert-dark timer text-center" id="timer{{$order_info->id}}" 
               data-key='{{ $order_info->id }}'
               data-value='{{$order_info->book_date}}'  value='{{$order_info->book_date}}' name='counter' >

               <strong>
                <b class="text-danger" id="display_timer{{ $order_info->id }}" ></b>
              </strong>
            </div>

            <div class="alert alert-success confirm text-center" data-toggle="modal" data-target="#modalepg" data-key="{{$order_info->id}}">
              <strong>Confirm</strong>
            </div>

            <div class="alert alert-danger cancel text-center"  data-toggle="modal" data-target="#modalepg" data-key="{{$order_info->id}}">
              <strong>Cancel</strong>
            </div>
          </div>
        </div>
      </div>
    </div> 
</div>
</div>
<!--  -->
</div>
</div>
</div>
</div>
</div>

<form action='{{action("Erp\ErpToolsController@addPreparationTime")}}' method='post' enctype="multipart/form-data" >
 {{ csrf_field() }}
 <div class="modal" id='widget-m2' tabindex="-1" role="dialog">
  <div class="modal-dialog  modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Add Preparation Time</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       <div class="form-row"> 

         <div class="col-md-8">
         <!--  <input class="form-control"   oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"
          type = "number"
          maxlength = "3" type="number" required min="1" max="120" name="prep_time" placeholder="Maximum 120 minutes..."> -->

          <input class="form-control minutesInput" type="number" min="10" max="120" value="" name="prep_time" placeholder="Maximum 120 minutes..."/>
        </div>
        <div class="col-md-4">minutes</div>

      </div>         
    </div>
    <div class="modal-footer mt-1">
      <input type='hidden' name='yes'  value="{{$order_info->is_confirmed}}" />
      <input type='hidden' name='orderno'  value="{{$order_info->id}}" />
      <button type="submit" class="btn btn-success" id="btnsaveconfirmation" name='btnsaveconfirmation' value='save'  >Save</button> 
      <button type="button" class="btn btn-danger" id="closeconfmodal" data-dismiss="modal">Close</button> 
    </div>
  </div>
</div>
</div>
</form>

<form action='{{ action("Erp\ErpToolsController@cancelPreparationTime") }}' method='post' enctype="multipart/form-data" >
 {{ csrf_field() }}
 <div class="modal" id='widget-m3' tabindex="-1" role="dialog">
  <div class="modal-dialog  modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Confirm Record Cancel</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="col-md-12 ">
          <label  class="form-label"> Remark:</label>
          <input type="text" name="remark" class=" form-control" >
        </div>
      </div>
      <div class="modal-footer mt-1">
        <input type='hidden' name='cancelled' />
        <input type='hidden' name='orderno'  value="{{$order_info->id}}" />
        <button type="submit" class="btn btn-success" id="btnsaveconfirmation" name='btnsaveconfirmation' value='save'  >Yes</button> 
        <button type="button" class="btn btn-danger" id="closeconfmodal" data-dismiss="modal">No</button> 
      </div>
    </div>
  </div>
</div>
</form>
<div class="modal fade" id="modalConfDelAgent" tabindex="-1" role="dialog" aria-labelledby="modalDisable" aria-hidden="true">
  <form method='post' action="">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="modalDisable">Confirmation</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <p>
            Are you sure you want to remove agent from this delivery task?
          </p>
        </div>
        <div class="modal-footer">
          <input type='hidden' name='key' id='key2'/> 
          <button type="submit" name="btnRemoveAgent" value="save" class="btn btn-primary btn-sm">Yes</button>
          <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">No</button> 
        </div>
      </div>
    </div>
  </form>
</div>

@endsection
@section('script')
<script>

 $(".minutesInput").on('keyup keypress blur change', function(e) {

    if($(this).val() > 120){
      $(this).val('120');
      return false;
    }

  });

  $(document).on("click", ".confirm", function()
  {
    $("#widget-m2").modal("show");
  });
  $(document).on("click", ".cancel", function()
  {
    $("#widget-m3").modal("show");
  });
  $(document).ready(function () {
    var timers = $(".timer");
    $.each(timers, function(i, v){
      var key = $(v).attr("data-key");
      var value = $(v).attr("data-value");
      var $date = new Date(value);
      var $countdown = new Date(value);
      $countdown.setMinutes($date.getMinutes()+5);
    //adding 5 minute to the time of order
    var countDownDate = new Date($countdown).getTime();
    var x = setInterval(function() {
      var now = new Date().getTime();
      var distance = countDownDate - now;
      var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
      var seconds = Math.floor((distance % (1000 * 60)) / 1000);

      var counter =  minutes + "m " + seconds + "s ";
      $("#display_timer" + key).html(counter);
      if (distance < 0) {
        clearInterval(x);
        $("#display_timer" + key).html("EXPIRED");
      }
    }, 1000);
  })
  });
</script>
@endsection

