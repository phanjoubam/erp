<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
*/


Route::post('/v3/pos/add-item-to-basket',  'Erp\PosController@addItemToCart') ; 
Route::post('/v3/pos/search-item-by-keyword',  'Erp\PosController@searchItemByKeyword') ;  
Route::post('/v3/pos/load-all-products',  'Erp\PosController@loadAllProducts') ; 
Route::post('/v2/web/customer-care/orders/assign-to-agent',  'Api\V2\WebControllerV2@assignOrderToAgent')->middleware('cors' );

Route::post('v3/web/customer-care/orders/pnd-assign-request', 'Api\V3\WebControllerV3@pndAssignRequest');