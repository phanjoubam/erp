<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes();
Route::get('/about-us', 'HomeController@aboutUs');
Route::get('/privacy-policy', 'HomeController@privacyPolicy');
Route::get('/terms-and-conditions', 'HomeController@termsOfUse');

/* ****************************************** */
//
//   ERP BLOCK
//
/* ****************************************** */
Route::get('/login', 'Auth\LoginController@erpLogin')->name('login');
Route::post('/login', 'Auth\LoginController@erpLogin')  ;
Route::get('/logout', 'UserController@logout');
// Route::get('/', 'Erp\ErpToolsController@erpDashboard')->middleware('auth' );
//sanatomba routes changes
Route::get('/', 'Erp\ErpToolsController@dashboard')->middleware('auth' );
Route::get('/Erp/customer-care/order/view-details/{orderno}', 'Erp\ErpToolsController@viewActiveOrderDetails')->middleware('auth' );
Route::get('/pos/overview',"Erp\ErpToolsController@overView")->middleware('auth');
Route::post('/erp/business/add-preparation-time', 'Erp\ErpToolsController@addPreparationTime')->middleware('auth' );
Route::post('/erp/business/cancel-preparation-time', 'Erp\ErpToolsController@cancelPreparationTime')->middleware('auth' );
//View Business Product Category
Route::get('/recent-products', 'Erp\ErpToolsController@viewRecentlyAddedItems')->middleware('auth' );
Route::get('/product-categories/all-products/{bin}/{category}', 'Erp\ErpToolsController@viewBusinessProducts')->middleware('auth' );

Route::get('/all-products', 'Erp\ErpToolsController@viewAllProducts')->middleware('auth' );
Route::get('/low-stock-products', 'Erp\ErpToolsController@viewLowStockProducts')->middleware('auth' );
Route::post('/product/remove-product-details', 'Erp\ErpToolsController@deleteProduct')->middleware('auth' );
 

Route::get('/product/edit-images', 'Erp\ErpToolsController@viewProductImages')->middleware('auth' );
Route::post('/product/upload-product-photo', 'Erp\ErpToolsController@uploadProductImage')->middleware('auth' );
Route::post('/product/remove-product-photo', 'Erp\ErpToolsController@removeProductImage')->middleware('auth' );

//Delete/Destroy Business products
Route::post('/erp/business/product-delete', 'Erp\ErpToolsController@destroyBusinessProducts')->middleware('auth' );
//Edit Business Product
Route::get('/erp/business/product-edit/{pr_id}', 'Erp\ErpToolsController@editBusinessProducts')->middleware('auth' );
//Edit Business Product save
Route::post('/erp/business/product-update', 'Erp\ErpToolsController@editBusinessProductSave')->middleware('auth' );
//View business earning report for daily
Route::get('/erp/business/get-daily-earning', 'Erp\ErpToolsController@viewBusinessDailyEarning')->middleware('auth' );
//Search business earning report for daily
Route::post('/erp/business/search-daily-earning', 'Erp\ErpToolsController@viewBusinessDailyEarning')->middleware('auth' );
//View business earning report for monthly
Route::get('/erp/business/get-monthly-earning', 'Erp\ErpToolsController@viewBusinessMonthlyEarning')->middleware('auth' );
//Search business earning report for monthly
Route::post('/erp/business/search-monthly-earning', 'Erp\ErpToolsController@viewBusinessMonthlyEarning')->middleware('auth' );

Route::post('/erp/orders/update-status', 'Erp\ErpToolsController@updateOrderStatus')->middleware('auth' );
//Generate pdf file for orders
Route::get('/erp/orders/sales/pdf/{order_no}', 'Erp\PDFController@pdfSalesDetails')->middleware('auth' );
Route::get('/erp/orders/add-new-order', 'Erp\ErpToolsController@addNewOrder')->middleware('auth' );
Route::post('/erp/orders/save-new-sales', 'Erp\ErpToolsController@saveNewOrder')->middleware('auth' );


//Services
Route::post("/erp/pos/order/generate-and-download-receipt", "Erp\PosController@downloadReceipt");


Route::get('/order/view-details/{orderno}', 'Erp\ErpToolsController@viewOrderDetails')->middleware('auth' );
Route::get('/order/view-print-receipt', 'Erp\ErpToolsController@viewPrintableReceipt')->middleware('auth' );


Route::get('/order/print-receipt', 'Erp\ErpToolsController@generatePrintableReceipt')->middleware('auth' );



Route::get('/services/manage-services', 'Erp\ErpToolsController@manageServices')->middleware('auth' );
Route::get('/services/add-new-service', 'Erp\ErpToolsController@manageServices')->middleware('auth' );



Route::get('/services/add-new-staff', 'Erp\ErpToolsController@addStaff')->middleware('auth' );
Route::post('/services/save-new-staff', 'Erp\ErpToolsController@saveStaff')->middleware('auth' );

Route::get('/services/manage-staffs', 'Erp\ErpToolsController@manageStaffs')->middleware('auth' );
Route::post('/services/remove-staff', 'Erp\ErpToolsController@removeStaff')->middleware('auth' );

Route::get('/services/staff/add-day-off', 'Erp\ErpToolsController@addStaffDayOff')->middleware('auth' );
Route::post('/services/save-day-off', 'Erp\ErpToolsController@saveStaffDayOff')->middleware('auth' );

Route::get('/services/add-new-service', 'Erp\ErpToolsController@addServiceDetails')->middleware('auth' );
Route::get('/services/manage-services/edit/{serviceid}', 'Erp\ErpToolsController@editServiceDetails')->middleware('auth' );
Route::post('/services/update-service-details', 'Erp\ErpToolsController@updateServiceDetails')->middleware('auth' );
Route::post('/services/save-service-details', 'Erp\ErpToolsController@saveServiceDetails')->middleware('auth' );


Route::get('/services/add-new-booking', 'Erp\ErpToolsController@addNewBooking')->middleware('auth' );
Route::get('/services/save-new-booking', 'Erp\ErpToolsController@saveNewBooking')->middleware('auth' );


Route::get('/services/view-bookings', 'Erp\ErpToolsController@manageBooking')->middleware('auth' );


Route::get('/services/view-queue', 'Erp\ErpToolsController@viewQueue')->middleware('auth' );
Route::post('/services/update-booking-status', 'Erp\ErpToolsController@updateBookingStatus')->middleware('auth' );


Route::get('/services/print-receipt', 'Erp\ErpToolsController@generateServiceBill')->middleware('auth' );

Route::get('/export/daily-sales-and-service-export-to-excel','Erp\ErpToolsController@MerchantDailyOrdersExport')->middleware('auth' );




//orders related
Route::get('/orders/view-active-orders', 'Erp\ErpToolsController@viewActiveOrders')->middleware('auth' );
Route::get('/orders/pnd/view-details/{orderno}', 'Erp\ErpToolsController@viewPnDOrderDetails')->middleware('auth' );

Route::get('/orders/view-completed-orders', 'Erp\ErpToolsController@viewCompletedOrders')->middleware('auth' );

Route::post('/orders/view-completed-orders', 'Erp\ErpToolsController@viewCompletedOrders')->middleware('auth' );
Route::get('/orders/search-all-orders', 'Erp\ErpToolsController@searchAllOrders')->middleware('auth' );
Route::post('/orders/search-all-orders', 'Erp\ErpToolsController@searchAllOrders')->middleware('auth' );


//CMS
Route::get('/admin/cms', 'Admin\CMSController@cmsDashboard')->middleware('auth' );
Route::get('/admin/cms/menus', 'Admin\CMSController@viewExistingCMSMenu')->middleware('auth' );

Route::get('/grid/agent-live-position', 'Admin\AdminDashboardController@agentLivePosition')->middleware('auth' );
Route::get('/admin/report/monthly-earning', 'Admin\AdminDashboardController@monthlyEarningReport')->middleware('auth' );
Route::get('/admin/report/daily-sales', 'Admin\AdminDashboardController@dailySalesReport')->middleware('auth' );






//remove booking

Route::get('/services/remove-bookings','Erp\ErpToolsController@removeBooking');

//product adding section

Route::get('/admin/product/enter-save-product', 'Erp\ErpToolsController@enterSaveProduct')->middleware('auth');
Route::post('/admin/product/save-product', 'Erp\ErpToolsController@saveProduct')->middleware('auth');
Route::get('/admin/product/show-product', 'Erp\ErpToolsController@showProduct')->middleware('auth');
Route::get('/admin/product/view-product-list', 'Erp\ErpToolsController@viewProductList')->middleware('auth');

//pick and drop erp for merchant
Route::get('/admin/customer-care/pick-and-drop-orders/view-all',
	'Erp\ErpToolsController@viewPickAndDropOrders')->middleware('auth');
Route::post('/admin/customer-care/pick-and-drop-orders/view-all',
	'Erp\ErpToolsController@viewPickAndDropOrders')->middleware('auth');

Route::get('/customer-care/add-pick-and-drop-orders',
	'Erp\ErpToolsController@addPickAndDropOrders')->middleware('auth');

Route::post('/admin/customer-care/pick-and-drop-orders/new-request', 'Erp\ErpToolsController@newPickAndDropRequest')->middleware('auth');
//convert receipt to order
Route::post('/admin/customer-care/convert-receipt-to-pick-and-drop-order',
	'Erp\ErpToolsController@convertReceiptToPickAndDropRequest')->middleware('auth');
Route::post('/admin/customer-care/pick-and-drop-orders/remove-assigned-agent',
	'Erp\ErpToolsController@removeAgentFromPnDOrder')->middleware('auth');
Route::post('/admin/pick-and-drop-orders/approve-self-assign-request',
	'Erp\ErpToolsController@processAgentSelfAssignRequest')->middleware('auth');
Route::post('/admin/order/update-pnd-remarks',
	'Erp\ErpToolsController@updatePnDOrderRemarks')->middleware('auth');
Route::get('/admin/customer-care/pnd-order/view-details/{orderno}',
	'Erp\ErpToolsController@viewPnDOrderDetailsforErp')->middleware('auth');

Route::get('/admin/customer-care/assist-order/view-details/{orderno}',
	'Erp\ErpToolsController@viewAssistOrderDetails')->middleware('auth');
Route::post('/admin/systems/order/remove-coupon-code',
	'Erp\ErpToolsController@removeCouponDiscount');
Route::post('/admin/customer-care/pick-and-drop-orders/remove-assigned-agent',
	'Erp\ErpToolsController@removeAgentFromPnDOrder')->middleware('auth');
// Route::post('/admin/order/update-pnd-remarks',
// 	'Erp\ErpToolsController@updatePnDOrderRemarks')->middleware('auth' );
Route::post('/admin/pnd-order/update-payment-mode',
	'Erp\ErpToolsController@updatePnDOrderPaymentUpdate')->middleware('auth' );

Route::post('/admin/pnd-order/update-order-status',
	'Erp\ErpToolsController@updatePnDOrderStatus')->middleware('auth' );

Route::get('/admin/customer-care/pick-and-drop-orders/view-completed',
	'Erp\ErpToolsController@viewPickAndDropCompletedOrders')->middleware('auth');
Route::post('/admin/customer-care/pick-and-drop-orders/view-completed',
	'Erp\ErpToolsController@viewPickAndDropCompletedOrders')->middleware('auth');

Route::post('/admin/remove-pick-and-drop-orders',
	'Erp\ErpToolsController@removePickAndDropOrders')->middleware('auth');

Route::post('/admin/order/assist/update-information',  'Erp\ErpToolsController@updateAssistOrder')->middleware('auth');


Route::get('/product/enter-sales-details','Erp\SalesController@enterSalesDetails')->middleware('auth');
Route::post('/product/enter-sales-details','Erp\SalesController@enterSalesDetails')->middleware('auth');

Route::get('/pos/prepare-sales-window','Erp\PosController@prepareSalesWindow')->middleware('auth');
Route::post('/pos/remove-item-from-sales-window','Erp\PosController@removeItemFromSalesWindow')->middleware('auth');

Route::post('/pos/save-order','Erp\PosController@placePosOrder')->middleware('auth');
Route::get('/pos/view-sales-orders','Erp\PosController@viewPosSalesOrders')->middleware('auth');
Route::get('/pos/view-order-details','Erp\PosController@viewPosOrdersDetails')->middleware('auth');
Route::get('/pos/orders/view-bill','Erp\PosController@downloadPosReceipt')->middleware('auth');

Route::get('/pos/pos-sales-window-touch-and-go','Erp\PosController@postSalesWindowTouchAndGo')->middleware('auth');
Route::post('/pos/remove-temporary-cart','Erp\PosController@removeTemporaryCart')->middleware('auth');

Route::get('/pos/pos-sales-window-touch-and-go-2','Erp\PosController@postSalesWindowTouchAndGo2')->middleware('auth');
Route::get('/pos/checkout-order','Erp\PosController@checkoutOrder')->middleware('auth');

Route::get('/product/suggest-product-list', 'Erp\SalesController@suggestProductList')
->middleware('auth' );
Route::get('/product/show-product-list', 'Erp\SalesController@showProductList')
->middleware('auth' );

Route::get('/product/delete-product-temp-cart', 'Erp\SalesController@deleteProductTempCart')
->middleware('auth' );

Route::get('/product/search-product-order', 'Erp\ErpToolsController@searchByDataEntered')
->middleware('auth' );

Route::get('/product/order-by-keyword', 'Erp\ErpToolsController@orderByKeyword')
->middleware('auth' );



Route::get('/bill/generate-receipt', 'Erp\ErpToolsController@downloadReceiptForErp')
->middleware('auth' );

Route::get('/reports/sales-analytics', 'Erp\ErpToolsController@salesAnalytics')
->middleware('auth' );

Route::get('/reports/billing-and-clearance/daily-sales', 'Erp\AccountsAndBillingController@dailySalesReport')
->middleware('auth' );

Route::get('/reports/sales-and-service-per-cycle','Erp\AccountsAndBillingController@cyclewiseSalesServiceReport')->middleware('auth' );
Route::get('/reports/monthly-earning', 'Erp\ErpToolsController@monthlyEarningReport')->middleware('auth' );

Route::get('/reports/monthly-sales-and-service-earning','Erp\AccountsAndBillingController@monthlySalesServiceEarningReport')
->middleware('auth' );


Route::get('reports/billing-and-clearance/sales-and-clearance-report','Erp\AccountsAndBillingController@salesAndClearanceReport')
->middleware('auth');

Route::post('reports/billing-and-clearance/sales-and-clearance-report','Erp\AccountsAndBillingController@salesAndClearanceReport')
->middleware('auth');

Route::get('products/create-coupon','Erp\ErpToolsController@createCoupon')
->middleware('auth');


Route::get('products/get-product-by-category','Erp\ErpToolsController@getProductByCategory')
->middleware('auth');

Route::get('products/product-select-list','Erp\ErpToolsController@productSelectList')
->middleware('auth');

 Route::post('products/save-coupon','Erp\ErpToolsController@saveCoupon')
->middleware('auth');

 Route::get('clearance/show-payment-due','Erp\ErpToolsController@showPaymentDue')
->middleware('auth');

 Route::post('clearance/show-payment-due','Erp\ErpToolsController@showPaymentDue')
->middleware('auth');

Route::post('/clearance/prepare-clearance-dues-form', 'Erp\ErpToolsController@salesClearanceForm')->middleware('auth' );
 Route::post('clearance/save-sales-clearance','Erp\ErpToolsController@saveSalesClearance')
->middleware('auth');

 Route::get('theme/update-theme','Erp\ErpToolsController@updateTheme')
->middleware('auth');

Route::get('/product/edit-images-fix', 'Erp\ErpToolsController@editImageFix')->middleware('auth' );



Route::get('/tools/prepare-product-import', 'Erp\ErpToolsController@prepareProductImportExcel')->middleware('auth' );
Route::post('/tools/save-product-import', 'Erp\ErpToolsController@uploadProductImportExcel')->middleware('auth');
Route::get('/tools/view-imported-products', 'Erp\ErpToolsController@viewProductImportExcel')->middleware('auth');
Route::post('/tools/import-products', 'Erp\ErpToolsController@saveImportableProducts')->middleware('auth');  
Route::post('/tools/import-single-product', 'Erp\ErpToolsController@saveImportableProduct')->middleware('auth');
// crs
Route::get('/view-car-packages', 'Erp\CarRentalServiceController@viewCarPages')->middleware('auth');

Route::match(array('GET', 'POST'), '/admin/business/add-car-rental-services',
'Erp\CarRentalServiceController@addCarRentalServices')->middleware('auth' );
Route::match(array('GET', 'POST'), '/admin/customer-care/car-rental-service-update',
'Erp\CarRentalServiceController@editCarRentalService')->middleware('auth');
Route::post('/admin/customer-care/delete-car-rental-service', 'Erp\CarRentalServiceController@deleteCarRentalServices')->middleware('auth');
Route::get('/admin/customer-care/business/view-more-details-crs/{srv_prid}','Erp\CarRentalServiceController@viewMoreDetailsCRS')->middleware('auth');
Route::post('/admin/customer-care/business/add-service-package-spec', 'Erp\CarRentalServiceController@addServicePackageSpec')->middleware('auth');
Route::post('/admin/customer-care/business/edit-service-package-spec', 'Erp\CarRentalServiceController@editServicePackageSpec')->middleware('auth');
Route::post('/admin/customer-care/business/delete-service-package-spec', 'Erp\CarRentalServiceController@deleteServicePackageSpec')->middleware('auth');
//
Route::get('/pick-and-drop-orders/view-active-orders', 'Erp\ErpToolsController@pickAndDropActiveOrders')->middleware('auth' );
// Route::get('/orders/pnd/view-details/{orderno}', 'Erp\ErpToolsController@viewPnDOrderDetails')->middleware('auth' );
