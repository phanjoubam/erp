<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductClothingModel extends Model
{
    protected $table = 'ba_product_clothing';
    public $timestamps = false;

}
