<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;



class User extends Authenticatable
{
    use Notifiable; 
	protected $table = 'ba_users';

	protected $fillable = [ 'bin', 'profile_id', 'phone', 'password', 'category', 'status',  ];

    protected $hidden = [
        'password', 'remember_token',
    ];

}
