<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomerReviewModel extends Model
{
    protected $table = 'ba_customer_reviews';
    public $timestamps = false;
    
}
