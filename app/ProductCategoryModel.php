<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductCategoryModel extends Model
{
    protected $table = 'ba_product_category';
    public $timestamps = false;

}
