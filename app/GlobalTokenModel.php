<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GlobalTokenModel extends Model
{
    protected $table = 'ba_global_token';
    public $timestamps = false;
}
