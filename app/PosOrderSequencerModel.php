<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PosOrderSequencerModel extends Model
{
	protected $table = 'ba_pos_order_sequencer';
    public $timestamps = false;
}
