<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PremiumBusiness extends Model
{
    protected $table = 'ba_premium_business';
    public $timestamps = false;
}
