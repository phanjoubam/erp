<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FavouriteBusiness extends Model
{
	protected $table = 'ba_fav_business';
}
