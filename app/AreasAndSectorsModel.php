<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AreasAndSectorsModel extends Model
{
    protected $table = 'ba_areas_sectors';
    public $timestamps = false;

}
