<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CouponRequestModel  extends Model
{
	protected $table = 'ba_coupons_request';
	public $timestamps = false;
}
