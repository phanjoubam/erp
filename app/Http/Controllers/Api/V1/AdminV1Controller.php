<?php

namespace App\Http\Controllers\Api\V1;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Crypt;  
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\File;
use Illuminate\Contracts\Encryption\DecryptException;
use App\Http\Controllers\Controller;



use App\User;
use App\Business;
use App\ServiceBooking;


 
use App\Traits\Utilities;

class AdminV1Controller extends Controller
{
	use Utilities;

	public function processBooking(Request $request)
   {

	   	$business = Business::find($request->bin);  
	    if( !isset($business))
	    {
	      
	      $data= ["message" => "failure",  'results' => array(),  "status_code" =>  999 ,
	      'detailed_msg' => 'Business information not found!'  ];
	      return json_encode( $data);
	      
	    } 
	
		if( $request->bookNo == "" || $request->action == ""  )
	   {
	     $data= ["message" => "failure", "status_code" =>  902 , 'detailed_msg' => 'Missing data to perform action.'  ];
	     return json_encode($data);
	   }


	   //generate a confirmation OTP
	   $bookinfo = ServiceBooking::find($request->bookNo); 
 


	   $sms_body ="";

	   if(isset($bookinfo))
	   {
	   		$bookinfo->book_status = $request->action;
	   		$save =$bookinfo->save();

	   		if(	 $save  )
			{ 
				$message = "success";
				$err_code = 1057;
				$err_msg = "Booking status updated.";

				switch($request->action )
				{
					case "confirmed":
						$sms_body = "Your booking is confirmed.";
						break;
					case "completed":
						$sms_body = "Your service is completed.";
						break;
					case "cancel_by_client":
						$sms_body = "Your booking is cancelled. Thanks for using our service.";
						break;
					case "cancel_by_owner":
						$sms_body = "Your booking is cancelled. Thanks for using our service.";
						break;  
				} 

			}
			else 
			{ 
				$message = "failure";
				$err_code = 1058;
				$err_msg = "Registration failed!";
			}

	   }
	   else 
	   {
	   	$message = "failure";
		$err_code = 999;
		$err_msg = "Booking information not found.";
	   }
 

 	if($sms_body != "")	
	{

		$customer = User::find($bookinfo->book_by);

 		if(isset($customer ) )
 		{
 			$phones = array($customer->phone); 
			$this->sendSmsAlert($phones, $sms_body);

 		} 
		
	}


	
	$data= ["message" => $message , "status_code" => $err_code ,  'detailed_msg' => $err_msg   ]; 
    return json_encode( $data);
	
  }

	
}
