<?php

namespace App\Http\Controllers\Api\V2;

 

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Crypt;  
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\File;
use Illuminate\Contracts\Encryption\DecryptException;
use App\Http\Controllers\Controller;



use App\Traits\Utilities;
use App\ProductCategoryModel;
use App\Business;
use App\ProductModel;
use App\BusinessTableReservation;
use App\RestaurantMenuAddon;

class RestaurantsApiControllerV2 extends Controller
{
	use Utilities;

	protected function getAllProductCategories(Request $request)
	{

		$business_category = $request->businessCategory;

		if($business_category == "")
		{
		    $product_groups = ProductCategoryModel::
		    select("category_name as category", "icon_path as iconUrl"  )  
			->get() ;
		}
		else 
		{
		    $product_groups = ProductCategoryModel::
		    where("business_category", $business_category) 
			->select("category_name as category", "icon_path as iconUrl"  )  
			->get() ;
		}

		foreach($product_groups as   $item)
		{
		    $source = base_path() . $item->iconUrl;
			if( $item->iconUrl != "")
			{
				$item->iconUrl = URL::to("/") .  $item->iconUrl  ;
			}
			else 
			{
				$item->iconUrl =  URL::to("/public/assets/app/any-product.png"); 
			}

		} 

		$data= ["message" => "success", "status_code" =>  5101,  'detailed_msg' => 'Product categories fetched.' , 
		'results' => $product_groups];

		return json_encode( $data);


	}


	protected function getProductCategories(Request $request)
  {

      $business_category = $request->businessCategory;
     

      $user_info =  json_decode($this->getUserInfo($request->userId));
        if(  $user_info->user_id == "na" )
        {
          $data= ["message" => "failure", "status_code" =>  901 , 
          'detailed_msg' => 'User has no priviledge to access this feature! Please consult admin.'  ];
          return json_encode($data);
        }
 
        if( $request->bin  == "")
        {
            $data= ["message" => "failure", "status_code" =>  902 , 'detailed_msg' => 'Missing data to perform action.'  ];
            return json_encode($data);
        }

 

        $business_info = Business::find( $request->bin ); 
        if( !isset($business_info) )
        {
            $data= ["message" => "failure", "status_code" =>  999 , 'detailed_msg' => 'No matching business found.'  ];
            return json_encode($data);
        }

 
 		if($business_category == "")
 		{
			$productCategories = DB::table("ba_product_category") 
			->select("category_name as categoryName", "icon_path as iconUrl" , DB::raw("'0' itemCount") ) 
			->get() ;

			$products  = DB::table("ba_products")
			->where("bin",$business_info->id)
			->select("category as categoryName",  DB::raw('count(*) as itemCount'))
			->groupBy("category")
			->get() ;

 		}
        else{
			$productCategories = DB::table("ba_product_category") 
			->where("business_category", $business_category)
			->select("category_name as categoryName", "icon_path as iconUrl" , DB::raw("'0' itemCount") ) 
			->get() ;

			$products  = DB::table("ba_products")
			->where("bin",$business_info->id)
			->select("category as categoryName",  DB::raw('count(*) as itemCount'))
			->groupBy("category")
			->get() ;
        }
        
        foreach($productCategories as   $item)
        {
			foreach($products as   $product)
			{
				if($product->categoryName == $item->categoryName)
				{
					$item->itemCount = $product->itemCount;
				} 
			}

			$source = base_path() . $item->iconUrl;
			if( $item->iconUrl != "" &&  file_exists(  $source )  )
			{
				$item->iconUrl = URL::to("/") .  $item->iconUrl  ;
			}
			else 
			{
				$item->iconUrl = URL::to("/public/assets/app/any-product.png" ); 
			} 
        }
		
		$data= ["message" => "success", "status_code" =>  5103 ,
		'detailed_msg' => 'Product categories fetched.', 
		'results' => $productCategories ];
		return json_encode( $data); 
    }

	public function addMenuCategories(Request $request)
  	{ 
  		//requested filled
  		$categoryName = $request->categoryName;
        $categoryDescription = $request->categoryDescription;
        $businessCategory = $request->businessCategory;
        $photo= $request->file('photo');

            
		if($categoryName == "" || $categoryDescription == "" || $businessCategory == "")
		{
			$data= ["message" => "failure", "status_code" =>  902 , 'detailed_msg' => 'Missing data to perform action.'  ];
			return json_encode($data);
		}
    
        $productCategory = ProductCategoryModel::where('category_name', $categoryName)
             ->first();

        if(!isset($productCategory)) 
		{
			$productCategory= new ProductCategoryModel;
		}
          
		$productCategory->category_name = $categoryName ;
		$productCategory->category_desc = $categoryDescription ;

		if($photo != "")
		{
			$imageName = "ico_" .  strtolower($request->categoryName) . time() . '.' .$request->photo->getClientOriginalExtension();
			$request->photo->move(public_path('/assets/app/icons'), $imageName); 
		
			$productCategory->icon_path = '/public/assets/app/icons/'.$imageName ;
		}
		$productCategory->business_category = $businessCategory ;

        $save = $productCategory->save();
        if($save)
		{
			$data= ["message" => "success", "status_code" =>  5105 ,  'detailed_msg' => 'Product Category is added successfully.'];
		}
		else{
			$data= ["message" => "failure", "status_code" =>  5106 ,  'detailed_msg' => 'Please Try Again.'];
		}
		return json_encode( $data);
           
          
  	}
  	protected function addRestaurantMenu(Request $request)
    {

      $user_info =  json_decode($this->getUserInfo($request->userId));
     	
      if(  $user_info->user_id == "na" )
        {
            $data= ["message" => "failure", "status_code" =>  901 , 
            'detailed_msg' => 'User has no priviledge to access this feature! Please consult admin.'  ] ;
            return json_encode($data);
        } 
 
        
        if( $request->menuName == "" ||  $request->description == "" ||  $request->categoryName== "" )
        {
    			$data= ["message" => "failure", "status_code" =>  902 , 'detailed_msg' => 'Missing data to perform action.'  ];
    			return json_encode($data);
        }

        $menu_code_count  = ProductModel::where("bin", $request->bin )
        ->where("category", $request->categoryName ) 
        ->count() + 1;

        $todays = date('Y-m-d');
        $startTime = date('H:i:s', strtotime($request->duration));
        $durations = $todays." ".$startTime;

        $id =   DB::table('ba_products') 
        ->max('id'); 

        $prCode = "BPR" .  $request->bin . "" . ( $id + 1)  ;  

        $restaurantMenu = new ProductModel ; 
        $restaurantMenu->bin = $request->bin;
        $restaurantMenu->pr_code =  $prCode ;
        $restaurantMenu->pr_name = $request->menuName;
        $restaurantMenu->description = $request->description;
        $restaurantMenu->stock_inhand = 100;
        $restaurantMenu->max_order = 100;
        $restaurantMenu->category = $request->categoryName; 
        $restaurantMenu->prep_duration =  $durations;
        $restaurantMenu->unit_price = $request->unitPrice; 
        $restaurantMenu->unit_value =  $request->unitValue ; 
        $restaurantMenu->unit_name =  $request->unitName ; 
        
        if( $request->discountPc != "")
        {
          $restaurantMenu->discountPc =   $request->discountPc ;
          $restaurantMenu->actual_price = $request->unitPrice - ( ( $request->discountPc / 100 ) * $request->unitPrice );
        }

        if( $request->discount != "")
        {
          $restaurantMenu->discount =   $request->discount ;
          $restaurantMenu->actual_price = $request->unitPrice - $request->discount;
        }

        $restaurantMenu->packaging = $request->packageCharge ;  
        $restaurantMenu->pack_type = $request->packageType ;
        $restaurantMenu->food_type = $request->foodType ;


        if($request->selectedfreeText!=null)
        {
        	$restaurantMenu->free_text = $request->selectedfreeText;
        }
        $property=$request['customize'];

        if(count($property)>0)
        {
        	$restaurantMenu->addon_available = "yes";
        }

        
        $save=$restaurantMenu->save(); 

        if($save)
        { 
		
    			$properties=$request['customize'];
    			for($i=0;$i<sizeof($properties);$i++)
    			{
    				$cumtomize=$properties[$i];
    				$cusname = $cumtomize['name'];
    				$cusprice = $cumtomize['price'];
    				$customization = new RestaurantMenuAddon;
    				$customization->menu_id= $restaurantMenu->id;
            $customization->pr_code= $prCode;
    				$customization->addon_name= $cusname;
    				$customization->price= $cusprice;
    				$customization->save();
    			}	

          $data= ["message" => "success", "status_code" =>  5007 ,  'detailed_msg' => 'Menu added successfully.', 'menuId' => $restaurantMenu->id ,'menuName' => $restaurantMenu->pr_name,'menuCode' => $restaurantMenu->pr_code];
        }
        else
        {
        	$data= ["message" => "failure", "status_code" =>  5008 ,  'detailed_msg' => 'Menu could not be added.', 'menuId' =>  0  ];
        }
    	return json_encode( $data);


    }
    protected function updateProductDetails(Request $request)
	{

		$user_info =  json_decode($this->getUserInfo($request->userId));
		if(  $user_info->user_id == "na" )
		{
		$data= ["message" => "failure", "status_code" =>  901 , 
		'detailed_msg' => 'User has no priviledge to access this feature! Please consult admin.'  ] ;
		return json_encode($data);
		} 



		if( $request->productId  == "" || $request->productName == "" || $request->description == "" ||  $request->categoryName== "" )
		{
		$data= ["message" => "failure", "status_code" =>  902 , 'detailed_msg' => 'Missing data to perform action.'  ];
		return json_encode($data);
		}


		$product = ProductModel::find( $request->productId ) ; 

		if(  !isset($product) )
		{
		$data= ["message" => "failure", "status_code" =>  999 , 
		'detailed_msg' => 'No product information found.'   ];
		return json_encode($data);
		}

    $product->pr_name = $request->productName;  
		$product->description = $request->description; 
		$product->category = $request->categoryName; 
		$product->unit_price = $request->unitPrice; 
		$save=$product->save(); 

		if($save)
		{
			$data= ["message" => "success", "status_code" =>  5013 ,  'detailed_msg' => 'Product updated successfully.'];
		}
		else
		{
			$data= ["message" => "failure", "status_code" =>  5014 ,  'detailed_msg' => 'Product could not be updated.'];
		}
		return json_encode( $data);


	}
	protected function updateMenuPrice(Request $request)
     {

        $user_info =  json_decode($this->getUserInfo($request->userId));
        if(  $user_info->user_id == "na" )
        {
            $data= ["message" => "failure", "status_code" =>  901 , 
            'detailed_msg' => 'User has no priviledge to access this feature! Please consult admin.'  ] ;
            return json_encode($data);
        } 
 
        
        if( $request->menuId  == ""   || $request->unitPrice == ""  )
        {
         $data= ["message" => "failure", "status_code" =>  902 , 'detailed_msg' => 'Missing data to perform action.'  ];
         return json_encode($data);
        }


         $restaurantMenu = ProductModel::find($request->menuId ) ; 
       
       if(  !isset($restaurantMenu) )
        {
            $data= ["message" => "failure", "status_code" =>  999 , 
            'detailed_msg' => 'No menu information found.' ];
            return json_encode($data);
        } 
  
         
        $restaurantMenu->unit_price = $request->unitPrice;
       
        if( $request->discountPc != "")
        {
          $restaurantMenu->discountPc =   $request->discountPc ;
          $restaurantMenu->actual_price = $request->unitPrice - ( ( $request->discountPc / 100 ) * $request->unitPrice );
        }

        if( $request->discount != "")
        {
          $restaurantMenu->discount =   $request->discount ;
          $restaurantMenu->actual_price = $request->unitPrice - $request->discount;
        }

        

        $save=$restaurantMenu->save(); 

        if($save)
        {
            $data= ["message" => "success", "status_code" =>  5015 ,  'detailed_msg' => 'Menu updated successfully.'];
        }
        else
        {
            $data= ["message" => "failure", "status_code" =>  5016 ,  'detailed_msg' => 'Menu could not be updated.'];
        }
        return json_encode( $data);
        
     }
     public function saveBusinessTable(Request $request)
  	{ 
  		//requested filled
  		$userId = $request->userId;
        $bin = $request->bin;
        $businessCategory = $request->businessCategory;
        $tableNumber= $request->totalTable;


		if($userId == "" || $bin == "" || $tableNumber == "")
		{
			$data= ["message" => "failure", "status_code" =>  902 , 'detailed_msg' => 'Missing data to perform action.'  ];
			return json_encode($data);
		}
		$business_info = Business::find( $bin ); 
        if( $business_info->category!=$businessCategory)
        {
            $data= ["message" => "failure", "status_code" =>  998 , 'detailed_msg' => 'No matching business found to this catgory.'  ];
            return json_encode($data);
        }
		$tableDetails = BusinessTableReservation::where('bin',$bin)
				->orderby('id', 'ASC')->get();

		if(isset($tableDetails))
		{
			foreach ($tableDetails as $keyidd) 
			{
				$serviceData = BusinessTableReservation::find($keyidd->id);
				$serviceData->delete();
			}
			
		}
    
        for ($x = 0; $x < $tableNumber; $x++) {
		$businessTable= new BusinessTableReservation;
		$businessTable->bin = $bin ;
		$businessTable->table_reservation = $x+1 ;
	    $save = $businessTable->save();
    	}
        if($save)
		{
			$data= ["message" => "success", "status_code" =>  5017 ,  'detailed_msg' => 'Add Business Table is added successfully.'];
		}
		else{
			$data= ["message" => "failure", "status_code" =>  5018 ,  'detailed_msg' => 'Please Try Again.'];
		}
		return json_encode( $data);
           
          
  	}
  	public function markItemAvailable(Request $request)
    {

        $user_info =  json_decode($this->getUserInfo($request->userId));  

        if(  $user_info->category != 1 )
        {
            $data= ["message" => "failure", "status_code" =>  901 , 
            'detailed_msg' => 'Only business owner has access to this feature! Please signin using your business acount.'  ] ;
            return json_encode($data);
        } 

  

      $restaurantMenu  = ProductModel::where("id", $request->menuId )
      ->where("bin", $user_info->bin )
      ->first() ;

      if( !isset($restaurantMenu))
      {
        $data = [ "message" => "failure",  'status_code' => 999, 'detailed_msg' => 'Menu not found.'  ];
        return json_encode($data); 
      }
 
      $restaurantMenu->stock_inhand =  100 ;  
      $restaurantMenu->save();


 

      $data = [ "message" => "success",  'status_code' => 8001, 'detailed_msg' => 'Item is marked available.'  ];
     
      return json_encode($data); 
     
    }


    



     public function markOutOfItem(Request $request)
    {

        $user_info =  json_decode($this->getUserInfo($request->userId));  

        if(  $user_info->category != 1 )
        {
            $data= ["message" => "failure", "status_code" =>  901 , 
            'detailed_msg' => 'Only business owner has access to this feature! Please signin using your business acount.'  ] ;
            return json_encode($data);
        } 

  

      $restaurantMenu  = ProductModel::where("id", $request->menuId )
      ->where("bin", $user_info->bin )
      ->first() ;

      if( !isset($restaurantMenu))
      {
        $data = [ "message" => "failure",  'status_code' => 999, 'detailed_msg' => 'Menu not found.'  ];
        return json_encode($data); 
      }
 
      $restaurantMenu->stock_inhand =  0 ;  
      $restaurantMenu->save();

 

      $data = [ "message" => "success",  'status_code' => 8003, 'detailed_msg' => 'Item is marked not available.'  ];
     
      return json_encode($data); 
     
    }

 


     //Edit restaurant menu
    protected function editRestaurantMenu(Request $request)
     {
       $user_info =  json_decode($this->getUserInfo($request->userId));
       if(  $user_info->user_id == "na" )
       {
         $data= ["message" => "failure", "status_code" =>  901 ,
         'detailed_msg' => 'User has no priviledge to access this feature! Please consult admin.'  ] ;
         return json_encode($data);
       }

 

       if( $request->menuId  == "")
       {
         $data= ["message" => "failure", "status_code" =>  902 , 'detailed_msg' => 'Missing data to perform action.'  ];
         return json_encode($data);
       }

 

       /*$todays = date('Y-m-d');
       $startTime = date('H:i:s', strtotime($request->duration));
       $durations = $todays." ".$startTime;*/

 


       $restaurantMenu = ProductModel::find( $request->menuId ) ;
       if(  !isset($restaurantMenu) )
       {
         $data= ["message" => "failure", "status_code" =>  999 ,
         'detailed_msg' => 'Restaurant Menu information not found.'   ];
         return json_encode($data);
       }

 

       $restaurantMenu->pr_name = $request->menuName;
       $restaurantMenu->description = $request->description;
       $restaurantMenu->category = $request->categoryName;
       $restaurantMenu->prep_duration =  $request->duration;
       $restaurantMenu->unit_price = $request->unitPrice;
       $restaurantMenu->unit_value =  $request->unitValue ;
       $restaurantMenu->unit_name =  $request->unitName ;
     


       if( $request->discountPc != "")
        {
          $restaurantMenu->discountPc =   $request->discountPc ;
          $restaurantMenu->actual_price = $request->unitPrice - ( ( $request->discountPc / 100 ) * $request->unitPrice );
        }

        if( $request->discount != "")
        {
          $restaurantMenu->discount =   $request->discount ;
          $restaurantMenu->actual_price = $request->unitPrice - $request->discount;
        }


       $restaurantMenu->packaging = $request->packageCharge ;  
       $restaurantMenu->pack_type = $request->packageType ;
       $restaurantMenu->food_type = $request->foodType ;
       $restaurantMenu->free_text =  $request->freetextAvailable ;
       $save=$restaurantMenu->save();

 

       if($save)
       {

 

         $data= ["message" => "success", "status_code" =>  5009 ,  'detailed_msg' => 'Restaurant Menu successfully updated.', 'menuId' => $restaurantMenu->id ,'menuName' => $restaurantMenu->pr_name,'menuCode' => $restaurantMenu->pr_code ];
       }
       else
       {
         $data= ["message" => "failure", "status_code" =>  5010 ,  'detailed_msg' => 'Restaurant Menu could not be updated.', 'menuId' =>  0  ];
       }
       return json_encode( $data);
     }

//Remove/Delete restaurant menu
    protected function removeRestaurantMenu(Request $request)
     {


        $user_info =  json_decode($this->getUserInfo($request->userId));
        if(  $user_info->user_id == "na" )
        {
            $data= ["message" => "failure", "status_code" =>  901 , 
            'detailed_msg' => 'User has no priviledge to access this feature! Please consult admin.'  ] ;
            return json_encode($data);
        } 
 
        
        if( $request->menuId  == "")
        {
         $data= ["message" => "failure", "status_code" =>  902 , 'detailed_msg' => 'Missing data to perform action.'  ];
         return json_encode($data);
        }

        $restaurantMenu = ProductModel::find( $request->menuId ) ; 

 
         if(  !isset($restaurantMenu) )
        {
            $data= ["message" => "failure", "status_code" =>  999 , 
            'detailed_msg' => 'Restaurant Menu information not found.'   ];
            return json_encode($data);
        } 


         $restaurantMenuAddon = RestaurantMenuAddon::where("menu_id",$restaurantMenu->id)->delete(); 
         $restaurantMenu->delete();

         $data = array( "message" => "success" , 'status_code' => 5025 , 'detailed_msg' => "Restaurant Menu is deleted successfully!") ;
         return json_encode( $data); 


     }

  
  //Add restaurant menu addon
	protected function updateRestaurantMenuAddon(Request $request)
	{
    if( $request->productCode  == "" || $request->addOns == "")
		{
  		$data= ["message" => "failure", "status_code" =>  902 , 'detailed_msg' => 'Missing data to perform action.'  ];
  		return json_encode($data);
		} 

    $checkAddonDetails = RestaurantMenuAddon::where('pr_code',$request->productCode) 
    ->get(); 
    if(isset($checkAddonDetails))
    {
      foreach ($checkAddonDetails as $keyidd) 
      {
        $serviceData = RestaurantMenuAddon::find($keyidd->id);
        $serviceData->delete();
      }
      $isTrue=true;
    } 

    if( $request->addOns == "[]" ) //blank arrray
    {
      $data= ["message" => "success", "status_code" =>  5011 ,  'detailed_msg' => 'Menu addon deleted.', 'customizeMenuId' =>  0  ];
      return json_encode( $data);
    }


 
    $addOnsJson = json_decode($request->addOns); 
    $addOnId=0; 
    $addOnLists = $addOnsJson[0]; 
 
    foreach ($addOnsJson as $jsonkey => $jsonvalue) {

      $restuarantMenuAddon = new RestaurantMenuAddon;
      $restuarantMenuAddon->pr_code = $request->productCode; 
      foreach ($jsonvalue  as $key => $value) { 
      
        if($key == "name")
        {
          $restuarantMenuAddon->addon_name =  $value;
          $restuarantMenuAddon->addon_description =  $value;
        }
        else  if($key == "price")
        {
          $restuarantMenuAddon->price =  $value;
        }
      } 

      $restuarantMenuAddon->save(); 
      $addOnId = $restuarantMenuAddon->id; 
 
    }

    

    $data= ["message" => "success", "status_code" =>  5011 ,  'detailed_msg' => 'Menu customize updated successfully.', 'customizeMenuId' => $addOnId ];
    return json_encode( $data);
	}


   	protected function getRestaurantMenuAddon(Request $request)
	{
	 	if($request->productCode  == "" )
		{
		 $data= ["message" => "failure", "status_code" =>  902 , 'detailed_msg' => 'Missing data to perform action.'  ];
		 return json_encode($data);
		}


		$menuRestaurantAddon  = DB::table("ba_restaurant_menu_addon")
		->where("pr_code", $request->productCode )
		->select( "id as addonId","pr_code as productCode", "addon_name as addonName",  "addon_description as addonDescription", "price as addonPrice"  )
		->get() ;

		$data= ["message" => "success", "status_code" =>  5013 ,  'detailed_msg' => 'Customize Menu Addon are fetched.' , 
		'results' => $menuRestaurantAddon];

		return json_encode( $data);
	}
	
  protected function onOffRestaurantMenuAddon(Request $request)
	{

    if( $request->menuId  == "")
		{
  		$data= ["message" => "failure", "status_code" =>  902 , 'detailed_msg' => 'Missing data to perform action.'  ];
  		return json_encode($data);
		}
				
		$productModel=ProductModel::where('id',$request->menuId)->first();
		$productModel->addon_available = $request->addon;
		$save=$productModel->save();
	
		if($save){
			$data= ["message" => "success", "status_code" =>  5027 ,  'detailed_msg' => 'Menu customize update successfully.'];
		}else
		{
			$data= ["message" => "failure", "status_code" =>  5028 ,  'detailed_msg' => 'Menu customize could not be update.'];
		}
		return json_encode( $data);
	}

	/*****Client Api****/
	protected function getMenuRestaurantUnderCategory(Request $request)
  {


    $products  = DB::table("ba_products")
		->where("bin",$request->bin)
    ->where("category", $request->category )
    ->select( "id as productId", "bin", 
            "pr_code as productCode", "pr_name as productName",  
            "description",  "gst_code as GST" ,"pack_type as packType" , 
            "stock_inhand as stock" , "max_order as maxOrderAllowed", "unit_price as unitPrice" , "actual_price as actualPrice",
            "unit_value as unitValue" , "unit_name as unitName", "discount", "discountPc", "category as productCategory", 
            "food_type as foodType", "free_text as freeText", 
            "addon_available as addon", "photos as images"    )
    ->paginate(10) ;





      foreach( $products as $item)
     	{
        $all_photos = array(); 
        $files =  explode(",",  $item->images ); 
        $files=array_filter($files); 

        if(count($files) > 0 )
        {

            $folder_path =  "/public/assets/image/store/bin_" .   $item->bin .  "/";
            foreach($files as $file )
            {
                if(file_exists( base_path() .  $folder_path .   $file ))
                {
                  $all_photos[]  =  URL::to( $folder_path  ) . "/" .    $file  ;
                }
                else
                {
                  $all_photos[]   = URL::to("/public/assets/image/no-image.jpg");
                }
              }

           } 

           $item->images = $all_photos;
     	}
     	$data= ["message" => "success", "status_code" =>  5029 ,  'detailed_msg' => 'Menu are fetched.' , 
     	'results' => $products];

    	return json_encode( $data);
     }
     protected function getMenuAddon(Request $request)
	{
	 	if($request->menuId  == "" )
		{
		 $data= ["message" => "failure", "status_code" =>  902 , 'detailed_msg' => 'Missing data to perform action.'  ];
		 return json_encode($data);
		}


		$menuRestaurantAddon  = DB::table("ba_restaurant_menu_addon")
		->where("menu_id", $request->menuId )
		->select( "id as addonId","menu_id as menuId", "addon_name as addonName",  "addon_description as addonDescription", "price as addonPrice"  )
		->get() ;

		$data= ["message" => "success", "status_code" =>  5031 ,  'detailed_msg' => 'Customize Menu Addon are fetched.' , 
		'results' => $menuRestaurantAddon];

		return json_encode( $data);
	}
     
}

