<?php

namespace App\Http\Controllers\Api\V2; 

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Crypt;  
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\File;
use Illuminate\Contracts\Encryption\DecryptException; 

use App\Http\Controllers\Controller;
use App\Jobs\SendEmaOrderSms;



use App\ShoppingItemAddonModel;
use App\Business;  
use App\ProductModel; 
use App\ServiceBooking;
use App\OrderStatusLogModel;
use App\CustomerProfileModel;
use App\ShoppingOrderItemsModel;
use App\AddressModel;

use App\Traits\Utilities;
use App\User;




class ShoppingBasketController extends Controller
{
	  use Utilities;

    protected function getProductCategories(Request $request)
    {
      $business_category = $request->businessCategory;

      if($business_category == "")
      {
          $product_groups = DB::table("ba_product_category") 
          ->select("category_name as category", "icon_path as iconUrl"  )  
          ->get() ;
        }
        else 
        {
            $product_groups = DB::table("ba_product_category") 
            ->where("business_category", $business_category) 
            ->select("category_name as category", "icon_path as iconUrl"  )  
            ->get() ;
        }





        foreach($product_groups as   $item)
        {
            $source = base_path() . $item->iconUrl;
 
          if( $item->iconUrl != "" &&  file_exists(  $source )  )
          {
            
            $item->iconUrl = URL::to("/") .  $item->iconUrl  ;
          }
          else 
          {
            $item->iconUrl =  "http://booktou.in/app/public/assets/app/any-product.png"; 
          }

        } 

     	$data= ["message" => "success", "status_code" =>  5003 ,  'detailed_msg' => 'Product categories fetched.' , 
     	'results' => $product_groups];
        
    	return json_encode( $data);


     }


     protected function getProductsUnderCategory(Request $request)
     {
       	$products  = DB::table("ba_products")
        ->join("ba_business", "ba_business.id", "=", "ba_products.bin" )   
        ->join("ba_business_category", "ba_business_category.name", "=", "ba_business.category" )    
        ->where("ba_products.bin",$request->bin)
        ->where("ba_products.category", $request->category )
        ->select( "ba_products.id as productId", "ba_products.bin", "ba_business.name",  "ba_business.category as businessCategory",
          "ba_business_category.display_type as uiLayoutMode",
                "ba_products.pr_code as productCode", "ba_products.pr_name as productName",  
                "ba_products.description",  "ba_products.gst_code as GST" ,"ba_products.pack_type as packType" , 
                "ba_products.stock_inhand as stock" , "ba_products.max_order as maxOrderAllowed", 
                "ba_products.unit_price as unitPrice" , "ba_products.actual_price as actualPrice",
                "ba_products.unit_value as unitValue" , "ba_products.unit_name as unitName", 
                "ba_products.discount", "ba_products.discountPc", 
                "ba_products.packaging as packagingCharge",
                "ba_products.category as productCategory", 
                "ba_products.food_type as foodType", "ba_products.free_text as freeText", 
                "ba_products.addon_available as addon", 
                DB::Raw("'[]' addOnList"),
                 "ba_products.photos as images", 
                 "is_bestseller as bestSeller",
                "ba_products.status"  )
        ->orderBy("stock_inhand", "desc")
        ->paginate(20) ;
  



      $prCodes = array();

      foreach( $products as $item)
      {
        $prCodes[] = $item->productCode;
      }
      $prCodes[] ="1";


      $product_add_ons =  DB::table("ba_restaurant_menu_addon")  
      ->whereIn("pr_code", $prCodes )
      ->select("id  as addOnId", "pr_code as productCode", "addon_name as addOnName", "price as addOnPrice")
      ->get(); 


     	foreach( $products as $item)
     	{
        $all_photos = array(); 
        $files =  explode(",",  $item->images ); 
        if(count($files) > 0 )
        {

          $files=array_filter($files);
          $folder_path =  "/public/assets/image/store/bin_" .   $item->bin .  "/";

          foreach($files as $file )
          {

            $source = base_path() .  $folder_path .   $file; 
            if(file_exists( $source  ))
            {

              $pathinfo = pathinfo( $source  );
              $new_file_name  =  $pathinfo['filename'].  "_sm" . '.' . $pathinfo['extension']; 
              $destination  = base_path() .  "/public/assets/image/store/bin_" . $item->bin  .  "/"  . $new_file_name ;
              if( file_exists( $destination ) ) //smaller file exists
              {
                $all_photos[]  =  URL::to( "/public/assets/image/store/bin_" . $item->bin  .  "/"  . $new_file_name);
              }
              else 
              {
                if(filesize(  $source ) > 307200) // 300 Kb
                {

                  $this->chunkImage($source, $folder_path , $destination ); //chunking image size 
                  $all_photos[]  =  URL::to( "/public/assets/image/store/bin_" . $item->bin  .  "/"  . $new_file_name); 

                }
                else 
                {
                  $all_photos[]  =  URL::to( $folder_path  ) . "/" .    $file  ;
                }
              } 
            }
            else
            {
              $all_photos[]   = URL::to("/public/assets/image/no-image.jpg");
            }
        }
      }

        $item->images = $all_photos; 
        $item->GST =  ( $item->GST == "" ? "Not Available" :  $item->GST );



        $addOnes = array();
        foreach($product_add_ons as $addon)
        {
          if( $addon->productCode == $item->productCode )
          {
            $addOnes[] =  $addon;                 
          }
        } 
        $item->addOnList =  $addOnes; 
 
           
     	}

  

      $final_result['last_page'] = $products->lastPage( ); 
      $final_result['data'] = $products->items();   

     	$data= ["message" => "success", "status_code" =>  5005 ,  'detailed_msg' => 'Products are fetched.' , 
     	'results' => $final_result];

    	return json_encode( $data);
     }



     //view product info by customer
     protected function viewProductInformation(Request $request)
     {

        if($request->productId  == ""   )
        {
          $data= ["message" => "failure", "status_code" =>  902 , 'detailed_msg' => 'Missing data to perform action.'  ];
          return json_encode($data);
        }

        $product_info  = DB::table("ba_products")
        ->join("ba_business", "ba_business.id", "=", "ba_products.bin" )  
        ->join("ba_business_category", "ba_business_category.name", "=", "ba_business.category" )  

        ->where("ba_products.id", $request->productId ) 
        ->select("ba_products.id as productId",
            "ba_products.bin", "ba_business.name",  
            "ba_business.category as businessCategory",
           "ba_business_category.display_type as uiLayoutMode",
            "ba_products.pr_code as productCode", 
            "ba_products.pr_name as productName", 
            "ba_products.gst_code as GST", 
            "ba_products.mfr_id as manufacturer",
            "ba_products.description",  
            "ba_products.stock_inhand as stock" ,  
            "ba_products.max_order as maximumOrderAllowed",
             "ba_products.unit_price as unitPrice" , 
             "ba_products.unit_name as unitName ", 
             "ba_products.pack_type as packType" ,  
             "ba_products.unit_value as unitValue" ,  
             "ba_products.discount", 
             "ba_products.discountPc", 
             "ba_products.actual_price as actualPrice",
              "ba_products.packaging as packagingCharge",
              "ba_products.category as productCategory", 
              "ba_products.food_type as foodType", 
              "ba_products.free_text as freeText", 
              "ba_products.addon_available as addon", 
               DB::Raw("'[]' addOnList"),
              "ba_products.photos as images"  , 
              "is_bestseller as bestSeller",
              "ba_products.tags", 
              "ba_products.status"  )
        ->first() ;   

 

 
        $all_photos = array();

        if(isset($product_info))
        {

          $product_add_ons =  DB::table("ba_restaurant_menu_addon")  
          ->where("pr_code", $product_info->productCode )
          ->select("id  as addOnId", "pr_code as productCode", "addon_name as addOnName", "price as addOnPrice")
          ->get(); 


          $product_info->addOnList =  $product_add_ons ;

          $files =  explode(",",  $product_info->images ); 
           if(count($files) > 0 )
           {
              $files=array_filter($files);
              $folder_path =  "/public/assets/image/store/bin_" .  $product_info->bin  . "/"; 
              foreach($files as $file )
              {
                if(file_exists( base_path() .  $folder_path .   $file ))
                {
                  $all_photos[]  =  URL::to( $folder_path  ) . "/" .    $file  ;
                }
                else
                {
                  $all_photos[]   = URL::to("/public/assets/image/no-image.jpg");
                }
              }

           }
   

            $product_info->images = $all_photos;
        } 

        
     
        $data= ["message" => "success", "status_code" =>  5011 ,  'detailed_msg' => 'Products are fetched.' , 
        'results' => $product_info];

        return json_encode( $data);
    }

 

     protected function saveShoppingOrder(Request $request)
     {

        $user_info =  json_decode($this->getUserInfo($request->userId));
        if(  $user_info->user_id == "na" )
        {
            $data= ["message" => "failure", "status_code" =>  901 , 
            'detailed_msg' => 'User has no priviledge to access this feature! Please consult admin.'  ] ;
            return json_encode($data);
        } 
        
        if( $request->bin  == "" ||  $request->addressId == ""  ||  $request->totalCost == "" || 
            $request->deliveryCharge  == ""   )
        {
         $data= ["message" => "failure", "status_code" =>  902 , 'detailed_msg' => 'Missing data to perform action.'  ];
         return json_encode($data);
        }
 	  

        if($request->addressId == 0 )//read from profile address
        {
            $customer = CustomerProfileModel::find($user_info->member_id);
            if(!isset($customer))
            {
                $data= ["message" => "failure",   "status_code" =>  999 , 'detailed_msg' => 'No matching address found!'  ];
                return json_encode( $data);
            }

            $address  =  $customer->locality;
            $landmark  =  $customer->landmark;
            $city  =  $customer->city ;
            $state   =  $customer->state;
            $pinCode  =  $customer->pin_code;
            $latitude   =  $customer->latitude;
            $longitude  =  $customer->longitude;

        }
        else 
        {
            $address_info = AddressModel::find( $request->addressId    ) ; 
            if( !isset($address_info) )
            {
                $data= ["message" => "failure", "status_code" =>  999 , 'detailed_msg' => 'No matching address found.'  ];
                return json_encode($data);
            }

            $address  =  $address_info->address;
            $landmark  =  $address_info->landmark;
            $city  =  $address_info->city;
            $state   =  $address_info->state;
            $pinCode  = $address_info->pin_code;
            $latitude   =  $address_info->latitude;
            $longitude  =  $address_info->longitude;

        }


        $business_info = Business::find( $request->bin  ) ; 
        if( !isset($business_info) )
        {
            $data= ["message" => "failure", "status_code" =>  999 , 'detailed_msg' => 'No matching business found.'  ];
            return json_encode($data);
        } 

 
        $new_order = new ServiceBooking ; 
        $new_order->bin = $request->bin; 
        $new_order->book_category = $business_info->category;  
        $new_order->book_by= $user_info->member_id;
        $new_order->staff_id = 0;
        $new_order->otp  =  $new_order->delivery_otp  = mt_rand(222222, 999999); 
        $new_order->address = $address;
        $new_order->landmark = $landmark;
        $new_order->city = $city;
        $new_order->state = $state;
        $new_order->pin_code  = $pinCode;
        $new_order->book_status = "new";
        $new_order->total_cost = $request->totalCost ;
        $new_order->delivery_charge = $request->deliveryCharge;
        $new_order->agent_payable  =  (0.6 *  $request->deliveryCharge ) ; //60% of total charge
        $new_order->service_fee  =  $request->deliveryCharge -  $new_order->agent_payable ; //60% of total charge 
        $new_order->cashback =  0;

        if($request->coupon != "")
        {
          $new_order->coupon = $request->coupon; 
          //calculated coupon discount  
        }
        if($request->discount != "")
        {
          $new_order->discount = $request->discount;  
        } 

        $new_order->payment_type = $request->payMode;  
        $new_order->latitude = $latitude;
        $new_order->longitude =  $longitude; 
        $new_order->book_date =  date('Y-m-d H:i:s');    

        if(strcasecmp($request->bookFlag, "TODAY") == 0 )
        { 
          $new_order->service_date =  date('Y-m-d H:i:s');  
        }
        else 
        { 

          if( date('Y-m-d') ==  date("Y-m-d", strtotime(  $request->deliveryDate)) ) //user specified today as delivery date and it is not allowed
          {  
            $new_order->service_date = date('Y-m-d H:i:s' , strtotime($new_order->book_date . ' +1 day')); //delivery next day
          }
          else if( date("Y-m-d", strtotime(  $request->deliveryDate))  > date('Y-m-d')  )
              {
                $new_order->service_date = date('Y-m-d H:i:s'  , strtotime($request->deliveryDate )); //delivery on specified date 
              }
              else 
              {
                $new_order->service_date = date('Y-m-d H:i:s' , strtotime($new_order->book_date . ' +1 day')); //delivery next day
              }
        }

        $new_order->cust_remarks =  ($request->orderRemarks != "" ? $request->orderRemarks : null);  
        $new_order->transaction_no =  ($request->transactionId  != "" ? $request->transactionId : "na");  
        $new_order->reference_no =  ($request->referenceNo  != "" ? $request->referenceNo : "na");  

        $save=$new_order->save(); 

        if($save)
        {

          $orderStatus = new OrderStatusLogModel;
          $orderStatus->order_no = $new_order->id; 
          $orderStatus->order_status = $new_order->book_status;
          $orderStatus->log_date = date('Y-m-d H:i:s');
          $orderStatus->save();

          //logs ends here

          $productCodes  = array_filter(explode( ",",  $request->productCodes));
          $productQty  = array_filter(explode( ",",  $request->productQty));

          $addOns = json_decode( stripslashes( $request->addOn), TRUE );
          $customizationText = json_decode( stripslashes( $request->customizationText), TRUE ); 
          $packagingCharges = json_decode( stripslashes( $request->packagingCharges), TRUE ); 


          


          for($i=0; $i < count($productCodes) ; $i++ ) 
          {
            $productCodes[$i] = trim( $productCodes[$i] ); 
          }
          //Fetch product details
          $productDetails = DB::table("ba_products")
          ->whereIn("pr_code", $productCodes)
          ->get();

          $totalItems= count( $productCodes );
          $productName=$productPrice=$productCategory=array();

          foreach ($productDetails as $value)
          {
            for($i=0; $i < $totalItems ; $i++ )
            {
                    if($value->pr_code ==  $productCodes[$i] )
                    {

                      //ading items
                      $items = new ShoppingOrderItemsModel;
                      $items->order_no = $new_order->id;
                      $items->pr_code = $productCodes[$i]; 
                      $items->pr_name = $value->pr_name;
                      $items->description = $value->description;
                      $items->qty  = $productQty[$i];  
                      $items->price  = $value->actual_price;
                      $items->unit  = $value->unit_name;
                      $items->unit  = $value->unit_name;
                      $items->category_name  = $value->category;

                      $customRequestText = null;
                      //finding custom text
                      foreach ($customizationText as $customRequests) 
                      {
                          foreach (   $customRequests  as $crkey => $customText ){
                            
                            if( $productCodes[$i]  == $crkey )
                            {
                              $customRequestText = $customText;
                              break;
                            } 
                          }    
                      } 
                      $items->custom_text  = $customRequestText; 


                      //product packaging charge 
                      foreach ( $packagingCharges as $packagingCharge) 
                      {
                          foreach (   $packagingCharge  as $prkey => $packagingCost ){
                            
                            if( $productCodes[$i]  == $prkey )
                            {
                              $items->package_charge = $packagingCost;
                              break;
                            } 
                          }    
                      } 
                        
                      $items->save(); 
 

                      break;
                    } 
                }

                foreach ($addOns as $addOn) 
                {
                    foreach (   $addOn  as $ckey => $addOnItems ){
                    if($value->pr_code == $ckey )
                    {
                      $addOnItems =  array_filter(  $addOnItems );

                     
                     $addOnItemDetails = DB::table("ba_restaurant_menu_addon")
                     ->whereIn("id", $addOnItems )
                     ->select("id", "addon_name", "price")
                     ->get(); 

                      foreach($addOnItemDetails as $addOnItemDetail)
                      {
                        $addOn = new ShoppingItemAddonModel;
                        $addOn->order_no =$new_order->id;
                        $addOn->pr_code = $ckey ;
                        $addOn->addon_id = $addOnItemDetail->id;
                        $addOn->addon_name = $addOnItemDetail->addon_name;
                        $addOn->addon_price =  $addOnItemDetail->price;
                        $addOn->save();  
                      }

                      break; 
                    }
                  }    
                } 
            }
            

            $msg = 'Your order is placed successfully through BookTou. Our delivery partner will contact you soon.'; 
            //SendEmaOrderSms::dispatch() ; 

            /*
                //saving shoping log  
                
            	//sms to buyer
            	$this->sendSmsAlert( array( $user_info->phone  ),  $msg );
            	//sms to seller  
            	$business_owner  = DB::table("ba_business")->select("phone_pri")->where("id", $request->bin )->first(); 
            	if(isset($business_owner ))
            	{
            		$msg = 'A new order is placed in your store through BookTou. Please process the order and update.'; 
            		$this->sendSmsAlert( array( $business_owner->phone_pri ),  $msg );
            	}
            */   

            $data= ["message" => "success", "status_code" =>  5001 ,  'detailed_msg' => $msg ];
        }
        else
        {
            $data= ["message" => "failure", "status_code" =>  5002 ,  'detailed_msg' => 'Your order could not be placed. Please contact sales'];
        }
        
        return json_encode( $data);


     }




     protected function showActiveOrders(Request $request)
     {
     	$user_info =  json_decode($this->getUserInfo( $request->userId ));
        if(  $user_info->user_id == "na" )
        {
            $data= ["message" => "failure", "status_code" =>  901 , 
            'detailed_msg' => 'User has no priviledge to access this feature! Please consult admin.'  ] ;
            return json_encode($data);
        }

        $orders   = DB::table("ba_service_booking")
     	->where("ba_service_booking.book_by", $user_info->member_id)
     	->whereRaw("book_status in( 'new', 'confirmed', 'in_route' ,'order_packed', 'pickup_did_not_come', 'cancelled')"  )

     	->select('id as orderNo', "address as address", "landmark",
     		"city", "state",  "pin_code as pinCode", 
            "total_cost as totalCost", 
     		"delivery_charge as deliveryCharge", "cashback", 
            DB::Raw("'4' totalItems"), 
            "book_status as status" )
     	->get() ; 
  
 
     	foreach( $orders as $item)
     	{
     		$item->totalItems = DB::table("ba_shopping_basket")
        ->where("order_no", $item->orderNo  )
        ->count(); 
     	}

        
 


     	$data= ["message" => "success", "status_code" =>  5007 ,  'detailed_msg' => 'Active orders fetched.' , 
     	'results' => $orders];

    	return json_encode( $data);
     }


     

     protected function cancelOrder(Request $request)
     {
     	 $user_info =  json_decode($this->getUserInfo( $request->userId ));
        if(  $user_info->user_id == "na" )
        {
            $data= ["message" => "failure", "status_code" =>  901 , 
            'detailed_msg' => 'User has no priviledge to access this feature! Please consult admin.'  ] ;
            return json_encode($data);
        } 
 	

 		   if( $request->orderNo  == ""  )
        {
          $data= ["message" => "failure", "status_code" =>  902 , 'detailed_msg' => 'Missing data to perform action.'  ];
         return json_encode($data);
        }
 	
 		$order_info = ServiceBooking::find($request->orderNo );

 		if( !isset($order_info))
 		{
 			$data= ["message" => "failure", "status_code" =>  999 , 'detailed_msg' => 'No order number found.'  ];
 			return json_encode($data);
 		}
 


 		if(  $order_info->book_status != "new" ||  $order_info->book_by != $user_info->member_id )
 		{
 			$data= ["message" => "failure", "status_code" =>  5010 , 'detailed_msg' => 'Order no longer available to cancel. Please reach out to customer care.'  ];
 			return json_encode($data);
 		}

 		$order_info->book_status = "canceled";
    $order_info->save();
    

     	 
     	$data= ["message" => "success", "status_code" =>  5009 ,  'detailed_msg' => 'Order is cancelled.' ];

    	return json_encode( $data);
     }

    //View order complete details by customer
    protected function viewOrderDetails(Request $request)
    {
        $user_info =  json_decode($this->getUserInfo( $request->userId ));
        if(  $user_info->user_id == "na" )
        {
            $data= ["message" => "failure", "status_code" =>  901 , 
            'detailed_msg' => 'User has no priviledge to access this feature! Please consult admin.'  ] ;
            return json_encode($data);
        }

        if( $request->orderNo   == "" )
        {
           $data= ["message" => "failure", "status_code" =>  902 , 'detailed_msg' => 'Order No no specified.'  ];
           return json_encode($data);
        }

        $order_info   = DB::table("ba_service_booking")
        ->join("ba_profile", "ba_service_booking.book_by", "=", "ba_profile.id")
        ->where("ba_service_booking.id", $request->orderNo )
        ->where("ba_service_booking.book_by",$user_info->member_id)  
        ->select('ba_service_booking.id as orderNo',
          "ba_service_booking.bin",
          "service_date as orderDate", 
           "ba_service_booking.service_date as serviceDate", 
            "fullname as customerName",  
            "address as deliveryAddress",  
            "phone as customerPhone",
            DB::Raw("'na' deliveryAgentName"),
            DB::Raw("'na' deliveryAgentPhone"),
            "book_status as status" ,
            "total_cost as totalCost",
            "delivery_charge as deliveryCharge",
            "delivery_otp as deliveryOtp",
            "cashback as cashBack",
            "discount",
            "coupon", 
             "payment_type as paymentMode",
            DB::Raw("'[]' orderItems") ,
            DB::Raw("'[]' timeline" 
          )
        )
        ->first() ;

        if( !isset($order_info) )
        {
            $data= ["message" => "failure", "status_code" =>  999 , 'detailed_msg' => 'No matching order found.'  ];
            return json_encode($data);
        }


        $order_items=  DB::table("ba_shopping_basket")
        ->join("ba_products","ba_shopping_basket.pr_code","=","ba_products.pr_code")
        ->select(

                "ba_shopping_basket.pr_code as productCode",
                 "ba_shopping_basket.pr_name as item",
                 "ba_shopping_basket.qty" , 
                 "ba_shopping_basket.unit",
                 "ba_products.unit_price as price",
                 "ba_shopping_basket.package_charge as packageCharge",
                 "ba_shopping_basket.custom_text as customText",
                 "ba_shopping_basket.category_name as category",
                 "photos as image" , 
                DB::Raw("'[]' addOns")
             )
        ->where("ba_shopping_basket.order_no", $order_info->orderNo  )
        ->get();


        $order_add_ons =  DB::table("ba_shopping_item_add_on")  
        ->where("order_no", $order_info->orderNo  )
        ->select("addon_id as addOnId", "pr_code as productCode", "addon_name as addOnName", "addon_price as addOnPrice")
        ->get(); 

         
         $orderStatusInfo =  DB::table("ba_order_status_log")
        ->select("order_status as orderStatus","log_date as statusDateTime")
        ->where("order_no", $order_info->orderNo  )
        ->get(); 
        
        
        //Fetch Agent Information
         $agentInfo=  DB::table("ba_delivery_order")
         ->join("ba_profile", "ba_delivery_order.member_id", "=", "ba_profile.id")
         ->select("ba_profile.fullname as deliveryAgentName","phone as deliveryAgentPhone")
         ->where("order_no", $request->orderNo  )
         ->get(); 

            $agentName="na";
            $agentPhone="na";

          foreach ($agentInfo as $value) 
          {

             $agentName = $value->deliveryAgentName;
             $agentPhone = $value->deliveryAgentPhone;
          }


        $folder_path =  "/public/assets/image/store/bin_" . $order_info->bin .  "/";
        foreach( $order_items as $item)
        {
            $all_photo = URL::to("/public/assets/image/no-image.jpg");
            $files =    explode(",",  $item->image )   ;

            $files=array_filter($files); 
   
              foreach( $files  as $file)
              {
 

                  $file = trim($file); 
                  if(  file_exists( base_path() .  $folder_path .   $file )   )
                    {
                      $all_photo = URL::to( $folder_path  ) . "/" .    $file  ;
                      break;
                    }  
              }  
             $item->image = $all_photo; 

            $addOnes = array();
            foreach($order_add_ons as $addon)
            {
               if( $addon->productCode == $item->productCode )
               {
                $addOnes[] =  $addon;                 
               }
            } 
            $item->addOns =  $addOnes; 
            $item->customText = ($item->customText == "" ) ? "NONE" : $item->customText; 
        }

        $order_info->orderItems =$order_items;
        $order_info->deliveryAgentName =$agentName;
        $order_info->deliveryAgentPhone =$agentPhone; 
        $order_info->timeline =$orderStatusInfo; 
        $order_info->coupon = ($order_info->coupon == null ? "NONE" : $order_info->coupon); 
       
        $data= ["message" => "success", "status_code" =>  55 ,  
        'detailed_msg' => 'Active orders fetched.' ,  
        'results' => $order_info ];

        return json_encode( $data);
     }


     //Fetch product categories
      protected function getProductsCategory(Request $request)
     {

         if( $request->bin  == "")
        {
         $data= ["message" => "failure", "status_code" =>  902 , 'detailed_msg' => 'Missing data to perform action.'  ];
         return json_encode($data);
        }

        
        $business_info = DB::table("ba_business")
        ->join("ba_business_category", "ba_business_category.name", "=", "ba_business.category" ) 
        ->where("ba_business.id",  $request->bin   )
        ->first();

 

        if( !isset($business_info) )
        {
          $data= ["message" => "failure", "status_code" =>  999 , 'detailed_msg' => 'No matching business found.'  ];
          return json_encode($data);
        }  

        $bin =$request->bin;



      $available_categories =  DB::table("ba_products")  
      ->where("bin" , $bin )
      ->select('category') 
      ->distinct()
      ->get();

      $category_names = array();
      foreach($available_categories as $item)
      {
        $category_names[] = $item->category ; 
      }
      $category_names[] = "0";
    

         $productCategories = DB::table("ba_product_category")   
        ->whereIn("category_name", $category_names )
        ->select( "ba_product_category.category_name as categoryName", "category_desc as description",  "ba_product_category.icon_path as iconUrl" ,   DB::Raw("'list' uiLayoutMode") )  
        ->orderBy("category_name", "asc")
        ->get() ;
 
 

        foreach($productCategories as   $item)
        {
            $source = base_path() . $item->iconUrl;
          if( $item->iconUrl != "" &&  file_exists(  $source )  )
          {
            
            $item->iconUrl = URL::to("/") .  $item->iconUrl  ;
          }
          else 
          {
            $item->iconUrl =  "http://booktou.in/app/public/assets/app/any-product.png"; 
          }

          $item->uiLayoutMode = $business_info->display_type;

        }

        $data= ["message" => "success", "status_code" =>  5005 ,  'detailed_msg' => 'Product categories fetched.' , 'results' => $productCategories];
        
        return json_encode( $data);

     }
 

  public function globalSearchProductOrService(Request $request)
  {
    if($request->keyword =="")
    {
      $data= ["message" => "failure", "status_code" =>  902 , 'detailed_msg' => 'No search keyword specified!'  ];
      return json_encode($data);
    }
  

      //search business 
      $businesses =  DB::table("ba_business")
      ->select("id as bin", "name", "tag_line as tagLine",  'shop_number as shopNumber',  "phone_pri as primaryPhone", 
        "phone_alt as alternatePhone", "locality", "landmark", "city", "state", "pin", 
        'tags', 'latitude','longitude' , 
        "profile_image as profileImgUrl", 
        "tags" , "banner as bannerImgUrl", 
        DB::Raw("'na' promoImgUrl"),
        DB::Raw("'0' favourite"),  
        "rating","category as businessCategory" , "is_open as isOpen" ) 
      ->whereRaw( implode(" or ", $where_clause) )
      ->orWhere("name", $request->tags)  
      ->get();
 
       
 
  foreach($businesses as $item)
  {

      if($item->profileImgUrl!=null)
      {
        $item->profileImgUrl = URL::to('/public'.$item->profileImgUrl); 
      }
      else
      {
        $item->profileImgUrl = URL::to('/public/assets/image/no-image.jpg')  ;   
      }

    if($item->bannerImgUrl != null )
    {
      $source = public_path() .  $item->bannerImgUrl;
      $pathinfo = pathinfo( $source  );
      $new_file_name  =  $pathinfo['filename'].  "_sm" . '.' . $pathinfo['extension'];
      $destination  = base_path() .  "/public/assets/image/business/bin_" . $item->bin . "/"   . $new_file_name ; 

      if(file_exists(  $source )  )
      {
        $info = getimagesize($source ); 

            if ($info['mime'] == 'image/jpeg')
                $image = imagecreatefromjpeg($source);

            elseif ($info['mime'] == 'image/gif')
                $image = imagecreatefromgif($source);

            elseif ($info['mime'] == 'image/png')
                $image = imagecreatefrompng($source);


          $original_w = $info[0];
          $original_h = $info[1];
          $thumb_w = 290; // ceil( $original_w / 2);
          $thumb_h = 200; //ceil( $original_h / 2 );

          $thumb_img = imagecreatetruecolor($original_w, $original_h);
          imagecopyresampled($thumb_img, $image,
                             0, 0,
                             0, 0,
                             $original_w, $original_h,
                             $original_w, $original_h);

          imagejpeg($thumb_img, $destination, 20);
        }
        $item->bannerImgUrl =  URL::to("/public/assets/image/business/bin_" . $item->bin . "/"   . $new_file_name );   

    }
    else
    {
      $item->bannerImgUrl = URL::to('/public/assets/image/no-image.jpg')  ;  
    }


    if($request->userId != "")
    {
      $favorited = FavouriteBusiness::where('user_id',$request->userId)
      ->where('bin',$item->bin)->get(); 
      $item->favourite = count($favorited); 
    }
    else
    {
      $item->favourite =  0;
    }



  }
    
    
  
  if(count($businesses)>0)
  {
    $data= ["message" => "success", "status_code" =>  1011 ,  'detailed_msg' => 'Business profile fetch.' ,  
    'results' => $businesses ];
  }
  else{
    $data= ["message" => "failure", "status_code" =>  1012 ,  'detailed_msg' => 'Business profile could not fetch.' ,  
    'results' => array() ];
  } 
    
    return json_encode( $data);
    
  }

 

  public function searchProducts(Request $request)
  {

    if($request->keyword =="" || $request->bin  =="")
   {
      $data= ["message" => "failure", "status_code" =>  902 , 'detailed_msg' => 'No search keyword specified!'  ];
      return json_encode($data);
   }


    $keyword = $request->keyword;
    $bin = $request->bin ;

    $products  = DB::table("ba_products") 
    ->join("ba_business", "ba_business.id", "=", "ba_products.bin" )  
    ->join("ba_business_category", "ba_business_category.name", "=", "ba_business.category" )  
    ->where(function($query)  use( $bin, $keyword ){
            $query->where('ba_products.bin', '=', $bin  )
            ->where('pr_name', 'like',   "%$keyword%");
        })
     ->orWhere(function($query) use( $bin, $keyword ) {
            $query->where('ba_products.bin', '=', $bin  )
            ->where('description', 'like', "%$keyword%");
     }) 

    ->select( "ba_products.id as productId", "ba_products.bin",  "ba_business.name",  
            "ba_business.category as businessCategory",
            "ba_business_category.display_type as uiLayoutMode", 
            "ba_products.pr_code as productCode", "ba_products.pr_name as productName",  
            "ba_products.description",  "ba_products.gst_code as GST" ,"ba_products.pack_type as packType" , 
            "ba_products.stock_inhand as stock" , "ba_products.max_order as maxOrderAllowed", "ba_products.unit_price as unitPrice" , "ba_products.actual_price as actualPrice",
            "ba_products.unit_value as unitValue" , "ba_products.unit_name as unitName", "ba_products.discount", "ba_products.discountPc", 
            "ba_products.packaging as packagingCharge",
            "ba_products.category as productCategory", 
            "ba_products.food_type as foodType", "ba_products.free_text as freeText", 
            "ba_products.addon_available as addon", 
            DB::Raw("'[]' addOnList"),
            "ba_products.photos as images", 
            "is_bestseller as bestSeller",
            "ba_products.status"  )
    ->paginate(20) ;


    $prCodes = array();
    foreach( $products as $item)
    {
      $prCodes[] = $item->productCode;
    }
    $prCodes[] ="1";

    $product_add_ons =  DB::table("ba_restaurant_menu_addon")  
    ->whereIn("pr_code", $prCodes )
    ->select("id  as addOnId", "pr_code as productCode", "addon_name as addOnName", "price as addOnPrice")
    ->get(); 
  

    foreach($products as $item)
    {
      if($item->GST == "" || $item->GST == null )
      {
        $item->GST = 0 ;

      } 
      $item->unitPrice = $item->unitPrice+0; 


      $all_photos = array(); 
      $files =  explode(",",  $item->images ); 
      if(count($files) > 0 )
      {

        $files=array_filter($files);
        $folder_path =  "/public/assets/image/store/bin_" .   $item->bin .  "/";

        foreach($files as $file )
        {

          $source = base_path() .  $folder_path .   $file; 
          if(file_exists( $source  ))
          {

            $pathinfo = pathinfo( $source  );
            $new_file_name  =  $pathinfo['filename'].  "_sm" . '.' . $pathinfo['extension']; 
            $destination  = base_path() .  "/public/assets/image/store/bin_" . $item->bin  .  "/"  . $new_file_name ;
            if( file_exists( $destination ) ) //smaller file exists
            {
              $all_photos[]  =  URL::to( "/public/assets/image/store/bin_" . $item->bin  .  "/"  . $new_file_name);
            }
            else 
            {
              if(filesize(  $source ) > 307200 ) // 300 Kb
              {
  
                $this->chunkImage($source, $folder_path , $destination ); //chunking image size 
                $all_photos[]  =  URL::to( "/public/assets/image/store/bin_" . $item->bin  .  "/"  . $new_file_name); 

              }
              else 
              {
                $all_photos[]  =  URL::to( $folder_path  ) . "/" .    $file  ;
              }
            } 
          }
          else
          {
            $all_photos[]   = URL::to("/public/assets/image/no-image.jpg");
          }
      } 
      }

      $item->images = $all_photos; 


      $addOnes = array();
      foreach($product_add_ons as $addon)
      {
        if( $addon->productCode == $item->productCode )
        {
          $addOnes[] =  $addon;                 
        }
      } 

      $item->addOnList =  $addOnes; 
 
 
    }

    $final_result['last_page'] = $products->lastPage( ); 
    $final_result['data'] = $products->items();  

    $data= ["message" => "success", "status_code" =>  1015 ,  'detailed_msg' => 'Matching products found.', 'results' => $final_result];         
    
    return json_encode($data); 

 
    }


protected function getProductsGrid(Request $request)
{

    //read business 
    $promoProducts = DB::table("ba_product_promotions")->get();

    $categories = DB::table("ba_product_category")->select("category_name")->get();

    $products  = DB::table("ba_products")  
    ->join("ba_business", "ba_business.id", "=", "ba_products.bin" ) 
    ->join("ba_business_category", "ba_business_category.name", "=", "ba_business.category" )   
    ->join("ba_product_promotions", "ba_product_promotions.pr_code", "=", "ba_products.pr_code" ) 

    ->where("ba_products.stock_inhand", "<>", "0")   
    ->select( "ba_products.id as productId", "ba_products.bin",  "ba_business.name",  
              "ba_business_category.display_type as uiLayoutMode",
              "ba_products.pr_code as productCode", "ba_products.pr_name as productName",  
              "ba_products.description",  "ba_products.gst_code as GST" ,"ba_products.pack_type as packType" , 
              "ba_products.stock_inhand as stock" , "ba_products.max_order as maxOrderAllowed", 
              "ba_products.unit_price as unitPrice" , "ba_products.actual_price as actualPrice",
              "ba_products.unit_value as unitValue" , "ba_products.unit_name as unitName", "ba_products.discount", 
              "ba_products.discountPc", 
              "ba_products.packaging as packagingCharge",
              "ba_products.category as productCategory", 
              "ba_products.food_type as foodType", "ba_products.free_text as freeText", 
              "ba_products.addon_available as addon",  
               "ba_products.photos as images", 
              "ba_products.status" ,
              "ba_business.category as businessCategory",  "ba_product_promotions.todays_deal as todaysDeal",
              "ba_business.is_open as isOpen"  )

    ->paginate(30) ;

    

    $rows = array();

    foreach( $products as $item)
    {

      $product_photo  = URL::to("/public/assets/image/no-image.jpg"); 
      $files =  explode(",",  $item->images ); 
      if(count($files) > 0 )
      {
        $files=array_filter($files);
        $folder_path =  "/public/assets/image/store/bin_" .   $item->bin .  "/";

          foreach($files as $file )
          {
            $source = base_path() .  $folder_path .   $file; 
            if(file_exists( $source  ))
            {

              $pathinfo = pathinfo( $source  );
              $new_file_name  =  $pathinfo['filename'].  "_sm" . '.' . $pathinfo['extension']; 
              $destination  = base_path() .  "/public/assets/image/store/bin_" . $item->bin  .  "/"  . $new_file_name ;
              if( file_exists( $destination ) ) //smaller file exists
              {
                $product_photo =  URL::to( "/public/assets/image/store/bin_" . $item->bin  .  "/"  . $new_file_name);
              }
              else 
              {
                if(filesize(  $source ) > 307200) // 300 Kb
                {

                  $this->chunkImage($source, $folder_path , $destination ); //chunking image size 
                  $product_photo =  URL::to( "/public/assets/image/store/bin_" . $item->bin  .  "/"  . $new_file_name);  
                  break;

                }
                else 
                {
                  $product_photo =  URL::to( $folder_path  ) . "/" .    $file  ;
                  break;
                }
              } 
            }
            
        }
      }

      $item->images = $product_photo;   

      $rows[] = $item;
    }
  
  shuffle($rows);

    $final_result = array();   
    $final_result['last_page'] = $products->lastPage( ); 
    $final_result['data'] =  $rows ;   

    $data= ["message" => "success", "status_code" =>  5005 ,  'detailed_msg' => 'Products are fetched.' ,  'results' => $final_result];

      return json_encode( $data);
  }



protected function getFeaturedProductsOfBusiness(Request $request)
{
    if( $request->bin  == "")
    {
      $data= ["message" => "failure", "status_code" =>  902 , 'detailed_msg' => 'Missing data to perform action.'  ];
      return json_encode($data);
    }

    //read business 
    $business_info  = DB::table("ba_business")
    ->join("ba_business_category", "ba_business_category.name", "=", "ba_business.category" )    
    ->where("ba_business.id",  $request->bin )
    ->select("ba_business.*", "ba_business_category.display_type")
    ->first(); 

    if( !isset($business_info) )
    {
      $data= ["message" => "failure", "status_code" =>  999 , 'detailed_msg' => 'No matching business found.'  ];
      return json_encode($data);
    }

  

    $products  = DB::table("ba_products")    
    ->join("ba_product_promotions", "ba_product_promotions.pr_code", "=", "ba_products.pr_code" )  
    ->where("ba_products.stock_inhand", "<>", "0")   
    ->where("ba_products.bin", $request->bin )   
    ->select( "ba_products.id as productId", "ba_products.bin",  DB::Raw("'na' name") ,  
        DB::Raw("'na' uiLayoutMode"),  "ba_products.pr_code as productCode", "ba_products.pr_name as productName",  
              "ba_products.description",  "ba_products.gst_code as GST" ,"ba_products.pack_type as packType" , 
              "ba_products.stock_inhand as stock" , "ba_products.max_order as maxOrderAllowed", 
              "ba_products.unit_price as unitPrice" , "ba_products.actual_price as actualPrice",
              "ba_products.unit_value as unitValue" , "ba_products.unit_name as unitName", "ba_products.discount", 
              "ba_products.discountPc", 
              "ba_products.packaging as packagingCharge",
              "ba_products.category as productCategory", 
              "ba_products.food_type as foodType", "ba_products.free_text as freeText", 
              "ba_products.addon_available as addon",  
               "ba_products.photos as images", 
              "ba_products.status" ,
              DB::Raw("'na' businessCategory"),
              "ba_product_promotions.todays_deal as todaysDeal", 
              DB::Raw("'close' isOpen")
                )
        ->paginate(30) ;



    foreach( $products as $item)
    {

      $product_photo  = URL::to("/public/assets/image/no-image.jpg"); 
      $files =  explode(",",  $item->images ); 
      if(count($files) > 0 )
      {
        $files=array_filter($files);
        $folder_path =  "/public/assets/image/store/bin_" .   $item->bin .  "/";

          foreach($files as $file )
          {
            $source = base_path() .  $folder_path .   $file; 
            if(file_exists( $source  ))
            {

              $pathinfo = pathinfo( $source  );
              $new_file_name  =  $pathinfo['filename'].  "_sm" . '.' . $pathinfo['extension']; 
              $destination  = base_path() .  "/public/assets/image/store/bin_" . $item->bin  .  "/"  . $new_file_name ;
              if( file_exists( $destination ) ) //smaller file exists
              {
                $product_photo =  URL::to( "/public/assets/image/store/bin_" . $item->bin  .  "/"  . $new_file_name);
              }
              else 
              {
                if(filesize(  $source ) > 307200) // 300 Kb
                {

                  $this->chunkImage($source, $folder_path , $destination ); //chunking image size 
                  $product_photo =  URL::to( "/public/assets/image/store/bin_" . $item->bin  .  "/"  . $new_file_name);  
                  break;

                }
                else 
                {
                  $product_photo =  URL::to( $folder_path  ) . "/" .    $file  ;
                  break;
                }
              } 
            }
            
        }
      }

      $item->images = $product_photo;  

      $item->name = $business_info->name;
      $item->businessCategory = $business_info->category;
      $item->uiLayoutMode = $business_info->display_type; 
      $item->isOpen = $business_info->is_open; 
           
    }
 
    $final_result = array();   
    $final_result['last_page'] = $products->lastPage( ); 
    $final_result['data'] = $products->items();   

    $data= ["message" => "success", "status_code" =>  5005 ,  'detailed_msg' => 'Products are fetched.' ,  'results' => $final_result];

      return json_encode( $data);
  }



protected function getAllProductStock(Request $request)
{

    $productCodes = json_decode( $request->productCodes, true  ); 
    $result = array();

    if(count( $productCodes  ) > 0)
    {


      $prCodes = array();
      foreach($productCodes  as $key => $value)
      {
        $prCodes[] = $key;
      }
      $prCodes[]= "0" ;

      $products  = DB::table("ba_products")  
      ->whereIn("pr_code",  $prCodes )
      ->select( "pr_code", "stock_inhand")
      ->get(); 
 

      foreach( $products as $item)
      {  
        foreach($productCodes  as $key => $value)
        {
          if($item->pr_code == $key )
          {
            $result[]["prodCode"]  = $item->pr_code;
             $result[]["stock"] = $item->stock_inhand;
            break; 
          }
        }       
      }
    }
  
    $data= ["message" => "success", "status_code" =>  5005 ,  'detailed_msg' => 'Products stock level fetched.' ,  'results' => $result];

      return json_encode( $data);
  }


  
  protected function getSimilarProducts(Request $request)
  {
    if( $request->productCode  == "" )
    {
        $data= ["message" => "failure", "status_code" =>  902 , 'detailed_msg' => 'Missing data to perform action.'  ];
        return json_encode($data);
    }

    //read business 
    $product_info  = DB::table("ba_products") 
    ->where("pr_code", $request->productCode )  
    ->first(); 

    if( !isset($product_info))
    {
        $data= ["message" => "failure", "status_code" =>  999 , 'detailed_msg' => 'No matching business found.'  ];
        return json_encode($data);
    }

    $tags = $product_info ->tags;

    $all_tags =  isset( $tags ) &&  $tags != null ? explode(",", $tags) : array($product_info ->pr_name );

   

    $products  = DB::table("ba_products")    
    ->join("ba_business", "ba_business.id", "=", "ba_products.bin" ) 

    ->where("ba_products.stock_inhand", "<>", "0")   
    ->whereIn("ba_products.tags", $all_tags )   
    ->where("ba_products.pr_code", "<>", $request->productCode  )   
    ->select( "ba_products.id as productId", "ba_products.bin",  DB::Raw("'na' name") ,  
        DB::Raw("'grid' uiLayoutMode"),  "ba_products.pr_code as productCode", "ba_products.pr_name as productName",  
              "ba_products.description",  "ba_products.gst_code as GST" ,"ba_products.pack_type as packType" , 
              "ba_products.stock_inhand as stock" , "ba_products.max_order as maxOrderAllowed", 
              "ba_products.unit_price as unitPrice" , "ba_products.actual_price as actualPrice",
              "ba_products.unit_value as unitValue" , "ba_products.unit_name as unitName", "ba_products.discount", 
              "ba_products.discountPc", 
              "ba_products.packaging as packagingCharge",
              "ba_products.category as productCategory", 
              "ba_products.food_type as foodType", "ba_products.free_text as freeText", 
              "ba_products.addon_available as addon",  
               "ba_products.photos as images", 
              "ba_products.status" ,
              "ba_business.category as businessCategory" ,
              DB::Raw("'no' todaysDeal" ),
              "is_bestseller as bestSeller" 
                )
        ->paginate(20) ;

 
    


    foreach( $products as $item)
    {

       $all_photos = array(); 
        $files =  explode(",",  $item->images ); 
        if(count($files) > 0 )
        {

          $files=array_filter($files);
          $folder_path =  "/public/assets/image/store/bin_" .   $item->bin .  "/";

          foreach($files as $file )
          {

            $source = base_path() .  $folder_path .   $file; 
            if(file_exists( $source  ))
            {

              $pathinfo = pathinfo( $source  );
              $new_file_name  =  $pathinfo['filename'].  "_sm" . '.' . $pathinfo['extension']; 
              $destination  = base_path() .  "/public/assets/image/store/bin_" . $item->bin  .  "/"  . $new_file_name ;
              if( file_exists( $destination ) ) //smaller file exists
              {
                $all_photos[]  =  URL::to( "/public/assets/image/store/bin_" . $item->bin  .  "/"  . $new_file_name);
              }
              else 
              {
                if(filesize(  $source ) > 307200) // 300 Kb
                {

                  $this->chunkImage($source, $folder_path , $destination ); //chunking image size 
                  $all_photos[]  =  URL::to( "/public/assets/image/store/bin_" . $item->bin  .  "/"  . $new_file_name); 

                }
                else 
                {
                  $all_photos[]  =  URL::to( $folder_path  ) . "/" .    $file  ;
                }
              } 
            }
            else
            {
              $all_photos[]   = URL::to("/public/assets/image/no-image.jpg");
            }
        }
      }

        $item->images = $all_photos; 
        $item->GST =  ( $item->GST == "" ? "Not Available" :  $item->GST );
        $item->name = $product_info->pr_name;
    }
 
    $final_result = array();   
    $final_result['last_page'] = $products->lastPage( ); 
    $final_result['data'] = $products->items();   

    $data= ["message" => "success", "status_code" =>  5005 ,  'detailed_msg' => 'Similar products are fetched.' ,  'results' => $final_result];

      return json_encode( $data);
  }


}
