<?php

namespace App\Http\Controllers\Api\V2; 

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Crypt;  
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\File;
use Illuminate\Contracts\Encryption\DecryptException;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel; 


 
use App\Business;  
use App\ProductModel;
use App\ShoppingOrderModel;
use App\ShoppingOrderItemsModel;
use App\CustomerProfileModel;
use App\DeliveryOrderModel;
use App\ServiceBooking;
use App\OrderStatusLogModel;
use App\ProductUnitModel;

use App\Traits\Utilities;
use App\User;


use App\Jobs\SendSellerOrderProcessingSms; 



class RetailStoreController extends Controller
{
	use Utilities;

     protected function addProduct(Request $request)
     {
        $user_info =  json_decode($this->getUserInfo($request->userId));
        if(  $user_info->user_id == "na" )
        {
          $data= ["message" => "failure", "status_code" =>  901 , 'detailed_msg' => 'User has no priviledge to access this feature! Please consult admin.'  ] ;
          return json_encode($data);
        }


        if( $request->productName == "" ||   $request->maxOrderQty == "" || $request->mfrId == "" || $request->description == "" ||  
          $request->initialStock == "" ||  $request->stockInHand  == "" ||  $request->minimumStock  == "" ||  $request->categoryName== "" || 
          $request->packageType == "" || $request->unitPrice == "" || $request->unitValue == "" || 
          $request->unitName == "" || $request->actualPrice == "" ||  $request->discountPc == "" || $request->foodType == "" )
        {
            $data= ["message" => "failure", "status_code" =>  902 , 'detailed_msg' => 'Mdissing data to perform action.' ]; 
            return json_encode($data);
        }

        $pr_code_count  = ProductModel::where("bin", $request->bin )
        ->where("category", $request->categoryName ) 
        ->count() + 1; 

        $id =   DB::table('ba_products') 
        ->max('id'); 

        $prCode = "BPR" .  $request->bin . "" . ( $id + 1)  ;  


        $discount = ( $request->discountPc /100 ) * $request->unitPrice; 

        $product = new ProductModel;
        $product->bin = $request->bin;
        $product->pr_code =  $prCode  ;
        $product->pr_name = $request->productName;
        $product->gst_code = $request->gst;
        $product->mfr_id = $request->mfrId;
        $product->description = $request->description;
        $product->initial_stock = $request->initialStock;
        $product->stock_inhand = $request->stockInHand;
        $product->min_required = $request->minimumStock;
        $product->category = $request->categoryName; 
        $product->max_order = $request->maxOrderQty;  
        $product->unit_price = $request->unitPrice; 
        $product->unit_name = $request->unitName ;
        $product->unit_value = $request->unitValue ;

         $discount =0;
        $discountPc = 0;
        if($request->discount == "" )
        {
          if($request->discountPc == "")
          {
            $discount = 0;
            $discountPc  = 0;
          }
          else 
          {
            $discountPc  = $request->discountPc ;
            $discount = ( $request->discountPc /100 ) * $request->unitPrice; 
          } 

        }
        else 
        {
          $discount =   $request->discount ;
          $product->discountPc  = 0 ;
        }
  
        $product->discount =  $discount ;
        $product->discountPc  = $discountPc ;
        $product->actual_price = $request->unitPrice - $discount ;




        $product->pack_type = $request->packageType ;
        $product->packaging = ( $request->packagingCharge == "" ? 0.00 :  $request->packagingCharge)  ;
        $product->food_type = $request->foodType ;
        $save=$product->save(); 

        if($save)
        {
        	$data= ["message" => "success", "status_code" =>  5001 ,  'detailed_msg' => 'Product added successfully.', 
          'productId' => $product->id,  'productCode' => $prCode  ];
        }
        else
        {
        	$data= ["message" => "failure", "status_code" =>  5002 ,  'detailed_msg' => 'Product could not be added.', 
          'productId' =>  0 ,  'productCode' => 'na'];
        }
    	return json_encode( $data);


     }



     protected function updateProductDetails(Request $request)
     {

        $user_info =  json_decode($this->getUserInfo($request->userId));
        if(  $user_info->user_id == "na" )
        {
            $data= ["message" => "failure", "status_code" =>  901 , 
            'detailed_msg' => 'User has no priviledge to access this feature! Please consult admin.'  ] ;
            return json_encode($data);
        }  

        
        if( $request->productId  == "" || $request->productName == "" ||   $request->maxOrderQty == "" ||  
           $request->mfrId == "" || $request->description == "" ||  $request->initialStock == "" || 
            $request->stockInHand  == "" ||  $request->minimumStock  == "" ||  $request->categoryName== "" )
        {
         $data= ["message" => "failure", "status_code" =>  902 , 'detailed_msg' => 'Missing data to perform action.'  ];
         return json_encode($data);
        }
 

        $product = ProductModel::find( $request->productId ) ; 
       
        if(  !isset($product) )
        {
            $data= ["message" => "failure", "status_code" =>  999 , 
            'detailed_msg' => 'No product information found.'   ];
            return json_encode($data);
        } 
        

        $product->pr_name = $request->productName; 
        $product->mfr_id = $request->mfrId;
        $product->description = $request->description;
        $product->initial_stock = $request->initialStock;
        $product->stock_inhand = $request->stockInHand;
        $product->min_required = $request->minimumStock;
        $product->category = $request->categoryName; 
        $product->max_order = $request->maxOrderQty;  
        $product->unit_price = $request->unitPrice;
        $product->unit_name = $request->unitName ; 
        $product->pack_type  = ( $request->packageType =="" ? "PACK" :$request->packageType );
        $product->packaging = ( $request->packagingCharge == "" ? 0.00 :  $request->packagingCharge)  ;
        $product->unit_value  = ( $request->unitValue =="" ? "1" :$request->unitValue );
        $product->unit_name =  $request->unitName ;
        

        $discount =0;
        $discountPc = 0;
        if($request->discount == "" )
        {
          if($request->discountPc == "")
          {
            $discount = 0;
            $discountPc  = 0;
          }
          else 
          {
            $discountPc  = $request->discountPc ;
            $discount = ( $request->discountPc /100 ) * $request->unitPrice; 
          } 

        }
        else 
        {
          $discount =   $request->discount ;
          $product->discountPc  = 0 ;
        }
  
        $product->discount =  $discount ;
        $product->discountPc  = $discountPc ;
        $product->actual_price = $request->unitPrice - $discount ;



        $product->food_type  = ( $request->foodType =="" ? "Not-applicable" :$request->foodType );
        $product->volume  = ( $request->volume =="" ? "Not Set" :$request->volume ); 
        $save=$product->save();
 


        if($save)
        {
            $data= ["message" => "success", "status_code" =>  5013 ,  'detailed_msg' => 'Product updated successfully.'];
        }
        else
        {
            $data= ["message" => "failure", "status_code" =>  5014 ,  'detailed_msg' => 'Product could not be updated.'];
        }
        return json_encode( $data);


     }



     //restocking
     protected function updateProductStock(Request $request)
     {

        $user_info =  json_decode($this->getUserInfo($request->userId));
        if(  $user_info->user_id == "na" )
        {
            $data= ["message" => "failure", "status_code" =>  901 , 
            'detailed_msg' => 'User has no priviledge to access this feature! Please consult admin.'  ] ;
            return json_encode($data);
        } 
 
        
        if( $request->productId  == ""    )
        {
          $data= ["message" => "failure", "status_code" =>  902 , 'detailed_msg' => 'Product no selected.'  ];
          return json_encode($data);
        }

        if( $request->stockInHand  == ""  &&  $request->unitPrice == ""  )
        {
         $data= ["message" => "failure", "status_code" =>  902 , 'detailed_msg' => 'Missing data to perform action.'  ];
         return json_encode($data);
        }

 

         $product = ProductModel::find($request->productId ) ; 
       
       if(  !isset($product) )
        {
            $data= ["message" => "failure", "status_code" =>  999 , 
            'detailed_msg' => 'No product information found.' ];
            return json_encode($data);
        } 


        if( ($product->stock_inhand +  $request->stockInHand ) <= 0 )
        {
          $product->stock_inhand = 0;
        } 
        else 
        {
          $product->stock_inhand = $product->stock_inhand +  $request->stockInHand;
        }      
         
         if(  $request->unitPrice != ""  )
        {
          $product->unit_price = $request->unitPrice;
        }
       

        $save=$product->save(); 

        if($save)
        {
            $data= ["message" => "success", "status_code" =>  5015 ,  'detailed_msg' => 'Product updated successfully.'];
        }
        else
        {
            $data= ["message" => "failure", "status_code" =>  5016 ,  'detailed_msg' => 'Product could not be updated.'];
        }
        return json_encode( $data);
        
     }
    

     protected function removeProduct(Request $request)
     {

        $user_info =  json_decode($this->getUserInfo($request->userId));
        if(  $user_info->user_id == "na" )
        {
            $data= ["message" => "failure", "status_code" =>  901 , 
            'detailed_msg' => 'User has no priviledge to access this feature! Please consult admin.'  ] ;
            return json_encode($data);
        } 
 
        
        if( $request->productId  == ""  || $request->bin  == ""   )
        {
         $data= ["message" => "failure", "status_code" =>  902 , 'detailed_msg' => 'Missing data to perform action.'  ];
         return json_encode($data);
        }


         $product = ProductModel::where("id", $request->productId )
         ->where("bin",  $request->bin)
         ->first() ; 


       
       if(  !isset($product) )
        {
            $data= ["message" => "failure", "status_code" =>  999 , 
            'detailed_msg' => 'No product information found.'   ];
            return json_encode($data);
        } 

        $product->delete(); 

         $data= ["message" => "success", "status_code" =>  5025 ,  'detailed_msg' => 'Product removed successfully.'];
       
        return json_encode( $data);


     }




     protected function getManufacturers(Request $request)
     {
         

        $manufactuers  = DB::table("ba_manufacturer")  
        ->select( "id as mfrId", "name", "description" )
        ->get() ;
  
        $data= ["message" => "success", "status_code" =>  5017 ,  'detailed_msg' => 'Products are fetched.' , 
        'results' =>  $manufactuers ];

        return json_encode( $data);
     }


     protected function getProductCategories(Request $request)
     {


        $user_info =  json_decode($this->getUserInfo($request->userId));
        if(  $user_info->user_id == "na" )
        {
          $data= ["message" => "failure", "status_code" =>  901 , 
          'detailed_msg' => 'User has no priviledge to access this feature! Please consult admin.'  ];
          return json_encode($data);
        }
 
        if( $request->bin  == "")
        {
            $data= ["message" => "failure", "status_code" =>  902 , 'detailed_msg' => 'Missing data to perform action.'  ];
            return json_encode($data);
        }

 

        $business_info = Business::find( $request->bin ); 
        if( !isset($business_info) )
        {
            $data= ["message" => "failure", "status_code" =>  999 , 'detailed_msg' => 'No matching business found.'  ];
            return json_encode($data);
        }

        $biz_category = $business_info->category;



 
        
        $productCategories = DB::table("ba_product_category") 
        ->where("business_category",  $biz_category)
        ->select("category_name as categoryName", "icon_path as iconUrl" , DB::raw("'0' itemCount") ) 
        ->get() ;

        $products  = DB::table("ba_products")
        ->where("bin",$business_info->id)
        ->select("category as categoryName",  DB::raw('count(*) as itemCount'))
        ->groupBy("category")
        ->get() ;


        foreach($productCategories as   $item)
        {
          foreach($products as   $product)
          {
              if($product->categoryName == $item->categoryName)
              {
                $item->itemCount = $product->itemCount;
              } 
          }

          $source = base_path() . $item->iconUrl;
          if( $item->iconUrl != "" &&  file_exists(  $source )  )
          {
            
            $item->iconUrl = URL::to("/") .  $item->iconUrl  ;
          }
          else 
          {
            $item->iconUrl =  "http://booktou.in/app/public/assets/app/any-product.png"; 
          }
 

        }
 
  

   
      $data= ["message" => "success", "status_code" =>  5003 ,  'detailed_msg' => 'Product categories fetched.' , 
      'results' => $productCategories];
        
      return json_encode( $data);

     }




      protected function getProductsUnderCategory(Request $request)
     {
        $user_info =  json_decode($this->getUserInfo($request->userId));
        if(  $user_info->user_id == "na" )
        {
            $data= ["message" => "failure", "status_code" =>  901 , 
            'detailed_msg' => 'User has no priviledge to access this feature! Please consult admin.'  ] ;
            return json_encode($data);
        } 

         if($request->bin  == "" || $request->category == ""   )
        {
         $data= ["message" => "failure", "status_code" =>  902 , 'detailed_msg' => 'Missing data to perform action.'  ];
         return json_encode($data);
        }
  

        $products  = DB::table("ba_products")
        ->where("category", $request->category )
        ->where("bin", $request->bin  )
        ->select( "id as productId",   "bin", 
          "pr_code as productCode", "pr_name as productName",  "description",  
          "photos as images", "stock_inhand as stock" , 
            "max_order as maximumOrderAllowed" , "unit_price as unitPrice", "actual_price as actualPrice" , "ba_products.discount", "ba_products.discountPc"  )
        ->get() ;
 
        foreach( $products as $item)
        {
            $all_photos = array(); 
            $files =  explode(",",  $item->images );

           if(count($files) > 0 )
           {
              $files=array_filter($files);
 
              $folder_path =  "/public/assets/image/store/bin_" .   $item->bin .   "/";

              foreach($files as $file )
              {
                if(file_exists( base_path() .  $folder_path .   $file ))
                {
                  $all_photos[]  =  URL::to( $folder_path  ) . "/" .    $file  ;
                }
                else
                {
                  $all_photos[]   = URL::to("/public/assets/image/no-image.jpg");
                }
              }

           } 

           $item->images = $all_photos;
 
        }
        $data= ["message" => "success", "status_code" =>  5005 ,  'detailed_msg' => 'Products are fetched.' , 
        'results' => $products];

        return json_encode( $data);
     }


    
    //view product info by retail owner
     protected function viewProductInformation(Request $request)
     {

        $user_info =  json_decode($this->getUserInfo($request->userId));
        if(  $user_info->user_id == "na" )
        {
            $data= ["message" => "failure", "status_code" =>  901 , 
            'detailed_msg' => 'User has no priviledge to access this feature! Please consult admin.'  ] ;
            return json_encode($data);
        } 


         if ( $request->productId  == ""  || $request->bin   == ""  )
        {
         $data= ["message" => "failure", "status_code" =>  902 , 'detailed_msg' => 'Missing data to perform action.'  ];
         return json_encode($data);
        }
  

        $product_info  = DB::table("ba_products")
        ->where("id", $request->productId )
        ->where("bin", $request->bin  )
        ->select( "id as productId",  
            "bin",
            "pr_code as productCode", 
            "pr_name as productName",
            "category as categoryName", 
            "gst_code as GST", 
            "mfr_id as manufacturer",
            "description", 
            "photos as images" , 
            "initial_stock as initialStock" ,  
            "min_required as minimumStock" ,  
            "stock_inhand as stock" ,  
            "max_order as maximumOrderAllowed", 
            "unit_price as unitPrice" , 
            "actual_price as actualPrice",
            "discount", 
            "discountPc",
            "prep_duration as duration",
            "food_type as foodType",
            "volume", 
            "pack_type as packageType",
            "unit_value as unitValue",
            "unit_name as unitName",  
            "addon_available as addon",
            "free_text as freeTextAvailable", 
            "packaging as packagingCharge",
            "status"  )
        ->first() ;


        $all_photos = array();

        if(isset($product_info))
        {
           $files =  explode(",",  $product_info->images );

           if(count($files) > 0 )
           {
              $files=array_filter($files);


              $folder_path =  "/public/assets/image/store/bin_" .  $product_info->bin .    "/";

              foreach($files as $file )
              {
                if(file_exists( base_path() .  $folder_path .   $file ))
                {
                  $all_photos[]  =  URL::to( $folder_path  ) . "/" .    $file  ;
                }
                else
                {
                  $all_photos[]   = URL::to("/public/assets/image/no-image.jpg");
                }

              }
           } 
        }


        $product_info->images = $all_photos;
     
        $data= ["message" => "success", "status_code" =>  5011 ,  'detailed_msg' => 'Products are fetched.' , 
        'results' => $product_info];

        return json_encode( $data);
     }


 

    protected function addProductPhotos(Request $request)
    {

      $user_info =  json_decode($this->getUserInfo($request->userId)); 
      if(  $user_info->bin  == "na" )
      {
        $data= ["message" => "failure", "status_code" =>  901 , 
        'detailed_msg' => 'User has no priviledge to access this feature! Please consult admin.'  ] ;
        return json_encode($data);
      } 

      
      if( $request->productId == ""  ||  $request->photos == "" )
      {
        $data= ["message" => "failure", "status_code" =>  902 , 'detailed_msg' => 'Missing data to perform action.'  ];
        return json_encode($data);
      }


     if(  $user_info->bin  >  0 )
     {
        $folder_url  = '/assets/image/store/bin_'. $user_info->bin . "/"     ;
        $folder_path = public_path(). '/assets/image/store/bin_'.  $user_info->bin  . "/"   ;
     }   
    
     $file_names=array();
    //get all existing images  

     $e_photos = DB::table("ba_products") 
     ->where("id", $request->productId) 
     ->select("photos")
     ->first() ;

     if(isset($e_photos))
     {
       $file_names = explode("," , $e_photos->photos);
     }
 

    
    if( !File::isDirectory($folder_path)){
        File::makeDirectory( $folder_path, 0777, true, true);
    }
 
    
    $i=1;
    foreach($request->file('photos') as $file)
    {
      $originalname = $file->getClientOriginalName();
      $extension = $file->extension( );
      $filename = "prim_" .  $request->productId . $i . time()  . ".{$extension}";
      $file->storeAs('uploads',  $filename );
      $imgUpload = File::copy(storage_path().'/app/uploads/'. $filename, $folder_path .  $filename); 


      //creating smaller photos 
      $source = $folder_path .  $filename;
      $new_file_name  =  "prim_" .  $request->productId . $i . time() ."_sm"  . ".{$extension}"; 
      $destination  = $folder_path  . $new_file_name ;

      if(filesize( $source ) > 307200  ) // 512 Kb
      {
        //compress    
        $info = getimagesize($source );
        if ($info['mime'] == 'image/jpeg')
          $image = imagecreatefromjpeg($source);
        elseif ($info['mime'] == 'image/gif')
          $image = imagecreatefromgif($source);
        elseif ($info['mime'] == 'image/png')

        $image = imagecreatefrompng($source);
        $original_w = $info[0];
        $original_h = $info[1];
        $thumb_w =  ceil( $original_w / 2  );
        $thumb_h =  ceil( $original_h / 2  );
        $thumb_img = imagecreatetruecolor($thumb_w, $thumb_h);
        imagecopyresampled($thumb_img, $image, 0, 0, 0, 0, $thumb_w, $thumb_h, $original_w, $original_h);
        imagejpeg($thumb_img, $destination);
        //compression ends  
      }
             
      //small photos ends 
      $file_names[] =  $filename ; 

      $i++; 
    }

      
      //removing extra space
      $file_names = array_filter($file_names);

 
      $photos =  implode(",", $file_names); 
      DB::table("ba_products") 
      ->where("id", $request->productId) 
      ->update(['photos' => $photos ]) ; 

      $data= ["message" => "success", "status_code" =>  5031 ,  'detailed_msg' => 'Product photos are uploaded.' ];
      return json_encode( $data);

     }


    protected function deleteProductPhoto(Request $request)
    { 
 
        $user_info =  json_decode($this->getUserInfo($request->userId)); 
        if(  $user_info->bin  == "na" )
        {
            $data= ["message" => "failure", "status_code" =>  901 , 
            'detailed_msg' => 'User has no priviledge to access this feature! Please consult admin.'  ] ;
            return json_encode($data);
        }  

        
        if( $request->productId == ""  ||  $request->fileName == "" )
        {
         $data= ["message" => "failure", "status_code" =>  902 , 'detailed_msg' => 'Missing data to perform action.'  ];
         return json_encode($data);
        }


        $pathinfo = pathinfo( $request->fileName ); 
        $actual_file_name  =  $pathinfo['filename'].'.'.$pathinfo['extension']; 


        $folder_path = public_path() . '/assets/image/store/bin_'.  $user_info->bin . "/pr_" . $request->productId  ;
        $file_name =  $folder_path . "/" .  $actual_file_name  ; 

        $rem_files= array();
        foreach (glob($folder_path . "/*"  ) as $filename) 
        {
            if( strcasecmp(  basename( $filename ) ,  $actual_file_name  ) == 0)  
            {
              File::delete($filename ); 
            }
            else 
            {
              $rem_files[] = basename( $filename );
            }
        }

        $photos =  implode(",", $rem_files);

        DB::table("ba_products")
        ->where("id", $request->productId)
        ->update(['photos' => $photos ]) ;

        $data= ["message" => "success", "status_code" =>  5091 ,  'detailed_msg' => 'Product photo deleted.' ];
        return json_encode( $data);


     }


     protected function showActiveOrders(Request $request)
     {

        $user_info =  json_decode($this->getUserInfo( $request->userId ));
        if(  $user_info->user_id == "na" )
        {
            $data= ["message" => "failure", "status_code" =>  901 , 
            'detailed_msg' => 'User has no priviledge to access this feature! Please consult admin.'  ] ;
            return json_encode($data);
        } 

        if( $request->bin   == "" )
        {
           $data= ["message" => "failure", "status_code" =>  902 , 'detailed_msg' => 'Missing data to perform action.'  ];
           return json_encode($data);
        }


        if($request->orderDate != "")
        {

          $orderDate = date('Y-m-d', strtotime($request->orderDate)); 
        }
        else 
        {
         $orderDate =  date('Y-m-d');
        }


        $orders   = DB::table("ba_service_booking")
        ->join("ba_profile", "ba_service_booking.book_by", "=", "ba_profile.id")
        ->where("ba_service_booking.bin", $request->bin )  
        ->where("ba_service_booking.is_confirmed", "yes" )  
        ->whereNotIn("ba_service_booking.book_status",  array('new', 'cancel_by_client', 'cancel_by_owner', 'canceled' )  )  
        ->whereRaw("service_date  >= '".    $orderDate .  "'"  ) 
        ->orderBy("ba_service_booking.id", "desc")
        ->select('ba_service_booking.id as orderNo', "service_date as orderDate", 
            "fullname as customerName",  
            "locality as deliveryAddress",  
            "phone as customerPhone",
            DB::Raw("'na' deliveryAgentName"),
            DB::Raw("'na' deliveryAgentPhone"),
            "book_status as status" ,
           "ba_service_booking.total_cost as  totalAmountOfOrder" 
        )
        ->get() ;

        $oids= array( 0 );
        foreach($orders as $item)
        {
           $oids[] = $item->orderNo;
        }

        $agents  = DB::table("ba_delivery_order")
         ->join("ba_profile", "ba_profile.id", "=", "ba_delivery_order.member_id")
         ->whereIn("order_no", $oids)
         ->select("ba_profile.*", "ba_delivery_order.order_no")
         ->get();



        foreach($orders as $item)
        {
           foreach($agents as $agent)
          {
            if($agent->order_no == $item->orderNo)
            {

             $item->deliveryAgentName = $agent->fullname;
             $item->deliveryAgentPhone = $agent->phone;

              break; 
            }
          }
        }
  
       
        $data= ["message" => "success", "status_code" =>  5007 ,  
        'detailed_msg' => 'Active orders fetched.' , 
        'results' => $orders];

        return json_encode( $data);
     }

     
 

     
     protected function viewOrderDetails(Request $request)
     {
        $user_info =  json_decode($this->getUserInfo( $request->userId ));
        if(  $user_info->user_id == "na" )
        {
            $data= ["message" => "failure", "status_code" =>  901 , 
            'detailed_msg' => 'User has no priviledge to access this feature! Please consult admin.'  ] ;
            return json_encode($data);
        }  

        if( $request->orderNo   == "" )
        {
         $data= ["message" => "failure", "status_code" =>  902 , 'detailed_msg' => 'Order No no specified.'  ];
         return json_encode($data);
        }
 
  

        $order_info   = DB::table("ba_service_booking")
        ->join("ba_profile", "ba_service_booking.book_by", "=", "ba_profile.id")
        ->where("ba_service_booking.id", $request->orderNo )
       
        ->select('ba_service_booking.id as orderNo',  "ba_service_booking.bin",
          "service_date as orderDate", 
           "ba_service_booking.service_date as serviceDate", 
            "fullname as customerName",  
            "address as deliveryAddress",  
            "phone as customerPhone",
            DB::Raw("'na' deliveryAgentName"),
            DB::Raw("'na' deliveryAgentPhone"),
            "book_status as status" ,
            "total_cost as totalCost",
            "delivery_charge as deliveryCharge", 
            "payment_type as paymentMode", 
            "cashback as cashBack",
            "discount",
            "coupon", 
            DB::Raw("'[]' orderItems") ,
            DB::Raw("'[]' timeline")
        )
        ->first() ;

      
 
 

        if( !isset($order_info) )
        {
            $data= ["message" => "failure", "status_code" =>  999 , 'detailed_msg' => 'No matching order found.'  ];
            return json_encode($data);
        }

   

        $order_items=  DB::table("ba_shopping_basket")
        ->join("ba_products","ba_shopping_basket.pr_code","=","ba_products.pr_code")
        ->select(
                "ba_products.id as productId",
                "ba_shopping_basket.pr_code as productCode",
                 "ba_shopping_basket.pr_name as item",
                 "ba_shopping_basket.description",
                 "ba_shopping_basket.qty" , 
                 "ba_shopping_basket.unit",
                 "ba_products.unit_price as price",
                 "ba_shopping_basket.package_charge as packageCharge",
                 "ba_shopping_basket.custom_text as customText",
                 "ba_shopping_basket.category_name as category",
                 "photos as image" , 
                DB::Raw("'[]' addOns")
             )
        ->where("ba_shopping_basket.order_no", $order_info->orderNo  )
        ->get();


        $order_add_ons =  DB::table("ba_shopping_item_add_on")  
        ->where("order_no", $order_info->orderNo  )
        ->select("addon_id as addOnId", "pr_code as productCode", "addon_name as addOnName", "addon_price as addOnPrice")
        ->get(); 

        $orderStatusInfo =  DB::table("ba_order_status_log")
        ->select("order_status as orderStatus","log_date as statusDateTime")
        ->where("order_no", $order_info->orderNo  )
        ->get();
  
 

          //Fetch Agent Information
         $agentInfo=  DB::table("ba_delivery_order")
         ->join("ba_profile", "ba_delivery_order.member_id", "=", "ba_profile.id")
         ->select("ba_profile.fullname as deliveryAgentName","phone as deliveryAgentPhone")
         ->where("order_no", $order_info->orderNo )
         ->get(); 

         $agentName="na";
         $agentPhone="na";

        foreach ($agentInfo as $value) 
        {
          $agentName = $value->deliveryAgentName;
          $agentPhone = $value->deliveryAgentPhone;
        }

  
        $folder_path =  "/public/assets/image/store/bin_" . $order_info->bin .  "/";
        foreach( $order_items as $item)
        {
          
          $all_photo = URL::to("/public/assets/image/no-image.jpg");
          $files =    explode(",",  $item->image )   ;

          $files=array_filter($files); 
 
            foreach( $files  as $file)
            {
                $file = trim($file); 
                if(  file_exists( base_path() .  $folder_path .   $file )   )
                {
                   $all_photo = URL::to( $folder_path  ) . "/" .    $file  ;
                   break;
                }
            }  
           $item->image = $all_photo; 

           $addOnes = array();
            foreach($order_add_ons as $addon)
            {
               if( $addon->productCode == $item->productCode )
               {
                $addOnes[] =  $addon;                 
               }
            } 
            $item->addOns =  $addOnes; 
            $item->customText = ($item->customText == "" ) ? "NONE" : $item->customText;


        }
 
        $order_info->orderItems =$order_items;
        $order_info->deliveryAgentName =$agentName;
        $order_info->deliveryAgentPhone =$agentPhone;   
        $order_info->timeline =$orderStatusInfo;
        $order_info->coupon = ($order_info->coupon == null ? "NONE" : $order_info->coupon); 


        $data= ["message" => "success", "status_code" =>  5007 ,  
        'detailed_msg' => 'Active orders fetched.' ,  
        'results' => $order_info ];
 

        return json_encode( $data);
     }







    // Order Processing (confirmed, order_packed and pickup_did_not_come)
    public function updateOrderStatus(Request $request)
    {
        $user_info =  json_decode($this->getUserInfo($request->userId));
        if(  $user_info->user_id == "na" )
        {
            $data= ["message" => "failure", "status_code" =>  901 , 'detailed_msg' => 'User has no priviledge to access this feature! Please consult admin.'  ];
            return json_encode($data);
        }


        //block if  user is not store owner code # 1 is store owner or business
        if( $user_info->category  != 1  )
        {
          $data= ["message" => "failure", "status_code" =>  901 , 'detailed_msg' => 'This feature is available to business owner.'  ];
          return json_encode($data);  
        }


        if( $request->orderNo == ""  ||  $request->orderStatus == "" || 
            ( $request->orderStatus != "confirmed" && $request->orderStatus != "completed" && 
                $request->orderStatus != "order_packed"  && $request->orderStatus != "pickup_did_not_come"   ) )
        {
         $data= ["message" => "failure", "status_code" =>  902 , 'detailed_msg' => 'Missing data to perform action.'  ];
         return json_encode($data);
        }


        //find order 
        $order_info  = ServiceBooking::find( $request->orderNo  );
        if( !isset($order_info))
        {
            $data= ["message" => "failure",   "status_code" =>  999 ,  'detailed_msg' => 'Order Infomation not found!'  ];
            return json_encode( $data);
        } 


        if( $user_info->bin   !=  $order_info->bin   )
        {
          $data= ["message" => "failure", "status_code" =>  901 , 
          'detailed_msg' =>  'Selected order not found!'   ];
          return json_encode($data);  
        }

        $order_info->book_status = $request->orderStatus;
        $save = $order_info->save();

        if( $save )
        {

           if($request->orderStatus == "order_packed" )
           {
            //update stock level  
            $order_items =  DB::table('ba_shopping_basket')
            ->where('order_no',   $request->orderNo  ) 
            ->get(); 
  
            foreach($order_items as $item)
            {
              DB::table('ba_products')
            ->where('pr_code',  $item->pr_code )
            ->decrement('stock_inhand',  $item->qty );  
 
            }  
            
           }
 

         DB::table('ba_order_status_log')
          ->where('order_no', $order_info->id)
          ->increment('seq_no', 1);
        
          $updateOrderStatus = new OrderStatusLogModel;
          $updateOrderStatus->order_no = $order_info->id;
          $updateOrderStatus->order_status= $order_info->book_status;
          $updateOrderStatus->log_date=  date('Y-m-d H:i:s' ); 
          $updateOrderStatus->save(); 

          SendSellerOrderProcessingSms::dispatch( $order_info->id , $order_info->book_status  );   

        $data= ["message" => "success", "status_code" =>  5073  ,  'detailed_msg' => 'Order status is successfully updated.'];

       }
       else
       {
          $data= ["message" => "failure", "status_code" =>  5074 ,  'detailed_msg' => 'Order status update failed.'];
       }

       return json_encode( $data);

    }


    protected function checkFavouriteBusiness(Request $request)
    {
      
      $user_info =  json_decode($this->getUserInfo( $request->userId ));
      if(  $user_info->user_id == "na" )
      {
        $data= ["message" => "failure", "status_code" =>  901 , 'detailed_msg' => 'User has no priviledge to access this feature! Please consult admin.'  ];
        return json_encode($data);
      }

      //block if  user is not agent. code # 0 is customer
      if( $user_info->category  != 0   )
      {
        $data= ["message" => "failure", "status_code" =>  901 , 'detailed_msg' => 'This feature is available to customer only.'  ];
        return json_encode($data);  
      }


      $count = DB::table("ba_fav_business")
      ->where("member_id", $user_info->member_id )
      ->where("bin", $request->bin )
      ->count();
 
      
      if(  $count > 0 )
      {
        $isFavourite = "yes";    
      }
      else 
      {
        $isFavourite = "no";  
      } 


      $data= ["message" => "success", "status_code" =>  7031 ,
      'detailed_msg' => 'Favourite status checking completed' , 
      'isFavourite' => $isFavourite];

      return json_encode( $data);
 

    }


    public function globalSearch(Request $request)
   {
      $keyword = $request->keyword;
      $businesses = Business::select(DB::raw('GROUP_CONCAT(tags) AS tags'), DB::raw('GROUP_CONCAT(name) AS names '))
      ->where("name", "like", "%$keyword%")
      ->orWhere("tags", "like", "%$keyword%")
      ->first(); 


    $alltags = explode(",", $businesses->tags);
    $tagList = array_filter(  array_unique($alltags) );

    $allnames  = explode(",", $businesses->names);
    $nameList  = array_filter( array_unique($allnames) );

    $merged = array_merge($nameList, $tagList); 

    sort($merged );
    
    
    $collection = collect( $merged );

    $flattened = $collection->flatten();

    $flattened->all();
        
    $result = array( 'tags' => $flattened) ;
        
    $data= ["message" => "success", "status_code" =>  1011 ,  'detailed_msg' => 'Business Tag Line', 'results' => $result];         
      return json_encode($data); 
    }




    // View Business Earning Report for daily
    protected function viewBusinessDailyEarning(Request $request)
    {

      $user_info =  json_decode($this->getUserInfo($request->userId));  
        if(  $user_info->user_id == "na" )
        {
            $data= ["message" => "failure", "status_code" =>  901 , 
            'detailed_msg' => 'User has no priviledge to access this feature! Please consult admin.'  ] ;
            return json_encode($data);
        } 

 

        if( $request->bin  == "")
        {
         $data= ["message" => "failure", "status_code" =>  902 , 'detailed_msg' => 'Missing data to perform action.'  ];
         return json_encode($data);
        }

   
        $business_info = Business::find( $request->bin ) ; 
        if( !isset($business_info) )
        {
            $data= ["message" => "failure", "status_code" =>  999 , 'detailed_msg' => 'No matching business found.'  ];
            return json_encode($data);
        } 

  


        if($request->reportDate == "" )
        {
          $today =   date('Y-m-d');  
        }
        else
        {
            $today = date('Y-m-d', strtotime( $request->reportDate ));
        }
 

        $profile   = DB::table("ba_profile") 
        ->join("ba_users", "ba_users.profile_id", "=", "ba_profile.id") 
        ->where("ba_users.bin",$business_info->id)
        ->select("ba_profile.*" )
        ->first();
        if( !isset($profile) )
        {
            $data= ["message" => "failure", "status_code" =>  999 , 'detailed_msg' => 'No matching business owner found.'  ];
            return json_encode($data);
        } 
 

        $totalEarning = 0;  

        $booking_list  = DB::table("ba_service_booking") 
        ->join("ba_profile", "ba_profile.id", "=", "ba_service_booking.book_by")  
        ->where("ba_service_booking.bin",$business_info->id) 
        ->whereIn("ba_service_booking.book_status", array('completed', 'delivered') )
        ->whereDate('ba_service_booking.service_date', $today)   
        ->select("ba_service_booking.id as orderNo", 
                 "ba_profile.fullname as orderBy",  
                 'ba_service_booking.book_status as orderStatus',
                 "ba_service_booking.total_cost as totalCost",
                 'ba_service_booking.delivery_charge as deliveryCharge',
                 DB::Raw("'0' deliverBy")
                 )
        ->get();
 
        if( !isset( $booking_list))
        {
          
          $data= [
              "message" => "success", "status_code" =>  5019 , 
              'detailed_msg' => 'Earning report are fetched.' ,
              "totalEarning" => $totalEarning,  
              'results' =>  array() ];
          return json_encode( $data); 

        }
 


        $customerIds =array();
        $agentIds = array();
        $orderNo =array();
        foreach ($booking_list as $item) 
        {
            $totalEarning += $item->totalCost;
            $customerIds[] = $item->orderBy;
            $agentIds[] = $item->deliverBy;
            $orderNo[] = $item->orderNo;


            //delivery info  

        }
 
        


 

         $agentInfo  = DB::table("ba_profile")
        ->whereIn("id", $agentIds )
        ->select("id as deliverBy","fullname")
        ->get();


        foreach ($booking_list as $item) 
        {
          foreach ($agentInfo as $aitem) 
          {
            if($item->deliverBy == $aitem->deliverBy )
            {
              $item->deliverBy= $aitem->fullname;break;
            }
 
          }   
        }



        $dealing_info   = DB::table("ba_payment_dealings")
        ->where("member_id",  $profile->id  )
        ->whereRaw("date(dealing_date) = '" . $today  . "'" ) 
        ->get();



        $totalPaid = 0;
        foreach($dealing_info as $item)
        {
          $totalPaid += $item->amount;
        }



        $data= ["message" => "success", "status_code" =>  5019 ,  'detailed_msg' => 'Earning report are fetched.' , 
        "totalEarning" => $totalEarning, 'paymentCleared' => $totalPaid,  'results' =>  $booking_list  ]; 

        return json_encode( $data);
     }




     // View Business Earning Report for monthly
     protected function viewBusinessMonthlyEarning(Request $request)
     {

 

        $user_info =  json_decode($this->getUserInfo($request->userId));  
        if(  $user_info->user_id == "na" )
        {
            $data= ["message" => "failure", "status_code" =>  901 , 
            'detailed_msg' => 'User has no priviledge to access this feature! Please consult admin.'  ] ;
            return json_encode($data);
        } 

 

         if( $request->bin  == "")
        {
         $data= ["message" => "failure", "status_code" =>  902 , 'detailed_msg' => 'Missing data to perform action.'  ];
         return json_encode($data);
        }

 


        $business_info = Business::find( $request->bin ) ;

 

        if( !isset($business_info) )
        {
            $data= ["message" => "failure", "status_code" =>  999 , 'detailed_msg' => 'No matching business found.'  ];
            return json_encode($data);
        } 

 


        if($request->year != "")
        {
            $year  = $request->year ; 
        }
        else
        {
            $year  = date('Y' );
        }

 

        if($request->month !="" )
        {

 

            $month = $request->month ;
        }
        else
        {
            $month = date('m' );
        }
        
        $totalEarning = 0;
        
         $booking_list  = DB::table("ba_service_booking")
        ->join("ba_profile", "ba_service_booking.book_by", "=", "ba_profile.id")
        ->join("ba_delivery_order", "ba_service_booking.id", "=", "ba_delivery_order.order_no")
        ->join("ba_order_status_log", "ba_service_booking.id", "=", "ba_order_status_log.order_no")
        ->where("ba_service_booking.bin",$business_info->id)
        ->whereIn("ba_order_status_log.order_status", array('completed', 'delivered') )
        ->whereMonth('ba_order_status_log.log_date', $month )
        ->whereYear('ba_order_status_log.log_date', $year )  
        ->select("ba_service_booking.id as orderNo", 
                 "ba_profile.fullname as orderBy",  
                 'book_status as orderStatus',
                 "total_cost as totalCost",
                 'delivery_charge as deliveryCharge',
                 "member_id as deliverBy"
                 )
        ->get();
 
       
       if( !isset( $booking_list))
        {
          
           $data= ["message" => "success", "status_code" =>  5019 ,  'detailed_msg' => 'Earning report are fetched.' , 
           "totalEarning" => $totalEarning,  'results' =>  array() ];
          return json_encode( $data);
          
        }

 


        $customerIds =array();
        $agentIds = array();
        $orderNo =array();
        foreach ($booking_list as $item) 
        {
            $totalEarning += $item->totalCost;
            $customerIds[] = $item->orderBy;
            $agentIds[] = $item->deliverBy;
            $orderNo[] = $item->orderNo;
        }

 


         $agentInfo  = DB::table("ba_profile")
        ->whereIn("id", $agentIds )
        ->select("id as deliverBy","fullname")
        ->get();

 

        
       foreach ($booking_list as $item) 
       {
            foreach ($agentInfo as $aitem) 
            {
                if($item->deliverBy == $aitem->deliverBy )
                {
                    $item->deliverBy= $aitem->fullname;break;
                }

 

            }   
       }

 


        $data= ["message" => "success", "status_code" =>  5019 ,  'detailed_msg' => 'Earning report are fetched.' , 
        "totalEarning" => $totalEarning,  'results' =>  $booking_list  ];

 

        return json_encode( $data);
     }


 
  
  public function getPaidPromotions(Request $request)
  {

    $premium_businesses = DB::table("ba_premium_business") 
    ->get();

    $bins = array(); 
    foreach ($premium_businesses as $item) 
    {
        $bins[] = $item->bin; 
    }

    
    /*
      $favorited = FavouriteBusiness::where('member_id',$request->userId)
      ->where('bin',$item->bin)->get(); 
      */
 
      
    $businesses = DB::table("ba_business")
    ->join("ba_business_category", "ba_business_category.name", "=", "ba_business.category")
    ->whereIn('ba_business.id',$bins)   
    ->select( 'ba_business.id as bin',  "ba_business.name", "ba_business.tag_line as tagLine", 'ba_business.shop_number as shopNumber',  
      'ba_business.phone_pri as primaryPhone', 'ba_business.phone_alt as alternatePhone', 
      "ba_business.locality", "ba_business.landmark", "ba_business.city", "ba_business.state", "ba_business.pin", 
      'ba_business.tags','ba_business.rating','ba_business.latitude','ba_business.longitude' , 
      "ba_business.profile_image as profileImgUrl",   "ba_business.banner as bannerImgUrl" ,
      DB::Raw("'na' promoImgUrl"),DB::Raw("'0' favourite"), 
      "ba_business.category as businessCategory", 
      "ba_business_category.main_type as mainBusinessType",  
       "is_open as isOpen" )
    ->orderBy("is_open", "asc")
    ->paginate(100);

    //BusinessCategory

    $openBiz = $closeBiz  = array();
    foreach($businesses as $item)
    {

      if($item->isOpen == "open" )
      {
        $openBiz[] = $item->bin;
      }
      else 
      {
        $closeBiz[] = $item->bin;
      }
      
 
 

      if($item->profileImgUrl!=null)
      {
          $item->profileImgUrl = URL::to('/public'.$item->profileImgUrl); 
      }
      else
      {
          $item->profileImgUrl = URL::to('/public/assets/image/no-image.jpg')  ;   
      }

      if($item->promoImgUrl!='na')
      {
          $item->promoImgUrl = URL::to('/public'.$item->promoImgUrl); 
      }
      else
      {
          $item->promoImgUrl = URL::to('/public/assets/image/no-image.jpg')  ;   
      }
   

        //compression   
        if($item->bannerImgUrl != null )
        {
          $source = public_path() .  $item->bannerImgUrl;
          $pathinfo = pathinfo( $source  );
          $new_file_name  =  $pathinfo['filename'].  "_sm" . '.' . $pathinfo['extension'];
          $destination  = base_path() .  "/public/assets/image/business/bin_" . $item->bin . "/"   . $new_file_name ; 

          if(file_exists(  $source )  )
          {

            $info = getimagesize($source ); 

              if ($info['mime'] == 'image/jpeg')
                  $image = imagecreatefromjpeg($source);

              elseif ($info['mime'] == 'image/gif')
                  $image = imagecreatefromgif($source);

              elseif ($info['mime'] == 'image/png')
                  $image = imagecreatefrompng($source);


            $original_w = $info[0];
            $original_h = $info[1];
            $thumb_w = 290; // ceil( $original_w / 2);
            $thumb_h = 200; //ceil( $original_h / 2 );

            $thumb_img = imagecreatetruecolor($original_w, $original_h);
            imagecopyresampled($thumb_img, $image,
                               0, 0,
                               0, 0,
                               $original_w, $original_h,
                               $original_w, $original_h);

            imagejpeg($thumb_img, $destination, 20);
          }
          $item->bannerImgUrl =  URL::to("/public/assets/image/business/bin_" . $item->bin . "/"   . $new_file_name );   

        }
        else
        {
          $item->bannerImgUrl = URL::to('/public/assets/image/no-image.jpg')  ;  
        }

        $item->favourite = 0; 

      } 


      //shuffling 
      shuffle($openBiz);
      shuffle($closeBiz);
  
      $finalBiz = array();
      foreach($openBiz as $obin)
      {
        foreach($businesses as $item)
        {
          if($item->bin == $obin)
          {
            $finalBiz[] = $item;
            break;
          }

        } 
      }

      foreach($closeBiz as $cbin)
      {
        foreach($businesses as $item)
        {
          if($item->bin == $cbin)
          {
            $finalBiz[] = $item;
            break;
          }

        }

      }
 
        
        $data["from"] =  1;
        $data["last_page"] =  1; 
        $data["per_page"] = 10;
        $data["to"] = 9;
        $data["total"] =  9;
        $data["current_page"] =  1;
        $data["data"] =  $finalBiz;




      $result = array( 'results' => $data) ; 
      return json_encode($result); 
    }




    public function getProductUnits(Request $request)
    {

      $units  =  DB::table("ba_product_units")  
      ->select( "unit" ) 
      ->orderBy("unit") 
      ->whereIn("bin",  array($request->bin, 0)) 
      ->get();
       

      if(isset($units))
      {
        $data = [ "message" => "success",  'status_code' => 955, 'detailed_msg' => 'Units are fetched.', 
        'results' =>  $units ];
      }
      else 
      {
        $data = [ "message" => "failure",  'status_code' => 956, 'detailed_msg' => 'Units could not be fetced.',
         'results' =>  array()  ];
      }
      
      return json_encode($data); 
     
    } 


    public function addProductUnit(Request $request)
    {

      $unit_info =ProductUnitModel::where("bin", $request->bin)
      ->where("unit",  $request->unit )
      ->first();
       

      if( !isset($unit_info ))
      {
         $unit_info = new ProductUnitModel;
      }

      $unit_info->unit = $request->unit;
      $unit_info->bin = $request->bin;

      $unit_info->save();
      $data = [ "message" => "success",  'status_code' => 957, 'detailed_msg' => 'Unit updated.'  ];
     
      return json_encode($data); 
     
    } 



    public function markOutOfStock(Request $request)
    {

        $user_info =  json_decode($this->getUserInfo($request->userId));  

        if(  $user_info->category != 1 )
        {
            $data= ["message" => "failure", "status_code" =>  901 , 
            'detailed_msg' => 'Only business owner has access to this feature! Please signin using your business acount.'  ] ;
            return json_encode($data);
        } 

  

      $product  = ProductModel::where("id", $request->productId )
      ->where("bin", $user_info->bin )
      ->first() ;

      if( !isset($product))
      {
        $data = [ "message" => "failure",  'status_code' => 999, 'detailed_msg' => 'Product not found.'  ];
        return json_encode($data); 
      }
 
      $product->stock_inhand =  0 ;  
      $product->save();

 

      $data = [ "message" => "success",  'status_code' => 8001, 'detailed_msg' => 'Product is marked out of stock.'  ];
     
      return json_encode($data); 
     
    }




    public function importProductsFromExcel(Request $request)
    {

        $user_info =  json_decode($this->getUserInfo($request->userId));  

        if(  $user_info->category != 1 )
        {
            $data= ["message" => "failure", "status_code" =>  901 , 
            'detailed_msg' => 'Only business owner has access to this feature! Please signin using your business acount.'  ] ;
            return json_encode($data);
        } 

  

      $product  = ProductModel::where("id", $request->productId )
      ->where("bin", $user_info->bin )
      ->first() ;

      if( !isset($product))
      {
        $data = [ "message" => "failure",  'status_code' => 999, 'detailed_msg' => 'Product not found.'  ];
        return json_encode($data); 
      }
 
      $product->stock_inhand =  0 ;  
      $product->save();

 

      $data = [ "message" => "success",  'status_code' => 8001, 'detailed_msg' => 'Product is marked out of stock.'  ];
     
      return json_encode($data); 
     
    }


}
