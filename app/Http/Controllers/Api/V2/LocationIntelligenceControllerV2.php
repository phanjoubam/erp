<?php

namespace App\Http\Controllers\Api\V2; 

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Crypt;  
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\File;
use Illuminate\Contracts\Encryption\DecryptException;
use App\Http\Controllers\Controller;

 
use App\Business;  
use App\ProductModel;
use App\AppCacheModel;
use App\ShoppingOrderModel;
use App\ShoppingOrderItemsModel;
use App\AddressModel;
use App\ScannedAddress;
use App\ScannedPin;
use App\User;

use App\Traits\Utilities;





class LocationIntelligenceControllerV2 extends Controller
{
	use Utilities;

     protected function generateNearbyAddresses(Request $request)
     {

        //https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=795001&destinations=795008&key=AIzaSyC8K4TaMdo9cvF7EyQGMkYFPVOfLm6iIKg

        $category  =  $request->category; 
        $businesses  =  DB::table("ba_business")  
        ->select( 'id as bin',  "name", "tag_line as tagLine", 'shop_number as shopNumber',  
          'phone_pri as primaryPhone', 'phone_alt as alternatePhone', 
          "locality", "landmark", "city", "state", "pin", 
          'tags','rating','latitude','longitude' , 
          "profile_image as profileImgUrl",   "banner as bannerImgUrl" ,
        DB::Raw("'na' promoImgUrl"),DB::Raw("'na' favourite"),"category as businessCategory",
        DB::Raw("'0' distance") ,  "is_open as isOpen"
         )
        ->where("category", $category)
        ->paginate(35); 

        $nearby_business = array();  
        $key = 'AIzaSyBELOQX7aTx-1k9ULzTuWPHTrrj--cMDkw';
        $source  =  $request->pinCode;  

        foreach($businesses  as $item)
        {

            $target = $item->pin;
            if( $target == null || $target=="") continue; 

            $url = 'https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=' .$source . '&destinations=' . $target . '&key=' . $key;


            $json_data = file_get_contents($url ); // get json content  
            $output =  json_decode($json_data); 

            if (json_last_error() === JSON_ERROR_NONE) 
            { 
                // JSON is valid 
                if( $output->status =="OK" )
                {
                    if(    count(  $output->rows ) > 0 &&   count(  $output->rows[0]->elements ) > 0  &&  $output->rows[0]->elements[0]->status =="OK"   )
                    {
                         
                        $distance_str =  $output->rows[0]->elements[0]->distance->text   ; 
                        $distance =  explode(" ", $distance_str);

                        if(count($distance) > 0)
                        {
                             $item->distance  = $distance[0] ;
                             if( $distance[0] < 15)
                             {
                                $nearby_business[] =$item ; 
                             }

                        }

                        $item->distance  += 0.0;

                        if($item->profileImgUrl!=null)
                        {
                            $item->profileImgUrl = URL::to('/public'.$item->profileImgUrl); 
                        }
                        else{
                            $item->profileImgUrl = URL::to('/public/assets/image/no-image.jpg')  ;   
                        }

                        if($item->bannerImgUrl!=null)
                        {
                            $item->bannerImgUrl = URL::to('/public'.$item->bannerImgUrl);   
                        }
                        else{
                            $item->bannerImgUrl = URL::to('/public/assets/image/no-image.jpg')  ;    
                        }

                        if($item->promoImgUrl!='na')
                        {
                            $item->promoImgUrl = URL::to('/public'.$item->promoImgUrl); 
                        }
                        else{
                            $item->promoImgUrl = URL::to('/public/assets/image/no-image.jpg')  ;     
                        }

                    }  
                } 
            }
 
            $item->favourite =  0; 
            $item->latitude  = round($item->latitude * 1.00, 2);
            $item->longitude  = round($item->longitude * 1.00 , 2); 

        }
 



        $final_result['current_page'] = $businesses->currentPage( ); 
        $final_result['first_page_url'] =  $businesses->url($businesses->currentPage( ) ) ; 
        $final_result['last_page'] = $businesses->lastPage( );
        $final_result['last_page_url'] =$businesses->url($businesses->lastPage( ) ) ;  
        $final_result['next_page_url'] = $businesses->nextPageUrl();   
        $final_result['per_page'] = $businesses-> perPage();
        $final_result['prev_page_url'] = $businesses->previousPageUrl();
        $final_result['total'] = $businesses->total( );
        $final_result['to'] = $businesses-> perPage();
        $final_result['path'] =  $request->url(); 
        $final_result['from'] =  intval( $businesses->total( ) /  $businesses-> perPage() ) ; 
        $final_result['data'] = $nearby_business ;
        $result = array( 'results' => $final_result) ;

        return json_encode($result); 
  
     }

 
    protected function fetchNearbyAddresses(Request $request)
    { 
        $category  =  $request->category; 
        $user_info =  json_decode($this->getUserInfo($request->userId) ); 
        $businesses = $ids = array();

        if(  $user_info->member_id > 0  )
        {
            $cached_list  =  DB::table("ba_app_cache")  
            ->join("ba_addresses", "ba_addresses.id", "=", "ba_app_cache.address_id") 
            ->where("profile_id", $user_info->member_id)
            ->where("address_id", $request->addressId )
            ->where("cache_type", "nearby_biz")
            ->select("cache_values")
            ->first(); 
 

            if(isset($cached_list))
            {
                $items = json_decode( $cached_list->cache_values   );
                foreach( $items->business as $bitem )
                {
                    $ids[] = $bitem;
                }   
            }
  

            $appcache = AppCacheModel::where("cache_type", "nearby_biz")
            ->where("profile_id", $user_info->member_id )
            ->where("address_id",  $request->addressId )
            ->first();   
            
            $member_id = $user_info->member_id;
        }
        else 
        {
            $appcache = AppCacheModel::where("cache_type", "nearby_biz")
            ->where("profile_id",  0 )
            ->where("pin_code",  $request->pinCode )
            ->first(); 
            $member_id =  0 ;
        }

        $nearby_business =array();

        if(count($ids) > 0)
        {
            $businesses  =  DB::table("ba_business")  
            ->select( 'id as bin',  "name", "tag_line as tagLine", 'shop_number as shopNumber', 
                'phone_pri as primaryPhone', 'phone_alt as alternatePhone', 
                "locality", "landmark", "city", "state", "pin", 
                'tags','rating','latitude','longitude' , 
                "profile_image as profileImgUrl",   "banner as bannerImgUrl" ,
                DB::Raw("'na' promoImgUrl"),DB::Raw("'na' favourite"),"category as businessCategory",
                DB::Raw("'0' distance") ,  "is_open as isOpen" )
            ->whereIn("id", $ids )
            ->paginate(10);


            foreach($businesses  as $item)
            {
                $item->distance  =  number_format( $item->distance ,6 , '.', '') ; 
                if($item->profileImgUrl!=null)
                {
                    $item->profileImgUrl = URL::to('/public'.$item->profileImgUrl); 
                }
                else
                {
                    $item->profileImgUrl = URL::to('/public/assets/image/no-image.jpg')  ;   
                }


                if($item->promoImgUrl!='na')
                {
                    $item->promoImgUrl = URL::to('/public'.$item->promoImgUrl); 
                }
                else
                {
                    $item->promoImgUrl = URL::to('/public/assets/image/no-image.jpg')  ;     
                }
                
                //banner url 
                if($item->bannerImgUrl != null )
                {
                    $source = public_path() .  $item->bannerImgUrl;
                    $pathinfo = pathinfo( $source  );
                    $new_file_name  =  $pathinfo['filename'].  "_sm" . '.' . $pathinfo['extension'];
                    $destination  = base_path() .  "/public/assets/image/business/bin_" . $item->bin . "/"   . $new_file_name ; 
                    
                    if(file_exists(  $source ) )
                    {
                        $info = getimagesize($source ); 
                        if ($info['mime'] == 'image/jpeg')
                            $image = imagecreatefromjpeg($source);
                        elseif ($info['mime'] == 'image/gif')
                            $image = imagecreatefromgif($source);
                        elseif ($info['mime'] == 'image/png')
                            $image = imagecreatefrompng($source);

                        $original_w = $info[0];
                        $original_h = $info[1];
                        $thumb_w = 290; // ceil( $original_w / 2);
                        $thumb_h = 200; //ceil( $original_h / 2 );

                        $thumb_img = imagecreatetruecolor($original_w, $original_h);
                        imagecopyresampled($thumb_img, $image,
                                0, 0,
                                0, 0,
                                $original_w, $original_h,
                                $original_w, $original_h ); 
                        imagejpeg($thumb_img, $destination, 20);


                    }
                    $item->bannerImgUrl =  URL::to("/public/assets/image/business/bin_" . $item->bin . "/"   . $new_file_name );     

                }
                else
                {
                    $item->bannerImgUrl = URL::to('/public/assets/image/no-image.jpg')  ;    
                }

                //banner url
                $item->favourite =  0;
                $item->latitude  =  number_format( $item->latitude ,6 , '.', '')   ; 
                $item->longitude  =  number_format( $item->longitude , 6 , '.', '')  ;    

                $nearby_business[] = $item; 
            }


        }
        else 
        {

            $city_name  = DB::table("ba_addresses") 
            ->select("city", "pin_code" )
            ->where("id", $request->addressId ) 
            ->first(); 



            $city  =    "all"; //records  

            $businesses  =  DB::table("ba_business")  
            ->select( 'id as bin',  "name", "tag_line as tagLine", 'shop_number as shopNumber',  
                'phone_pri as primaryPhone', 'phone_alt as alternatePhone', 
                "locality", "landmark", "city", "state", "pin", 
                'tags','rating','latitude','longitude' , 
                "profile_image as profileImgUrl",   "banner as bannerImgUrl" ,
                DB::Raw("'na' promoImgUrl"),DB::Raw("'na' favourite"),"category as businessCategory",
                DB::Raw("'0' distance") ,  "is_open as isOpen" )
            ->where("category", $category)
            ->where("city", $city)
            ->paginate(30);
         
            $nearby_business_found = array();  
            $key = 'AIzaSyBELOQX7aTx-1k9ULzTuWPHTrrj--cMDkw';
            $source  =  $request->pinCode;  

            foreach($businesses  as $item)
            {
                $target = $item->pin;  
                if( $target == null || $target=="") continue;  


                $url = 'https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=' .$source . '&destinations=' . $target . '&key=' . $key;


                $json_data = file_get_contents($url ); // get json content  
                $output =  json_decode($json_data); 
                if (json_last_error() === JSON_ERROR_NONE) 
                {
                    // JSON is valid 
                    if( $output->status =="OK" )
                    {
                        if(    count(  $output->rows ) > 0 &&   count(  $output->rows[0]->elements ) > 0  &&  $output->rows[0]->elements[0]->status =="OK"   )
                        {
                             
                            $distance_str =  $output->rows[0]->elements[0]->distance->text   ; 
                            $distance =  explode(" ", $distance_str); 
                            $travel_duration_sec   =  $output->rows[0]->elements[0]->duration->value   ; 
                            $travel_duration_min   = floor($travel_duration_sec/60);
  

                            if(count($distance) > 0  )
                            { 
                                 $item->distance  = number_format(  $distance[0] , 2 , '.', '')  ;
                                
                                 if( $distance[0] < 15 && $travel_duration_min <= 1800  ) 
                                 {
                                    $nearby_business_found[] =  $item->bin;
                                    $nearby_business[] =$item ;  
                                 } 
                            }

                            if($item->profileImgUrl!=null)
                            {
                                $item->profileImgUrl = URL::to('/public'.$item->profileImgUrl); 
                            }
                            else
                            {
                                $item->profileImgUrl = URL::to('/public/assets/image/no-image.jpg')  ;   
                            }
                            if($item->promoImgUrl!='na')
                            {
                                $item->promoImgUrl = URL::to('/public'.$item->promoImgUrl); 
                            }
                            else
                            {
                                $item->promoImgUrl = URL::to('/public/assets/image/no-image.jpg')  ;     
                            }

                            //banner url 
                            if($item->bannerImgUrl != null )
                            {
                                $source = public_path() .  $item->bannerImgUrl;
                                $pathinfo = pathinfo( $source  );
                                $new_file_name  =  $pathinfo['filename'].  "_sm" . '.' . $pathinfo['extension'];
                                $destination  = base_path() .  "/public/assets/image/business/bin_" . $item->bin . "/"   . $new_file_name ; 
                                if(file_exists(  $source ) )
                                {
                                    $info = getimagesize($source ); 
                                    if ($info['mime'] == 'image/jpeg')
                                        $image = imagecreatefromjpeg($source);
                                    elseif ($info['mime'] == 'image/gif')
                                        $image = imagecreatefromgif($source);
                                    elseif ($info['mime'] == 'image/png')
                                        $image = imagecreatefrompng($source);
                                    $original_w = $info[0];
                                    $original_h = $info[1];
                                    $thumb_w = 290; // ceil( $original_w / 2);
                                    $thumb_h = 200; //ceil( $original_h / 2 );

                                    $thumb_img = imagecreatetruecolor($original_w, $original_h);
                                    imagecopyresampled($thumb_img, $image,
                                                           0, 0,
                                                           0, 0,
                                                           $original_w, $original_h,
                                                           $original_w, $original_h);

                                    imagejpeg($thumb_img, $destination, 20);
                                }
                                $item->bannerImgUrl =  URL::to("/public/assets/image/business/bin_" . $item->bin . "/"   . $new_file_name );

                            }
                            else
                            {
                                $item->bannerImgUrl = URL::to('/public/assets/image/no-image.jpg')  ;    
                            }
 
                            //banner url
                        }  
                    } 
                }

                $item->favourite = 0 ; 
                $item->latitude  = number_format( $item->latitude , 6 , '.', '')  ; 
                $item->longitude  = number_format( $item->longitude , 6 , '.', '') ;    

            }

            //saved matched businesses in the cache files  

            if( !isset($appcache ))
            {
                $appcache  = new AppCacheModel;
                $appcache->profile_id = $member_id; 
                $appcache->pin_code = $request->pinCode; 
            }
            $appcache->cache_type = "nearby_biz";
            $appcache->cache_values =  json_encode( array("business" => $nearby_business_found ));
            $appcache->cache_date = date('Y-m-d H:i:s');
            $appcache->save();  

        }
   

        $final_result['current_page'] = $businesses->currentPage( ); 
        $final_result['first_page_url'] =  $businesses->url($businesses->currentPage( ) ) ; 
        $final_result['last_page'] = $businesses->lastPage( );
        $final_result['last_page_url'] =$businesses->url($businesses->lastPage( ) ) ;  
        $final_result['next_page_url'] = $businesses->nextPageUrl();   
        $final_result['per_page'] = $businesses-> perPage();
        $final_result['prev_page_url'] = $businesses->previousPageUrl();
        $final_result['total'] = $businesses->total( );
        $final_result['to'] = $businesses-> perPage();
        $final_result['path'] =  $request->url(); 
        $final_result['from'] =  intval( $businesses->total( ) /  $businesses-> perPage() ) ; 
        $final_result['data'] = $nearby_business ; 
        $result = array( 'results' => $final_result) ; 
        return json_encode($result);  
     }


    protected function fetchNearbyBusinessViaGeoPosition(Request $request)
    {

        $cdn_url =   $_ENV['APP_CDN'] ;  
        $cdn_path =   $_ENV['APP_CDN_PATH'] ;  

        $state = $city_name = "na";
        $category  =  $request->category; 
        $user_info =  json_decode($this->getUserInfo($request->userId) ); 
        $businesses = $ids = array(); 
        $final_result = $nearby_business_found = $nearby_business =array(); 



        if( $request->addressId == "" )
        {
            $selected_address  = DB::table("ba_addresses")  
            ->where("id", $request->addressId ) 
            ->select('city', "state", 'latitude', 'longitude', "pin_code" )
            ->first();

            if( !isset($selected_address))
            {
                $selected_address  = DB::table("ba_profile")  
                ->where("id", $user_info->member_id) 
                ->select('city', "state",  'latitude', 'longitude', "pin_code")
                ->first(); 
            }

            if( !isset($selected_address))
            {
                $data = array("message" => "failure", "status_code" =>  9018 ,  
                'detailed_msg' => 'No nearby business found. Please retry after changing or updating your address.'  ); 
                return json_encode( $data); 
            } 

        }
        else 
        {

        }


        if(  $user_info->member_id > 0  ) //login user 
        {
            $member_id = $user_info->member_id;

            if( $request->addressId != "" ) //addressId is specified
            {
                $selected_address  = DB::table("ba_addresses")  
                ->where("id", $request->addressId ) 
                ->select('city', "state", 'latitude', 'longitude', "pin_code" )
                ->first(); 

            }
            else  //addressId not specified
            { 

                $selected_address  = DB::table("ba_profile")  
                ->where("id", $user_info->member_id) 
                ->select('city', "state",  'latitude', 'longitude', "pin_code")
                ->first();  
            } 
        }
        else
        {
            
            $selected_address  = DB::table("ba_addresses")  
            ->where("pin_code", $request->pinCode  ) 
            ->select('city', "state",  'latitude', 'longitude', "pin_code")
            ->first(); 

        } 

        if( !isset($selected_address))
        {
            $data = array("message" => "failure", "status_code" =>  9018 ,   'detailed_msg' => 'No nearby business found. Please retry after changing or updating your address.'  ); 
            return json_encode( $data); 
        }
 
         $state =  $selected_address->state; 
         $city_name =  $selected_address->state;  
        

        $businesses = DB::table("ba_business")
        ->join("ba_business_category", "ba_business_category.name", "=", "ba_business.category")  
        ->select( 'ba_business.id as bin',  "ba_business.name", "ba_business.tag_line as tagLine", 
         'ba_business.shop_number as shopNumber',   'ba_business.phone_pri as primaryPhone', 'ba_business.phone_alt as alternatePhone', 
         "ba_business.locality", "ba_business.landmark", "ba_business.city", "ba_business.state", "ba_business.pin", 
         'ba_business.tags','ba_business.rating','ba_business.latitude','ba_business.longitude' , 
         "ba_business.profile_image as profileImgUrl",   "ba_business.banner as bannerImgUrl" ,
          DB::Raw("'na' promoImgUrl"),DB::Raw("'0' favourite"), 
          "ba_business.category as businessCategory",  DB::Raw("'0' distance") ,
         "ba_business_category.main_type as mainBusinessType",  
         "is_open as isOpen" )  
        ->where("category", $category ) 
        ->where("state", $state ) 
        ->whereRaw(" latitude <> 0 and longitude <> 0 ")
        ->orderBy("is_open", "asc")
        ->paginate(20);


        $source_lat = $selected_address->latitude;
        $source_long = $selected_address->longitude;
        $source_state  = $selected_address->state;  
        $city_name = $selected_address->city;  
 


        $nearby_business = array();
        foreach($businesses  as $item)
        {
            $target_lat = $item->latitude;  
            $target_long = $item->longitude;   
            $target =  $target_lat . "," . $target_long ;
            
            $dist = $this->getDrivingDistance( $source_lat ,  $source_long,  $target_lat, $target_long ); 

            $distance = explode(" " , $dist['distance'])    ; 
            $item->latitude = number_format( $item->latitude , 8 , '.', '')   ; 
            $item->longitude = number_format( $item->longitude , 8 , '.', '')  ;   
            $item->distance  =  count($distance) > 0 ? $distance[0] : "0.00";
            $item->distance  = number_format(  $distance[0] , 2 , '.', '')  ;
            $item->duration = $dist['time'];

            

            $item->favourite = 0; 
        
            $item->shopNumber = ($item->shopNumber == "" ) ? "NA" : $item->shopNumber;

            if($item->profileImgUrl!=null)
            {
                $item->profileImgUrl =  $cdn_url   .$item->profileImgUrl ; 
            }
            else
            {
                $item->profileImgUrl =  $cdn_url .'/assets/image/no-image.jpg'  ;   
            }


            if($item->promoImgUrl!='na')
            {
                $item->promoImgUrl = $cdn_url   .$item->promoImgUrl ;  
            }
            else
            {
                $item->promoImgUrl =  $cdn_url .'/assets/image/no-image.jpg'  ;  
            }
 
            
            if($item->bannerImgUrl != null )
            {
                $source = $cdn_path  .  $item->bannerImgUrl;
                $pathinfo = pathinfo( $source  );
                $new_file_name  =  $pathinfo['filename'].  "_sm" . '.' . $pathinfo['extension'];
                $destination  = $cdn_path . "/assets/image/business/bin_" . $item->bin . "/"   . $new_file_name ; 
                
                if(file_exists(  $source ) )
                {
                    $info = getimagesize($source ); 
                    if ($info['mime'] == 'image/jpeg')
                    $image = imagecreatefromjpeg($source);
                    elseif ($info['mime'] == 'image/gif')
                    $image = imagecreatefromgif($source);
                    elseif ($info['mime'] == 'image/png')
                    $image = imagecreatefrompng($source); 
                    $original_w = $info[0];
                    $original_h = $info[1];
                    $thumb_w = 290;  
                    $thumb_h = 200;   
                    $thumb_img = imagecreatetruecolor($original_w, $original_h);
                    imagecopyresampled($thumb_img, $image, 0, 0, 0, 0, $original_w, $original_h, $original_w, $original_h ); 
                    imagejpeg($thumb_img, $destination, 20);  
                }
                $item->bannerImgUrl =  $cdn_url . "/assets/image/business/bin_" . $item->bin . "/"   . $new_file_name  ;     
                
            }
            else
            {
                $item->bannerImgUrl = $cdn_url .'/assets/image/no-image.jpg'  ;      
            } 

            $nearby_business[] =$item ;

        }


        $final_result['last_page'] = $businesses->lastPage( ); 
        $final_result['data'] = $nearby_business ;   
        $result = array( 'results' => $final_result) ;
        return json_encode($result);  

    }





    protected function fetchNearbyBusinessViaGeoPosition_caching(Request $request)
    {

        $key = 'AIzaSyC9KrtxONZFci3FxO0SPZJSejUXHpvgmHI' ;    
        $category  =  $request->category; 
        $user_info =  json_decode($this->getUserInfo($request->userId) ); 
        $businesses = $ids = array(); 
        $final_result = $nearby_business_found = $nearby_business =array(); 


        if(  $user_info->member_id > 0  )
        {
            $member_id = $user_info->member_id;
            $cached_list  =  DB::table("ba_app_cache")  
            ->join("ba_addresses", "ba_addresses.id", "=", "ba_app_cache.address_id") 
            ->where("profile_id",  $member_id  )
            ->where("address_id", $request->addressId )
            ->where("biz_category",  $request->category )
            ->where("cache_type", "nearby_biz")
            ->select("cache_values")
            ->first(); 
  

                $ids = array();

                if(isset($cached_list))
                {
                    $items = json_decode( $cached_list->cache_values   );
                    foreach( $items->business as $bitem )
                    {
                        $ids[] = $bitem->bin;
                    }   
                }
                  

                if(count( $ids )  > 0 )
                {

                    //show from scanned list   
                    $businesses = DB::table("ba_business")
                    ->join("ba_business_category", "ba_business_category.name", "=", "ba_business.category")  
                    ->select( 'ba_business.id as bin',  "ba_business.name", "ba_business.tag_line as tagLine", 
                        'ba_business.shop_number as shopNumber',  
                      'ba_business.phone_pri as primaryPhone', 'ba_business.phone_alt as alternatePhone', 
                      "ba_business.locality", "ba_business.landmark", "ba_business.city", "ba_business.state", "ba_business.pin", 
                      'ba_business.tags','ba_business.rating','ba_business.latitude','ba_business.longitude' , 
                      "ba_business.profile_image as profileImgUrl",   "ba_business.banner as bannerImgUrl" ,
                      DB::Raw("'na' promoImgUrl"),DB::Raw("'0' favourite"), 
                      "ba_business.category as businessCategory",  DB::Raw("'0' distance") ,
                      "ba_business_category.main_type as mainBusinessType",  
                       "is_open as isOpen" ) 
                    ->whereIn('ba_business.id', $ids )  
                    ->where("category", $category ) 
                    ->orderBy("is_open", "asc")
                    ->paginate(10);
  

                    foreach($businesses  as $item)
                    {
                        $nearby_business[] =$item ;
                    }
                    
                }
                else 
                {

                    //no scanned list found. Recalculated and update
                    $selected_address  = DB::table("ba_addresses")  
                    ->where("id", $request->addressId ) 
                    ->select('city', 'latitude', 'longitude', "pin_code")
                    ->first();
                    
                    if( !isset($selected_address))
                    {
                        $selected_address  = DB::table("ba_profile")  
                        ->where("id", $user_info->member_id) 
                        ->select('city', 'latitude', 'longitude', "pin_code")
                        ->first(); 
                    }

                    if( !isset($selected_address))
                    {
                        $data = array("message" => "failure", "status_code" =>  9018 ,  
                            'detailed_msg' => 'No nearby business found. Please retry after changing or updating your address.'  ); 
                        return json_encode( $data); 
                    } 
  
                    $source_lat = $selected_address->latitude;  
                    $source_long = $selected_address->longitude;  
                    $source_pin  = $selected_address->pin_code;  
                    $city_name = $selected_address->city; 

                    $city_name_parts  = explode(" ", $selected_address->city); 
                    $name_parts  = preg_split("/[_,\- ]+/", $city_name); 
                    $where_clause = " ( category ='" . $category . "' and latitude <> 0 and longitude <> 0  ) and ( "; 

 
                    $like_clause = array();
                    foreach($name_parts as $nitem)
                    {
                        $like_clause[] = " city like '%" . $nitem  . "%' ";
                    }

                    $where_clause .=  implode(" or ", $like_clause) . " ) ";

 
                    
                    if(    $source_lat == 0 && $source_long == 0   )
                    {
                        
                        if(   $source_pin == "" )
                        {
                            $data = array("message" => "failure", "status_code" =>  9018 ,  
                            'detailed_msg' => 'No nearby business found. Please retry after changing or updating your address.'  ); 
                            return json_encode( $data); 
                        }
                        else 
                        {
                            $source =  $source_pin;
                        } 
                    }
                    else 
                    {
                        $source =  $source_lat. "," .$source_long ;    
                    } 


                    //city found    
                    $businesses = DB::table("ba_business")
                    ->join("ba_business_category", "ba_business_category.name", "=", "ba_business.category")  
                    ->select( 'ba_business.id as bin',  "ba_business.name", "ba_business.tag_line as tagLine", 
                        'ba_business.shop_number as shopNumber',  
                      'ba_business.phone_pri as primaryPhone', 'ba_business.phone_alt as alternatePhone', 
                      "ba_business.locality", "ba_business.landmark", "ba_business.city", "ba_business.state", "ba_business.pin", 
                      'ba_business.tags','ba_business.rating','ba_business.latitude','ba_business.longitude' , 
                      "ba_business.profile_image as profileImgUrl",   "ba_business.banner as bannerImgUrl" ,
                      DB::Raw("'na' promoImgUrl"),DB::Raw("'0' favourite"), 
                      "ba_business.category as businessCategory",  DB::Raw("'0' distance") ,
                      "ba_business_category.main_type as mainBusinessType",  
                       "is_open as isOpen" ) 
                    ->whereRaw( $where_clause )   
                    ->orderBy("is_open", "asc")  
                    ->paginate(50); 

                    $scanned_bins = array();

                    foreach($businesses  as $item)
                    {
                        $target_lat = $item->latitude;  
                        $target_long = $item->longitude;   
                        $target =  $target_lat . "," . $target_long ; 

                        $url = 'https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=' .$source . '&destinations=' . $target . '&key=' . $key;
 
                       
                        $json_data = file_get_contents($url ); // get json content  
                        $output =  json_decode($json_data); 
                        if (json_last_error() === JSON_ERROR_NONE) 
                        {
                            // JSON is valid 
                            if( $output->status =="OK" )
                            {

                                if(    count(  $output->rows ) > 0 &&   
                                    count(  $output->rows[0]->elements ) > 0  &&  $output->rows[0]->elements[0]->status =="OK"   )
                                {
                                     

                                    $distance_str =  $output->rows[0]->elements[0]->distance->text   ; 
                                    $distance =  explode(" ", $distance_str); 
                                    $travel_duration_sec   =  $output->rows[0]->elements[0]->duration->value   ; 
                                    $travel_duration_min   = floor($travel_duration_sec/60);
                                    
                                    
                                    if(count($distance) > 0  )
                                    { 
                                        $item->distance  = number_format(  $distance[0] , 2 , '.', '')  ;
                                        
                                        if( $distance[0] <= 6.25 && $travel_duration_min <= 2000  ) 
                                        {
                                            $item->latitude  =  number_format( $item->latitude , 8 , '.', '')   ; 
                                            $item->longitude  =  number_format( $item->longitude , 8 , '.', '')  ;  
                                            $nearby_business_found[] =  array("bin" => $item->bin, "distance" => $item->distance );
                                            $nearby_business[] =$item ;  
                                        }
                                    }
                                }    
                            }  
                        } 
                        //saving  scanned bins 
                        $scanned_bins[] = $item->bin;
                        $scanbin = new ScannedAddress ;
                        $scanbin->bin =$item->bin; 
                        $scanbin->address_id = $request->addressId;
                        $scanbin->save();  
                    } //end of for loop 


                    $cached_list = AppCacheModel::where("address_id", $request->addressId )
                    ->where("profile_id",$member_id )
                    ->where("biz_category", $request->category )
                    ->first();

                    if( !isset($cached_list))
                    {
                        $cached_list  = new AppCacheModel;
                    } 

                    $cached_list->profile_id = $member_id; 
                    $cached_list->address_id = $request->addressId;
                    $cached_list->biz_category = $request->category;
                    $cached_list->cache_type = "nearby_biz";
                    $cached_list->cache_date = date('Y-m-d H:i:s');
                    $cached_list->cache_values =  json_encode( array("business" => $nearby_business_found ));
                    $cached_list->save();
                } 
            }
            else  //guest users searching business by pin code 
            {
                $member_id =  0 ; 
                $cached_list  =  AppCacheModel::where("cache_type", "nearby_biz")
                ->where("profile_id",  $member_id  )
                ->where("pin_code",  $request->pinCode )
                ->where("biz_category",  $request->category )
                ->first(); 

                $ids = array();
                if(isset($cached_list))
                {
                    $items = json_decode( $cached_list->cache_values   );
                    foreach( $items->business as $bitem )
                    {
                        $ids[] = $bitem->bin;
                    }   
                } 
               
                
                if(count( $ids )  > 0 )
                {
                    //show from scanned list 
                    $businesses = DB::table("ba_business")
                    ->join("ba_business_category", "ba_business_category.name", "=", "ba_business.category")  
                    ->select( 'ba_business.id as bin',  "ba_business.name", "ba_business.tag_line as tagLine", 
                        'ba_business.shop_number as shopNumber',  
                      'ba_business.phone_pri as primaryPhone', 'ba_business.phone_alt as alternatePhone', 
                      "ba_business.locality", "ba_business.landmark", "ba_business.city", "ba_business.state", "ba_business.pin", 
                      'ba_business.tags','ba_business.rating','ba_business.latitude','ba_business.longitude' , 
                      "ba_business.profile_image as profileImgUrl",   "ba_business.banner as bannerImgUrl" ,
                      DB::Raw("'na' promoImgUrl"),DB::Raw("'0' favourite"), 
                      "ba_business.category as businessCategory",  DB::Raw("'0' distance") ,
                      "ba_business_category.main_type as mainBusinessType",  
                       "is_open as isOpen" )
                    ->where("category",  $category )
                    ->whereIn("ba_business.id", $ids ) 
                    ->paginate(10); 
 
                    foreach($businesses  as $item)
                    {
                        $nearby_business[] =$item ;
                    } 
                    
                }
                else 
                {
                    //no scanned list found. Recalculated and update
                    $selected_address  = DB::table("ba_addresses")  
                    ->where("pin_code", $request->pinCode  ) 
                    ->select('city' )
                    ->first();
                    
                    if( !isset($selected_address))
                    {
                        $data = array("message" => "failure", "status_code" =>  9018 ,  
                        'detailed_msg' => 'No nearby business found. Please retry after changing or updating your address.'  ); 
                        return json_encode( $data); 
                    }
                    
                    $city_name = $selected_address->city;
                    $source =  $request->pinCode ; 

                    //city found   

                    $businesses = DB::table("ba_business")
                    ->join("ba_business_category", "ba_business_category.name", "=", "ba_business.category")  
                    ->select( 'ba_business.id as bin',  "ba_business.name", "ba_business.tag_line as tagLine", 
                        'ba_business.shop_number as shopNumber',  
                      'ba_business.phone_pri as primaryPhone', 'ba_business.phone_alt as alternatePhone', 
                      "ba_business.locality", "ba_business.landmark", "ba_business.city", "ba_business.state", "ba_business.pin", 
                      'ba_business.tags','ba_business.rating','ba_business.latitude','ba_business.longitude' , 
                      "ba_business.profile_image as profileImgUrl",   "ba_business.banner as bannerImgUrl" ,
                      DB::Raw("'na' promoImgUrl"),DB::Raw("'0' favourite"), 
                      "ba_business.category as businessCategory",  DB::Raw("'0' distance") ,
                      "ba_business_category.main_type as mainBusinessType",  
                       "is_open as isOpen" )
                    ->where("category", $category  )
                    ->whereRaw("city like '%" . $city_name . "%'" )
                    ->where("latitude", "<>", 0)
                    ->where("longitude", "<>", 0)  
                    ->paginate(10); 

 

                    foreach($businesses  as $item)
                    {

                        $target_lat = $item->latitude;  
                        $target_long = $item->longitude;   
                        $target =  $target_lat . "," . $target_long ;    

                        $url = 'https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=' .$source . '&destinations=' . $target . '&key=' . $key;
 

                         
                        $json_data = file_get_contents($url ); // get json content  
                        $output =  json_decode($json_data); 
                        if (json_last_error() === JSON_ERROR_NONE) 
                        {
                            // JSON is valid 
                            if( $output->status =="OK" )
                            {
                                if(  count(  $output->rows ) > 0 && count(  $output->rows[0]->elements ) > 0  &&  $output->rows[0]->elements[0]->status =="OK"   )
                                {
                                    
                                    $distance_str =  $output->rows[0]->elements[0]->distance->text   ; 
                                    $distance =  explode(" ", $distance_str); 
                                    $travel_duration_sec   =  $output->rows[0]->elements[0]->duration->value   ; 
                                    $travel_duration_min   = floor($travel_duration_sec/60);
                                    
                                    
                                    if(count($distance) > 0  )
                                    { 
                                        $item->distance  = number_format(  $distance[0] , 2 , '.', '')  ;
                                        
                                        if( $distance[0] <= 6.25 && $travel_duration_min <= 2000  ) 
                                        {
                                            $item->latitude  =  number_format( $item->latitude , 8 , '.', '')   ; 
                                            $item->longitude  =  number_format( $item->longitude , 8 , '.', '')  ;  
                                            $nearby_business_found[] =  array("bin" => $item->bin, "distance" => $item->distance );
                                            $nearby_business[] =$item ;  
                                        }
                                    }
                                    
                                }  
                                
                            } 
                            
                        }

                         //saving  scanned bins 
                        $scanned_bins[] = $item->bin;
                        $scanbin = new ScannedPin ;
                        $scanbin->bin =$item->bin; 
                        $scanbin->pin_code  = $request->pinCode ;
                        $scanbin->save(); 
 
                    } //end of forloop
 
                    //preparing for future save  
                    $cached_list = AppCacheModel::where("pin_code", $request->pinCode )
                    ->where("profile_id",$member_id )
                    ->where("biz_category",  $request->category )
                    ->first();

                    if( !isset($cached_list))
                    {
                        $cached_list  = new AppCacheModel;
                    }  

                    $cached_list->profile_id = $member_id; 
                    $cached_list->pin_code = $request->pinCode ; 
                    $cached_list->cache_type = "nearby_biz";
                    $cached_list->biz_category = $request->category;
                    $cached_list->cache_values =  json_encode( array("business" => $nearby_business_found ));
                    $cached_list->cache_date = date('Y-m-d H:i:s');
                    $cached_list->save();  

                }
                 
            }

            //image formatting 
            foreach( $nearby_business  as $item)
            {
                if($item->profileImgUrl!=null)
                {
                    $item->profileImgUrl = URL::to('/public'.$item->profileImgUrl); 
                }
                else
                {
                    $item->profileImgUrl = URL::to('/public/assets/image/no-image.jpg')  ;   
                }


                if($item->promoImgUrl!='na')
                {
                    $item->promoImgUrl = URL::to('/public'.$item->promoImgUrl); 
                }
                else
                {
                    $item->promoImgUrl = URL::to('/public/assets/image/no-image.jpg')  ;     
                }
                
                //banner url 
                if($item->bannerImgUrl != null )
                {
                    $source = public_path() .  $item->bannerImgUrl;
                    $pathinfo = pathinfo( $source  );
                    $new_file_name  =  $pathinfo['filename'].  "_sm" . '.' . $pathinfo['extension'];
                    $destination  = base_path() .  "/public/assets/image/business/bin_" . $item->bin . "/"   . $new_file_name ; 
                    
                    if(file_exists(  $source ) )
                    {
                        $info = getimagesize($source ); 
                        if ($info['mime'] == 'image/jpeg')
                            $image = imagecreatefromjpeg($source);
                        elseif ($info['mime'] == 'image/gif')
                            $image = imagecreatefromgif($source);
                        elseif ($info['mime'] == 'image/png')
                            $image = imagecreatefrompng($source);

                        $original_w = $info[0];
                        $original_h = $info[1];
                        $thumb_w = 290; // ceil( $original_w / 2);
                        $thumb_h = 200; //ceil( $original_h / 2 );

                        $thumb_img = imagecreatetruecolor($original_w, $original_h);
                        imagecopyresampled($thumb_img, $image,
                                0, 0,
                                0, 0,
                                $original_w, $original_h,
                                $original_w, $original_h ); 
                        imagejpeg($thumb_img, $destination, 20);


                    }
                    $item->bannerImgUrl =  URL::to("/public/assets/image/business/bin_" . $item->bin . "/"   . $new_file_name );     

                }
                else
                {
                    $item->bannerImgUrl = URL::to('/public/assets/image/no-image.jpg')  ;    
                }

                //banner url  
                $item->favourite =  0;
                $item->latitude  =  number_format( $item->latitude ,6 , '.', '')   ; 
                $item->longitude  =  number_format( $item->longitude , 6 , '.', '')  ; 
                $item->distance  =  number_format( $item->distance ,6 , '.', '') ; 

            }

            $final_result['last_page'] = $businesses->lastPage( ); 
            $final_result['data'] = $nearby_business ;   
            $result = array( 'results' => $final_result) ;
            return json_encode($result); 

        }

}
