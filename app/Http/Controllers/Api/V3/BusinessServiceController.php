<?php

namespace App\Http\Controllers\Api\V3; 

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Crypt;  
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\File;
use Illuminate\Contracts\Encryption\DecryptException;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel; 


 
use App\Business;  
use App\ProductModel;
use App\ShoppingOrderModel;
use App\ShoppingOrderItemsModel;
use App\CustomerProfileModel;
use App\DeliveryOrderModel;
use App\ServiceBooking;
use App\OrderStatusLogModel;
use App\ProductUnitModel;
use App\ProductFoodModel;
use App\ProductClothingModel;
use App\ProductSpecsModel;


use App\Traits\Utilities;
use App\User;


use App\Jobs\SendSellerOrderProcessingSms; 



class BusinessServiceController extends Controller
{
	  use Utilities;

     protected function foodAddProduct(Request $request)
     {
        $user_info =  json_decode($this->getUserInfo($request->userId));
        if(  $user_info->user_id == "na" )
        {
          $data= ["message" => "failure", "status_code" =>  901 , 'detailed_msg' => 'User has no priviledge to access this feature! Please consult admin.'  ] ;
          return json_encode($data);
        }


        if( $request->productName == "" ||   $request->maxOrderQty == "" || $request->mfrId == "" || $request->description == "" ||  
          $request->initialStock == "" ||  $request->stockInHand  == "" ||  $request->minimumStock  == "" ||  $request->categoryName== "" ||  
          $request->unitPrice == "" || $request->unitValue == "" ||  $request->unitName == "" || $request->actualPrice == "" ||  $request->discountPc == ""  )
        {
            $data= ["message" => "failure", "status_code" =>  902 , 'detailed_msg' => 'Missing data to perform action.' ]; 
            return json_encode($data);
        }

        $pr_code_count  = ProductModel::where("bin", $request->bin )
        ->where("category", $request->categoryName ) 
        ->count() + 1; 

        $id =   DB::table('ba_products') 
        ->max('id'); 

        $prCode = "BPR" .  $request->bin . "" . ( $id + 1)  ;  


        $discount = ( $request->discountPc /100 ) * $request->unitPrice; 

        $product = new ProductModel;
        $product->bin = $request->bin;
        $product->pr_code =  $prCode  ;
        $product->pr_name = $request->productName;
        $product->gst_code = $request->gst;
        $product->mfr_id = $request->mfrId;
        $product->description = $request->description;
        $product->category = $request->categoryName; 
        $save=$product->save();  
        

        if($save)
        {
          $sub_details =  new ProductFoodModel; 
          $sub_details->packaging = ( $request->packagingCharge == "" ? 0.00 :  $request->packagingCharge)  ;
          $sub_details->food_type = $request->foodType ; 
          $sub_details->pack_type = ( $request->packageType== "" ? "PACK" : $request->packageType )  ;   
          $sub_details->pr_code = $prCode;
          $sub_details->bar_code  = ( $request->barCode == "" ? null : $request->barCode );
          $sub_details->initial_stock = $request->initialStock;
          $sub_details->stock_inhand = $request->stockInHand;
          $sub_details->min_required = $request->minimumStock; 
              $sub_details->max_order = $request->maxOrderQty;  
              $sub_details->unit_price = $request->unitPrice; 
              $sub_details->unit_name = $request->unitName ;
              $sub_details->unit_value = $request->unitValue ;
              $sub_details->actual_price = $request->unitPrice - $discount ;   //  $request->actualPrice ;
              
              if( $request->discountPc != "")
              {
                $sub_details->discountPc =   $request->discountPc ;
                $sub_details->actual_price = $request->unitPrice - ( ( $request->discountPc / 100 ) * $request->unitPrice );
              }

              if( $request->discount != "")
              {
                $sub_details->discount =   $request->discount ;
                $sub_details->actual_price = $request->unitPrice - $request->discount;
              } 

              $sub_details->sub_tags = $request->tags ;
              $sub_details->save();
              $data= ["message" => "success", "status_code" =>  5001 ,  'detailed_msg' => 'Product added successfully.',  'productId' => $product->id,  'productCode' => $prCode  ];
        }
        else
        {
          $data= ["message" => "failure", "status_code" =>  5002 ,  'detailed_msg' => 'Product could not be added.', 
          'productId' =>  0 ,  'productCode' => 'na'];
        }
      return json_encode( $data);


     }



     protected function clothingAddProduct(Request $request)
     {
        $user_info =  json_decode($this->getUserInfo($request->userId));
        if(  $user_info->user_id == "na" )
        {
          $data= ["message" => "failure", "status_code" =>  901 , 'detailed_msg' => 'User has no priviledge to access this feature! Please consult admin.'  ] ;
          return json_encode($data);
        }


        if( $request->productName == "" ||   $request->maxOrderQty == "" || $request->mfrId == "" || $request->description == "" ||  
          $request->initialStock == "" ||  $request->stockInHand  == "" ||  $request->minimumStock  == "" ||  $request->categoryName== "" ||  
          $request->unitPrice == "" || $request->unitValue == "" ||  $request->unitName == "" || $request->actualPrice == "" ||  $request->discountPc == ""  )
        {
            $data= ["message" => "failure", "status_code" =>  902 , 'detailed_msg' => 'Missing data to perform action.' ]; 
            return json_encode($data);
        }

        $pr_code_count  = ProductModel::where("bin", $request->bin )
        ->where("category", $request->categoryName ) 
        ->count() + 1; 

        $id =   DB::table('ba_products') 
        ->max('id'); 

        $prCode = "BPR" .  $request->bin . "" . ( $id + 1)  ;  


        $discount = ( $request->discountPc /100 ) * $request->unitPrice; 

        $product = new ProductModel;
        $product->bin = $request->bin;
        $product->pr_code =  $prCode  ;
        $product->pr_name = $request->productName;
        $product->gst_code = $request->gst;
        $product->mfr_id = $request->mfrId;
        $product->description = $request->description;
        $product->category = $request->categoryName; 
        $save=$product->save();  
        

        if($save)
        {

          $sub_details =  new ProductClothingModel; 
          $sub_details->size =  $request->size ; 
          $sub_details->color =  $request->color ;   
          $sub_details->pattern = $request->pattern; 

          $sub_details->pr_code = $prCode;
            $sub_details->bar_code  = ( $request->barCode == "" ? null : $request->barCode );
            $sub_details->initial_stock = $request->initialStock;
              $sub_details->stock_inhand = $request->stockInHand;
              $sub_details->min_required = $request->minimumStock; 
              $sub_details->max_order = $request->maxOrderQty;  
              $sub_details->unit_price = $request->unitPrice; 
              $sub_details->unit_name = $request->unitName ;
              $sub_details->unit_value = $request->unitValue ;
              $sub_details->actual_price = $request->unitPrice - $discount ;   //  $request->actualPrice ;
              
              if( $request->discountPc != "")
              {
                $sub_details->discountPc =   $request->discountPc ;
                $sub_details->actual_price = $request->unitPrice - ( ( $request->discountPc / 100 ) * $request->unitPrice );
              }

              if( $request->discount != "")
              {
                $sub_details->discount =   $request->discount ;
                $sub_details->actual_price = $request->unitPrice - $request->discount;
              } 

              $sub_details->sub_tags = $request->tags ;
              $sub_details->save();
              $data= ["message" => "success", "status_code" =>  5001 ,  'detailed_msg' => 'Product added successfully.',  'productId' => $product->id,  'productCode' => $prCode  ];
        }
        else
        {
          $data= ["message" => "failure", "status_code" =>  5002 ,  'detailed_msg' => 'Product could not be added.', 
          'productId' =>  0 ,  'productCode' => 'na'];
        }
      return json_encode( $data);


     }



    // API to add Product Specification
    //  
    // 
    protected function addProductSpecification(Request $request)
    {
        $user_info =  json_decode($this->getUserInfo($request->userId));
        if(  $user_info->user_id == "na" )
        {
          $data= ["message" => "failure", "status_code" =>  901 , 'detailed_msg' => 'User has no priviledge to access this feature! Please consult admin.'  ] ;
          return json_encode($data);
        } 



        if( $request->productCode  == "" ||   $request->subDetailsId == "" ||  $request->specifications == ""     )
        {
            $data= ["message" => "failure", "status_code" =>  902 , 'detailed_msg' => 'Missing data to perform action.' ]; 
            return json_encode($data);
        }


        if( $request->specifications == "[]" ) //blank arrray
        {
          $data= ["message" => "success", "status_code" =>  5011 ,  'detailed_msg' => 'Product specification not provided!'  ];
          return json_encode( $data);
        }


 
       $msg = "Specification could not be saved!"; 
       $err_code = 5002;
      $specJson = json_decode($request->specifications);  

      foreach ($specJson as $jsonRow) { 

        $specTitle  =  $specValue = $specDetails  ="";

        foreach ($jsonRow as $jsonkey => $jsonvalue) { 
 
              if($jsonkey == "specTitle")
              {
                 $specTitle  =  $jsonvalue; 
              }
              else  if($jsonkey == "specValue")
              {
                 $specValue  =  $jsonvalue; 
              }else  if($jsonkey == "specDetails")
              {
                 $specDetails  =  $jsonvalue; 
              }  
        }


        if( $specTitle   != ""    )
            {

              $specification = ProductSpecsModel::where("pr_code", $request->productCode )
              ->where("sub_id", $request->subDetailsId )
              ->where("spec_title", $specTitle )
              ->first();

              if( !isset($specification))
              {
                $specification = new ProductSpecsModel;
              }

              $specification->pr_code =$request->productCode;
              $specification->sub_id =  $request->subDetailsId;
              $specification->spec_title = $specTitle ;
              $specification->spec_value = $specValue  ;
              $specification->spec_details = $specDetails  ;
              $save=$specification->save();  

              if($save)
              {
                 $msg = 'Product specification successfully.'  ; 
                 $err_code = 5001;
              } 

            }


      }
    
      $data= ["message" => "success", "status_code" =>  $err_code ,  'detailed_msg' => $msg  ]; 
      return json_encode( $data);


     }





     //view product info by clothing items
     protected function viewProductDetails(Request $request)
     {
 

        $user_info =  json_decode($this->getUserInfo($request->userId));
        if(  $user_info->user_id == "na" )
        {
            $data= ["message" => "failure", "status_code" =>  901 , 
            'detailed_msg' => 'User has no priviledge to access this feature! Please consult admin.'  ] ;
            return json_encode($data);
        } 


         if ( $request->productId  == ""  || $request->bin   == ""  )
        {
         $data= ["message" => "failure", "status_code" =>  902 , 'detailed_msg' => 'Missing data to perform action.'  ];
         return json_encode($data);
        }
  

        $product_info  = DB::table("ba_products")
        ->where("id", $request->productId )
        ->where("bin", $request->bin  )
        ->select( "id as productId",  
            "bin",
            "pr_code as productCode", 
            "pr_name as productName",
            "category as categoryName", 
            "gst_code as GST", 
            "mfr_id as manufacturer",
            "description",
            "category",
            DB::Raw("'[]' sizeColorMap") ,  
            DB::Raw("'[]' subDetails")  )
        ->first() ;
 

 
        if( !isset( $product_info  ))
        {
          $data= ["message" => "failure", "status_code" =>  902 , 'detailed_msg' => 'No matching found found!'  ];
          return json_encode($data);
        }

        $product_subdetails  = DB::table("ba_product_clothing")
        ->where("pr_code", $product_info->productCode ) 
        ->select( "id as productSubId",   
            "bar_code as barCode",  
            "size",
            "color",
            "gender", 
            "pattern", 
            "initial_stock as initialStock",
            "min_required as minimumStock" ,  
            "stock_inhand as stock" ,  
            "max_order as maximumOrderAllowed", 
            "unit_price as unitPrice" , 
            "unit_value as unitValue",
            "unit_name as unitName",   
            "photos as images" ,    
            "actual_price as actualPrice",
            "discount", 
            "discountPc", 
            "pack_type as packageType" )
        ->get() ;
    
    $sizes =  array();

    foreach( $product_subdetails as $item )
    {
       $sizes[] =  $item->size;
    }
    $sizes = array_unique($sizes);


    $sizeColorMap  =array();
    $i=0;
    foreach( $sizes as $item )
    {
      $sizeColorMap[$i]['size'] = $item ;
      $color =  array();
      foreach( $product_subdetails as $subitem )
      {
        if($item == $subitem->size )
        {
          $color[] =  $subitem->color;
        } 
      }

      $sizeColorMap[$i]['color'] = $color ;
      $i++;
      
    }


    $product_info->sizeColorMap = $sizeColorMap;

    $all_photos = array();

        foreach( $product_subdetails as $item )
        {
           $files =  explode(",",  $item->images );
           $files =  array_filter($files  ); 

           if(count($files) > 0 )
           {
              $files=array_filter($files); 

              $folder_path =  $_ENV['APP_CDN_PATH'] . "/assets/image/store/bin_" .  $product_info->bin .    "/";

              foreach($files as $file )
              {
                if(file_exists( $folder_path . $file ))
                {
                  $all_photos[]  =  URL::to( $folder_path  ) . "/" .    $file  ;
                }
                else
                {
                  $all_photos[]   = $_ENV['APP_CDN']  . "/assets/image/no-image.jpg";
                }

              }
 
           }
           else 
           { 

              $all_photos[]   =  $_ENV['APP_CDN']  . "/assets/image/no-image.jpg";
           }

           $item->images = $all_photos; 
        }


        $product_info->subDetails  = $product_subdetails ; 
     
        $data= ["message" => "success", "status_code" =>  5011 ,  'detailed_msg' => 'Product details fetched!' , 
        'results' => $product_info];

        return json_encode( $data);
     }




}
