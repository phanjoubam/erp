<?php

namespace App\Http\Controllers\Api\V3; 

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Crypt;  
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\File; 
use Illuminate\Contracts\Encryption\DecryptException;
use App\Http\Controllers\Controller;

use App\Jobs\SendOrderConfirmationSms;
use App\Jobs\SendOrderAssignToAgentSms;
 

use App\ShoppingOrderItemsModel;  
use App\ShoppingCartTemporary;  
use App\AgentServiceAreaModel; 
use App\CustomerProfileModel;
use App\DeliveryOrderModel;
use App\ServiceBooking;
use App\OrderStatusLogModel; 
use App\ProductModel; 
use App\Business;
use App\Traits\Utilities;
use App\User; 
use App\MarketingCallLog;
use App\CallbackEnquiryModel;
use App\MemberWishlistModel;
use App\SponsoredPackageModel;
use App\ServiceProductModel;
use App\LandingScreenSectionModel;
use App\PickAndDropRequestModel;
use App\PndassignModel;
use App\AgentattendModel;

class WebControllerV3 extends Controller
{
	use Utilities;

     protected function addItemToCart(Request $request)
     {
      if(  $request->prid == "" ||  $request->bin  == ""  || $request->qty == ""  )
      {
       $data= ["message" => "failure", "status_code" =>  902 , 'detailed_msg' => 'Missing data to perform action.'  ];
       return json_encode($data);
      }
 
      $uid = $sid = 0 ;
      try
      {
         $uid =  Crypt::decrypt( $request->guid );
         $sid =  Crypt::decrypt( $request->gsid );
      }
      catch (DecryptException $e)
      {
        $data= ["message" => "failure", "status_code" =>  999 , 'detailed_msg' =>   'Invalid access!'  ];
        return json_encode($data);
      }
	  
	    $cart_count = 0;
	  
      if($uid == 0)
      {

        $cart_item = ShoppingCartTemporary::where( "bin",  $request->bin )
        ->where( "prid",  $request->prid )
        ->where( "session_id",  $sid )
        ->first(); 

        if( !isset(  $cart_item )  )
          {
            $cart_item = new ShoppingCartTemporary;
            $cart_item->session_id =  $sid  ; 
            $cart_item->prid = $request->prid;
            $cart_item->subid = $request->subid;
            $cart_item->bin = $request->bin;
          }
		  
		//cart item count 
		$cart_count = ShoppingCartTemporary::where( "session_id",  $sid ) 
        ->count();
		

      }
      else 
      {
          $user_info =  json_decode ( $this->getUserInfo( $uid  ) );  
          if(  $user_info->user_id == "na" )
          {
              $data= ["message" => "failure", "status_code" =>  901 ,  'detailed_msg' => 'User has no priviledge to access this feature! Please consult admin.'  ] ;
              return json_encode($data);
          } 

          if( $user_info->category  != 0  )
          {
            $data= ["message" => "failure", "status_code" =>  901 , 'detailed_msg' => 'This feature is available to customer only.'  ];
            return json_encode($data);
          }


          $cart_item = ShoppingCartTemporary::where( "bin",  $request->bin )
          ->where( "prid",  $request->prid )
          ->where( "member_id",  $user_info->member_id )
          ->first();

          if( !isset(  $cart_item )  )
          {
            $cart_item = new ShoppingCartTemporary;
            $cart_item->member_id = $user_info->member_id ;
            $cart_item->session_id =  $sid  ;
            $cart_item->prid = $request->prid;
            $cart_item->subid = $request->subid;
            $cart_item->bin = $request->bin;
          }
		  
		//cart item count 
		$cart_count = ShoppingCartTemporary::where( "member_id",  $user_info->member_id ) 
        ->count();

      }
 
		
		
		if( $request->action == "" || $request->action != "add")
        {
          if($request->qty <= 0)
		  {
			  //remove item
			  $cart_item->delete(); 
			  $data= ["message" => "success", "status_code" =>  6001 ,   'detailed_msg' => 'Item added to cart.'  ];
			   return json_encode( $data); 
		  }
		  else 
		  {
			  $cart_item->qty = $request->qty; 
		  }
        }
		else  
			if($request->action == "add")
			{
			  $cart_item->qty += $request->qty; 
			}
			
			
        
        //calculating cost
        $product = DB::table("ba_products")
        ->join("ba_product_variant","ba_products.id","=","ba_product_variant.prid") 
        ->where("id", $request->prid ) 
        ->select("ba_products.id","bin", "ba_product_variant.actual_price", "pr_name", DB::Raw("'' photos"))
        ->first();

        $cdn_url =   config('app.app_cdn') ;  
        $cdn_path =  config('app.app_cdn_path'); // cnd - /var/www/html/api/public 
        $cms_base_url = config('app.app_cdn')  ;  
        $cms_base_path = $cdn_path; 

        if ($product->photos=="") {
            $product->photos =   $cdn_url .  "/assets/image/no-image.jpg";
         }

        // dd($product);
        $product_photos = DB::table("ba_product_photos")
        ->where("prid",$product->id)
        ->first(); 

		    $product->photos = $product_photos->image_url; 
		     
        if(isset($product))
        {
				  $cart_item->cost  =  $cart_item->qty * $product->actual_price; 
		    } 
 
        $save = $cart_item->save(); 
	 
	
        if($save)
        {
          $data= ["message" => "success", "status_code" =>  6001 , 
		  'detailed_msg' => 'Item added to cart.',  'product'=> $product, 'cart_count' => $cart_count ];
        }
        else
        {
        	$data= ["message" => "failure", "status_code" =>  6002 , 
			'detailed_msg' =>  "Item could not be added to cart." , 'cart_count' => 0 ];
        } 
 
        return json_encode( $data); 
     } 

	
	
	protected function addWishlistProducts(Request $request)
    {
		$save = null;
		if(  $request->prid == ""    )
		  {
		   $data= ["message" => "failure", "status_code" =>  902 , 'detailed_msg' => 'Missing data to perform action.'  ];
		   return json_encode($data);
		  }
	 
		  $uid = $sid = 0 ;
		  try
		  {
			 $uid =  Crypt::decrypt( $request->guid );
			 $sid =  Crypt::decrypt( $request->gsid );
		  }
		  catch (DecryptException $e)
		  {
			$data= ["message" => "failure", "status_code" =>  999 , 'detailed_msg' =>   'Invalid access!'  ];
			return json_encode($data);
		  }
		  
		  $cart_count = 0;
	  
      if($uid == 0)
      {

        $wishlist = MemberWishlistModel::where( "prid",  $request->prid )
        ->where( "session_id",  $sid )
        ->first(); 

        if( !isset(  $wishlist )  )
        {
			$wishlist = new MemberWishlistModel; 
			$wishlist->prid = $request->prid;
            $wishlist->session_id =  $sid  ; 
			$wishlist->save();
			$data= ["message" => "success", "status_code" =>  6001 ,  'detailed_msg' => 'Item added to wishlist.'  ];
		  
        }
		else 
		{
			   
        	$data= ["message" => "failure", "status_code" =>  6002 , 
			'detailed_msg' =>  "Item could not be added to wishlist."   ];
         
		  }
          
		
      }
      else 
      {
          $user_info =  json_decode ( $this->getUserInfo( $uid  ) );  
          if(  $user_info->user_id == "na" )
          {
              $data= ["message" => "failure", "status_code" =>  901 ,  'detailed_msg' => 'User has no priviledge to access this feature! Please consult admin.'  ] ;
              return json_encode($data);
          } 

          if( $user_info->category  != 0  )
          {
            $data= ["message" => "failure", "status_code" =>  901 , 'detailed_msg' => 'This feature is available to customer only.'  ];
            return json_encode($data);
          }


          $cart_item = MemberWishlistModel::where( "member_id",  $user_info->member_id )
		  ->where( "prid",  $request->prid )
          ->first();

          if( !isset(  $cart_item )  )
          {
            $wishlist = new MemberWishlistModel; 
			$wishlist->prid = $request->prid;
            $wishlist->member_id = $user_info->member_id;
			$wishlist->save();
			$data= ["message" => "success", "status_code" =>  6001 ,  'detailed_msg' => 'Item added to wishlist.'  ];
		  
          }
		  else 
		  {
			   
        	$data= ["message" => "failure", "status_code" =>  6002 , 
			'detailed_msg' =>  "Item could not be added to wishlist."   ];
         
		  }

		}
	   
	  
 
        return json_encode( $data);  
 
    }
	
	//get all carts
	protected function getActiveCarts(Request $request)
    {
		
		$uid = $sid = 0 ;
		try
		{
			$uid =  Crypt::decrypt( $request->guid );
			$sid =  Crypt::decrypt( $request->gsid );
		}
		catch (DecryptException $e)
		{
			$data= ["message" => "failure", "status_code" =>  999 , 'detailed_msg' =>   'Invalid access!'  ];
			return json_encode($data);
		}
		
		if($uid == 0)
		{

			$cart_item = ShoppingCartTemporary::where( "bin",  $request->bin )
			->where( "prid",  $request->prid )
			->where( "session_id",  $sid )
			->first(); 

			if( !isset(  $cart_item )  )
			  {
				$cart_item = new ShoppingCartTemporary;
				$cart_item->session_id =  $sid  ; 
				$cart_item->prid = $request->prid;
				$cart_item->subid = $request->subid;
				$cart_item->bin = $request->bin;
			  } 

		}
		
	   


        $gsid = $request->session()->get('shopping_session'   );    
        $uid = $request->session()->get('__user_id_'   );  

        $member = DB::table("ba_profile")
        ->join("ba_users", "ba_users.profile_id", "=", "ba_profile.id") 
        ->where("ba_users.id",  $uid  ) 
        ->select("ba_profile.*")
        ->first();

        if(!isset($member ))
        {
            return redirect("/");
        }
		
		
		$cart  = DB::table("ba_temp_cart")   
        ->join("ba_products", "ba_products.id", "=", "ba_temp_cart.prid") 
        ->where("member_id",  $member->id   ) 
        ->select( "ba_products.*", "ba_temp_cart.session_id", 
            "ba_temp_cart.member_id" , "ba_temp_cart.bin" , 
            "ba_temp_cart.prid", "ba_temp_cart.subid" , "ba_temp_cart.qty as pqty" )
        ->get();



        $cdn_url =   config('app.app_cdn') ;  
        $cdn_path =  config('app.app_cdn_path'); // cnd - /var/www/html/api/public 
        $cms_base_url = config('app.app_cdn')  ;  
        $cms_base_path = $cdn_path  ;  
        $no_image  =   $cdn_url .  "/assets/image/no-image.jpg";  
 

        foreach( $cart as $item)
        { 
            $thumbnail = $no_image ; 
            $files =  explode(",",  $item->photos );   
            if(count($files) > 0 )
            { 
                $product_folder_path =  $cms_base_path .  "/assets/image/store/bin_" .   $item->bin .  "/";
                $product_folder_url = $cms_base_url . "/assets/image/store/bin_" .   $item->bin .  "/";  
                $files=array_filter($files); 
                foreach($files as $file )
                {
                    $source = $product_folder_path .  $file;   
                    if(file_exists( $source  ))
                    {
                        $pathinfo = pathinfo( $source  );
                        $target_file_name  =  $pathinfo['filename'].  "_sm" . '.' . $pathinfo['extension']; 
                        $target_file_path   = $cms_base_path .  "/assets/image/store/bin_" . $item->bin  .  "/"  . $target_file_name ; 
                        if( file_exists( $target_file_path) )  
                        { 
                           $thumbnail  =  $product_folder_url . $target_file_name; 
                        }
                        else 
                        { 
                           $thumbnail = $product_folder_url .   $file;
                        } 

                        break;
                    }  
                } 

                $item->photos = $thumbnail;
            }
            else 
            {
                $item->photos = $no_image ;
            }

         }

 

        $bins = array(); 
        $bins[] = 0;
        foreach($cart as $item)
        {
            $bins[] = $item->bin;
        } 
         

        $bins= array_filter($bins);

        $businesses  = DB::table("ba_business")
        ->whereIn("id", $bins )
        ->get();
   

    
        $data = array(  'businesses' =>$businesses , 'cart' => $cart   );
        return view("store_front.checkout")->with(  $data );
    }
	
	
	
	

     protected function toggleBusinessBlock(Request $request)
    {
        if(  $request->bin  == "" )
        {
         $data= ["message" => "failure", "status_code" =>  902 , 'detailed_msg' => 'No order is selected.'  ];
         return json_encode($data);
        } 

        $business = Business::find($request->bin); 

        if(   !isset( $business )  )
        {
         $data= ["message" => "failure", "status_code" =>  999 , 'detailed_msg' => 'No matching business found.'  ];
         return json_encode($data);
        }
  
        if( $request->status  == "true" )
        {
          $business->is_block = "yes";   
        }
        else 
        {
          $business->is_block = "no";   
        } 
        $business->save();  

        $data= ["message" => "success", "status_code" =>  7005 ,  'detailed_msg' =>  "Business status is updated."   ]; 
        return json_encode( $data);  
    }

    protected function toggleBusinessOpen(Request $request)
    {
        if(  $request->bin  == "" )
        {
         $data= ["message" => "failure", "status_code" =>  902 , 'detailed_msg' => 'No order is selected.'  ];
         return json_encode($data);
        } 

        $business = Business::find($request->bin); 

        if(   !isset( $business )  )
        {
         $data= ["message" => "failure", "status_code" =>  999 , 'detailed_msg' => 'No matching business found.'  ];
         return json_encode($data);
        }
  
        if( $request->status  == "true" )
        {
          $business->is_open = "open";   
        }
        else 
        {
          $business->is_open = "close";   
        } 
        $business->save();  

        $data= ["message" => "success", "status_code" =>  7005 ,  'detailed_msg' =>  "Business status is updated."   ]; 
        return json_encode( $data);  
    }
	
	 
	
	protected function saveCallLog(Request $request)
    {
		if(  $request->key  == "" || $request->logdate  == "" || $request->logsummary  == "" )
        {
         $data= ["message" => "failure", "status_code" =>  999 , 'detailed_msg' => 'Missing data to perform action.'  ];
         return json_encode($data);
        }
		
		
		$log = new MarketingCallLog;
		$log->member_id = $request->key;
		$log->log_date = date('Y-m-d H:i:s', strtotime($request->logdate) );
		$log->call_description = $request->logsummary;
		$log->save();
		$data= ["message" => "success", "status_code" =>  8301 ,  'detailed_msg' =>  "Call log saved."   ]; 
        return json_encode( $data);  
	  
		
	}
	
	
	
	protected function scanForNewOrders(Request $request)
    {
		if(  $request->lastScanId == ""  )
        {
			$data= ["message" => "failure", "status_code" =>  999 , 'detailed_msg' => 'Missing data to perform action.'  ];
			return json_encode($data);
        }
		
		$today = date('Y-m-d');
		$last_id =  $request->lastScanId ;
		 
		$orders = DB::table("ba_order_sequencer")
		->where( 'id',  '>',  $last_id  )
		->where( 'cc_notified',     'no')
		->whereDate('entry_date', $today)
		->orderBy("id", "desc")
		->first();
		
		if(isset($orders))
		{
			$last_id = $orders->id;
			
			DB::table("ba_order_sequencer")
			->where( 'id',   $last_id  )
			->update( array('cc_notified' => 'yes' )); 
		}
		 
		$data= ["message" => "success", "status_code" =>  8311 ,  'detailed_msg' =>  "There is new order", 'lastId' => $last_id  ]; 
        return json_encode( $data);   
		
	}
	
	
	
	protected function generateCallbackOTP(Request $request)
    {
		if( $request->phone == ""  )
       {
		   $err_msg = "Phone number not provided!";
          $err_code = 9000;
              
        $data = array(
          "message" => "failure" , 'status_code' => $err_code ,
          'detailed_msg' => $err_msg) ;
        
        return json_encode( $data ); 
      }
	  
	  $phone = $request->phone  ;
	  $rand = mt_rand(111111, 999999);
	  
	  
	  /*
	  
		  $callback =  CallbackEnquiryModel::find("phone",  $phone)
		  ->whereDate("enquiry_date", date('Y-m-d')) 
		  ->first();
		  
		  if(  isset($callback))
		  { 
			  
			  return redirect('/shopping/make-order-enquiry')->with("err_msg", "Please regenerate OTP and try again!");
		  }
	  */



      //saving profile
      $callback = new CallbackEnquiryModel;
      $callback->phone  = $request->phone ; 
      $callback->otp  =  $rand; 
	  $callback->enquiry_date  =  date('Y-m-d');
      $callback->save();  


      //generate a confirmation OTP 
      $gap_time = strtotime( date('Y-m-d H:i:s') ) +(60*10) ;
      
        
      //sending sms  
      $err_msg =  "OTP send to verified OTP.";
	  
	  $message_row = DB::table("ba_sms_templates")
	  ->where("name", "otp_generate")
	  ->first();
	  
	  if(isset($message_row))
	  {
		  $otp_msg =  urlencode( str_replace(  "###", $rand,  $message_row->message ) );
		  $this->sendSmsFromTemplate(  $message_row->dlt_entity_id,  $message_row->dlt_header_id , $message_row->dlt_template_id ,   array($phone)   ,  $otp_msg  ); 
	  } 
		
		

      $err_status ="success";
      $err_code =  3503;
      $data = array( "message" => $err_status  , "status_code"=>$err_code , 'detailed_msg' => $err_msg) ;
      return json_encode($data);
	  
	
	}
	
	
	
	
	protected function updateEnquirySpamStatus(Request $request)
    {
		if( $request->key == "" || $request->status == "" )
       {
		   $err_msg = "Field missing!";
          $err_code = 9000;
              
        $data = array(
          "message" => "failure" , 'status_code' => $err_code ,
          'detailed_msg' => $err_msg) ;
        
        return json_encode( $data ); 
      }
	  
	   $callback =  CallbackEnquiryModel::find( $request->key  ) ;

            if( !isset($callback))
            {
               $err_status ="failure";
      $err_code =  3508;
      $data = array( "message" => $err_status  , "status_code"=>$err_code , 'detailed_msg' => "No enquiry log found.") ;
            }

$callback->is_spam = $request->status ;
$callback->save();

$err_msg =  "Enquiry spam status updated.";
      $err_status ="success";
      $err_code =  3503;
      $data = array( "message" => $err_status  , "status_code"=>$err_code , 'detailed_msg' => $err_msg) ;
      return json_encode($data);
	  
	
	}
	
	
	
	
	protected function addCCRemarks(Request $request)
    {
		
		if( $request->oid == "" || $request->ccremark == "" )
        {
		   $err_msg = "Field missing!";
		   $err_code = 9000;
		   $data = array( "message" => "failure" , 'status_code' => $err_code , 'detailed_msg' => $err_msg) ;
		   return json_encode( $data ); 
        }
		
		
		$order_info = ServiceBooking::find($request->oid);  
        if(   !isset( $order_info)  )
        {
         $data= ["message" => "failure", "status_code" =>  999 , 'detailed_msg' => 'No matching order found.'  ];
         return json_encode($data);
        }
		
        $order_info->cc_remarks = $request->ccremark;
        $order_info->save();
		
		$err_msg =  "CC remarks updated.";
		  $err_status ="success";
		  $err_code =  5501;
		  $data = array( "message" => $err_status  , "status_code"=>$err_code , 'detailed_msg' => $err_msg) ;
		  return json_encode($data);
	  
	
	}
	
	
	
	
	protected function getBusinessInfo(Request $request)
    {
	 
		
		if( $request->bin == ""  )
        {
		   $err_msg = "Field missing!";
		   $err_code = 9000;
		   $data = array( "message" => "failure" , 'status_code' => $err_code , 'detailed_msg' => $err_msg) ;
		   return json_encode( $data ); 
        }
		
		
		$business = DB::table("ba_business")
		->where("id", $request->bin)
		->select("id as bin", "name",  "phone_pri as phone", 
		"locality", "landmark", "city", "state", "pin",  "category",	
		"latitude", "longitude"  )
		->first() ;  
        if(   !isset( $business)  )
        {
         $data= ["message" => "failure", "status_code" =>  999 , 'detailed_msg' => 'No matching business found.'  ];
         return json_encode($data);
        }
		 
		
		$err_msg =  "Business information fetched.";
		$err_status ="success";
		$err_code =  5507;
		$data = array( "message" => $err_status  , "status_code"=>$err_code , 'detailed_msg' => $err_msg ,  'result' =>$business) ;
		
		
		return json_encode($data);
	  
	
	}


  //get service calender
  protected function getDaySlots(Request $request)
    {
          $uid = 0 ;
          try
          {
             $uid =  Crypt::decrypt( $request->guid ); 
          }
          catch (DecryptException $e)
          {
            $data= ["message" => "failure", "status_code" =>  999 , 'detailed_msg' =>   'Invalid access!'  ];
            return json_encode($data);
          } 
        
        $user_info =  json_decode($this->getUserInfo($uid));

        if($user_info->user_id == "na" )
        {
          $data= ["message" => "failure", "status_code" =>  901 , 'detailed_msg' => 'User has no priviledge to access this feature! Please consult admin.'  ] ;
          return json_encode($data);
        }
    
        if($request->bin == ""   || $request->dayName == "")
        {
         $data= ["message" => "failure", "status_code" =>  902 , 'detailed_msg' => 'Missing data to perform action.'  ];
         return json_encode($data);
        }
    
        if( $request->dayName != "" )
        {
          $dayName = $request->dayName;
        }
    
        $date = date('Y-m-d', strtotime($request->date));
        $service_slots = DB::table("ba_services_duration") 
        ->where("bin", $request->bin)
        ->where("day_name",  $dayName )
        ->orderBy("start_time", "asc")
        ->select("slot_number as slotNo", "day_name as dayName", "start_time as startTime", "end_time as endTime","status as currentStatus","bin",DB::Raw("'' slot_duration"))
        ->get();
  
        if (count($service_slots)>0) {

           $msg = "success";
           $code = 7007;
           $data= ['results' => $service_slots,'selectedDate'=>$date];
           $returnHTML = view('store_front.booking.day_slots')->with($data)->render(); 
          
        }else{

           $msg = "Time slots not available! for the specific business";
           $code = 7777;
           $returnHTML="";
        } 
        

        return response()->json(array("message" => $msg, "status_code" => $code,'html'=>$returnHTML));
        //return json_encode( $data); 

     }



    //get staff availability
  public function getStaffAvailability(Request $request)
   {
      $uid = 0 ;
          try
          {
             $uid =  Crypt::decrypt( $request->guid ); 
          }
          catch (DecryptException $e)
          {
            $data= ["message" => "failure", "status_code" =>  999 , 'detailed_msg' =>   'Invalid access!'  ];
            return json_encode($data);
          } 
        
      $user_info =  json_decode($this->getUserInfo($uid));
      if($user_info->user_id == "na" )
      {
        $data= ["message" => "failure", "status_code" =>  901 ,
        'detailed_msg' => 'User has no priviledge to access this feature! Please signup to use our services.'  ];
        return json_encode($data);
      }
      if($request->bin == ""  )
      {
        $data= ["message" => "failure", "status_code" =>  999 ,
            'detailed_msg' => 'Business information not found!'  ];
            return json_encode( $data);
      }
    
    $date = $request->date == "" ?  date('Y-m-d') :  date('Y-m-d', strtotime( $request->date ) )  ;
    $bin = $request->bin;
    $time = $request->time;
    $endtime = $request->endtime;
    
    $staffs = DB::table("ba_users")  
    ->select("profile_id"  )
    ->where("bin", $request->bin)
    ->whereIn("category", array("1", '10' ) )
    ->get();
  
    
    $staffid = array();  
    foreach( $staffs as $item)
    {
      $staffid[] = $item->profile_id;
    }
    $staffid[] = -1;
    
    $staff_available = DB::table("ba_profile") 
    ->whereNotIn("id",  function($query) use ($date, $bin )
    {
      $query->select("staff_id") 
      ->from('ba_staff_day_off')
      ->where("bin",  $bin ) 
      ->whereDate("off_date",  $date ) ;  
    })
    ->select("id as staffId", "fname as staffName", "profile_photo as profilePicture" ) 
    ->whereIn("id",  $staffid )
    ->get();
    
    $day_offs = DB::table("ba_staff_day_off")
    ->where("bin", $bin)
    ->get();
    
    
    
    foreach($staff_available as $item)
    {
      if($item->profilePicture == "" || $item->profilePicture == null)
      {
        $item->profilePicture = config('app.app_cdn') . '/assets/image/no-image.jpg'  ;  
      } 
      
      
      $item->available = "yes";
      
      foreach($day_offs as $off ) 
      {
        if($item->staffId ==  $off->staff_id )
        {
          $item->available = "no";
          break;
        } 
      }
      
    }

    // section for available staff in time slots for particular date
      
        $staffid = array();  
        foreach( $staffs as $item)
        {
          $staffid[] = $item->profile_id;
        }
        $staffid[] = 0;
        
        $staff_available = DB::table("ba_profile") 
        ->whereNotIn("id",  function($query) use ($date, $bin )
        {
          $query->select("staff_id") 
          ->from('ba_staff_day_off')
          ->where("bin",  $bin ) 
          ->whereDate("off_date",  $date ) ;  
        })
        ->select("id as staffId", "fullname as staffName", "profile_photo as profilePicture" ) 
        ->whereIn("id",  $staffid )
        ->get(); 

        
        $day_offs = DB::table("ba_staff_day_off")
        ->where("bin", $bin)
        ->whereDate("off_date", $date )
        ->get(); 
        
        $day_slots = DB::table("ba_service_days")
        ->where("bin", $bin)
        ->where("slot_number", $request->slotNo)
        ->whereDate("service_date", $date )
        ->get();


        foreach($staff_available as $item)
          {
            if($item->profilePicture == "" || $item->profilePicture == null)
            {
              $item->profilePicture = config('app.app_cdn') . '/assets/image/no-image.jpg'  ;  
            } 
             
            $item->available = "yes";
            $isOnLeave =  "no";
            foreach($day_offs as $off ) 
            {
              if($item->staffId ==  $off->staff_id )
              {
                $isOnLeave = "yes";
                break;
              } 
            }
            
            if($isOnLeave == "yes")
            {
              $item->available = "no";
            }
            else 
            {
              foreach($day_slots as $slot ) 
              {
                if($item->staffId ==  $slot->staff_id  && $slot->status  != "open" )
                {
                  $item->available = "no";
                  break;
                }
              }
            }  
          }

    //time slots ends here
    
     

    $data= ['results' => $staff_available,'time'=>$time,'endtime'=>$endtime,'selectedDate'=>$date];
        
    $returnHTML = view('store_front.booking.staff_list')->with($data)->render(); 

    return response()->json(array("message" => "success", "status_code" => 1077,'html'=>$returnHTML));
  
  }
  
  
public function showAssistSteps(Request $request)
  {
       if( $request->assistid == ""  )
         {
         $err_msg = "Field missing!";
         $err_code = 9000;
         $data = array( "message" => "failure" , 'status_code' => $err_code , 'detailed_msg' => $err_msg) ;
         return json_encode( $data ); 
         }

          $uid = $sid = 0 ;
          try
          {
             $uid =  Crypt::decrypt( $request->guid );
             $sid =  Crypt::decrypt( $request->gsid );
          }
          catch (DecryptException $e)
          {
            $data= ["message" => "failure", "status_code" =>  999 , 'detailed_msg' =>   'Invalid access!'  ];
            return json_encode($data);
          }



         $task_for = $request->assistid;
         session()->put('category',$task_for);

         $returnHTML = view('store_front.assist.assist_steps_wizard')->with('taskid',$task_for)->render();
       
         $err_status ="success";
         $err_msg = "Action perform";
         $err_code =  7001;
         $data = array( "message" => $err_status  , "status_code"=>$err_code , 'detailed_msg' => $err_msg,
          'html'=>$returnHTML,'userkey'=>$uid);
      
      
         return json_encode($data);
    
  }
  

  //API method
	protected function toggleSponsoredService(Request $request)
    {

      //checking if parameters are missing
        if( $request->srv_prid == "")
        {
         $data= ["message" => "failure", "status_code" =>  902 , 'detailed_msg' => 'No order is selected.'  ];
         return json_encode($data);
        }
        //checking if the package actually exists
        $service = ServiceProductModel::find($request->srv_prid); 
        if(   !isset( $service )  )
        {
         $data= ["message" => "failure", "status_code" =>  999 , 'detailed_msg' => 'No matching package found.'  ];
         return json_encode($data);
        }
        $sponsored_package = SponsoredPackageModel::where('bin',$service->bin)
        ->where('package_id',$service->id)
        ->first();
        $delete_existing = false;
        if(   !isset( $sponsored_package )  )
        {
          if($request->status)
          {
            $sponsored_package = new SponsoredPackageModel;
            $sponsored_package->bin = $service->bin;
            $sponsored_package->package_id =  $request->srv_prid;
            $sponsored_package->save();
          }
          else 
          {
            $delete_existing= true;
          }
        }
        else 
        {
          $delete_existing = true;
        }  
        if( $delete_existing )
        {
            DB::table("ba_sponsored_package")
            ->where("package_id", $request->srv_prid)
            ->where("bin",$service->bin )
            ->delete();
        }

        $data= ["message" => "success", "status_code" =>  7005 ,  
        'detailed_msg' =>  "Packages sponsorsip status updated successfully!"   ]; 
        return json_encode( $data);  
  }


   protected function toggleBookingShow(Request $request)
    {
        if(  $request->section_id  == "" )
        {
         $data= ["message" => "failure", "status_code" =>  902 , 'detailed_msg' => 'No order is selected.'  ];
         return json_encode($data);
        } 

        $sections = LandingScreenSectionModel::find($request->section_id); 

        if(   !isset( $sections )  )
        {
         $data= ["message" => "failure", "status_code" =>  999 , 'detailed_msg' => 'No matching sections found.'  ];
         return json_encode($data);
        }
  
        if( $request->status  == "true" )
        {
          $sections->show_in_app = "yes";   
        }
        else 
        {
          $sections->show_in_app = "no";   
        } 
        $sections->save();  

        $data= ["message" => "success", "status_code" =>  7005 ,  'detailed_msg' =>  "sections status is updated."   ]; 
        return json_encode( $data);  
    }

    protected function pndAssignRequest(Request $request)
      {

        if( $request->pnd_id  == "" || $request->agent_id  == "" )
        {
         $data= ["message" => "failure", "status_code" =>  902 , 'detailed_msg' => 'Missing data to perform action.'  ];
         return json_encode($data);
        }

        $order_info = PickAndDropRequestModel::find( $request->pnd_id );
 
        if(   !isset( $order_info)  )
        {
         $data= ["message" => "failure", "status_code" =>  999 , 'detailed_msg' => 'No matching order found.'  ];
         return json_encode($data);
        }

        $active_agents  = DB::table("ba_agent_attendance")
        ->where("agent_id", $request->agent_id )
        ->first(); 
      
         $assign_agent = PndassignModel::where("book_id", $request->pnd_id)
         ->get();

         if(count($assign_agent) == 2)
         {
            $data= ["message" => "failure", "status_code" =>  999 ,  'detailed_msg' =>  "Max Assign Limit Reach." ];

            return json_encode( $data); 
         }

        $assign_agent = new PndassignModel;  
        $assign_agent->request_by = $order_info->request_by;
        $assign_agent->agent_id = $active_agents->agent_id;
        $assign_agent->book_id = $order_info->id;
        $assign_agent->request_status = "new";
        $save = $assign_agent->save(); 
        if($save)
        {         
          $data= ["message" => "success", "status_code" =>  7001 ,  'detailed_msg' =>  "Order assigned to delivery agent." ];

        }
        else
        {
          $data= ["message" => "failure", "status_code" =>  7002 ,  'detailed_msg' => 'Order assignment to agent failed.'];
        }
        return json_encode( $data); 
     }


}


