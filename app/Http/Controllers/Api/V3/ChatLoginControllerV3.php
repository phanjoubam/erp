<?php

namespace App\Http\Controllers\Api\V3;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Crypt;  
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\File;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Support\Facades\Cookie;

use App\Http\Controllers\Controller;
use App\User;
use App\Business;
use App\Traits\Utilities;
use App\BusinessHour;
use App\ServiceDuration;
use App\BusinessService;
use App\CustomerProfileModel;


class ChatLoginControllerV3 extends Controller
{
     use Utilities;
        
     // public function CustomerChatLogin(Request $request)
     // {
     //   if( $request->phone == "" || $request->password == "")
     //   {
     //      $data = ["message" => "failure", "status_code" =>  902 , 'detailed_msg' => 'Missing data to perform action.'  ];
     //      return json_encode($data);
     //   }

     //   $user = User::where("phone", $request->phone )->where("category", 0)->first();

     //   if(!isset($user))
     //   {
     //        $data= ["message" => "failure",  "status_code" =>  997 ,
     //        'detailed_msg' => 'User is not registered with us.'  ];
     //        return json_encode( $data); 
     //    }
         

     //    $data['memberId'] = $user->profile_id; 


     //    if(Hash::check( $request->password , $user->password )) 
     //    {
     //        $message = "success";
     //        $err_code = 1059;
     //        $err_msg =  'Login successful!'  ;
            
     //        $data['profilePicture'] = URL::to('/public/assets/image/no-image.jpg' );
     //        $data['coverPhoto'] =URL::to('/public/assets/image/no-image.jpg' );


     //        if(  $user->category == 0 ) //customer 
     //        {
     //            $fullname = "";
     //            $cust_profile = CustomerProfileModel::find(  $user->profile_id  ) ;  

     //            if( !isset($cust_profile))
     //            {
     //                $setupComplete = "no";    
     //            }
     //            else 
     //            {
     //                if( $cust_profile->fname == "" ||  $cust_profile->lname == "" || $cust_profile->locality == ""  || 
     //                $cust_profile->landmark== "" ||  $cust_profile->city  == "" || $cust_profile->state == "" ||  
     //                $cust_profile->pin_code  == "" ||   $cust_profile->phone  == ""   ) 
     //                {

     //                    $setupComplete = "no";    
     //                }
     //                else 
     //                { 
     //                    $setupComplete = "yes"; 
     //                }

     //                if($cust_profile->profile_photo!=null)
     //                {
     //                    $data['profilePicture'] = URL::to('/public/assets/image/member/') . $cust_profile->profile_photo ;
     //                } 
     //                if($cust_profile->cover_photo!=null)
     //                {
     //                    $data['coverPhoto'] = URL::to('/public/assets/image/member/') . $cust_profile->cover_photo ;
     //                }  
     //            }
 
     //            $data['businessAddress'] = '';
     //            $data['businessCategory'] = '' ;
     //            $data['businessMainType'] = '';
     //            $data['businessName'] = "";
     //            $data['bin'] = 0; 
     //            $data['businessSetup'] = ''; 
     //            $data['customerSetup'] = $setupComplete  ;  
     //            $data['agentSetup'] =  '';  
     //            $data['isVerified'] = (  $user->status == 0 ) ? "no" :  "yes";  
     //            $data['name'] =  $cust_profile->fullname; 



     //        }

     //        $data['category'] = $user->category ;
     //        $data['userId'] = $user->id ;  
     //        $data['sessionToken'] = md5(time()); 
     //        $data["message"] = $message;
     //        $data["status_code"] = $err_code ;
     //        $data["detailed_msg"] =  $err_msg   ; 
     //    }
     //    else 
     //    {
     //          $message = "failure";
     //          $err_code = 1060;
     //          $err_msg =  'Login failed due to wrong password!'  ; 
     //          $data["message"] = $message;
     //          $data["status_code"] = $err_code ;
     //          $data["detailed_msg"] =  $err_msg   ; 

     //    }
     //    $assists_task = DB::table('ba_assist_category')
     //    ->orderBy('display_order','asc')
     //    ->get();

     //    $returnHTML = view('store_front.assist.chat_assist')->with('assists',$assists_task)->render();
           
     //    $data["phone"] = $request->phone ;
     //    $data["html"] = $returnHTML ;  
     //    return json_encode($data);

     // }


  public function showChatResult(Request $request)
  {
    if( $request->quest == ""  )
         {
         $err_msg = "Field missing!";
         $err_code = 9001;
         $data = array( "message" => "failure" , 'status_code' => $err_code , 'detailed_msg' => $err_msg) ;
         return json_encode( $data ); 
         }

         $question = $request->quest;

         $result = DB::table("ba_bot_chat")
         ->where("question",$question)
         ->select("answer")
         ->first();

         $err_status ="success";
         $err_msg = "Action perform";
         $err_code =  7000;
         $data = array( "message" => $err_status  , "status_code"=>$err_code , 'detailed_msg' => $err_msg,
          'result'=>$result->answer); 
         return json_encode($data);  
  }

  
  public function loadShowAssist(Request $request)
  {
     
   if( $request->chat == "")
         {
         $err_msg = "Field missing!";
         $err_code = 9001;
         $data = array( "message" => "failure" , 'status_code' => $err_code , 'detailed_msg' => $err_msg) ;
         return json_encode( $data ); 
         } 


         $assists_task = DB::table('ba_assist_category')
         ->orderBy('display_order','asc')
         ->get();        
         $data = array('assists'=>$assists_task);
         $returnHTML = view('store_front.assist.chat_assist')->with($data)->render();
         $err_status ="success";
         $err_msg = "Action perform";
         $err_code =  7000;
         $data = array( "message" => $err_status  , "status_code"=>$err_code , 'detailed_msg' => $err_msg,
         'html'=>$returnHTML); 
         return json_encode($data);  
  }
}
