<?php

namespace App\Http\Controllers\Api\V3; 

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Crypt;  
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\File;
use Illuminate\Contracts\Encryption\DecryptException; 

use App\Http\Controllers\Controller;
use App\Jobs\SendEmaOrderSms;

 
use App\ShoppingCartTemporary;  
use App\ShoppingItemAddonModel;
use App\Business;  
use App\ProductModel; 
use App\ServiceBooking;
use App\OrderStatusLogModel;
use App\CustomerProfileModel;
use App\ShoppingOrderItemsModel;
use App\AddressModel;
use App\ProductWishListModel;

use App\Traits\Utilities;
use App\User;




class ShoppingBasketController extends Controller
{
	  use Utilities;

    //view product info by clothing items
    protected function viewClothingProductDetails(Request $request)
    {
      
      $user_info =  json_decode($this->getUserInfo($request->userId));
      if(  $user_info->user_id == "na" )
      {
         $data= ["message" => "failure", "status_code" =>  901 , 'detailed_msg' => 'User has no priviledge to access this feature! Please consult admin.'  ] ;
         return json_encode($data);
      }
      
      
      if ( $request->productCode  == ""    )
      {
        $data= ["message" => "failure", "status_code" =>  902 , 'detailed_msg' => 'Missing data to perform action.'  ];
        return json_encode($data);
      }
 

      $product_info  = DB::table("ba_products")
      ->join("ba_product_category", "ba_product_category.category_name", "=", "ba_products.category")
      ->where("ba_products.pr_code", $request->productCode ) 
      ->where("ba_product_category.business_category", "CLOTHING" ) 
      ->select( "ba_products.id as productId",  
           "ba_products.bin",
           "ba_products.pr_code as productCode", 
           "ba_products.pr_name as productName",
           "ba_products.category as categoryName", 
           "ba_products.gst_code as GST", 
           "ba_products.mfr_id as manufacturer",
           "ba_products.description",
           "ba_products.category",
           "unit_value as unitValue",
           "unit_name as unitName",   
           "allow_gift_wrap as allowedGiftWrap",
           "gift_wrap_charge as giftWrapCharge" ,
           DB::Raw("'[]' sizeColorMap") ,  
           DB::Raw("'[]' subDetails")   )
       ->first() ;
  


       if( !isset( $product_info  ))
       {
         $data= ["message" => "failure", "status_code" =>  902 , 'detailed_msg' => 'No matching clothing product found!'  ];
         return json_encode($data);
       }
 

       $product_subdetails  = DB::table("ba_product_clothing")
       ->where("pr_code", $product_info->productCode ) 
       ->select( "id as productSubId",   
           "bar_code as barCode",  
           "size",
           "color",
           "gender", 
           "pattern", 
           "initial_stock as initialStock",
           "min_required as minimumStock" ,  
           "stock_inhand as stock" ,  
           "max_order as maximumOrderAllowed", 
           "unit_price as unitPrice" ,  
           "photos as images" ,    
           "actual_price as actualPrice",
           "discount", 
           "discountPc", 
           "pack_type as packageType" ,
           DB::Raw("'[]' productSpecs") )
       ->get() ;
   
   $sizes = $subids =  array();

   foreach( $product_subdetails as $item )
   {
      $sizes[] =  $item->size;
      $subids[] = $item->productSubId;
   }

   $sizes = array_unique($sizes);
   $subids[] =0;


  $images  = DB::table("ba_product_photos")
       ->where("pr_code", $product_info->productCode ) 
       ->whereIn("sub_id", $subids)  
       ->select( "sub_id", "image_url_sm" )
       ->get() ;
 

   $sizeColorMap  =array();
   $i=0;
   foreach( $sizes as $item )
   {
     $sizeColorMap[$i]['size'] = $item ;
     $color =  array();
     foreach( $product_subdetails as $subitem )
     {
       if($item == $subitem->size )
       {
         $color[] =  $subitem->color;
       } 
     }

     $sizeColorMap[$i]['color'] = $color ;
     $i++;
     
   }
   
   $product_info->sizeColorMap = $sizeColorMap; 
   

   $techSpecs  = DB::table("ba_product_specs")
       ->where("pr_code", $product_info->productCode ) 
       ->whereIn("sub_id", $subids)  
       ->select( "sub_id as productSubId", "spec_title as title", "spec_value as details" )
       ->get() ;



   
   foreach( $product_subdetails as $item )
   {
        $allspecs =  $all_photos = array();
        foreach($images as $file )
          {
            if( $item->productSubId == $file->sub_id)
            {
              $all_photos[]   = $file->image_url_sm ; 
            } 
          } 

        if(count($all_photos) ==   0 )
        {  
          $all_photos[]   =  config('app.app_cdn')  . "/assets/image/no-image.jpg";
        }
        
        $item->images = $all_photos;
        
        
        foreach($techSpecs  as $spec)
        {
          if($spec->productSubId ==   $item->productSubId)
          {
            $allspecs[] = $spec;
          }
        } 
        
        $item->productSpecs =  $allspecs ; 

    }
    
    $product_info->subDetails  = $product_subdetails ; 
 
    
       $data= ["message" => "success", "status_code" =>  5011 ,  'detailed_msg' => 'Product details fetched!' , 
       'results' => $product_info];

       return json_encode( $data);
    }


    //view product info by footwear items
    protected function viewFootwearProductDetails(Request $request)
    {
      $user_info =  json_decode($this->getUserInfo($request->userId));
      if(  $user_info->user_id == "na" )
      {
           $data= ["message" => "failure", "status_code" =>  901 , 
           'detailed_msg' => 'User has no priviledge to access this feature! Please consult admin.'  ] ;
           return json_encode($data);
      } 
       
       
       if ( $request->productCode  == ""    )
       {
        $data= ["message" => "failure", "status_code" =>  902 , 'detailed_msg' => 'Missing data to perform action.'  ];
        return json_encode($data);
       }
 

       $product_info  = DB::table("ba_products")
       ->join("ba_product_category", "ba_product_category.category_name", "=", "ba_products.category")
       ->where("ba_products.pr_code", $request->productCode ) 
       ->where("ba_product_category.business_category", "FOOTWEAR" ) 
       ->select( "ba_products.id as productId",  
           "ba_products.bin",
           "ba_products.pr_code as productCode", 
           "ba_products.pr_name as productName",
           "ba_products.category as categoryName", 
           "ba_products.gst_code as GST", 
           "ba_products.mfr_id as manufacturer",
           "ba_products.description",
           "ba_products.category",
           "unit_value as unitValue",
           "unit_name as unitName",   
           "allow_gift_wrap as allowedGiftWrap",
           "gift_wrap_charge as giftWrapCharge" ,
           DB::Raw("'[]' sizeColorMap") ,  
           DB::Raw("'[]' subDetails")  )
       ->first() ;



       if( !isset( $product_info  ))
       {
         $data= ["message" => "failure", "status_code" =>  902 , 'detailed_msg' => 'No matching product found!'  ];
         return json_encode($data);
       }

       $product_subdetails  = DB::table("ba_product_footwear")
       ->where("pr_code", $product_info->productCode ) 
       ->select( "id as productSubId",   
           "bar_code as barCode",  
           "size",
           "color",
           "gender",  
           "initial_stock as initialStock",
           "min_required as minimumStock" ,  
           "stock_inhand as stock" ,  
           "max_order as maximumOrderAllowed", 
           "unit_price as unitPrice" ,  
           "photos as images" ,    
           "actual_price as actualPrice",
           "discount", 
           "discountPc", 
           "pack_type as packageType" )
       ->get() ;
   
   $sizes =  array();

   foreach( $product_subdetails as $item )
   {
      $sizes[] =  $item->size;
   }
   $sizes = array_unique($sizes);


   $sizeColorMap  =array();
   $i=0;
   foreach( $sizes as $item )
   {
     $sizeColorMap[$i]['size'] = $item ;
     $color =  array();
     foreach( $product_subdetails as $subitem )
     {
       if($item == $subitem->size )
       {
         $color[] =  $subitem->color;
       } 
     }

     $sizeColorMap[$i]['color'] = $color ;
     $i++;
     
   }


   $product_info->sizeColorMap = $sizeColorMap;
   $all_photos = array();
   
   foreach( $product_subdetails as $item )
   {
	   $files =  explode(",",  $item->images );
       $files =  array_filter($files  ); 

          if(count($files) > 0 )
          {
             $files=array_filter($files); 

             $folder_path =  config('app.app_cdn_path')  . "/assets/image/store/bin_" .  $product_info->bin .    "/";

             foreach($files as $file )
             {
               if(file_exists( $folder_path . $file ))
               {
                 $all_photos[]  =  URL::to( $folder_path  ) . "/" .    $file  ;
               }
               else
               {
                 $all_photos[]   = config('app.app_cdn')    . "/assets/image/no-image.jpg";
               }

             }

          }
          else 
          { 

             $all_photos[]   =  config('app.app_cdn')    . "/assets/image/no-image.jpg";
          }

          $item->images = $all_photos; 
       }


       $product_info->subDetails  = $product_subdetails ; 
    
       $data= ["message" => "success", "status_code" =>  5011 ,  'detailed_msg' => 'Product details fetched!' , 
       'results' => $product_info];

       return json_encode( $data);
    }
	
	
	
	
	//adding to temporary cart 
	protected function addItemToCart(Request $request)
    {
      if(  $request->productId == "" || $request->subProductId == ""   || $request->qty == ""  )
      {
       $data= ["message" => "failure", "status_code" =>  902 , 'detailed_msg' => 'Missing data to perform action.'  ];
       return json_encode($data);
      }
	  
	  $product_info = ProductModel::find($request->subProductId);
	  if(!isset($product_info))
	  {
		  $data= ["message" => "failure",   "status_code" =>  999 , 'detailed_msg' => 'No matching address found!'  ];
          return json_encode( $data);
	  }
			
	  
	  $lsid =  0 ;
	  $user_info =  json_decode($this->getUserInfo($request->userId));
      if(  $user_info->user_id == "na" )
      {
		//expecting a session ID 
		$lsid =   $request->lsid  ;
		if(  $request->lsid == ""     )
		{
			   $data= ["message" => "failure", "status_code" =>  902 , 'detailed_msg' => 'Missing data to perform action.'  ];
			   return json_encode($data);
		}
	  
	   
		
		$cart_item = ShoppingCartTemporary::where(  "prid",  $request->productId )
        ->where( "session_id",  $lsid )
        ->first(); 
		
		if( !isset(  $cart_item )  )
        {
            $cart_item = new ShoppingCartTemporary;
            $cart_item->session_id =  $lsid ; 
            $cart_item->prid = $request->productId;
            $cart_item->subid = $request->subProductId; 
			$cart_item->bin = $product_info->bin;
        } 
      }
	else 
	{
		if( $user_info->category  != 0  )
		{
			$data= ["message" => "failure", "status_code" =>  901 , 'detailed_msg' => 'This feature is available to customer only.'  ];
			return json_encode($data);
		}
		
		$cart_item = ShoppingCartTemporary::where( "bin",  $request->bin )
          ->where( "prid",  $request->prid )
          ->where( "member_id",  $user_info->member_id )
          ->first();

          if( !isset(  $cart_item )  )
          {
            $cart_item = new ShoppingCartTemporary;
            $cart_item->member_id = $user_info->member_id ; 
            $cart_item->prid = $request->productId;
            $cart_item->subid = $request->subProductId; 
			$cart_item->bin = $product_info->bin;
          }  
	}	

	if($request->action == "add")
    {
		$cart_item->qty += $request->qty; 
    }
    else 
    {
		$cart_item->qty = $request->qty; 
    }
	
	
	//calculating cost
	$product = DB::table("ba_products") 
	->where("id", $request->prid ) 
	->select("actual_price")
	->first() ;
	
	if(isset($product))
    {
		$cart_item->cost = $cart_item->qty * $product->actual_price ; 
    } 
 
        $save = $cart_item->save(); 
  
        if($save)
        {
          $data= ["message" => "success", "status_code" =>  6001 , 'detailed_msg' => 'Item added to cart.' ];
        }
        else
        {
        	$data= ["message" => "failure", "status_code" =>  6002 ,   'detailed_msg' =>  "Item could not be added to cart." ];
        } 
 
        return json_encode( $data); 
		 
     }
	
	
	
	//get shopping cart
	protected function getShoppingCartItems(Request $request)
    {

        if( !$request->session()->has('__user_id_' ))
        {
            return redirect("/shopping/login")->with("err_msg", "Please login to place an order!"); 
        } 


        $gsid = $request->session()->get('shopping_session'   );    
        $uid = $request->session()->get('__user_id_'   );  

        $member = DB::table("ba_profile")
        ->join("ba_users", "ba_users.profile_id", "=", "ba_profile.id") 
        ->where("ba_users.id",  $uid  ) 
        ->select("ba_profile.*")
        ->first();

        if(!isset($member ))
        {
            return redirect("/");
        }

        if($gsid != "")
        {
            //scan and update all product with matching member id  
            DB::table("ba_temp_cart")    
            ->where("session_id", $gsid  ) 
            ->orWhere("member_id", $member->id )
            ->update( ['member_id' =>  $member->id , 'session_id' => $gsid   ] ) ; 

        }
 
        $cart  = DB::table("ba_temp_cart")   
        ->join("ba_products", "ba_products.id", "=", "ba_temp_cart.prid") 
        ->where("member_id",  $member->id   ) 
        ->select( "ba_products.*", "ba_temp_cart.session_id", 
            "ba_temp_cart.member_id" , "ba_temp_cart.bin" , 
            "ba_temp_cart.prid", "ba_temp_cart.subid" , "ba_temp_cart.qty as pqty" )
        ->get();



        $cdn_url =   config('app.app_cdn') ;  
        $cdn_path =  config('app.app_cdn_path'); // cnd - /var/www/html/api/public 
        $cms_base_url = config('app.app_cdn')  ;  
        $cms_base_path = $cdn_path  ;  
        $no_image  =   $cdn_url .  "/assets/image/no-image.jpg";  
 

        foreach( $cart as $item)
        { 
            $thumbnail = $no_image ; 
            $files =  explode(",",  $item->photos );   
            if(count($files) > 0 )
            { 
                $product_folder_path =  $cms_base_path .  "/assets/image/store/bin_" .   $item->bin .  "/";
                $product_folder_url = $cms_base_url . "/assets/image/store/bin_" .   $item->bin .  "/";  
                $files=array_filter($files); 
                foreach($files as $file )
                {
                    $source = $product_folder_path .  $file;   
                    if(file_exists( $source  ))
                    {
                        $pathinfo = pathinfo( $source  );
                        $target_file_name  =  $pathinfo['filename'].  "_sm" . '.' . $pathinfo['extension']; 
                        $target_file_path   = $cms_base_path .  "/assets/image/store/bin_" . $item->bin  .  "/"  . $target_file_name ; 
                        if( file_exists( $target_file_path) )  
                        { 
                           $thumbnail  =  $product_folder_url . $target_file_name; 
                        }
                        else 
                        { 
                           $thumbnail = $product_folder_url .   $file;
                        } 

                        break;
                    }  
                } 

                $item->photos = $thumbnail;
            }
            else 
            {
                $item->photos = $no_image ;
            }

         }

 

        $bins = array(); 
        $bins[] = 0;
        foreach($cart as $item)
        {
            $bins[] = $item->bin;
        } 
         

        $bins= array_filter($bins);

        $businesses  = DB::table("ba_business")
        ->whereIn("id", $bins )
        ->get();
   

    
        $data = array(  'businesses' =>$businesses , 'cart' => $cart   );
        return view("store_front.checkout")->with(  $data );
    }
	
	
	
	//adding to wishlist
	protected function addToWishList(Request $request)
    {
      if(  $request->productCode == "" ||  $request->productSubId == "" )
      {
		   $data= ["message" => "failure", "status_code" =>  902 , 'detailed_msg' => 'Missing data to perform action.'  ];
		   return json_encode($data);
      }
	  
 
	  $user_info =  json_decode($this->getUserInfo($request->userId));
      if(  $user_info->user_id == "na" )
      {
		  $data= ["message" => "failure", "status_code" =>  901 , 'detailed_msg' => 'User has no priviledge to access this feature! Please consult admin.'  ] ;
          return json_encode($data);
      }
	  
	  if( $user_info->category  != 0  )
	  {
			$data= ["message" => "failure", "status_code" =>  901 , 'detailed_msg' => 'This feature is available to customer only.'  ];
			return json_encode($data);
	  }
	  
	  
	  $wishlist = ProductWishListModel::where( "pr_code",  $request->productCode   )
	  ->where( "sub_id",   $request->productSubId ) 
      ->where( "member_id",  $user_info->member_id )
      ->first();
	  
	  if( !isset( $wishlist  ) )
      {
		$wishlist = new ProductWishListModel;
        $wishlist->member_id = $user_info->member_id ; 
        $wishlist->pr_code = $request->productCode;
        $wishlist->sub_id = $request->productSubId;
        $wishlist->save();
      }
	  else 
	  {
		  $wishlist->delete();
	  }
	  $data= ["message" => "success", "status_code" =>  6007 ,   'detailed_msg' =>  "Wishlist updated!" ];
	  return json_encode( $data);  
	  
     }
	 
	 
	 protected function getProductsUnderCategory(Request $request)
     {
       	$products  = DB::table("ba_products") 
        ->join("ba_business", "ba_business.id", "=", "ba_products.bin" )   
        ->join("ba_business_category", "ba_business_category.name", "=", "ba_business.category" )    
        ->where("ba_products.bin",$request->bin)
        ->where("ba_products.category", $request->category )
        ->select( "ba_products.id as productId", 
			"ba_products.bin", "ba_business.name",  "ba_business.category as businessCategory",
			"ba_business_category.display_type as uiLayoutMode",
			"ba_products.pr_code as productCode", "ba_products.pr_name as productName",  
			"ba_products.description", 
			"ba_products.gst_code as GST" ,
			"ba_products.pack_type as packType" ,  
			"ba_products.unit_value as unitValue" , 
			"ba_products.unit_name as unitName", 
			"ba_products.category as productCategory", 
			
			DB::Raw("'0' stock"),
			DB::Raw("'0' maxOrderAllowed"),
			DB::Raw("'0' unitPrice"),
			DB::Raw("'0' actualPrice"), 
			DB::Raw("'0' discount"),
			DB::Raw("'0' discountPc"),
			DB::Raw("'0' packagingCharge"),
			DB::Raw("'Not-applicable' foodType"),
			DB::Raw("'no' freeText"),
			DB::Raw("'no' addon"),
			DB::Raw("'0' addon"),
			DB::Raw("'[]' addOnList"),
			DB::Raw("'[]' images"),
			DB::Raw("'no' bestSeller"), 
            DB::Raw("'1' status")  
			)  
        ->paginate(20) ;
   

      $prCodes = array();

      foreach( $products as $item)
      {
        $prCodes[] = $item->productCode;
      }
      $prCodes[] ="1"; 
      $product_add_ons =  DB::table("ba_restaurant_menu_addon")  
      ->whereIn("pr_code", $prCodes )
      ->select("id  as addOnId", "pr_code as productCode", "addon_name as addOnName", "price as addOnPrice")
      ->get(); 
	  
	  
	  $product_variants =  DB::table("ba_product_variant")  
      ->whereIn("pr_code", $prCodes ) 
      ->get();
	  
	  
	foreach( $products as $item)
    {
            $all_photos = array(); 
            
            $files =  explode(",",  $item->images ); 
            if(count($files) > 0 )
            {
                 
                $folder_path =  config('app.app_cdn_path')   . "/assets/image/store/bin_" .  $item->bin .    "/";
                $files=array_filter($files); 
                  foreach($files as $file )
                  {

						$source =  $folder_path . $file ;   
						if(file_exists( $source  ))
						{
							$pathinfo = pathinfo( $source  );
							$new_file_name  =  $pathinfo['filename'].  "_sm" . '.' . $pathinfo['extension'];  
							$destination  =  $folder_path . $new_file_name ; 
							  if( file_exists( $destination ) ) //smaller file exists
							  {
								  $all_photos[]  = config('app.app_cdn')  .   "/assets/image/store/bin_" . $item->bin  .  "/"  . $new_file_name ;
							  }
							  else 
							  {
								if(filesize(  $source ) > 307200) // 300 Kb
								{
									$this->chunkImage($source, $folder_path , $destination ); //chunking image size 
									$all_photos[]  = config('app.app_cdn')  .  "/assets/image/store/bin_" . $item->bin  .  "/"  . $new_file_name ;  
								}
								else 
								{
								  $all_photos[]  =  config('app.app_cdn')  .  "/assets/image/store/bin_" . $item->bin  .  "/" .    $file  ;
								}
							  } 
						}
						else
                        {
                          $all_photos[]   = config('app.app_cdn') .  "/assets/image/no-image.jpg" ;
                        }
                   
                }
            }

        $item->images = $all_photos; 
        $item->GST =  ( $item->GST == "" ? "Not Available" :  $item->GST );



        $addOnes = array();
        foreach($product_add_ons as $addon)
        {
          if( $addon->productCode == $item->productCode )
          {
            $addOnes[] =  $addon;                 
          }
        } 
        $item->addOnList =  $addOnes;   
	
		//updating product information
		foreach($product_variants as $variant)
        {
          if( $variant->pr_code == $item->productCode )
          {
                         
          }
        } 

		
    } 
       


      $final_result['last_page'] = $products->lastPage( ); 
      $final_result['data'] = $products->items();   

     	$data= ["message" => "success", "status_code" =>  5005 ,  'detailed_msg' => 'Products are fetched.' , 
     	'results' => $final_result];

      return json_encode( $data);
      

     }
	 
	 
	 //view product info by customer
     protected function viewProductInformation(Request $request)
     {

        if($request->productId  == ""   )
        {
          $data= ["message" => "failure", "status_code" =>  902 , 'detailed_msg' => 'Missing data to perform action.'  ];
          return json_encode($data);
        }

        
        $product_info  = DB::table("ba_products")
        ->join("ba_business", "ba_business.id", "=", "ba_products.bin" )  
        ->join("ba_business_category", "ba_business_category.name", "=", "ba_business.category" )  
        ->join("ba_product_variant","ba_product_variant.pr_code","=","ba_products.pr_code")
        ->where("ba_products.id", $request->productId ) 
        ->select("ba_products.id as productId",
            "ba_products.bin", "ba_business.name",  
            "ba_business.category as businessCategory",
           "ba_business_category.display_type as uiLayoutMode",
            "ba_products.pr_code as productCode", 
            "ba_products.pr_name as productName", 
            "ba_products.gst_code as GST", 
            "ba_products.mfr_id as manufacturer",
            "ba_products.description", 
            "ba_product_variant.stock_inhand as stock" ,  
            "ba_product_variant.max_order as maximumOrderAllowed", 
             "ba_products.unit_price as unitPrice" , 
             "ba_products.unit_name as unitName ", 
             "ba_products.pack_type as packType" ,  
             "ba_products.unit_value as unitValue" ,  
             "ba_product_variant.discount", 
             "ba_product_variant.discountPc", 
             "ba_products.actual_price as actualPrice",
              "ba_products.packaging as packagingCharge",
              "ba_products.category as productCategory", 
              "ba_products.food_type as foodType", 
              "ba_products.free_text as freeText", 
              "ba_products.addon_available as addon", 
               DB::Raw("'[]' addOnList"),
              "ba_products.photos as images"  , 
              "ba_products.is_bestseller as bestSeller",
              "ba_products.tags", 
              "ba_products.status"  )
        ->first() ;

       
        $all_photos = array();

        if(isset($product_info))
        {

          $product_add_ons =  DB::table("ba_restaurant_menu_addon")  
          ->where("pr_code", $product_info->productCode )
          ->select("id  as addOnId", "pr_code as productCode", "addon_name as addOnName", "price as addOnPrice")
          ->get(); 


          $product_info->addOnList =  $product_add_ons ;

          $files =  explode(",",  $product_info->images ); 
           if(count($files) > 0 )
           {
              $files=array_filter($files);
              $folder_path =  "/public/assets/image/store/bin_" .  $product_info->bin  . "/"; 
              foreach($files as $file )
              {
                if(file_exists( base_path() .  $folder_path .   $file ))
                {
                  $all_photos[]  =  URL::to( $folder_path  ) . "/" .    $file  ;
                }
                else
                {
                  $all_photos[]   = URL::to("/public/assets/image/no-image.jpg");
                }
              }

           }
   

            $product_info->images = $all_photos;
        } 
               
     
        $data= ["message" => "success", "status_code" =>  5011 ,  'detailed_msg' => 'Products are fetched.' , 
        'results' => $product_info];

        return json_encode( $data);
    }


	 
}
