<?php

namespace App\Http\Controllers\Api\V3; 

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Crypt;  
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\File; 
use Illuminate\Contracts\Encryption\DecryptException;
use App\Http\Controllers\Controller;

use App\Jobs\SendOrderConfirmationSms;
use App\Jobs\SendOrderAssignToAgentSms;
 

use App\ShoppingOrderItemsModel;  
use App\ShoppingCartTemporary;  
use App\AgentServiceAreaModel; 
use App\CustomerProfileModel;
use App\DeliveryOrderModel;
use App\ServiceBooking;
use App\OrderStatusLogModel; 
use App\ProductModel; 
use App\Business;
use App\Traits\Utilities;
use App\User; 
use App\MarketingCallLog;
use App\CallbackEnquiryModel;
use App\MemberWishlistModel;


class WebBookingControllerV3 extends Controller
{
	use Utilities; 
    
    
	
	 //get service calender
  protected function getDaySlots(Request $request)
    {
           if($request->bin == ""   )
    {
     $data= ["message" => "failure", "status_code" =>  902 , 'detailed_msg' => 'Missing data to perform action.'  ];
     return json_encode($data);
    }
    
    
    if( $request->dayName != "" )
    {
      $dayName = $request->dayName;
    }
    
    $date = date('Y-m-d', strtotime($request->date));
    /*
    $service_slots = DB::table("ba_services_duration") 
    ->where("bin", $request->bin)
    ->where("day_name",  $dayName )
    ->orderBy("start_time", "asc")
    ->select("slot_number as slotNo", "day_name as dayName", "start_time as startTime", "end_time as endTime", 
    "status as currentStatus" )
    ->get();
    */
    
    //fetching slots 
    $service_slots = DB::table("ba_service_days") 
    ->where("bin", $request->bin)
    ->whereDate("service_date", $date )
    ->orderBy("slot_number", "asc")
    ->select("slot_number as slotNo", "start_time as startTime", 
    "end_time as endTime","bin",
     DB::Raw("'booked' currentStatus" ) )
    ->distinct()
    ->get();   
    
    //checking slot availability 
    $all_slots = DB::table("ba_service_days") 
    ->where("bin", $request->bin)
    ->whereDate("service_date", $date ) 
    ->select("slot_number as slotNo", "start_time as startTime", "status" ) 
    ->get();
    
    foreach($service_slots as $unique_slot)
    {
      $status = $unique_slot->currentStatus;
      foreach($all_slots  as $slot)
      {
        //finding open slot
        if($unique_slot->slotNo == $slot->slotNo && strcasecmp($slot->status, "open" ) == 0 )
        {
          $status = "open";
          break; 
        } 
      }
      $unique_slot->currentStatus = $status; 
    }

    // staff list

    $date = $request->date == "" ?  date('Y-m-d') :  date('Y-m-d', strtotime( $request->date ) )  ;
    $bin = $request->bin;

    $staffs = DB::table("ba_users")  
    ->select("profile_id"  )
    ->where("bin", $request->bin)
    ->whereIn("category", array("1", '10' ))
    ->get();
  
    
    $staffid = array();  
    foreach( $staffs as $item)
    {
      $staffid[] = $item->profile_id;
    }
    
    if(count($staffid) == 0)
    {
      $data= ["message" => "failure", "status_code" =>  999 , 'detailed_msg' => 'No staff found!'  ];
      return json_encode( $data);
    }
    
    $staff_available = DB::table("ba_profile") 
    ->whereNotIn("id",  function($query) use ($date, $bin )
    {
      $query->select("staff_id") 
      ->from('ba_staff_day_off')
      ->where("bin",  $bin ) 
      ->whereDate("off_date",  $date ) ;  
    })
    ->select("id as staffId", "fullname as staffName", "profile_image_url as profilePicture", 
    DB::Raw("'no' available") ) 
    ->whereIn("id",  $staffid )
    ->get(); 
      
    // 

    if (count($service_slots)>0) {

           $msg = "success";
           $code = 7007;
           $data= ['results' => $service_slots,
                   'selectedDate'=>$date,
                   'staffs' => $staff_available]; 
        
           $returnHTML = view('store_front.booking.day_slots')->with($data)->render(); 
          
        }else{

           $msg = "Time slots not available! for the specific business";
           $code = 7777;
           $returnHTML="";
        } 
        

        return response()->json(array("message" => $msg, 
                                      "status_code" => $code,
                                      'html'=>$returnHTML)
                                );
        //return json_encode( $data); 

     }



    //get staff availability
  public function getStaffAvailability(Request $request)
   {
      $uid = 0 ;
          try
          {
             $uid =  Crypt::decrypt( $request->guid ); 
          }
          catch (DecryptException $e)
          {
            $data= ["message" => "failure", "status_code" =>  999 , 'detailed_msg' =>   'Invalid access!'  ];
            return json_encode($data);
          } 
        
      $user_info =  json_decode($this->getUserInfo($uid));
      if($user_info->user_id == "na" )
      {
        $data= ["message" => "failure", "status_code" =>  901 ,
        'detailed_msg' => 'User has no priviledge to access this feature! Please signup to use our services.'  ];
        return json_encode($data);
      }
      if($request->bin == ""  )
      {
        $data= ["message" => "failure", "status_code" =>  999 ,
            'detailed_msg' => 'Business information not found!'  ];
            return json_encode( $data);
      }
    
    $date = $request->date == "" ?  date('Y-m-d') :  date('Y-m-d', strtotime( $request->date ) )  ;
    $bin = $request->bin;
    $time = $request->time;
    $endtime = $request->endtime;

    //time slots ends here
    
     $staffs = DB::table("ba_users")  
    ->select("profile_id"  )
    ->where("bin", $request->bin)
    ->whereIn("category", array("1", '10' ) )
    ->get();
  
    
    $staffid = array();  
    foreach( $staffs as $item)
    {
      $staffid[] = $item->profile_id;
    }
    
    if(count($staffid) == 0)
    {
      $data= ["message" => "failure", "status_code" =>  999 , 'detailed_msg' => 'No staff found!'  ];
      return json_encode( $data);
    }
    
    
    $staff_available = DB::table("ba_profile") 
    ->whereNotIn("id",  function($query) use ($date, $bin )
    {
      $query->select("staff_id") 
      ->from('ba_staff_day_off')
      ->where("bin",  $bin ) 
      ->whereDate("off_date",  $date ) ;  
    })
    ->select("id as staffId", "fullname as staffName", "profile_image_url as profilePicture", 
    DB::Raw("'no' available") ) 
    ->whereIn("id",  $staffid )
    ->get();
    
    $day_offs = DB::table("ba_staff_day_off")
    ->where("bin", $bin)
    ->whereDate("off_date", $date )
    ->get(); 
    
    $day_slots = DB::table("ba_service_days")
    ->where("bin", $bin)
    ->where("slot_number", $request->slotNo)
    ->whereDate("service_date", $date )
    ->get(); 
    
     
        
    foreach($staff_available as $item)
    { 
      $isOnLeave =  "no";
      foreach($day_offs as $off ) 
      {
        if($item->staffId ==  $off->staff_id )
        {
          $isOnLeave = "yes";
          break;
        } 
      }
     
      if($isOnLeave == "yes")
      {
        $item->available = "no";
      }
      else 
      {
        foreach($day_slots as $slot ) 
        {
          if($item->staffId ==  $slot->staff_id  && $slot->status  == "open" )
          {
            $item->available = "yes";
            break;
          }
        }
      } 
    }  
    
    //$returnHTML = view('store_front.booking.staff_list')->with($data)->render();
    //return response()->json(array("message" => "success", "status_code" => 1077,'html'=>$returnHTML));
    return response()->json(array( 
                                  "message" => "success", 
                                  "status_code" =>  7007 ,
                                  'detailed_msg' => 'Business hours fetched.' ,
                                  'results' => $staff_available,
                                  'time'=>$time,
                                  'endtime'=>$endtime,
                                  'selectedDate'=>$date
                                ));
  
  } 
	
	 public function getServiceNames(Request $request)
  {
          if ($request->category=="" || $request->bin=="") {
              return redirect('services/add-new-booking')->with('err_msg','missing data to perform action!');
          }

          $products = DB::table("ba_service_products") 
          ->where("service_category",$request->category)
          ->where("bin",$request->bin)
          ->select("ba_service_products.*")
          ->get();  

          $result= ['results' => $products]; 
          $returnHTML = view('store_front.service.service_name_list')->with($result)->render(); 

         

          $data= ["message" => "success", "status_code" =>  1111 , 
                  'detailed_msg' => 'service name fetch.','html'=>$returnHTML];     

          return json_encode($data);
  }


  protected function getAvailableDays(Request $request)
    {
          $uid = 0 ;
          try
          {
             $uid =  Crypt::decrypt( $request->guid ); 
          }
          catch (DecryptException $e)
          {
            $data= ["message" => "failure", "status_code" =>  999 , 'detailed_msg' =>   'Invalid access!'  ];
            return json_encode($data);
          } 

          $user_info =  json_decode($this->getUserInfo($uid));
          if(  $user_info->user_id == "na" )
          {
            $data= ["message" => "failure", "status_code" =>  901 , 'detailed_msg' => 'User has no priviledge to access this feature! Please consult admin.'  ] ;
            return json_encode($data);
          }
    
          
          if(  $request->bin == "" || $request->month  == ""  || $request->year == ""   )
          {
             $data= ["message" => "failure", "status_code" =>  999 , 'detailed_msg' => "Missing data to perform action." ];
             return json_encode($data);
          }

     $bin = $request->bin ;
     
     $business = Business::find($bin);
     if(  !isset($business) )
     {
        $data= ["message" => "failure", "status_code" =>  999 ,  'detailed_msg' => 'No matching business found!' ]; 
        return json_encode($data);
      }

      $lastday =  cal_days_in_month(CAL_GREGORIAN,  $request->month , $request->year );
      $days = array();
      if( strcasecmp($business->sub_module, "sns") == 0 )
      {

         $booked_dates = DB::table("ba_service_days")  
         ->whereMonth("service_date",  $request->month )
         ->whereYear("service_date",  $request->year  )  
         ->where("bin",$request->bin)   
         ->select(DB::Raw( "distinct service_date"), "status") 
         ->get(); 
          
         for($i= 1 ; $i<=$lastday ; $i++)
         {
           $status ="closed";
           $found = false;
           foreach($booked_dates  as $item)
           {
             if( date('d', strtotime($item->service_date)) == $i)
             {
               if( $item->status == "engage" || $item->status == "booked" )
               {
                 $status ="booked";
               }
               else 
               {
                 $status ="open";
               }
               $found = true; 
               break;
             } 
           }
           
           if( !$found)
           {
             $status ="close";
           } 
//$days[] = array( 'serviceProductId' => 0,   "dayNumber" => $i, "status" => $status); 
           $days[] = array(  "dayNumber" => $i, "status" => $status); 
         }

      }
      else if( strcasecmp($business->sub_module, "paa") == 0 )
      {
        $booked_dates  = array(); 
        //need selected product ID in future code update
        if(  $request->serviceProductIds != "" )
        {
          $service_product_ids = explode( ",", $request->serviceProductIds ); 
          if(sizeof($service_product_ids) > 0 )
          {
            $booked_dates  = DB::table("ba_service_booking")  
            ->join("ba_service_booking_details", "ba_service_booking_details.book_id", "=", "ba_service_booking.id" )
           ->whereMonth("ba_service_booking.service_date",  $request->month )
           ->whereYear("ba_service_booking.service_date",  $request->year  )  
           ->whereIn("ba_service_booking_details.service_product_id",  $service_product_ids  )   
           ->where("ba_service_booking.bin",$request->bin)  
           ->whereIn("book_status",  array('new','confirmed','engaged', 'pending-payment') )   
           ->select("ba_service_booking.id as book_id", "service_date",  "ba_service_booking_details.service_product_id", 
            DB::Raw("'close' status") ) 
           ->get(); 
          } 

        }

          for($i=  1; $i<=$lastday ; $i++)
          {
               $status ="open";
               $found = false;
               foreach($booked_dates  as $item)
               {
                 if( date('d', strtotime($item->service_date)) == $i)
                 {
                   $found = true; 
                   break;
                 } 
               }
               
               if( $found )
               {
                 $status ="close";
               } 

               //$days[] = array( 'serviceProductId' => 0,   "dayNumber" => $i, "status" => $status); 

               $days[] = array(    "dayNumber" => $i, "status" => $status); 
          } 
 
      }

      $data= ["message" => "success", "status_code" =>  "3021" ,  
      'detailed_msg' => "Available dates are fetched.", 
     'results' =>  $days ]; 
     
      return json_encode( $data); 

     }



     protected function checkDateAvailability(Request $request)
  {

     if(  $request->bin == "" || $request->date  == ""  || $request->serviceProductIds == "")
     {
         return response()->json(array(
          "detailed_msg" => 'Missing data to perform action!', 
          "status_code" => 999,
          "message" => "failure"
         ));
     }

    $service_product_ids  = $request->serviceProductIds; 

    if(sizeof($service_product_ids) == 0 )
    {
      
       return response()->json(array(
        "detailed_msg" => 'No service selected!', 
        "status_code" => 999,
        "message" => "failure"));
    } 

     $bin = $request->bin ;
     $business = Business::find($bin);

     if(  !isset($business) )
     {
        return response()->json(array(
        "detailed_msg" => 'No matching business found!', 
        "status_code" => 999,
        "message" => "failure"));
      } 

      if( strcasecmp($business->sub_module, "sns") == 0 )
      {

         $booked_dates = DB::table("ba_service_days")  
         ->where("service_date", date('Y-m-d', strtotime($request->date)))  
         ->where("bin",$request->bin)   
         ->select(DB::Raw( "distinct service_date"), "status") 
         ->get(); 
          
         for($i= 1 ; $i<=$lastday ; $i++)
         {
           $status ="closed";
           $found = false;
           foreach($booked_dates  as $item)
           {
             if( date('d', strtotime($item->service_date)) == $i)
             {
               if( $item->status == "engage" || $item->status == "booked" )
               {
                 $status ="booked";
               }
               else 
               {
                 $status ="open";
               }
               $found = true; 
               break;
             } 
           }
           
           if( !$found)
           {
             $status ="close";
           }
           $days[] = array( 'serviceProductId' => 0,   "dayNumber" => $i, "status" => $status);
         }

      }
      else if( strcasecmp($business->sub_module, "paa") == 0 )
      {

        
        $booked_dates  = array(); 
        //need selected product ID in future code update

        $booked_dates  = DB::table("ba_service_booking")  
        ->join("ba_service_booking_details", "ba_service_booking_details.book_id", "=", "ba_service_booking.id" )
        ->whereDate("ba_service_booking.service_date", date('Y-m-d', strtotime( $request->date))) 
        ->whereIn("ba_service_booking_details.service_product_id",  $service_product_ids )   
           ->where("ba_service_booking.bin", $request->bin)  
           //->whereIn("book_status",  array('new','confirmed','engaged', 'pending-payment'))
           ->whereIn("book_status",  array('new','confirmed','engaged'))   
           ->select("ba_service_booking.id as book_id", "service_date",  
            "ba_service_booking_details.service_product_id", 
            DB::Raw("'close' status") ) 
           ->get();
    
           //preparing available days : FUTURE CODE
           foreach($service_product_ids as $service_product_id)
           {

            $status ="open";
            $found = false;

            foreach($booked_dates as $item)
                 { 
                   if(  $item->service_product_id == $service_product_id)
                   {
                     $found = true; 
                     break;
                   } 
                 }
                 
                 if( $found )
                 {
                   $status ="close";
                 } 

                 $days[] = array( 'serviceProductId' => $service_product_id,   "dayNumber" =>  date('d', strtotime( $request->date  ))  ,  "status" => $status,'photos'=>'','productName'=>''); 
               
           } 
 
      } 


      $productPhotos = DB::table('ba_service_products')
      ->whereIn('id',$service_product_ids)
      ->get();

     
      $i = 0;
      for ($i=0; $i<count($days);$i++) {
        foreach($productPhotos as $photos)
        {
          if ($days[$i]['serviceProductId'] == $photos->id) {

             $days[$i]['photos'] = $photos->photos;
             $days[$i]['productName'] = $photos->srv_name;
          }
        }
      }

      // getting booking hour for particular day

      $business = DB::table("ba_business")
     ->where("id", $request->bin )
     ->select("id as bin", "name", "tag_line as tagLine", "phone_pri as phone",
              "locality", "landmark", "city", "state", "pin", "profile_image as profileImgUrl", "banner as bannerImgUrl" , "rating" ,
              DB::Raw("'[]' services")
              )
     ->first();

      if( !isset($business))
      {
      $data= ["message" => "failure",  'results' => array(),  "status_code" =>  999 ,
          'detailed_msg' => 'Business information not found!'  ];
        return json_encode( $data);
      }

      //show services

      $fbh = array(
        array("dayName" => "Monday", "shiftBreakUps" => array() , "shifts" => array()),
        array("dayName" => "Tuesday", "shiftBreakUps" => array() , "shifts" => array()),
        array("dayName" => "Wednesday", "shiftBreakUps" => array() , "shifts" => array()),
        array("dayName" => "Thursday", "shiftBreakUps" => array() , "shifts" => array()),
        array("dayName" => "Friday", "shiftBreakUps" => array() , "shifts" => array()),
        array("dayName" => "Saturday", "shiftBreakUps" => array() , "shifts" => array()),
        array("dayName" => "Sunday", "shiftBreakUps" => array() , "shifts" => array())
        );


      $business_duration = DB::table("ba_services_duration")
      ->where("bin", $request->bin)
      ->select( "day_name as dayName", DB::Raw("  CONCAT( start_time , '-',  end_time) as  shiftBreakUps") )
      ->orderby('slot_number','ASC')
      ->get();


        for($i=0; $i < sizeof($fbh) ; $i++)
        {
          $j = 0;
          foreach($business_duration as $item)
          {
            if(strcasecmp($fbh[$i]['dayName'], $item->dayName) == 0 )
            {
              $first[$i] = explode("-", $item->shiftBreakUps);
                      $open = date('h:i A', strtotime($first[$i][0]));
                      $close = date('h:i A', strtotime($first[$i][1]));
                      $joinTwoItem = $open. " - " .$close;
              $fbh[$i]['shiftBreakUps'][$j] = $joinTwoItem;
              $j++;
            }
          }
        }

        $business_hours = DB::table("ba_shop_hours")
        ->where("bin", $request->bin)
        ->select( "day_name as dayName", DB::Raw("  CONCAT( day_open , '-',  day_close) as  shifts") )
        ->get();


          for($i=0; $i < sizeof($fbh) ; $i++)
          {
            $j = 0;
            foreach($business_hours as $item)
            {
              if(strcasecmp($fbh[$i]['dayName'], $item->dayName) == 0 )
              {
                $first[$i] = explode("-", $item->shifts);
                        $open = date('h:i A', strtotime($first[$i][0]));
                        $close = date('h:i A', strtotime($first[$i][1]));
                        $joinTwoItem = $open. " - " .$close;
                $fbh[$i]['shifts'][$j] = $joinTwoItem;
                $j++;


              }
            }
          } 

          

      // booking hours fetch ends here

      $dayname = $request->dayName;

      $data = array('days'=>$days,'servicehours'=>$fbh,'dayName'=>$dayname,'serviceDate'=>$request->date);
      
      //$returnHTML = view('store_front.service.view_service_product_photos')->with($data)->render();
      $returnHTML = view('store_front.service.view_service_product_photos')->with($data)->render();

      return response()->json(array(
        "message" => "success", 
        "status_code" =>  "3021" ,  
        'detailed_msg' => "Available dates are fetched.",
        //'results' =>  $days,
        'html' =>  $returnHTML ));

     }

     
  //fetch all slots for booking sub-module
  protected function checkBookingDateAvailability(Request $request)
  {

     if(  $request->bin == "" || $request->date  == ""  || $request->serviceProductIds == "")
     {
         return response()->json(array(
          "detailed_msg" => 'Missing data to perform action!', 
          "status_code" => 999,
          "message" => "failure"
         ));
     }

    $service_product_ids  = $request->serviceProductIds; 

    if(sizeof($service_product_ids) == 0 )
    {
      
       return response()->json(array(
        "detailed_msg" => 'No service selected!', 
        "status_code" => 999,
        "message" => "failure"));
    } 

     $bin = $request->bin ;
     $business = Business::find($bin);

     if(  !isset($business) )
     {
        return response()->json(array(
        "detailed_msg" => 'No matching business found!', 
        "status_code" => 999,
        "message" => "failure"));
      }  
      if( strcasecmp($business->sub_module, "paa") != 0 )
      {
        return response()->json(array(
          "detailed_msg" => 'Selected business is not registered under booking service!', 
          "status_code" => 999,
          "message" => "failure"));
      }
      
      $booked_dates  = array(); 
        //need selected product ID in future code update

        $booked_dates  = DB::table("ba_service_booking")  
        ->join("ba_service_booking_details", "ba_service_booking_details.book_id", "=", "ba_service_booking.id" )
        ->whereDate("ba_service_booking.service_date", date('Y-m-d', strtotime( $request->date))) 
        ->whereIn("ba_service_booking_details.service_product_id",  $service_product_ids )   
           ->where("ba_service_booking.bin", $request->bin)  
           //->whereIn("book_status",  array('new','confirmed','engaged', 'pending-payment'))
           ->whereIn("book_status",  array('new','confirmed','engaged'))   
           ->select("ba_service_booking.id as book_id", "service_date",  
            "ba_service_booking_details.service_product_id", 
            DB::Raw("'close' status") ) 
           ->get();
     
           foreach($service_product_ids as $service_product_id)
           {

            $status ="open";
            $found = false;

            foreach($booked_dates as $item)
                 { 
                   if(  $item->service_product_id == $service_product_id)
                   {
                     $found = true; 
                     break;
                   } 
                 }
                 
                 if( $found )
                 {
                   $status ="close";
                 } 

                 $days[] = array( 'serviceProductId' => $service_product_id,   "dayNumber" =>  date('d', strtotime( $request->date  ))  ,  "status" => $status ); 
               
           } 
           
           $business_hours = DB::table("ba_shop_hours")
           ->where("bin", $request->bin)
           ->where("day_name",  $request->dayName)
           ->select( "day_name as dayName", DB::Raw("  CONCAT( time_format( day_open,  '%r' )  , '-',  time_format( day_close,  '%r' ) ) as  shifts") )
           ->get();      
      
          
      
      return response()->json(array(
        "message" => "success", 
        "status_code" =>  "3021" ,  
        'detailed_msg' => "Available dates are fetched.",
        'business_hours' =>$business_hours,
        'results' =>  $days ));

     }


  public function getServiceDays(Request $request)
   {


    if($request->bin == "")
     {
       $data= ["message" => "failure", "status_code" =>  902 , 'detailed_msg' => "No business selected to it's services." ];
       return json_encode($data);
     }

     $business = DB::table("ba_business")
     ->where("id", $request->bin )
     ->select("id as bin", "name", "tag_line as tagLine", "phone_pri as phone",
              "locality", "landmark", "city", "state", "pin", "profile_image as profileImgUrl", "banner as bannerImgUrl" , "rating" ,
              DB::Raw("'[]' services")
              )
     ->first();

      if( !isset($business))
      {
      $data= ["message" => "failure",  'results' => array(),  "status_code" =>  999 ,
          'detailed_msg' => 'Business information not found!'  ];
        return json_encode( $data);
      }

      //show services

      $fbh = array(
        array("dayName" => "Monday", "shiftBreakUps" => array() , "shifts" => array()),
        array("dayName" => "Tuesday", "shiftBreakUps" => array() , "shifts" => array()),
        array("dayName" => "Wednesday", "shiftBreakUps" => array() , "shifts" => array()),
        array("dayName" => "Thursday", "shiftBreakUps" => array() , "shifts" => array()),
        array("dayName" => "Friday", "shiftBreakUps" => array() , "shifts" => array()),
        array("dayName" => "Saturday", "shiftBreakUps" => array() , "shifts" => array()),
        array("dayName" => "Sunday", "shiftBreakUps" => array() , "shifts" => array())
        );


      $business_duration = DB::table("ba_services_duration")
      ->where("bin", $request->bin)
      ->select( "day_name as dayName", DB::Raw("  CONCAT( start_time , '-',  end_time) as  shiftBreakUps") )
      ->orderby('slot_number','ASC')
      ->get();


        for($i=0; $i < sizeof($fbh) ; $i++)
        {
          $j = 0;
          foreach($business_duration as $item)
          {
            if(strcasecmp($fbh[$i]['dayName'], $item->dayName) == 0 )
            {
              $first[$i] = explode("-", $item->shiftBreakUps);
                      $open = date('h:i A', strtotime($first[$i][0]));
                      $close = date('h:i A', strtotime($first[$i][1]));
                      $joinTwoItem = $open. " - " .$close;
              $fbh[$i]['shiftBreakUps'][$j] = $joinTwoItem;
              $j++;
            }
          }
        }

        $business_hours = DB::table("ba_shop_hours")
        ->where("bin", $request->bin)
        ->select( "day_name as dayName", DB::Raw("  CONCAT( day_open , '-',  day_close) as  shifts") )
        ->get();


          for($i=0; $i < sizeof($fbh) ; $i++)
          {
            $j = 0;
            foreach($business_hours as $item)
            {
              if(strcasecmp($fbh[$i]['dayName'], $item->dayName) == 0 )
              {
                $first[$i] = explode("-", $item->shifts);
                        $open = date('h:i A', strtotime($first[$i][0]));
                        $close = date('h:i A', strtotime($first[$i][1]));
                        $joinTwoItem = $open. " - " .$close;
                $fbh[$i]['shifts'][$j] = $joinTwoItem;
                $j++;


              }
            }
          } 

        $data= ["message" => "success", "status_code" =>  1029 ,
        'detailed_msg' => 'Business hours fetched.' ,
        'results' => $fbh] ;

        return json_encode($data);

        }
	
	  
}
