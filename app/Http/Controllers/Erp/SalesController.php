<?php

namespace App\Http\Controllers\Erp; 

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Contracts\Encryption\DecryptException;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\URL;



use App\Business; 
use App\ProductCategoryModel; 
use App\ProductModel;
use App\ShoppingOrderModel;
use App\ShoppingOrderItemsModel;
use App\AddressModel;
use App\MemberAddressModel;
use App\DeliveryOrderModel;
use App\ServiceBooking;
use App\OrderStatusLogModel; 
use App\CustomerProfileModel;
use App\PaymentDealingModel;
use App\PremiumBusiness;
use App\ProductImportedModel; 
use App\CloudMessageModel; 
use App\PosCartTemporary;
use App\OrderSequencerModel;

use App\Imports\ProductImport;

use App\Traits\Utilities;

use PDF;


class SalesController extends Controller
{
    //
	use Utilities; 
 	
	 public function suggestProductList(Request $request)

	 {		 
	        
	      $search = $request->search;
          $bin = session()->get('_bin_');

	      if($search)
	      {
	         $result = 
	         ProductModel::select('pr_code','pr_name','description')
             ->join("ba_product_variant","ba_products.id","=","ba_product_variant.prid")
             ->where('pr_name', 'like','%'.$search .'%')
             ->where('bin',$bin)
             ->where('ba_product_variant.stock_inhand',">",0) 
	         ->limit(30)
	         ->get();
	      }

	      $response = array();
           

	      foreach($result as $result){

            $path = '<img src="'.$result->photos.'" height="50px" weight="50px"/>';

	         $response[] = array(
                "label"=>$result->pr_name,
                "value"=>$result->pr_code,
                "icon"=> 'http://localhost/erp/public/assets/image/store/bin_'.$bin.'/'.$result->photos.''
            );

	      }
	      echo json_encode($response);
	      exit;

	 }
 

	protected function enterSalesDetails(Request $request)
	{
        $bin = $request->session()->get("_bin_"); 
        $uid  = $request->session()->get("__user_id_");  
        
        try
        {
            $cartno  =  Crypt::decrypt( $request->enccno ); 
        }
        catch (DecryptException $e)
        {
            $cartno =  1; 
        } 
 

        $todayDate = date('Y-m-d');
        $randomcode = mt_rand(10,1003679731);
        session()->put('_cartid_', $randomcode);

        $total_temp_cart = DB::table('ba_pos_temp_cart')
        ->where('bin', $bin )
        ->select(DB::Raw('DISTINCT cart_no as cartNo'))
        ->get();

        $count = count($total_temp_cart);
 
         $theme = DB::table("ba_theme")
        ->first();
        
        if ($request->btn_save=="SaveOrder") {

             $orderNO=0;  
          //fetching result id from ba_serviceBooking
          // $resultOrder = DB::table('ba_service_booking')
          // ->select(DB::Raw('max(id) as orderID'))
          // ->first();
          //
            
         $business = Business::find(session()->get('_bin_'));
        //generating order no for the particular booking
          // if($resultOrder)
          //   {
              
          //       $orderNO = $resultOrder->orderID + 1;
              
          //   }
          //   else
          //   {
          //       $orderNO = $orderNO + 1;
          //   }
        //generating order no ends here

            $sequencer = new OrderSequencerModel;
            $sequencer->type = "pnd";
            $save = $sequencer->save();
                
            if( !isset($save) )
            { 
             return redirect("product/enter-sales-details")->with('theme',$theme);  
            }

            $servicebooking = new ServiceBooking();
            $servicebooking->id = $sequencer->id;
            $servicebooking->bin = session()->get('_bin_');

            $servicebooking->book_category= $business->category;

            $servicebooking->book_by  = 0 ;
            $servicebooking->staff_id  = 0;
            $servicebooking->otp = mt_rand(10,123456);
            $servicebooking->is_confirmed = "yes";
            $servicebooking->customer_name = $request->customerName;
            $servicebooking->customer_phone = $request->customerPhone;
            $servicebooking->address  = "NA" ;
            $servicebooking->book_status = "confirmed";
            $servicebooking->delivery_charge =  $request->delivery_charge;
            $servicebooking->discount =  $request->discount;
            $servicebooking->total_cost =  $request->total_amount;
            $servicebooking->source = "business";
            $servicebooking->clerance_status = "paid";

            $servicebooking->book_date = new \DateTime();
            $servicebooking->service_date = new \DateTime();
             
            //saving the record in ba_service_booking table
            $save = $servicebooking->save();



            //inserting record into shopping basket
               
            for($i = 0; $i < count($request->category); $i++) {
                $category = $request->category[$i];
                $product_code = $request->product_code[$i]; 
                $productname = $request->product_name[$i]; 
                $qty = $request->quantity[$i];
                $unitname = $request->unit[$i];
                $amount = $request->amount[$i]; 
                $prid = $request->prid[$i]; 

                $shoping = new ShoppingOrderItemsModel;
                $shoping->order_no = $sequencer->id;
                $shoping->pr_code = $product_code;
                $shoping->pr_name = $productname;
                $shoping->qty = $qty;
                $shoping->unit = $unitname;
                $shoping->price = $amount;
                $shoping->category_name = $category;

                $saveshopping = $shoping->save();

                if (!$saveshopping) {
                   return redirect("product/enter-sales-details")->with('theme',$theme);  
                }

               
            } 
            //record insertion ends here
             //deleting temporary cart items from pos_temp_cart when save order is click


                $session_id = $request->sessionVariable;
               
                $delete = DB::table('ba_pos_temp_cart')
                ->where('session_id','=',$session_id)->delete();

                if (!$delete) {
                   return redirect("product/enter-sales-details")->with('theme',$theme);  
                }

                //deleting record ends here

                //generating pdf file when saving the record

                $order_info = $order_items =null;
               
                $order_info = ServiceBooking::find( $sequencer->id );

                $order_items = DB::table("ba_shopping_basket")
                ->where("order_no", $sequencer->id)
                ->get( );
                
                $pdf_file =  "rcp-" .time(). '-' . $sequencer->id  . ".pdf";
                $save_path = public_path() . "/assets/erp/bills/"  .  $pdf_file;

                $pdf = PDF::setOptions(['defaultFont' => 'sans-serif'])
                ->setPaper('a4', 'portrait')
                ->loadView('docs.bill_small_size' ,  
                array('data' =>   $order_info , 'order_items' => $order_items ));

                //$pdf->download( $pdf_file  );
                $pdf->save($save_path);
                //pdf generations ends here

            if ($save) {
               session()->forget('_cartid_');
               $data = array('total_temp_cart'=>$total_temp_cart,'count'=>$count,'pdf_file'=>$pdf_file);
                return view('sales.sales_entry_details')
                ->with('data',$data)
                ->with('theme',$theme);
            }
        }
        
       

        $data = array('total_temp_cart'=>$total_temp_cart,'count'=>$count, 'cartno' => $cartno );
		return view('sales.sales_entry_details')
        ->with('data',$data)
        ->with('theme',$theme);
	}


    


	protected function showProductList(Request $request)
	{

        $todayDate = date('Y-m-d');
        $product_code = $request->prodcode;
        $result = DB::table('ba_products')
                ->join("ba_product_variant","ba_products.id","=","ba_product_variant.prid")
                ->where('pr_code',$product_code)
                //->where('stock_inhand',">",0)
                ->select()
                ->get();

        $total_temp_cart = DB::table('ba_pos_temp_cart')
        ->whereDate('order_date',$todayDate)
        ->select(DB::Raw('DISTINCT session_id as orderid'))
        ->get();

        $count = count($total_temp_cart);

        $amount=0;$discount=0;$proid=0;
     
        foreach ($result as $val) {

            $amount=$val->unit_price;
            $discount=$val->discount;
            $proid=$val->id;

        //inserting record into the pos_temp_cart

           
            //checking product for adding multiple times with same product id
            $getTempCart = DB::table("ba_pos_temp_cart")
            ->where('session_id',session()->get('_cartid_'))
            ->where('bin',session()->get('_bin_'))
            ->where('prid',$proid)
            ->select()
            ->first();

            if ($getTempCart) {
                
                $update = PosCartTemporary::find($getTempCart->id);

                $update->qty = $getTempCart->qty+1;
                $productQuantity = $update->save();

                if (!$productQuantity) {
                     return redirect("product/enter-sales-details");
                }
                 
                 $data = array('product'=>$result );
                 $returnHTML = view('sales.productList')->with($data)->render(); 
                 return response()->json(['success'=>true,'html'=>$returnHTML,
                'status'=>'"'.$product_code.'" Added to Cart','amount'=>$amount,
                'discount'=>$discount,'prodcode'=>$product_code,'prid'=>$proid,
                'total_temp_cart'=>$total_temp_cart,'count'=>$count]);

            }
            //if found increment quantity and returns with json result
        
            $pos_cart = new PosCartTemporary;
            $pos_cart->session_id = session()->get('_cartid_');
            $pos_cart->bin = session()->get('_bin_');
            $pos_cart->prid = $val->id;
            $pos_cart->subid  = $val->id;
            $pos_cart->qty = 1;
            $pos_cart->cost = $val->actual_price;
            $pos_cart->order_date = date('Y-m-d');

            $save = $pos_cart->save();

            if (!$save) {
                
                return redirect("product/enter-sales-details");
            }


        //record insertion ends here        

    }

    if($result)
        
    {
         $data = array('product'=>$result );
         $returnHTML = view('sales.productList')->with($data)->render(); 
         return response()->json(['success'=>true,'html'=>$returnHTML,
        'status'=>'"'.$product_code.'" Added to Cart','amount'=>$amount,
        'discount'=>$discount,'prodcode'=>$product_code,'prid'=>$proid,
                'total_temp_cart'=>$total_temp_cart,'count'=>$count]);
    }
        

}


protected function deleteProductTempCart(Request $request)
{

    $id=$request->productid;
    $session_id = session()->get('_cartid_');
    $bin = session()->get('_bin_');



    $temp = DB::table('ba_pos_temp_cart')
    ->where('session_id',$session_id)
    ->where('bin',$bin)
    ->where('prid',$id)
    ->select()
    ->first();

    $delete = PosCartTemporary::find($temp->id);

    $deletetemp_data = $delete->delete();

    if ($deletetemp_data) {
         return response()->json(['success'=>true]);
    }

}








}
