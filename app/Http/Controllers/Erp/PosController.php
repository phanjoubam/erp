<?php

namespace App\Http\Controllers\Erp; 

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Contracts\Encryption\DecryptException;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\URL;



use App\Business; 
use App\ProductCategoryModel; 
use App\ProductModel;
use App\ShoppingOrderModel;
use App\ShoppingOrderItemsModel;
use App\AddressModel;
use App\MemberAddressModel;
use App\DeliveryOrderModel;
use App\ServiceBooking;
use App\OrderStatusLogModel; 
use App\CustomerProfileModel;
use App\PaymentDealingModel;
use App\PremiumBusiness;
use App\ProductImportedModel; 
use App\CloudMessageModel; 
use App\Imports\ProductImport;
use App\PosCartTemporary;
use App\Traits\Utilities;
use App\PosOrderModel;
use App\PosOrderSequencerModel;
use App\PosOrderBasketModel;

use PDF;


class PosController extends Controller
{
    //
	use Utilities; 
 
	public function downloadReceipt( Request $request)
		{

			$order_info = $order_items =null;
			if( $request->o  != ""  )
			{
				$order_info = SalesOrderModel::find( $request->o );
				$order_items = DB::Connection("servicesCon")
				->table("ms_sales_order_items")
				->where("order_no", $request->o )
				->get( );
			}
			else
			{
				return ;
			}

			$pdf_file =  "rcp" . $request->o . ".pdf";
			$save_path = public_path() . "/assets/ebills/"  .  $pdf_file    ;

		 		$pdf = PDF::setOptions(['defaultFont' => 'sans-serif'  ])
			  ->setPaper('a4', 'portrait')
			  ->loadView('retails.bill_image_thermal' ,  array('data' =>   $order_info , 'order_items' => $order_items )  )
				->save($save_path )  ;

 			$url = URL::to( "/public/assets/ebills/"  .  $pdf_file  )  ;

			$data= ["message" => "success", "status_code" =>  6001 , 'detailed_msg' =>   'Receipt generated.' , 'url' =>  $url ];
			return json_encode($data);


		}
 

	public function viewCustomerBill( Request $request)
	{

		$order_info = $order_items =null;
		if( $request->o  != "" || $request->p !=  "" )
		{
			$order_info = SalesOrderModel::find( $request->o );

			$order_items = DB::Connection("servicesCon")
			->table("ms_sales_order_items")
			->where("order_no", $request->o )
			->get( );
		}
		else
		{
			return ;
		}


		$pdf_file =  "ebill_" . $request->o . ".pdf";
		$save_path = public_path() . "/assets/ebills/"  .  $pdf_file    ;

	//	return view('retails.bill_image' , array('data' =>   $order_info , 'order_items' => $order_items ) )   ;

	/*
 		$pdf = PDF::setOptions(['defaultFont' => 'sans-serif'  ])
	  ->setPaper('a4', 'portrait')
	  ->loadView('retails.bill_image' ,  array('data' =>   $order_info , 'order_items' => $order_items )  )
		->save($save_path )  ;
*/


		$pdf = PDF::setOptions(['defaultFont' => 'sans-serif'  ])
		->setPaper('a4', 'portrait')
		->loadView('retails.bill_image' ,  array('data' =>   $order_info , 'order_items' => $order_items )  )  ;

		return $pdf->download($order_info->cust_phone  .  '.pdf');


		//$jpeg_file =  "ebill_" . $request->o . ".jpg";
	//	$image_path = public_path() . "/assets/ebills/"  .  $jpeg_file    ;



		return;


	}



	public function viewCustomerBillSmall( Request $request)
		{

			$order_info = $order_items =null;
			if( $request->o  != "" || $request->p !=  "" )
			{
				$order_info = SalesOrderModel::find( $request->o );
				$order_items = DB::Connection("servicesCon")
				->table("ms_sales_order_items")
				->where("order_no", $request->o )
				->get( );
			}
			else
			{
				return ;
			}

			$pdf_file =  "ebill_" . $request->o . time(). ".pdf";
			$save_path = public_path() . "/assets/ebills/"  .  $pdf_file    ;

			$pdf = PDF::setOptions(['defaultFont' => 'sans-serif'  ])
			->setPaper('a5', 'portrait')
			->loadView('retails.bill_image_thermal', array('data' =>   $order_info , 'order_items' => $order_items )  )  ;

			return $pdf->download($order_info->cust_phone  .  '.pdf');

			//$jpeg_file =  "ebill_" . $request->o . ".jpg";
			//$image_path = public_path() . "/assets/ebills/"  .  $jpeg_file    ;
			return;

		}
 	 


 	protected function prepareSalesWindow(Request $request)
    {
        $bin = $request->session()->get("_bin_"); 
        $uid  = $request->session()->get("__user_id_");  
        $cartno = 0; 
        try
        {
        	$cartno  =  Crypt::decrypt( $request->enccno ); 
        }
        catch (DecryptException $e)
        {
            
        } 


        $cart_items  = DB::table("ba_pos_temp_cart")   
        ->join("ba_product_variant", "ba_product_variant.prsubid", "=", "ba_pos_temp_cart.subid")  
        ->where("ba_pos_temp_cart.cart_no",  $cartno  ) 
        ->where("ba_pos_temp_cart.bin",  $bin ) 
        ->select(
        	"ba_pos_temp_cart.id as key",   
            "ba_pos_temp_cart.cart_no",  
            "ba_pos_temp_cart.subid", 
            "ba_pos_temp_cart.qty" , 
            "ba_pos_temp_cart.cost",
            DB::Raw("'na' product_name"),
            DB::Raw("'na' product_desc"), 
            "ba_product_variant.*" 
             )
        ->get();

        $prsubid = array();
        foreach($cart_items as $item)
        {
        	$prsubid[] = $item->subid; 
        } 
 		$prsubid[] =0;

 		$photos  = DB::table("ba_product_photos")   
        ->whereIn("prsubid",  $prsubid)
        ->get();

        $products  = DB::table("ba_products")   
        ->join("ba_product_variant", "ba_product_variant.prid", "=", "ba_products.id")  
        ->whereIn("ba_product_variant.prsubid",  $prsubid)
        ->select("ba_products.pr_name", "ba_products.description", "ba_product_variant.prsubid")
        ->get(); 

        $cdn_url =   config('app.app_cdn') ;  
        $cdn_path =  config('app.app_cdn_path'); // cnd - /var/www/html/api/public 
        $cms_base_url = config('app.app_cdn')  ;  
        $cms_base_path = $cdn_path  ;  
        $no_image  =   $cdn_url .  "/assets/image/no-image.jpg";  
 
       $theme = DB::table("ba_theme")
        ->first();

        $total_temp_cart = DB::table('ba_pos_temp_cart')
        ->where('bin', $bin )
        ->select(DB::Raw('DISTINCT cart_no as cartNo'))
        ->get();


        $count = count($total_temp_cart); 
 	 
 		if( $cartno == 0 )
 		{
 			//generate new cart 
 			$new_cartno =  DB::table('ba_pos_temp_cart')
	        ->where('bin', $bin )
	        ->max('cart_no')  ;
	        $new_cartno = ($new_cartno == "") ? 1 : ( $new_cartno + 1) ;  
 		}
 		else 
 		{
 			$cartno == 1;
 			$new_cartno =1;
 		} 


 		$data = array('total_temp_cart'=>$total_temp_cart,'count'=>$count, 
        	'cartno' => $cartno, 
        	'new_cartno' => $new_cartno ,
        	'bin' => $bin,
        	'products' => $products, 
        	'cart_items' => $cart_items, 'photos' =>$photos );
		
		return view('sales.sales_entry_details')
        ->with('data',$data)
        ->with('theme',$theme); 
    }


    protected function checkoutOrder(Request $request)
    {
        $bin = $request->session()->get("_bin_"); 
        $uid  = $request->session()->get("__user_id_");  
        $cartno = 0; 
        try
        {
        	$cartno  =  Crypt::decrypt( $request->enccno ); 
        }
        catch (DecryptException $e)
        {
            
        } 


        $cart_items  = DB::table("ba_pos_temp_cart")   
        ->join("ba_product_variant", "ba_product_variant.prsubid", "=", "ba_pos_temp_cart.subid")  
        ->where("ba_pos_temp_cart.cart_no",  $cartno  ) 
        ->where("ba_pos_temp_cart.bin",  $bin ) 
        ->select(
        	"ba_pos_temp_cart.id as key",   
            "ba_pos_temp_cart.cart_no",  
            "ba_pos_temp_cart.subid", 
            "ba_pos_temp_cart.qty" , 
            "ba_pos_temp_cart.cost",
            DB::Raw("'na' product_name"),
            DB::Raw("'na' product_desc"), 
            "ba_product_variant.*" 
             )
        ->get();

        $prsubid = array();
        foreach($cart_items as $item)
        {
        	$prsubid[] = $item->subid; 
        } 
 		$prsubid[] =0;

 		$photos  = DB::table("ba_product_photos")   
        ->whereIn("prsubid",  $prsubid)
        ->get();

        $products  = DB::table("ba_products")   
        ->join("ba_product_variant", "ba_product_variant.prid", "=", "ba_products.id")  
        ->whereIn("ba_product_variant.prsubid",  $prsubid)
        ->select("ba_products.pr_name", "ba_products.description", "ba_product_variant.prsubid")
        ->get(); 

        $cdn_url =   config('app.app_cdn') ;  
        $cdn_path =  config('app.app_cdn_path'); // cnd - /var/www/html/api/public 
        $cms_base_url = config('app.app_cdn')  ;  
        $cms_base_path = $cdn_path  ;  
        $no_image  =   $cdn_url .  "/assets/image/no-image.jpg";  
 
       $theme = DB::table("ba_theme")
        ->first();

        $total_temp_cart = DB::table('ba_pos_temp_cart')
        ->where('bin', $bin )
        ->select(DB::Raw('DISTINCT cart_no as cartNo'))
        ->get();


        $count = count($total_temp_cart); 
 	 
 		if( $cartno == 0 )
 		{
 			//generate new cart 
 			$new_cartno =  DB::table('ba_pos_temp_cart')
	        ->where('bin', $bin )
	        ->max('cart_no')  ;
	        $new_cartno = ($new_cartno == "") ? 1 : ( $new_cartno + 1) ;  
 		}
 		else 
 		{
 			$cartno == 1;
 			$new_cartno =1;
 		} 


 		$data = array(
            'total_temp_cart'=>$total_temp_cart,
            'count'=>$count, 
        	'cartno' => $cartno, 
        	'new_cartno' => $new_cartno ,
        	'bin' => $bin,
        	'products' => $products, 
        	'cart_items' => $cart_items, 
            'photos' =>$photos );
		
		return view('sales.sales_checkout')
        ->with('data',$data)
        ->with('theme',$theme);


    }



	protected function postSalesWindowTouchAndGo2(Request $request)
    {

    	$bin = $request->session()->get("_bin_"); 
        $uid  = $request->session()->get("__user_id_");  
        $cartno = 0; 
        try
        {
        	$cartno  =  Crypt::decrypt( $request->enccno ); 
        }
        catch (DecryptException $e)
        {
            
        }

        $cart_items  = DB::table("ba_pos_temp_cart")   
        ->join("ba_product_variant", "ba_product_variant.prsubid", "=", "ba_pos_temp_cart.subid")  
        ->where("ba_pos_temp_cart.cart_no",  $cartno  )
        ->where("ba_pos_temp_cart.bin",  $bin )  
        ->select("ba_pos_temp_cart.id as key",   
            "ba_pos_temp_cart.cart_no",  
            "ba_pos_temp_cart.subid", 
            "ba_pos_temp_cart.qty" , 
            "ba_pos_temp_cart.cost",
            DB::Raw("'na' product_name"),
            DB::Raw("'na' product_desc"), 
            "ba_product_variant.*" )
        ->get();

        $prsubid = array();
        foreach($cart_items as $item)
        {
        	$prsubid[] = $item->subid; 
        } 
 		$prsubid[] =0;

 		$photos  = DB::table("ba_product_photos")   
        ->whereIn("prsubid",  $prsubid)
        ->get();
        $products  = DB::table("ba_products")   
        ->join("ba_product_variant", "ba_product_variant.prid", "=", "ba_products.id")  
        ->whereIn("ba_product_variant.prsubid",  $prsubid)
        ->select("ba_products.pr_name", "ba_products.description", "ba_product_variant.prsubid")
        ->get();
        $cdn_url =   config('app.app_cdn') ;  
        $cdn_path =  config('app.app_cdn_path'); // cnd - /var/www/html/api/public 
        $cms_base_url = config('app.app_cdn')  ;  
        $cms_base_path = $cdn_path  ;  
        $no_image  =   $cdn_url .  "/assets/image/no-image.jpg";  
        $theme = DB::table("ba_theme")
        ->first();

        $total_temp_cart = DB::table('ba_pos_temp_cart')
        ->where('bin', $bin )
        ->select(DB::Raw('DISTINCT cart_no as cartNo'))
        ->get();


        $count = count($total_temp_cart); 
 	 
 		if( $cartno == 0 )
 		{
 			//generate new cart 
 			$new_cartno =  DB::table('ba_pos_temp_cart')
	        ->where('bin', $bin )
	        ->max('cart_no')  ;
	        $new_cartno = ($new_cartno == "") ? 1 : ( $new_cartno + 1) ;  
 		}
 		else 
 		{
 			$cartno == 1;
 			$new_cartno =1;
 		}
 

 		$all_products = DB::table("ba_products")   
        ->join("ba_product_variant", "ba_product_variant.prid", "=", "ba_products.id")   
        ->where("ba_products.bin",  $bin) 
        ->select("ba_products.pr_name", "ba_products.description", "ba_product_variant.*")
        ->paginate(20); 

        $allprsubids = array();
        foreach($all_products as $item)
        {
        	$allprsubids[] = $item->prsubid; 
        } 
 		$allprsubids[] =0;

 		$all_photos  = DB::table("ba_product_photos")   
        ->whereIn("prsubid",  $allprsubids)
        ->get();

        $categories  = DB::table("ba_products")   
        ->where("bin",  $bin)
        ->select(DB::Raw("distinct category"))
        ->get();



 		$data = array('total_temp_cart'=>$total_temp_cart,'count'=>$count, 
        	'cartno' => $cartno, 
        	'new_cartno' => $new_cartno ,
        	'bin' => $bin,
        	'products' => $products, 
        	'categories'=>$categories,
        	'all_products' => $all_products,
        	'all_photos' =>$all_photos,
        	'cart_items' => $cart_items, 'photos' =>$photos );
		
		return view('sales.pos_touch_and_go_sale')
        ->with('data',$data)
        ->with('theme',$theme);


    }
 	

    //modern look
 	protected function postSalesWindowTouchAndGo(Request $request)
    {

    	$bin = $request->session()->get("_bin_"); 
        $uid  = $request->session()->get("__user_id_");  
        $cartno = 0; 
        try
        {
        	$cartno  =  Crypt::decrypt( $request->enccno ); 
        }
        catch (DecryptException $e)
        {
            
        }

        $cart_items  = DB::table("ba_pos_temp_cart")   
        ->join("ba_product_variant", "ba_product_variant.prsubid", "=", "ba_pos_temp_cart.subid")  
        ->where("ba_pos_temp_cart.cart_no",  $cartno  )
        ->where("ba_pos_temp_cart.bin",  $bin )  
        ->select("ba_pos_temp_cart.id as key",   
            "ba_pos_temp_cart.cart_no",  
            "ba_pos_temp_cart.subid", 
            "ba_pos_temp_cart.qty" , 
            "ba_pos_temp_cart.cost",
            DB::Raw("'na' product_name"),
            DB::Raw("'na' product_desc"), 
            "ba_product_variant.*" )
        ->get();

        $prsubid = array();
        foreach($cart_items as $item)
        {
        	$prsubid[] = $item->subid; 
        } 
 		$prsubid[] =0;

 		$photos  = DB::table("ba_product_photos")   
        ->whereIn("prsubid",  $prsubid)
        ->get();


        $products  = DB::table("ba_products")   
        ->join("ba_product_variant", "ba_product_variant.prid", "=", "ba_products.id")  
        ->whereIn("ba_product_variant.prsubid",  $prsubid)
		->where("ba_products.bin",  $bin )  
        ->select("ba_products.pr_name", "ba_products.description", "ba_product_variant.prsubid")
        ->get();



        $cdn_url =   config('app.app_cdn') ;  
        $cdn_path =  config('app.app_cdn_path'); // cnd - /var/www/html/api/public 
        $cms_base_url = config('app.app_cdn')  ;  
        $cms_base_path = $cdn_path  ;  
        $no_image  =   $cdn_url .  "/assets/image/no-image.jpg";  
        $theme = DB::table("ba_theme")
        ->first();

        $total_temp_cart = DB::table('ba_pos_temp_cart')
        ->where('bin', $bin )
        ->select(DB::Raw('DISTINCT cart_no as cartNo'))
        ->get();


        $count = count($total_temp_cart);
 		if( $cartno == 0 )
 		{
 			//generate new cart 
 			$new_cartno =  DB::table('ba_pos_temp_cart')
	        ->where('bin', $bin )
	        ->max('cart_no')  ;
	        $new_cartno = ($new_cartno == "") ? 1 : ( $new_cartno + 1) ;  
 		}
 		else 
 		{
 			$cartno == 1;
 			$new_cartno =1;
 		}
  
 		$all_products = DB::table("ba_products")   
        ->join("ba_product_variant", "ba_product_variant.prid", "=", "ba_products.id")   
        ->where("ba_products.bin",  $bin)  
        ->select("ba_products.pr_name", "ba_products.description", "ba_product_variant.*")
        ->paginate(30); 

        $allprsubids = array();
        foreach($all_products as $item)
        {
        	$allprsubids[] = $item->prsubid; 
        } 
 		$allprsubids[] =0;

 		$all_photos  = DB::table("ba_product_photos")   
        ->whereIn("prsubid",  $allprsubids)
        ->get();

        $categories  = DB::table("ba_products")   
        ->where("bin",  $bin)
        ->select(DB::Raw("distinct category"))
        ->get();
 

 		$data = array('total_temp_cart'=>$total_temp_cart,'count'=>$count, 
        	'cartno' => $cartno, 
        	'new_cartno' => $new_cartno ,
        	'bin' => $bin,
        	'products' => $products, 
        	'categories'=>$categories,
        	'all_products' => $all_products,
        	'all_photos' =>$all_photos,
        	'cart_items' => $cart_items, 
        	'photos' =>$photos,
        	'pagesize' => count($all_products) );
		
		return view('sales.pos_touch_and_go_sale_2')
        ->with('data',$data)
        ->with('theme',$theme);
 
    }


    protected function removeItemFromSalesWindow(Request $request)
    {
        $bin = $request->session()->get("_bin_"); 
        $uid  = $request->session()->get("__user_id_");  
           
        $cartno =  1; 
        try
        {
            $cartno  =  Crypt::decrypt( $request->enccno ); 
        }
        catch (DecryptException $e)
        {
           return redirect("/pos/prepare-sales-window"  )->with("err_msg", "No cart selected!");  
        }

        if($request->key == "")
        {
        	return redirect("/pos/prepare-sales-window?enccno=" . $request->enccno); 
	    }

	    //removing selected item from shopping cart
	    DB::table("ba_pos_temp_cart")  
        ->where("id", $request->key) 
        ->delete();
 		
 		//redirect to same cart
 		return redirect("/pos/prepare-sales-window?enccno=" . $request->enccno); 
    }


	protected function removeTemporaryCart(Request $request)
    {
        $bin = $request->session()->get("_bin_"); 
        $uid  = $request->session()->get("__user_id_");  

        if($request->uitype == "modern")
 		{
 			$redirect =   "/pos/pos-sales-window-touch-and-go" ;  			
 		}
 		else 
 		{
 			$redirect =  "/pos/prepare-sales-window" ; 
 		}


        $cartno =  1; 
        try
        {
            $cartno  =  Crypt::decrypt( $request->enccno ); 
        }
        catch (DecryptException $e)
        {
           return redirect( $redirect  )->with("err_msg", "No cart selected!");  
        }

        
	    //removing selected item from shopping cart
	    DB::table("ba_pos_temp_cart")  
        ->where("cart_no",  $cartno ) 
        ->where("bin",  $bin ) 
        ->delete();

        //find the minimum cart no 
        $mincartno = DB::table("ba_pos_temp_cart")    
        ->where("bin",  $bin ) 
        ->min("cart_no");

         $new_cartno = ($mincartno == "") ? 1 : ( $mincartno  ) ;  

 		
 		//redirect to same cart
 		return redirect( $redirect . "?enccno=" . Crypt::encrypt( $new_cartno ) ) ;  
 		
    } 

    protected function placePosOrder(Request $request)
	{
		$bin = $request->session()->get("_bin_"); 
        $uid  = $request->session()->get("__user_id_");   
        try
        {
            $cartno  =  Crypt::decrypt( $request->enccno ); 
        }
        catch (DecryptException $e)
        {
            $cartno =  1; 
        } 
 
        $today =  date('Y-m-d H:i:s' );
        $servicedate =  date('Y-m-d', strtotime($request->date ));
        $cname = $request->cname ;
        $cphone = $request->cphone ;
        $caddress = $request->caddress ;  
        
        if ($request->btnorder == "placeorder") 
        {

			//find automatic sequence no. for merchant 
			$max_orderno = DB::table('ba_pos_order_sequencer')
			->where("bin", $bin )
			->max('next_seq_no'); 
			//generate order no
			$next_orderno = $max_orderno+1; 


        	$cart_items  = DB::table("ba_pos_temp_cart")   
	        ->join("ba_product_variant", "ba_product_variant.prsubid", "=", "ba_pos_temp_cart.subid")  
	        ->where("ba_pos_temp_cart.cart_no",  $cartno  )
	        ->where("ba_pos_temp_cart.bin",  $bin )  
	        ->select(
	        	"ba_pos_temp_cart.id as key",   
	            "ba_pos_temp_cart.cart_no",  
	            "ba_pos_temp_cart.bin",  
	            "ba_pos_temp_cart.subid", 
	            "ba_pos_temp_cart.qty" , 
	            "ba_pos_temp_cart.cost", 
	            "ba_product_variant.*" 
	             )
	        ->get();

	        $prsubid = array();
	        foreach($cart_items as $item)
	        {
	        	$prsubid[] = $item->subid; 
	        } 
	 		$prsubid[] =0; 

	        $products  = DB::table("ba_products")   
	        ->join("ba_product_variant", "ba_product_variant.prid", "=", "ba_products.id")  
	        ->whereIn("ba_product_variant.prsubid",  $prsubid)
	        ->select("ba_products.pr_name", "ba_products.description", "ba_product_variant.prsubid", "ba_product_variant.unit_price")
	        ->get();

	        $new_order = new  PosOrderModel; 
	        $new_order->bin = $bin;   
			$new_order->biz_order_no = $next_orderno;
	        $new_order->book_date = $today;
	        $new_order->service_date = $servicedate;
	        $new_order->staff_id =$uid;  
			$new_order->customer_name = ($cname != "" ) ? $cname :  "not provided";  
			$new_order->customer_phone = ($cphone != "" ) ? $cphone :  "not provided";  
	        $new_order->address =  ($caddress != "" ) ? $caddress :  "not provided";  
	        $new_order->book_status =  ($servicedate==$today) ? "completed" : "new"; 
	        $new_order->extra_cost =  $request->extracost ;  
	        $new_order->service_fee  = (0.005 *  $request->tbtotalcost ) ;  //0.005% 
			$new_order->discount =  $request->tbdiscount ;    

			$cgst = $request->tbcgst;
	        $sgst = $request->tbsgst;
	        $totaltax = $cgst + $sgst ; 
	        $totalcost = $request->tbtotalcost;
	        if($totaltax > 0 )
	        { 
		        $cost_notax =  ($totalcost * 100 / (100 + $totaltax) );
		    }
		    else 
		    {
		    	$cost_notax =$totalcost;
		    }
		    $new_order->total_cost = $totalcost ; 
		    $new_order->cgst =   ( $totalcost - $cost_notax) / 2 ;
		    $new_order->sgst =   ( $totalcost - $cost_notax) / 2 ;  
			$new_order->payment_type =  $request->paymode ; 
			//$new_order->transaction_no =  ( $request->transactionId == "" ) ? $request->transactionId :  null;
	        $new_order->cust_remarks =  $request->remarks ;  
	        $save = $new_order->save();

	        if($save)
	        {
	        	//saving basket 
		        foreach ($cart_items as $item)
	            {
	            	$items = new PosOrderBasketModel;
		        	$items->order_no =  $new_order->id;
					$items->prsubid = $item->subid ;  
		        	$items->pr_name = "na";
		        	$items->description ="na";	
	            	foreach ($products as $product)
		            {
		            	if($product->prsubid  == $item->subid )
		            	{
		            		$items->pr_name = $product->pr_name;
		            		$items->description = $product->description; 
		            		$items->unit_price = $product->unit_price;
		            		break;	            		
		            	}
		            } 
	            	//ading items 
					$items->qty  = $item->qty;
					$items->price  = $item->cost ; 
					$items->unit  = $item->unit_name; 
					$items->package_charge = $item->packaging; 
					$items->save();  

					//deduct stock level 
				    DB::table("ba_product_variant")    
				    ->where("prsubid",  $item->subid )
				    ->decrement('stock_inhand' ,  $item->qty); 
	            }
	            
				
				//clear pos cache 
	            DB::table("ba_pos_temp_cart")  
	            ->where("ba_pos_temp_cart.cart_no",  $cartno  )  
	            ->where("ba_pos_temp_cart.bin",   $bin )
		        ->delete(); 
				//clear pos order no cache 
				DB::table("ba_pos_order_sequencer")  
	            ->where("bin",  $bin  )   
		        ->delete();
				//insert next order no
				$sequencer = new PosOrderSequencerModel;
				$sequencer->bin = $bin ; 
				$sequencer->next_seq_no = $next_orderno;
				$sequencer->save(); 

	        }
	        else 
	        {
	        	return redirect("/pos/view-sales-orders"   )
	        	->with("err_msg", "Order placement failed. Please retry!");
	        } 
        }
        return redirect('/pos/view-sales-orders');
	}




	protected function viewPosSalesOrders(Request $request)
	{
  
		$bin = $request->session()->get("_bin_"); 
		$all_orders = DB::table("ba_pos_order") 
		->where("ba_pos_order.bin",$bin) 
		->orderBy("id", "desc")
		->get(); 
		 
		$theme = DB::table("ba_theme")
    	->first();

    	$data = array( 
    		'all_orders'=>$all_orders,
    		'theme'=>$theme
      	);
 
		return view('sales.view_pos_orders')->with($data);

	}


	protected function viewPosOrdersDetails(Request $request)
	{
		$bin = $request->session()->get("_bin_"); 
		$orderno = $request->key; 
		$business = DB::table("ba_business")
		->where("id",$bin)
		->first();

		$order_info = DB::table("ba_pos_order")
		->where("id", $orderno ) 
		->first(); 

		if( !isset($order_info))
		{
			return redirect("/pos/view-sales-orders"  )->with("err_msg", "No order was selected!"); 
		}

		if(  $order_info->bin != $bin )
		{
			return redirect("/pos/view-sales-orders"  )->with("err_msg", "Selected order does not belong to your shop!"); 
		}


		$all_orders = DB::table("ba_pos_order_basket")
		->where("order_no",  $orderno ) 
		->get();


		$theme = DB::table("ba_theme")
    	->first();

    	$data = array(
    		'business'=>$business,
    		'all_orders'=>$all_orders, 
    		'order'=>$order_info,
    		'theme'=>$theme
      	);

      	return view('sales.view_pos_orders_details')->with($data);

	}


	protected function downloadPosReceipt(Request $request)
	{
		$bin = $request->session()->get("_bin_"); 
		$orderno = $request->o; 
		$business = DB::table("ba_business")
		->where("id",$bin)
		->first();

		$order_info = DB::table("ba_pos_order")
		->where("id", $orderno ) 
		->first(); 

		if( !isset($order_info))
		{
			return redirect("/pos/view-sales-orders"  )->with("err_msg", "No order was selected!"); 
		}

		if(  $order_info->bin != $bin )
		{
			return redirect("/pos/view-sales-orders"  )->with("err_msg", "Selected order does not belong to your shop!"); 
		}

		$all_orders = DB::table("ba_pos_order_basket")
		->where("order_no",$order_info->id) 
		->get();
 

	    	$data = array(
	    		'business'=>$business,
	    		'all_orders'=>$all_orders, 
	    		'order'=>$order_info 
	      	);  

            return view('sales.ebill_pos')->with($data);
  	

         $pdf_file =  "erp_".time().'_'.date('Y-m-d').".pdf";
         $save_path = public_path() . "/assets/erp/bills/".$pdf_file;

         $pdf = PDF::setOptions(['defaultFont' => 'sans-serif'  ])
                ->setPaper('a4', 'portrait')
                ->loadView('sales.ebill_pos' ,$data  )   ;
               return $pdf->download( $pdf_file  );  
            

    }




	//WEB API
 	protected function addItemToCart(Request $request)
	{

		if(  $request->subid == "" ||  $request->guid  == ""  ||  $request->gbin  == ""  ||  $request->cartno  == ""  )
	    {
	       $data= ["message" => "failure", "status_code" =>  902 , 'detailed_msg' => 'Missing data to perform action.'  ];
	       return json_encode($data);
	    }
 

	    $uid = $bin = 0 ;
	    try
	    {
	         $uid =  Crypt::decrypt( $request->guid );
	         $bin =  Crypt::decrypt( $request->gbin );
	    }
	    catch (DecryptException $e)
	    {
	        $data= ["message" => "failure", "status_code" =>  999 , 'detailed_msg' =>   'Invalid access!'  ];
	        return json_encode($data);
	    }

	    //calculating cost
        $product_info = DB::table("ba_products")
        ->join("ba_product_variant", "ba_product_variant.prid", "=", "ba_products.id") 
        
        ->whereRaw("( ba_product_variant.prsubid = ? or ba_product_variant.barcode =?)" , 
            [$request->subid, $request->subid] ) 

        ->where("ba_products.bin", $bin)
        
        ->select("ba_products.bin", "ba_product_variant.prsubid", 
            "ba_product_variant.actual_price",  "ba_products.pr_name", "ba_products.description")
        ->first() ;


        if( !isset($product_info))
        {
          $data= ["message" => "failure", "status_code" =>  999 , 'detailed_msg' =>   'Selected product is not inventoried. Please add product information first.'  ];
	        return json_encode($data);
        } 

        //check if there is already an existing carts  

	    $cart_item = PosCartTemporary::where( "bin",  $bin  )
	    ->where( "subid",  $product_info->prsubid ) 
	    ->where( "cart_no",  $request->cartno ) 
	    ->first();


	    $current_qty = 1;

	    if( !isset(  $cart_item )  )
	    { 
	    	$cart_item = new PosCartTemporary; 
	        $cart_item->subid = $product_info->prsubid;
	        $cart_item->bin = $bin;
	        $cart_item->qty =  $current_qty ;
	    }
	    else 
	    {
	    	if( $request->action == "del" || $request->action == "rem")
	        {
	        	$current_qty = $cart_item->qty -1 ;
	        	if( $current_qty <= 0 )
				  {
					  //remove item
					  $cart_item->delete(); 
					  $data= ["message" => "success", "status_code" =>  6001 ,   'detailed_msg' => 'Item qty in cart updated.' ,
					  'enccno' => Crypt::encrypt( $request->cartno) ];
					   return json_encode( $data); 
				  }
				  else 
				  {
					  $cart_item->qty =$current_qty;
				  }
	        }
			else
			{ 
				if($request->action == "add")
				{
					$current_qty = $cart_item->qty + 1 ;
					$cart_item->qty =$current_qty;
				}
			}
	    }
	    $cart_item->cart_no =  $request->cartno ;
	    $cart_item->cost  =  $cart_item->qty * $product_info->actual_price ; 
        $save = $cart_item->save(); 
	 
	
        if($save)
        {
          $data= ["message" => "success", "status_code" =>  6001 , 
		  'detailed_msg' => 'Item added to cart.' ,
		  'enccno' => Crypt::encrypt( $request->cartno) ];
        }
        else
        {
        	$data= ["message" => "failure", "status_code" =>  6002 , 
			'detailed_msg' =>  "Item could not be added to cart." ,
		  'enccno' => Crypt::encrypt( $request->cartno) ];
        }  
        return json_encode( $data); 
	}



	protected function searchItemByKeyword(Request $request)
	{

		if(  $request->keyword == "" ||  $request->guid  == ""  ||  $request->gbin  == ""   )
	    {
	       $data= ["message" => "failure", "status_code" =>  902 , 'detailed_msg' => 'Missing data to perform action.'  ];
	       return json_encode($data);
	    }

	    $uid = $bin = 0 ;
	    try
	    {
	         $uid =  Crypt::decrypt( $request->guid );
	         $bin =  Crypt::decrypt( $request->gbin );
	    }
	    catch (DecryptException $e)
	    {
	        $data= ["message" => "failure", "status_code" =>  999 , 'detailed_msg' =>   'Invalid access!'  ];
	        return json_encode($data);
	    }

        $search_key = $request->keyword;

        if (get_magic_quotes_gpc())
        {
            $search_key = stripslashes(  $search_key  );
        }

        $search_key = str_replace("'", "''", $search_key );   


	   
		    //calculating cost


	        $all_products = DB::table("ba_products")
	        ->join("ba_product_variant", "ba_product_variant.prid", "=", "ba_products.id")
	        ->whereRaw(" ( ba_products.pr_name like '%" .  $search_key. "%' or ba_products.description like '%" . $search_key . "%' or ba_products.tags like '%" . $search_key  . "%' )")
	        ->where("ba_products.bin",  $bin ) 
	        ->select("ba_products.bin", "ba_products.pr_name as name", "ba_products.description", 
	        	"ba_product_variant.prsubid", "ba_product_variant.actual_price as price",  "ba_product_variant.unit_price", 
	        	"ba_product_variant.stock_inhand as stock", 
	        	DB::Raw("'na' photo"))
	        ->paginate(20); 
	    
 

        $allprsubids = array();
        foreach($all_products as $item)
        {
        	$allprsubids[] = $item->prsubid; 
        } 
 		$allprsubids[] =0;

 		$all_photos  = DB::table("ba_product_photos")   
        ->whereIn("prsubid",  $allprsubids)
        ->get();

        $cdn_url =   config('app.app_cdn') ;    
        $img_url = $cdn_url .  "/assets/image/no-image.jpg";  
        foreach($all_products as $item)
        {
        	foreach($all_photos as $photo)
	        {
	        	if($item->prsubid == $photo->prsubid)
	        	{
	        		$img_url = $photo->image_url;
	        		break;
	        	} 
	        }

	        $item->photo =$img_url;
        }

        if($all_products->count() >0 )
        {
        	$data= ["message" => "success", "status_code" =>  6007 , 
		  'detailed_msg' => 'Matching products are fetched.' ,
		  'results' =>  $all_products->items() ];
        }
        else 
        {
        	$data= ["message" => "success", "status_code" =>  6008, 
		  'detailed_msg' => 'No matching products found.'  ];	
        }
         
        
        return json_encode( $data); 
	}

	 



	protected function loadAllProducts(Request $request)
	{

		if(    $request->guid  == ""  ||  $request->gbin  == ""   )
	    {
	    	$data= ["message" => "failure", "status_code" =>  902 , 'detailed_msg' => 'Missing data to perform action.'  ];
	        return json_encode($data);
	    } 

	    $uid = $bin = 0 ;
	    try
	    {
	         $uid =  Crypt::decrypt( $request->guid );
	         $bin =  Crypt::decrypt( $request->gbin );
	    }
	    catch (DecryptException $e)
	    {
	        $data= ["message" => "failure", "status_code" =>  999 , 'detailed_msg' =>   'Invalid access!'  ];
	        return json_encode($data);
	    }

	    $category = $request->category; 

        $search_key = $request->keyword;

        if (get_magic_quotes_gpc())
        {
            $search_key = stripslashes(  $search_key  );
        }

        $search_key = str_replace("'", "\'", $search_key );   

        //$search_key = preg_replace('/[^A-Za-z0-9\-]/', ' ',  $request->keyword ) ;


	    $all_products = DB::table("ba_products")
	        ->join("ba_product_variant", "ba_product_variant.prid", "=", "ba_products.id")
	        ->whereRaw(" ( ba_products.pr_name like '%" . $search_key . "%' or ba_products.description like '%" . 
               $search_key . "%' or ba_products.tags like '%" . $search_key . "%' )")
	        ->where("ba_products.bin",  $bin )  
	        ->select("ba_product_variant.prsubid as id" , "ba_products.pr_name as name",   DB::Raw("'na' photo"))
	        ->get();

	    $allprsubids = array();
        foreach($all_products as $item)
        {
        	$allprsubids[] = $item->id; 
        } 
 		$allprsubids[] =0;

 		$all_photos  = DB::table("ba_product_photos")   
        ->whereIn("prsubid",  $allprsubids)
        ->get();

        $cdn_url =   config('app.app_cdn') ;    
        $img_url = $cdn_url .  "/assets/image/no-image.jpg";  
        foreach($all_products as $item)
        {
        	foreach($all_photos as $photo)
	        {
	        	if($item->id == $photo->prsubid)
	        	{
	        		$img_url = $photo->image_url;
	        		break;
	        	} 
	        }

	        $item->photo =$img_url;
        } 

	   	$data= ["message" => "success", "status_code" =>  6009 ,  'detailed_msg' => 'Matching products are fetched.' , 
	   	'results' =>  $all_products ];
	   	return json_encode( $data); 
	}
}
