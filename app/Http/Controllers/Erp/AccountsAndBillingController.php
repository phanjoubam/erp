<?php

namespace App\Http\Controllers\Erp;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Session;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Support\Facades\File;
use Maatwebsite\Excel\Facades\Excel; 
use App\Libraries\FirebaseCloudMessage; 
use Jenssegers\Agent\Agent;


use App\PickAndDropRequestModel;
use App\Business; 
use App\BusinessCategory;
use App\ProductCategoryModel; 
use App\ProductModel;
use App\ShoppingOrderModel;
use App\ShoppingOrderItemsModel;
use App\AddressModel;
use App\MemberAddressModel;
use App\DeliveryOrderModel;
use App\ServiceBooking;
use App\OrderStatusLogModel; 
use App\CustomerProfileModel;
use App\PaymentDealingModel;
use App\PremiumBusiness;
use App\ProductImportedModel; 
use App\CloudMessageModel;
use App\ProductPromotion;
use App\GlobalSettingsModel;
use App\SliderModel; 
use App\StaffAttendanceModel;
use App\CouponModel;
use App\OrderSequencerModel;
use App\Imports\ProductImport; 
use App\BusinessMenu;
use App\AccountsLog;
use App\CmsBannerSlidersModel;
use App\CmsProductsListedModel;
use App\CmsProductGroupsModel;
use App\OrderRemarksModel;
use App\AgentCacheModel;
use App\StaffSalaryModel;
use App\DailyExpenseModel;
use App\ComplaintModel;



use App\Traits\Utilities; 
use PDF;
class AccountsAndBillingController extends Controller
{
   public function dailySalesReport(Request $request )
    {
        if($request->todayDate != "")
        {
            $todayDate  =  date('Y-m-d' , strtotime($request->todayDate)) ; 
        }
        else
        {
            $todayDate  = date('Y-m-d' );
        }

        $totalEarning = 0;
 
        $bin = $request->session()->get('_bin_');

        $first = DB::table('ba_service_booking')
        ->join('ba_business', 'ba_service_booking.bin', '=', 'ba_business.id')
        ->wheredate('ba_service_booking.service_date', $todayDate)
        ->whereIn("book_status", array('completed', 'delivered') ) 
        ->where("ba_business.frno", "0")
        ->where("ba_business.id",$bin) 
        ->select('ba_service_booking.id', 
          DB::Raw("'customer' source"), 
          "ba_business.id as bin" ,   
          'total_cost as orderCost', 
          DB::Raw("'0.00' packingCost"), 
          'delivery_charge as deliveryCharge', 
          'discount',  'payment_type  as paymentType',
          'book_status as bookingStatus', 
          'ba_business.name', "ba_business.category", 
          "ba_business.commission",
          DB::Raw("'booktou' paymentTarget"),
          DB::Raw("'normal' orderType")) ;

        $second = DB::table('ba_pick_and_drop_order')
        ->join('ba_users', 'ba_pick_and_drop_order.request_by', '=', 'ba_users.profile_id')
        ->wheredate('ba_pick_and_drop_order.service_date', $todayDate)
        ->whereIn("book_status", array('completed', 'delivered') ) 
        ->where("request_from", "customer")
        ->where("ba_users.franchise", "0") 
        ->where("ba_users.bin", $bin)
        ->select('ba_pick_and_drop_order.id',  
          "source",
          DB::Raw("'0' bin"),
          'total_amount as orderCost', 
          'packaging_cost as packingCost',  
          'service_fee as deliveryCharge', 
          DB::Raw("'0' discount") ,  
          'pay_mode as paymentType', 
          'book_status  as bookingStatus',  
          'pickup_name as name', 
          DB::Raw("'customer' category"), 
          DB::Raw("'0' commission"),       
          "payment_target as paymentTarget",
          DB::Raw("'assist' orderType"))
        ->orderBy("id", "asc")   ;



        $result = DB::table('ba_pick_and_drop_order')
        ->join('ba_business', 'ba_pick_and_drop_order.request_by', '=', 'ba_business.id')
        ->wheredate('ba_pick_and_drop_order.service_date', $todayDate)
        ->whereIn("book_status", array('completed', 'delivered')) 
        ->where("request_from", "business")
        ->where("ba_business.frno", "0")
        ->where("ba_business.id",$bin) 
        ->union($first) 
        ->union($second) 
        ->select('ba_pick_and_drop_order.id',  
          "source","ba_business.id as bin",
          'total_amount as orderCost', 
          'packaging_cost as packingCost',  
          'service_fee as deliveryCharge', 
          DB::Raw("'0' discount") ,  'pay_mode as paymentType', 
          'book_status  as bookingStatus',  
          'ba_business.name', "ba_business.category", 
          "ba_business.commission",          
          "payment_target as paymentTarget",
          DB::Raw("'pnd' orderType"))
        ->orderBy("id", "asc")  
        ->get();


        $oids =  $bins = array();
        foreach($result as $item)
        {
            if($item->orderType == "normal")
            {
                $oids[] = $item->id ;  
            }
            $bins[] = $item->bin ; 
        }
        $bins[] = $oids[]= 0;

        $normal_sales_cart = DB::table("ba_shopping_basket") 
        ->whereIn("order_no",  $oids  )  
        ->get();  
 
 
        $all_business  = DB::table("ba_business")  
        ->whereIn("id", $bins ) 
        ->get(); 
  

        $theme = DB::table("ba_theme")
      ->first();

        $data = array(  'sales' =>  $result , 
            'cart_items' =>$normal_sales_cart,  
            'date' =>  $todayDate,
            'businesses' => $all_business,'theme'=>$theme );  

        return view("billing.sales_report_per_day")->with(  $data);  
    }

    protected function cyclewiseSalesServiceReport(Request $request )
    {
         
        if($request->year != "")
        {
            $year  = $request->year ; 
        }
        else
        {
            $year  = date('Y' );
        }
 

        if($request->month !="" )
        {
          $month = $request->month ;
        }
        else
        {
            $month = date('m' );
        }

        $cycle = 1;
        if($request->cycle == "" ||  $request->cycle  == 1 )
        {
            $startDay   =  1 ;  
            $endDay   =    7;
            $cycle = 1;
        }
        else if(  $request->cycle  == 2 )
        {
            $startDay   = 8 ;  
            $endDay   =    14;
            $cycle = 2;
        }else if(  $request->cycle  == 3)
        {
            $startDay   = 15 ;  
            $endDay   =    21;
            $cycle = 3;
        }else if(  $request->cycle  == 4)
        {
            $startDay   = 22 ;  
            $endDay   =    28;
            $cycle = 4;
        }
        else  
        {
            $totalDays = cal_days_in_month(CAL_GREGORIAN, $month , $year );

            if( $totalDays > 28)
            {
                $startDay   = 29;  
                $endDay   =    $totalDays;   
                $cycle = 5; 
            }
            else 
            {
                $startDay   =  1 ;  
                $endDay   =    7;
                $cycle = 1;
            } 
        }


        $bin = $request->session()->get('_bin_');

        $first = DB::table('ba_service_booking')   
        ->whereMonth("service_date", $month )
        ->whereYear("service_date", $year )
        ->whereDay("service_date", ">=",  $startDay )
        ->whereDay("service_date", "<=",  $endDay )
        ->whereIn("book_status",  array('delivered', 'completed')) 
        ->where("bin",$bin)
        ->orderBy("ba_service_booking.service_date")
        ->select('ba_service_booking.id', 
          'ba_service_booking.service_date',
          "bin",
          DB::Raw("'customer' source"), 
          'total_cost as orderCost', 
          DB::Raw("'0.00' packingCost"), 
          'delivery_charge as deliveryCharge', 
          'discount',  'payment_type  as paymentType',
          'book_status as bookingStatus', DB::Raw("'booktou' paymentTarget"),
          DB::Raw("'normal' orderType")) ;
 		
 		 

        $result = DB::table('ba_pick_and_drop_order')
        ->where("request_from", "business")
        ->whereMonth("service_date", $month )
        ->whereYear("service_date", $year )
        ->whereDay("service_date", ">=",  $startDay )
        ->whereDay("service_date", "<=",  $endDay )
        ->whereIn("book_status",  array('delivered', 'completed'))
        ->where("request_by", $bin)
        ->orderBy("ba_pick_and_drop_order.service_date")
        ->union($first) 
        ->select('ba_pick_and_drop_order.id', 
          'ba_pick_and_drop_order.service_date',
          "request_by as bin",
          "source",
          'total_amount as orderCost',
          'packaging_cost as packingCost',  
          'service_fee as deliveryCharge', 
          DB::Raw("'0' discount") ,  'pay_mode as paymentType', 
          'book_status  as bookingStatus',             
          "payment_target as paymentTarget",
          DB::Raw("'pnd' orderType"))  
        ->get();  

        $oids =  $bins = array();
        foreach($result as $item)
        {
            if($item->orderType == "normal")
            {
                $oids[] = $item->id ;  
            }
            $bins[] = $item->bin ; 
        }
        $bins[] = $oids[] = 0;

        $normal_sales_cart = DB::table("ba_shopping_basket") 
        ->whereIn("order_no",  $oids  )  
        ->get();  
 
 
        $all_business  = DB::table("ba_business")  
        ->whereIn("id", $bins ) 
        ->get();  
        
        $theme = DB::table("ba_theme")
        ->first();

       $data =  array(   
        'results' =>  $result,  
        'cart_items' =>$normal_sales_cart, 
        'businesses' => $all_business, 
        'cycle' => $cycle,
        'month' =>  $month , 
        'year' => $year,
        'theme'=>$theme  ); 

       return view("billing.sales_report_per_cycle")->with( $data ); 
     }


 protected function monthlySalesServiceEarningReport(Request $request )
    {
        if($request->year != "")
        {
            $year  = $request->year ; 
        }
        else
        {
            $year  = date('Y' );
        }
 

        if($request->month !="" )
        {
          $month = $request->month ;
        }
        else
        {
            $month = date('m' );
        }

       	$bin = $request->session()->get('_bin_');

        $first = DB::table('ba_service_booking')   
        ->whereMonth("service_date", $month )
        ->whereYear("service_date", $year )
        ->where("bin",$bin)
        ->whereIn("book_status",  array('delivered', 'completed')) 
        ->orderBy("ba_service_booking.service_date")
        ->select('ba_service_booking.id', 
          'ba_service_booking.service_date',
          "bin",
          DB::Raw("'customer' source"), 
          'total_cost as orderCost', 
          DB::Raw("'0.00' packingCost"), 
          'delivery_charge as deliveryCharge', 
          'discount',  'payment_type  as paymentType',
          'book_status as bookingStatus', DB::Raw("'booktou' paymentTarget"),
          DB::Raw("'normal' orderType"));
 
        $result = DB::table('ba_pick_and_drop_order')  
        ->where("request_from", "business")
        ->whereMonth("service_date", $month )
        ->whereYear("service_date", $year )
        ->whereIn("book_status",  array('delivered', 'completed'))
        ->where("request_by",$bin) 
        ->orderBy("ba_pick_and_drop_order.service_date")
        ->union($first) 
        ->select('ba_pick_and_drop_order.id', 
            'ba_pick_and_drop_order.service_date',
            "request_by as bin",
          "source",
          'total_amount as orderCost',
          'packaging_cost as packingCost',  
          'service_fee as deliveryCharge', 
          DB::Raw("'0' discount") ,  'pay_mode as paymentType', 
          'book_status  as bookingStatus',             
          "payment_target as paymentTarget",
          DB::Raw("'pnd' orderType"))  
        ->get();  

        $oids =  $bins = array();
        foreach($result as $item)
        {
            if($item->orderType == "normal")
            {
                $oids[] = $item->id ;  
            }
            $bins[] = $item->bin ; 
        }
        $bins[] = $oids[]= 0;

        $normal_sales_cart = DB::table("ba_shopping_basket") 
        ->whereIn("order_no",  $oids  )  
        ->get();  
 
 
        $all_business  = DB::table("ba_business")  
        ->whereIn("id", $bins ) 
        ->get(); 

        $theme = DB::table("ba_theme")
        ->first();
       $data =  array(   
        'results' =>  $result,  
        'cart_items' =>$normal_sales_cart, 
        'businesses' => $all_business, 
        'month' =>  $month , 
        'year' => $year ,'theme'=>$theme );
       return view("billing.sales_report_per_month_for_merchant")->with( $data ); 
     }





 // View Business Normal and PnD Sales Report for monthly
     protected function salesAndClearanceReport(Request $request )
     {

        $bin = $request->session()->get('_bin_');
        $cycles =  array(1);
        if( isset($request->cycle))
        {
            $cycles =  $request->cycle;
        }
        

        if($request->year != "")
        {
            $year  = $request->year ; 
        }
        else
        {
            $year  = date('Y' );
        } 

        if($request->month !="" )
        {
          $month = $request->month ;
        }
        else
        {
            $month = date('m' );
        }

        $totalDays = cal_days_in_month(CAL_GREGORIAN, $month , $year );

        $allowed_cycles = true;
        if(count($cycles) > 0)
        {
            //check if adjoining cycles are selected
            
            for($i=0; $i<count($cycles)-1; $i++)
            {
                for($j= $i+1; $j <count($cycles) ; $j++)
                {
                    if( $cycles[$i] + 1 != $cycles[$j]  )
                    {
                        $allowed_cycles = false; 
                    }
                    break;

                } 
            }

            if( count($cycles)  == 1 )
            {
                if($cycles[0]  == 1 )
                {
                    $startDay   =  1 ;  
                    $endDay   =    7;
                    $cycle = 1;
                }
                else if($cycles[0]  == 2 )
                {
                    $startDay   = 8 ;  
                    $endDay   =    14;
                    $cycle = 2;
                }else if($cycles[0]  == 3 )
                {
                    $startDay   = 15 ;  
                    $endDay   =    21;
                    $cycle = 3;
                }else if($cycles[0]  == 4 )
                {
                    $startDay   = 22 ;  
                    $endDay   =    28;
                    $cycle = 4;
                }
                else  
                {
                    if( $totalDays > 28)
                    {
                        $startDay   = 29;  
                        $endDay   =  $totalDays; 
                    }
                }

            }
            else  //count($cycles)  > 1
            {
                $size = count($cycles)-1;
                
                if($cycles[0]  == 1 )
                {
                    $startDay   =  1 ;  
                }
                else if($cycles[0]  == 2 )
                {
                    $startDay   = 8 ;  
                }else if($cycles[0]  == 3 )
                {
                    $startDay   = 15 ;  
                }else if($cycles[0]  == 4 )
                {
                    $startDay   = 22 ; 
                }
                else  
                {
                    if( $totalDays > 28)
                    {
                        $startDay   = 29;   
                    }
                    else 
                    {
                        $startDay   = 0;   
                    }
                }
 

                if($cycles[$size]  == 1 )
                { 
                    $endDay   =    7; 
                }
                else if($cycles[$size]  == 2 )
                { 
                    $endDay   =    14; 
                }else if($cycles[$size]  == 3 )
                { 
                    $endDay   =    21; 
                }else if($cycles[$size]  == 4 )
                {  
                    $endDay   =    28; 
                }
                else  
                {
                    if( $totalDays > 28)
                    {
                        $endDay   =  $totalDays; 
                    }
                    else 
                    {
                        $endDay   = 0;   
                    }
                }

            } 

        }

        if(  !$allowed_cycles  )
        {
            return redirect("/admin/billing-and-clearance/sales-and-clearance-report?bin=" . $bin )
            ->with("err_msg", "Billing cycles selected are not adjacent. Report generation blocked."); 
        }

            $business  =  DB::table("ba_business")  
           ->join("ba_business_commission", "ba_business_commission.bin", "=", "ba_business.id") 
           ->where("ba_business_commission.month", $month )
           ->where("ba_business_commission.year", $year ) 
           ->where("ba_business.id", $bin ) 
           ->select("ba_business.id", "ba_business.name",    "ba_business_commission.commission") 
           ->first(); 

           

        if( !isset($business)  )
        {
            return redirect("/reports/billing-and-clearance/payment-due-merchants"  )
            ->with("err_msg", "Business not selected. Report generation blocked."); 
        }


        // return redirect("/admin/billing-and-clearance/sales-and-clearance-report?bin=" . $bin )
        // ->with("err_msg", "Selected month has only 4 weeks of payment.");  

        $first = DB::table('ba_service_booking')  
        ->where("ba_service_booking.bin", $bin )
        ->whereDay("service_date", ">=",  $startDay )
        ->whereDay("service_date", "<=",  $endDay )
        ->whereMonth("service_date", $month )
        ->whereYear("service_date", $year )
        ->where("clerance_status", "paid" )
        ->orderBy("ba_service_booking.service_date")
        ->select('ba_service_booking.id', 
          'ba_service_booking.service_date',
          DB::Raw("'customer' source"), 
          'total_cost as orderCost', 
          DB::Raw("'0.00' packingCost"), 
          'delivery_charge as deliveryCharge', 
          'discount',  'payment_type  as paymentType',
          'book_status as bookingStatus', DB::Raw("'booktou' paymentTarget"),
          DB::Raw("'normal' orderType")) ;
 
        $result = DB::table('ba_pick_and_drop_order')  
        ->where("request_from", "business")
        ->where("request_by", $bin )
        ->whereDay("service_date", ">=",  $startDay )
          ->whereDay("service_date", "<=",  $endDay )
          ->whereMonth("service_date", $month )
          ->whereYear("service_date", $year )
          ->where("clerance_status", "paid" )
          ->orderBy("ba_pick_and_drop_order.service_date")
        ->union($first)  
        ->select('ba_pick_and_drop_order.id', 
            'ba_pick_and_drop_order.service_date',
          "source",
          'total_amount as orderCost',
          'packaging_cost as packingCost',  
          'service_fee as deliveryCharge', 
          DB::Raw("'0' discount") ,  'pay_mode as paymentType', 
          'book_status  as bookingStatus',             
          "payment_target as paymentTarget",
          DB::Raw("'pnd' orderType"))  
        ->get();  

        $oids = array();
        foreach($result as $item)
        {
            if($item->orderType == "normal")
            {
                $oids[] = $item->id ; 

            } 
        }
        $oids[]= 0;

        $normal_sales_cart = DB::table("ba_shopping_basket") 
        ->whereIn("order_no",  $oids  )  
        ->get();  
 

       $all_business  = DB::table("ba_business")
       ->where("id",$bin) 
       ->get(); 

       $theme = DB::table("ba_theme")
      ->first();

       $data =  array(  'business' => $business, 
        'businesses' => $all_business,
        'results' =>  $result,  
        'cart_items' =>$normal_sales_cart,
        'bin' => $bin,   
        'cycles' => $cycles,
        'start' =>  $startDay , 
        'end' =>  $endDay , 
        'month' =>  $month , 
        'year' => $year,'theme'=>$theme  );
        
       return view("billing.sales_report_per_cycle_for_merchant")->with( $data ); 
     }
}
