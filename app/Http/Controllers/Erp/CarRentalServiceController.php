<?php

namespace App\Http\Controllers\Erp; 

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Session;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Support\Facades\File;
use Maatwebsite\Excel\Facades\Excel; 
use App\Libraries\FirebaseCloudMessage; 
use Illuminate\Support\Facades\Hash;
use Jenssegers\Agent\Agent;

use App\User; 
use App\PickAndDropRequestModel;
use App\Business; 
use App\BusinessCategory;
use App\ProductCategoryModel; 
use App\ProductModel;
use App\ShoppingOrderModel;
use App\ShoppingOrderItemsModel;
use App\AddressModel;
use App\MemberAddressModel;
use App\DeliveryOrderModel;
use App\ServiceBooking;
use App\OrderStatusLogModel; 
use App\CustomerProfileModel;
use App\PaymentDealingModel;
use App\PremiumBusiness;
use App\ProductImportedModel; 
use App\CloudMessageModel;
use App\ProductPromotion;
use App\GlobalSettingsModel;
use App\SliderModel; 
use App\StaffAttendanceModel;
use App\CouponModel;
use App\OrderSequencerModel;
use App\Imports\ProductImport; 
use App\BusinessMenu;
use App\AccountsLog;
use App\CmsBannerSlidersModel;
use App\CmsProductsListedModel;
use App\CmsProductGroupsModel;
use App\OrderRemarksModel;
use App\AgentCacheModel;
use App\StaffSalaryModel;
use App\DailyExpenseModel;
use App\ComplaintModel;
use App\BusinessPaymentModel;
use App\Franchise;
use App\BusinessCommissionModel;
use App\DailyDepositModel;
use App\FeedbackMerchant;
use App\Imports\ProductsImport;
use App\Exports\MonthlyTaxesExport;
use App\Exports\AdminDashboardExport;
use App\Exports\MonthlyAgentsOrdersExport;
use App\Exports\DailyAgentsOrdersExport;
use App\Exports\DailySalesOrderExport;
use App\Exports\MonthlyCommissionExport;
use App\Traits\Utilities; 
use PDF;
use App\DepositTargetModel;
use App\ProductVariantModel;
use App\ImportProductFileModel;
use App\SectionTypeModel;
use App\SectionDetailsModel;
use App\AppSectionModel;
use App\AppSectionDetailsModel;
use App\PosSubscription;
use App\SelfPickUpModel;
use App\PosPayment;
use App\BrandModel;
use App\ServiceCategoryModel;
use App\AgentEarningModel;
use App\AgentSalaryAdvanceModel;
use App\PopularProductsModel;
use App\AddDiscountProductsModel;
use App\BusinessServiceProduct;
use App\PackageMetaModel;
use App\ServicePackageSpecModel;
use App\SponsoredPackageModel;
use App\LandingScreenSectionModel;
use App\LandingScreenSectionContenModel;
use App\MetaKeyModel;
class CarRentalServiceController  extends Controller
{
    use Utilities;
    
 
protected function viewCarPages(Request $request)
  
  {
    
    $bin = $request->session()->get("_bin_");

    if($bin == "")
    {
      return redirect('/view-car-packages');
    }

    $services= DB::table("ba_service_products")
    ->where("ba_service_products.bin",$bin)
    ->paginate(10);

    $business= Business::find( $bin) ;
   $srv_category=DB::table("ba_service_category")
   ->where("bookable_category","CRS")
   ->get();
    $data = array(
                  'services' => $services, 
                  'bin'=>$bin,
                  'business'=>$business,"srv_category"=>$srv_category
                  
                );   
    return view("car_services.car_service_packages")->with('data',$data);

  }
  protected function addCarRentalServices(Request $request)

  {

if( isset($request->btnsaveconfirmation) && $request->btnsaveconfirmation =="save")
      { 

    $bin = $request->bin; 

    $business  = Business::find($bin); 
    if( !isset($business ))
    {
      return redirect("/view-car-packages")->with("err_msg", "No car rental services found.");
    }

    $cdn_url = config('app.app_cdn'); 
    $cdn_path =  config('app.app_cdn_path'); 
    $folder_name = 'services_'. $bin ; 
    $folder_path =  $cdn_path . "/assets/upload/" . $folder_name ;
 
    $service_product = new BusinessServiceProduct; 
 

    if ($request->hasFile('photo')) {

      if( !File::isDirectory($folder_path))
      {
        File::makeDirectory( $folder_path, 0777, true, true);
      }  
      
      $file = $request->photo;
      $originalname = $file->getClientOriginalName(); 
      $extension = $file->extension();
      $filename = "service_" . time()  . ".{$extension}";
      $file->storeAs('upload',  $filename );
      $imgUpload = File::copy(storage_path().'/app/upload/'. $filename, $folder_path . "/" . $filename);
      $file_names[] =  $filename ;
      $file_url = $cdn_url . '/assets/upload/' . $folder_name  . "/" . $filename ; 
      $service_product->photos= $file_url;
    }
        $service_product->service_category = $request->service_category;
        $service_product->bin  = $business->id;
        $service_product->srv_name = $request->srv_name;
        $service_product->srv_details = $request->srv_details;
        $service_product->pricing = $request->pricing;
        $service_product->actual_price = $request->actual_price;
        $service_product->max_price = $request->max_price;
        // $service_product->discount = $request->discount;
        // $service_product->gst_code = $request->gst_code;
        $service_product->duration_hr = $request->duration_hr;
        $service_product->duration_min = $request->duration_min; 
        $service_product->srv_status = $request->srv_status;
        $service_product->service_type = $request->service_type;
        $service_product->gender = $request->gender;
        $service_product->valid_start_date = date("Y-m-d H:i:s",strtotime($request->valid_start_date));
        $service_product->valid_end_date = date("Y-m-d H:i:s",strtotime($request->valid_end_date));
        $service_product->save();   

      return redirect("/view-car-packages")->with("err_msg", "record add successfully!");
    }   
    else 
    {
     return redirect("/view-car-packages") ;
   }

 }

 protected function deleteCarRentalServices(Request $request)
    {

         if($request->bin == ""   )
            {
              return redirect("/view-car-packages")->with("err_msg", "No car rental services selected.");
            }

             $bin = $request->bin; 
             $business  = Business::find($bin); 
             if( !isset($business ))
             {
                return redirect("/view-car-packages")->with("err_msg", "No car rental services found.");
             }

              if ($request->key=="") 
             {
              return redirect("/view-car-packages")->with("err_msg", "field missing!");
             }

        $key = $request->key;

        $delete = DB::table("ba_service_products")
        ->where("id",$key) 
        ->delete();

        if ($delete) {
            return redirect("/view-car-packages")
            ->with("err_msg","record deleted!");
        }else{
            return redirect("/view-car-packages")
            ->with("err_msg","failed!");
        }

    }   

  public function editCarRentalService(Request $request)
  {


    if($request->bin == ""   )
    {
      return redirect("/view-car-packages")->with("err_msg", "No car rental services selected.");
    }

    $bin = $request->bin; 
    $business  = Business::find($bin); 
    if( !isset($business ))
    {
      return redirect("/view-car-packages")->with("err_msg", "No car rental services found.");
    }

    if ($request->key=="") 
    {
      return redirect("/view-car-packages")->with("err_msg", "field missing!");
    }

    $cdn_url = config('app.app_cdn'); 
    $cdn_path =  config('app.app_cdn_path'); 

    $id = $request->key; 

    $folder_name = 'services_'. $bin ; 
    $folder_path =  $cdn_path . "/assets/upload/" . $folder_name ;
 

    $service_product = BusinessServiceProduct::find($id);
    $bin = $service_product->bin; 

    if ($request->hasFile('photo')) {

      if( !File::isDirectory($folder_path))
      {
        File::makeDirectory( $folder_path, 0777, true, true);
      }  
      
      $file = $request->photo;
      $originalname = $file->getClientOriginalName(); 
      $extension = $file->extension( );
      $filename = "service_" . time()  . ".{$extension}";
      $file->storeAs('upload',  $filename );
      $imgUpload = File::copy(storage_path().'/app/upload/'. $filename, $folder_path . "/" . $filename);
      $file_names[] =  $filename ;
      $file_url = $cdn_url . '/assets/upload/' . $folder_name  . "/" . $filename ;

      $service_product->photos= $file_url;
    }
      $service_product->srv_name = $request->srv_name;
      $service_product->pricing = $request->pricing;
      $service_product->service_type = $request->service_type;     
      $save = $service_product->save();   

    if ($save) {
      return redirect("/view-car-packages" )->with("err_msg", "record udpate successfully!");
    }  
    else 
    {
     return redirect("/view-car-packages") ;
   } 
 }

 protected function viewMoreDetailsCRS(Request $request)
  
  {
    
    $id = $request->srv_prid;

    if($id == "")
    {
      return redirect('/view-car-packages');
    }

    $services= DB::table("ba_service_products")
    ->where("ba_service_products.id",$id)
    ->first();
    $package_meta_key=DB::table("ba_package_meta_data")
    ->get();
    $srv_package_spec=DB::table("ba_service_package_spec")
    ->where("srv_prid",$id)
    ->get();
    $data = array('crs_services' =>$services,"pk_meta_key"=>$package_meta_key,"srv_package_spec"=>$srv_package_spec);   
    return view("car_services.view_more_details")->with('data',$data);

  }
protected function addServicePackageSpec(Request $request)
  {   
    if( isset($request->btnsaveconfirmation) && $request->btnsaveconfirmation =="save")
    {

      $id = $request->srv_prid;

      if($id == "")
      {
        return redirect('/view-car-packages');
      }

      $service_product = BusinessServiceProduct::find( $id ) ;

      if(  !isset($service_product ) )
      {
        return redirect("/admin/customer-care/business/view-more-details-crs/" . $id )->with("err_msg", "No service product found.");
      }

      $packages_spec = ServicePackageSpecModel::where("srv_prid", $request->id )->first();
      if(  !isset($production ) )
      {
        $packages_spec = new ServicePackageSpecModel;
      }
    
      $packages_spec->srv_prid = $request->srv_prid;
      $packages_spec->meta_key = $request->metakey;
      $packages_spec->meta_value = $request->metavalue;
      $packages_spec->meta_key_text = $request->meta_key_text;

      $packages_spec->save(); 

      return redirect("/admin/customer-care/business/view-more-details-crs/" . $id )->with("err_msg", " Added to packages service specification.");

    }
    else 
    {
     return redirect("/view-car-packages") ;
   } 
 }
protected function editServicePackageSpec(Request $request)
 {

  $id = $request->srv_prid;

  if($id == "")
  {
    return redirect('/view-car-packages');
  }

  $service_product = ServiceProductModel::find( $id ) ;

  if(  !isset($service_product ) )
  {
    return redirect("/admin/customer-care/business/view-more-details-crs/" . $id )->with("err_msg", "No service product found.");
  }

  if ($request->key=="") {
   return redirect("/admin/customer-care/business/view-more-details-crs/" . $id )->with("err_msg", "missing data to perform action!."); 
 }

 $key = $request->key;

 $packages_spec = ServicePackageSpecModel::find($key);
 $packages_spec->meta_key = $request->meta_key ;
 $packages_spec->meta_value = $request->meta_value; 
 $packages_spec->meta_key_text = $request->meta_keytext;           
 $save = $packages_spec->save();   

 if ($save) {
  return redirect("/admin/customer-care/business/view-more-details-crs/" . $id)->with("err_msg", "record udpate successfully!");
}  
else 
{
 return redirect("/view-car-packages") ;
} 
}
protected function deleteServicePackageSpec(Request $request)
 {

  $id = $request->srv_prid;

  if($id == "")
  {
    return redirect('/view-car-packages');
  }

  $service_product = ServiceProductModel::find( $id ) ;

  if(  !isset($service_product ) )
  {
    return redirect("/admin/customer-care/business/view-more-details-crs/" . $id )->with("err_msg", "No service product found.");
  }

  if ($request->key=="") {
   return redirect("/admin/customer-care/business/view-more-details-crs/" . $id )->with("err_msg", "missing data to perform action!."); 
 }

 $key = $request->key;

  $delete = DB::table("ba_service_package_spec")
  ->where("id",$key) 
  ->delete(); 

 if ($delete) {
  return redirect("/admin/customer-care/business/view-more-details-crs/" . $id)->with("err_msg", "record delete successfully!");
}  
else 
{
 return redirect("/admin/customer-care/business/view-more-details-crs/" . $id)->with("err_msg","failed!") ;
} 
}

}