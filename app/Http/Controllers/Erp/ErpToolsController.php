<?php

namespace App\Http\Controllers\Erp;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\File;
use Illuminate\Contracts\Encryption\DecryptException;
use Maatwebsite\Excel\Facades\Excel;


use App\Business;
use App\ProductCategoryModel;
use App\ProductModel;
use App\ProductVariantModel;
use App\ProductPhotosModel;
use App\ShoppingOrderModel;
use App\ShoppingOrderItemsModel;
use App\AddressModel;
use App\MemberAddressModel;
use App\DeliveryOrderModel;
use App\ServiceBooking;
use App\OrderStatusLogModel;
use App\CustomerProfileModel;
use App\PaymentDealingModel;
use App\PremiumBusiness;
use App\ProductImportedModel;
use App\CloudMessageModel;
use App\Imports\ProductsImport;
use App\OrderSequencerModel;
use App\BusinessServiceProduct;
use App\PickAndDropRequestModel;
use App\OrderRemarksModel;
use App\Theme;
use App\User;
use App\CouponRequestModel;

use App\Traits\Utilities;

use PDF;



class ErpToolsController extends Controller
{

   use Utilities;

   public function erpDashboard(Request $request)
   {

    $bin = $request->session()->get("_bin_");
    $status = array( 'delivered','completed' ) ;
    $today = date('Y-m-d');

    $normal_orders = DB::table("ba_service_booking")
    ->join("ba_profile", "ba_profile.id", "=", "ba_service_booking.book_by")
    ->where("bin",$bin)
    ->whereRaw("date(service_date) >= '$today' " )
    ->select("ba_service_booking.id",
        "ba_service_booking.biz_order_no", "ba_service_booking.service_date",
        "ba_service_booking.preferred_time",  "ba_service_booking.book_status",
        "ba_profile.fullname as  customer_name" ,
        "ba_service_booking.address",
        "ba_service_booking.landmark",
        "ba_service_booking.seller_payable as total_cost","ba_service_booking.payment_type as pay_mode" )  ;


    $all_orders = DB::table("ba_pick_and_drop_order")
    ->where("request_by", $bin )
    ->whereRaw("date(service_date) >= '$today' " )
    ->select("id",  "biz_order_no", "service_date", "delivery_time as preferred_time", "book_status",   "fullname as customer_name", "address", "landmark", "total_amount as total_cost", "pay_mode" )
    ->union($normal_orders )
    ->get();

    $data = array('all_orders' =>  $all_orders );

    return view("dashboard_business")->with(  $data  );

}

public function pickAndDropActiveOrders(Request $request)
{
    $bin = $request->session()->get("_bin_");

    $status = array( 'new','confirmed', 'cancel_by_client', 'canceled',
        'cancel_by_owner','order_packed','pickup_did_not_come','in_route',
        'package_picked_up','delivery_scheduled' )  ;

    $today = date('Y-m-d' , strtotime("7 days ago"));
  
    $all_orders = DB::table("ba_pick_and_drop_order")
    ->where("request_by", $bin )
    ->whereIn("book_status",$status)
    ->whereRaw("date(service_date) >= '$today' " )
    ->select("id", "service_date", "delivery_time as preferred_time", "book_status",   "fullname as customer_name", "address", "landmark", "total_amount as total_cost", "pay_mode",
       DB::Raw("'pnd' order_type")  )
    // ->union($normal_orders )
    ->get();

    $theme = DB::table("ba_theme")
    ->first();

    $data = array(
        'all_orders' =>  $all_orders ,
        'sub_menu' => 'order_sub_menu','theme'=>$theme
    );

    return view("orders.pnd_active_orders")->with(  $data  );
}

public function viewActiveOrders(Request $request)
{

    $bin = $request->session()->get("_bin_");
    $status = array( 'new','confirmed', 'cancel_by_client', 'canceled',
        'cancel_by_owner','order_packed','pickup_did_not_come','in_route',
        'package_picked_up','delivery_scheduled' )  ;

    $today = date('Y-m-d' , strtotime("7 days ago"));

    $normal_orders = DB::table("ba_service_booking")
    ->where("bin",$bin)
    ->whereIn("book_status",$status)
    ->whereRaw("date(service_date) >= '$today' " )
    ->select("id", "service_date", "preferred_time",  "book_status",   DB::Raw("'na' customer_name"), "address", "landmark", "seller_payable as total_cost","payment_type as pay_mode" ,
      DB::Raw("'normal' order_type"))  ;


    $all_orders = DB::table("ba_pick_and_drop_order")
    ->where("request_by", $bin )
    ->whereIn("book_status",$status)
    ->whereRaw("date(service_date) >= '$today' " )
    ->select("id", "service_date", "delivery_time as preferred_time", "book_status",   "fullname as customer_name", "address", "landmark", "total_amount as total_cost", "pay_mode",
       DB::Raw("'pnd' order_type")  )
    ->union($normal_orders )
    ->get();

    $theme = DB::table("ba_theme")
    ->first();

    $data = array(
        'all_orders' =>  $all_orders ,
        'sub_menu' => 'order_sub_menu','theme'=>$theme
    );

    return view("orders.view_all_active")->with(  $data  );

}
protected function dashboard(Request $request)
{

    if($request->filter_date=="")
    {
        $today = date('Y-m-d');
    }else{
        $today = date('Y-m-d',strtotime($request->filter_date));
    }

    $bin = $request->session()->get("_bin_"); 

    
    $all_orders = DB::table("ba_service_booking")
    ->where("bin",$bin)
    ->whereRaw("date(service_date) >= '$today' " )
    ->where("book_status","new")
    ->select("id", "service_date", "preferred_time",  "book_status", "book_date", "customer_name", "address","customer_phone", "landmark","city","pin_code" ,"is_confirmed","seller_payable as total_cost","payment_type","payment_status" )
    ->orderBy("id", "desc")        
    ->get();

    foreach($all_orders as $order)
    {
        $time = strtotime($order->book_date);
        $book_time = date("H:i:s", $time);
        $order->book_time = $book_time;
    }

    $today_orders = DB::table("ba_service_booking")
    ->where("bin",$bin)
    ->whereRaw("date(service_date) >= '$today' " )
    ->whereNotIn('book_status', ['new'])
    ->select("id", "service_date", "preferred_time",  "book_status", "book_date","customer_name", "address","customer_phone", "landmark","city","pin_code" ,"is_confirmed","seller_payable as total_cost","payment_type","payment_status" )
    ->orderBy("id", "desc")      
    ->get();

    foreach($today_orders as $order)
    {
        $time = strtotime($order->book_date);
        $book_time = date("H:i:s", $time);
        $order->book_time = $book_time;
    }

    $theme = DB::table("ba_theme")
    ->first();
    $data = array('today'=>$today,'all_orders' =>$all_orders,'theme'=>$theme,"today_orders"=>$today_orders );
    return view("dashboard_erp_02")->with(  $data  );
}

protected function viewActiveOrderDetails(Request $request)
{

    $cdn_url =   config('app.app_cdn')    ;  
    $cdn_path =  config('app.app_cdn_path')  ; 
    $cms_base_url =  config('app.app_cdn')   ;  
    $cms_base_path = $cdn_path  ;  
    $image  =   $cdn_url .  "/assets/image/no-image.jpg";
    $userid= $request->session()->get('__user_id_');

    $orderno = $request->orderno;

    if($orderno == "")
    {
        return redirect("/admin/customer-care/orders/view-all"); 
    }
    $order_info   = DB::table("ba_service_booking") 
    ->join("ba_business", "ba_service_booking.bin", "=", "ba_business.id")
    ->select("ba_service_booking.*", "ba_business.name as businessName","ba_business.locality as businessLocality", 
      "ba_business.landmark as businessLandmark", "ba_business.city as businessCity", 
      "ba_business.state as businessState", "ba_business.pin as businessPin", "ba_business.phone_pri as businessPhone",  "ba_business.banner" )
    ->where("ba_service_booking.id", $orderno )    
    ->first() ;
    
    if( !isset($order_info))
    {
      return redirect("/admin/customer-care/orders/view-all"); 
  }

  $bin = $order_info->bin;

  $order_items   = DB::table("ba_shopping_basket")   
  ->join("ba_products", "ba_products.pr_code",  "=", "ba_shopping_basket.pr_code")
  ->where("ba_shopping_basket.order_no", $orderno )    
  ->select("ba_shopping_basket.*" , DB::Raw("'na' photos")  )
  ->get() ;      

  if( count ($order_items) == 0)
  {
    $order_items   = DB::table("ba_shopping_basket")   
    ->join("ba_product_variant", "ba_product_variant.prsubid",  "=", "ba_shopping_basket.prsubid")
    ->where("ba_shopping_basket.order_no", $orderno )    
    ->select("ba_shopping_basket.*" , DB::Raw("'na' photos"))
    ->get() ;
} 

$customer_info   = DB::table("ba_profile") 
->where("id", $order_info->book_by )    
->first() ;

if( !isset($customer_info))
{
    return redirect("/admin/customer-care/orders/view-all"); 
}

$agent_info=  DB::table("ba_delivery_order")
->join("ba_profile", "ba_delivery_order.member_id", "=", "ba_profile.id")
->select("ba_profile.fullname as deliveryAgentName","ba_profile.phone as deliveryAgentPhone")
->where("order_no", $orderno  )
->where("order_type",  'client order'  )
->first(); 

$remarks    = DB::table("ba_order_remarks")   
->where("order_no", $orderno )  
->orderBy("id", "asc")   
->get() ;
      //images  
$agent_uploaded_images =  DB::table("ba_agent_delivery_logs")  
->where("order_no",  $orderno )  
->select("image_url as imageUrl", "upload_date as uploadedDate" )
->get();

$today = date('Y-m-d');

$all_agents = DB::table("ba_profile")
->join("ba_users", "ba_users.profile_id", "=", "ba_profile.id")
->whereIn("ba_profile.id", function($query) use ( $today ) { 
  $query->select("staff_id")
  ->from("ba_staff_attendance")
  ->whereDate("log_date",   $today  ); 
}) 
->where("ba_users.category", 100)
->where("ba_users.status", "<>" , "100") 
->where("ba_profile.fullname", "<>", "" )
->where("ba_users.franchise", 0)
->select( "ba_profile.id", "ba_profile.fullname", "ba_profile.fullname as nameWithTask" , 
    "ba_profile.phone", DB::Raw("'yes' isFree") , "ba_profile.profile_photo" )  
->get();

$subids =  array(); 
foreach( $order_items as $item )
{
  $subids[] = $item->prsubid;
}
$subids[] =0;

$product_images =  DB::table('ba_product_photos') 
->whereIn('prsubid', $subids)
->select( "prsubid", "image_url" )   
->get();

$image_url = "https://cdn.booktou.in/assets/image/no-image.jpg";

foreach($order_items as $order_item)
{
    foreach($product_images as $image )
    {
      if( $order_item->prsubid == $image->prsubid)
      {
        $image_url = $image->image_url ; 
        break;
    } 
}

$order_item->photos =  $image_url ; 
}

/* photo processing ends */
$franchise_list    = DB::table("ba_franchise")   
->where("current_status", "active" )  
->select()  
->get() ;

$routed_franchise = DB::table("ba_franchise")   
->where("current_status", "active" ) 
->where("frno",$order_info->route_to_frno) 
->first() ;

$user_info = DB::table("ba_users")
->where('id',$userid)
->first();

$adminFrno = $user_info->franchise;

$all_staffs =  DB::table("ba_profile")   
->join("ba_users", "ba_users.profile_id", "=", "ba_profile.id")
->where("is_booktou_staff", "yes" )
->where("ba_users.status", "<>", "100" )  
->where("ba_users.category",   "100" )  
->where("franchise", 0)
->select("ba_profile.*", "ba_users.category" ) 
->get();
// ................................staff attedence     
$active_agents =  DB::table("ba_agent_attendance")
->whereRaw("date(log_date) = '$today' " )    
->get();  
$agent_id = array();
$agent_id[] = 0; 
foreach($active_agents as $agent)
{
  $agent_id[] = $agent->agent_id;
}
$status = ['ready-for-task', 'ready-for-multiple-task'];
$agents_status = DB::table("ba_agent_locations")
->whereRaw("date(updated_at) = '$today' " ) 
->whereIn("agent_id", $agent_id )
->whereIn("agent_status",$status)
    // ->select("agent_id ")
->count();
$result = array('order_info' => $order_info , 
    'adminFrno' => $adminFrno ,
    'customer' =>  $customer_info, 
    'order_items' => $order_items, 
    'agent_info' => $agent_info, 
    'remarks' => $remarks ,
    'all_agents' =>$all_agents, 
    'staffs' =>$all_staffs,
    'franchise'=>$franchise_list, 
    'routed_frno'=>$routed_franchise,
    'agent_uploaded_images' => $agent_uploaded_images ,
    'agents_status'=>$agents_status,
    'active_agents'=>$active_agents);
return view("view_order_details")->with('data',$result);
}
public function addPreparationTime(Request $request)
{   
    
    if( isset($request->btnsaveconfirmation) && $request->btnsaveconfirmation =="save")
    {

      $id = $request->orderno;

      if($id == "")
      {
        return redirect('/')->with("err_msg", "No found.");
    }

    $service_booking = ServiceBooking::find( $id ) ;

    if(  !isset($service_booking ) )
    {
        return redirect('/Erp/customer-care/order/view-details/'.$id)->with("err_msg", "No found.");
    }

    $set_prep_time =ServiceBooking::find( $id );
    $set_prep_time->preparation_time =$request->prep_time;
    $set_prep_time->is_confirmed ="yes";
    $set_prep_time->book_status ="confirmed";
    $save = $set_prep_time->save();

    if($save){

    DB::table("ba_order_status_log")
    ->where("order_no",$id)
    ->increment('seq_no', 1);
   
    $update_log = new OrderStatusLogModel;
    $update_log->order_no = $id;
    $update_log->order_status = "confirmed";
    $update_log->log_date = date('Y-m-d H:i:s');
    $update_log->seq_no = 1;
    if($request->remark != "")
    {
      $update_log->remarks =  $request->remark ;
    }
    $update_log->remarks =  "confirmed by merchant"; 
  
    $save = $update_log->save();

    }

    
    
    if($save){
       return redirect('/Erp/customer-care/order/view-details/'.$id)->with("err_msg", " Added preparation time.");
   } 

}
else 
{
   return redirect('/Erp/customer-care/order/view-details/'.$id)->with("err_msg","failed!") ;
} 
} 
protected function cancelPreparationTime(Request $request)
{

  $id = $request->orderno;


  if($id == "")
  {
    return redirect('/');
}

$service_booking = ServiceBooking::find( $id ) ;

if(  !isset($service_booking ) )
{
    return redirect('/Erp/customer-care/order/view-details/'.$id)->with("err_msg", "No found.");
}

$book_statuss =ServiceBooking::find( $id );
$book_statuss->book_status ="cancelled";
$save = $book_statuss->save();

if( $save )
{    
    DB::table('ba_order_status_log')
    ->where("order_no",$id)
    ->increment('seq_no', 1);
    $order_status = new OrderStatusLogModel;
    $order_status->order_no  = $request->orderno;
    $order_status->order_status ="cancelled";
    $order_status->seq_no = 1;
    $order_status->log_date =date("Y-m-d H:i:s");
    if($request->remark != "")
    {
      $order_status->remarks =  $request->remark ;
  }
  $order_status->remarks =  "cancelled by merchant"; 
  
  
  $save=$order_status->save();

}

if ($save) {
  return redirect('/Erp/customer-care/order/view-details/'.$id)->with("err_msg", "order cancel successfully!");
}  
else 
{
   return redirect('/Erp/customer-care/order/view-details/'.$id)->with("err_msg","failed!") ;
} 
}
protected function setPreparationTime(Request $request)
{
  $bin = $request->bin;

  $order_no = $request->orderNo;
  $prep_time = $request->preparatiionTime;
  $set_prep_time = ServiceBooking::find($order_no);
  if( !isset($set_prep_time))
  {
     $data= ["message" => "failure",  "status_code" =>  999 ,
     'detailed_msg' => 'order information not found!'  ];
     return json_encode( $data);

 }
 if($set_prep_time->bin !=  $request->bin)
 {
    $data= ["message" => "failure", "status_code" =>  901 ,
    'detailed_msg' => 'Order does not belong to this Business'  ] ;
    return json_encode($data);
}
$set_prep_time->preparation_time =$prep_time;
$set_prep_time->is_confirmed ="yes";
$save = $set_prep_time->save();
if ($save)
{
   $data= ["message" => "sucess",  "status_code" =>  200 ,
   'detailed_msg' => 'preparation time is set'  ];
   return json_encode( $data);
} 
}
public function viewCompletedOrders(Request $request)
{

    $bin = $request->session()->get("_bin_");

    $dbstatus = $request->filter_status;


    if ($dbstatus!="") {
        if( $dbstatus == "-1")
        {
          $status = array( 'new','confirmed', 'cancel_by_client', 'canceled', 'delivered',
            'cancel_by_owner','order_packed','pickup_did_not_come','in_route',
            'package_picked_up','delivery_scheduled' ) ;
      }
      else
      {
          $status[] =  $dbstatus;
      }
  }
  else{
     $status = array( 'delivered', 'completed' ) ;
 }

 if ($request->filter_date!="") {

     $today =date('Y-m-d',strtotime($request->filter_date));

 }else{

     $today = date('Y-m-d' , strtotime("7 days ago"));
 }



 $today = date('Y-m-d' , strtotime("7 days ago"));

 $normal_orders = DB::table("ba_service_booking")
 ->where("bin",$bin)
 ->whereRaw("date(service_date) >= '$today' " )
 ->whereIn("book_status",$status)
 ->select("id", "service_date", "preferred_time",  "book_status",   DB::Raw("'na' customer_name"), "address", "landmark", "seller_payable as total_cost","payment_type as pay_mode" ,
  DB::Raw("'normal' order_type"))  ;


 $all_orders = DB::table("ba_pick_and_drop_order")
 ->where("request_by", $bin )
 ->whereRaw("date(service_date) >= '$today' " )
 ->whereIn("book_status",$status)
 ->select("id", "service_date", "delivery_time as preferred_time", "book_status",   "fullname as customer_name", "address", "landmark", "total_amount as total_cost", "pay_mode",
   DB::Raw("'pnd' order_type")  )
 ->union($normal_orders )
 ->get();

 $theme = DB::table("ba_theme")
 ->first();

 $data = array(
    'all_orders' =>  $all_orders ,
    'sub_menu' => 'order_sub_menu','theme'=>$theme
);

 return view("orders.view_completed_orders")->with(  $data  );

}


public function searchAllOrders(Request $request)
{

    $bin = $request->session()->get("_bin_");

    $dbstatus = $request->filter_status;

    if ($dbstatus!="") {
        if( $dbstatus == "-1")
        {
          $status = array( 'new','confirmed', 'cancel_by_client', 'canceled', 'delivered',
            'completed', 'cancel_by_owner','order_packed','pickup_did_not_come','in_route',
            'package_picked_up','delivery_scheduled' ) ;
      }
      else
      {
          $status[] =  $dbstatus;
      }
  }
  else{
     $status = array( 'new','confirmed', 'cancel_by_client', 'canceled', 'delivered',
        'cancel_by_owner','order_packed','pickup_did_not_come','in_route',
        'package_picked_up','delivery_scheduled' ) ;
 }


 if ($request->filter_date!="") {

     $today =date('Y-m-d',strtotime($request->filter_date));

 }else{

     $today = date('Y-m-d' , strtotime("7 days ago"));
 }



 $normal_orders = DB::table("ba_service_booking")
 ->where("bin",$bin)
 ->whereRaw("date(service_date) >= '$today' " )
 ->whereIn("book_status",$status)
 ->select("id", "service_date", "preferred_time",  "book_status",   DB::Raw("'na' customer_name"), "address", "landmark", "seller_payable as total_cost","payment_type as pay_mode" ,
  DB::Raw("'normal' order_type"))  ;


 $all_orders = DB::table("ba_pick_and_drop_order")
 ->where("request_by", $bin )
 ->whereRaw("date(service_date) >= '$today' " )
 ->whereIn("book_status",$status)
 ->select("id", "service_date", "delivery_time as preferred_time", "book_status",   "fullname as customer_name", "address", "landmark", "total_amount as total_cost", "pay_mode",
   DB::Raw("'pnd' order_type")  )
 ->union($normal_orders )
 ->get();

 $theme = DB::table("ba_theme")
 ->first();

 $data = array(
    'all_orders' =>  $all_orders ,
    'sub_menu' => 'order_sub_menu','theme'=>$theme
);

 return view("orders.view_completed_orders")->with(  $data  );

}




protected function viewPnDOrderDetails(Request $request)
{
  $orderno = $request->orderno;
  if($orderno == "")
  {
    return redirect("/admin/customer-care/business/view-all");
}


$order_info   = DB::table("ba_pick_and_drop_order")
->join("ba_business", "ba_pick_and_drop_order.request_by", "=", "ba_business.id")

->select("ba_pick_and_drop_order.*",
    "ba_business.name as businessName","ba_business.locality as businessLocality",
    "ba_business.landmark as businessLandmark", "ba_business.city as businessCity",
    "ba_business.state as businessState", "ba_business.pin as businessPin",
    "ba_business.phone_pri as businessPhone",  "ba_business.banner" )
->where("ba_pick_and_drop_order.id", $orderno )
->where("request_from", "business")
->first() ;

if( !isset($order_info))
{
    return redirect("/admin/customer-care/business/view-all"  );
}

$agent_info=  DB::table("ba_delivery_order")
->join("ba_profile", "ba_delivery_order.member_id", "=", "ba_profile.id")
->select("ba_profile.fullname as deliveryAgentName","ba_profile.phone as deliveryAgentPhone")
->where("order_no", $orderno  )
->where("order_type",  "pick and drop" )
->first();

$theme = DB::table("ba_theme")
->first();

$result = array('order_info' => $order_info ,    'agent_info' => $agent_info );
return view("orders.pnd_order_detailed_view")->with('data',$result)->with('theme',$theme);


}


public function prepareProductImportExcel(Request $request)
{

 $theme = DB::table("ba_theme")
 ->first();

 $data = array('Title' => "Product Import from Excel");
          //return json_encode( $data);
 return view("tools.product_import")->with('data',$data)->with('theme',$theme);

}



public function uploadProductImportExcel (Request $request)
{

    $bin = $request->session()->get("_bin_");
    $excel_path ="none";
    if(isset($request->file))
    {
       $file = $request->file;
       $originalname = $file->getClientOriginalName();
       $extension = $file->extension( );
       $filename = "products_" .  time(). ".{$extension}";
       $request->file->storeAs('uploads',  $filename );
       $imgUpload = File::copy(storage_path().'/app/uploads/'. $filename,
           public_path().'/upload/docs/'. $filename);
       $excel_path = public_path() . '/upload/docs/'. $filename ;

   }

   if(file_exists( $excel_path  ))
   {
      Excel::import(new ProductsImport  ,  $excel_path );
      return redirect('/tools/view-imported-products')->with('err_msg', 'Products are imported. Verify and publish them!');
  }

}


public function viewProductImportExcel (Request $request)
{

 $bin = $request->session()->get("_bin_");
 $business = Business::find($bin);
 if(!isset($business))
 {
     Auth::logout();
     $request->session()->flush();
     return redirect("/login")->with("err_msg",  "Session expired!");
 }

 $products= DB::table("ba_products_imported")
 ->where("bin", $bin)
 ->paginate(20);

 $theme = DB::table("ba_theme")
 ->first();
 

       //  $categories= DB::table("ba_product_category")
       // ->where("business_category",  $business->category )  
       // ->orderBy("category_name")
       // ->get();
 

        // $data = array('products' =>  $products , 'categories' => $categories,  'bin' => $bin);
 $data = array('products' =>  $products , 'bin' => $bin);
 return view("tools.importable_products")->with("data",$data)->with('theme',$theme);
}

public function saveImportableProducts(Request $request)
{
 $bin = $request->session()->get("_bin_");
 
 if(strcasecmp($request->btnsave, "save") == 0)
 {
   if(count($request->cb_select) > 0)
   {
    $product = array();

    $i=0;
    foreach($request->cb_select as $prod)
    {  
     $product[$i]['key'] = $prod;
     $product[$i]['barcode'] = $request->barcode[$i];
     $product[$i]['pr_name'] = $request->pr_name[$i];
     $product[$i]['pr_desc'] = $request->pr_desc[$i];
     $product[$i]['pr_price'] = $request->pr_price[$i];
     $product[$i]['pr_stock'] = $request->pr_stock[$i];
     $product[$i]['pr_category'] = $request->pr_category[$i];
     $i++;
 }

 $k=0;
 foreach($product as $items)
 {
                         //get product code 
   $id =   DB::table('ba_products') 
   ->max('id');  
   $prCode = "bpc".$bin."".($id + 1 );
   
                          //save product profile
   $product = new ProductModel;
   $product->bin = $bin ;
   $product->pr_code = $prCode ;
   $product->barcode = $items['barcode'];
   $product->pr_name = $items['pr_name'];
   $product->description = $items['pr_desc']; 
   $product->category = $items['pr_category'];
   $product->origin_country = "India";
   $save_product = $product->save();

                          //save product variant 
   if ($save_product) 
   {
    $discount =0;
    $discountPc = 0; 
    $productVariant = new ProductVariantModel;
    $productVariant->prid = $product->id;
    $productVariant->initial_stock= $items['pr_stock'];
    $productVariant->stock_inhand= $items['pr_stock'];
    $productVariant->min_required=  $items['pr_stock'];
    $productVariant->max_order= 1;   
    $productVariant->unit_value = 1 ;  
    $productVariant->unit_name = "PACK" ;
    $productVariant->discount =  $discount ;
    $productVariant->discountPc = $discountPc; 
    $productVariant->unit_price= $items['pr_price'];  
    $productVariant->actual_price = $items['pr_price'] - $discount ;    
    $save = $productVariant->save(); 
}

DB::table('ba_products_imported') 
->where('id', $items['key'])
->delete();  

$k++;
}
}
}
else if(strcasecmp($request->btnsave, "delete") == 0)
{
   if(count($request->cb_select) > 0)
   {
     DB::table("ba_products_imported")
     ->whereIn("id", $request->cb_select)
     ->delete();
 }

}

return redirect("/tools/view-imported-products")->with("err_msg", "All selected products are imported.");

}



public function saveImportableProduct(Request $request)
{
 $bin = $request->session()->get("_bin_");
 
 if(strcasecmp($request->btnsave, "save") == 0)
 {
          //get product code 
  $id =   DB::table('ba_products') 
  ->max('id');  
  $prCode = "bpc".$bin."".($id->id + 1 );
  
          //save product profile
  $product = new ProductModel;
  $product->bin = $bin ;
  $product->pr_code = $prCode ;
  $product->barcode = $request->barcode;
  $product->pr_name = $request->pname;
  $product->description = $request->desc; 
  $product->category = "OTHERS";
  $product->origin_country = "India";
  $save_product = $product->save();

          //save product variant 
  if ($save_product) 
  {
    $discount =0;
    $discountPc = 0; 
    $productVariant = new ProductVariantModel;
    $productVariant->prid = $product->id;
    $productVariant->initial_stock= $request->stock;
    $productVariant->stock_inhand= $request->stock;
    $productVariant->min_required=  $request->stock;
    $productVariant->max_order= 1;   
    $productVariant->unit_value = 1 ;  
    $productVariant->unit_name = "PACK" ;
    $productVariant->discount =  $discount ;
    $productVariant->discountPc  = $discountPc ; 
    $productVariant->unit_price= $request->uniprice;    
    $productVariant->actual_price = $request->uniprice - $discount ;    
    $save = $productVariant->save(); 
}
          //delete import entry 
DB::table('ba_products_imported') 
->where('id', $request->key)
->delete();  
}


return redirect("/tools/view-imported-products")->with("err_msg", "One product information saved.");

}




public function viewRecentlyAddedItems(Request $request)
{

    	//$bin=Auth::user()->bin;
  $bin = $request->session()->get("_bin_");
      // $bin = 194;


  $products = DB::table("ba_products")
  ->join("ba_product_variant","ba_products.id","=","ba_product_variant.prid")
  ->where("bin", $bin )
  ->orderBy("id", "desc")
  ->paginate(20) ;



  $allCategories= DB::table("ba_product_category")
  ->get();

  $theme = DB::table("ba_theme")
  ->first();

  $data = array('products' =>  $products ,  'allCategories' => $allCategories,  'bin' => $bin);

  return view("products.view_latest_entry")->with("data",$data)->with('theme',$theme);
}


public function viewAllProducts(Request $request)
{
    $bin = $request->session()->get("_bin_");
    $keyword = $request->keyword;
    $business = Business::find($bin);
    if(!isset($business))
    {
        Auth::logout();
        $request->session()->flush();
        return redirect("/login")->with("err_msg",  "Session expired!");
    }

    if($keyword != "" )
    {
        $products = DB::table("ba_products")
        ->join("ba_product_variant","ba_products.id","=","ba_product_variant.prid")
        ->where("bin", $bin )
        ->whereRaw("pr_name like '%$keyword%'")
        ->orderBy("pr_name", "asc")
        ->paginate(20);
    }
    else
    {
        $products = DB::table("ba_products")
        ->join("ba_product_variant","ba_products.id","=","ba_product_variant.prid")
        ->where("bin", $bin )
        ->orderBy("pr_name", "asc")
        ->paginate(20);

    }
    
        //dd($products);
    $allCategories= DB::table("ba_product_category")
    ->where("business_category", $business->category)
    ->get( );

    $theme = DB::table("ba_theme")
    ->first();
    
    $unit= DB::table('ba_product_units')
    ->select('id','unit')
    ->get();
    
    $product_categories = DB::table('ba_product_category')
    ->join("ba_business", "ba_business.category", "=", "ba_product_category.business_category")
    ->select('ba_product_category.category_name','ba_product_category.business_category')
    ->where("ba_business.id", $bin )
    ->get();
    $data = array('products' =>  $products ,  'allCategories' => $allCategories, 'product_categories' =>$product_categories, 'units'=>$unit, 'bin' => $bin);
    return view("products.all_products")->with("data",$data)->with('theme',$theme);
}


public function viewLowStockProducts(Request $request)
{
      //$bin=Auth::user()->bin;
  $bin = $request->session()->get("_bin_");
      // $bin = 194;


  $products = DB::table("ba_products")
  ->join("ba_product_variant","ba_products.id","=","ba_product_variant.prid")
  ->where("bin", $bin)
  ->whereRaw("(ba_product_variant.stock_inhand - ba_product_variant.min_required)<= 0" )
  ->orderBy("id", "desc")
  ->paginate(20);


  $allCategories= DB::table("ba_product_category")
  ->get();

  $theme = DB::table("ba_theme")
  ->first();

  $data = array('products' =>  $products ,  'allCategories' => $allCategories,
      'bin' => $bin, 'sub_menu'  => 'products_sub_menu', 'page_title' => 'Low Stock Products');

  return view("products.low_stock_products")->with("data",$data)->with('theme',$theme);
}


    // View business/shop products
protected function viewBusinessProducts(Request $request)
{



 $id = $request->bin;
 $category = $request->category;

 $productCategory= DB::table("ba_product_category")
 ->where("id",$category)
 ->first();



 $allCategories= DB::table("ba_product_category")
 ->get();




 $products= DB::table("ba_products")
 ->where("bin",$id)
 ->where("category",$productCategory->category_name)
 ->get();



 $theme = DB::table("ba_theme")
 ->first();


 $data = array('products' => $products,
  'allCategories' => $allCategories, 'category' => $productCategory);

 return view("erp.products.view_category_wise")->with('data',$data)->with('theme',$theme);

}


//Edit/Update Business products
public function editBusinessProducts(Request $request)
{
 $id = $request->pr_id;

 $product = ProductModel::find($id);

 $productCategory = DB::table("ba_product_category")
 ->get();

 $productUnits = DB::table("ba_product_units")
 ->get();

 $theme = DB::table("ba_theme")
 ->first();

 $data = array('product' => $product,
     'productCategory' => $productCategory,
     'productUnits' => $productUnits );


 return view("erp.business_product_edit")->with("data",$data)->with('theme',$theme);

}

protected function editBusinessProductSave(Request $request)
{

     	//$input = filter_input_array(INPUT_POST);
     // $input['id'];
 if( isset($request->btnSave) && $request->btnSave =="Save"):

    if( $request->productName == "" ||   $request->maxOrder == "" ||
      $request->productDescription == "" ||  $request->initialStock == "" ||
      $request->stockInHand  == "" ||  $request->minStock  == "" ||
      $request->categoryName== "" || $request->unitPrice == "")
    {

       return back()->with("err_msg", "Missing data to perform action.");
   }

   $product = ProductModel::find($input['id'] ) ;

   if(  !isset($product) )
   {

    return back()->with("err_msg", "No product information found.");
}


$product->pr_name = $request->productName;
$product->description = $request->productDescription;
$product->initial_stock = $request->initialStock;
$product->stock_inhand = $request->stockInHand;
$product->min_required = $request->minStock;
$product->category = $request->categoryName;
$product->max_order = $request->maxOrder;
$product->unit_price = $request->unitPrice;
$save=$product->save();


if($save)
{

    return back()->with("err_msg", "Product updated successfully.");
}
else
{

    return back()->with("err_msg", "Product could not be updated.");
}

else:

  return back();

endif;

}

    //Delete/Destroy Business products
public function destroyBusinessProducts(Request $request)
{
    $id=$request->pr_id;

    $products = ProductModel::where('id',$id)->get();
    if(count($products)>0)
    {
        foreach ($products as $item)
        {
            File::delete('public'.$item->photos);
            $productInfo = ProductModel::find($item->id);
            $productInfo->delete();
        }
        $data= ['err_msg' => 'Delete product success.'  ];

    }
    else{
        $data= ['err_msg' => 'Delete product failed.'  ];
    }

    return back();

}



   // View Business Earning Report for daily
protected function viewBusinessDailyEarning(Request $request  )
{

  $bin = $request->session()->get("_bin_");


  if($request->filter_date == "" )
  {

    $today =   date('Y-m-d');

}
else
{
    $today = date('Y-m-d', strtotime( $request->filter_date ));
}



$totalEarning = 0;

$business_info  = Business::find( $bin);

if( !isset($business_info ))
{

  return redirect("/erp");
}

$business_owner_info  = DB::table("ba_profile")
->join("ba_users", "ba_users.profile_id", "=", "ba_profile.id" )
->where("ba_users.bin",   $bin)
->select("ba_profile.*")
->first();


if( !isset($business_owner_info ))
{
  return redirect("/erp")->with("err_msg", "No businss found.");
}


$status = [  'completed', 'delivered'  ];

$booking_list  = DB::table("ba_service_booking")
->join("ba_profile", "ba_service_booking.book_by", "=", "ba_profile.id")
->where("ba_service_booking.bin", $bin )
->whereIn("ba_service_booking.book_status",  $status )
->whereDate('ba_service_booking.service_date', $today)
->select("ba_service_booking.id as orderNo",
   "ba_profile.fullname as orderBy",
   'book_status as orderStatus',
   "total_cost as totalCost",
   'delivery_charge as deliveryCharge'  )
->get();

foreach ($booking_list as $item)
{
    $totalEarning += $item->totalCost;

}



$paymentInfo  = DB::table("ba_payment_dealings")
->join("ba_profile", "ba_profile.id", "=", "ba_payment_dealings.member_id")
->where("member_id", $business_owner_info->id )
->whereRaw("date(bill_date) ='$today'")
->get();

$theme = DB::table("ba_theme")
->first();

$data = array(

    'business' => $business_info , 'owner_profile' => $business_owner_info,
    "totalEarning" => $totalEarning, 'payments' =>  $paymentInfo,  'results' =>  $booking_list);


return view("erp.business_daily_earning")->with('data',$data)->with('theme',$theme);
}


    // View Business Earning Report for monthly
protected function viewBusinessMonthlyEarning(Request $request)
{

 $bin = $request->session()->get("_bin_");

 if($request->year != "")
 {
    $year  = $request->year ;
}
else
{
    $year  = date('Y' );
}

if($request->month !="" )
{

    $month = $request->month ;
}
else
{
    $month = date('m' );
}

$totalEarning = 0;


$business_info  = Business::find( $bin);

if( !isset($business_info ))
{

  return redirect("/erp");
}

$business_owner_info  = DB::table("ba_profile")
->join("ba_users", "ba_users.profile_id", "=", "ba_profile.id" )
->where("ba_users.bin",   $bin)
->select("ba_profile.*")
->first();


if( !isset($business_owner_info ))
{
  return redirect("/erp")->with("err_msg", "No businss found.");
}


$status = [  'completed', 'delivered'  ];

$booking_list  = DB::table("ba_service_booking")
->join("ba_profile", "ba_service_booking.book_by", "=", "ba_profile.id")
->where("ba_service_booking.bin", $bin )
->whereIn("ba_service_booking.book_status",  $status )
->whereMonth('ba_service_booking.service_date', $month )
->whereYear('ba_service_booking.service_date', $year )
->select("ba_service_booking.id as orderNo",
   "ba_profile.fullname as orderBy",
   'book_status as orderStatus',
   "total_cost as totalCost",
   'delivery_charge as deliveryCharge'  )
->get();

foreach ($booking_list as $item)
{
    $totalEarning += $item->totalCost;

}



$paymentInfo  = DB::table("ba_payment_dealings")
->join("ba_profile", "ba_profile.id", "=", "ba_payment_dealings.member_id")
->where("member_id", $business_owner_info->id )
->whereMonth('bill_date', $month )
->whereYear('bill_date', $year )
->get();

$theme = DB::table("ba_theme")
->first();

$data = array(

    'business' => $business_info , 'owner_profile' => $business_owner_info,
    "totalEarning" => $totalEarning, 'payments' =>  $paymentInfo,  'results' =>  $booking_list );

return view("erp.business_monthly_earning")->with('data',$data)->with('theme',$theme);

}




     //update single column in the product table
protected function updateProductField(Request $request)
{
    if( $request->pid == "" || $request->colName == "" ||  $request->colValue == ""  )
    {
       $data= ["message" => "failure", "status_code" =>  902 , 'detailed_msg' => 'Missing data to perform action.'  ];
       return json_encode($data);
   }

   $pid = $request->pid;
   $colName  = $request->colName;
   $colValue = $request->colValue;



   $product = ProductModel::find($pid);

   if( !isset($product) )
   {
    $data= ["message" => "failure", "status_code" =>  999 , 'detailed_msg' => 'No product information found.'  ];
    return json_encode($data);
}

DB::table("ba_products")
->where("id", $pid )
->update([$colName=>$colValue]);

$data= ["message" => "success", "status_code" =>  5092 ,  'detailed_msg' => 'Update successfull.'];
return json_encode($data);
}


protected function viewOrderDetails(Request $request)
{

 $orderno = $request->orderno;


 if($orderno == "")
 {
    return redirect("/orders/view-active-orders");
}

$ordersequence = OrderSequencerModel::find(  $orderno) ;


if( !isset($ordersequence))
{

    return redirect("/orders/view-active-orders");
}



$theme = DB::table("ba_theme")
->first();

if($ordersequence->type == "normal")
{
    $order_info   = DB::table("ba_service_booking")
    ->join("ba_business", "ba_service_booking.bin", "=", "ba_business.id")
    ->select("ba_service_booking.*", "ba_business.name as businessName",
      "ba_business.locality as businessLocality", "ba_business.landmark as businessLandmark",
      "ba_business.city as businessCity", "ba_business.state as businessState",
      "ba_business.pin as businessPin", "ba_business.phone_pri as businessPhone",
      "ba_business.banner" )
    ->where("ba_service_booking.id", $orderno )
    ->get();

    if( !isset($order_info))
    {
      return redirect("/orders/view-active-orders");
  }

  $customer_info   = DB::table("ba_profile")
  ->where("id", $order_info->book_by )
  ->first() ;

  if( !isset($customer_info))
  {
      return redirect("/orders/view-active-orders");
  }

  $order_items   = DB::table("ba_shopping_basket")
  ->join("ba_products","ba_shopping_basket.pr_code","=","ba_products.pr_code")
  ->where("order_no", $orderno )
  ->select("ba_shopping_basket.*","ba_products.*", DB::Raw("'' photos"), "ba_products.pr_code")
  ->get() ;

  if( !isset($order_items))
  {
      return redirect("/orders/view-active-orders");
  }

  $agent_info=  DB::table("ba_delivery_order")
  ->join("ba_profile", "ba_delivery_order.member_id", "=", "ba_profile.id")
  ->select("ba_profile.fullname as deliveryAgentName","ba_profile.phone as deliveryAgentPhone")
  ->where("order_no", $orderno  )
  ->first();



  $result = array('order_info' => $order_info , 'customer' =>  $customer_info,
    'order_items' => $order_items, 'agent_info' => $agent_info ,'order_type'=>$ordersequence->type );
  return view("orders.order_details_view")->with('data',$result)->with('theme',$theme);

}
elseif($ordersequence->type == "booking")
{
  $order_info   = DB::table("ba_service_booking")
  ->join("ba_business", "ba_service_booking.bin", "=", "ba_business.id")
  ->select("ba_service_booking.*", "ba_business.name as businessName",
    "ba_business.locality as businessLocality", "ba_business.landmark as businessLandmark",
    "ba_business.city as businessCity", "ba_business.state as businessState",
    "ba_business.pin as businessPin", "ba_business.phone_pri as businessPhone",
    "ba_business.banner" )
  ->where("ba_service_booking.id", $orderno )
  ->first() ;

  if( !isset($order_info))
  {
    return redirect("/orders/view-active-orders");
}

$customer_info   = DB::table("ba_profile")
->where("id", $order_info->book_by )
->first() ;

if( !isset($customer_info))
{
    return redirect("/orders/view-active-orders");
}

$booking_details   = DB::table("ba_service_booking")
->join("ba_service_booking_details","ba_service_booking.id","=","ba_service_booking_details.book_id")
->join("ba_service_products","ba_service_booking_details.service_product_id",
  "=","ba_service_products.id")
->where("ba_service_booking.id", $orderno )
->select()
->get() ;


$staff = DB::table('ba_service_booking')
->join('ba_users' ,'ba_service_booking.staff_id', '=', 'ba_users.profile_id')
->join('ba_service_booking_details','ba_service_booking.id', '=', 'ba_service_booking_details.book_id')
->join('ba_profile','ba_users.profile_id','=','ba_profile.id')
->where("ba_service_booking.id",'=',$orderno)
->select('ba_profile.fullname','ba_profile.email')
->groupby('ba_profile.fullname','ba_profile.email')
->get();

$result_venue = DB::table('ba_service_booking')
->join('ba_business','ba_service_booking.bin','=', 'ba_business.id')
->where("ba_service_booking.id",'=',$orderno)
->select()
->get();



$result = array('order_info' => $order_info , 'customer' =>  $customer_info,
    'booking'=>$booking_details,'order_type'=>$ordersequence->type,
    'staff'=>$staff,'venue'=>$result_venue );

return view("orders.order_details_view")->with('data',$result)->with('theme',$theme);


}elseif ($ordersequence->type == "pnd") {

    return redirect('admin/customer-care/pnd-order/view-details/'.$orderno);

}




}

public function viewPrintableReceipt( Request $request)
{

 $order_info = $order_items =null;
 if( $request->o  == ""  )
 {
   return ;
}



$orderno = $request->o;
$order_info = ServiceBooking::find( $request->o );
$order_items   = DB::table("ba_shopping_basket")
->join("ba_products","ba_shopping_basket.pr_code","=","ba_products.pr_code")
->where("order_no", $orderno )
->select("ba_shopping_basket.*","ba_products.photos as image" )
->get() ;


$customer   = DB::table("ba_profile")
->where("id", $order_info->book_by )
->first() ;



$public_path = public_path();

$folder_url  =  URL::to('/public')  .  '/assets/bills/btm-' .  $order_info->bin .  "/"     ;
$folder_path =   $public_path  .  '/assets/bills/btm-' .  $order_info->bin .  "/"     ;

if( !File::isDirectory($folder_path)){
   File::makeDirectory( $folder_path, 0777, true, true);
}

$pdf_file =  "btr-" . $request->o . ".pdf";
$save_path = $folder_path .  $pdf_file  ;



$pdf = PDF::setOptions(['defaultFont' => 'sans-serif',   'isRemoteEnabled' => true  ])
->setPaper('a4', 'portrait')
->loadView('docs.ebill' ,  array('data' =>   $order_info , 'order_items' => $order_items, 'customer'=>$customer  )  )
->save($save_path )  ;
$url = URL::to(  $folder_url  .  $pdf_file  )  ;
return redirect($url);
}


public function generatePrintableReceipt( Request $request)
{

 $order_info = $order_items =null;
 if( $request->o  != ""  )
 {
    $orderno = $request->o;
    $order_info = ServiceBooking::find( $request->o );
    $order_items   = DB::table("ba_shopping_basket")
    ->join("ba_products","ba_shopping_basket.pr_code","=","ba_products.pr_code")
    ->where("order_no", $orderno )
    ->select("ba_shopping_basket.*","ba_products.photos as image")
    ->get() ;
}
else
{
    return ;
}
$public_path = public_path();

$folder_url  =  URL::to('/public')  .  '/assets/bills/btm-' .  $order_info->bin .  "/"     ;
$folder_path =   $public_path  .  '/assets/bills/btm-' .  $order_info->bin .  "/"     ;

if( !File::isDirectory($folder_path)){
    File::makeDirectory( $folder_path, 0777, true, true);
}

$pdf_file =  "btr-" . $request->o . ".pdf";
$save_path = $folder_path .  $pdf_file  ;

$pdf = PDF::setOptions(['defaultFont' => 'sans-serif'  ])
->setPaper('a4', 'portrait')
->loadView('docs.ebill' ,  array('data' =>   $order_info , 'order_items' => $order_items )  )
->save($save_path )  ;

$url = URL::to(  $folder_url  .  $pdf_file  )  ;

$data= ["message" => "success", "status_code" =>  6001 , 'detailed_msg' =>   'eBill generated.' , 'url' =>  $url ];
return json_encode($data);

}


//receipt not purchase
public function generateServiceBill( Request $request)
{

  $order_info = $order_items =null;
  if( $request->o  != ""  )
  {
    $orderno = $request->o;
    $order_info =  DB::table("ba_service_booking")
    ->join("ba_profile","ba_profile.id","=","ba_service_booking.book_by")
    ->where("ba_service_booking.id", $orderno )
    ->select("ba_service_booking.*","ba_profile.fullname", "ba_profile.phone", "ba_profile.locality")
    ->first() ;

    $order_items   = DB::table("ba_service_booking_details")
    ->where("ba_service_booking_details.book_id", $orderno )
    ->get() ;

}
else
{
    return;
    //return redirect("/services/new-bookings")->with("err_msg", "Failed to download invoice!");
}

$public_path = public_path();

$folder_url  =  URL::to('/public')  .  '/assets/bills/btm-' .  $order_info->bin .  "/"     ;
$folder_path =   $public_path  .  '/assets/bills/btm-' .  $order_info->bin .  "/"     ;

if( !File::isDirectory($folder_path)){
    File::makeDirectory( $folder_path, 0777, true, true);
}

$pdf_file =  "btr-" . $request->o . ".pdf";
$save_path = $folder_path .  $pdf_file  ;

$pdf = PDF::setOptions(['defaultFont' => 'sans-serif'  ])
->setPaper('a4', 'portrait')
->loadView('docs.ebill_services' ,  array('data' =>   $order_info , 'order_items' => $order_items )  )  ;


return $pdf->download('ebill-' . $request->o  .   mt_rand(0,20)  .    '.pdf');


}
     // Order Processing (confirmed, order_packed and pickup_did_not_come)
public function updateOrderStatus(Request $request)
{

  $bin = $request->session()->get("_bin_");

  if ( isset($request->btnSave) && $request->btnSave =="Save")
  {



    if( $request->order_no == ""  ||  $request->orderStatus == "" ||
        ( $request->orderStatus != "confirmed" && $request->orderStatus != "completed" &&
            $request->orderStatus != "order_packed"  && $request->orderStatus != "pickup_did_not_come"   ) )
    {
       return back()->with("err_msg", 'Missing data to perform action.');
   }


        //find order
   $order_info  = ServiceBooking::find( $request->order_no  );
   if( !isset($order_info))
   {
    return back()->with("err_msg", 'Order Infomation not found!');
}


if( $bin   !=  $order_info->bin   )
{
  return back()->with("err_msg", 'Selected order not found!');
}

$order_info->book_status = $request->orderStatus;
$save = $order_info->save();

if( $save )
{

 if($request->orderStatus == "order_packed" )
 {
            //update stock level
    $order_items =  DB::table('ba_shopping_basket')
    ->where('order_no',   $request->order_no  )
    ->get();

    foreach($order_items as $item)
    {
      DB::table('ba_products')
      ->where('pr_code',  $item->pr_code )
      ->decrement('stock_inhand',  $item->qty );

  }

}


DB::table('ba_order_status_log')
->where('order_no', $order_info->id)
->increment('seq_no', 1);

$updateOrderStatus = new OrderStatusLogModel;
$updateOrderStatus->order_no = $order_info->id;
$updateOrderStatus->order_status= $order_info->book_status;
$updateOrderStatus->log_date=  date('Y-m-d H:i:s' );
$updateOrderStatus->save();


return back()->with("err_msg", 'Order status is successfully updated.');

}
else
{

  return back()->with("err_msg", 'Order status update failed.');
}


}



}


public function addNewOrder(Request $request)
{
 $bin = $request->session()->get("_bin_");

 $productCategory = DB::table("ba_product_category")
 ->get();

 $products = DB::table("ba_products")
 ->where("bin",$bin)
 ->get();

 $productUnits = DB::table("ba_product_units")
 ->get();

 $theme = DB::table("ba_theme")
 ->first();
 $data = array('productCategory' => $productCategory,
  'products' => $products,
  'productUnits' => $productUnits);


 return view("erp.orders.add_new_orders")->with("data",$data)->with('theme',$theme);

}

protected function saveNewOrder(Request $request)
{

 if( isset($request->btnSave) && $request->btnSave =="Save"):

    if( $request->customerName == "")
    {

       return back()->with("err_msg", "Missing data to perform action.");

   }

   $profile = new CustomerProfileModel;
   $name = $request->customerName;
   $arr2 = explode(" " , $name);
   $totalname = count( $arr2 );

   if($totalname == 3)
   {
    $profile->fname = $arr2[0];
    $profile->mname = $arr2[1];
    $profile->lname = $arr2[2];
}
else if($totalname == 2)
{
    $profile->fname = $arr2[0];
    $profile->lname = $arr2[1];
    $profile->mname = null ;
}

$profile->fullname = $name;
$profile->phone = $request->phone;
$profile->locality = $request->address;
$profile->latitude = 0;
$profile->longitude = 0;
$profile->rating = 0;
$save = $profile->save();

if ($save!=0) {

    $new_order = new ServiceBooking ;
    $new_order->bin = $request->session()->get("_bin_");
    $new_order->book_category = "VARIETY STORE";
    $new_order->book_by= $profile->id;
    $new_order->staff_id = 0;
    $new_order->address = $profile->locality;
    $new_order->landmark = $request->landmark;
    $new_order->city = $request->city;
    $new_order->state = $request->state;
    $new_order->pin_code  = $request->pinCode;
    $new_order->book_status = "completed";
    $new_order->total_cost = 0;
    $new_order->otp = 0;
    $new_order->delivery_charge = 0;
    $new_order->agent_payable  = 0;
    $new_order->cashback =  0;
    $new_order->payment_type = "Cash on Delivery";
    $new_order->latitude = 0;
    $new_order->longitude = 0;
    $new_order->book_date =  date('Y-m-d H:i:s');
    $new_order->service_date =  date('Y-m-d H:i:s');
    $save_order = $new_order->save();

    if($save_order)
    {

        $dataSet = [];
        if ($request->productName>0)
        {


          foreach($request->productName as $i => $productName)
          {



           $dataSet[] = [
            'order_no' => $new_order->id,
            'pr_code' => "test",
            'pr_name' => $productName,
            'qty' => $request->quantity[$i],
            'price' => $request->unitPrice[$i],
            'category_name' => "test",

        ];


        DB::table('ba_shopping_basket')->insert($dataSet);


    }

}


}
return back()->with("err_msg", "Sales updated successfully.");
}




else
{

    return back()->with("err_msg", "Product could not be updated.");
}



else:

  return back();

endif;

}



public function manageServices(Request $request)
{

    $bin = $request->session()->get("_bin_");
    $status = array( 'delivered','completed' ) ;
    $today = date('Y-m-d');

    $normal_orders = DB::table("ba_service_booking")
    ->where("bin",$bin)
    ->whereRaw("date(service_date) >= '$today' " )
    ->select("id", "service_date", "preferred_time",  "book_status",   DB::Raw("'na' customer_name"), "address", "landmark", "seller_payable as total_cost","payment_type as pay_mode" ,
      DB::Raw("'normal' order_type") )  ;


    $all_orders = DB::table("ba_pick_and_drop_order")
    ->where("request_by", $bin )
    ->whereRaw("date(service_date) >= '$today' " )
    ->select("id", "service_date", "delivery_time as preferred_time", "book_status",   "fullname as customer_name", "address", "landmark", "total_amount as total_cost", "pay_mode",
       DB::Raw("'pnd' order_type")  )
    ->union($normal_orders )
    ->get();

    $theme = DB::table("ba_theme")
    ->first();

    $data = array('all_orders' =>  $all_orders,'theme'=>$theme );

    return view("orders.view_all_active")->with(  $data  );

}




public function viewProductImages(Request $request)
{
 $bin = $request->session()->get("_bin_");

 $key = $request->key;
 $product_variant= DB::table("ba_product_variant")
 ->where("prsubid",  $key)
 ->first();

 if(   !isset($product_variant))
 {
   return redirect("/all-products")->with("err_mgs", "No product variant found!");
}

$product_info= DB::table("ba_products")
->where("id",  $product_variant->prid)
->first();

if(   !isset($product_info))
{
    return redirect("/all-products")->with("err_mgs", "No product found!");
}


if($product_info->bin != $bin)
{
    return redirect("/all-products")->with("err_mgs", "Product selected does not belong to your business!");
}


$product_photos= DB::table("ba_product_photos")
->where("prsubid", $key)
->get();

$theme = DB::table("ba_theme")
->first();

$data = array(
 'title'  => 'Manage Product Image',
 'product_info' => $product_info,
 'product_variant' => $product_variant,
 'product_photos'=>$product_photos,
 'key' => $key
);
return view("products.view_product_image")->with('data',$data)->with('theme',$theme);

}

public function uploadProductImage(Request $request)
{
    $bin = $request->session()->get("_bin_");

    $key = $request->key;

    if(  $key == ""   )
    {
       return redirect("/product/edit-images?key=".$key  )->with('detailed_msg', 'Missing data to perform action.' );
   }

   $product_variant = ProductVariantModel::find(  $key) ;


   if(   !isset($product_variant))
   {
    return redirect("/all-products")->with("err_mgs", "No product variant found!");
}

$product_info =  ProductModel::find(  $product_variant->prid ) ;
if(   !isset($product_info))
{
    return redirect("/all-products")->with("err_mgs", "No product found!");
}

if($product_info->bin != $bin)
{
  return redirect("/all-products")->with("err_mgs", "Product selected does not belong to your business!");
}

$cdn_url =   config('app.app_cdn')    ;
      $cdn_path =  config('app.app_cdn_path')  ; // cnd - /var/www/html/api/public
      $no_image  =   $cdn_url .  "/assets/image/no-image.jpg";


      $folder_url  =  $cdn_url . '/assets/image/store/bin_'. $product_info->bin    ;
      $folder_path =  $cdn_path  . '/assets/image/store/bin_'.   $product_info->bin  ; 
      if( !File::isDirectory($folder_path))
      {
          File::makeDirectory( $folder_path, 0777, true, true);
      }


      $i=1;
      foreach($request->file('photos') as $file)
      {
        
        $originalname = $file->getClientOriginalName();
        $extension = $file->extension( );
        $filename = "prim_" .  $product_variant->id . $i . time()  . ".{$extension}";
        $file->storeAs('uploads',  $filename );
        $imgUpload = File::copy(storage_path().'/app/uploads/'. $filename, $folder_path . "/" . $filename);
        $product_photos = new ProductPhotosModel ;
        $product_photos->prid = $product_info->id;
        $product_photos->prsubid = $product_variant->prsubid;
        $product_photos->image_url  = $folder_url . "/" . $filename;
        $product_photos->image_path = $filename;
        $product_photos->save();
        $i++;
    }
    
    return redirect("/product/edit-images?key=".$key );

}




//Remove product image. Delete image from the folder also
public function removeProductImage(Request $request)
{
    $bin = $request->session()->get("_bin_");
    $prsubid = $request->key;
    $imageid = $request->imageid;
    if(  $prsubid == ""   ||  $imageid == "" )
    {
        return redirect("/product/edit-images?key=".$key  )->with('detailed_msg', 'Missing data to perform action.' );
    }

    $product_photo = ProductPhotosModel::find(  $imageid) ;
    if(   !isset($product_photo))
    {
        return redirect("/all-products")->with("err_mgs", "No product photo found!");
    }

    $product_info =  DB::table("ba_product_photos")
    ->join("ba_products", "ba_products.id", "=", "ba_product_photos.prid")
    ->where("ba_product_photos.id",  $imageid )
    ->select("ba_products.*")
    ->first();

    if(   !isset($product_info))
    {
        return redirect("/all-products")->with("err_mgs", "No product information found!");
    }

    //removing from database
    $product_photo->delete();

    //removing from folder
    $cdn_path =  config('app.app_cdn_path')  ;
    $file_path  = $cdn_path  . '/assets/image/store/bin_'.   $product_info->bin . "/" . $product_photo->image_path  ;



    if($product_photo->image_path  != "" && File::exists($file_path))
    {
        File::delete($file_path);
    }
    else
    {
        $file_path  = $cdn_path  .   $product_photo->image_path  ;
        File::delete($file_path);
    }


    return redirect("/product/edit-images?key=".$prsubid );

}

public function addServiceDetails(Request $request)
{

  $bin = $request->session()->get("_bin_");
  $bookable = DB::table("ba_bookable_category")
  ->where("main_module","APPOINTMENT")
  ->get();

  $book_submodule=array();

  foreach($bookable as $items)
  {
    $book_submodule[]=$items->sub_module;
}

$service_categories =  DB::table("ba_service_category")
->where("bookable_category",$book_submodule)
->get();

$theme = DB::table("ba_theme")
->first();

$data = array('service_categories' =>  $service_categories,
  'bin' =>$bin,
  'page_title' => 'Add New Service Services','theme'=>$theme );
return view("services.add_service_product")->with(  $data  );

}


public function editServiceDetails(Request $request)
{

  $bin = $request->session()->get("_bin_");

  if( $request->serviceid == ""  )
  {
    return redirect("/services/manage-services")->with("err_msg",   'Missing data to perform action.'  );
}

$service_categories =  DB::table("ba_service_category")
->get();
$service_product =  BusinessServiceProduct::find( $request->serviceid );

if( !isset($service_product)  )
{
  return redirect("/services/manage-services")->with("err_msg",   'No service information found.'  );
}

$theme = DB::table("ba_theme")
->first();

$data = array('service_categories' =>  $service_categories,
  'service_product' => $service_product,
  'bin' =>$bin,
  'serviceid' => $request->serviceid ,
  'page_title' => 'Manage Listed Services','theme'=>$theme );
return view("services.edit_service_product")->with(  $data  );

}


public function saveServiceDetails(Request $request)
{

  $bin = $request->session()->get("_bin_");

  $service_product =  BusinessServiceProduct::where("service_category",  $request->category )
  ->where("bin",  $bin )
  ->where("srv_name", $request->service )
  ->where("pricing", $request->minprice )
  ->where("gender", $request->gender )
  ->where("duration_hr", $request->hour )
  ->where("duration_min", $request->minute )
  ->where("service_type", $request->servicetype )
  ->first();


  if( isset($service_product)  )
  {
    return redirect("/services/add-new-service")->with("err_msg",   'A similar service already exists.'  );
}

$service_product = new  BusinessServiceProduct;
$service_product->bin  = $bin ;
$service_product->service_category = $request->category;
$service_product->srv_name = $request->service;
$service_product->srv_details = $request->details;
$service_product->pricing = $request->minprice;
$service_product->max_price = $request->maxprice;
$service_product->actual_price = $request->maxprice;
$service_product->duration_hr =   $request->hour  ;
$service_product->duration_min =   $request->minute  ;
$service_product->service_type = $request->servicetype;
$service_product->gender = $request->gender;

if ($request->category=="OFFER") {
    $service_product->valid_start_date = date('Y-m-d',strtotime($request->valid_from)); 
    $service_product->valid_end_date = date('Y-m-d',strtotime($request->valid_to));  
    $service_product->discount = $request->discount;
}

$service_product->save();

return redirect("/services/add-new-service")->with("err_msg",   'Service information updated!'  );

}

protected function viewServiceDetails(Request $request)
{
  $bin = $request->session()->get("_bin_");

  $bookable = DB::table("ba_bookable_category")
  ->where("main_module","APPOINTMENT")
  ->get();

  $book_submodule=array();

  foreach($bookable as $items)
  {
    $book_submodule[]=$items->sub_module;
}

$service_categories =  DB::table("ba_service_category")
->where("bookable_category",$book_submodule)
->get();

if ($request->stype=="") {
  $data = array('service_categories' =>  $service_categories,
    'bin' =>$bin ); 
}else{

    $result = DB::table('ba_service_products')
    ->where('bin',$bin)
    ->where('service_category',$request->stype)
    ->get();

    $data = array('service_categories' =>  $service_categories,
        'bin' =>$bin,'result'=>$result);
}


return view('services.manage_service')->with($data);

}

public function updateServiceDetails(Request $request)
{

  $bin = $request->session()->get("_bin_");
  if( $request->serviceid == ""  )
  {
    return redirect("/services/view-service-details")->with("err_msg",   'Missing data to perform action.'  );
}

$service_product =  BusinessServiceProduct::find( $request->serviceid );
if( !isset($service_product)  )
{
    return redirect("/services/view-service-details")->with("err_msg",   'No service information found.'  );
}

$service_product->service_category = $request->category;
$service_product->srv_name = $request->service;
$service_product->srv_details = $request->details;
$service_product->pricing = $request->minprice;
$service_product->max_price = $request->maxprice;
$service_product->actual_price = $request->maxprice;
$service_product->duration_hr =   $request->hour  ;
$service_product->duration_min =   $request->minute  ;
$service_product->service_type = $request->servicetype;
$service_product->gender = $request->gender;

if ($request->category=="OFFER") {
    $service_product->valid_start_date = date('Y-m-d',strtotime($request->valid_from)); 
    $service_product->valid_end_date = date('Y-m-d',strtotime($request->valid_to));  
    $service_product->discount = $request->discount;
}

$service_product->save();

return redirect("/services/view-service-details")->with("err_msg",   'Service information updated!'  );

}



public function saveNewBooking(Request $request)
{

  $bin = $request->session()->get("_bin_");
  $business = Business::find($bin );

  $member_id = $request->session()->get("_member_id");


  if( !isset($business))
  {
   return redirect("/services/add-new-booking")->with("err_msg" , 'Business information not found!' );
}

if( $request->serviceDate == "" || $request->staffId == "" || $request->serviceProductId == "" || $request->serviceTimeSlots == "")
{
   return redirect("/services/add-new-booking")->with("err_msg" ,  'Missing data to perform action.'  );
}

      //save sequencer
$keys = array('a', 'A', 'b', 'b', 'X', 'u', 'W', 'w', 'e', 'G');
$rp_1 = mt_rand(0, 9);
$rp_2 = mt_rand(0, 9);
$tracker  = $keys[$rp_1] . $keys[$rp_2] .  time() ;
$sequencer = new OrderSequencerModel;
$sequencer->type = "service";
$sequencer->tracker_session =  $tracker;
$save = $sequencer->save();

if( !isset($save) )
{
    return redirect("/services/add-new-booking")->with("err_msg" ,   'Failed to generate order no sequence.'  );
}

      //check if slot is available


$otp = mt_rand(111111, 999999);
$newbooking = new ServiceBooking;
$newbooking->id = $sequencer->id ;
$newbooking->bin =   $bin ;
$newbooking->book_category= $business->category;
$newbooking->book_by =   $member_id;
$newbooking->staff_id = $request->staffId;
$newbooking->otp  =  $otp ;
$newbooking->address = "na";
$newbooking->landmark = "na";
$newbooking->city = "na";
$newbooking->state = "na";
$newbooking->pin_code  = "na";
$newbooking->book_status = "new";
$newbooking->total_cost =  $newbooking->delivery_charge  =  0.00;
$newbooking->agent_payable  = $newbooking->service_fee  =  0.00;
$newbooking->cashback =  0;
$newbooking->payment_type = "OTC";
$newbooking->latitude =  $newbooking->longitude =  0;
$newbooking->book_date =  date('Y-m-d H:i:s');

if(strcasecmp($request->bookFlag, "TODAY") == 0 )
{
 $newbooking->service_date =  date('Y-m-d H:i:s');
}
else
{
       $newbooking->service_date = date('Y-m-d H:i:s'  , strtotime($request->serviceDate )); //service on specified date
   }

   $newbooking->cust_remarks =  ($request->bookingRemark != "" ? $request->bookingRemark : null);
   $newbooking->transaction_no =    "na" ;
   $newbooking->reference_no =    "na"  ;
   $save=$newbooking->save();

   if($save)
   {
     $serviceProductIds =  array_filter(explode( ",",  $request->serviceProductIds));
     $serviceTimeSlots =  array_filter(explode( ",",  $request->serviceTimeSlots));
     $pos =0;
     foreach ($serviceProductIds as $item)
     {
       $newbookingDetails = new ServiceBookingDetails;
       $newbookingDetails->book_id = $sequencer->id;  ;
       $newbookingDetails->service_product_id = trim(  $item  )   ;
       $newbookingDetails->service_time = date('H:i:s', strtotime( $serviceTimeSlots[$pos] ));
       $newbookingDetails->status = "new";
       $newbookingDetails->save();
       $pos++;
   }


   return redirect("/services/view-bookings")->with("err_msg" ,   "Your booking is placed successfully." );

}
else{


    return redirect("/services/view-bookings")->with("err_msg" ,  "Failed to place your booking details." );
}


} 


public function viewQueue(Request $request)
{
    $bin = $request->session()->get("_bin_");

    $today = date('Y-m-d');

    if($request->filter_date)
    {
        $today = date('Y-m-d');
        $normal_orders = DB::table("ba_service_booking")
        ->join("ba_order_sequencer", "ba_order_sequencer.id", "=", "ba_service_booking.id")
        ->where("bin",$bin)
        ->where("ba_order_sequencer.type", "booking")
        ->whereDate("service_date", $today )
        ->whereNull("ba_service_booking.preferred_time" )
        ->select("ba_service_booking.id", "service_date", "book_by", "book_date", "service_date" , "book_status",   DB::Raw("'na' customer_name"), "address", "landmark", "total_cost","payment_type as pay_mode" ,
           "ba_order_sequencer.type"   )
        ->paginate(20) ;
    }
    else
    {
        $normal_orders = DB::table("ba_service_booking")
        ->join("ba_order_sequencer", "ba_order_sequencer.id", "=", "ba_service_booking.id")
        ->where("bin",$bin)
        ->where("ba_order_sequencer.type", "booking")
        ->whereNull("ba_service_booking.preferred_time" )
        ->whereDate("service_date", $today )
        ->select("ba_service_booking.id", "service_date", "book_by", "book_date", "service_date"  ,"book_status",
            DB::Raw("'na' service_time"), DB::Raw("'na' service_code"),DB::Raw("'na' added_to_queue"),DB::Raw("'na' status"),
            DB::Raw("'na' customer_name"), "address", "landmark", "total_cost","payment_type as pay_mode" ,
            "ba_order_sequencer.type"   )
        ->paginate(20) ;
    }


    $book_nos = array( '0' );
    $cust_ids = array( '0' );
    foreach($normal_orders as $item)
    {
        $book_nos[] = $item->id;
        $cust_ids[] = $item->book_by;
    }

    $booked_services= DB::table("ba_service_booking_details")
    ->whereIn("book_id", $book_nos)
    ->select("book_id", "service_product_id", "service_name", "service_time", "added_to_queue", "status")
    ->get();

    $cust_ids = array_filter($cust_ids);
    $cust_ids =  array_unique($cust_ids);

    $customers = DB::table("ba_profile")
    ->whereIn("id", $cust_ids)
    ->get();


    $allorders = array();
    foreach($normal_orders as $item)
    {
        foreach($booked_services as $bookeditem)
          if($bookeditem->book_id == $item->id )
          {
            $item->service_product_id  = $bookeditem->service_product_id;
            $item->service_time = $bookeditem->service_time;
            $item->added_to_queue = $bookeditem->added_to_queue;
            $item->status = $bookeditem->status;
            break;
        }

        $allorders[] = $item;
    }



    $keys = array_column($allorders, 'service_time');
    array_multisort($keys, SORT_ASC, $allorders);

    $theme = DB::table("ba_theme")
    ->first();

    $data = array('orders' =>  $allorders , 'booked_services' => $booked_services,  'customers' => $customers,   'page_title' => 'Manage Listed Services','theme'=>$theme );

    return view("orders.view_bookings")->with(   $data );

}

public function updateBookingStatus(Request $request)
{
  $bin = $request->session()->get("_bin_");


  if(     $request->bookingno == "" || $request->status == ""  	)
  {
    return redirect("/services/view-bookings")->with("err_msg",     'Missing data to perform action.' );
}

if( $request->status  != "completed"  && $request->status  != "cancel_by_owner" &&  $request->status  != "cancelled" &&  $request->status  != "engaged" )
{
    return redirect("/services/view-bookings")->with("err_msg",   'Invalid booking status.'   );
}


$business = Business::find(  $bin);
if( !isset($business))
{
  return redirect("/services/view-bookings")->with("err_msg",    'Business information not found!'  );
}

    //check if booking no is his/her
$booking  = ServiceBooking::find( $request->bookingno) ;
if( !isset( $booking )  )
{
  return redirect("/services/view-bookings")->with("err_msg",    'Booking information not found!'  );
}
else
{
  DB::table("ba_realtime_queue")
  ->where("book_id", $request->bookingno)
  ->update( array('status' => $request->status  ));

      //take a log
  $orderStatus = new OrderStatusLogModel;
  $orderStatus->order_no = $booking->id  ;
  $orderStatus->order_status = $request->status;
  $orderStatus->log_date = date('Y-m-d H:i:s');
  $orderStatus->save();
  return redirect("/services/view-bookings")->with("err_msg", "Service booking status updated!" );
}

}


public function addStaff(Request $request)
{

  $bin = $request->session()->get("_bin_");
  $theme = DB::table("ba_theme")
  ->first();

  $data = array('theme'=>$theme);
  return view("staffs.add_new_staff")->with($data) ;

}


public function manageStaffs(Request $request)
{

  $bin = $request->session()->get("_bin_");


  $all_staffs = DB::table("ba_profile")
  ->join("ba_users", "ba_users.profile_id", "=", "ba_profile.id")
  ->where("ba_users.bin", $bin)
  ->select("ba_profile.*")
  ->get( );
  $theme = DB::table("ba_theme")
  ->first();
  return view("staffs.manage_staffs")->with( array('staffs' => $all_staffs,'theme'=>$theme ) ) ;

} 

public function saveStaff(Request $request)
{

    if( !$request->session()->has("_bin_")   )
    {
      return redirect ("/login")->with("err_msg", "Session has expired!");
  }

  $bin = $request->session()->get("_bin_");

  if($request->phone == "" || $request->fname == "" || $request->lname == ""  || $request->email == ""   || $request->password == ""  ||
    $request->address == ""  || $request->city  == ""  || $request->state == ""  || $request->pin == "")
  {
      return redirect("/services/add-new-staff")->with("err_msg",   'Missing data to perform action.'  );
  }


  $staff = DB::table("ba_users")
  ->select("profile_id"  )
  ->where("bin",  $bin )
  ->where("phone", $request->phone)
  ->whereIn("category", array("1", '10' ) )
  ->first();

  if(isset($staff))
  {
    return redirect("/services/add-new-staff")->with("err_msg",   'A staff with same phone exists.'  );
}
else
{
    
    
			//saving profile
 $otp = mt_rand(232323,999999);
 $otpdate =  date('Y-m-d H:i:s');
 $currentDate = strtotime($otpdate);
 $futureDate = $currentDate+(60*5);
 $otp_expiresOn = date("Y-m-d H:i:s", $futureDate);

 $name  = explode(" ", $request->name);
 $profile = new  CustomerProfileModel;
 $profile->fname = $request->fname  ;
 $profile->mname = $request->mname != "" ? $request->mname : null ;
 $profile->lname =  $request->lname ;
 $profile->fullname =  $request->fname . " " . ($request->mname != "" ? $request->mname . " " : "" ) .  $request->lname ;
 $profile->phone = $request->phone;
 $profile->email = $request->email;
 $profile->locality = $request->address;
 $profile->landmark = $request->landmark;

 $profile->city = $request->city;
 $profile->state = $request->state;
 $profile->pin_code = $request->pin; 

            // profile photo upload area
 if ($request->photo!="") {

    $folder_path =  config('app.app_cdn_path')   . "/assets/image/business/bin_" . $bin .    "/";
    
    if( !File::isDirectory($folder_path)){
        File::makeDirectory( $folder_path, 0755, true, true);
    }
    
    $file =  $request->file('photo');
    $originalname = $file->getClientOriginalName();
    $extension = $file->extension();
    $filename =  "profile_" . $bin . "_" .  time() . ".{$extension}"; 
    $file->storeAs('uploads',  $filename );
    $imgUpload = File::copy(storage_path(). '/app/uploads/'. $filename ,   $folder_path .  $filename ); 
    $profile->profile_photo = config('app.app_cdn') . "/assets/image/business/bin_" . $bin .    "/" . $filename;  
}

$save=$profile->save();

if(	 $save  )
{
    $newuser = new User;
    $newuser->profile_id = $profile->id;
    $newuser->bin =  $bin ;
    $newuser->phone = $request->phone;
    $newuser->password =  Hash::make( $request->password )  ;
    $newuser->category =  10;
    $newuser->otp = $otp;
    $newuser->otp_expires =  $otp_expiresOn;
    $save =$newuser->save();
}
return redirect("/services/add-new-staff")->with("err_msg",    'Staff profile saved.' )  ;
}

}


protected function updateStaff(Request $request)
{
    if( !$request->session()->has("_bin_")   )
    {
      return redirect ("/login")->with("err_msg", "Session has expired!");
  }
  
  $bin = $request->session()->get("_bin_");
  
  if($request->phone == "" || $request->fname == "" || $request->lname == ""  || $request->email == "" || $request->key==""|| $request->address == ""  || $request->city  == ""  || $request->state == ""  || $request->pin == "")
  {
      return redirect("/services/add-new-staff")->with("err_msg",   'Missing data to perform action.'  );
  }


  $profile =  CustomerProfileModel::find($request->key);
  
  $userobj = DB::table('ba_users')
  ->where('profile_id',$profile->id)
  ->first();

            //updating profile
  $otp = mt_rand(232323,999999);
  $otpdate =  date('Y-m-d H:i:s');
  $currentDate = strtotime($otpdate);
  $futureDate = $currentDate+(60*5);
  $otp_expiresOn = date("Y-m-d H:i:s", $futureDate);

  $name  = explode(" ", $request->name);
  
  $profile->fname = $request->fname  ;
  $profile->mname = $request->mname != "" ? $request->mname : null ;
  $profile->lname =  $request->lname ;
  $profile->fullname =  $request->fname . " " . ($request->mname != "" ? $request->mname . " " : "" ) .  $request->lname ;
  $profile->phone = $request->phone;
  $profile->email = $request->email;
  $profile->locality = $request->address;
  $profile->landmark = $request->landmark;

  $profile->city = $request->city;
  $profile->state = $request->state;
  $profile->pin_code = $request->pin; 

            // profile photo upload area
  if ($request->photo!="") {

    $folder_path =  config('app.app_cdn_path')   . "/assets/image/business/bin_" . $bin .    "/";
    
    if( !File::isDirectory($folder_path)){
        File::makeDirectory( $folder_path, 0755, true, true);
    }
    
    $file =  $request->file('photo');
    $originalname = $file->getClientOriginalName();
    $extension = $file->extension();
    $filename =  "profile_" . $bin . "_" .  time() . ".{$extension}"; 
    $file->storeAs('uploads',  $filename );
    $imgUpload = File::copy(storage_path(). '/app/uploads/'. $filename ,   $folder_path .  $filename ); 
    $profile->profile_photo = config('app.app_cdn') . "/assets/image/business/bin_" . $bin .    "/" . $filename;  
}

$save = $profile->save();

if(  $save  )
{
    $newuser = User::find($userobj->id);

    $newuser->profile_id = $profile->id;
    $newuser->bin =  $bin ;
    $newuser->phone = $request->phone; 
    $newuser->category =  10;
    $newuser->otp = $otp;
    $newuser->otp_expires =  $otp_expiresOn;
    $save =$newuser->save();
}
return redirect("/services/manage-staffs")->with("err_msg",    'Staff profile updated successfully!.' )  ;

}


public function removeStaff(Request $request)
{


    if( !$request->session()->has("_bin_")   )
    {
      return redirect ("/login")->with("err_msg", "Session has expired!");
  }

  $bin = $request->session()->get("_bin_");

  if( $request->key == ""  )
  {
     return redirect("/services/manage-staffs")->with("err_msg",   'Missing data to perform action.'  );
 }


 $staff_user = User::where( "profile_id", $request->key  )->first();

 if( !isset($staff_user))
 {
    return redirect("/services/manage-staffs")->with("err_msg",   'No matching staff found!'  );
}
else
{
    $staff_user->delete();
        //now delete profile
    DB::table('ba_profile')
    ->where('id', '=',   $request->key  )
    ->delete();

    return redirect("/services/manage-staffs")->with("err_msg",    'Staff profile removed.' )  ;
}

}



public function addStaffDayOff(Request $request)
{

   $bin = $request->session()->get("_bin_");

   $staffs =  DB::table("ba_profile")
   ->join("ba_users", "ba_users.profile_id", "=", "ba_profile.id")
   ->where("ba_users.bin", $bin)
   ->select("ba_profile.*")
   ->get( );


   $staff_day_offs = DB::table('ba_staff_day_off')
   ->join("ba_profile", "ba_profile.id", "=", "ba_staff_day_off.staff_id")
   ->whereMonth('off_date',  date('m') )
   ->whereYear('off_date',  date('Y') )
   ->select("ba_profile.*", "ba_staff_day_off.off_date")
   ->get();

   $theme = DB::table("ba_theme")
   ->first();

   return view("staffs.add_staff_day_off")->with( array('staffs' => $staffs, 'day_offs' => $staff_day_offs,'theme'=>$theme ) ) ;



}
public function saveStaffDayOff(Request $request)
{
  if( !$request->session()->has("_bin_")   )
  {
    return redirect ("/login")->with("err_msg", "Session has expired!");
}

$bin = $request->session()->get("_bin_");

if( $request->staff == "" || $request->dayoff == ""  )
{
   return redirect("/services/staff/add-day-off")->with("err_msg",   'Missing data to perform action.'  );
}


$staff_user = User::where( "profile_id", $request->staff  )->first();

if( !isset($staff_user))
{
    return redirect("/services/staff/add-day-off")->with("err_msg",   'No matching staff found!'  );
}
else
{

        //check if day off is present
    $day_off = DB::table('ba_staff_day_off')
    ->where('staff_id', $request->staff  )
    ->whereDate('off_date',  date('Y-m-d', strtotime($request->dayoff))  )
    ->first();

    $day_off = StaffDayOff::where('staff_id', $request->staff  )
    ->whereDate('off_date',  date('Y-m-d', strtotime($request->dayoff))  )
    ->first();

        if(  $request->onleave == "on") //add in day off list
        {
          if(isset($day_off))
          {
            return redirect("/services/staff/add-day-off")->with("err_msg",   'Staff already in day-off list!'  );
        }
        else
        {
            $day_off = new StaffDayOff;
            $day_off->staff_id  =$request->staff ;
            $day_off->off_date  =  date('Y-m-d', strtotime($request->dayoff))   ;
            $day_off->bin   = $bin  ;
            $day_off->save();
            return redirect("/services/staff/add-day-off")->with("err_msg",   'Staff added in day-off list!' );
        }
    }
    else
    {
      if(isset($day_off))
      {
        $day_off->delete();
        return redirect("/services/staff/add-day-off")->with("err_msg",   'Staff removed from day-off list!'  );
    }
    else
    {
        return redirect("/services/staff/add-day-off")->with("err_msg",   'Staff not in day-off list. Nothing to update!'  );
    }
}


}

}

//delete booking method from 1930 to 2025
public function removeBooking (Request $request)
{

    $id = $request->code;
    $bin = $request->session()->get("_bin_");

    $today = date('Y-m-d');
    if ($id!=null)
    {
        $delete = DB::table('ba_service_booking')
        ->where('id', '=', $id)->delete();

        $delete_bs_details = DB::table('ba_service_booking_details')
        ->where('book_id', '=', $id)->delete();




        if($request->filter_date)
        {
          $today = date('Y-m-d');
          $normal_orders = DB::table("ba_service_booking")
          ->join("ba_order_sequencer", "ba_order_sequencer.id", "=", "ba_service_booking.id")
          ->where("bin",$bin)
          ->where("ba_order_sequencer.type", "booking")
          ->whereRaw("date(service_date) >= '$today' " )
          ->select("ba_service_booking.id", "service_date", "book_by", "book_date", "service_date" , "book_status",   DB::Raw("'na' customer_name"), "address", "landmark", "total_cost","payment_type as pay_mode" ,
             "ba_order_sequencer.type"   )
          ->paginate(20) ;
      }
      else
      {
          $normal_orders = DB::table("ba_service_booking")
          ->join("ba_order_sequencer", "ba_order_sequencer.id", "=", "ba_service_booking.id")
          ->where("bin",$bin)
          ->where("ba_order_sequencer.type", "booking")
          ->select("ba_service_booking.id", "service_date", "book_by", "book_date", "service_date"  ,"book_status",
              DB::Raw("'na' service_time"), DB::Raw("'na' service_code"),DB::Raw("'na' added_to_queue"),DB::Raw("'na' status"),
              DB::Raw("'na' customer_name"), "address", "landmark", "total_cost","payment_type as pay_mode" ,
              "ba_order_sequencer.type"   )
          ->paginate(20) ;
      }

      $book_nos = array( '0' );
      $cust_ids = array( '0' );
      foreach($normal_orders as $item)
      {
          $book_nos[] = $item->id;
          $cust_ids[] = $item->book_by;
      }

      $booked_services= DB::table("ba_service_booking_details")
      ->whereIn("book_id", $book_nos)
      ->select("book_id", "service_product_id", "service_name", "service_time", "added_to_queue", "status")
      ->get();

      $cust_ids = array_filter($cust_ids);
      $cust_ids =  array_unique($cust_ids);

      $customers = DB::table("ba_profile")
      ->whereIn("id", $cust_ids)
      ->get();

      $allorders = array();
      foreach($normal_orders as $item)
      {
          foreach($booked_services as $bookeditem)
            if($bookeditem->book_id == $item->id )
            {
              $item->service_product_id  = $bookeditem->service_product_id;
              $item->service_time = $bookeditem->service_time;
              $item->added_to_queue = $bookeditem->added_to_queue;
              $item->status = $bookeditem->status;
              break;
          }

          $allorders[] = $item;
      }



      $keys = array_column($allorders, 'service_time');
      array_multisort($keys, SORT_ASC, $allorders);

      $data = array('orders' =>  $allorders , 'booked_services' => $booked_services,  'customers' => $customers,   'page_title' => 'Manage Listed Services' );


      $returnHTML = view('orders.view_booking_list')->with($data)->render();


      $msg ="record deleted sucessfully";
  }else
  {
    $msg ="failed to delete the record";
}


return response()->json(array('success' => true, 'msg'=>$msg,'html'=>$returnHTML));





}




//section for product adding section


//product adding section
protected function enterSaveProduct(Request $request)
{
    $bin = $request->session()->get("_bin_");
    $category = DB::table('ba_product_category')
    ->join("ba_business", "ba_business.category", "=", "ba_product_category.business_category")
    ->select('ba_product_category.category_name','ba_product_category.business_category')
    ->where("ba_business.id", $bin )
    ->get();

    $theme = DB::table("ba_theme")
    ->first();

    $unit = DB::table('ba_product_units')
    ->select('id','unit')
    ->get();

    $prsubid = $request->subid;
    $product = DB::table('ba_products')
    ->join("ba_product_variant","ba_products.id","=","ba_product_variant.prid")
    ->where('ba_product_variant.prsubid',$prsubid)
    ->where("ba_products.bin", $bin)
    ->first();


    if (isset($product)) {
      $data = array('cat_'=>$category,'unit'=>$unit,'prod'=>$product);
      return view('products.add_products')->with('all',$data)->with('theme',$theme);
  }
  else
  {
      return redirect("/all-products")->with("err_msg", "Selected product not found!");
  }

}


protected function saveProduct(Request $request)
{

  $bin= $request->session()->get("_bin_");

  if ( strcasecmp($request->btn_save, 'save') == 0)  {

    $id=0;

    $resultOrder = DB::table('ba_products')
    ->select(DB::Raw('max(id) as productID'))
    ->get();

    if($resultOrder)
    {
      foreach ($resultOrder as $order) {
        $id = $order->productID+1;
    }
}
else
{
  $id = $id+1;
}


$prod_category = $request->productCategory;
$prod_name = $request->productName;
$bar_code = $request->barCode;
$prod_code = $request->productCode;
$prod_desc = $request->productDescription;
$prod_price = $request->productprice;
$prod_packcharge = $request->productPackCharge;
$prod_foodtype = $request->productFoodType;
$prod_discountpercentage = $request->productDiscountPercentage;
$prod_discount = $request->productDiscount;

$prod_unit = $request->productUnit;
$prod_volume = $request->productVolume;
$prod_UnitV = $request->productUnitValue;
$prod_Initial = $request->productInitialStock;
$prod_Stock = $request->productStock;
$prod_minimum = $request->productMiniumStock;
$prod_maximum = $request->productMaximumOrder;

$cgst = $request->cgst;
$sgst = $request->sgst;


$maxpid =   DB::table('ba_products')
->max('id');
$prCode = "bpc" .  $bin . "" . ( $maxpid + 1 )  ;


$product = new ProductModel();
$product->bin = $bin;
$product->category = $prod_category;
$product->pr_code =  $prCode;
$product->origin_country = "India";
$product->pr_name = $prod_name;
$product->description = $prod_desc;
$save_product = $product->save();



if ( !$save_product ) {
    return redirect('/all-products')->with('err_msg','Failed to add product details');
}

$productVariant = new ProductVariantModel;
$productVariant->prid=$product->id;
$productVariant->barcode= ($bar_code== "") ? null :  $bar_code ;
$productVariant->initial_stock=$prod_Initial;
$productVariant->stock_inhand=$prod_Stock;
$productVariant->min_required=$prod_minimum;
$productVariant->max_order=$prod_maximum;
$productVariant->unit_price=$prod_price;
$productVariant->unit_value=$prod_UnitV;
$productVariant->actual_price=$prod_price;
$productVariant->cgst_pc = ( $cgst > 0) ? $cgst : 0.00;
$productVariant->sgst_pc = ( $sgst> 0) ? $cgst : 0.00;

$productVariant->volume=$prod_volume;


if($prod_discount!="")
{
  $productVariant->discount=$prod_discount;
  $productVariant->actual_price= $prod_price - $prod_discount;
}
else
{
  $productVariant->discount= 0;
  $productVariant->actual_price= $prod_price;
}


if($prod_discountpercentage!="")
{
    $productVariant->discountPc = $prod_discountpercentage;
    $productVariant->actual_price= $prod_price - ( $prod_discountpercentage *  $prod_price / 100 ) ;
}
else
{
    $productVariant->discountPc= 0;
    $productVariant->actual_price= $prod_price;
}

$productVariant->food_type = $prod_foodtype;
$productVariant->unit_name = $prod_unit;

if($prod_volume!="")  $productVariant->volume =$prod_volume ;

$save_info = $productVariant->save();

if($save_info)
{
    return redirect('/all-products')->with('err_msg','Product saved successfully.');
}
else
{
    return redirect('/all-products')->with('err_msg','Product could not saved.');
}

}
else{


      //find product subid
  $variant = ProductVariantModel::find($request->subid);

  if (!$variant) {
      return redirect('/all-products')->with('err_msg','No product variant found!');
  }

  $cgst = ( $request->cgst > 0) ?$request->cgst : 0.00;
  $sgst = (  $request->sgst > 0) ?  $request->sgst  : 0.00;


    //updating variant
  $update_variant = DB::table('ba_product_variant')
  ->where("prsubid",$request->subid)
  ->update(array(
    'barcode' => $request->barCode ,
    'unit_price' => $request->productprice,
    'actual_price' =>  ( $request->productprice - $request->productDiscount),
    'food_type' => $request->productFoodType,
    'discount' => $request->productDiscount,
    'discountPc' => $request->productDiscountPercentage,
    'packaging'=>$request->productPackCharge,
    'unit_value'=>$request->productUnitValue,
    'unit_name' => $request->productUnit,
    'volume'=>$request->productVolume,
    'stock_inhand'=>$request->productStock,
    'initial_stock' => $request->productInitialStock,
    'min_required'=>$request->productMiniumStock,
    'max_order'=>$request->productMaximumOrder,
    'cgst_pc' => $cgst,
    'sgst_pc' => $sgst
));

  $update_info = DB::table('ba_products')
  ->where("id",$variant->prid)
  ->update(array(
    'pr_name' => $request->productName,
    'category' => $request->productCategory,
    'description'=>$request->productDescription
));


  if ($update_info || $update_variant ) {

     return redirect('admin/product/enter-save-product?subid=' . $request->subid )->with('err_msg','Update sucessfull!');

 }else{

    return redirect('admin/product/enter-save-product?subid=' . $request->subid)->with('err_msg','Failed to update!');
}
}


}

public function viewProductList(Request $request)
{
  $prcode = $request->key;
  $bin = $request->session()->get("_bin_");
  $result = DB::table('ba_products')
  ->where('bin',$bin)
  ->where('pr_code',$prcode)
  ->get();

  $theme = DB::table("ba_theme")
  ->first();

  $data = array('product'=>$result);
  return view('products.view_productsList')
  ->with('all',$data)->with('theme',$theme);
}

protected function deleteProduct(Request $request)
{
  $bin = session()->get('_bin_');
  $prsubid = $request->sbid;

  if($prsubid == 0)
  {
    return redirect('/all-products');
}

  //get product id
$variants  = DB::table('ba_product_variant')
->where('prsubid',$prsubid)
->get();

  //since there was only one variant we can delete record from parent table
if(count($variants ) == 1)
{
    $prid = $variants[0]->prid;
    //delete product 
    $delete = DB::table('ba_products')
    ->where('id', $prid)
    ->delete();
}

  //delete variant
DB::table('ba_product_variant')
->where('prsubid',$prsubid)
->delete();


  //delete variant images 
DB::table('ba_product_photos')
->where('prsubid',$prsubid)
->delete();

return redirect('/all-products')->with('err_msg','Product deleted sucessfull!'); 

}


//dashboard

public function overView(Request $request)
{
    $bin = session()->get('_bin_');
    
    if($bin == "")
    {
      return redirect("/erp/customer-care/business/view-all")->with("err_msg", "No business selected!");
  }

  $today = date('Y-m-d');
  $year = date('Y');
  $month= date('m');
  $report_date = date('Y-m-d', strtotime("7 days ago"));

  $normal_orders = DB::table("ba_service_booking")
  ->join("ba_business", "ba_business.id", "=", "ba_service_booking.bin")
  ->where("ba_service_booking.bin",  $bin  )
  ->whereRaw("date(ba_service_booking.service_date) = '$today' " )
  ->select("ba_service_booking.*")
  ->get();


  $pnd_orders = DB::table("ba_pick_and_drop_order")
  ->join("ba_business", "ba_business.id", "=", "ba_pick_and_drop_order.request_by")
  ->whereRaw("date(service_date)= '$today' " )
  ->whereIn("request_from", array ('business'))
  ->where("ba_business.id", $bin)
  ->select("ba_pick_and_drop_order.*")
  ->get();

    //sales chart for pnd and normal orders

  $normal_sales_chart_data = DB::table("ba_service_booking")
  ->join("ba_business", "ba_business.id", "=", "ba_service_booking.bin")
  ->where("ba_business.id",  $bin  )
  ->whereDate("service_date", ">=", $report_date )
  ->whereIn("ba_service_booking.book_status",array('completed','delivered'))
  ->select( DB::Raw("date(service_date) as serviceDate"), DB::Raw("count(*) as normalOrder")  )
  ->groupBy("serviceDate")
  ->get();



  $pnd_sales_chart_data = DB::table("ba_pick_and_drop_order")
  ->join("ba_business", "ba_business.id", "=", "ba_pick_and_drop_order.request_by")
  ->whereDate("service_date", ">=", $report_date )
  ->whereIn("ba_pick_and_drop_order.book_status",array('completed','delivered'))
  ->whereIn("request_from", array ('business', 'customer'))
  ->where("ba_business.id", $bin)
  ->select( DB::Raw("date(service_date) as serviceDate"), DB::Raw("count(*) as pndOrders"), DB::Raw("sum(service_fee) as totalRevenue"))
  ->groupBy("serviceDate")
  ->get();


  $normal_sales_revenue_data = DB::table("ba_service_booking")
  ->join("ba_business", "ba_business.id", "=", "ba_service_booking.bin")
  ->where("ba_business.id",  $bin  )
  ->whereDate("service_date", ">=", $report_date )
  ->whereIn("ba_service_booking.book_status",array('completed','delivered'))
  ->select(
    DB::Raw("sum(ba_service_booking.total_cost) as normalTotalCost"))
  ->first();


  $pnd_sales_revenue_data = DB::table("ba_pick_and_drop_order")
  ->join("ba_business", "ba_business.id", "=", "ba_pick_and_drop_order.request_by")
  ->whereDate("service_date", ">=", $report_date )
  ->whereIn("ba_pick_and_drop_order.book_status",array('completed','delivered'))
  ->whereIn("request_from", array ('business', 'customer'))
  ->where("ba_business.id", $bin)
  ->select(
    DB::Raw("sum(ba_pick_and_drop_order.total_amount ) as pndTotalAmount"))
  ->first();


  $data_sales_revenue = $normal_sales_revenue_data->normalTotalCost;
  $data_pnd_revenue = $pnd_sales_revenue_data->pndTotalAmount;


    //sales chart for normal and pnd ends here

    //revenue code for normal order and pnd order starts from here
  $NormalrevenueMonthResult = DB::table('ba_service_booking')
  ->select(
    DB::Raw('sum(if(month(service_date) = 1,  service_fee ,0)) as Jan,
       sum(if(month(service_date) = 2,  service_fee ,0)) as Feb ,
       sum(if(month(service_date) = 3,  service_fee ,0)) as Mar ,
       sum(if(month(service_date) = 4,  service_fee ,0)) as  Apr ,
       sum(if(month(service_date) = 5,  service_fee ,0)) as  May ,
       sum(if(month(service_date) = 6,  service_fee ,0)) as  Jun ,
       sum(if(month(service_date) = 7,  service_fee ,0)) as   Jul ,
       sum(if(month(service_date) = 8,  service_fee ,0)) as  Aug ,
       sum(if(month(service_date) = 9,  service_fee ,0)) as  Sep ,
       sum(if(month(service_date) = 10, service_fee ,0)) as  Oct ,
       sum(if(month(service_date) = 11, service_fee  ,0)) as  Nov ,
       sum(if(month(service_date) = 12, service_fee ,0)) as  "Dec" '))
  ->whereYear('service_date',$year)
  ->where('bin',"=",$bin)
  ->whereIn('book_status',array('completed','delivered'))
  ->get();

  $normaldata = array();

  foreach ($NormalrevenueMonthResult as $rev) {
   array_push(
      $normaldata,$rev->Jan,$rev->Feb,$rev->Mar,$rev->Apr,$rev->May,
      $rev->Jun,$rev->Jul,$rev->Aug,$rev->Sep,$rev->Oct,$rev->Nov,
      $rev->Dec);


}


        //pnd commission calculation logic

$PNDCommisionMonthWiseResult = DB::table('ba_pick_and_drop_order')
->join('ba_business','ba_pick_and_drop_order.request_by','=','ba_business.id')
->select(
    DB::Raw(
        'sum(if(month(service_date) = 1,total_amount*commission/100,0)) as Jan,
        sum(if(month(service_date) = 2,total_amount*commission/100,0)) as Feb,
        sum(if(month(service_date) = 3,total_amount*commission/100,0)) as Mar,
        sum(if(month(service_date) = 4,total_amount*commission/100,0)) as Apr,
        sum(if(month(service_date) = 5,total_amount*commission/100,0)) as May,
        sum(if(month(service_date) = 6,total_amount*commission/100,0)) as Jun,
        sum(if(month(service_date) = 7,total_amount*commission/100,0)) as Jul,
        sum(if(month(service_date) = 8,total_amount*commission/100,0)) as Aug,
        sum(if(month(service_date) = 9,total_amount*commission/100,0)) as Sep,
        sum(if(month(service_date) = 10,total_amount*commission/100,0))as Oct,
        sum(if(month(service_date) = 11,total_amount*commission/100,0))as Nov,
        sum(if(month(service_date) = 12,total_amount*commission/100,0)) as "Dec" '))
->whereYear('service_date',$year)
->where('request_by',$bin)
->where(DB::Raw('source="booktou" or source="customer" '))
->whereIn('book_status',array('completed','delivered'))
->get();

//setting the variable for commision
$jantotal=0;$febtotal=0;$martotal=0;$aprtotal=0;
$maytotal=0;$juntotal=0;$jultotal=0;$augtotal=0;
$septotal=0;$octtotal=0;$novtotal=0;$dectotal=0;
//variable setting ends here

foreach ($PNDCommisionMonthWiseResult as $commission) {
  $jantotal = $commission->Jan;
  $febtotal = $commission->Feb;
  $martotal = $commission->Mar;
  $aprtotal = $commission->Apr;
  $maytotal = $commission->May;
  $juntotal = $commission->Jun;
  $jultotal = $commission->Jul;
  $augtotal = $commission->Aug;
  $septotal = $commission->Sep;
  $octtotal = $commission->Oct;
  $novtotal = $commission->Nov;
  $dectotal = $commission->Dec;
}

    //ends here

$PNDrevenueMonthWiseResult = DB::table('ba_pick_and_drop_order')
->select(
    DB::Raw('sum(if(month(service_date) = 1, service_fee,0)) as Jan,
       sum(if(month(service_date) = 2, service_fee,0)) as Feb ,
       sum(if(month(service_date) = 3,service_fee,0)) as Mar ,
       sum(if(month(service_date) = 4, service_fee,0)) as  Apr ,
       sum(if(month(service_date) = 5, service_fee,0)) as  May ,
       sum(if(month(service_date) = 6, service_fee,0)) as  Jun ,
       sum(if(month(service_date) = 7, service_fee,0)) as   Jul ,
       sum(if(month(service_date) = 8, service_fee,0)) as  Aug ,
       sum(if(month(service_date) = 9, service_fee,0)) as  Sep ,
       sum(if(month(service_date) = 10, service_fee,0)) as  Oct ,
       sum(if(month(service_date) = 11, service_fee,0)) as  Nov ,
       sum(if(month(service_date) = 12, service_fee,0)) as  "Dec"  '))
->whereYear('service_date',$year)
->where('request_by',"=",$bin)
->whereIn('book_status',array('completed','delivered'))
->get();




$pnddata=array();

foreach ($PNDrevenueMonthWiseResult as  $pnd) {
  array_push(
      $pnddata,
      $pnd->Jan-$jantotal,
      $pnd->Feb-$febtotal,
      $pnd->Mar-$martotal,
      $pnd->Apr-$aprtotal,
      $pnd->May-$maytotal,
      $pnd->Jun-$juntotal,
      $pnd->Jul-$jultotal,
      $pnd->Aug-$augtotal,
      $pnd->Sep-$septotal,
      $pnd->Oct-$octtotal,
      $pnd->Nov-$novtotal,
      $pnd->Dec-$dectotal);
}
    //revenue section for pnd and normal order ends here

$business = DB::table("ba_business")
->where("id",$bin)
->first();

$theme = DB::table("ba_theme")
->first();

$pos_report_date = date('Y-m-d', strtotime("20 days ago"));

$pos_order_count = DB::table("ba_pos_order") 
->where("bin",$bin)
->whereRaw("date(service_date) >= '$pos_report_date' " )
->select(DB::Raw("ba_pos_order.*"))
->get();

$pos_sales_chart_data = DB::table("ba_pos_order") 
->where("bin", $bin )
->whereDate("service_date", ">=", $pos_report_date)
    //->whereIn("ba_service_booking.book_status",array('completed','delivered'))
->select( 
  DB::Raw("date(service_date) as serviceDate"), 
  DB::Raw("count(*) as posOrders")
)
->groupBy("serviceDate")
->get();

    //dd($pos_order_count);
    //ends here
$data = array(
  "normal"=>$normal_orders,
  "pnd"=>$pnd_orders,
  "normal_"=>$normaldata,
  "pnd_"=>$pnddata,
  "normal_sales_chart_data"=>$normal_sales_chart_data,
  "pnd_sales_chart_data"=>$pnd_sales_chart_data,
  "biz"=>$business,
  "sales_revenue"=>$data_sales_revenue,
  "pnd_revenue"=>$data_pnd_revenue,
  "theme"=>$theme,
  "bin"=>$bin,
  "report_date"=>$report_date,
  "todayDate"=>$today,
  "pos"=>$pos_order_count,
  "pos_sales_chart_data"=>$pos_sales_chart_data
);

return view('sales.overview')->with($data);


}
//dashboard


//pick and drop request
protected function viewPickAndDropOrders(Request $request)
{

    $bin = session()->get('_bin_');

    $business_info = Business::find( $bin  );
    if( !isset($business_info) )
    {
        return redirect("/admin/customer-care/pick-and-drop-orders/view-all")->with("err_msg",  'No matching business found.'  );
    }


    if( isset($request->filter_date))
    {
        $orderDate = date('Y-m-d', strtotime($request->filter_date));
    }
    else
    {
        $orderDate = date('Y-m-d' , strtotime("7 days ago"))  ;
    }

    $bin = $request->session()->get('_bin_');

    $business = DB::table("ba_business")
    ->where("id", $bin)
    ->first();


    if( isset($request->btn_search) )
    {
        //change from here
        $request->session()->put('_last_search_', $request->filter_status);
        $request->session()->put('_last_search_date',  $request->filter_date );
        $bin = $request->filter_business;
        $dbstatus = $request->filter_status;

        if( $dbstatus != "")
        {
            if( $dbstatus == "-1")
            {
              $status = array( 'new','confirmed', 'cancel_by_client', 'cancel_by_owner', 'delivered', 'cancelled',
                'order_packed','pickup_did_not_come','in_route', 'package_picked_up','delivery_scheduled' ) ;
          }
          else
          {
              $status[] =  $dbstatus;
          }
      }
      else
      {
          $status = array( 'new','confirmed', 'cancel_by_client', 'cancel_by_owner','order_packed','pickup_did_not_come',
            'delivered', 'cancelled',
            'in_route', 'package_picked_up','delivery_scheduled' ) ;
      }

      if($bin == -1)
      {

          $day_bookings  = DB::table("ba_pick_and_drop_order")
          ->join("ba_business", "ba_business.id", "=", "ba_pick_and_drop_order.request_by")
          ->whereRaw("date(ba_pick_and_drop_order.service_date) >= '$orderDate' ")
          ->whereIn("ba_pick_and_drop_order.book_status", $status)
          ->where("ba_pick_and_drop_order.request_from", "business" )
          ->where("ba_business.frno",  0 )
          ->where("ba_business.request_by", $bin )
          ->orderBy("ba_pick_and_drop_order.id", "desc")
          ->select("ba_pick_and_drop_order.*",

              DB::Raw("'0' mid") ,
              "ba_business.id as bin",
              "ba_business.name",
              "ba_business.phone_pri as primaryPhone",
              "ba_business.locality as locality" ,
              DB::Raw("'pnd' orderType")

          )
          ->orderBy("ba_pick_and_drop_order.id", "desc")
          ->get();




      }
      else
      {
        $day_bookings  = DB::table("ba_pick_and_drop_order")
        ->join("ba_business", "ba_business.id", "=", "ba_pick_and_drop_order.request_by")
        ->whereRaw("date(ba_pick_and_drop_order.service_date) >= '$orderDate' ")
        ->whereIn("ba_pick_and_drop_order.book_status", $status)
        ->where("ba_business.id",$bin)
        ->where("ba_pick_and_drop_order.request_from", "business" )
        ->where("ba_business.frno",  0 )
        ->where("ba_business.request_by", $bin )
        ->orderBy("ba_pick_and_drop_order.id", "desc")
        ->select("ba_pick_and_drop_order.*",

          DB::Raw("'0' mid") ,
          "ba_business.id as bin",
          "ba_business.name",
          "ba_business.phone_pri as primaryPhone",
          "ba_business.locality as locality" ,
          DB::Raw("'pnd' orderType")
      )
        ->orderBy("ba_pick_and_drop_order.id", "desc")
        ->get();

    }


        //changes ends here for Sanatomba


}
else
{
    $bin = $request->session()->get('_bin_');

    $request->session()->remove('_last_search_date' );
    $status = array( 'new','confirmed', 'cancel_by_client',
        'cancel_by_owner','order_packed','pickup_did_not_come','in_route',
        'package_picked_up','delivery_scheduled' ) ;

    $day_bookings  = DB::table("ba_pick_and_drop_order")
    ->join("ba_business", "ba_business.id", "=", "ba_pick_and_drop_order.request_by")
    ->whereRaw("date(ba_pick_and_drop_order.service_date) >= '$orderDate' ")
    ->whereIn("ba_pick_and_drop_order.book_status", $status)
    ->where("ba_pick_and_drop_order.request_from", "business" )
        //->where("ba_business.frno",  0 )
    ->where('request_by',$bin)
    ->orderBy("ba_pick_and_drop_order.id", "desc")
    ->select("ba_pick_and_drop_order.*",

      DB::Raw("'0' mid") ,
      "ba_business.id as bin",
      "ba_business.name",
      "ba_business.phone_pri as primaryPhone",
      "ba_business.locality as locality" ,
      DB::Raw("'pnd' orderType")

  )
    ->orderBy("ba_pick_and_drop_order.id", "desc")
    ->get();


}

      //finding free agents
$today = date('Y-m-d');

$all_agents = DB::table("ba_profile")
->join("ba_users", "ba_users.profile_id", "=", "ba_profile.id")

->whereIn("ba_profile.id", function($query) use ( $today ) {
  $query->select("staff_id")
  ->from("ba_staff_attendance")
  ->whereDate("log_date",   $today  );
})

->where("ba_users.category", 100)
->where("ba_users.status", "<>" , "100")
->where("ba_profile.fullname", "<>", "" )
->where("ba_users.franchise", 0)

->select( "ba_profile.id", "ba_profile.fullname", "ba_profile.fullname as nameWithTask" ,
    "ba_profile.phone", DB::Raw("'yes' isFree") , "ba_profile.profile_photo" )
->get();


$delivery_orders = DB::table("ba_delivery_order")
->whereRaw(" date(accepted_date) = '$today'" )
->where("completed_on", null )
->where("order_type", 'pick and drop' )
->select("member_id", DB::Raw("count(*) as cnt") )
->groupBy('member_id')
->get();

$free_agents = array();

      //Fetching assigned orders
$delivery_orders_list = DB::table("ba_delivery_order")
->join("ba_profile", "ba_profile.id", "=", "ba_delivery_order.member_id")
->select("ba_delivery_order.*", "ba_delivery_order.id as aid", "ba_profile.fullname")
->whereRaw(" date(accepted_date) ='$today'" )
->get();


foreach ($all_agents as $item)
{
 $is_free = true  ;
 $item->nameWithTask =   $item->nameWithTask . " ( 0 tasks )";
 foreach($delivery_orders as $ditem)
 {
  if($item->id == $ditem->member_id )
  {
    $item->nameWithTask =   $item->fullname . " (" . $ditem->cnt .  " tasks)";
    if( $ditem->cnt >= 80 )
    {
        $is_free = false ;
        break;
    }
}
}

if(  !$is_free )
{
  $item->isFree = "no";
}


          //image compression
if($item->profile_photo != null )
{

    $source = public_path() . "/assets/image/member/" .  $item->profile_photo;
    $pathinfo = pathinfo( $source  );
    $new_file_name  =  $pathinfo['filename'].  "_sm" . '.' . $pathinfo['extension'];
    $destination  = base_path() .  "/public/assets/image/member/"   . $new_file_name ;

    if(file_exists(  $source ) &&  !file_exists( $destination ))
    {
      $info = getimagesize($source );

      if ($info['mime'] == 'image/jpeg')
          $image = imagecreatefromjpeg($source);

      elseif ($info['mime'] == 'image/gif')
          $image = imagecreatefromgif($source);

      elseif ($info['mime'] == 'image/png')
          $image = imagecreatefrompng($source);


      $original_w = $info[0];
      $original_h = $info[1];
                $thumb_w = 290; // ceil( $original_w / 2);
                $thumb_h = 200; //ceil( $original_h / 2 );

                $thumb_img = imagecreatetruecolor($original_w, $original_h);
                imagecopyresampled($thumb_img, $image,
                 0, 0,
                 0, 0,
                 $original_w, $original_h,
                 $original_w, $original_h);

                imagejpeg($thumb_img, $destination, 20);
            }
            $item->profile_photo =  URL::to("/public/assets/image/member/"   . $new_file_name );

        }
        else
        {
          $item->profile_photo = URL::to('/public/assets/image/no-image.jpg')  ;
      }

  }


  $last_keyword = "0";
  if($request->session()->has('_last_search_' ) )
  {
      $last_keyword = $request->session()->get('_last_search_' );
  }


  $last_keyword_date = date('d-m-Y') ;
  if($request->session()->has('_last_search_date' ) )
  {
    $last_keyword_date = $request->session()->get('_last_search_date' );
}

$requests_to_process  = DB::table("ba_order_process_request")
->join("ba_profile", "ba_profile.id", "=", "ba_order_process_request.staff_id")
->select("ba_order_process_request.order_no", "ba_profile.id", "ba_profile.fullname")
->whereDate("log_date", $today )
->get();



$customerphones = $merchants = array();
foreach($day_bookings as $item)
{
    $merchants[] = $item->bin;
    $customerphones[] = $item->phone;
}
$merchants[] = 0;

$merchants = array_unique($merchants);

$businesses  = DB::table("ba_business")
->where("id", $bin )
->get();


$all_business  = DB::table("ba_business")
->where("id", $bin )
->get();


$all_staffs = DB::table("ba_profile")
->join("ba_users", "ba_users.profile_id", "=", "ba_profile.id")
->select( "ba_profile.id", "ba_profile.fullname",  "ba_users.id as user_id" )
->where("ba_users.category", ">", "1")
->where("ba_users.status", "<>" , "0")
->get();

      //dd($all_staffs);
$banned_list = DB::table("ba_ban_list")
->whereIn("phone",  $customerphones)
->get();

$theme = DB::table("ba_theme")
->first();

$data = array( 'title'=>'Manage Pick-And-Drop Orders' ,
    'business' => $business,
    'date' => $orderDate, 'all_agents' => $all_agents ,  'staffs' =>$all_staffs,
    'delivery_orders_list' => $delivery_orders_list,
    'results' => $day_bookings,
    'requests_to_process' =>$requests_to_process,
    'all_business' => $all_business,
    'todays_business' => $businesses,
    'last_keyword' => $last_keyword,
    'last_keyword_date' =>  $last_keyword_date ,
    'banned_list' =>$banned_list,
    'refresh' => 'true',
    'breadcrumb_menu' => 'pnd_sub_menu'
);
return view("orders.pick_and_drop_orders_table")->with('data',$data)->with('theme',$theme);
}


protected function newPickAndDropRequest(Request $request)
{


    $bin = $request->session()->get("_bin_");

 

  $orderinfo = PickAndDropRequestModel::where('request_by',$bin)->first();

  $business_info = Business::find( $bin  );
  if( !isset($business_info) )
  {
    return redirect("/admin/customer-care/pick-and-drop-orders/view-all")->with("err_msg",  'No matching business found.'  );
  }

    $address  =  $request->address;
    $landmark  =  $request->landmark;
    $latitude   =  $request->latitude;
    $longitude  =  $request->longitude;

    //save sequencer
    $sequencer = new OrderSequencerModel;
    $sequencer->type = "pnd";
    $save = $sequencer->save();

    if( !isset($save) )
    {
       return redirect("/admin/customer-care/pick-and-drop-orders/view-all")->with("err_msg",  'Failed to generate order no sequence.'  );
    }


    $new_order = new PickAndDropRequestModel ;
    $new_order->id = $sequencer->id;
    $new_order->request_by = $bin;
    $new_order->request_from = "business";
    $new_order->source =  "business";
    $new_order->otp  =   mt_rand(222222, 999999);
    $new_order->pickup_name = ( $request->pucname  != "" ?  $request->pucname  :  $business_info->name );
    $new_order->pickup_phone =  ( $request->pucphone  != "" ?  $request->pucphone  : $business_info->phone_pri );
    $new_order->pickup_address =  ( $request->puaddress  != "" ?  $request->puaddress  : $business_info->locality );
    $new_order->pickup_landmark =  ( $request->pulandmark  != "" ?  $request->pulandmark  : $business_info->landmark );
    $new_order->address = $address;
    $new_order->landmark =  ( $landmark  != "" ?  $landmark  : null);
    $new_order->latitude =   0.00 ;
    $new_order->longitude =  0.00 ;
    $new_order->drop_name = $request->cname;
    $new_order->drop_phone =  $request->cphone ;
    $new_order->drop_address =  $address;
    $new_order->drop_landmark =  ( $landmark  != "" ?  $landmark  : null);
    $new_order->drop_latitude =   0.00 ;
    $new_order->drop_longitude =  0.00 ;
    $new_order->delivery_model = $request->delivery_model;


     $new_order->fullname  = $request->cname; //obsolete
     $new_order->phone  = $request->cphone ; //obsolete


     $new_order->book_status = "new";
     $new_order->service_fee = $new_order->requested_fee =  $request->convincefee ;
     $new_order->packaging_cost = ($request->packcost != "" ? $request->packcost : 0.00);
     $new_order->total_amount = $request->total;
     $new_order->agent_payable = $request->delagentfee;
     $new_order->tax = $request->tax;
     $new_order->book_date =  date('Y-m-d H:i:s');
     $new_order->service_date = date("Y-m-d", strtotime(  $request->servicedate));
     $new_order->pickup_details =  ($request->remarks != "" ? $request->remarks : null);

     if($request->session()->has('__user_id_' ) )
     {
        $new_order->staff_user_id  =  $request->session()->get('__user_id_' );
    }

    $keys = array('a', 'A', 'b', 'b', 'X', 'u', 'W', 'w', 'e', 'G');
    $rp_1 = mt_rand(0, 9);
    $rp_2 = mt_rand(0, 9);
    $new_order->tracking_session = $keys[$rp_1] . $keys[$rp_2] .  time() ;


    $new_order->pay_mode =   $request->paymode ;
    $new_order->payment_target =  strcasecmp($request->paymode, "online") == 0 ? "merchant" : "booktou"  ;
    $new_order->delivery_model = $request->delivery_model;
    $save=$new_order->save();

    if($save)
    {
      return redirect("/admin/customer-care/pnd-order/view-details/".$sequencer->id  )->with("err_msg",  'New pick-and-drop order placed!'  );
  }
  else
  {
    return redirect("/admin/customer-care/pick-and-drop-orders/view-all")->with("err_msg",  'Your pick-and-drop order could not be placed. Please retry'  );
}
}

protected function addPickAndDropOrders(Request $request)
{
    
    $bin =session()->get('_bin_');
    $is_premium =  "no";
    $request->session()->put('IS_PREMIUM', "no");

    $premium_business  = DB::table("ba_premium_business")
    ->where("bin",  $bin ) 
    ->where("ended_on", ">=",  date("Y-m-d H:i:s") )
    ->first();

    
    if( isset($premium_business)  )
    {
       $request->session()->put('IS_PREMIUM', "yes");
       $is_premium =  "yes";
    }


    
    

         
    $business_info = Business::find( $bin  );
    if( !isset($business_info) )
    {
        return redirect("/customer-care/pick-and-drop-orders/view-all")->with("err_msg",  'No matching business found.'  );
    }


    if( isset($request->filter_date))
    {
        $orderDate = date('Y-m-d', strtotime($request->filter_date));
    }
    else
    {
        $orderDate = date('Y-m-d' , strtotime("7 days ago"))  ;
    }

    $bin = $request->session()->get('_bin_');

      
    $business = DB::table("ba_business")
    ->where("id", $bin)
    ->first();


    if( isset($request->btn_search) )
    {
        //change from here
        $request->session()->put('_last_search_', $request->filter_status);
        $request->session()->put('_last_search_date',  $request->filter_date );
        $bin = $request->filter_business;
        $dbstatus = $request->filter_status;

        if( $dbstatus != "")
        {
            if( $dbstatus == "-1")
            {
              $status = array( 'new','confirmed', 'cancel_by_client', 'cancel_by_owner', 'delivered', 'cancelled',
                'order_packed','pickup_did_not_come','in_route', 'package_picked_up','delivery_scheduled' ) ;
          }
          else
          {
              $status[] =  $dbstatus;
          }
      }
      else
      {
          $status = array( 'new','confirmed', 'cancel_by_client', 'cancel_by_owner','order_packed','pickup_did_not_come',
            'delivered', 'cancelled',
            'in_route', 'package_picked_up','delivery_scheduled' ) ;
      }

      if($bin == -1)
      {

          $day_bookings  = DB::table("ba_pick_and_drop_order")
          ->join("ba_business", "ba_business.id", "=", "ba_pick_and_drop_order.request_by")
          ->whereRaw("date(ba_pick_and_drop_order.service_date) >= '$orderDate' ")
          ->whereIn("ba_pick_and_drop_order.book_status", $status)
          ->where("ba_pick_and_drop_order.request_from", "business" )
          ->where("ba_business.frno",  0 )
          ->where("ba_business.request_by", $bin )

          ->orderBy("ba_pick_and_drop_order.id", "desc")
          ->select("ba_pick_and_drop_order.*",

              DB::Raw("'0' mid") ,
              "ba_business.id as bin",
              "ba_business.name",
              "ba_business.phone_pri as primaryPhone",
              "ba_business.locality as locality" ,
              DB::Raw("'pnd' orderType")

          )
          ->orderBy("ba_pick_and_drop_order.id", "desc")
          ->get();


      }
      else
      {
        $day_bookings  = DB::table("ba_pick_and_drop_order")
        ->join("ba_business", "ba_business.id", "=", "ba_pick_and_drop_order.request_by")
        ->whereRaw("date(ba_pick_and_drop_order.service_date) >= '$orderDate' ")
        ->whereIn("ba_pick_and_drop_order.book_status", $status)
        ->where("ba_business.id",$bin)
        ->where("ba_pick_and_drop_order.request_from", "business" )
        ->where("ba_business.frno",  0 )
        ->where("ba_business.request_by", $bin )
        ->orderBy("ba_pick_and_drop_order.id", "desc")
        ->select("ba_pick_and_drop_order.*",

          DB::Raw("'0' mid") ,
          "ba_business.id as bin",
          "ba_business.name",
          "ba_business.phone_pri as primaryPhone",
          "ba_business.locality as locality" ,
          DB::Raw("'pnd' orderType")
      )
        ->orderBy("ba_pick_and_drop_order.id", "desc")
        ->get();

    }
        //changes ends here for Sanatomb
}
else
{


    $bin = $request->session()->get('_bin_');
    $request->session()->remove('_last_search_date' );
    $status = array( 'new','confirmed', 'cancel_by_client',
        'cancel_by_owner','order_packed','pickup_did_not_come','in_route',
        'package_picked_up','delivery_scheduled' ) ;

    $day_bookings  = DB::table("ba_pick_and_drop_order")
    ->join("ba_business", "ba_business.id", "=", "ba_pick_and_drop_order.request_by")
    ->whereRaw("date(ba_pick_and_drop_order.service_date) >= '$orderDate' ")
    ->whereIn("ba_pick_and_drop_order.book_status", $status)
    ->where("ba_pick_and_drop_order.request_from", "business" )
        //->where("ba_business.frno",  0 )
    ->where('request_by',$bin)
    ->orderBy("ba_pick_and_drop_order.id", "desc")
    ->select("ba_pick_and_drop_order.*",

      DB::Raw("'0' mid") ,
      "ba_business.id as bin",
      "ba_business.name",
      "ba_business.phone_pri as primaryPhone",
      "ba_business.locality as locality" ,
      DB::Raw("'pnd' orderType")

  )
    ->orderBy("ba_pick_and_drop_order.id", "desc")
    ->get();


}

      //finding free agents
$today = date('Y-m-d');

$all_agents = DB::table("ba_profile")
->join("ba_users", "ba_users.profile_id", "=", "ba_profile.id")

->whereIn("ba_profile.id", function($query) use ( $today ) {
  $query->select("staff_id")
  ->from("ba_staff_attendance")
  ->whereDate("log_date",   $today  );
})

->where("ba_users.category", 100)
->where("ba_users.status", "<>" , "100")
->where("ba_profile.fullname", "<>", "" )
->where("ba_users.franchise", 0)

->select( "ba_profile.id", "ba_profile.fullname", "ba_profile.fullname as nameWithTask" ,
    "ba_profile.phone", DB::Raw("'yes' isFree") , "ba_profile.profile_photo" )
->get();

$delivery_orders = DB::table("ba_delivery_order")
->whereRaw(" date(accepted_date) = '$today'" )
->where("completed_on", null )
->where("order_type", 'pick and drop' )
->select("member_id", DB::Raw("count(*) as cnt") )
->groupBy('member_id')
->get();

$free_agents = array();

      //Fetching assigned orders
$delivery_orders_list = DB::table("ba_delivery_order")
->join("ba_profile", "ba_profile.id", "=", "ba_delivery_order.member_id")
->select("ba_delivery_order.*", "ba_delivery_order.id as aid", "ba_profile.fullname")
->whereRaw(" date(accepted_date) ='$today'" )
->get();


foreach ($all_agents as $item)
{
 $is_free = true  ;
 $item->nameWithTask =   $item->nameWithTask . " ( 0 tasks )";
 foreach($delivery_orders as $ditem)
 {
  if($item->id == $ditem->member_id )
  {
    $item->nameWithTask =   $item->fullname . " (" . $ditem->cnt .  " tasks)";
    if( $ditem->cnt >= 80 )
    {
        $is_free = false ;
        break;
    }
}
}

if(  !$is_free )
{
  $item->isFree = "no";
}


          //image compression
if($item->profile_photo != null )
{

    $source = public_path() . "/assets/image/member/" .  $item->profile_photo;
    $pathinfo = pathinfo( $source  );
    $new_file_name  =  $pathinfo['filename'].  "_sm" . '.' . $pathinfo['extension'];
    $destination  = base_path() .  "/public/assets/image/member/"   . $new_file_name ;

    if(file_exists(  $source ) &&  !file_exists( $destination ))
    {
      $info = getimagesize($source );

      if ($info['mime'] == 'image/jpeg')
          $image = imagecreatefromjpeg($source);

      elseif ($info['mime'] == 'image/gif')
          $image = imagecreatefromgif($source);

      elseif ($info['mime'] == 'image/png')
          $image = imagecreatefrompng($source);


      $original_w = $info[0];
      $original_h = $info[1];
                $thumb_w = 290; // ceil( $original_w / 2);
                $thumb_h = 200; //ceil( $original_h / 2 );

                $thumb_img = imagecreatetruecolor($original_w, $original_h);
                imagecopyresampled($thumb_img, $image,
                 0, 0,
                 0, 0,
                 $original_w, $original_h,
                 $original_w, $original_h);

                imagejpeg($thumb_img, $destination, 20);
            }
            $item->profile_photo =  URL::to("/public/assets/image/member/"   . $new_file_name );

        }
        else
        {
          $item->profile_photo = URL::to('/public/assets/image/no-image.jpg')  ;
      }

  }


  $last_keyword = "0";
  if($request->session()->has('_last_search_' ) )
  {
      $last_keyword = $request->session()->get('_last_search_' );
  }


  $last_keyword_date = date('d-m-Y') ;
  if($request->session()->has('_last_search_date' ) )
  {
    $last_keyword_date = $request->session()->get('_last_search_date' );
}

$requests_to_process  = DB::table("ba_order_process_request")
->join("ba_profile", "ba_profile.id", "=", "ba_order_process_request.staff_id")
->select("ba_order_process_request.order_no", "ba_profile.id", "ba_profile.fullname")
->whereDate("log_date", $today )
->get();



$customerphones = $merchants = array();
foreach($day_bookings as $item)
{
    $merchants[] = $item->bin;
    $customerphones[] = $item->phone;
}
$merchants[] = 0;

$merchants = array_unique($merchants);

$businesses  = DB::table("ba_business")
->where("id", $bin )
->get();


$all_business  = DB::table("ba_business")
->where("id", $bin )
->get();


$all_staffs = DB::table("ba_profile")
->join("ba_users", "ba_users.profile_id", "=", "ba_profile.id")
->select( "ba_profile.id", "ba_profile.fullname",  "ba_users.id as user_id" )
->where("ba_users.category", ">", "1")
->where("ba_users.status", "<>" , "0")
->get();

      //dd($all_staffs);
$banned_list = DB::table("ba_ban_list")
->whereIn("phone",  $customerphones)
->get();

$theme = DB::table("ba_theme")
->first();
// ....................
$active_agents =  DB::table("ba_agent_attendance")
->whereRaw("date(log_date) = '$today' " )    
->get();  
$agent_id = array();
$agent_id[] = 0;
foreach($active_agents as $agent)
{
  $agent_id[] = $agent->agent_id;
}
$status = ['ready-for-task', 'ready-for-multiple-task'];
$agents_status = DB::table("ba_agent_locations")
->whereRaw("date(updated_at) = '$today' " )
->whereIn("agent_id", $agent_id )
->whereIn("agent_status",$status)
    // ->select("agent_id ")
->count();

$data = array( 'title'=>'Manage Pick-And-Drop Orders' ,
    'business' => $business,
    'date' => $orderDate, 'all_agents' => $all_agents ,  'staffs' =>$all_staffs,
    'delivery_orders_list' => $delivery_orders_list,
    'results' => $day_bookings,
    'requests_to_process' =>$requests_to_process,
    'all_business' => $all_business,
    'todays_business' => $businesses,
    'last_keyword' => $last_keyword,
    'last_keyword_date' =>  $last_keyword_date ,
    'banned_list' =>$banned_list,
    'refresh' => 'true',
    'agents_status'=>$agents_status,
    'active_agents'=>$active_agents,
    'breadcrumb_menu' => 'pnd_sub_menu',
    'is_premium'=>$is_premium 
);
return view("orders.add_pick_and_drop_orders")->with('data',$data)->with('theme',$theme);
}

protected function convertReceiptToPickAndDropRequest(Request $request)
{

  if(   $request->cname == "" ||  $request->cphone  == ""  ||  $request->address   == "" ||
    $request->sfee   == "" ||  $request->total   == "" ||  $request->servicedate    == "" || $request->erono  == "" )
  {

    return redirect("/admin/customer-care/pick-and-drop-orders/view-all")->with("err_msg", "Important fields are missing!");
}

$new_order = PickAndDropRequestModel::find(   $request->erono  );
if( !isset($new_order) )
{
    return redirect("/admin/customer-care/pick-and-drop-orders/view-all")->with("err_msg",  'No matching receipt found.'  );
}


$address  =  $request->address;
$landmark  =  $request->landmark;
$latitude   =  $request->latitude;
$longitude  =  $request->longitude;

$new_order->otp  =  mt_rand(222222, 999999);
$new_order->fullname  = $request->cname;
$new_order->phone  = $request->cphone ;
$new_order->address = $address;
$new_order->landmark =  ( $landmark  != "" ?  $landmark  : null);
$new_order->latitude =   0.00 ;
$new_order->longitude =  0.00 ;



$new_order->request_by =  $request->ebin ;
$new_order->pickup_name = ( $request->pucname  != "" ?  $request->pucname  : null);
$new_order->pickup_phone =  ( $request->pucphone  != "" ?  $request->pucphone  : null);
$new_order->pickup_address =  ( $request->puaddress  != "" ?  $request->puaddress  : null);
$new_order->pickup_landmark =  ( $request->pulandmark  != "" ?  $request->pulandmark  : null);

$new_order->address = $address;
$new_order->landmark =  ( $landmark  != "" ?  $landmark  : null);
$new_order->latitude =   0.00 ;
$new_order->longitude =  0.00 ;

$new_order->delivery_model = $request->deliverymodel;
$new_order->drop_name = $request->cname;
$new_order->drop_phone =  $request->cphone ;
$new_order->drop_address =  $address;
$new_order->drop_landmark =  ( $landmark  != "" ?  $landmark  : null);
$new_order->drop_latitude =   0.00 ;
$new_order->drop_longitude =  0.00 ;


     //////

$new_order->service_fee = $new_order->requested_fee =  $request->sfee ;
$new_order->total_amount = $request->total  ;
$new_order->service_date = date("Y-m-d", strtotime(  $request->servicedate));
$new_order->pickup_details =  ($request->remarks != "" ? $request->remarks : null);

$new_order->pay_mode =   $request->paymode ;
$save=$new_order->save();

if($save)
{
  return redirect("/admin/customer-care/pick-and-drop-orders/view-all")->with("err_msg",  'Receipt converted to pick-and-drop order!'  );
}
else
{
  return redirect("/admin/customer-care/pick-and-drop-orders/view-all")->with("err_msg",  'Pick-and-drop order could not be saved. Please retry'  );
}
}


protected function removeAgentFromPnDOrder(Request $request)
{

  if( $request->key == ""   )
  {
    return redirect("/admin/customer-care/pick-and-drop-orders/view-all")->with("err_msg", 'Missing data to perform action.'  );
}

      //incrementing
      // DB::table('ba_delivery_order')
      // ->where('order_no', $request->key)
      // ->where('order_type',  "pick and drop")
      // ->delete();


      //  DB::table('ba_pick_and_drop_order')
      // ->where('id', $request->key)
      // ->update(["book_status" => 'new'  ]);

$orderinfo = DeliveryOrderModel::where('order_no',$request->key)->first();


if ($orderinfo->order_type=="pick and drop") {
  $order_type = "pick and drop";
}
else
{
  $order_type = "assist";
}

$delete =  DB::table('ba_delivery_order')
->where('order_no', $request->key)
->where('order_type', $order_type)
->delete();


if ($delete) {
  DB::table('ba_pick_and_drop_order')
  ->where('id', $request->key)
  ->update(["book_status" => 'new'  ]);
}






return redirect("/admin/customer-care/pnd-order/view-details/" .  $request->key )->with("err_msg", 'Agent freed from PnD Order. Please reassign another free agent!'  );
}


protected function processAgentSelfAssignRequest(Request $request)
{

    if(  $request->confsakey == "" ||  $request->confsaaid == ""  )
    {
      return redirect("/admin/customer-care/pick-and-drop-orders/view-all")->with("err_msg", 'Missing data to perform action.');
  }



  $member_info  = CustomerProfileModel::find($request->confsaaid);
  if(   !isset( $member_info)  )
  {
      return redirect("/admin/customer-care/pick-and-drop-orders/view-all")->with("err_msg",  'No matching agent profile found.'   );
  }


  $order_info = PickAndDropRequestModel::find( $request->confsakey );
  if(   !isset( $order_info)  )
  {
    return redirect("/admin/customer-care/pick-and-drop-orders/view-all")->with("err_msg",   'No matching order found.'  );
}


$business_owner  = DB::table("ba_business")
->select( "name", "phone_pri as phone")
->where("id", $order_info->request_by )
->first();

if(  !isset( $business_owner)  )
{
    return redirect("/admin/customer-care/pick-and-drop-orders/view-all")->with("err_msg",     'No matching business found.'  );
}

$delivery_order = DeliveryOrderModel::where("member_id", $request->confsaaid )
->where("order_type", "pick and drop" )
->where("order_no", $request->confsakey)
->first();

if( !isset($delivery_order))
{
  $delivery_order = new DeliveryOrderModel;
}

$delivery_order->member_id = $request->confsaaid;
$delivery_order->order_no = $request->confsakey;
$delivery_order->accepted_date = date('Y-m-d H:i:s');
$delivery_order->order_type =  "pick and drop";
$save = $delivery_order->save();

if($save)
{
  $order_info->book_status = "delivery_scheduled";
  $order_info->save();

  $orderStatus = new OrderStatusLogModel;
  $orderStatus->order_no = $order_info->id;
  $orderStatus->order_status = "delivery_scheduled";
  $orderStatus->log_date = date('Y-m-d H:i:s');
  $orderStatus->save();


         /*

        //send cloud SMS
        $response = array();
        $response['title']     = "New Order Assigned";
        $response['message']   = "Order # " . $order_info->id . " is assigned to you.";
        $response['imageUrl'] = config('app.app_cdn')  . "/assets/image/delivery-task.jpg";

        $receipent = User::where("profile_id",  $request->confsaaid )->first()  ;



        if(  isset($receipent ) )
        {
          $firebase = new FirebaseCloudMessage();
          $regId    = $receipent->firebase_token;
          $response = $firebase->sendToSingleMember( $regId, $response, "agent" );

        }




        //send sms to customer
          $phone = $order_info->phone  ;
          $tracking_sms  =  "Your order (# " . $order_info->id  .
          ") from " . urlencode( $business_owner->name ) . " will be delivered via bookTou. Please track the progress using this link - https://booktou.in/shopping/where-is-my-order/" . $order_info->tracking_session   ;
          $this->sendSmsAlert(  array($phone)   ,  $tracking_sms);

        */


          return redirect("/admin/customer-care/pick-and-drop-orders/view-all")->with("err_msg",      "Order assigned to delivery agent." );

      }
      else
      {
        return redirect("/admin/customer-care/pick-and-drop-orders/view-all")->with("err_msg",    'Order assignment to agent failed.' );
    }


}

protected function updatePnDOrderRemarks(Request $request)
{
    $orderno = $request->orderno;
    if($orderno == "")
    {
      return redirect("/admin/customer-care/pick-and-drop-orders/view-all");
  }

  if( $request->remarks == "" || $request->type == "" )
  {
      if($request->turl == "list")
      {
        return redirect("/admin/customer-care/pick-and-drop-orders/view-all")->with("err_msg", "Order remarks fields missing!");
    }
    else
    {
        return redirect("/admin/customer-care/pnd-order/view-details/" . $orderno )->with("err_msg", "Order remarks fields missing!");
    }

}

$order_info   =  PickAndDropRequestModel::find( $orderno) ;

if( !isset($order_info))
{
  return redirect("/admin/customer-care/pick-and-drop-orders/view-all")->with("err_msg", "No matching order found!");
}

$remark = new OrderRemarksModel();
$remark->order_no = $orderno;
$remark->remarks = ($request->remarks == "" ? "payment info update" :  $request->remarks ) ;
$remark->rem_type = $request->type ;
$remark->remark_by = $request->session()->get('_member_id' );
$remark->save();

if($request->turl == "list")
{
    return redirect("/admin/customer-care/pick-and-drop-orders/view-all")->with("err_msg", "Order remarks updated!");
}
else
{
    return redirect("/admin/customer-care/pnd-order/view-details/" . $orderno )->with("err_msg", "Order remarks updated!");
}


}


protected function viewPnDOrderDetailsforErp(Request $request)
{

 $orderno = $request->orderno;

 if($orderno == "")
 {
    return redirect("/orders/view-active-orders");
}

$pndOrder = PickAndDropRequestModel::where("id", $orderno)->first();

if(!isset($pndOrder))
{
    return redirect("/orders/view-active-orders");
}



$from = $pndOrder->request_from;

$order_info   = DB::table("ba_pick_and_drop_order")
->join("ba_business", "ba_pick_and_drop_order.request_by", "=", "ba_business.id")
->select("ba_pick_and_drop_order.*",
    "ba_business.name as businessName","ba_business.locality as businessLocality",
    "ba_business.landmark as businessLandmark", "ba_business.city as businessCity",
    "ba_business.state as businessState", "ba_business.pin as businessPin",
    "ba_business.phone_pri as businessPhone",  "ba_business.banner","ba_business.commission" )
->where("ba_pick_and_drop_order.id", $orderno )
      //->where("request_from", "business")
->where("request_from", $from)
->first() ;

if( !isset($order_info))
{
    return redirect("/admin/customer-care/business/view-all"  );
}


$agent_info=  DB::table("ba_delivery_order")
->join("ba_profile", "ba_delivery_order.member_id", "=", "ba_profile.id")
->select("ba_profile.fullname as deliveryAgentName","ba_profile.phone as deliveryAgentPhone")
->where("order_no", $orderno  )
->where("order_type",  "pick and drop" )
->first();

$remarks    = DB::table("ba_order_remarks")
->where("order_no", $orderno )
->orderBy("id", "asc")
->get() ;


$today = date('Y-m-d');
$active_agents = DB::table("ba_profile")
      ->join("ba_users", "ba_users.profile_id", "=", "ba_profile.id")

      ->whereIn("ba_profile.id", function($query) use ( $today ) { 
          $query->select("agent_id")
          ->from("ba_agent_attendance")
          ->whereDate("log_date",   $today  ); 
        })

      ->where("ba_users.category", 100)
      ->where("ba_users.status", "<>" , "100") 
      ->where("ba_profile.fullname", "<>", "" )
      ->where("ba_users.franchise", 0) 

      ->select( "ba_profile.id", "ba_profile.fullname", "ba_profile.fullname as nameWithTask" , 
        "ba_profile.phone", DB::Raw("'yes' isFree") , "ba_profile.profile_photo" )  
      ->get();

$theme = DB::table("ba_theme")
->first();



$result = array('order_info' => $order_info ,
 'agent_info' => $agent_info ,
 'remarks' => $remarks,
 'active_agents'=>$active_agents);
return view("orders.pnd_order_detailed_view_2")->with('data',$result)->with('theme',$theme);
}
protected function removeCouponDiscount(Request $request)
{

  if($request->btnsave =='save')
  {
    $orderno = $request->key;
    $order_info = ServiceBooking::find($orderno);
    if( !isset($order_info))
    {
        return redirect("/admin/customer-care/orders/view-all")->with("err_msg", 'No order found to remove coupon.'  );
    }
    else
    {
      $order_costing =  DB::table("ba_shopping_basket")
      ->where("order_no", $orderno  )
      ->get();

      if( !isset($order_costing))
      {
        return redirect("admin/customer-care/orders/view-all")->with("err_msg", 'No order found to remove coupon.'  );
    }

    $item_cost = 0.00;
    $packaging_cost = 0.00;
    foreach($order_costing as $item)
    {
        $item_cost  += $item->qty * $item->price;
        $packaging_cost +=  $item->qty * $item->package_charge;
    }

    $order_info->coupon  = null ;
    $order_info->discount = 0;
    $order_info->seller_payable =  $order_info->total_cost =  ($item_cost + $packaging_cost);
    $order_info->save();

    return redirect("/admin/customer-care/order/view-details/" .  $orderno )->with("err_msg", 'Coupon discount removed.'  );
}
}


}

protected function updatePnDOrderPaymentUpdate(Request $request)
{

  $orderno = $request->orderno;
  if($orderno == "")
  {
    return redirect("/admin/customer-care/business/view-all");
}

if( $request->paytarget == "" || $request->paymode == "" )
{
    return redirect("/admin/customer-care/pnd-order/view-details/" . $orderno )->with("err_msg", "PnD payment info missing!");
}


$order_info   = PickAndDropRequestModel::find( $orderno) ;

if( !isset($order_info))
{
    return redirect("/admin/customer-care/business/view-all"  )->with("err_msg", "No matching order found!");
}

$paytarget =   $request->paytarget ;
$paymode =  $request->paymode ;


        //$order_info->pay_mode = $paymode;
$order_info->payment_target =$paytarget;
$order_info->cc_remarks =  $paymode ;

if($request->total != "" )
{
  $order_info->total_amount =  $request->total ;
}

if($request->delivery != "" )
{
  $order_info->requested_fee  =  $order_info->service_fee = $request->delivery;
}

$order_info->save();


        //save remarks
$remark = new OrderRemarksModel();
$remark->order_no =  $orderno ;
$remark->remarks = $request->remarks ;
$remark->rem_type = "payment update" ;
$remark->remark_by = $request->session()->get('__member_id_' );
$remark->save();


return redirect("/admin/customer-care/pnd-order/view-details/" . $orderno )->with("err_msg", "PnD payment information updated!");


}

protected function updatePnDOrderStatus(Request $request)
{

 $orderno = $request->orderno;
 if($orderno == "")
 {
  return redirect("/admin/customer-care/pick-and-drop-orders/view-all");
}

if( $request->status == ""   )
{
  return redirect("/admin/customer-care/pnd-order/view-details/" . $orderno )
  ->with("err_msg", "Order status missing!");
}


$order_info   = PickAndDropRequestModel::find( $orderno) ;

if( !isset($order_info))
{
  return redirect("/admin/customer-care/pick-and-drop-orders/view-all")->with("err_msg", "No matching order found!");
}

$order_info->book_status =  $request->status ;
$order_info->save();


        //save remarks
$remark = new OrderRemarksModel();
$remark->order_no =  $orderno ;
$remark->remarks = $request->remarks ;
$remark->rem_type = "order status update" ;
$remark->remark_by = $request->session()->get('__member_id_' );
$remark->save();


$orderStatus = new OrderStatusLogModel;
$orderStatus->order_no =  $orderno ;
$orderStatus->order_status =  $request->status ;
$orderStatus->remarks =  $request->remarks ;
$orderStatus->log_date = date('Y-m-d H:i:s');
$orderStatus->save();


if(  $order_info->request_from == "business")
{
    return redirect("/admin/customer-care/pnd-order/view-details/" . $orderno )
    ->with("err_msg", "Order status updated!");
}
else  if(  $order_info->request_from == "customer")
{
    return redirect("/admin/customer-care/assist-order/view-details/" . $orderno )
    ->with("err_msg", "Order status updated!");
}
else
{
  return redirect("/admin/customer-care/pick-and-drop-orders/view-all");
}


}


      //View completed pick and drop
protected function viewPickAndDropCompletedOrders(Request $request)
{

  if( isset($request->filter_date))
  {
    $orderDate = date('Y-m-d', strtotime($request->filter_date));
    $today  = date('Y-m-d', strtotime($request->filter_date));
}
else
{
    $today = date('Y-m-d');
    $orderDate = date('Y-m-d');
}

$bin = $request->session()->get('_bin_');

if( isset($request->btn_search) )
{

    $request->session()->put('_last_search_', $request->filter_status);
    $request->session()->put('_last_search_date',  $request->filter_date );


    $dbstatus = $request->filter_status;

    if( $dbstatus != "")
    {
        if( $dbstatus == "-1")
        {
          $status = array(  'cancel_by_client', 'cancel_by_owner', 'delivered', 'cancelled'  ) ;
      }
      else
      {
          $status[] =  $dbstatus;
      }
  }
  else
  {
      $status =  array(  'cancel_by_client', 'cancel_by_owner', 'delivered', 'cancelled'  ) ;
  }


  $day_bookings = DB::table("ba_pick_and_drop_order")
  ->join("ba_business", "ba_business.id", "=", "ba_pick_and_drop_order.request_by")
  ->whereRaw("date(ba_pick_and_drop_order.service_date) = '$orderDate' ")
  ->whereIn("ba_pick_and_drop_order.book_status", $status)
  ->orderBy("ba_pick_and_drop_order.id", "desc")
  ->select("ba_pick_and_drop_order.*",
   "ba_business.id as bin", "ba_business.name", "ba_business.phone_pri as businessPhone" )
  ->get();


}
else
{

    $request->session()->remove('_last_search_date' );
    $status = array(  'cancel_by_client', 'cancel_by_owner', 'delivered', 'cancelled'  ) ;

    $day_bookings = DB::table("ba_pick_and_drop_order")
    ->join("ba_business", "ba_business.id", "=", "ba_pick_and_drop_order.request_by")
    ->whereRaw("date(ba_pick_and_drop_order.service_date) >= '$orderDate' ")
    ->whereIn("ba_pick_and_drop_order.book_status", $status)
    ->orderBy("ba_pick_and_drop_order.id", "desc")
    ->select("ba_pick_and_drop_order.*",
       "ba_business.id as bin", "ba_business.name", "ba_business.phone_pri as businessPhone" )
    ->get();

}

$all_agents = DB::table("ba_profile")
->join("ba_users", "ba_users.profile_id", "=", "ba_profile.id")
->select( "ba_profile.id", "ba_profile.fullname", "ba_profile.fullname as nameWithTask" ,
    "ba_profile.phone", DB::Raw("'yes' isFree") , "ba_profile.profile_photo" )
->where("ba_users.category", 100)
->where("ba_users.status", "<>" , "100")
->where("ba_profile.fullname", "<>", "" )
->get();

$delivery_orders = DB::table("ba_delivery_order")
->whereRaw(" date(accepted_date) = '$today'" )
->where("completed_on", null )
->where("order_type", 'pick and drop' )
->select("member_id", DB::Raw("count(*) as cnt") )
->groupBy('member_id')
->get();

$free_agents = array();

      //Fetching assigned orders
$delivery_orders_list = DB::table("ba_delivery_order")
->join("ba_profile", "ba_profile.id", "=", "ba_delivery_order.member_id")
->select("ba_delivery_order.*", "ba_delivery_order.id as aid", "ba_profile.fullname")
->whereRaw(" date(accepted_date) ='$today'" )
->get();


foreach ($all_agents as $item)
{
 $is_free = true  ;
 $item->nameWithTask =   $item->nameWithTask . " ( 0 tasks )";
 foreach($delivery_orders as $ditem)
 {
  if($item->id == $ditem->member_id )
  {
    $item->nameWithTask =   $item->fullname . " (" . $ditem->cnt .  " tasks)";
    if( $ditem->cnt >= 50 )
    {
        $is_free = false ;
        break;
    }
}
}

if(  !$is_free )
{
  $item->isFree = "no";
}


          //image compression
if($item->profile_photo != null )
{

    $source = public_path() . "/assets/image/member/" .  $item->profile_photo;
    $pathinfo = pathinfo( $source  );
    $new_file_name  =  $pathinfo['filename'].  "_sm" . '.' . $pathinfo['extension'];
    $destination  = base_path() .  "/public/assets/image/member/"   . $new_file_name ;

    if(file_exists(  $source ) &&  !file_exists( $destination ))
    {
      $info = getimagesize($source );

      if ($info['mime'] == 'image/jpeg')
          $image = imagecreatefromjpeg($source);

      elseif ($info['mime'] == 'image/gif')
          $image = imagecreatefromgif($source);

      elseif ($info['mime'] == 'image/png')
          $image = imagecreatefrompng($source);


      $original_w = $info[0];
      $original_h = $info[1];
                $thumb_w = 290; // ceil( $original_w / 2);
                $thumb_h = 200; //ceil( $original_h / 2 );

                $thumb_img = imagecreatetruecolor($original_w, $original_h);
                imagecopyresampled($thumb_img, $image,
                 0, 0,
                 0, 0,
                 $original_w, $original_h,
                 $original_w, $original_h);

                imagejpeg($thumb_img, $destination, 20);
            }
            $item->profile_photo =  URL::to("/public/assets/image/member/"   . $new_file_name );

        }
        else
        {
          $item->profile_photo = URL::to('/public/assets/image/no-image.jpg')  ;
      }

  }


  $last_keyword = "0";
  if($request->session()->has('_last_search_' ) )
  {
      $last_keyword = $request->session()->get('_last_search_' );
  }


  $last_keyword_date = date('d-m-Y') ;
  if($request->session()->has('_last_search_date' ) )
  {
    $last_keyword_date = $request->session()->get('_last_search_date' );
}


$all_business  = DB::table("ba_business")
->where("id", $bin )
->get();

$theme = DB::table("ba_theme")
->first();

$data = array( 'title'=>'Manage Pick-And-Drop Orders' ,
    'date' => $orderDate, 'all_agents' => $all_agents ,
    'delivery_orders_list' => $delivery_orders_list,
    'results' => $day_bookings,
    'todays_business' =>$all_business,
    'last_keyword' => $last_keyword, 'last_keyword_date' =>  $last_keyword_date ,
    'refresh' => 'true',
    'breadcrumb_menu' => 'pnd_old_sub_menu'
);
return view("orders.pick_and_drop_old_orders_table")->with('data',$data)->with('theme',$theme);
}



protected function removePickAndDropOrders(Request $request)
{

  if ($request->pndid=="") {
   return redirect("/admin/customer-care/pick-and-drop-orders/view-all")
   ->with("err_msg", "mismatch information!");
}

$id = $request->pndid;

$deleterOrderSequence = DB::table('ba_order_sequencer')
->where('id','=',$id)->delete();

$deletepnd = DB::table('ba_pick_and_drop_order')
->where('id','=',$id)->delete();


if ($deleterOrderSequence && $deletepnd) {

    return redirect("/admin/customer-care/pick-and-drop-orders/view-all")
    ->with("err_msg", "record deleted sucessfull!");
}


}




protected function updateAssistOrder(Request $request)
{

  if(   $request->cname == "" ||  $request->cphone  == ""  ||  $request->address   == "" ||
    $request->sfee   == "" ||  $request->total   == "" ||  $request->servicedate    == "" || $request->erono  == "" )
  {

    return redirect("/admin/customer-care/pick-and-drop-orders/view-all")->with("err_msg", "Important fields are missing!");
}

$new_order = PickAndDropRequestModel::find(   $request->erono  );
if( !isset($new_order) )
{
    return redirect("/admin/customer-care/pick-and-drop-orders/view-all")->with("err_msg",  'No matching receipt found.'  );
}


$address  =  $request->address;
$landmark  =  $request->landmark;
$latitude   =  $request->latitude;
$longitude  =  $request->longitude;

$new_order->otp  =  mt_rand(222222, 999999);
$new_order->fullname  = $request->cname;
$new_order->phone  = $request->cphone ;
$new_order->address = $address;
$new_order->landmark =  ( $landmark  != "" ?  $landmark  : null);
$new_order->latitude =   0.00 ;
$new_order->longitude =  0.00 ;


$new_order->pickup_name = ( $request->pucname  != "" ?  $request->pucname  : null);
$new_order->pickup_phone =  ( $request->pucphone  != "" ?  $request->pucphone  : null);
$new_order->pickup_address =  ( $request->puaddress  != "" ?  $request->puaddress  : null);
$new_order->pickup_landmark =  ( $request->pulandmark  != "" ?  $request->pulandmark  : null);

$new_order->address = $address;
$new_order->landmark =  ( $landmark  != "" ?  $landmark  : null);
$new_order->latitude =   0.00 ;
$new_order->longitude =  0.00 ;


$new_order->drop_name = $request->cname;
$new_order->drop_phone =  $request->cphone ;
$new_order->drop_address =  $address;
$new_order->drop_landmark =  ( $landmark  != "" ?  $landmark  : null);
$new_order->drop_latitude =   0.00 ;
$new_order->drop_longitude =  0.00 ;


     //////

$new_order->service_fee = $new_order->requested_fee =  $request->sfee ;
$new_order->total_amount = $request->total  ;
$new_order->service_date = date("Y-m-d", strtotime(  $request->servicedate));
$new_order->pickup_details =  ($request->remarks != "" ? $request->remarks : null);

$new_order->pay_mode =   $request->paymode ;
$save=$new_order->save();

if($save)
{
  return redirect("/admin/customer-care/pick-and-drop-orders/view-all")->with("err_msg",  'Receipt converted to pick-and-drop order!'  );
}
else
{
  return redirect("/admin/customer-care/pick-and-drop-orders/view-all")->with("err_msg",  'Pick-and-drop order could not be saved. Please retry'  );
}
}



protected function searchByDataEntered(Request $request)
{
    $keyword = $request->searchProduct;
    $bin = $request->session()->get('_bin_');

    if ($keyword=="") {
     return redirect('/all-products');
 }

 if (is_numeric($keyword)) {

  $order_info = OrderSequencerModel::find($keyword);

  if (!$order_info) {

   return redirect('product/order-by-keyword')
   ->with('err_msg','please specify correct order no!');
}else{

        //if order info is found

    switch ($order_info->type) {

      case 'pnd':
      return redirect('admin/customer-care/pnd-order/view-details/'.$order_info->id);
      break;

      case 'normal':
      return redirect('order/view-details/'.$order_info->id);
      break;

      case 'booking':
      return redirect('order/view-details/'.$order_info->id);
      break;

      case 'assist':
      return redirect('admin/customer-care/assist-order/view-details/'.$order_info->id);
      break;

  }




}



}else{

  $product_list = DB::table('ba_products')
  ->join("ba_product_variant","ba_product_variant.prid","ba_products.id")
  ->where('pr_name', 'like','%'.$keyword.'%')
  ->where('bin',$bin)
  ->orderBy("pr_name", "asc")
  ->paginate(20);

  $allCategories= DB::table("ba_product_category")
  ->get();

  $theme = DB::table("ba_theme")
  ->first();

  $data = array('products' =>  $product_list ,  'allCategories' => $allCategories,  'bin' => $bin);
  return view('products.search_product_by_keyword')->with('data',$data)->with('theme',$theme);
}



}



protected function viewAssistOrderDetails(Request $request)
{

 $orderno = $request->orderno;

 if($orderno == "")
 {
    return redirect("/all-products");
}

$pndOrder = PickAndDropRequestModel::find($orderno);

$from = $pndOrder->request_from;

$order_info   = DB::table("ba_pick_and_drop_order")
->join("ba_profile", "ba_pick_and_drop_order.request_by", "=", "ba_profile.id")

->select("ba_pick_and_drop_order.*")
->where("ba_pick_and_drop_order.id", $orderno )
      //->where("request_from", "business")
->where("request_from", $from)
->first() ;

if( !isset($order_info))
{
    return redirect("/all-products"  );
}


$agent_info=  DB::table("ba_delivery_order")
->join("ba_profile", "ba_delivery_order.member_id", "=", "ba_profile.id")
->select("ba_profile.fullname as deliveryAgentName","ba_profile.phone as deliveryAgentPhone")
->where("order_no", $orderno  )
->where("order_type",  "pick and drop" )
->first();

$remarks    = DB::table("ba_order_remarks")
->where("order_no", $orderno )
->orderBy("id", "asc")
->get() ;


$today = date('Y-m-d');
$all_agents = DB::table("ba_profile")
->join("ba_users", "ba_users.profile_id", "=", "ba_profile.id")

->whereIn("ba_profile.id", function($query) use ( $today ) {
  $query->select("staff_id")
  ->from("ba_staff_attendance")
  ->whereDate("log_date",   $today  );
})

->where("ba_users.category", 100)
->where("ba_users.status", "<>" , "100")
->where("ba_profile.fullname", "<>", "" )
->where("ba_users.franchise", 0)

->select( "ba_profile.id", "ba_profile.fullname", "ba_profile.fullname as nameWithTask" ,
    "ba_profile.phone", DB::Raw("'yes' isFree") , "ba_profile.profile_photo" )
->get();

$theme = DB::table("ba_theme")
->first();

$result = array('order_info' => $order_info ,
 'agent_info' => $agent_info ,
 'all_agents' => $all_agents ,
 'remarks' => $remarks);
return view("orders.assist_order_detailed_view")->with('data',$result)->with('theme',$theme);


}


protected function  orderByKeyword()
{
    $theme = DB::table("ba_theme")
    ->first();
    return view('orders.search_order_by_keyword')->with('theme',$theme);

}



public function downloadReceiptForErp( Request $request)
{


  $order_info = $order_items =null;
  if( $request->o  != ""  )
  {
    $order_info = ServiceBooking::find( $request->o );



    $order_items = DB::table("ba_shopping_basket")
    ->where("order_no", $request->o )
    ->get( );
    $pdf_file =  "rcp-" .time(). '-' . $request->o . ".pdf";
    $save_path = public_path() . "/assets/erp/bills/"  .  $pdf_file    ;

    $pdf = PDF::setOptions(['defaultFont' => 'sans-serif'  ])
    ->setPaper('a4', 'portrait')
    ->loadView('docs.bill_small_size' ,  array('data' =>   $order_info , 'order_items' => $order_items )  )   ;


    return $pdf->download( $pdf_file  );
}
else
{
    return ;
}
}



protected function salesAnalytics(Request $request)
{

    $bin = session()->get('_bin_');

    if($bin == "")
    {
      return redirect("/erp/customer-care/business/view-all")->with("err_msg", "No business selected!");
  }


  $year =  date('Y');
  $month = date('m');
  $status = [ 'completed', 'delivered'];

  $booking_list = DB::table('ba_service_booking')
  ->where('bin',$bin)
  ->whereYear('service_date',$year)
  ->whereMonth('service_date',$month)
  ->whereIn('book_status',$status)
  ->select()
  ->get();


  $orderno = array();
  foreach ($booking_list as $booking) {

    $orderno[]=$booking->id;
}

$result = DB::table('ba_shopping_basket')
->select("ba_shopping_basket.pr_name",
    DB::Raw('sum(ba_shopping_basket.qty) as quantity ,
       sum(ba_shopping_basket.qty * ba_shopping_basket.price) as actualPrice,

       sum(ba_shopping_basket.cgst) as cGST,
       sum(ba_shopping_basket.sgst) as sGST '))
->whereIn('ba_shopping_basket.order_no',$orderno)
->groupBy('ba_shopping_basket.pr_name')
->get();


    //$dataResult =  array( );

$months=array();
$sales=0;

foreach ($result as  $value) {
 $sales+= $value->actualPrice;
}


$revenueMonthResult = DB::table('ba_service_booking')
->select(
    DB::Raw('sum(if(month(service_date) = 1,  service_fee ,0)) as Jan,
       sum(if(month(service_date) = 2,  service_fee ,0)) as Feb ,
       sum(if(month(service_date) = 3,  service_fee ,0)) as Mar ,
       sum(if(month(service_date) = 4,  service_fee ,0)) as  Apr ,
       sum(if(month(service_date) = 5,  service_fee ,0)) as  May ,
       sum(if(month(service_date) = 6,  service_fee ,0)) as  Jun ,
       sum(if(month(service_date) = 7,  service_fee ,0)) as   Jul ,
       sum(if(month(service_date) = 8,  service_fee ,0)) as  Aug ,
       sum(if(month(service_date) = 9,  service_fee ,0)) as  Sep ,
       sum(if(month(service_date) = 10, service_fee ,0)) as  Oct ,
       sum(if(month(service_date) = 11, service_fee  ,0)) as  Nov ,
       sum(if(month(service_date) = 12, service_fee ,0)) as  "Dec" '))
->whereYear('service_date',$year)
->where('bin',"=",$bin)
->whereIn('book_status',$status)
->get();

$revdata = array();

foreach ($revenueMonthResult as $rev) {
   array_push(
      $revdata,$rev->Jan,$rev->Feb,$rev->Mar,$rev->Apr,$rev->May,
      $rev->Jun,$rev->Jul,$rev->Aug,$rev->Sep,$rev->Oct,$rev->Nov,
      $rev->Dec);


}


    //pnd commission calculation logic

$PNDCommisionMonthWiseResult = DB::table('ba_pick_and_drop_order')
->join('ba_business','ba_pick_and_drop_order.request_by','=','ba_business.id')
->select(
    DB::Raw(
        'sum(if(month(service_date) = 1,total_amount*commission/100,0)) as Jan,
        sum(if(month(service_date) = 2,total_amount*commission/100,0)) as Feb,
        sum(if(month(service_date) = 3,total_amount*commission/100,0)) as Mar,
        sum(if(month(service_date) = 4,total_amount*commission/100,0)) as Apr,
        sum(if(month(service_date) = 5,total_amount*commission/100,0)) as May,
        sum(if(month(service_date) = 6,total_amount*commission/100,0)) as Jun,
        sum(if(month(service_date) = 7,total_amount*commission/100,0)) as Jul,
        sum(if(month(service_date) = 8,total_amount*commission/100,0)) as Aug,
        sum(if(month(service_date) = 9,total_amount*commission/100,0)) as Sep,
        sum(if(month(service_date) = 10,total_amount*commission/100,0))as Oct,
        sum(if(month(service_date) = 11,total_amount*commission/100,0))as Nov,
        sum(if(month(service_date) = 12,total_amount*commission/100,0)) as "Dec" '))
->whereYear('service_date',$year)
->where('request_by',$bin)
->where(DB::Raw('source="booktou" or source="customer" '))
->whereIn('book_status',$status)
->get();

//setting the variable for commision
$jantotal=0;$febtotal=0;$martotal=0;$aprtotal=0;
$maytotal=0;$juntotal=0;$jultotal=0;$augtotal=0;
$septotal=0;$octtotal=0;$novtotal=0;$dectotal=0;
//variable setting ends here

foreach ($PNDCommisionMonthWiseResult as $commission) {
  $jantotal = $commission->Jan;
  $febtotal = $commission->Feb;
  $martotal = $commission->Mar;
  $aprtotal = $commission->Apr;
  $maytotal = $commission->May;
  $juntotal = $commission->Jun;
  $jultotal = $commission->Jul;
  $augtotal = $commission->Aug;
  $septotal = $commission->Sep;
  $octtotal = $commission->Oct;
  $novtotal = $commission->Nov;
  $dectotal = $commission->Dec;
}

    //ends here


       //pnd revenue months
$PNDrevenueMonthWiseResult = DB::table('ba_pick_and_drop_order')
->select(
    DB::Raw('sum(if(month(service_date) = 1, service_fee,0)) as Jan,
       sum(if(month(service_date) = 2, service_fee,0)) as Feb ,
       sum(if(month(service_date) = 3,service_fee,0)) as Mar ,
       sum(if(month(service_date) = 4, service_fee,0)) as  Apr ,
       sum(if(month(service_date) = 5, service_fee,0)) as  May ,
       sum(if(month(service_date) = 6, service_fee,0)) as  Jun ,
       sum(if(month(service_date) = 7, service_fee,0)) as   Jul ,
       sum(if(month(service_date) = 8, service_fee,0)) as  Aug ,
       sum(if(month(service_date) = 9, service_fee,0)) as  Sep ,
       sum(if(month(service_date) = 10, service_fee,0)) as  Oct ,
       sum(if(month(service_date) = 11, service_fee,0)) as  Nov ,
       sum(if(month(service_date) = 12, service_fee,0)) as  "Dec"  '))
->whereYear('service_date',$year)
->where('request_by',"=",$bin)
->whereIn('book_status',$status)
->get();

       //echo json_encode($revenueMonthResult);die();


$pnddata=array();

foreach ($PNDrevenueMonthWiseResult as  $pnd) {
  array_push(
      $pnddata,
      $pnd->Jan-$jantotal,
      $pnd->Feb-$febtotal,
      $pnd->Mar-$martotal,
      $pnd->Apr-$aprtotal,
      $pnd->May-$maytotal,
      $pnd->Jun-$juntotal,
      $pnd->Jul-$jultotal,
      $pnd->Aug-$augtotal,
      $pnd->Sep-$septotal,
      $pnd->Oct-$octtotal,
      $pnd->Nov-$novtotal,
      $pnd->Dec-$dectotal);
}



$PNDrevenueMonthResult = DB::table('ba_pick_and_drop_order')
->select(
    DB::Raw('sum(total_amount) as totalAmount'))
->whereYear('service_date',$year)
->whereMonth('service_date',$month)
->where('request_by',"=",$bin)
->whereIn('book_status',$status)
->get();

$pndAmount = 0;
foreach ($PNDrevenueMonthResult as  $value) {
    $pndAmount+= $value->totalAmount;

}

$PNDResult = DB::table('ba_pick_and_drop_order')
->join('ba_business','ba_pick_and_drop_order.request_by', '=' ,'ba_business.id')
->whereYear('service_date',$year)
->whereMonth('service_date',$month)
->where('request_by',"=",$bin)
->whereIn('book_status',$status)
->select('cust_remarks','source','commission','total_amount')
->get();
    //dd($PNDrevenueMonthResult);die();

    //$datarevenueResult =  array('rev_list' => $revenueMonthResult );
    //$dataPNDResult =  array('pnd_list' => $PNDResult );




     //calculating top 10 product list
$booking_product_10 = DB::table('ba_service_booking')
->where('bin',$bin)
->whereYear('service_date',$year)
->whereMonth('service_date',$month)
->whereIn('book_status',$status)
->select()
->get();

$order_id = array();

foreach ($booking_product_10 as $order) {
  $order_id[]=$order->id;
}


$top_5_productlist = DB::table('ba_shopping_basket')
->select("ba_shopping_basket.pr_name",
    DB::Raw('sum(ba_shopping_basket.qty) as quantity'))
->whereIn('ba_shopping_basket.order_no',$order_id)
->groupBy('ba_shopping_basket.pr_name')
->orderBy('quantity','desc')
->take(5)
->get();

$theme = DB::table("ba_theme")
->first();

    //ends here
$data = array('serv_list' => $result,
  'rev_list'=>$revenueMonthResult,
  'pnd_list'=>$PNDResult,
  'revdata'=>$revdata,
  'pnddata'=>$pnddata,
  'sales'=>$sales,
  'pndsales'=>$pndAmount,
  'most_sales_product'=>$top_5_productlist,
  'theme'=>$theme );

return view('sales.sales_analytics')->with($data);

}

    //sales analytics ends here for normal and pnd

    //sales analytics for pos
protected function posSalesAnalytics(Request $request)
{

    $bin = session()->get('_bin_');
    
    if($bin == "")
    {
        return redirect("/erp/customer-care/business/view-all")->with("err_msg", "No business selected!");
    }


    $year =  date('Y');
    $month = date('m');
    $status = [ 'completed', 'delivered'];

        //monthly pos order details
    $booking_list = DB::table('ba_pos_order')
    ->where('bin',$bin)
    ->whereYear('service_date',$year)
    ->whereMonth('service_date',$month)
    ->whereIn('book_status',$status)
    ->get(); 
    
    $orderno = array();
    foreach ($booking_list as $booking) {

        $orderno[]=$booking->id;
    }


    $result = DB::table('ba_pos_order_basket')
    ->select("ba_pos_order_basket.pr_name",
        DB::Raw('sum(ba_pos_order_basket.qty) as quantity ,
           sum(ba_pos_order_basket.qty * ba_pos_order_basket.price) as actualPrice,

           sum(ba_pos_order_basket.cgst) as cGST,
           sum(ba_pos_order_basket.sgst) as sGST '))
    ->whereIn('ba_pos_order_basket.id',$orderno)
    ->groupBy('ba_pos_order_basket.pr_name')
    ->get();
        //monthly pos order ends here
    

        //yearly fetch pos order result
    $pos_yearly_result = DB::table('ba_pos_order')
    ->where('bin',$bin)
    ->whereYear('service_date',$year) 
    ->whereIn('book_status',$status)
    ->select(
      DB::Raw("month(service_date) as monthName"),
      "biz_order_no as bizOrderNo",
      "id as orderNo",
      DB::Raw("'0' revenueAmount")
  )
    ->get();

    $bizOrderNo = $prSubid = $order_no = array();
    foreach ($pos_yearly_result as $items) {
     $order_no[]=$items->bizOrderNo;
     $order_no[] = $items->orderNo; 
 }

 $pos_shopping_basket = DB::table('ba_pos_order_basket') 
 ->whereIn("order_no",$order_no) 
 ->select(
  
   DB::Raw("'0' purchaseRate"), 
   "qty as quantity",
   "price as salesPrice",
   "order_no as orderNo",
   "prsubid"
)  
 ->get(); 
 
 
 foreach ($pos_shopping_basket as $shopping) {
    $prSubid[] = $shopping->prsubid;
}

$product_inventory = DB::table("ba_product_inventory")
->whereIn('pr_subid',$prSubid)
->where("ba_product_inventory.status",1)
->select(
  "rate as purchaseRate" ,"pr_subid as prsubid"
)

->get();
foreach ($pos_shopping_basket as $items) {
 foreach ($product_inventory as $list) {
  if ($items->prsubid == $list->prsubid) {
   $items->purchaseRate = $items->salesPrice - $items->quantity*$list->purchaseRate;
   break;
}
}
}


        //revenue generation for ba_pos_orders

foreach ($pos_yearly_result as $pyr) {
  $rate = 0;
  foreach ($pos_shopping_basket as $basket) {
      if ($pyr->orderNo==$basket->orderNo) {
          $rate += $basket->purchaseRate;
          $pyr->revenueAmount = $rate;
      }
  }
}

        //setting for months;
$months_list = ["Jan"=>0,"Feb"=>0,"Mar"=>0,"Apr"=>0,"May"=>0,"Jun"=>0,"Jul"=>0,"Aug"=>0,"Sep"=>0,"Oct"=>0,"Nov"=>0,"Dec"=>0];
        //
$posMonthlyRevenueData = array();

         //getting revenue amount accoring to monthwise
$jan = $feb=$mar = $apr  = $may = $june = $july = $aug = $sep = $oct = $nov = $dec =0;

foreach ($pos_yearly_result as $list) {
  switch ($list->monthName) {
      case '1':
      $jan += $list->revenueAmount;
      break;
      case '2':
      $feb += $list->revenueAmount;
      break;
      case '3':
      $mar += $list->revenueAmount;
      break;
      case '4':
      $apr += $list->revenueAmount;
      break;
      case '5':
      $may += $list->revenueAmount;
      break;
      case '6':
      $june += $list->revenueAmount;
      break;
      case '7':
      $july += $list->revenueAmount;
      break;
      case '8':
      $aug += $list->revenueAmount;
      break;
      case '9':
      $sep += $list->revenueAmount;
      break;
      case '10':
      $oct += $list->revenueAmount;
      break;
      case '11':
      $nov += $list->revenueAmount;
      break;
      
      default:
      $dec += $list->revenueAmount;
      break;
  }  
}    
         //monthwise data for revenue ends here
array_push($posMonthlyRevenueData,$jan,$feb,$mar,$apr,$may,$june,$july,$aug,$sep,$oct,$nov,$dec);  
        //pos orders ends here 

$pos_product_10 = DB::table('ba_pos_order')
->where('bin',$bin)
->whereYear('service_date',$year)
->whereMonth('service_date',$month)
->whereIn('book_status',$status)
->select()
->get();

$order_id = array();

foreach ($pos_product_10 as $order) {
  $order_id[]=$order->id;
}

$top_10_productlist = DB::table('ba_pos_order_basket')
->select("ba_pos_order_basket.pr_name",
    DB::Raw('sum(ba_pos_order_basket.qty) as quantity'))
->whereIn('ba_pos_order_basket.order_no',$order_id)
->groupBy('ba_pos_order_basket.pr_name')
->orderBy('quantity','desc')
->take(10)
->get();

$theme = DB::table("ba_theme")
->first();

    //ends here
$data = array(
  'result' => $result, 
  'pos_monthly_revenue'=>$posMonthlyRevenueData,
  'most_sales_product'=>$top_10_productlist,
  'theme'=>$theme );

return view('sales.pos_sales_analytics')->with($data);
}
    //


public function monthlyEarningReport(Request $request)
{
  $endmonth =12;
  $year = $request->year == "" ?  date('Y')  : $request->year  ;
  $bin = $request->session()->get('_bin_');
  $report_data = array( );

  for($i=1; $i <= $endmonth  ; $i++){


    $income = DB::table("ba_service_booking")
    ->whereRaw("year(service_date)= '$year'")
    ->where("book_status", "delivered")
    ->where("bin", $bin)
    ->groupBy("currentMonth")
    ->select( DB::Raw("month(service_date) as currentMonth"),  DB::Raw("sum(seller_payable) as totalSale"), DB::Raw("sum(delivery_charge) as deliveryCommission")  )
    ->get();

    $pnd_income = DB::table("ba_pick_and_drop_order")
    ->whereRaw("year(service_date)= '$year'")
    ->where("book_status", "delivered")
    ->where("request_by", $bin)
    ->groupBy("currentMonth")
    ->select(DB::Raw("month(service_date) as currentMonth"),  DB::Raw("sum(total_amount) as totalSale"), DB::Raw("sum(service_fee) as deliveryCommission")  )
    ->get();



    $report_data[$i] = array(  'month' =>  date("F", mktime(0, 0, 0, $i, 10)), 'year' => $year ,
      'normalSale' => 0,  'normalOrderCommission' => 0 ,
      'assistSale' => 0,  'assistOrderCommission' => 0,
      'pndSale' => 0,  'pndOrderCommission' => 0);

    foreach($income as $item)
    {
     if($item->currentMonth == $i) {
       $report_data[$i]["normalSale"] += $item->totalSale;
       $report_data[$i]["normalOrderCommission"] += $item->deliveryCommission;
       break;
   }

}


foreach($pnd_income as $item)
{
 if($item->currentMonth == $i) {
   $report_data[$i]["pndSale"] += $item->totalSale;
   $report_data[$i]["pndOrderCommission"] += $item->deliveryCommission;
   break;
}

}

}

$theme = DB::table("ba_theme")
->first();

$data = array(   'year' =>  $year, 'report_data' => $report_data);
return view("billing.income_report")->with('data', $data)->with('theme',$theme);
}


protected function createCoupon(Request $request)
{
  $bin = $request->session()->get('_bin_');

  $business = Business::find($bin);

  $productCategory= DB::table("ba_product_category")
  ->where("business_category",$business->category)
  ->get();


  $theme = DB::table("ba_theme")
  ->first();

  $data = array('bin' => $bin,'category'=>$productCategory,"theme"=>$theme);

  return view("products.create_coupon")->with($data);
}


protected function getProductByCategory(Request $request)
{

  $category = $request->code;

  $bin = $request->session()->get('_bin_');

  if ($category=="0") {


      return response()->json(['success'=>true]);

  }else {

    $products= DB::table("ba_products")
    ->join("ba_product_variant","ba_products.id","=","ba_product_variant.prid")
    ->where("category",$category)
    ->where("bin",$bin)
    ->get();

    $data = array('products' => $products);

    $returnHTML = view('products.product_by_category_list')->with($data)->render();

    return response()->json(['success'=>true,'html'=>$returnHTML]);

}




}



protected function productSelectList(Request $request)
{

  $prodid = $request->code;

  $bin = $request->session()->get('_bin_');

  $products= DB::table("ba_products")
  ->where("id",$prodid)
  ->where("bin",$bin)
  ->get();

  $data = array('products' => $products);

  $returnHTML = view('products.selected_product')->with($data)->render();

  return response()->json(['success'=>true,'html'=>$returnHTML]);


}

protected function saveCoupon(Request $request)
{



    if ($request->couponcode=="" && $request->prodCat=="") {


        return redirect("products/create-coupon");

    }


    $bin = $request->session()->get('_bin_');

        //select all_category
    if ($request->category=="0") {


        $business = Business::find($bin);

        $productCategory= DB::table("ba_product_category")
        ->where("business_category",$business->category)
        ->get();


        foreach ($productCategory as $items) {

          $coupon = new CouponRequestModel;

          $coupon->code = $request->couponcode;
          $coupon->bin = $bin;
          $coupon->biz_category = $items->category_name;
          $coupon->description = $request->description;
          $coupon->valid_from  =  date('Y-m-d');
          $coupon->coupon_type = "coupon";


          $save = $coupon->save();

      }

      if ($save) {

        return redirect("products/create-coupon");

    }



}
        //select all_category ends here
else{



  $cat =  $request->prodCat;
  $prod = $request->prodCode;

  $merged = collect($cat)->zip($prod)->transform(function ($values) {
    return [
        'category' => $values[0],
        'prod' => $values[1],
    ];
});

  $products = $merged->all();






  foreach ($products as $key => $item) {

      $coupon = new CouponRequestModel;

      $coupon->code = $request->couponcode;
      $coupon->bin = $bin;
      $coupon->pr_code = $item["prod"];
      $coupon->biz_category = $item["category"];
      $coupon->description = $request->description;
      $coupon->valid_from  =  date('Y-m-d');
      $coupon->coupon_type = "coupon";

      $save = $coupon->save();

  }




}


if ($save) {

  return redirect("products/create-coupon")->with("err_msg","Coupon save successfully!");

}


}

protected function showPaymentDue(Request $request)
{

    $bin = $request->session()->get('_bin_');
    $cycles =  array(1);
    if( isset($request->cycle))
    {
        $cycles =  $request->cycle;
    }


    if($request->year != "")
    {
        $year  = $request->year ;
    }
    else
    {
        $year  = date('Y' );
    }

    if($request->month !="" )
    {
      $month = $request->month ;
  }
  else
  {
    $month = date('m' );
}

$totalDays = cal_days_in_month(CAL_GREGORIAN, $month , $year );

$allowed_cycles = true;
if(count($cycles) > 0)
{
            //check if adjoining cycles are selected

    for($i=0; $i<count($cycles)-1; $i++)
    {
        for($j= $i+1; $j <count($cycles) ; $j++)
        {
            if( $cycles[$i] + 1 != $cycles[$j]  )
            {
                $allowed_cycles = false;
            }
            break;

        }
    }

    if( count($cycles)  == 1 )
    {
        if($cycles[0]  == 1 )
        {
            $startDay   =  1 ;
            $endDay   =    7;
            $cycle = 1;
        }
        else if($cycles[0]  == 2 )
        {
            $startDay   = 8 ;
            $endDay   =    14;
            $cycle = 2;
        }else if($cycles[0]  == 3 )
        {
            $startDay   = 15 ;
            $endDay   =    21;
            $cycle = 3;
        }else if($cycles[0]  == 4 )
        {
            $startDay   = 22 ;
            $endDay   =    28;
            $cycle = 4;
        }
        else
        {
            if( $totalDays > 28)
            {
                $startDay   = 29;
                $endDay   =  $totalDays;
            }
        }

    }
            else  //count($cycles)  > 1
            {
                $size = count($cycles)-1;

                if($cycles[0]  == 1 )
                {
                    $startDay   =  1 ;
                }
                else if($cycles[0]  == 2 )
                {
                    $startDay   = 8 ;
                }else if($cycles[0]  == 3 )
                {
                    $startDay   = 15 ;
                }else if($cycles[0]  == 4 )
                {
                    $startDay   = 22 ;
                }
                else
                {
                    if( $totalDays > 28)
                    {
                        $startDay   = 29;
                    }
                    else
                    {
                        $startDay   = 0;
                    }
                }


                if($cycles[$size]  == 1 )
                {
                    $endDay   =    7;
                }
                else if($cycles[$size]  == 2 )
                {
                    $endDay   =    14;
                }else if($cycles[$size]  == 3 )
                {
                    $endDay   =    21;
                }else if($cycles[$size]  == 4 )
                {
                    $endDay   =    28;
                }
                else
                {
                    if( $totalDays > 28)
                    {
                        $endDay   =  $totalDays;
                    }
                    else
                    {
                        $endDay   = 0;
                    }
                }

            }

        }

        if(  !$allowed_cycles  )
        {
            return redirect("/admin/billing-and-clearance/sales-and-clearance-report?bin=" . $bin )
            ->with("err_msg", "Billing cycles selected are not adjacent. Report generation blocked.");
        }

        $business  =  DB::table("ba_business")
        ->join("ba_business_commission", "ba_business_commission.bin", "=", "ba_business.id")
        ->where("ba_business_commission.month", $month )
        ->where("ba_business_commission.year", $year )
        ->where("ba_business.id", $bin )
        ->select("ba_business.id", "ba_business.name",    "ba_business_commission.commission")
        ->first();



        if( !isset($business)  )
        {
            return redirect("/reports/billing-and-clearance/payment-due-merchants"  )
            ->with("err_msg", "Business not selected. Report generation blocked.");
        }


        // return redirect("/admin/billing-and-clearance/sales-and-clearance-report?bin=" . $bin )
        // ->with("err_msg", "Selected month has only 4 weeks of payment.");

        $first = DB::table('ba_service_booking')
        ->where("ba_service_booking.bin", $bin )
        ->whereDay("service_date", ">=",  $startDay )
        ->whereDay("service_date", "<=",  $endDay )
        ->whereMonth("service_date", $month )
        ->whereYear("service_date", $year )
        ->where("clerance_status", "un-paid" )
        ->orderBy("ba_service_booking.service_date")
        ->select('ba_service_booking.id',
          'ba_service_booking.service_date',
          DB::Raw("'customer' source"),
          'total_cost as orderCost',
          DB::Raw("'0.00' packingCost"),
          'delivery_charge as deliveryCharge',
          'discount',  'payment_type  as paymentType',
          'book_status as bookingStatus', DB::Raw("'booktou' paymentTarget"),
          DB::Raw("'normal' orderType")) ;

        $result = DB::table('ba_pick_and_drop_order')
        ->where("request_from", "business")
        ->where("request_by", $bin )
        ->whereDay("service_date", ">=",  $startDay )
        ->whereDay("service_date", "<=",  $endDay )
        ->whereMonth("service_date", $month )
        ->whereYear("service_date", $year )
        ->where("clerance_status", "un-paid" )
        ->orderBy("ba_pick_and_drop_order.service_date")
        ->union($first)
        ->select('ba_pick_and_drop_order.id',
            'ba_pick_and_drop_order.service_date',
            "source",
            'total_amount as orderCost',
            'packaging_cost as packingCost',
            'service_fee as deliveryCharge',
            DB::Raw("'0' discount") ,  'pay_mode as paymentType',
            'book_status  as bookingStatus',
            "payment_target as paymentTarget",
            DB::Raw("'pnd' orderType"))
        ->get();

        $oids = array();
        foreach($result as $item)
        {
            if($item->orderType == "normal")
            {
                $oids[] = $item->id ;

            }
        }
        $oids[]= 0;

        $normal_sales_cart = DB::table("ba_shopping_basket")
        ->whereIn("order_no",  $oids  )
        ->get();


        $all_business  = DB::table("ba_business")
        ->where("id",$bin)
        ->get();

        $theme = DB::table("ba_theme")
        ->first();

        $data =  array(  'business' => $business,
            'businesses' => $all_business,
            'results' =>  $result,
            'cart_items' =>$normal_sales_cart,
            'bin' => $bin,
            'cycles' => $cycles,
            'start' =>  $startDay ,
            'end' =>  $endDay ,
            'month' =>  $month ,
            'year' => $year,
            'theme'=>$theme  );

        return view("billing.payment_due")->with( $data );

    }



    protected function saveSalesClearance(Request $request)
    {

        $bin = $request->session()->get('_bin_');


        if($request->amount == "" ||$request->paymode == "" || $request->clearedon == ""  )
        {
          return redirect('/')->with("err_msg", "No data provided for clearance.");
      }
      $orders = $request->paymentClearance;


      if(count($orders) == 0)
      {
          return redirect('/')->with("err_msg", "No data provided for clearance.");
      }


      $payment = new PaymentDealingModel;
      $payment->bin = $bin  ;
      $payment->amount =  $request->amount ;
      $payment->cleared_on = date('Y-m-d', strtotime($request->clearedon))  ;
      $payment->transact_no =  ( $request->refno != "" ) ? $request->refno  : "not provided"  ;
      $payment->payment_mode = $request->paymode;
      $payment->remarks =   ( $request->remarks != "" ) ? $request->remarks  : "not provided"  ;
      $payment->order_nos =  implode(",",$orders) ;
      $payment->cycle =  $request->cycle;
      $payment->save();

      DB::table("ba_pick_and_drop_order")
      ->whereIn('id', $orders )
      ->update( ['clerance_status' => 'paid'] ) ;

      DB::table("ba_service_booking")
      ->whereIn('id', $orders )
      ->update( ['clerance_status' => 'paid'] );


      return redirect("/clearance/show-payment-due")->with('err_msg', "Payment clerance information saved." );

  }



  protected function updateTheme(Request $request)
  {

      if ($request->name=="") {
       return redirect("/");
   }

   $theme =  DB::table("ba_theme")
   ->where('code', 0)
   ->first();

   if ($theme) {

       $getTheme = Theme::find($theme->id);

   }else{

    $getTheme = new Theme;

}

$getTheme->code = 0;
$getTheme->theme_name = $request->name;

$save = $getTheme->save();

if ($save) {
 return response()->json(['success'=>true]);
}


}

public function MerchantDailyOrdersExport(Request $request)
{

    $todayDate  = date('Y-m-d');
    $bin = $request->bin;

    $totalEarning = 0;

    $first = DB::table('ba_service_booking')
    ->join('ba_business', 'ba_service_booking.bin', '=', 'ba_business.id')
    ->wheredate('ba_service_booking.service_date', $todayDate)
    ->whereIn("book_status", array('completed', 'delivered') )
    ->where("ba_business.frno", "0")
    ->where("ba_service_booking.bin",$bin)
    ->select('ba_service_booking.id',
      DB::Raw("'customer' source"),
      "ba_business.id as bin" ,
      'total_cost as orderCost',
      DB::Raw("'0.00' packingCost"),
      'delivery_charge as deliveryCharge',
      'discount',  'payment_type  as paymentType',
      'book_status as bookingStatus',
      'ba_business.name', "ba_business.category",
      "ba_business.commission",
      DB::Raw("'booktou' paymentTarget"),
      DB::Raw("'normal' orderType")) ;

    $result = DB::table('ba_pick_and_drop_order')
    ->join('ba_business', 'ba_pick_and_drop_order.request_by', '=', 'ba_business.id')
    ->wheredate('ba_pick_and_drop_order.service_date', $todayDate)
    ->whereIn("book_status", array('completed', 'delivered') )
    ->where("request_from", "business")
    ->where("ba_business.frno", "0")
    ->where("ba_pick_and_drop_order.request_by",$bin)
    ->union($first)
    ->select('ba_pick_and_drop_order.id',
      "source","ba_business.id as bin",
      'total_amount as orderCost',
      'packaging_cost as packingCost',
      'service_fee as deliveryCharge',
      DB::Raw("'0' discount") ,  'pay_mode as paymentType',
      'book_status  as bookingStatus',
      'ba_business.name', "ba_business.category",
      "ba_business.commission",
      "payment_target as paymentTarget",
      DB::Raw("'pnd' orderType"))
    ->orderBy("id", "asc")
    ->get();


    $oids =  $bins = array();
    foreach($result as $item)
    {
        if($item->orderType == "normal")
        {
            $oids[] = $item->id ;
        }
    }


    $normal_sales_cart = DB::table("ba_shopping_basket")
    ->whereIn("order_no",  $oids  )
    ->get();


    $all_business  = DB::table("ba_business")
    ->where("id", $bin )

    ->first();



    $data = array(  'sales' =>  $result ,
        'cart_items' =>$normal_sales_cart,
        'date' =>  $todayDate,
        'business' => $all_business );


    $pdf_file =  "booktou_salesbill_" .time(). '_' . $todayDate . ".pdf";
    $save_path = public_path() . "/assets/ccportal/bills/"  .  $pdf_file    ;

    $pdf = PDF::setOptions(['defaultFont' => 'sans-serif'  ])
    ->setPaper('a4', 'portrait')
    ->loadView('exports.merchant_daily_sales_report' ,  $data  )   ;
    return $pdf->download( $pdf_file  );

}


public function editImageFix(Request $request)
{
    $page = ( $request->page == "" ? 0: $request->page );
    $size=5000;

    $cdn_url =   config('app.app_cdn')    ;
    $cdn_path =  config('app.app_cdn_path')  ;

    $product_images  = DB::table("ba_product_photos")
    ->whereRaw("ba_product_photos.image_url is  null" )
    ->whereRaw("ba_product_photos.image_path <> '' " )
    ->offset( ( $page - 1 ) * $size )
    ->limit($size)
    ->get();

    $counter=0;
    foreach($product_images as $item)
    {

        $all_photos = array();
        $all_thumbnails  = array();
        $files =  explode(",",  $item->image_path);

        if(count($files) > 0 )
        {
           $product_folder_path =  $cdn_path .  "/assets/image/store/bin_" .   $item->bin .  "/";
           $product_folder_url = $cdn_url . "/assets/image/store/bin_" .   $item->bin .  "/";

           $files=array_filter($files);


           $pos = 0;
           foreach($files as $file )
           {
             $source = $product_folder_path .  $file;
             if(file_exists( $source  ))
             {
               $pathinfo = pathinfo( $source  );
               $target_file_name  =  $pathinfo['filename'].   '.' . $pathinfo['extension'];
               $target_file_path   = $cdn_path .  "/assets/image/store/bin_" . $item->bin  .  "/"  . $target_file_name ;

             if( file_exists( $target_file_path) ) //smaller file exists
             {
                 $all_photos[]  =  $product_folder_url . $target_file_path;
                 $all_thumbnails[] =  $product_folder_url . $target_file_name;
             }
             else
             {
                 $all_photos[]  =  $product_folder_url .   $file;
                 $all_thumbnails[] = $product_folder_url .   $file;
             }

             if($pos == 0)
             {
                DB::table("ba_product_photos")
                ->where("id" , $item->id  )
                ->update(
                    array('image_url' =>  $product_folder_url .  $target_file_name,
                        'image_path' =>  "/assets/image/store/bin_" . $item->bin  .  "/"  . $target_file_name
                    )
                );
            }
            else
            {
                $product_photos = new ProductPhotosModel ;
                $product_photos->prid = $item->prid;
                $product_photos->prsubid = $item->prsubid;
                $product_photos->image_url  =  $product_folder_url .  $target_file_name;
                $product_photos->image_path = "/assets/image/store/bin_" . $item->bin  .  "/"  . $target_file_name;
                $product_photos->save();
            }
        }

        $pos++;
    }
}

$counter++;

}

echo $counter . " records processed!";

}

protected function posMerchantOrders(Request $request)
{
    $bin = $request->session()->get('_bin_');

    if ($bin=="") {
        return redirect('reports/pos-merchant-orders');
    }
    if ($request->year=="") {
        $year =  date('Y');     
    }else{
        $year =  $request->year;
    }

    if ($request->year=="") {
        $month = date('m');    
    }else{
        $month =  $request->month;
    }

    
    
    $status = [ 'completed', 'delivered'];

        //monthly pos order details
    $result = DB::table('ba_pos_order')
    ->where('bin',$bin)
    ->whereYear('service_date',$year)
    ->whereMonth('service_date',$month)
    ->whereIn('book_status',$status)
    ->get(); 
    
        // $orderno = array();
        // foreach ($booking_list as $booking) {

        //     $orderno[]=$booking->id;
        // }


        // $result = DB::table('ba_pos_order_basket')
        // ->select( "ba_pos_order_basket.id","ba_pos_order_basket.pr_name",
        //           'ba_pos_order_basket.qty  as quantity' ,
        //           'ba_pos_order_basket.unit_price as unitPrice',
        //           'ba_pos_order_basket.cgst as cGST',
        //           'ba_pos_order_basket.sgst as sGST')
        //   ->whereIn('ba_pos_order_basket.id',$orderno)
        //    ->get();
        //monthly pos order ends here
    
    $monthname = date('F', mktime(0, 0, 0, $month, 10));

    $data = array('result'=>$result,'month'=>$monthname);
    return view('orders.monthly_orders')->with($data);
} 

protected function PosMonthlyOrdersToPdf(Request $request)
{               
    $bin = $request->session()->get('_bin_');

    if ($bin=="") {
        return redirect('reports/pos-merchant-orders');
    }

    if ($request->year=="") {
        $year =  date('Y');     
    }else{
        $year =  $request->year;
    }

    if ($request->year=="") {
        $month = date('m');    
    }else{
        $month =  $request->month;
    }
    $monthname = date('F', mktime(0, 0, 0, $month, 10));

    $status = [ 'completed', 'delivered'];

              //monthly pos order details
    $result = DB::table('ba_pos_order')
    ->where('bin',$bin)
    ->whereYear('service_date',$year)
    ->whereMonth('service_date',$month)
    ->whereIn('book_status',$status)
    ->get(); 
    
    $monthname = date('F', mktime(0, 0, 0, $month, 10));

    $data = array('result'=>$result);
    

             //return view('exports.pos_monthly_sales')->with($data);

    $pdf_file =  "booktou_pos_salesbill_" .time(). '_' . $monthname . ".pdf";
    $save_path = public_path() . "/assets/ccportal/bills/"  .  $pdf_file    ;

    $pdf = PDF::setOptions(['defaultFont' => 'sans-serif'  ])
    ->setPaper('a4', 'portrait')
    ->loadView('exports.pos_monthly_sales' ,  $data  )   ;
    return $pdf->download( $pdf_file  );
}


protected function posDailyOrders(Request $request)
{
    $bin = $request->session()->get('_bin_');

    if ($bin=="") {
        return redirect('reports/pos-merchant-orders');
    }
    
    if ($request->filter_date=="") {
        $todayDate = date('Y-m-d');    
    }else{
        $todayDate =  $request->filter_date;
    } 
    
    $status = [ 'completed', 'delivered'];

        //daily pos order details
    $result = DB::table('ba_pos_order')
    ->where('bin',$bin)
    ->whereDate('service_date','>=',$todayDate)
    ->whereIn('book_status',$status)
    ->get(); 
    
    $data = array('result'=>$result,'today'=>$todayDate);
    return view('orders.pos_daily_orders')->with($data);
}







}
