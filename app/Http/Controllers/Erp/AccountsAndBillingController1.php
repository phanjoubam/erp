<?php

namespace App\Http\Controllers\Erp; 

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Session;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Support\Facades\File;
use Maatwebsite\Excel\Facades\Excel; 
use App\Libraries\FirebaseCloudMessage; 
use Jenssegers\Agent\Agent;
 



use App\PickAndDropRequestModel;
use App\Business; 
use App\BusinessCategory;
use App\ProductCategoryModel; 
use App\ProductModel;
use App\ShoppingOrderModel;
use App\ShoppingOrderItemsModel;
use App\AddressModel;
use App\MemberAddressModel;
use App\DeliveryOrderModel;
use App\ServiceBooking;
use App\OrderStatusLogModel; 
use App\CustomerProfileModel;
use App\PaymentDealingModel;
use App\PremiumBusiness;
use App\ProductImportedModel; 
use App\CloudMessageModel;
use App\ProductPromotion;
use App\GlobalSettingsModel;
use App\SliderModel; 
use App\StaffAttendanceModel;
use App\CouponModel;
use App\OrderSequencerModel;
use App\Imports\ProductImport; 
use App\BusinessMenu;
use App\AccountsLog;
use App\CmsBannerSlidersModel;
use App\CmsProductsListedModel;
use App\CmsProductGroupsModel;
use App\OrderRemarksModel;
use App\AgentCacheModel;
use App\StaffSalaryModel;
use App\DailyExpenseModel;
use App\ComplaintModel;

use App\Imports\ProductsImport;
use App\Exports\MonthlyTaxesExport;
use App\Exports\AdminDashboardExport;
use App\AccountsDailyLogModel;

use App\OrderVoucherModel; 
use App\OrderJournalEntryModel;




use App\Traits\Utilities; 
use PDF;

class AccountsAndBillingController1  extends Controller
{
    use Utilities;


    protected function dailyAccountLedgerEntry(Request $request)
    {

    	if($request->btnsearch != "")
    	{
    		$date = date('Y-m-d', strtotime($request->datefilter));
    	}
    	else 
    	{
    		$date = date('Y-m-d');
    	}
    	

    	$ledgers = DB::table("ba_accounts_daily_log")
    	->whereDate("account_date", $date  )
    	->get();
 

    	if(isset($request->btnsave))
    	{
    		$dailylog = AccountsDailyLogModel::where("account_name", $request->type)
    		->whereDate("account_date", date('Y-m-d', strtotime( $request->date) ))
    		->first();

    		if( !isset($dailylog))
    		{
    			$dailylog = new AccountsDailyLogModel;
    		} 

    		$dailylog->account_name = $request->type;
    		$dailylog->amount = $request->amount;
    		$dailylog->details = $request->details;
    		$dailylog->entered_by = Session::get('__user_id_') ;
    		$dailylog->account_date = date('Y-m-d', strtotime( $request->date) );
    		$dailylog->entry_date = date('Y-m-d');
    		$dailylog->save();
 
    		return redirect('/admin/accounts/account-ledger-entry')->with("err_msg", "Ledger entry saved!");
    	}


    	$data = array( 
        'title'=>'Account ledger entry' ,  
        'date' => $date,
        'ledgers' =>$ledgers );

         return view("admin.accounts.add_daily_closing")->with( $data);

    }
    


    protected function weeklyPaymentDueMerchants(Request $request )
    {

        if( $request->month  == "")
        { 
            $month   =  date('m');
        }
        else 
        {
          $month   =  $request->month ; 
        }  

        if( $request->year   == "")
        { 
            $year = date('Y');
        }
        else 
        {
          $year   =  $request->year; 
        }


         $cycle = 1;
        if($request->cycle == "" ||  $request->cycle  == 1 )
        {
            $startDay   =  1 ;  
            $endDay   =    7;
            $cycle = 1;
        }
        else if(  $request->cycle  == 2 )
        {
            $startDay   = 8 ;  
            $endDay   =    14;
            $cycle = 2;
        }else if(  $request->cycle  == 3)
        {
            $startDay   = 15 ;  
            $endDay   =    21;
            $cycle = 2;
        }else if(  $request->cycle  == 4)
        {
            $startDay   = 22 ;  
            $endDay   =    28;
            $cycle = 4;
        }
        else  
        {
            $totalDays = cal_days_in_month(CAL_GREGORIAN, $month , $year );

            if( $totalDays > 28)
            {
                $startDay   = 29;  
                $endDay   =    $totalDays;   
                $cycle = 5; 
            }
            else 
            {
                return redirect("/admin/billing-and-clearance/payment-due-merchants" )->with("err_msg", "Selected month has only 4 weeks of payment."); 
            } 
        }
 

        $pnd_businesses = DB::table('ba_business')  
        ->whereIn("id", function($query) use (   $month, $year ) { 

          $query->select('request_by as bin' )
          ->from('ba_pick_and_drop_order') 
          ->where("request_from", "business") 
          ->whereMonth("service_date", $month )
          ->whereYear("service_date", $year )
          ->whereIn("book_status",  array ( 'completed', 'delivered') ); 

        })
        ->where("frno", 0 ) 
        ->select('ba_business.id', "ba_business.name", "ba_business.category" ) 
        ->orderBy("ba_business.name",'asc');  

        

        $all_businesses = DB::table('ba_business')  
        ->whereIn("id", function($query) use (    $month, $year ) { 

          $query->select('bin' )
          ->from('ba_service_booking')  
          ->whereMonth("service_date", $month )
          ->whereYear("service_date", $year )
          ->whereIn("book_status",  array ( 'completed', 'delivered') ); 

        })
        ->where("frno", 0 ) 
        ->select('ba_business.id', "ba_business.name", "ba_business.category"  ) 
        ->orderBy("ba_business.name",'asc')
        ->union($pnd_businesses)
        ->get();  

  
        $pnd_sales = DB::table("ba_pick_and_drop_order")
        ->where("request_from", "business") 
          ->whereMonth("service_date", $month )
          ->whereYear("service_date", $year )
          ->whereIn("book_status",  array ( 'completed', 'delivered') )
          ->select( "request_by", DB::Raw("sum(total_amount) as totalAmount") )
          ->groupBy("request_by")
          ->get(); 

        $normal_sales = DB::table("ba_service_booking") 
          ->whereMonth("service_date", $month )
          ->whereYear("service_date", $year )
          ->whereIn("book_status",  array ( 'completed', 'delivered') )
          ->select( "bin", DB::Raw("sum(total_cost) as totalAmount") )
          ->groupBy("bin")
          ->get();   


        $data = array(  'businesses' => $all_businesses , 'pnd_sales' =>$pnd_sales,  'normal_sales' => $normal_sales, 'endDay' => $endDay , 'month' =>  $month ,  'year' => $year ); 
        return view("admin.billing.payment_due_report")->with(  $data);


    }

 

    protected function viewPaymentDueMerchants(Request $request )
    {

        if( $request->month  == "")
        { 
            $month   =  date('m');
        }
        else 
        {
          $month   =  $request->month ; 
        }  

        if( $request->year   == "")
        { 
            $year = date('Y');
        }
        else 
        {
          $year   =  $request->year; 
        }


         $cycle = 1;
        if($request->cycle == "" ||  $request->cycle  == 1 )
        {
            $startDay   =  1 ;  
            $endDay   =    7;
            $cycle = 1;
        }
        else if(  $request->cycle  == 2 )
        {
            $startDay   = 8 ;  
            $endDay   =    14;
            $cycle = 2;
        }else if(  $request->cycle  == 3)
        {
            $startDay   = 15 ;  
            $endDay   =    21;
            $cycle = 2;
        }else if(  $request->cycle  == 4)
        {
            $startDay   = 22 ;  
            $endDay   =    28;
            $cycle = 4;
        }
        else  
        {
            $totalDays = cal_days_in_month(CAL_GREGORIAN, $month , $year );

            if( $totalDays > 28)
            {
                $startDay   = 29;  
                $endDay   =    $totalDays;   
                $cycle = 5; 
            }
            else 
            {
                return redirect("/admin/billing-and-clearance/payment-due-merchants" )->with("err_msg", "Selected month has only 4 weeks of payment."); 
            } 
        }
 

        $pnd_businesses = DB::table('ba_business')  
        ->whereIn("id", function($query) use (   $month, $year ) { 

          $query->select('request_by as bin' )
          ->from('ba_pick_and_drop_order') 
          ->where("request_from", "business") 
          ->whereMonth("service_date", $month )
          ->whereYear("service_date", $year )
          ->whereIn("book_status",  array ( 'completed', 'delivered') ); 

        })
        ->where("frno", 0 ) 
        ->select('ba_business.id', "ba_business.name", "ba_business.category" , "ba_business.phone_pri") 
        ->orderBy("ba_business.name",'asc');  

        

        $all_businesses = DB::table('ba_business')  
        ->whereIn("id", function($query) use (    $month, $year ) { 

          $query->select('bin' )
          ->from('ba_service_booking')  
          ->whereMonth("service_date", $month )
          ->whereYear("service_date", $year )
          ->whereIn("book_status",  array ( 'completed', 'delivered') ); 

        })
        ->where("frno", 0 ) 
        ->select('ba_business.id', "ba_business.name", "ba_business.category", "ba_business.phone_pri"  ) 
        ->orderBy("ba_business.name",'asc')
        ->union($pnd_businesses)
        ->get();  

  
        $pnd_sales = DB::table("ba_pick_and_drop_order")
        ->where("request_from", "business") 
          ->whereMonth("service_date", $month )
          ->whereYear("service_date", $year )
          ->whereIn("book_status",  array ( 'completed', 'delivered') )
          ->select( "request_by", DB::Raw("sum(total_amount) as totalAmount") )
          ->groupBy("request_by")
          ->get(); 

        $normal_sales = DB::table("ba_service_booking") 
          ->whereMonth("service_date", $month )
          ->whereYear("service_date", $year )
          ->whereIn("book_status",  array ( 'completed', 'delivered') )
          ->select( "bin", DB::Raw("sum(total_cost) as totalAmount") )
          ->groupBy("bin")
          ->get();   


        $data = array(  'businesses' => $all_businesses ,
         'pnd_sales' =>$pnd_sales,  'normal_sales' => $normal_sales, 'endDay' => $endDay , 'month' =>  $month ,  'year' => $year ); 
        return view("admin.billing.payment_due_report")->with(  $data);


    }


    protected function monthlyServiceReport(Request $request )
    {

        if( $request->month  == "")
        { 
            $month   =  date('m');
        }
        else 
        {
          $month   =  $request->month ; 
        }  

        if( $request->year   == "")
        { 
            $year = date('Y');
        }
        else 
        {
          $year   =  $request->year; 
        }
  

        $pnd_businesses = DB::table('ba_business')  
        ->whereIn("id", function($query) use (   $month, $year ) { 

          $query->select('request_by as bin' )
          ->from('ba_pick_and_drop_order') 
          ->where("request_from", "business") 
          ->whereMonth("service_date", $month )
          ->whereYear("service_date", $year )
          ->whereIn("book_status",  array ( 'completed', 'delivered') ); 

        })
        ->where("frno", 0 ) 
        ->select('ba_business.id', "ba_business.name", "ba_business.category" ) 
        ->orderBy("ba_business.name",'asc');  

        

        $all_businesses = DB::table('ba_business')  
        ->whereIn("id", function($query) use (    $month, $year ) { 

          $query->select('bin' )
          ->from('ba_service_booking')  
          ->whereMonth("service_date", $month )
          ->whereYear("service_date", $year )
          ->whereIn("book_status",  array ( 'completed', 'delivered') ); 

        })
        ->where("frno", 0 ) 
        ->select('ba_business.id', "ba_business.name", "ba_business.category"  ) 
        ->orderBy("ba_business.name",'asc')
        ->union($pnd_businesses)
        ->get();  

  
        $pnd_sales = DB::table("ba_pick_and_drop_order")
        ->where("request_from", "business") 
          ->whereMonth("service_date", $month )
          ->whereYear("service_date", $year )
          ->whereIn("book_status",  array ( 'completed', 'delivered') )
          ->select( "request_by", DB::Raw("sum(total_amount) as totalAmount") )
          ->groupBy("request_by")
          ->get(); 

        $normal_sales = DB::table("ba_service_booking") 
          ->whereMonth("service_date", $month )
          ->whereYear("service_date", $year )
          ->whereIn("book_status",  array ( 'completed', 'delivered') )
          ->select( "bin", DB::Raw("sum(total_cost) as totalAmount") )
          ->groupBy("bin")
          ->get();   


        $data = array(  'businesses' => $all_businesses , 
            'pnd_sales' =>$pnd_sales, 
         'normal_sales' => $normal_sales, 
         'month' =>  $month ,  'year' => $year ); 
        return view("admin.billing.monthly_service_report")->with(  $data);


    }


    // View Business Normal and PnD Sales Report for monthly
     protected function salesAndClearanceReport(Request $request )
     {

        $bin = $request->bin; 
        if($request->year != "")
        {
            $year  = $request->year ; 
        }
        else
        {
            $year  = date('Y' );
        }
 

        if($request->month !="" )
        {
          $month = $request->month ;
        }
        else
        {
            $month = date('m' );
        }

        $cycle = 1;
        if($request->cycle == "" ||  $request->cycle  == 1 )
        {
            $startDay   =  1 ;  
            $endDay   =    7;
            $cycle = 1;
        }
        else if(  $request->cycle  == 2 )
        {
            $startDay   = 8 ;  
            $endDay   =    14;
            $cycle = 2;
        }else if(  $request->cycle  == 3)
        {
            $startDay   = 15 ;  
            $endDay   =    21;
            $cycle = 3;
        }else if(  $request->cycle  == 4)
        {
            $startDay   = 22 ;  
            $endDay   =    28;
            $cycle = 4;
        }
        else  
        {
            $totalDays = cal_days_in_month(CAL_GREGORIAN, $month , $year );

            if( $totalDays > 28)
            {
                $startDay   = 29;  
                $endDay   =    $totalDays;   
                $cycle = 5; 
            }
            else 
            {
                return redirect("/admin/billing-and-clearance/sales-and-clearance-report?bin=" . $bin )
                ->with("err_msg", "Selected month has only 4 weeks of payment."); 
            } 
        }
 

        $first = DB::table('ba_service_booking')  
        ->where("ba_service_booking.bin", $bin )
        ->whereDay("service_date", ">=",  $startDay )
        ->whereDay("service_date", "<=",  $endDay )
        ->whereMonth("service_date", $month )
        ->whereYear("service_date", $year )
        ->orderBy("ba_service_booking.service_date")
        ->select('ba_service_booking.id', 
          'ba_service_booking.service_date',
          DB::Raw("'customer' source"), 
          'total_cost as orderCost', 
          DB::Raw("'0.00' packingCost"), 
          'delivery_charge as deliveryCharge', 
          'discount',  'payment_type  as paymentType',
          'book_status as bookingStatus', DB::Raw("'booktou' paymentTarget"),
          DB::Raw("'normal' orderType")) ;
 
        $result = DB::table('ba_pick_and_drop_order')  
        ->where("request_from", "business")
        ->where("request_by", $bin )
        ->whereDay("service_date", ">=",  $startDay )
          ->whereDay("service_date", "<=",  $endDay )
          ->whereMonth("service_date", $month )
          ->whereYear("service_date", $year )
          ->orderBy("ba_pick_and_drop_order.service_date")
        ->union($first)  
        ->select('ba_pick_and_drop_order.id', 
            'ba_pick_and_drop_order.service_date',
          "source",
          'total_amount as orderCost',
          'packaging_cost as packingCost',  
          'service_fee as deliveryCharge', 
          DB::Raw("'0' discount") ,  'pay_mode as paymentType', 
          'book_status  as bookingStatus',             
          "payment_target as paymentTarget",
          DB::Raw("'pnd' orderType"))  
        ->get();  

        $oids = array();
        foreach($result as $item)
        {
            if($item->orderType == "normal")
            {
                $oids[] = $item->id ; 

            } 
        }
        $oids[]= 0;

        $normal_sales_cart = DB::table("ba_shopping_basket") 
        ->whereIn("order_no",  $oids  )  
        ->get();  

       $business  = Business::find(  $bin)  ;


       $all_business  = DB::table("ba_business")   
      ->get(); 

       $data =  array(  'business' => $business, 
        'businesses' => $all_business,
        'results' =>  $result,  
        'cart_items' =>$normal_sales_cart,
        'bin' => $bin,  
        'cycle' => $cycle,
        'month' =>  $month , 
        'year' => $year  );
       return view("admin.billing.sales_report_per_cycle_for_merchant")->with( $data ); 
     }

 

    protected function dailySalesAndServiceReport(Request $request )
    {

        if($request->reportDate == "")
        {
               $date   =  date('Y-m-d');  
        }
        else
        {
            $date   =  date('Y-m-d', strtotime($request->reportDate)); 
        }
         

        $pnd_businesses = DB::table('ba_business')  
        ->whereIn("id", function($query) use (  $date   ) { 

          $query->select('request_by as bin' )
          ->from('ba_pick_and_drop_order') 
          ->where("request_from", "business")
          ->whereDate("service_date", "=",   $date  )
          ->whereIn("book_status",  array ( 'completed', 'delivered') ); 

        })
        ->where("frno", 0 ) 
        ->select('ba_business.id', "ba_business.name", "ba_business.category" ) 
        ->orderBy("ba_business.name",'asc');  

        

        $all_businesses = DB::table('ba_business')  
        ->whereIn("id", function($query) use ( $date  ) { 

          $query->select('bin' )
          ->from('ba_service_booking') 
          ->whereDate("service_date", "=",   $date  )
          ->whereIn("book_status",  array ( 'completed', 'delivered') ); 

        })
        ->where("frno", 0 ) 
        ->select('ba_business.id', "ba_business.name", "ba_business.category"  ) 
        ->orderBy("ba_business.name",'asc')
        ->union($pnd_businesses)
        ->get();  

  
        $pnd_sales = DB::table("ba_pick_and_drop_order")
        ->where("request_from", "business")
        ->whereDate("service_date", "=",   $date  )
        ->whereIn("book_status",  array ( 'completed', 'delivered') )
        ->select( "ba_pick_and_drop_order.*" ) 
        ->get(); 
    

         $normal_sales = DB::table('ba_service_booking')   
        ->whereDate("service_date",   $date  )
        ->whereIn("book_status",  array ( 'completed', 'delivered') )
        ->get();  

        $data = array(  'businesses' => $all_businesses , 'pnd_sales' =>$pnd_sales,  
            'normal_sales' => $normal_sales,  
            'reportDate' => $date ); 
        return view("admin.billing.daily_sales_and_service")->with(  $data);


    }




    // View Business Normal and PnD Sales Report for monthly
     protected function dailySalesAndServiceMerchantReport(Request $request )
     {

        $bin = $request->bin; 
        
        if($request->reportDate == "")
        {
            $date   =  date('Y-m-d');  
        }
        else
        {
            $date   =  date('Y-m-d', strtotime($request->reportDate)); 
        }
 


        if( $bin == 0 || $bin == "")
        {
            $bin == 0;
            $normals = DB::table('ba_service_booking')   
            ->whereDate("service_date", "=",   $date  )
            ->orderBy("ba_service_booking.id") 
            ->select('ba_service_booking.id', 
                "bin as requestBy",
                'ba_service_booking.service_date',
              DB::Raw("'customer' source"), 
              'total_cost as orderCost', 
              'delivery_charge as deliveryCharge', 
              'discount',  'payment_type  as paymentType',
              'book_status as bookingStatus', DB::Raw("'booktou' paymentTarget"),
              DB::Raw("'normal' orderType")) ; 

            $assists = DB::table('ba_pick_and_drop_order')  
            ->where("request_from", "customer") 
            ->whereDate("service_date", "=",   $date  )
            ->orderBy("ba_pick_and_drop_order.id")  
            ->select('ba_pick_and_drop_order.id', 
                "request_by as requestBy",
                'ba_pick_and_drop_order.service_date',
              "source",
              'total_amount as orderCost', 
              'service_fee as deliveryCharge', 
              DB::Raw("'0' discount") ,  'pay_mode as paymentType', 
              'book_status  as bookingStatus',             
              "payment_target as paymentTarget",
              DB::Raw("'assist' orderType"))   ;


            $result = DB::table('ba_pick_and_drop_order')  
            ->where("request_from", "business") 
            ->whereDate("service_date", "=",   $date  )
            ->orderBy("ba_pick_and_drop_order.id") 
            ->union($normals)  
            ->union($assists)  
            ->select('ba_pick_and_drop_order.id', 
            "request_by as requestBy",
            'ba_pick_and_drop_order.service_date',
            "source",
            'total_amount as orderCost', 
            'service_fee as deliveryCharge', 
            DB::Raw("'0' discount") ,  'pay_mode as paymentType', 
            'book_status  as bookingStatus',             
            "payment_target as paymentTarget",
              DB::Raw("'pnd' orderType"))  
            ->get(); 
        }
        else 
        {
            $first = DB::table('ba_service_booking')  
            ->where("ba_service_booking.bin", $bin )
            ->whereDate("service_date", "=",   $date  )
            ->orderBy("ba_service_booking.id") 
            ->select('ba_service_booking.id', 
                "bin as requestBy",
                'ba_service_booking.service_date',
              DB::Raw("'customer' source"), 
              'total_cost as orderCost', 
              'delivery_charge as deliveryCharge', 
              'discount',  'payment_type  as paymentType',
              'book_status as bookingStatus', DB::Raw("'booktou' paymentTarget"),
              DB::Raw("'normal' orderType")) ; 

            $result = DB::table('ba_pick_and_drop_order')  
            ->where("request_from", "business")
            ->where("request_by", $bin )
            ->whereDate("service_date", "=",   $date  )
            ->orderBy("ba_pick_and_drop_order.id") 
            ->union($first)  
            ->select('ba_pick_and_drop_order.id', 
                "request_by as requestBy",
                'ba_pick_and_drop_order.service_date',
              "source",
              'total_amount as orderCost', 
              'service_fee as deliveryCharge', 
              DB::Raw("'0' discount") ,  'pay_mode as paymentType', 
              'book_status  as bookingStatus',             
              "payment_target as paymentTarget",
              DB::Raw("'pnd' orderType"))  
            ->get();

        } 
     
        
        $memberids = $bins = $oids = array();
        foreach($result as $item)
        {
            if($item->orderType == "normal")
            {
                $oids[] = $item->id ;
                $bins[] = $item->requestBy ;

            }
            if($item->orderType == "pnd")
            {
                $bins[] = $item->requestBy ;
            }
            if($item->orderType == "assist")
            {
                $memberids[] = $item->requestBy ;
            }
            
            
        }
        $oids[]= $bins[]= $memberids[]=0;

        $normal_sales_cart = DB::table("ba_shopping_basket") 
        ->whereIn("order_no",  $oids  )  
        ->get(); 


        $businesses  =  DB::table("ba_business") 
        ->whereIn("id",  $bins  )  
        ->get();

        $profiles  =  DB::table("ba_profile") 
        ->whereIn("id",  $memberids  )  
        ->get();  

       

       $data =  array(  'businesses' =>$businesses, 
        'profiles' => $profiles,
        'results' =>  $result, 
        'cart_items' => $normal_sales_cart,
        'bin' => $bin ,'reportDate' => $date );
       return view("admin.billing.detailed_sales_report_for_day")->with( $data ); 
     }





    public function generateClearanceBill( Request $request)
    {
        $bin = $request->bin; 
        if($request->year != "")
        {
            $year  = $request->year ; 
        }
        else
        {
            $year  = date('Y' );
        }


        if($request->month !="" )
        {
          $month = $request->month ;
        }
        else
        {
            $month = date('m' );
        }


        if($request->cycle == "" ||  $request->cycle  == 1 )
        {
            $startDay   =  1 ;  
            $endDay   =    7;
            $cycle = 1;
        }
        else if(  $request->cycle  == 2 )
        {
            $startDay   = 8 ;  
            $endDay   =    14;
            $cycle = 2;
        }else if(  $request->cycle  == 3)
        {
            $startDay   = 15 ;  
            $endDay   =    21;
           $cycle = 3;
        }else if(  $request->cycle  == 4)
        {
            $startDay   = 22 ;  
            $endDay   =    28;
            $cycle = 4;
        }
        else  
        {
            $totalDays = cal_days_in_month(CAL_GREGORIAN, $month , $year );

            if( $totalDays > 28)
            {
                $startDay   = 29;  
                $endDay   =    $totalDays;    
                $cycle = 5;
            }
            else 
            {

                return redirect("/admin/billing-and-clearance/sales-and-clearance-report?bin=" . $bin )->with("err_msg", 
                    "Selected month has only 4 weeks of payment.");

            }
            
        } 


        $first = DB::table('ba_service_booking')  
        ->where("ba_service_booking.bin", $bin )
         ->whereDay("service_date", ">=",  $startDay )
          ->whereDay("service_date", "<=",  $endDay )
          ->whereMonth("service_date", $month )
          ->whereYear("service_date", $year )
          ->orderBy("ba_service_booking.service_date")
        ->select('ba_service_booking.id', 
            'ba_service_booking.service_date',
          DB::Raw("'customer' source"), 
          'total_cost as orderCost', 
          DB::Raw("'0.00' packingCost"), 
          'delivery_charge as deliveryCharge', 
          'discount',  'payment_type  as paymentType',
          'book_status as bookingStatus', DB::Raw("'booktou' paymentTarget"),
          DB::Raw("'normal' orderType")) ; 

        $orders = DB::table('ba_pick_and_drop_order')  
        ->where("request_from", "business")
        ->where("request_by", $bin )
        ->whereDay("service_date", ">=",  $startDay )
          ->whereDay("service_date", "<=",  $endDay )
          ->whereMonth("service_date", $month )
          ->whereYear("service_date", $year )
          ->orderBy("ba_pick_and_drop_order.service_date")
        ->union($first)  
        ->select('ba_pick_and_drop_order.id', 
            'ba_pick_and_drop_order.service_date',
          "source",
          'total_amount as orderCost', 
          "packaging_cost as packingCost" , 
          'service_fee as deliveryCharge', 
          DB::Raw("'0' discount") ,  'pay_mode as paymentType', 
          'book_status  as bookingStatus',             
          "payment_target as paymentTarget",
          DB::Raw("'pnd' orderType"))  
        ->get();

        $oids = array();
        foreach($orders as $item)
        {
            if($item->orderType == "normal")
            {
                $oids[] = $item->id ; 

            } 
        }
        $oids[]= 0;

        $normal_sales_cart = DB::table("ba_shopping_basket") 
        ->whereIn("order_no",  $oids  )  
        ->get(); 
 

       $business  = Business::find(  $bin)  ;
       $data =  array(  'business' =>$business,   'orders' =>  $orders, 
       'cart_items' =>$normal_sales_cart,
       'bin' => $bin,  
       'startDate' => $startDay,   'endDate' => $endDay, 'month' => $month, 'year' => $year ); 

       $pdf_file =  "booktou_mcbill_" .time(). '_' . $bin . ".pdf";
       $save_path = public_path() . "/assets/erp/bills/"  .  $pdf_file    ;

        $pdf = PDF::setOptions(['defaultFont' => 'sans-serif'  ])
        ->setPaper('a4', 'portrait')
        ->loadView('admin.billing.clearance_bill_a4' ,  $data  )   ;

        return view('admin.billing.clearance_bill_a4')->with( $data );

        //return $pdf->download( $pdf_file  ); 

    }
    

    protected function monthlySalesServiceEarningReport(Request $request )
    {
        if($request->year != "")
        {
            $year  = $request->year ; 
        }
        else
        {
            $year  = date('Y' );
        }
 

        if($request->month !="" )
        {
          $month = $request->month ;
        }
        else
        {
            $month = date('m' );
        }

       
        $first = DB::table('ba_service_booking')   
        ->whereMonth("service_date", $month )
        ->whereYear("service_date", $year )
        ->whereIn("book_status",  array('delivered', 'completed')) 
        ->orderBy("ba_service_booking.service_date")
        ->select('ba_service_booking.id', 
          'ba_service_booking.service_date',
          "bin",
          DB::Raw("'customer' source"), 
          'total_cost as orderCost', 
          DB::Raw("'0.00' packingCost"), 
          'delivery_charge as deliveryCharge', 
          'discount',  'payment_type  as paymentType',
          'book_status as bookingStatus', DB::Raw("'booktou' paymentTarget"),
          DB::Raw("'normal' orderType")) ;
 
        $result = DB::table('ba_pick_and_drop_order')  
        ->where("request_from", "business")
        ->whereMonth("service_date", $month )
        ->whereYear("service_date", $year )
        ->whereIn("book_status",  array('delivered', 'completed')) 
        ->orderBy("ba_pick_and_drop_order.service_date")
        ->union($first)  
        ->select('ba_pick_and_drop_order.id', 
            'ba_pick_and_drop_order.service_date',
            "request_by as bin",
          "source",
          'total_amount as orderCost',
          'packaging_cost as packingCost',  
          'service_fee as deliveryCharge', 
          DB::Raw("'0' discount") ,  'pay_mode as paymentType', 
          'book_status  as bookingStatus',             
          "payment_target as paymentTarget",
          DB::Raw("'pnd' orderType"))  
        ->get();  

        $oids =  $bins = array();
        foreach($result as $item)
        {
            if($item->orderType == "normal")
            {
                $oids[] = $item->id ;  
            }
            $bins[] = $item->bin ; 
        }
        $bins[] = $oids[]= 0;

        $normal_sales_cart = DB::table("ba_shopping_basket") 
        ->whereIn("order_no",  $oids  )  
        ->get();  
 
 
        $all_business  = DB::table("ba_business")  
        ->whereIn("id", $bins ) 
        ->get(); 

       $data =  array(   
        'results' =>  $result,  
        'cart_items' =>$normal_sales_cart, 
        'businesses' => $all_business, 
        'month' =>  $month , 
        'year' => $year  );
       return view("admin.billing.sales_report_per_month_for_merchant")->with( $data ); 
     }


    public function dailySalesReport(Request $request )
    {
        if($request->todayDate != "")
        {
            $todayDate  =  date('Y-m-d' , strtotime($request->todayDate)) ; 
        }
        else
        {
            $todayDate  = date('Y-m-d' );
        }

        $totalEarning = 0;
 
        $bin = $request->session()->get('_bin_');

        $first = DB::table('ba_service_booking')
        ->join('ba_business', 'ba_service_booking.bin', '=', 'ba_business.id')
        ->wheredate('ba_service_booking.service_date', $todayDate)
        ->whereIn("book_status", array('completed', 'delivered') ) 
        ->where("ba_business.frno", "0")
        //->where("ba_business.id",$bin) 
        ->select('ba_service_booking.id', 
          DB::Raw("'customer' source"), 
          "ba_business.id as bin" ,   
          'total_cost as orderCost', 
          DB::Raw("'0.00' packingCost"), 
          'delivery_charge as deliveryCharge', 
          'discount',  'payment_type  as paymentType',
          'book_status as bookingStatus', 
          'ba_business.name', "ba_business.category", 
          "ba_business.commission",
          DB::Raw("'booktou' paymentTarget"),
          DB::Raw("'normal' orderType")) ;

        $second = DB::table('ba_pick_and_drop_order')
        ->join('ba_users', 'ba_pick_and_drop_order.request_by', '=', 'ba_users.profile_id')
        ->wheredate('ba_pick_and_drop_order.service_date', $todayDate)
        ->whereIn("book_status", array('completed', 'delivered') ) 
        ->where("request_from", "customer")
        ->where("ba_users.franchise", "0") 
        ->select('ba_pick_and_drop_order.id',  
          "source",
          DB::Raw("'0' bin"),
          'total_amount as orderCost', 
          'packaging_cost as packingCost',  
          'service_fee as deliveryCharge', 
          DB::Raw("'0' discount") ,  
          'pay_mode as paymentType', 
          'book_status  as bookingStatus',  
          'pickup_name as name', 
          DB::Raw("'customer' category"), 
          DB::Raw("'0' commission"),       
          "payment_target as paymentTarget",
          DB::Raw("'assist' orderType"))
        ->orderBy("id", "asc")   ;



        $result = DB::table('ba_pick_and_drop_order')
        ->join('ba_business', 'ba_pick_and_drop_order.request_by', '=', 'ba_business.id')
        ->wheredate('ba_pick_and_drop_order.service_date', $todayDate)
        ->whereIn("book_status", array('completed', 'delivered') ) 
        ->where("request_from", "business")
        ->where("ba_business.frno", "0") 
        ->union($first) 
        ->union($second) 
        ->select('ba_pick_and_drop_order.id',  
          "source","ba_business.id as bin",
          'total_amount as orderCost', 
          'packaging_cost as packingCost',  
          'service_fee as deliveryCharge', 
          DB::Raw("'0' discount") ,  'pay_mode as paymentType', 
          'book_status  as bookingStatus',  
          'ba_business.name', "ba_business.category", 
          "ba_business.commission",          
          "payment_target as paymentTarget",
          DB::Raw("'pnd' orderType"))
        ->orderBy("id", "asc")  
        ->get();


        $oids =  $bins = array();
        foreach($result as $item)
        {
            if($item->orderType == "normal")
            {
                $oids[] = $item->id ;  
            }
            $bins[] = $item->bin ; 
        }
        $bins[] = $oids[]= 0;

        $normal_sales_cart = DB::table("ba_shopping_basket") 
        ->whereIn("order_no",  $oids  )  
        ->get();  
 
 
        $all_business  = DB::table("ba_business")  
        ->whereIn("id", $bins ) 
        ->get(); 
  



        $data = array(  'sales' =>  $result , 
            'cart_items' =>$normal_sales_cart,  
            'date' =>  $todayDate,
            'businesses' => $all_business );  

        return view("reports.billing.sales_report_per_day")->with(  $data);  
    }



  public function monthlyTaxableSalesReport(Request $request) 
  {
        if( $request->month  == "")
        { 
            $month   =  date('m');
            $monthname = date('F', mktime(0, 0, 0, $month, 10));
        }
        else 
        {
          $month   =  $request->month ;
          $monthname = date('F', mktime(0, 0, 0, $month, 10));
        }
 

        if( $request->year   == "")
        { 
            $year = date('Y');
        }
        else 
        {
          $year   =  $request->year; 
        }
 


        $first = DB::table('ba_service_booking')
        ->join('ba_business', 'ba_service_booking.bin', '=', 'ba_business.id')
        ->whereMonth('ba_service_booking.service_date', $month)
        ->whereYear('ba_service_booking.service_date', $year)
        ->whereIn("book_status", array('completed', 'delivered') ) 
        ->where("ba_business.frno", "0") 
        ->select('ba_service_booking.id', 
          DB::Raw("'customer' source"), 
          "ba_business.id as bin" ,   
          'total_cost as orderCost', 
          DB::Raw("'0.00' packingCost"), 
          'delivery_charge as deliveryCharge', 
          'discount',  'payment_type  as paymentType',
          'book_status as bookingStatus', 
          'ba_business.name', "ba_business.category", 
          "ba_business.commission",
          DB::Raw("'booktou' paymentTarget"),
          DB::Raw("'normal' orderType"), "book_date", "service_date") ;

        $second = DB::table('ba_pick_and_drop_order')
        ->join('ba_users', 'ba_pick_and_drop_order.request_by', '=', 'ba_users.profile_id')
        ->whereMonth('ba_pick_and_drop_order.service_date', $month)
        ->whereYear('ba_pick_and_drop_order.service_date', $year)
        ->whereIn("book_status", array('completed', 'delivered') ) 
        ->where("request_from", "customer")
        ->where("ba_users.franchise", "0") 
        ->select('ba_pick_and_drop_order.id',  
          "source",
          DB::Raw("'0' bin"),
          'total_amount as orderCost', 
          'packaging_cost as packingCost',  
          'service_fee as deliveryCharge', 
          DB::Raw("'0' discount") ,  
          'pay_mode as paymentType', 
          'book_status  as bookingStatus',  
          'pickup_name as name', 
          DB::Raw("'customer' category"), 
          DB::Raw("'0' commission"),       
          "payment_target as paymentTarget",
          DB::Raw("'assist' orderType"), "book_date", "service_date")
        ->orderBy("id", "asc")   ;



        $result = DB::table('ba_pick_and_drop_order')
        ->join('ba_business', 'ba_pick_and_drop_order.request_by', '=', 'ba_business.id')
        ->whereMonth('ba_pick_and_drop_order.service_date', $month)
        ->whereYear('ba_pick_and_drop_order.service_date', $year)
        ->whereIn("book_status", array('completed', 'delivered') ) 
        ->where("request_from", "business")
        ->where("ba_business.frno", "0") 
        ->union($first) 
        ->union($second) 
        ->select('ba_pick_and_drop_order.id',  
          "source","ba_business.id as bin",
          'total_amount as orderCost', 
          'packaging_cost as packingCost',  
          'service_fee as deliveryCharge', 
          DB::Raw("'0' discount") ,  'pay_mode as paymentType', 
          'book_status  as bookingStatus',  
          'ba_business.name', "ba_business.category", 
          "ba_business.commission",          
          "payment_target as paymentTarget",
          DB::Raw("'pnd' orderType"), "book_date", "service_date")
        ->orderBy("id", "asc")  
        ->get();


        $oids =  $bins = array();
        foreach($result as $item)
        {
            if($item->orderType == "normal")
            {
                $oids[] = $item->id ;  
            }
            $bins[] = $item->bin ; 
        }
        $bins[] = $oids[]= 0;

        $normal_sales_cart = DB::table("ba_shopping_basket") 
        ->whereIn("order_no",  $oids  )  
        ->get();  
 
 
        $all_business  = DB::table("ba_business")  
        ->whereIn("id", $bins ) 
        ->get(); 
   


        $data = array(  'sales' =>  $result , 
            'cart_items' =>$normal_sales_cart,  
            'monthname' =>  $monthname,
            'businesses' => $all_business );  

        return view("admin.reports.monthly_tax_report")->with(  $data);  
 
  }


  public function orderTaxDetails(Request $request) 
  {
        if( $request->o  == "")
        {
            return redirect("//admin/accounting/monthly-taxable-sales-report")
            ->with("err_msg", "No order selected!"); 
        }
        else 
        {
          $orderno   =  $request->o ; 
        }
 
        
        $order_sequence = DB::table("ba_order_sequencer")
        ->where("id", $orderno)
        ->first();

        if($order_sequence->type == "normal" || $order_sequence->type  == "booking" )
        {
            $order_info = DB::table('ba_service_booking')
            ->join('ba_business', 'ba_service_booking.bin', '=', 'ba_business.id')
            ->where('ba_service_booking.id',  $orderno)
                    ->whereIn("book_status", array('completed', 'delivered') ) 
                    ->where("ba_business.frno", "0") 
                    ->select('ba_service_booking.id', 
                      DB::Raw("'customer' source"), 
                      "ba_business.id as bin" ,   
                      'total_cost as orderCost', 
                      DB::Raw("'0.00' packingCost"), 
                      'delivery_charge as deliveryCharge', 
                      'discount',  'payment_type  as paymentType',
                      'book_status as bookingStatus', 
                      'ba_business.name', "ba_business.category", 
                      "ba_business.commission",
                      DB::Raw("'booktou' paymentTarget"),
                      DB::Raw("'normal' orderType"), "book_date", "service_date")
                      ->first() ; 
        }
        else if($order_sequence->type  == "pnd"  )
        {
            $order_info = DB::table('ba_pick_and_drop_order')
            ->join('ba_business', 'ba_pick_and_drop_order.request_by', '=', 'ba_business.id')
            ->where('ba_pick_and_drop_order.id',  $orderno) 
            ->where("request_from", "business")
            ->where("ba_business.frno", "0")  
            ->select('ba_pick_and_drop_order.id',  
                  "source","ba_business.id as bin",
                  'total_amount as orderCost', 
                  'packaging_cost as packingCost',  
                  'service_fee as deliveryCharge', 
                  DB::Raw("'0' discount") ,  'pay_mode as paymentType', 
                  'book_status  as bookingStatus',  
                  'ba_business.name', "ba_business.category", 
                  "ba_business.commission",          
                  "payment_target as paymentTarget",
                  DB::Raw("'pnd' orderType"), "book_date", "service_date") 
            ->first() ;
                
            }
            else if(  $order_sequence->type  == "assist")
            {
                $order_info = DB::table('ba_pick_and_drop_order')
                ->join('ba_users', 'ba_pick_and_drop_order.request_by', '=', 'ba_users.profile_id')
                ->where('ba_pick_and_drop_order.id',  $orderno)
                ->where("request_from", "customer")
                ->where("ba_users.franchise", "0") 
                ->select('ba_pick_and_drop_order.id',  
                  "source",
                  DB::Raw("'0' bin"),
                  'total_amount as orderCost', 
                  'packaging_cost as packingCost',  
                  'service_fee as deliveryCharge', 
                  DB::Raw("'0' discount") ,  
                  'pay_mode as paymentType', 
                  'book_status  as bookingStatus',  
                  'pickup_name as name', 
                  DB::Raw("'customer' category"), 
                  DB::Raw("'0' commission"),       
                  "payment_target as paymentTarget",
                  DB::Raw("'assist' orderType"), "book_date", "service_date")
                ->first() ;
            }
        
        if( !isset($order_info) )
        {
            return redirect("/admin/accounting/monthly-taxable-sales-report")
            ->with("err_msg", "No order selected!"); 
        }
         

 
        $normal_sales_cart = DB::table("ba_shopping_basket") 
        ->where("order_no",  $orderno )  
        ->get();  
 
        $business  = DB::table("ba_business")  
        ->where("id", $order_info->bin ) 
        ->first();  


        $data = array(  
            'orderno' => $orderno,
            'order_info' =>  $order_info , 
            'cart_items' =>$normal_sales_cart,   
            'business' => $business );  

        return view("admin.reports.order_tax_report")->with(  $data);  
 
  }


  public function viewVoucherDetails(Request $request) 
  {
        if( $request->o  == "")
        {
            return redirect("//admin/accounting/monthly-taxable-sales-report")
            ->with("err_msg", "No order selected!"); 
        }
        else 
        {
          $orderno   =  $request->o ; 
        }
 
        
        $order_sequence = DB::table("ba_order_sequencer")
        ->where("id", $orderno)
        ->first();

        if($order_sequence->type == "normal" || $order_sequence->type  == "booking" )
        {
            $order_info = DB::table('ba_service_booking')
            ->join('ba_business', 'ba_service_booking.bin', '=', 'ba_business.id')
            ->where('ba_service_booking.id',  $orderno)
                    ->whereIn("book_status", array('completed', 'delivered') ) 
                    ->where("ba_business.frno", "0") 
                    ->select('ba_service_booking.id', 
                      DB::Raw("'customer' source"), 
                      "ba_business.id as bin" ,   
                      'total_cost as orderCost', 
                      DB::Raw("'0.00' packingCost"), 
                      'delivery_charge as deliveryCharge', 
                      'discount',  'payment_type  as paymentType',
                      'book_status as bookingStatus', 
                      'ba_business.name', "ba_business.category", 
                      "ba_business.commission",
                      DB::Raw("'booktou' paymentTarget"),
                      DB::Raw("'normal' orderType"), "book_date", "service_date")
                      ->first() ; 
        }
        else if($order_sequence->type  == "pnd"  )
        {
            $order_info = DB::table('ba_pick_and_drop_order')
            ->join('ba_business', 'ba_pick_and_drop_order.request_by', '=', 'ba_business.id')
            ->where('ba_pick_and_drop_order.id',  $orderno) 
            ->where("request_from", "business")
            ->where("ba_business.frno", "0")  
            ->select('ba_pick_and_drop_order.id',  
                  "source","ba_business.id as bin",
                  'total_amount as orderCost', 
                  'packaging_cost as packingCost',  
                  'service_fee as deliveryCharge', 
                  DB::Raw("'0' discount") ,  'pay_mode as paymentType', 
                  'book_status  as bookingStatus',  
                  'ba_business.name', "ba_business.category", 
                  "ba_business.commission",          
                  "payment_target as paymentTarget",
                  DB::Raw("'pnd' orderType"), "book_date", "service_date") 
            ->first() ;
                
            }
            else if(  $order_sequence->type  == "assist")
            {
                $order_info = DB::table('ba_pick_and_drop_order')
                ->join('ba_users', 'ba_pick_and_drop_order.request_by', '=', 'ba_users.profile_id')
                ->where('ba_pick_and_drop_order.id',  $orderno)
                ->where("request_from", "customer")
                ->where("ba_users.franchise", "0") 
                ->select('ba_pick_and_drop_order.id',  
                  "source",
                  DB::Raw("'0' bin"),
                  'total_amount as orderCost', 
                  'packaging_cost as packingCost',  
                  'service_fee as deliveryCharge', 
                  DB::Raw("'0' discount") ,  
                  'pay_mode as paymentType', 
                  'book_status  as bookingStatus',  
                  'pickup_name as name', 
                  DB::Raw("'customer' category"), 
                  DB::Raw("'0' commission"),       
                  "payment_target as paymentTarget",
                  DB::Raw("'assist' orderType"), "book_date", "service_date")
                ->first() ;
            }
        
        if( !isset($order_info) )
        {
            return redirect("/admin/accounting/monthly-taxable-sales-report")
            ->with("err_msg", "No order selected!"); 
        }
         

 
        $normal_sales_cart = DB::table("ba_shopping_basket") 
        ->where("order_no",  $orderno )  
        ->get();  
 
        $business  = DB::table("ba_business")  
        ->where("id", $order_info->bin ) 
        ->first();  


        $data = array(  
            'orderno' => $orderno,
            'order_info' =>  $order_info , 
            'cart_items' =>$normal_sales_cart,   
            'business' => $business );  

        return view("admin.reports.order_tax_report")->with(  $data); 
 
  }



  public function prepareVoucherEntry(Request $request) 
  {
        if( $request->o  == "")
        {
            return redirect("//admin/accounting/monthly-taxable-sales-report")
            ->with("err_msg", "No order selected!"); 
        }
        else 
        {
          $orderno   =  $request->o ; 
        }

        $order_sequence = DB::table("ba_order_sequencer")
        ->where("id", $orderno)
        ->first();

        if($order_sequence->type == "normal" || $order_sequence->type  == "booking" )
        {
            $order_info = DB::table('ba_service_booking')
            ->join('ba_business', 'ba_service_booking.bin', '=', 'ba_business.id')
            ->where('ba_service_booking.id',  $orderno)
                    ->whereIn("book_status", array('completed', 'delivered') ) 
                    ->where("ba_business.frno", "0") 
                    ->select('ba_service_booking.id', 
                      DB::Raw("'customer' source"), 
                      "ba_business.id as bin" ,   
                      'total_cost as orderCost', 
                      DB::Raw("'0.00' packingCost"), 
                      'delivery_charge as deliveryCharge', 
                      'discount',  'payment_type  as paymentType',
                      'book_status as bookingStatus', 
                      'ba_business.name', "ba_business.category", 
                      "ba_business.commission",
                      DB::Raw("'booktou' paymentTarget"),
                      DB::Raw("'normal' orderType"), "book_date", "service_date")
                      ->first() ; 
        }
        else if($order_sequence->type  == "pnd"  )
        {
            $order_info = DB::table('ba_pick_and_drop_order')
            ->join('ba_business', 'ba_pick_and_drop_order.request_by', '=', 'ba_business.id')
            ->where('ba_pick_and_drop_order.id',  $orderno) 
            ->where("request_from", "business")
            ->where("ba_business.frno", "0")  
            ->select('ba_pick_and_drop_order.id',  
                  "source","ba_business.id as bin",
                  'total_amount as orderCost', 
                  'packaging_cost as packingCost',  
                  'service_fee as deliveryCharge', 
                  DB::Raw("'0' discount") ,  'pay_mode as paymentType', 
                  'book_status  as bookingStatus',  
                  'ba_business.name', "ba_business.category", 
                  "ba_business.commission",          
                  "payment_target as paymentTarget",
                  DB::Raw("'pnd' orderType"), "book_date", "service_date") 
            ->first() ;
                
            }
            else if(  $order_sequence->type  == "assist")
            {
                $order_info = DB::table('ba_pick_and_drop_order')
                ->join('ba_users', 'ba_pick_and_drop_order.request_by', '=', 'ba_users.profile_id')
                ->where('ba_pick_and_drop_order.id',  $orderno)
                ->where("request_from", "customer")
                ->where("ba_users.franchise", "0") 
                ->select('ba_pick_and_drop_order.id',  
                  "source",
                  DB::Raw("'0' bin"),
                  'total_amount as orderCost', 
                  'packaging_cost as packingCost',  
                  'service_fee as deliveryCharge', 
                  DB::Raw("'0' discount") ,  
                  'pay_mode as paymentType', 
                  'book_status  as bookingStatus',  
                  'pickup_name as name', 
                  DB::Raw("'customer' category"), 
                  DB::Raw("'0' commission"),       
                  "payment_target as paymentTarget",
                  DB::Raw("'assist' orderType"), "book_date", "service_date")
                ->first() ;
            }
        
        if( !isset($order_info) )
        {
            return redirect("/admin/accounting/monthly-taxable-sales-report")
            ->with("err_msg", "No order selected!"); 
        }
          
 
        $normal_sales_cart = DB::table("ba_shopping_basket") 
        ->where("order_no",  $orderno )  
        ->get();  
 
        $business  = DB::table("ba_business")  
        ->where("id", $order_info->bin ) 
        ->first();  

        $month = date('m', strtotime($order_info->book_date));
        $year  = date('Y', strtotime($order_info->book_date));

        $account_types = DB::table('bta_accounts')
        ->get() ;


        $balance_sheet = DB::table('bta_order_journal')
        ->join('bta_journal_entry', 'bta_journal_entry.vno', '=', 'bta_order_journal.vno') 
        ->whereMonth("voucher_date", $month  )
        ->whereYear("voucher_date", $year )
        ->where("bta_order_journal.vno", "<=",  $orderno )
        ->select( "bta_journal_entry.ac_key_name" , "bta_journal_entry.rule",
            DB::Raw(" sum(debit) as debit") , 
            DB::Raw(" sum(credit) as credit")  ) 
        ->groupBy("bta_journal_entry.ac_key_name" ) 
        ->groupBy("bta_journal_entry.rule" ) 
        ->get() ;
         
 

        $data = array(  
            'orderno' => $orderno,
            'order_info' =>  $order_info , 
            'cart_items' =>$normal_sales_cart,   
            'business' => $business,
            'account_types' =>$account_types ,
            'balance_sheet' => $balance_sheet);  
 

        return view("admin.reports.order_to_voucher_entry")->with(  $data); 
 
  }


 

  public function updateVoucherEntry(Request $request) 
  {
    if( $request->o  == "")
    {
        return redirect("//admin/accounting/monthly-taxable-sales-report")
        ->with("err_msg", "No order selected!"); 
    }
    else 
    {
        $orderno   =  $request->o ; 
    }
    
    if($orderno < 13876     )
    {
        return redirect("//admin/accounting/monthly-taxable-sales-report")
        ->with("err_msg", "No order selected!");  
    }

    //finding order
     $order_sequence = DB::table("ba_order_sequencer")
        ->where("id", $orderno)
        ->first();

        if($order_sequence->type == "normal" || $order_sequence->type  == "booking" )
        {
            $order_info = DB::table('ba_service_booking')
            ->join('ba_business', 'ba_service_booking.bin', '=', 'ba_business.id')
            ->where('ba_service_booking.id',  $orderno)
                    ->whereIn("book_status", array('completed', 'delivered') ) 
                    ->where("ba_business.frno", "0") 
                    ->select('ba_service_booking.id', 
                      DB::Raw("'customer' source"),  
                      "customer_name as customer",
                      "address",
                      "ba_business.id as bin" ,   
                      'total_cost as orderCost', 
                      DB::Raw("'0.00' packingCost"), 
                      'delivery_charge as deliveryCharge', 
                      'discount',  'payment_type  as paymentType',
                      'book_status as bookingStatus', 
                      'ba_business.name', "ba_business.category", 
                      "ba_business.commission",
                      DB::Raw("'booktou' paymentTarget"),
                      DB::Raw("'normal' orderType"), "book_date", "service_date")
                      ->first() ; 
        }
        else if($order_sequence->type  == "pnd"  )
        {
            $order_info = DB::table('ba_pick_and_drop_order')
            ->join('ba_business', 'ba_pick_and_drop_order.request_by', '=', 'ba_business.id')
            ->where('ba_pick_and_drop_order.id',  $orderno) 
            ->where("request_from", "business")
            ->where("ba_business.frno", "0")  
            ->select('ba_pick_and_drop_order.id',  
                  "source",
                   "drop_name as customer",
                   "drop_address as address",
                  "ba_business.id as bin",
                  'total_amount as orderCost', 
                  'packaging_cost as packingCost',  
                  'service_fee as deliveryCharge', 
                  DB::Raw("'0' discount") ,  'pay_mode as paymentType', 
                  'book_status  as bookingStatus',  
                  'ba_business.name', "ba_business.category", 
                  "ba_business.commission",          
                  "payment_target as paymentTarget",
                  DB::Raw("'pnd' orderType"), "book_date", "service_date") 
            ->first() ;
                
            }
            else if(  $order_sequence->type  == "assist")
            {
                $order_info = DB::table('ba_pick_and_drop_order')
                ->join('ba_users', 'ba_pick_and_drop_order.request_by', '=', 'ba_users.profile_id')
                ->where('ba_pick_and_drop_order.id',  $orderno)
                ->where("request_from", "customer")
                ->where("ba_users.franchise", "0") 
                ->select('ba_pick_and_drop_order.id',  
                  "source",
                   "drop_name as customer",
                   "drop_address as address",
                  DB::Raw("'0' bin"),
                  'total_amount as orderCost', 
                  'packaging_cost as packingCost',  
                  'service_fee as deliveryCharge', 
                  DB::Raw("'0' discount") ,  
                  'pay_mode as paymentType', 
                  'book_status  as bookingStatus',  
                  'pickup_name as name', 
                  DB::Raw("'customer' category"), 
                  DB::Raw("'0' commission"),       
                  "payment_target as paymentTarget",
                  DB::Raw("'assist' orderType"), "book_date", "service_date")
                ->first() ;
            }
        
    if( !isset($order_info) )
    {
        return redirect("/admin/accounting/monthly-taxable-sales-report")->with("err_msg", "No order selected!"); 
    }

    //saving new voucher or updating existing one
    $order_journal = OrderVoucherModel::find( $orderno) ;
    if( !isset($order_journal))
    {
        $order_journal = new OrderVoucherModel ;
    }
    $order_journal->vno  = $orderno ;
    $order_journal->voucher_date = $order_info->book_date;
    $order_journal->cust_name = $order_info->customer;
    $order_journal->cust_address = $order_info->address;
    $order_journal->related_order =$orderno;
    $order_journal->save();


    $cash_dr  = $request->tbcasset;
    $merchant_cr  = $request->tbmerlbt;
    $comm_cr  = $request->tbcommin; 
    $delivery_cr  = $request->tbdelin; 
    $cgst_cr  = $request->tbcgstlbt;
    $sgst_cr  = $request->tbsgstlbt;

    //making voucher entries
    //finding existing entry for cash_asset
    $voucher_entry = OrderJournalEntryModel::where("vno", $order_journal->vno  )
    ->where("ac_key_name",  "cash_asset")
    ->first();
    if( !isset($voucher_entry))
    {
        $voucher_entry = new OrderJournalEntryModel ;
    }
    $voucher_entry->vno =  $orderno ;
    $voucher_entry->ac_title = "Cash A/c";
    $voucher_entry->ac_key_name = "cash_asset";
    $voucher_entry->ac_type = "asset";
    $voucher_entry->debit = $cash_dr;  
    $voucher_entry->rule= "dr"; 
    $voucher_entry->save();

    //finding existing entry for merchant_liability
    $voucher_entry = OrderJournalEntryModel::where("vno", $order_journal->vno  )
    ->where("ac_key_name",  "merchant_liability")
    ->first();
    if( !isset($voucher_entry))
    {
        $voucher_entry = new OrderJournalEntryModel ;
    }
    $voucher_entry->vno = $orderno ;
    $voucher_entry->ac_title = "Merchant A/c";
    $voucher_entry->ac_key_name = "merchant_liability";
    $voucher_entry->ac_type = "liability"; 
    $voucher_entry->credit= $merchant_cr ; 
    $voucher_entry->rule= "cr"; 
    $voucher_entry->save();


    //finding existing entry for comm_income
    $voucher_entry = OrderJournalEntryModel::where("vno", $order_journal->vno  )
    ->where("ac_key_name",  "comm_income")
    ->first();
    if( !isset($voucher_entry))
    {
        $voucher_entry = new OrderJournalEntryModel ;
    }
    $voucher_entry->vno = $orderno ;
    $voucher_entry->ac_title = "Commission Income A/c";
    $voucher_entry->ac_key_name = "comm_income";
    $voucher_entry->ac_type = "income"; 
    $voucher_entry->credit= $comm_cr ; 
    $voucher_entry->rule= "cr"; 
    $voucher_entry->save();


    //finding existing entry for delivery_income
    $voucher_entry = OrderJournalEntryModel::where("vno", $order_journal->vno  )
    ->where("ac_key_name",  "delivery_income")
    ->first();
    if( !isset($voucher_entry))
    {
        $voucher_entry = new OrderJournalEntryModel ;
    }
    $voucher_entry->vno = $orderno ;
    $voucher_entry->ac_title = "Delivery Charge A/c";
    $voucher_entry->ac_key_name = "delivery_income";
    $voucher_entry->ac_type = "income"; 
    $voucher_entry->credit= $delivery_cr ; 
    $voucher_entry->rule= "cr"; 
    $voucher_entry->save();

    //finding existing entry for sgst_liability
    $voucher_entry = OrderJournalEntryModel::where("vno", $order_journal->vno  )
    ->where("ac_key_name",  "sgst_liability")
    ->first();
    if( !isset($voucher_entry))
    {
        $voucher_entry = new OrderJournalEntryModel ;
    }
    $voucher_entry->vno = $orderno ;
    $voucher_entry->ac_title = "Ouput SGST";
    $voucher_entry->ac_key_name = "sgst_liability";
    $voucher_entry->ac_type = "liability"; 
    $voucher_entry->credit= $sgst_cr ; 
    $voucher_entry->rule= "cr"; 
    $voucher_entry->save();

    
    //finding existing entry for cgst_liability
    $voucher_entry = OrderJournalEntryModel::where("vno", $order_journal->vno  )
    ->where("ac_key_name",  "cgst_liability")
    ->first();
    if( !isset($voucher_entry))
    {
        $voucher_entry = new OrderJournalEntryModel ;
    }
    $voucher_entry->vno = $orderno ;
    $voucher_entry->ac_title = "Output CGST";
    $voucher_entry->ac_key_name = "cgst_liability";
    $voucher_entry->ac_type = "liability"; 
    $voucher_entry->credit= $cgst_cr ; 
    $voucher_entry->rule= "cr"; 
    $voucher_entry->save();

    return redirect("/admin/accounting/prepare-voucher-entry?o=" . $orderno )
    ->with("err_msg", "Voucher entry updated!");

  }

    protected function cyclewiseSalesServiceReport(Request $request )
    {
         $bin = $request->bin; 
        if($request->year != "")
        {
            $year  = $request->year ; 
        }
        else
        {
            $year  = date('Y' );
        }
 

        if($request->month !="" )
        {
          $month = $request->month ;
        }
        else
        {
            $month = date('m' );
        }

        $cycle = 1;
        if($request->cycle == "" ||  $request->cycle  == 1 )
        {
            $startDay   =  1 ;  
            $endDay   =    7;
            $cycle = 1;
        }
        else if(  $request->cycle  == 2 )
        {
            $startDay   = 8 ;  
            $endDay   =    14;
            $cycle = 2;
        }else if(  $request->cycle  == 3)
        {
            $startDay   = 15 ;  
            $endDay   =    21;
            $cycle = 3;
        }else if(  $request->cycle  == 4)
        {
            $startDay   = 22 ;  
            $endDay   =    28;
            $cycle = 4;
        }
        else  
        {
            $totalDays = cal_days_in_month(CAL_GREGORIAN, $month , $year );

            if( $totalDays > 28)
            {
                $startDay   = 29;  
                $endDay   =    $totalDays;   
                $cycle = 5; 
            }
            else 
            {
                $startDay   =  1 ;  
                $endDay   =    7;
                $cycle = 1;
            } 
        }

         $first = DB::table('ba_service_booking')   
        ->whereMonth("service_date", $month )
        ->whereYear("service_date", $year )
        ->whereDay("service_date", ">=",  $startDay )
        ->whereDay("service_date", "<=",  $endDay )
        ->whereIn("book_status",  array('delivered', 'completed')) 
        ->orderBy("ba_service_booking.service_date")
        ->select('ba_service_booking.id', 
          'ba_service_booking.service_date',
          "bin",
          DB::Raw("'customer' source"), 
          'total_cost as orderCost', 
          DB::Raw("'0.00' packingCost"), 
          'delivery_charge as deliveryCharge', 
          'discount',  'payment_type  as paymentType',
          'book_status as bookingStatus', DB::Raw("'booktou' paymentTarget"),
          DB::Raw("'normal' orderType")) ;
 
        $result = DB::table('ba_pick_and_drop_order')  
        ->where("request_from", "business")
        ->whereMonth("service_date", $month )
        ->whereYear("service_date", $year )
         ->whereDay("service_date", ">=",  $startDay )
        ->whereDay("service_date", "<=",  $endDay )
        ->whereIn("book_status",  array('delivered', 'completed')) 
        ->orderBy("ba_pick_and_drop_order.service_date")
        ->union($first)  
        ->select('ba_pick_and_drop_order.id', 
            'ba_pick_and_drop_order.service_date',
            "request_by as bin",
          "source",
          'total_amount as orderCost',
          'packaging_cost as packingCost',  
          'service_fee as deliveryCharge', 
          DB::Raw("'0' discount") ,  'pay_mode as paymentType', 
          'book_status  as bookingStatus',             
          "payment_target as paymentTarget",
          DB::Raw("'pnd' orderType"))  
        ->get();  

        $oids =  $bins = array();
        foreach($result as $item)
        {
            if($item->orderType == "normal")
            {
                $oids[] = $item->id ;  
            }
            $bins[] = $item->bin ; 
        }
        $bins[] = $oids[]= 0;

        $normal_sales_cart = DB::table("ba_shopping_basket") 
        ->whereIn("order_no",  $oids  )  
        ->get();  
 
 
        $all_business  = DB::table("ba_business")  
        ->whereIn("id", $bins ) 
        ->get();  
 

       $data =  array(   
        'results' =>  $result,  
        'cart_items' =>$normal_sales_cart, 
        'businesses' => $all_business, 
        'cycle' => $cycle,
        'month' =>  $month , 
        'year' => $year  ); 
 


       return view("admin.billing.sales_report_per_cycle")->with( $data ); 
     }
}

