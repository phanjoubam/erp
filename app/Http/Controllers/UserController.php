<?php

namespace App\Http\Controllers; 
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Http\Request; 
use Illuminate\Support\Facades\Auth;


class UserController extends Controller
{
   
   

	public function logout(Request $request)
    {
    	Auth::logout(); 
		$request->session()->flush(); 
		return redirect()->intended('login');
    }


     

 	public function logoutCustomer (Request $request)
    {
    	Auth::logout(); 
		$request->session()->flush(); 
		return redirect('/');
    }
	 

}
