<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BusinessReview extends Model
{
    protected $table = 'ba_business_review';
}
