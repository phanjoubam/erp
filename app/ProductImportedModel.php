<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductImportedModel extends Model
{
	protected $fillable = [  'bin', 'barcode', 'pr_name', 'description', 'unit_price',  'stock' ];
    protected $table = 'ba_products_imported';
    public $timestamps = false;

}
