<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderStatusLogModel extends Model
{
    protected $table = 'ba_order_status_log';
	public $timestamps = false;
}
