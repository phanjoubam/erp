<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AgentattendModel extends Model
{
    protected $table = 'ba_agent_attendance';
    public $timestamps = false;
}
