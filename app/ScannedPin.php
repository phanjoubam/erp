<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ScannedPin extends Model
{
	protected $table = 'ba_pin_scanned';
	public $timestamps = false;
} 