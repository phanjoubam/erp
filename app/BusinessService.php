<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BusinessService extends Model
{
	protected $table = 'ba_business_services';
}
