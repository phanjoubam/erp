<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CouponModel  extends Model
{
	protected $table = 'ba_coupons';
	public $timestamps = false;
}
