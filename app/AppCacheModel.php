<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AppCacheModel extends Model
{
	protected $table = 'ba_app_cache';
	public $timestamps = false;
}
