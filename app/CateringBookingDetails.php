<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CateringBookingDetails extends Model
{
    protected $table = 'ba_catering_booking_details';
}
