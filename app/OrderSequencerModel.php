<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderSequencerModel extends Model
{
	protected $table = 'ba_order_sequencer';
    public $timestamps = false;
}
