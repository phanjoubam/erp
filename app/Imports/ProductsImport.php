<?php

namespace App\Imports;

use App\ProductImportedModel;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;

class ProductsImport implements ToModel, WithStartRow
{



    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row )
    {
      return new ProductImportedModel([
        'bin' => $row[0],
        'barcode' => ($row[1] == "" ? null : $row[1] ), 
        'pr_name'   => ($row[2] == "" ? $row[1] : $row[2] ) ,
        'description'    => ($row[3] == "" ? $row[1] : $row[3] ) ,
        'unit_price'    =>  $row[4],
        'stock' => ($row[5] == "" ? 0: $row[5] ),
     ]);
    }

    public function startRow(): int
   {
       return 2;
   }

}
