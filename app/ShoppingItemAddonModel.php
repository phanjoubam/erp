<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShoppingItemAddonModel extends Model
{
    protected $table = 'ba_shopping_item_add_on';
    public $timestamps = false;
}
