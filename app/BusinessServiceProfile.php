<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BusinessServiceProfile extends Model
{
    protected $table = 'ba_business_service_profile';
	public $timestamps = false;
}
