<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BusinessPhoto extends Model
{
	protected $table = 'ba_business_photos';
}
