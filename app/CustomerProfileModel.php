<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomerProfileModel extends Model
{
    protected $table = 'ba_profile';
    public $timestamps = false;

}
