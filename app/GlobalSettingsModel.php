<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GlobalSettingsModel extends Model
{
    protected $table = 'ba_global_settings';
    public $timestamps = false;
}
