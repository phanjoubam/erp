<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CateringSubMenu extends Model
{
    protected $table = 'ba_catering_sub_menu';
}
