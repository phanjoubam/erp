<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentDealingModel  extends Model
{
    protected $table = 'ba_payment_dealings'; 
    public $timestamps = false;
}
