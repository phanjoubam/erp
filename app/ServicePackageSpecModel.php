<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServicePackageSpecModel extends Model
{
	protected $table = 'ba_service_package_spec';
	public $timestamps = false;
}
