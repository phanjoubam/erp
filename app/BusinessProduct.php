<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BusinessProduct extends Model
{
     protected $table = 'ba_business_product';
}
