<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StaffAvailability extends Model
{
	protected $table = 'ba_business_staff_availability';
}
