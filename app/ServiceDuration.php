<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceDuration extends Model
{
	protected $table = 'ba_services_duration';
}
