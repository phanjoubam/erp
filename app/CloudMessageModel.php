<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CloudMessageModel extends Model
{
    protected $table = 'ba_cloud_message';
     public $timestamps = false;
}
