<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MemberAddressModel extends Model
{
    protected $table = 'ba_member_address';
    public $timestamps = false;

}
