<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AgentServiceAreaModel  extends Model
{
    protected $table = 'ba_agent_areas';
    public $timestamps = false;

}
