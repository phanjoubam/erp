<?php

namespace App\Traits;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\URL;

use App\Business;
use App\User; 

use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use GuzzleHttp\Promise;



trait Utilities
{

    public function getUserInfo($uid)
    {
		 
        $data = array( "user_id" => 'na', 'profileIsComplete' => 'no',  'name' => 'na' ,  "bin" => "na", 
          "member_id" => "na" , "business"=> "na",  
          'category' => '-1' , 'pinCode' => 'na'); 
        $user = User::find($uid); 
 
        if( isset($user) )
        {


          //0 - customer, 1 - salon owner, 10 - staff, 100-delivery executive 

          if(  $user->category == 1 || $user->category == 10  )
          {
              
              $business_info = DB::table("ba_business") 
              ->select( "id", "name", "phone_pri as phone" )
              ->where("id", $user->bin ) 
              ->first();
              
              if(isset($business_info ))
              {
                $data['name'] = $business_info->name ;
                $data['bin'] = $business_info->id  ;
                $data['name'] = $business_info->phone ;
                 $data['member_id'] = $user->profile_id   ;
                $data['profileIsComplete'] = 'yes'; 
              }

          }
          else  if(  $user->category == 0 || $user->category == 100  )
          {
              $profile_info  = DB::table("ba_profile") 
              ->select( "id", "fullname as name", "phone"  )
              ->where("id", $user->profile_id ) 
              ->first();
              
              if(isset($profile_info ))
              {
                $data['name'] = $profile_info->name ;
                $data['bin'] = 0   ;
                $data['member_id'] = $profile_info->id  ;
                $data['phone'] = $profile_info->phone ;
                 $data['pinCode'] =  $user->pin_code ;
              }
          }
          else 
          {
              
          }
 
            $data['category'] = $user->category ;
      			$data['user_id'] = $user->id ;
      			$data['name'] = $user->fname ;
            $data['userAddress'] = $user->locality ;

			 
        }
        return json_encode($data);

    }
 
	 
 public function sendSmsAlert( $phones, $message  )
 { 


    $api_key = '35EE230E8E71EA';
    $contacts = implode(",", $phones) ;
    $from = 'TECHIT';
    $sms_text =  $message   ;
    //Submit to server
    $ch = curl_init();
      curl_setopt($ch,CURLOPT_URL, "http://sms.acceptsms.com/app/smsapi/index.php");
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, "key=".$api_key."&campaign=0&routeid=9&type=text&contacts=".$contacts."&senderid=".$from."&msg=".$sms_text);
      $response = curl_exec($ch);
      curl_close($ch);
       

      /*
    foreach($phones as $phone )
    {
         $ch = curl_init('https://www.txtguru.in/imobile/api.php?');
         curl_setopt($ch, CURLOPT_POST, 1);
         curl_setopt($ch, CURLOPT_POSTFIELDS, "username=ezrasorokhaiba&password=Mayol@2019&source=INFSMS&dmobile=". $phone .
         "&message=" . $message );
         curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
         $data = curl_exec($ch); 
     } 
*/
   

}




public function sendFCMNotification( $tokens , $title, $message )
{

    $notification = array(
     'title' => $title ,
     'body' => $message ,
     'sound' => 'default',
     'badge' => '1',
     'image' => "https://booktou.in/app/public/assets/image/logo.png"  );


   $api_key = 'AAAATlUiw28:APA91bGnNJEV_wqD-ROY22yxNGNVPUP12frmjiWaDPKT_vnGcUQ9ufvW14tXV5PnswkaL_v8b0urqgTrjr0_ibK40NtNGyO7oIxFfcIikNXsKIS2rphxPFh4gtUA_ID7NbQ3FuF6IIGI';
   $headers = array(
     'Content-Type:application/json',
     'Authorization:key='. $api_key
   );

   $ch = curl_init('https://fcm.googleapis.com/fcm/send');
   curl_setopt($ch, CURLOPT_POST, true);
   curl_setopt($ch, CURLOPT_RETURNTRANSFER, true );
   curl_setopt($ch, CURLOPT_HTTPHEADER,$headers);

   foreach($tokens as $token)
   {
     $arrayToSend = array('to' => $token, 'data' => $notification,'priority'=>'high');
     $json = json_encode($arrayToSend);
     curl_setopt($ch, CURLOPT_POSTFIELDS,   $json   );
     $data = curl_exec($ch); 
   }

 }



 public function calcGeoDistance( $latitude1, $longitude1, $latitude2, $longitude2)
 {


    $theta = $longitude1 - $longitude2; 
    $distance = (sin(deg2rad($latitude1)) * sin(deg2rad($latitude2))) + (cos(deg2rad($latitude1)) * cos(deg2rad($latitude2)) * cos(deg2rad($theta))); 
    $distance = acos($distance); 
    $distance = rad2deg($distance); 
    $distance = $distance * 60 * 1.1515 * 1.609344;   

    return $distance ;  
}

  public function chunkImage($source , $target_folder , $destination )
  {

    //compress
    $info = getimagesize($source );

    if ($info['mime'] == 'image/jpeg')

      $image = imagecreatefromjpeg($source);

    elseif ($info['mime'] == 'image/gif')

      $image = imagecreatefromgif($source);

    elseif ($info['mime'] == 'image/png')
      $image = imagecreatefrompng($source);


    $original_w = $info[0];
    $original_h = $info[1];
    $thumb_w =  ceil( $original_w / 2  );
    $thumb_h =  ceil( $original_h / 2  );

    $thumb_img = imagecreatetruecolor($thumb_w, $thumb_h);
    
    imagecopyresampled($thumb_img, $image, 0, 0, 0, 0, $thumb_w, $thumb_h, $original_w, $original_h);

    imagejpeg($thumb_img, $destination); 
                            

  }

 public  function getDrivingDistance($lat1, $long1, $lat2, $long2)
{
    $key = 'AIzaSyC9KrtxONZFci3FxO0SPZJSejUXHpvgmHI' ;   
    $url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=".$lat1.",".$long1."&destinations=".$lat2.",".$long2."&mode=driving&key=" . $key ;
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    $response = curl_exec($ch);
    curl_close($ch);
    $response_a = json_decode($response, true);
    $dist = $response_a['rows'][0]['elements'][0]['distance']['text'];
    $time = $response_a['rows'][0]['elements'][0]['duration']['text'];

    return array('distance' => $dist, 'time' => $time);
}

}
