<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ScannedAddress extends Model
{
	protected $table = 'ba_bin_scanned';
	public $timestamps = false;
} 