<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BusinessOwner extends Model
{
    protected $table = 'ba_business_owner';
}
