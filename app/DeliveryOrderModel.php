<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DeliveryOrderModel extends Model
{
    protected $table = 'ba_delivery_order';
    public $timestamps = false;
}
