<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductUnitModel  extends Model
{
    protected $table = 'ba_product_units'; 
    public $timestamps = false;
}
