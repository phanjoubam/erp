<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceBooking extends Model
{
	protected $table = 'ba_service_booking';
	public $timestamps = false;
}
