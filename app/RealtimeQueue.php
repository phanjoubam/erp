<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RealtimeQueue extends Model
{
    protected $table = 'ba_realtime_queue';
    public $timestamps = false;
}
