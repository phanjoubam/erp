<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BusinessServiceProduct  extends Model
{
	protected $table = 'ba_service_products';
	public $timestamps = false;
	
	//obsolte
}
