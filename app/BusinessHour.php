<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BusinessHour extends Model
{
	protected $table = 'ba_shop_hours';
}
