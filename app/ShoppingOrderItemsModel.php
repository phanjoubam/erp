<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShoppingOrderItemsModel extends Model
{
    protected $table = 'ba_shopping_basket';
    public $timestamps = false;
}
