<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductSpecsModel  extends Model
{
    protected $table = 'ba_product_specs';
    public $timestamps = false;

}
