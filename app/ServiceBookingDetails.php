<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceBookingDetails extends Model
{
    protected $table = 'ba_service_booking_details';
}
