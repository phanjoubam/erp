<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShoppingOrderModel extends Model
{
     protected $table = 'ba_shopping_order';
    public $timestamps = false;
}
