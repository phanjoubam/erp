<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductVariantModel extends Model
{
    protected $primaryKey = 'prsubid';  
    protected $table = 'ba_product_variant';
    public $timestamps = false;

}
