<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StaffAttendanceModel extends Model
{
	protected $table = 'ba_staff_attendance';
    public $timestamps = false;
}
