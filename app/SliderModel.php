<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SliderModel extends Model
{
	protected $table = 'ba_slides';
	public $timestamps = false; 
	
}
