<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PosOrderModel extends Model
{
    protected $table = 'ba_pos_order'; 
    public $timestamps = false;
}
