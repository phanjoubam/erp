<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PosOrderBasketModel  extends Model
{
    protected $table = 'ba_pos_order_basket'; 
    public $timestamps = false;
}
