<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CateringBooking extends Model
{
    protected $table = 'ba_catering_booking';
}
