<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PndassignModel extends Model
{
  protected $table = 'ba_pnd_assign_request';
    public $timestamps = false;
}
