<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StaffDayOff  extends Model
{
	protected $table = 'ba_staff_day_off';
    public $timestamps = false;
}
