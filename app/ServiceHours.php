<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceHours extends Model
{
	protected $table = 'ba_services_hours';
}
