<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BusinessReceipt extends Model
{
    protected $table = 'ba_business_receipt';
}
