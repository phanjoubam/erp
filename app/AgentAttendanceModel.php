<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AgentAttendanceModel extends Model
{
	protected $table = 'ba_active_agents';
	public $timestamps = false;
}
