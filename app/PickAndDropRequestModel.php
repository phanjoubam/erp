<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PickAndDropRequestModel  extends Model
{
	protected $table = 'ba_pick_and_drop_order';
	public $timestamps = false;
}
