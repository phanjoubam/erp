<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BusinessSales extends Model
{
    protected $table = 'ba_business_sales';
}
