<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CallbackEnquiryModel extends Model
{
	protected $table = 'ba_callback_enquiry';
    public $timestamps = false;
}
