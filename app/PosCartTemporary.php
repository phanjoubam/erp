<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PosCartTemporary extends Model
{
    protected $table = 'ba_pos_temp_cart'; 
    public $timestamps = false;
}
